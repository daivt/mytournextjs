export const GTM_ID = process.env.NEXT_PUBLIC_GOOGLE_TAG_MANAGER_ID;
export const MKT_CODE = process.env.NEXT_PUBLIC_MKT_CODE;
export const MKT_CORE = process.env.NEXT_PUBLIC_MKT_CORE;

export const pageview = (url) => {
  window &&
    window.dataLayer &&
    window.dataLayer.push({
      event: "pageview",
      page: url,
    });
};

export const addEventGtm = (eventName = "", data = {}) => {
  window &&
    window.dataLayer &&
    window.dataLayer.push({
      event: eventName,
      ...data,
    });
};
