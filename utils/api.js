import axios from "axios";
import Qs from "qs";
import cookie from "js-cookie";

import * as constants from "@utils/constants";
import { isEmpty, getAppHash, getDeviceInfo, getPlatform } from "./helpers";

const request = axios.create();

let isAlreadyFetchingAccessToken = false;
let subscribers = [];

const ISSERVER = typeof window === "undefined";
let deviceId = null;
if (!ISSERVER) {
  const key = `${process.env.NEXT_PUBLIC_APP_ID}_v2021-05-03`;
  deviceId = `${new Date().valueOf()}-${Math.random()}`;
  const value = localStorage.getItem(key);
  if (value === null) {
    localStorage.setItem(key, deviceId);
  } else {
    deviceId = value;
  }
}

function onAccessTokenFetched(access_token) {
  subscribers = subscribers.map((callback) => callback(access_token));
  subscribers = [];
}

function addSubscriber(callback) {
  subscribers.push(callback);
}

request.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => Promise.reject(error)
);

request.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    const originalRequest = error.config;
    if (
      error.response &&
      error.response.status === 401 &&
      isEmpty(error.response.data)
    ) {
      const refreshToken = cookie.get(constants.REFRESH_TOKEN);
      if (
        refreshToken &&
        !originalRequest._retry &&
        error.config.url.indexOf("/public-api/v1/login") === -1
      ) {
        originalRequest._retry = true;
        if (!isAlreadyFetchingAccessToken) {
          isAlreadyFetchingAccessToken = true;
          const dataDTO = {
            refresh_token: refreshToken,
            grant_type: "refresh_token",
          };
          // login(dataDTO)
          //   .then(({ data }) => {
          //     isAlreadyFetchingAccessToken = false;
          //     // update token to cookie
          //     cookie.set(constants.TOKEN, data.access_token);
          //     cookie.set(constants.REFRESH_TOKEN, data.refresh_token);
          //     onAccessTokenFetched(data.access_token);
          //   })
          //   .catch(() => {
          //     subscribers = [];
          //     isAlreadyFetchingAccessToken = false;
          //     cookie.remove(constants.TOKEN);
          //     cookie.remove(constants.REFRESH_TOKEN);
          //   });
        }
        const retryOriginalRequest = new Promise((resolve) => {
          addSubscriber((access_token) => {
            originalRequest.headers.Authorization = `Bearer ${access_token}`;
            resolve(axios(originalRequest));
          });
        });
        return retryOriginalRequest;
      } else {
        subscribers = [];
        isAlreadyFetchingAccessToken = false;
        cookie.remove(constants.TOKEN);
        cookie.remove(constants.REFRESH_TOKEN);
      }
    } else {
      return Promise.reject(error.response || { data: {} });
    }
  }
);

const getBaseUrl = (severVice = "") => {
  switch (severVice) {
    case constants.HOTEL_SERVICE:
      return `${process.env.NEXT_PUBLIC_DOMAIN_GATE}${constants.BASE_URL_HOTEL_SERVICE}`;
    case constants.PAYMENT_SERVICE:
      return `${process.env.NEXT_PUBLIC_DOMAIN_GATE}${constants.BASE_URL_PAYMENT_SERVICE}`;
    case constants.PROMOTION_SERVICE:
      return `${process.env.NEXT_PUBLIC_DOMAIN_GATE}${constants.BASE_URL_PROMOTION_SERVICE}`;
    case constants.AUTH_SERVICE:
      return `${process.env.NEXT_PUBLIC_DOMAIN_GATE}${constants.BASE_URL_AUTH_SERVICE}`;
    case constants.ACCOUNT_SERVICE:
      return `${process.env.NEXT_PUBLIC_DOMAIN_GATE}${constants.BASE_URL_ACCOUNT_SERVICE}`;
    case constants.FLIGHT_SERVICE:
      return `${process.env.NEXT_PUBLIC_DOMAIN_BACKEND_CORE}`;
    case constants.MY_TOUR_SERVICE:
      return constants.BASE_URL_MY_TOUR_SERVICE;
    default:
      return constants.BASE_URL;
  }
};
const api = (options = {}) => {
  if (!options.noAuthentication) {
    if (!isEmpty(cookie.get(constants.TOKEN))) {
      options.headers = {
        ...options.headers,
        login_token: `${cookie.get(constants.TOKEN)}`,
      };
    }
  }
  options.headers = {
    ...options.headers,
    caId: 17,
    deviceId,
    deviceInfo: getDeviceInfo(),
    ["accept-language"]: "vi-VN",
  };
  if (
    options.severVice === constants.HOTEL_SERVICE ||
    options.severVice === constants.PAYMENT_SERVICE ||
    options.severVice === constants.PROMOTION_SERVICE ||
    options.severVice === constants.AUTH_SERVICE ||
    options.severVice === constants.ACCOUNT_SERVICE
  ) {
    options.headers = {
      ...options.headers,
      lang: "vi",
      platform: getPlatform(),
      appId: process.env.NEXT_PUBLIC_APP_ID,
      appHash: getAppHash(),
      version: constants.VERSION_HOTEL_SERVICE,
    };
  }
  if (options.severVice === constants.MY_TOUR_SERVICE) {
    options.headers = {
      ...options.headers,
      ["X-Auth-UserId"]: "G8BKFJLD8HG6GUH",
      // ["X-Auth-UserId"]: "admin123",
      // ["Mytour-OS"]: "Android",
      ["X-App-Mytour"]: "mytour",
      // ["User-Agent"]: navigator.userAgent,
    };
    delete options.headers.caId;
    delete options.headers.deviceId;
    delete options.headers.deviceInfo;
    delete options.headers.login_token;
  }

  return request({
    baseURL: getBaseUrl(options.severVice),
    ...options,
    paramsSerializer: (params) =>
      Qs.stringify(params, { arrayFormat: "repeat" }),
    headers: {
      ...options.headers,
    },
  });
};

export default api;
