export const TOKEN = "token";
export const REFRESH_TOKEN = "refresh_token";
export const EXPIRED_REFRESH_TOKEN = 30 * 24 * 60 * 60 * 1000;
export const EXPIRED_TIME = 30 * 60 * 1000;
export const LONG_TRANSIT_DURATION = 5 * 3600 * 1000;
export const DOMAIN_GATE = "https://gate.dev.tripi.vn";
export const BASE_URL_HOTEL_SERVICE = "/hotels";
export const BASE_URL_PAYMENT_SERVICE = "/payment";
export const BASE_URL_PROMOTION_SERVICE = "/promotion";
export const BASE_URL_AUTH_SERVICE = "/auth";
export const BASE_URL_ACCOUNT_SERVICE = "/account";
export const BASE_URL_FLIGHT_SERVICE = "https://dev-api.tripi.vn";
export const BASE_URL_MY_TOUR_SERVICE = "https://api.mytour.vn";
export const BASE_URL = "https://contents-api-dev.mytour.vn";
export const BASE_URL_CONDITION_MYTOUR =
  "https://mytour.vn/news/135152-chinh-sach-va-quy-dinh-chung.html";
export const HOTEL_SERVICE = "HOTEL_SERVICE";
export const FLIGHT_SERVICE = "FLIGHT_SERVICE";
export const PAYMENT_SERVICE = "PAYMENT_SERVICE";
export const PROMOTION_SERVICE = "PROMOTION_SERVICE";
export const MY_TOUR_SERVICE = "MY_TOUR_SERVICE";
export const AUTH_SERVICE = "AUTH_SERVICE";
export const ACCOUNT_SERVICE = "ACCOUNT_SERVICE";
/////////CONFIG SERVER HOTEL SERVICE///////////////
export const APP_ID = "afwsax2y0smfv9wgg43kcabc9r9ksk1j";
export const HASH_KEY = "91%qc2!Nbd%840nFRZ0#5Y^8oevm#sHc";
export const VERSION_HOTEL_SERVICE = "1.0";

/////////data save local storage ///////////
export const LAST_HOTEL_BROWSER_ITEMS = "last-hotel-browser-items";
export const LAST_LOCATION_RECENT_SEARCH_HISTORY_ITEMS =
  "last-location-recent-search-history-items";
export const LAST_SEARCH_HISTORY = "last-search-history";
export const LAST_HOTEL_FAVORITES = "list-hotel-favorites";
export const LAT_LONG_USER = "lat-long-user";
export const LAST_BOOKING_HOTEL = "last-booking-hotel";
export const LAST_BOOKING_HOTEL_INFO = "last-booking-hotel-info";
export const LAST_REGISTER_INFO = "last-register-info";
export const SEARCH_RECENT = "SEARCH_RECENT";
export const SEARCH_PREV = "SEARCH_PREV";
////////////////////////////////////////////

export const PAYMENT_HOLDING_CODE = "PL";
export const PAYMENT_TRIPI_CREDIT_CODE = "CD";
export const PAYMENT_ATM_CODE = "ATM";
export const PAYMENT_VISA_CODE = "VM";
export const PAYMENT_VNPAY_CODE = "QR";
export const PAYMENT_TRANSFER_CODE = "DBT";
export const DELAY_TIMEOUT_POLLING = 500;
export const RADIUS_GET_HOTEL_LAT_LONG = 5;

export const listEventHotel = {
  HotelViewList: "HotelViewList",
  HotelViewDetail: "HotelViewDetail",
  HotelSearchBySlug: "HotelSearchBySlug",
  HotelSearchByName: "HotelSearchByName",
  HotelInitCheckout: "HotelInitCheckout",
  HotelAddToCart: "HotelAddToCart",
  HotelPurchase: "HotelPurchase",
  HotelAddPaymentInfo: "HotelAddPaymentInfo",
  HotelViewListPromo: "HotelViewListPromo",
  HotelAddPromo: "HotelAddPromo",
  HotelPurchaseCompleted: "HotelPurchaseCompleted",
  HotelPurchaseFailed: "HotelPurchaseFailed",
  HotelFiltered: "HotelFiltered",
};
export const listEventFlight = {
  FlightViewListTicket: "FlightViewListTicket",
  FlightViewDetail: "FlightViewDetail",
  FlightSearchOneDirection: "FlightSearchOneDirection",
  FlightSearchRoundTrip: "FlightSearchRoundTrip",
  FlightInitCheckout: "FlightInitCheckout",
  FlightAddToCart: "FlightAddToCart",
  FlightPurchase: "FlightPurchase",
  FlightAddPaymentInfo: "FlightAddPaymentInfo",
  FlightAddInsurance: "FlightAddInsurance",
  FlightViewListPromo: "FlightViewListPromo",
  FlightAddPromo: "FlightAddPromo",
  FlightPurchaseCompleted: "FlightPurchaseCompleted",
  FlightPurchaseFailed: "FlightPurchaseFailed",
};

export const optionClean = {
  allowedTags: [
    "h3",
    "h4",
    "h5",
    "h6",
    "blockquote",
    "p",
    "a",
    "ul",
    "ol",
    "nl",
    "li",
    "b",
    "i",
    "strong",
    "em",
    "strike",
    "code",
    "hr",
    "br",
    "div",
    "table",
    "thead",
    "caption",
    "tbody",
    "tr",
    "th",
    "td",
    "pre",
    "iframe",
    "img",
  ],
  allowedAttributes: {
    a: ["href", "name", "target"],
    img: ["src", "style", "alt", "width", "height"],
    p: ["style"],
  },
  allowedClasses: {
    "*": ["text-uppercase", "red"],
  },
};
export const formatDate = {
  firstYear: "YYYY-MM-DD",
  firstDay: "DD-MM-YYYY",
  dayAndMonth: "DD/MM",
  placeholder: "dd/mm/yyyy",
};

export const routeStatic = {
  HOME: {
    href: "/",
    as: "/",
  },
  HOTEL: {
    href: "/khach-san",
  },
  FLIGHT: {
    href: "/ve-may-bay",
    as: "/ve-may-bay",
  },
  BLOG: {
    href: "/location",
  },
  HOTEL_DETAIL: {
    href: "/khach-san/[alias]",
    as: "/khach-san/[alias]",
  },
  LISTING_HOTEL: {
    href: "/khach-san/[...slug]",
    as: "/khach-san/[...slug]",
  },
  TOP_PRICE_HOTEL: {
    href: "/khach-san/khach-san-gia-tot/[...slug]",
    as: "/khach-san/khach-san-gia-tot/[...slug]",
  },
  SIGN_UP: {
    href: "/sign-up",
  },
  FAVORITE_DESTINATIONS_HOTEL_LIST: {
    href: "/diem-den-yeu-thich",
  },
  FLIGHT_GOOD_PRICES: {
    href: "/ve-may-bay/chang-bay-gia-tot",
  },
  FLIGHT_LANDING: {
    href: "/ve-may-bay/[...slug]",
  },
  FLIGHT_LISTING_RESULT: {
    href: "/ve-may-bay/listing",
  },
  HOTEL_HOT_DEALS: {
    href: "/khach-san-gia-tot",
  },
  TOP_HOTEL: {
    href: "/top-khach-san-hang-dau",
  },
  BOOKING_HOTEL: {
    href: "/khach-san/dat-phong",
  },
  PAYMENT_HOTEL: {
    href: "/khach-san/thanh-toan",
  },
  RESULT_PAYMENT_BOOKING: {
    href: "/khach-san/ket-qua-dat-phong/[transactionId]",
  },
  ACCOUNT: {
    href: "/tai-khoan",
  },
  ACCOUNT_ORDER_BOOKING: {
    href: "/tai-khoan/don-phong",
  },
  ACCOUNT_ORDER_BOOKING_DETAIL: {
    href: "/tai-khoan/don-phong/[id]",
  },
  ACCOUNT_ORDER_FLIGHT: {
    href: "/tai-khoan/ve-may-bay",
  },
  ACCOUNT_ORDER_FLIGHT_DETAIL: {
    href: "/tai-khoan/ve-may-bay/[id]",
  },
  ACCOUNT_FAVORITES_HOTEL: {
    href: "/tai-khoan/khach-san-yeu-thich",
  },
  ACCOUNT_INFO: {
    href: "/tai-khoan/doi-thong-tin",
  },
  REGISTER_ACCOUNT: {
    href: "/dang-ky",
  },
  REGISTER_ACCOUNT_STEP_2: {
    href: "/dang-ky/nhap-otp",
  },
  REGISTER_ACCOUNT_STEP_3: {
    href: "/dang-ky/nhap-otp/hoan-thanh",
  },
  LOGIN: {
    href: "/dang-nhap",
  },
  FORGET_PASSWORD: {
    href: "/quen-mat-khau",
  },
  FORGET_PASSWORD_STEP_2: {
    href: "/quen-mat-khau/nhap-otp",
  },
  FORGET_PASSWORD_STEP_3: {
    href: "/quen-mat-khau/nhap-otp/hoan-thanh",
  },
  TOP_HOTEL_BRAND: {
    href: "/thuong-hieu-khach-san-hang-dau",
  },
  REPAY_BOOKING_FLIGHT: {
    href: "/ve-may-bay/payment/paymentBookingHolding",
  },
  PARTNERSHIP: {
    href: "/partnership",
  },
  DOWNLOAD_APP: {
    href: "/tai-ung-dung",
  },
};

export const hotLine = {
  hotelHn: "024.7109.9999",
  hotelHcm: "028.7109.9998",
  flight: "1900 2083",
};

export const LINK_DOWLOAD_APP = {
  ios:
    "https://apps.apple.com/us/app/mytour-%C4%91%E1%BA%B7t-ph%C3%B2ng-v%C3%A9-m%C3%A1y-bay/id1149730203",
  android:
    "https://play.google.com/store/apps/details?id=vn.mytour.apps.android&hl=vi&gl=US",
};

export const EMAIL_HOTLINE = {
  HOTEL: "info@mytour.vn",
  MARKETING: "marketing@mytour.vn",
  SUPPORT: "hotro@mytour.vn",
};
export const TRAVEL_TYPE = [
  { name: "Công tác", type: "business" },
  { name: "Cặp đôi", type: "couple" },
  { name: "Gia đình", type: "family" },
  { name: "Bạn bè", type: "friend" },
  { name: "Cá nhân", type: "single" },
  { name: "Nhóm", type: "group" },
  { name: "Khác", type: "other" },
];

export const DIALOG_MESSAGE_TYPE = {
  ALERT: 1,
  HTML: 2,
};

export const socialAccount = {
  // local
  // gg_plus_id:
  //   "150457992313-n3grbvu122vsgi5kc962r4rc7jdhqds0.apps.googleusercontent.com",
  // facebook_id: "659508064601650",
  gg_plus_id:
    "701699179758-9tb2gjluvjnu2m218tka373fv62cq8rq.apps.googleusercontent.com",
  facebook_id: "857393964278669",
};

export const IMAGE_LOADING_BOOK_TICKET =
  "https://storage.googleapis.com/tripi-assets/mytour/banner/loading_flight.gif";

/**
 * Messages code display price
 */

/**
 * Danh sách loại ghế
 */
export const SEAT_TYPE_LIST = [
  {
    cid: 1,
    name: "Phổ thông",
    description: "Bay tiết kiệm, đáp ứng mọi nhu cầu cơ bản",
  },
  {
    cid: 2,
    name: "Phổ thông đặc biệt",
    description: "Chi phí hợp lý với bữa ăn ngon và chỗ để chân rộng rãi.",
  },
  {
    cid: 3,
    name: "Thương gia",
    description: "Bay đẳng cấp, với quầy làm thủ tục và khu ghế ngồi riêng.",
  },
  {
    cid: 4,
    name: "Hạng nhất",
    description: "Hạng cao cấp nhất, với dịch vụ 5 sao được cá nhân hoá.",
  },
];

/**
 * Danh sách loại ghế
 */
export const LIFT_TIME_LIST = [
  {
    id: 1,
    name: "Sáng sớm",
    from: "00:00",
    to: "06:00",
  },
  {
    id: 2,
    name: "Buổi sáng",
    from: "06:00",
    to: "12:00",
  },
  {
    id: 3,
    name: "Buổi chiều",
    from: "12:00",
    to: "18:00",
  },
  {
    id: 4,
    name: "Buổi tối",
    from: "18:00",
    to: "24:00",
  },
];

/**
 * Danh sách giá trị sắp xếp vé máy bay
 */
export const SORTER_FLIGHT_LIST = [
  {
    id: 1,
    name: "Đề xuất",
  },
  {
    id: 2,
    name: "Giá tăng dần",
  },
  {
    id: 3,
    name: "Thời gian cất cánh",
  },
  {
    id: 4,
    name: "Thời gian bay",
  },
];

/**
 * Số điểm dừng
 */
export const NUM_STOP_LIST = [
  { id: 0, name: "Bay thẳng" },
  { id: 1, name: "1 điểm dừng" },
  { id: 2, name: "> 2 điểm dừng" },
];

export const GENDER_LIST = [
  {
    id: "M",
    label: "Ông",
    label_1: "Nam",
  },
  {
    id: "F",
    label: "Bà",
    label_1: "Nữ",
  },
];

/**
 * Danh sách giá trị sắp xếp vé máy bay
 */
export const CUSTOMER_TYPE = {
  ADULT: 1,
  CHILDREN: 2,
  BABY: 3,
};
export const CUSTOMER_TYPE_TEXT = {
  ADULT: "adult",
  CHILDREN: "children",
  INFANT: "infant",
};

export const flightPaymentStatus = {
  FAIL: "fail",
  SUCCESS: "success",
  CANCELLED_BY_PROVIDER: "cancelled_by_provider",
  PENDING: "pending",
  AWAITING_PAYMENT: "awaiting-payment",
  WAITING_PAYMENT: "waiting_payment",
  COMPLETED: "completed",
  REFUNDED_POSTED: "refunded-posted",
  REFUNDED: "refunded",
  DISPUTED: "disputed",
  WAITING: "waiting",
  HOLDING: "holding",
  CANCELLING_HOLDING: "cancelling_holding",
  PAYMENT_SUCCESS: "payment_success",
};

export const flightPaymentMethodCode = {
  CREDIT: "CD",
  VISA_MASTER: "VM",
  QR_CODE: "QR",
  ATM: "ATM",
  HOLDING: "PL",
  CASH: "CA",
  BANK_TRANSFER: "BT",
  BANK_TRANSFER_2: "DBT",
};

export const genderLabel = {
  M: "Nam",
  F: "Nữ",
};

export const paymentTypes = {
  SUCCESS: "success",
  FAIL: "fail",
  HOLD_SUCCESS: "holding",
};

export const airlinesName = [
  {
    id: 1,
    name: "vietnam-airline",
  },
  {
    id: 2,
    name: "bamboo",
  },
  {
    id: 3,
    name: "pacific",
  },
  {
    id: 4,
    name: "vietjet",
  },
];

export const airlineColor = [
  {
    id: 1,
    color: "#D59E2A",
  },
  {
    id: 2,
    color: "#64AF53",
  },
  {
    id: 3,
    color: "#00529b",
  },
  {
    id: 4,
    color: "#E5322B",
  },
];

export const defaultCountry = {
  id: 232,
  code: "VN",
  name: "Viet Nam",
  phonecode: 84,
};

export const bannerHome =
  "https://storage.googleapis.com/tripi-assets/mytour/banner/home-banner.jpg";
export const bannerHotel =
  "https://storage.googleapis.com/tripi-assets/mytour/banner/banner_hotel.jpg";
export const bannerFlight =
  "https://storage.googleapis.com/tripi-assets/mytour/banner/banner_flight.jpg";

export const LINK_FLIGHT_BOADING_PASS =
  "https://storage.googleapis.com/tripi-assets/mytour/images/boading-pass.gif";

export const listString = {
  IDS_MT_PLACE_HOLDER_INPUT_SEARCH: "Nhập tên khách sạn, địa danh...",
  IDS_MT_PLACE_HOLDER_INPUT_SEARCH_1: "Tìm theo tên tỉnh, thành",
  IDS_MT_TEXT_SEARCH_HOTEL: "Tìm khách sạn",
  IDS_MT_TEXT_SEARCH: "Tìm kiếm",
  IDS_MT_TEXT_FROM: "Từ",
  IDS_MT_TEXT_TO: "Đến",
  IDS_MT_TEXT_MYTOUR: "Mytour.vn",
  IDS_MT_TEXT_MYTOUR_INTRO:
    "Đặt vé may bay, đặt phòng khách sạn siêu dễ dàng, quản lý đơn vé, đơn phòng siêu tiện lợi với ứng dụng Mytour.",
  IDS_MT_TEXT_MYTOUR_INTRO_REVIEW:
    "Mytour làm dịch vụ rất tốt, giá cả hợp lý. Nhân viên rất nhiệt tình, lịch sự. Tôi sẽ chọn dịch vụ của Mytour cho những lần đi du lịch sắp tới.",
  IDS_MT_TEXT_CUSTOMER: "Khách hàng",
  IDS_MT_TEXT_SUPPORT_ODER_FLIGHT: "Hỗ trợ đặt vé máy bay",
  IDS_MT_TEXT_HN: "Hà Nội:",
  IDS_MT_TEXT_HCM: "TPHCM:",
  IDS_MT_HOTLINE_TEXT_HN: "Hà Nội",
  IDS_MT_HOTLINE_TEXT_HCM: "TP Hồ Chí Minh",
  IDS_MT_TEXT_HOTLINE_HN: "(024) 7109 9999",
  IDS_MT_TEXT_HOTLINE_HCM: "(024) 7109 9998",
  IDS_MT_TEXT_HOTLINE_SUPPORT_ODER_FLIGHT: "1900 2083",
  IDS_MT_TEXT_SINGLE_CUSTOMER: "khách",
  IDS_MT_TEXT_CLIENT: "Hành khách",
  IDS_MT_TEXT_SINGLE_ROOM: "phòng",
  IDS_MT_TEXT_HOTEL: "Khách sạn",
  IDS_MT_TEXT_FLIGHT: "Vé máy bay",
  IDS_MT_TEXT_NUMBER_CUSTOMER_SEAT: "Số khách, hạng ghế",
  IDS_MT_TEXT_RATING_SEAT: "Hạng ghế",
  IDS_MT_TEXT_APPLY: "Áp dụng",
  IDS_MT_TEXT_ADULT: "Người lớn",
  IDS_MT_TEXT_CHILDREN: "Trẻ em",
  IDS_MT_TEXT_CHILDREN_AGE: "(2-11 tuổi)",
  IDS_MT_TEXT_BABY: "Em bé",
  IDS_MT_TEXT_BABY_AGE: "(< 2 tuổi)",
  IDS_MT_TEXT_LOAD_MORE: "Xem thêm",
  IDS_MT_TEXT_COMMON: "Phổ biến",
  IDS_MT_TEXT_ROUND_TRIP: "Khứ hồi",
  IDS_MT_TEXT_HOLDING_TICKET_PAY_LATER:
    "Chưa cần thanh toán ngay, đặt chỗ để giữ giá tốt trên Mytour",
  IDS_MT_TEXT_FILTER: "Bộ lọc",
  IDS_MT_TEXT_FILTER_SINGLE: "Lọc",
  IDS_MT_TEXT_CLEAR_FILTER: "Xóa lọc",
  IDS_MT_TEXT_FILTER_PRICE: "Mức giá",
  IDS_MT_TEXT_FILTER_PRICE_RANGE: "Khoảng giá",
  IDS_MT_TEXT_FILTER_PARTNER: "Hãng bay",
  IDS_MT_TEXT_FILTER_FLY_TIME: "Thời gian bay",
  IDS_MT_TEXT_FILTER_LIFT_TIME: "Thời gian cất cánh",
  IDS_MT_TEXT_FILTER_SEAT_TYPE: "Hạng vé",
  IDS_MT_TEXT_FILTER_FREE: "Miễn phí",
  IDS_MT_TEXT_FILTER_TYPE: "Loại hình",
  IDS_MT_TEXT_FILTER_LOCATION: "Địa điểm",
  IDS_MT_TEXT_FILTER_CONVENIENT: "Tiện nghi",
  IDS_MT_TEXT_TRAVEL_GUIDE: "Cẩm nang du lịch",
  IDS_MT_TEXT_LOGIN: "Đăng nhập",
  IDS_MT_TEXT_EMAIL: "Email",
  IDS_MT_TEXT_SIGNUP: "Đăng ký",
  IDS_MT_TEXT_REGISTER_MEMBER: "Đăng ký thành viên",
  IDS_MT_TEXT_REGISTER_BY_EMAIL: "Đăng ký bằng Email",
  IDS_MT_TEXT_REGISTER_DES1:
    "Bằng cách nhấn nút Đăng ký, bạn đã đồng ý với các",
  IDS_MT_TEXT_TERM_AND_POLICY: "Điều khoản dịch vụ và chính sách bảo mật",
  IDS_MT_TEXT_REGISTER_DES2: "của Mytour.vn",
  IDS_MT_TEXT_REGISTER_DES3: "Đã có tài khoản Mytour.vn?",
  IDS_MT_TEXT_REGISTER_DES4: "Lợi ích khi tạo tài khoản",
  IDS_MT_TEXT_REGISTER_DES5: "Đặt phòng giảm giá đến 40%.",
  IDS_MT_TEXT_REGISTER_DES6: "Tích lũy điểm thưởng Vpoint với mỗi đơn phòng",
  IDS_MT_TEXT_REGISTER_DES7: "Nhận nhiều ưu đãi đặc biệt",
  IDS_MT_TEXT_SIGN_UP_VIA_GG: "Đăng ký bằng Google",
  IDS_MT_TEXT_LOGIN_VIA_GG: "Đăng nhập bằng Google",
  IDS_MT_TEXT_SIGN_UP_VIA_FB: "Đăng ký bằng Facebook",
  IDS_MT_TEXT_LOGIN_VIA_FB: "Đăng nhập bằng Facebook",
  IDS_MT_TEXT_CREATE_NEW_ACCOUNT: "Tạo tài khoản mới?",
  IDS_MT_TEXT_REMENBER_SIGNIN: "Nhớ đăng nhập",
  IDS_MT_TEXT_FORGET_PASSWORD: "Quên mật khẩu",
  IDS_MT_TEXT_FORGET_PASSWORD_DES1: "Vui lòng nhập email để khôi phục mật khẩu",
  IDS_MT_TEXT_ENTER_EMAIL: "Nhập địa chỉ email",
  IDS_MT_TEXT_SEND_REQUEST: "Gửi yêu cầu",
  IDS_MT_TEXT_RETURN_PAGE: "Quay lại trang vừa xem",
  IDS_MT_TEXT_RETURN_ORDER: "Quay lại đơn hàng",
  IDS_MT_TEXT_STEP_FORGET_PASSWORD: "Các bước tiến hành khôi phục mật khẩu:",
  IDS_MT_TEXT_STEP_FORGET_PASSWORD_1:
    "1. Nhập email đăng ký và gửi tới hệ thống.",
  IDS_MT_TEXT_STEP_FORGET_PASSWORD_2: "2. Check email và click vào đường link.",
  IDS_MT_TEXT_STEP_FORGET_PASSWORD_3: "3. Tiến hành đổi mật khẩu",
  IDS_MT_TEXT_SEND_REQUEST_SUCCESS: "Gửi yêu cầu thành công",
  IDS_MT_TEXT_FORGET_PASSWORD_DES2: "Yêu cầu khôi phục mật khẩu từ:",
  IDS_MT_TEXT_FORGET_PASSWORD_DES3:
    "vừa được gửi tới hệ thống. Vui lòng check email để tiến hành Reset mật khẩu.",
  IDS_MT_TEXT_NEW_PASSWORD: "Mật khẩu mới",
  IDS_MT_TEXT_PASSWORD: "Mật khẩu",
  IDS_MT_TEXT_CONFIRM_PASSWORD: "Xác nhận lại mật khẩu",
  IDS_MT_TEXT_CODE_OTP: "Mã OTP",
  IDS_MT_TEXT_CLOSE: "Đóng",
  IDS_MT_TEXT_CANCEL_VN: "Hủy",
  IDS_MT_TEXT_SELECT: "Chọn",
  IDS_MT_TEXT_BOOKING: "Đặt phòng",
  IDS_MT_TEXT_DEPARTURE: "Chiều đi",
  IDS_MT_TEXT_DEPARTURE_LOWCASE: "chiều đi",
  IDS_MT_TEXT_RETURN: "Chiều về",
  IDS_MT_TEXT_RETURN_LOWCASE: "chiều về",
  IDS_MT_TEXT_FREE_CANCEL: "Miễn phí hoàn hủy",
  IDS_MT_TEXT_CONFIRM_NOW: "Xác nhận ngay",
  IDS_MT_TEXT_CONFIRM_AROUNR_30_MINUTES: "Xác nhận trong 30 phút",
  IDS_MT_TEXT_INFO_CONTACT: "Thông tin liên hệ",
  IDS_MT_TEXT_INFO_FLIGHT_CONTACT_DESC:
    "Mã đặt chỗ sẽ được gửi theo thông tin liên hệ dưới đây",
  IDS_MT_TEXT_INFO_CUSTOMER_FLIGHT: "Thông tin hành khách",
  IDS_MT_TEXT_ADD_PASSPORT: "Thêm hộ chiếu",
  IDS_MT_TEXT_PASSPORT: "Hộ chiếu",
  IDS_MT_TEXT_INFO_CUSTOMER_FLIGHT_NOTE_NO_ACCENT:
    "Nhập tiếng Việt không dấu, không có ký tự đặc biệt.",
  IDS_MT_TEXT_INFO_CUSTOMER_FLIGHT_NOTE_NAME:
    "Nhập họ tên trùng khớp trên giấy tờ tùy thân.",
  IDS_MT_TEXT_PAYMENT_MENTHODS: "Phương thức thanh toán",
  IDS_MT_TEXT_ADD_BAGGAGE: "Thêm hành lý ký gửi",
  IDS_MT_TEXT_BAGGAGE_NOTE:
    "Thêm hành lý để thuận tiện hơn cho chuyến đi. Mua bây giờ rẻ hơn tại quầy",
  IDS_MT_TEXT_DETAIL_ROOM: "Chi tiết đơn phòng",
  IDS_MT_TEXT_PLACEHOLDER_ENTER_PHONE_NUMBER: "Nhập số điện thoại",
  IDS_MT_TEXT_PLACEHOLDER_ENTER_ADDRESS: "Tỉnh/Thành (tùy chọn)",
  IDS_MT_TEXT_PHONE_NUMBER: "Số điện thoại",
  IDS_MT_TEXT_REQUEST_SPECIAL: "Yêu cầu đặc biệt",
  IDS_MT_TEXT_INVOICE: "Xuất hóa đơn",
  IDS_MT_TEXT_PAYMENT: "Thanh toán",
  IDS_MT_TEXT_PLACEHOLDER_ENTER_REQUEST_OTHER: "Nhập yêu cầu khác",
  IDS_MT_TEXT_COMPANY_NAME: "Tên công ty",
  IDS_MT_TEXT_CODE_INVOICE: "Mã số thuế",
  IDS_MT_TEXT_ADDRESS_COMPANY: "Địa chỉ công ty",
  IDS_MT_TEXT_ADDRESS_INVOICE: "Địa chỉ nhận hóa đơn",
  IDS_MT_TEXT_PLACEHOLDER_ENTER_ADDRESS_INVOICE: "Nhập địa chỉ nhận hóa đơn",
  IDS_MT_TEXT_CHECK_IN: "Check-In",
  IDS_MT_TEXT_CHECK_OUT: "Check-Out",
  IDS_MT_TEXT_VAT: "Thuế VAT (10%)",
  IDS_MT_TEXT_TRANSACTION_FEE: "Phí giao dịch",
  IDS_MT_TEXT_TOTAL_PRICE: "Tổng tiền",
  IDS_MT_TEXT_TOTAL: "Tổng",
  IDS_MT_TEXT_PLEASE_ENTER_EMAIL: "Bạn chưa nhập email",
  IDS_MT_TEXT_EMAIL_INVALIDATE: "Địa chỉ email sai định dạng.",
  IDS_MT_TEXT_PLEASE_ENTER_PASSWORD: "Vui lòng nhập mật khẩu",
  IDS_MT_TEXT_PLEASE_ENTER_OTP: "Vui lòng nhập mã OTP",
  IDS_MT_TEXT_VALIDATE_OTP: "Mã OTP chỉ có 6 kí tự",
  IDS_MT_TEXT_PLEASE_ENTER_CONFIRM_PASSWORD: "Vui lòng nhập lại mật khẩu",
  IDS_MT_TEXT_VALIDATE_PASSWORD: "Mật khẩu phải từ 6 đến 50 kí tự",
  IDS_MT_TEXT_VALIDATE_FULLNAME: "Họ và tên phải có ít nhất 2 từ",
  IDS_MT_TEXT_VALIDATE_NAME_MAX: "Họ và tên không được quá 50 kí tự",
  IDS_MT_TEXT_VALIDATE_EMAIL_MAX: "Email không được quá 50 kí tự",
  IDS_MT_TEXT_PASSWORD_CONFIRM_NOT_MATCH_PASSWORD:
    "Mật khẩu không trùng với mật khẩu trên",
  IDS_MT_TEXT_PLACEHOLDER_ENTER_CODE_SALE_OFF: "Nhập mã giảm giá",
  IDS_MT_TEXT_PLEASE_ENTER_YOUR_NAME: "Bạn chưa nhập họ tên.",
  IDS_MT_TEXT_PLEASE_ENTER_PHONE_NUMBER: "Bạn chưa nhập số điện thoại",
  IDS_MT_TEXT_PLEASE_ENTER_GENDER: "Bạn chưa chọn giới tính",
  IDS_MT_TEXT_PLEASE_ENTER_DOB: "Bạn chưa chọn ngày sinh",
  IDS_MT_TEXT_PAYMENT_METHOD_1: "Chuyển khoản",
  IDS_MT_TEXT_PAYMENT_METHOD_3: "Thẻ tín dụng",
  IDS_MT_TEXT_PAYMENT_METHOD_3_DES_5:
    "Bằng cách nhấp vào nút Thanh toán, bạn đã đồng ý với",
  IDS_MT_TEXT_PAYMENT_METHOD_3_DES_6: "Điều kiện và Điều khoản",
  IDS_MT_TEXT_PAYMENT_METHOD_3_DES_7: "của chúng tôi",
  IDS_MT_TEXT_CREATE_ACCOUNT_SUCCESS: "Đăng ký tài khoản thành công",
  IDS_MT_TEXT_LOGIN_ACCOUNT_SUCCESS: "Đăng nhập thành công.!",
  IDS_MT_TEXT_ERROR_ACTION: "Đã có lỗi xảy ra, xin vui lòng thử lại.!",
  IDS_MT_TEXT_SUCCESS_ACTION: "Yêu cầu thành công!",
  IDS_MT_TEXT_ACCOUNT: "Tài khoản",
  IDS_MT_TEXT_MANAGEMENT_BOOKING_ROOM: "Quản lý đơn phòng",
  IDS_MT_TEXT_HOTEL_FAVORITE: "Khách sạn yêu thích",
  IDS_MT_TEXT_MANAGEMENT_ACCOUNT: "Quản lý tài khoản",
  IDS_MT_TEXT_HOTEL_REVIEW: "Đánh giá khách sạn",
  IDS_MT_TEXT_LOGOUT: "Đăng xuất",
  IDS_MT_TEXT_LOGOUT_SUCCESS: "Đăng xuất thành công!",
  IDS_MT_TEXT_NOT_FOUND_404_DES_1: "404",
  IDS_MT_TEXT_NOT_FOUND_404_DES_2: "Oops…",
  IDS_MT_TEXT_NOT_FOUND_404_DES_3: "Trang này có lẽ đang đi du lịch rồi!",
  IDS_MT_TEXT_NOT_FOUND_404_DES_4: "Bạn tìm lại trang khác nhé",
  IDS_MT_TEXT_NOT_FOUND_403_DES_1: "403",
  IDS_MT_TEXT_NOT_FOUND_403_DES_2: "Bạn chưa có quyền truy cập trang này",
  IDS_MT_TEXT_NOT_FOUND_403_DES_3: "Quay lại trang chủ",
  IDS_MT_TEXT_NAME: "Họ & Tên",
  IDS_MT_TEXT_PLEASE_ENTER_NAME: "Vui lòng nhập họ & tên của bạn",
  IDS_MT_TEXT_PAYMENT_TA_DES_1:
    "Mytour sẽ chủ động liên hệ với quý khách trong vòng 30 phút để xác nhận phòng trống và hướng dẫn thanh toán.",
  IDS_MT_TEXT_PAGE_HOME: "Trang chủ",
  IDS_MT_TEXT_ACCOUNT_USER: "Tài khoản cá nhân",
  IDS_MT_TEXT_VPOINT: "Điểm thưởng Vpoint",
  IDS_MT_TEXT_INFO_ACCOUNT: "Thông tin tài khoản",
  IDS_MT_TEXT_CURRENT_NO_INFO: "Hiện chưa có thông tin",
  IDS_MT_TEXT_ADDRESS_MY_HOME: "Địa chỉ nhà",
  IDS_MT_TEXT_DISTRICT_OR_CITY: "Tỉnh/ Thành phố",
  IDS_MT_TEXT_EDIT: "Chỉnh sửa",
  IDS_MT_TEXT_INFO_INVOICE: "Thông tin xuất hóa đơn",
  IDS_MT_TEXT_SAVE: "Lưu",
  IDS_MT_TEXT_CANCEL: "Cancel",
  IDS_MT_TEXT_INFO_SETTING: "Thông tin tùy chỉnh",
  IDS_MT_TEXT_REGISTER_EMAIL_FROM_MYTOUR:
    "Đăng ký nhận Email từ Mytour.vn (thông tin khuyến mãi, thông báo)",
  IDS_MT_TEXT_RECEIVE_EMAIL_SALE: "Nhận Email thông tin khuyến mãi",
  IDS_MT_TEXT_RECEIVE_EMAIL_NOTIFICATION: "Nhận Email thông báo",
  IDS_MT_TEXT_GENDER: "Giới tính",
  IDS_MT_TEXT_DATE_OF_BIRTH: "Ngày sinh",
  IDS_MT_TEXT_DATE_OF_BIRTH_INVALID: "Ngày sinh không hợp lệ",
  IDS_MT_TEXT_FILTER_FOLLOW: "Lọc theo",
  IDS_MT_TEXT_VIEW_DETAIL: "Xem chi tiết",
  IDS_MT_TEXT_CODE_ROOM: "Mã phòng",
  IDS_MT_TEXT_NUMBER_NIGHT: "Số đêm",
  IDS_MT_TEXT_EQUAL: "bằng",
  IDS_MT_TEXT_STATUS_PAYMENT_WAITTING: "Chờ thanh toán",
  IDS_MT_TEXT_STATUS_PAYMENT_DONE: "Đã thanh toán",
  IDS_MT_TEXT_STATUS_PAYMENT_CANCEL: "Đã hủy",
  IDS_MT_TEXT_RECHECK: "Kiểm tra lại",
  IDS_MT_TEXT_BOOKING_ROOM: "Phòng đặt",
  IDS_MT_TEXT_NO_REVIEW_DES: "Hiện tại bạn chưa có đánh giá về khách sạn này",
  IDS_MT_TEXT_WRITE_REVIEW_HOTEL: "Viết đánh giá",
  IDS_MT_TEXT_WRITE_REVIEW_HOTEL_DETAIL: "Chi tiết đánh giá",
  IDS_MT_TEXT_TYPE_TRIP: "Kiểu du lịch",
  IDS_MT_TEXT_TITLE: "Tiêu đề",
  IDS_MT_TEXT_PLAHOLDER_TITLE: "Nhập tiêu đề đánh giá",
  IDS_MT_TEXT_CONTENT: "Nội dung",
  IDS_MT_TEXT_PLAHOLDER_CONTENT: "Vui lòng nhập nội dung đánh giá của bạn",
  IDS_MT_TEXT_INFO_BOOKING_ROOM: "Thông tin đặt phòng",
  IDS_MT_TEXT_CODE_INVOICE_ROOM: "Mã đơn phòng",
  IDS_MT_TEXT_DATE_BOOKING: "Ngày đặt",
  IDS_MT_TEXT_PRICE_BOOKING: "Chi phí đặt phòng",
  IDS_MT_TEXT_POLICY_CANCEL_BOOKING_1:
    "Hoàn hủy, thay đổi trong vòng 7 ngày đến khi nhận phòng, tính phí 100% tổng tiền",
  IDS_MT_TEXT_POLICY_CANCEL_BOOKING_2:
    "Không tính phí hoàn hủy, thay đổi nếu báo trước 7 ngày",
  IDS_MT_TEXT_PRICE_DISCOUNT: "Tiền giảm trừ",
  IDS_MT_TEXT_PAYMENT_VPOINT: "Thanh toán Vpoint",
  IDS_MT_TEXT_NO_SUPPORT_BAGGAGE:
    "Chúng tôi chưa hỗ trợ mua hành lý cho chặng bay này",
  IDS_MT_TEXT_PRICE_EX: "Phí phụ thu",
  IDS_MT_TEXT_PAYMENT_ACTION: "Thanh toán",
  IDS_MT_TEXT_PAYMENT_CONFIRM: "Xác nhận thanh toán",
  IDS_MT_TEXT_PAYMENT_CONFIRM_TEXT: "Bạn đang tiến hành thanh toán",
  IDS_MT_TEXT_TOTAL_PRICE_EX: "(đã bao gồm phụ phí)",
  IDS_MT_TEXT_FREE_SURCHANGE: "Miễn phụ phí",
  IDS_MT_TEXT_BOOKING_AGAIN: "Đặt lại khách sạn này",
  IDS_MT_TEXT_ASK_PAYMENT_DONE: "Bạn đã thanh toán thành công?",
  IDS_MT_TEXT_NOT_PAYMENT_BY_MY: "Bạn vẫn chưa thanh toán?",
  IDS_MT_TEXT_PAYMENT_DONE_BY_MY: "Tôi đã thanh toán",
  IDS_MT_TEXT_ASK_PAYMENT_DONE_DES:
    "Bạn vui lòng thông báo khi đã hoàn tất thanh toán để Mytour xác nhận đơn phòng thành công nhanh hơn nhé.",
  IDS_MT_TEXT_PLACE_HOLDER_SEARCH_BLOG: "Tìm địa danh, ẩm thực, review,…",
  IDS_MT_TEXT_DISCOVER: "Khám phá",
  IDS_MT_TEXT_CATEGORY_LINK: "Chủ đề liên quan",
  IDS_MT_TEXT_POPULAR_PLACE: "Địa danh nổi bật",
  IDS_MT_TEXT_TOP_KEY_WORK: "Từ khóa nổi bật",
  IDS_MT_TEXT_NEW_BLOG: "Bài viết mới",
  IDS_MT_TEXT_HOTEL_NEAR: "Khách sạn liên quan",
  IDS_MT_TEXT_CAN_YOU_RELATE: "Có thể bạn quan tâm",
  IDS_MT_TEXT_TOP_FLIGHT_DISCOVERY:
    "Những chuyến bay giá tốt nhất trong tháng khởi hành từ",
  IDS_MT_TEXT_FLIGHT_START_DATE: "Khởi hành",
  IDS_TEXT_DISCOUNT_CARD_EXPIRED: "Hạn sử dụng",
  IDS_TEXT_USE_CODE_WHEN_PAYMENT: "Nhập mã khi thanh toán",
  IDS_TEXT_SEND_MAIL_SMS:
    "Sau khi hoàn tất thanh toán, vé điện tử sẽ được gửi ngay qua SMS và Email của bạn.",
  IDS_TEXT_CONDITION_AND_RULES: "Điều kiện & thể lệ chương trình",
  IDS_TEXT_USE: "Sử dụng",
  IDS_TEXT_VOUCHER_SELECT: "Chọn mã giảm giá",
  IDS_TEXT_VOUCHER_TIPS: "Hoặc chọn một mã giảm giá dưới đây",
  IDS_MT_TEXT_FAVORITE: "Yêu thích",
  IDS_MT_TEXT_ODER: "Đơn hàng",
  IDS_MT_TEXT_SUPPORT_247: "Hỗ trợ khách hàng 24/7",
  IDS_MT_TEXT_FLY: "Chuyến bay",
  IDS_TEXT_WHY_BOOK_HOTEL_SUPPORT_247_DESC:
    "Chat là có, gọi là nghe, không quản đêm hôm, ngày nghỉ và ngày lễ.",
  IDS_TEXT_WHY_BOOK_HOTEL_TICKET_TITLE: "Hơn 8000+ khách sạn dọc Việt Nam",
  IDS_TEXT_WHY_BOOK_HOTEL_TICKET_DESC:
    "Hàng nghìn khách sạn, đặc biệt là 4 sao và 5 sao, cho phép bạn thoải mái lựa chọn, giá cạnh tranh, phong phú.",
  IDS_TEXT_WHY_BOOK_HOTEL_SECURE_PAYMENT_TITLE: "Thanh toán bảo mật",
  IDS_TEXT_WHY_BOOK_HOTEL_SECURE_PAYMENT_DESC:
    "Bạn không cần lo lắng về tài khoản thanh toán",
  IDS_TEXT_WHY_BOOK_HOTEL_WALLET_TITLE: "Tích điểm",
  IDS_TEXT_WHY_BOOK_HOTEL_WALLET_DESC:
    "Bạn không cần lo lắng về tài khoản thanh toán",
  IDS_TEXT_WHY_BOOK_FLIGHT_SUPPORT_TITLE: "Dịch vụ khách hàng xuất sắc",
  IDS_TEXT_WHY_BOOK_FLIGHT_SUPPORT_DESC:
    "Chat là có, gọi là nghe, không quản đêm hôm, ngày nghỉ và ngày lễ.",
  IDS_TEXT_WHY_BOOK_FLIGHT_TICKET_TITLE: "Săn vé giá tốt nội địa, quốc tế",
  IDS_TEXT_WHY_BOOK_FLIGHT_TICKET_DESC:
    "So sánh giá tốt nhất từ các hãng hàng không nội địa và 50.000 đường bay quốc tế.",
  IDS_TEXT_WHY_BOOK_FLIGHT_SECURE_PAYMENT_TITLE:
    "Ứng dụng tiện lợi, thanh toán đơn giản",
  IDS_TEXT_WHY_BOOK_FLIGHT_SECURE_PAYMENT_DESC:
    "Đặt vé nhanh chóng, chỉ cần 2 thao tác đơn giản với nhiều phương thức thanh toán.",
  IDS_TEXT_WHY_BOOK_FLIGHT: "Tại sao đặt vé trên Mytour?",
  IDS_TEXT_WHY_BOOK_HOTEL: "Tại sao chọn Mytour?",
  IDS_TEXT_CAN_BELIEVE: "Tin nổi không?",
  IDS_TEXT_HOT_PRICE_ON_MYTOUR: "Giá sốc chỉ có trên Mytour",
  IDS_TEXT_VIEW_ALL: "Xem tất cả",
  IDS_TEXT_CAN_YOU_LOVE: "Bạn có thể thích",
  IDS_TEXT_FLY_HOT_DEAL: "Chặng bay giá tốt nhất",
  IDS_TEXT_TOP_HOTEL_SEARCH_AND_BOOKING:
    "Top khách sạn được tìm kiếm và đặt nhiều nhất",
  IDS_TEXT_PEOPLE_CARE_THE_MOST: "Mọi người quan tâm nhiều nhất",
  IDS_TEXT_VIEW_LAST_HISTORY: "Xem gần đây",
  IDS_TEXT_VIEW_LAST_HISTORY_HOTEL: "Khách sạn bạn xem gần đây",
  IDS_TEXT_DESTINATION_FAVORITE: "Điểm đến yêu thích",
  IDS_TEXT_DESTINATION_HOT_MYTOUR_RECOMMENDED:
    "Địa điểm hot nhất do Mytour đề xuất",
  IDS_TEXT_HOTLINE_BOX_TITLE: "Hotline 24/7",
  IDS_TEXT_HOTLINE_BOX_DESC:
    "Sẵn sàng giải đáp thắc mắc của quý khách về đặt phòng khách sạn, vé máy bay",
  IDS_TEXT_CODE_DEALS_HOT: "Mã giảm giá siêu hot",
  IDS_TEXT_VOUCHER: "Mã giảm giá",
  IDS_TEXT_HOTEL_BIG_SALE: "Khách sạn sale khủng",
  IDS_MT_TEXT_CONTACT_NAME: "Họ tên (Ví dụ: NGUYEN VAN A)",
  IDS_MT_TEXT_CONTACT_NAME_SIMPLE: "Họ tên",
  IDS_MT_TEXT_CONTACT_SAME_INFO_CUSTOMER:
    "Thông tin liên hệ cũng là thông tin hành khách",
  IDS_TEXT_FIND_AND_BOOKING: "Tìm và đặt vé máy bay khuyến mãi, giá rẻ",
  IDS_TEXT_NOT_FOUND_INFO_CUSTOMER:
    "Không tồn tại thông tin hành khách. Vui lòng kiểm tra lại!.",
  IDS_TEXT_DAY_OUT: "Ngày đi",
  IDS_TEXT_DAY_IN: "Ngày về",
  IDS_TEXT_INSURANCE_TITLE: "Bảo hiểm",
  IDS_TEXT_BUY_INSURANCE: "Mua bảo hiểm",
  IDS_TEXT_INSURANCE_CHUBB_TITLE: "Được cung cấp bởi Chubb Việt Nam",
  IDS_TEXT_CHUBB_CONTENT_HEAD:
    "Mỗi hành trình, mỗi chuyến đi, Chubb luôn đồng hành!",
  IDS_TEXT_CHUBB_CONTENT_LI_01:
    "Lên đến 210 triệu đồng cho Quyền lợi Tai nạn Cá nhân.",
  IDS_TEXT_CHUBB_CONTENT_LI_02:
    "Lên đến 1,2 triệu đồng cho Quyền lợi Huỷ, Hoãn chuyến bay.",
  IDS_TEXT_CHUBB_CONTENT_LI_03:
    "Lên đến 1 triệu đồng cho Quyền lợi Hành lý bị chậm trễ.",
  IDS_TEXT_CHUBB_CONTENT_LINK:
    "Quyền lợi, Quy tắc bảo hiểm và Nội dung tuyên bố và uỷ quyền",
  IDS_TEXT_CHUBB_TERM_HREF:
    "https://docs.atuat.acegroup.com/aceStatic/Doc/VN/ChubbFlight/PolicyWordingTripi.pdf",
  IDS_TEXT_CHUBB_CONTENT_FOOTER_01:
    "Với việc lựa chọn Bảo hiểm Chubb Flight, tôi đã hiểu và đồng ý với",
  IDS_TEXT_CHUBB_CONTENT_FOOTER_02: "do Chubb quy định.",
  IDS_TEXT_CHUBB_CONTENT_NOTE:
    "Lưu ý: Nếu chuyến đi được bảo hiểm theo Hợp đồng có thay đổi, liên hệ hotline +84 28 3910 7300 hoặc email travel.vn@chubb.vn để điều chỉnh nhằm đảm bảo hợp đồng bảo hiểm có hiệu lực.",
  IDS_TEXT_DES_HOTEL_BANNER_MOBILE:
    "Phòng đẹp, giá tốt với những trải nghiệm tuyệt vời",
  IDS_MT_TEXT_PLAHOLDER_SEARCH_HOTEL_MOBILE: "Nhập địa điểm, khách sạn",
  IDS_TEXT_RECENT_SEARCH: "Tìm kiếm gần đây",
  IDS_TEXT_RECENT_SEARCH_FLIGTH: "Chuyến bay bạn tìm kiếm gần đây",
  IDS_TEXT_COUNT_CUSTOMER: "Số khách",
  IDS_MT_TEXT_ROOM: "Phòng",
  IDS_MT_TEXT_FIND_CITY_OR_AIRPORT: "Tìm thành phố hoặc sân bay",
  IDS_MT_TEXT_ALLOW_GPS:
    "Mytour.vn sử dụng vị trí hiện tại của bạn để tìm khách sạn gần bạn phù hợp nhất. Hãy cho phép truy cập vị trí của bạn.",
  IDS_MT_TEXT_ALLOW_GPS_SUBMIT: "Cấp quyền truy cập",
  IDS_MT_TEXT_FOR_ANOTHER_TIME: "Để lúc khác",
  IDS_MT_TEXT_CHECK_IN_ROOM: "Nhận phòng",
  IDS_MT_TEXT_CHECK_OUT_ROOM: "Trả phòng",
  IDS_MT_TEXT_SORT: "Sắp xếp",
  IDS_MT_TEXT_MAP: "Bản đồ",
  IDS_MT_TEXT_CANCEL_FILTER: "Xóa lọc",
  IDS_MT_TEXT_RANGE_PRICE_ONE_NIGHT: "Khoảng giá 1 đêm",
  IDS_MT_TEXT_STAR: "Xếp hạng",
  IDS_MT_TEXT_SERVICE_ATTACH: "Dịch vụ đi kèm",
  IDS_MT_TEXT_CANCEL_POLICY: "Hủy Linh Hoạt",
  IDS_MT_TEXT_USER_VOTE: "Người dùng đánh giá",
  IDS_MT_TEXT_USER_VOTE_GREAT: "Tuyệt vời (9.0+)",
  IDS_MT_TEXT_USER_VOTE_VERY_GOOD: "Rất tốt (8.0+)",
  IDS_MT_TEXT_USER_VOTE_GOOD: "Tốt (7.0+)",
  IDS_MT_TEXT_HOTEL_TYPE: "Loại khách sạn",
  IDS_MT_TEXT_HOTEL_TYPE_HOME_HOTEL: "Khách sạn",
  IDS_MT_TEXT_HOTEL_TYPE_HOTELS: "Hotels",
  IDS_MT_TEXT_AREA: "Khu vực",
  IDS_MT_TEXT_LOAD_COMPACT: "Rút gọn",
  IDS_MT_PLACE_HOLDER_INPUT_SEARCH_MOBILE: "Nhập tên khách sạn, địa điểm",
  IDS_MT_TEXT_REMOVE_RECENT_SEARCH: "Xóa lịch sử tìm kiếm",
  IDS_TEXT_FLIGHT_BEST_PRICE: "Giá rẻ nhất",
  IDS_TEXT_FLIGHT_RUNNING_OUT: "Sắp hết vé",
  IDS_TEXT_FLIGHT_NOT_FOUND: "Không có chuyến bay với ngày bạn chọn",
  IDS_TEXT_FLIGHT_CHANGE_SEARCH_INFO: "Vui lòng thay đổi thông tin tìm kiếm",
  IDS_MT_TEXT_TRY_AGAIN_SEARCH_KEYWORD: "Vui lòng thử từ khóa khác",
  IDS_MT_TEXT_NO_RESULT_ABOUT: "Không có kết quả cho",
  IDS_TEXT_EXPORT_INVOICE: "Xuất hoá đơn",
  IDS_TEXT_PLACEHOLDER_SEARCH_TAX_CODE: "Mã số thuế",
  IDS_TEXT_PLACEHOLDER_INVOICE_NAME: "Tên công ty",
  IDS_TEXT_PLACEHOLDER_INVOICE_ADDRESS: "Địa chỉ công ty",
  IDS_TEXT_PLACEHOLDER_INVOICE_RECEIVER: "Địa chỉ người nhận",
  IDS_TEXT_PLACEHOLDER_INVOICE_CUSTOMER_NAME: "Tên khách hàng",
  IDS_TEXT_PLACEHOLDER_INVOICE_NOTE: "Ghi chú",
  IDS_TEXT_CONDITION_EXPORT_INVOICE: "Điều kiện XHĐ",
  IDS_MT_TEXT_ONE_PARTIAL_CANCEL: "Hoàn hủy một phần",
  IDS_MT_TEXT_ROOM_FREE_BREAK_FAST: "Bữa sáng miễn phí",
  IDS_MT_TEXT_ROOM_NO_FREE_BREAK_FAST: "Không bao gồm bữa ăn sáng",
  IDS_MT_TEXT_ONLY: "Chỉ còn",
  IDS_MT_TEXT_ONLY_SHOW_PRICE_MOBILE: "Giá chỉ có trên điện thoại",
  IDS_MT_TEXT_TYPE_PROMO_CODE: "Nhập mã",
  IDS_MT_TEXT_BOOKING_NOW: "Đặt ngay",
  IDS_MT_TEXT_LOGIN_SHOW_PRICE: "Đăng nhập để xem giá",
  IDS_TEXT_NUM_STOPS_0: "Bay thẳng",
  IDS_TEXT_NUM_STOPS_1: "điểm dừng",
  IDS_MT_TEXT_NO_SEARCH_HOTEL: "Không có khách sạn với ngày bạn chọn",
  IDS_MT_TEXT_PLEASE_CHANGE_INFO_SEARCH: "Vui lòng thay đổi thông tin tìm kiếm",
  IDS_MT_TEXT_EVALUATE: "Đánh giá",
  IDS_MT_TEXT_EVALUATE_LEVEL_LOCATION: "Vị trí",
  IDS_MT_TEXT_EVALUATE_LEVEL_PRICE: "Giá cả",
  IDS_MT_TEXT_EVALUATE_LEVEL_SERVE: "Phục vụ",
  IDS_MT_TEXT_EVALUATE_LEVEL_CLEAN: "Vệ sinh",
  IDS_MT_TEXT_NO_FAVORITE_HOTEL: "Chưa có khách sạn yêu thích",
  IDS_MT_TEXT_SEE_REVIEW: "Xem đánh giá",
  IDS_MT_TEXT_COMPARE_PRICE_THREE_SOURCE:
    "Chúng tôi đã so sánh giá thấp nhất từ 3 nhà cung cấp khác!",
  IDS_MT_TEXT_FIND_NEAR_BY: "Tìm kiếm trong khu vực này",
  IDS_TEXT_BUTTON_ADD: "Thêm",
  IDS_TEXT_ADD_PASSPORT_MODAL_TITLE: "Thêm hộ chiếu",
  IDS_TEXT_PASSPORT_RESIDENCE: "Quốc tịch",
  IDS_TEXT_PASSPORT_COUNTRY: "Quốc gia cấp",
  IDS_TEXT_PASSPORT_NUMBER: "Số hộ chiếu",
  IDS_TEXT_PASSPORT_EXPIRED: "Ngày hết hạn",
  IDS_TEXT_INVALID_DEFAULT: "Bạn chưa điền đủ thông tin",
  IDS_TEXT_INVALID_PASSPORT_NUMBER: "Nhập số hộ chiếu",
  IDS_TEXT_INVALID_PASSPORT_EXPIRY: "Nhập ngày hết hạn",
  IDS_TEXT_RESIDENCE_DEFAULT: "Viet Nam",
  IDS_MT_SELECT_ROOM: "Chọn phòng",
  IDS_MT_HIGHT_LIGHT_ADDRESS: "Địa điểm nổi bật",
  IDS_TEXT_CONDITION_INVOICE_TITLE: "Điều kiện xuất hoá đơn",
  IDS_TEXT_CONDITION_INVOICE_CONTENT_1:
    "Hạn yêu cầu xuất hóa đơn: Quý khách cần cung cấp thông tin xuất hoá đơn trước hoặc ngay khi xuất vé.",
  IDS_TEXT_CONDITION_INVOICE_CONTENT_2:
    "Thời gian xuất hóa đơn: từ 5 đến 7 ngày làm việc.",
  IDS_TEXT_CONDITION_INVOICE_CONTENT_3:
    "Hóa đơn xuất sẽ được gửi về email mà quý khách cung cấp trong mục thông tin xuất hoá đơn.",
  IDS_TEXT_CONDITION_INVOICE_CONTENT_4:
    "Xuất theo giá trị đơn hàng không bao gồm phí thanh toán, xuất trước giá trị GGCK (nếu có).",
  IDS_TEXT_CONDITION_INVOICE_CONTENT_5:
    "Quý khách vui lòng kiểm tra kỹ các thông tin xuất hoá đơn đã nhập, trong trường hợp thông tin sai lệch sẽ xuất hoá đơn sau 5 ngày kể từ ngày xác nhận lại.",
  IDS_MT_TEXT_CONVENIENT_HOTEL: "Tiện nghi khách sạn",
  IDS_MT_TEXT_LOAD_REVIEW_HOTEL: "Đánh giá",
  IDS_MT_TEXT_LOAD_MORE_REVIEW: "Xem thêm đánh giá",
  IDS_MT_TEXT_POLICY: "Chính sách",
  IDS_MT_TEXT_DESCRIPTION: "Mô tả",
  IDS_MT_TEXT_INFO_HOTEL: "Thông tin khách sạn",
  IDS_MT_TEXT_REQUEST_PRICE_HOTEL: "Yêu cầu giá",
  IDS_MT_TEXT_HOTEL_NO_REVIEW: "Khách sạn này chưa có đánh giá",
  IDS_MT_TEXT_HOTEL_MISS_ROOM: "Bạn bỏ lỡ mất rồi",
  IDS_MT_TEXT_HOTEL_ROOM_SOLD_OUT:
    "Phòng cuối cùng của chúng tôi vừa được bán hết. Vui lòng chọn ngày khác hoặc liên hệ với chúng tôi qua để được hỗ trợ liên hệ khách sạn",
  IDS_MT_TEXT_HOTEL_HOTLINE_CONTACT: "hotline: 024 7109 9999",
  IDS_MT_TEXT_HOTEL_SUPPORT_CONTACT: "để được hỗ trợ liên hệ khách sạn",
  IDS_MT_TEXT_FLIGHT_SUPPORT_CONTACT:
    "Mọi yêu cầu hỗ trợ vui lòng liên hệ tổng đài",
  IDS_MT_TEXT_HOTEL_POLICY: "Chính sách khách sạn",
  IDS_MT_TEXT_HOTEL_DES: "Mô tả khách sạn",
  IDS_MT_TEXT_ROOM_POLICY: "Chính sách phòng",
  IDS_MT_TEXT_ROOM_CANCEL_POLICY: "Chính sách hủy",
  IDS_MT_TEXT_ROOM_CONVIENT: "Tiện nghi phòng",
  IDS_MT_TEXT_CONTINUE: "Tiếp tục",
  IDS_TEXT_ORDER_DETAIL: "Chi tiết đơn hàng",
  IDS_TEXT_FLIGHT_ORDER_CODE: "Mã đơn hàng",
  IDS_TEXT_FLIGHT_ROOM_CODE: "Mã đặt phòng",
  IDS_TEXT_FLIGHT_BOOK_CODE: "Mã đặt chỗ",
  IDS_TEXT_FLIGHT_BOOK_CODE_INBOUND: "Mã đặt chỗ chiều về ",
  IDS_TEXT_FLIGHT_BOOK_CODE_OUTBOUND: "Mã đặt chỗ chiều đi",
  IDS_TEXT_ORDER_INSURANCE: "Bảo hiểm du lịch",
  IDS_TEXT_ORDER_INSURANCE_PACKAGE: "Gói bảo hiểm",
  IDS_TEXT_ORDER_INSURANCE_START: "Bắt đầu",
  IDS_TEXT_ORDER_INSURANCE_END: "Kết thúc",
  IDS_TEXT_ORDER_INSURANCE_NUMBER: "Số người",
  IDS_TEXT_ORDER_INSURANCE_PRICE: "Thành tiền",
  IDS_TEXT_ORDER_PAYMENT_TOTAL_PRICE: "Tổng tiền thanh toán",
  IDS_TEXT_ORDER_PAYMENT_METHOD: "Phương thức thanh toán",
  IDS_TEXT_ORDER_PAYMENT_STATUS: "Trạng thái thanh toán",
  IDS_TEXT_ORDER_PAYMENT_STATUS_FAIL: "Thất bại",
  IDS_TEXT_ORDER_PAYMENT_STATUS_SUCCESS: "Thành công",
  IDS_TEXT_ORDER_PAYMENT_STATUS_HOLDING: "Đang giữ chỗ",
  IDS_TEXT_ORDER_PAYMENT_STATUS_CANCELLED: "Đã hủy",
  IDS_TEXT_ORDER_PAYMENT_STATUS_REFUNDED: "Đã hoàn tiền",
  IDS_TEXT_ORDER_PAYMENT_STATUS_PROCESSING: "Chúng tôi đang giữ vé cho bạn",
  IDS_TEXT_NOTE_PROCESSING_PAYMENT:
    "Đang trong quá trình xử lý... Quý khách vui lòng đợi trong giây lát!.",
  IDS_TEXT_ORDER_PAYMENT_STATUS_WAITING_PAYMENT: "Chờ thanh toán",
  IDS_TEXT_ORDER_PAYMENT_STATUS_WAITING_CANCEL: "Đang chờ hủy",
  IDS_TEXT_ORDER_PAYMENT_STATUS_DISPUTED: "Có sự cố",
  IDS_TEXT_FLIGHT_BOOKING_HOLD_TIME: "Giữ chỗ trong",
  IDS_TEXT_FLIGHT_BOOKING_FAIL: "Đặt vé thất bại",
  IDS_TEXT_FLIGHT_BOOKING_HOLD_SUCCESS: "Giữ chỗ thành công",
  IDS_TEXT_FLIGHT_BOOKING_SUCCESS: "Đặt vé thành công",
  IDS_TEXT_FLIGHT_BOOKING_RESULT_DESC_FAIL:
    "Đơn phòng không thể thanh toán do giao dịch bị gián đoạn.",
  IDS_TEXT_FLIGHT_BOOKING_RESULT_DESC_HOLD_SUCCESS:
    "Đơn hàng của bạn đã được giữ thành công. Bạn có thể xem lại nội dung chi tiết đơn hàng.",
  IDS_TEXT_FLIGHT_BOOKING_RESULT_DESC_SUCCESS_PC:
    "Đơn hàng của bạn đã được giữ thành công.",
  IDS_TEXT_FLIGHT_BOOKING_RESULT_DESC_SUCCESS:
    "Bạn có thể xem lại nội dung chi tiết đơn hàng.",
  IDS_MT_TEXT_ROOM_NUMBER: "Số phòng",
  IDS_MT_TEXT_ROOM_NUMBER_POLICY: "Số phòng phải ít hơn hoặc bằng số khách",
  IDS_MT_TEXT_CONFIRM: "Xác nhận",
  IDS_MT_TEXT_INCLUDE_TAX_FEE_VAT: "Đã bao gồm thuế, phí, VAT",
  IDS_MT_TEXT_DISCOUNT_CODE: "Mã giảm giá",
  IDS_MT_TEXT_SUB_FEE: "Phụ phí thanh toán",
  IDS_MT_TEXT_TAX_FEE: "Thuế và phí sân bay",
  IDS_MT_TEXT_TOTAL_PRICE_OUTBOUND: "Tổng giá chiều đi",
  IDS_MT_TEXT_TOTAL_PRICE_INBOUND: "Tổng giá chiều về",
  IDS_MT_TEXT_TOTAL_PRICE_TICKET: "Tổng giá vé",
  IDS_MT_TEXT_PRICE_DETAILS_OUTBOUND: "Chi tiết giá chiều đi",
  IDS_MT_TEXT_PRICE_DETAILS_INBOUND: "Chi tiết giá chiều về",
  IDS_MT_TEXT_PRICE_ADD_BAGGAGE: "Hành lý mua thêm",
  IDS_MT_TEXT_INVOICING_AND_OTHER_REQUEST: "Xuất hoá đơn và các yêu cầu khác",
  IDS_MT_TEXT_PRICE_DETAIL: "Chi tiết giá",
  IDS_MT_TEXT_TRANSACTION_FEES_AT_MT: "Phí giao dịch tại Mytour",
  IDS_MT_TEXT_BUTTON_HOLD_BOOKING: "Giữ chỗ",
  IDS_MT_TEXT_REQUEST_POPULAR: "Yêu cầu phổ biến",
  IDS_MT_TEXT_REQUEST_PRIVATE: "Yêu cầu riêng của bạn",
  IDS_MT_TEXT_USE_PROMO_CODE: "Sử dụng mã giảm giá",
  IDS_MT_TEXT_CHOOSE_TWO_PROMO_CODE: "Hoặc chọn một mã giảm giá dưới đây",
  IDS_MT_TEXT_POLICY_REQUEST_PRIVATE:
    "Lưu ý: Các yêu cầu của bạn chỉ được đáp ứng tuỳ theo khách sạn",
  IDS_MT_TEXT_FLIGHT_TERM_PAYMENT:
    "Bằng cách nhấn nút Thanh toán, bạn đồng ý với",
  IDS_MT_TEXT_LOADING_MORE: "Đang tải thêm",
  IDS_MT_TEXT_HOTEL_RECENT: "Khách sạn khác xung quanh",
  IDS_MT_TEXT_DES_PAYMENT_METHOD:
    "Sau khi hoàn tất thanh toán, mã xác nhận phòng sẽ được gửi ngay qua SMS và Email của bạn.",
  IDS_MT_TEXT_PAYMENT_METHOD_ATM: "Thẻ ATM nội địa",
  IDS_MT_TEXT_SELECT_BANK: "Chọn ngân hàng",
  IDS_MT_TEXT_PAYMENT_METHOD_ATM_DES:
    "Tất cả thông tin thanh toán của bạn được bảo mật",
  IDS_MT_TEXT_BANK_TRANSFER_DES: "Hướng dẫn thanh toán sẽ được gửi qua email",
  IDS_MT_TEXT_BOOKING_SUCCESS: "Đặt phòng thành công",
  IDS_MT_TEXT_BOOKING_PENDING: "Đang chờ thanh toán",
  IDS_MT_TEXT_BOOKING_FAILED: "Đặt phòng thất bại",
  IDS_MT_TEXT_BOOKING_REQUEST_PRIVATE: "Yêu cầu riêng",
  IDS_MT_TEXT_BOOKING_DETAILS: "Chi tiết đặt phòng",
  IDS_MT_TEXT_POLICY_NOTE: "Lưu ý",
  IDS_MT_RECEIPT_PAYMENT: "Gửi biên nhận thanh toán",
  IDS_MT_RECEIPT_PAYMENT_DESC:
    "Để quá trình xác nhận diễn ra nhanh hơn, vui lòng gửi biên nhận thanh toán của bạn",
  IDS_MT_CHOOSE_PHOTO: "Chọn ảnh",
  IDS_MT_SEND_PHOTO: "Gửi ảnh",
  IDS_MT_TEXT_REQUIRED_FIELD: "Thông tin bắt buộc",
  IDS_MT_TEXT_INVALID_PHONE_NUMBER: "Số điện thoại không hợp lệ",
  IDS_MT_TEXT_INVALID_NAME_MINLENGHT: "Họ và tên phải có ít nhất 2 từ",
  IDS_MT_TEXT_INVALID_FORMAT_EMAIL: "Email không đúng định dạng",
  IDS_MT_TEXT_INFO_PAYMENT_TRANSFER: "Thông tin chuyển khoản",
  IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_BANK: "Ngân hàng",
  IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_BRANCH: "Chi nhánh",
  IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_NUMBER_ACCOUNT: "Số tài khoản",
  IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_NAME_ACCOUNT: "Tên chủ tài khoản",
  IDS_MT_TEXT_INFO_COPPY: "Sao chép",
  IDS_MT_TEXT_AMOUNT_MONEY: "Số tiền",
  IDS_MT_TEXT_CONTENT_TRANSFER: "Nội dung chuyển khoản",
  IDS_MT_TEXT_DOWNLOAD_APP: "Tải ứng dụng",
  IDS_MT_TEXT_HOTLINE: "Hotline",
  IDS_MT_TOUR_AND_EVENT: "Tour & Sự kiện",
  IDS_MT_PROMOTION: "Ưu đãi",
  IDS_MT_MYTOUR_DESCRIPTION:
    "Mytour là thành viên của VNTravel Group - Một trong những tập đoàn đứng đầu Đông Nam Á về du lịch trực tuyến và các dịch vụ liên quan",
  IDS_MT_COPYRIGHT:
    "Copyright © 2020 - CÔNG TY CỔ PHẦN DU LỊCH VIỆT NAM VNTRAVEL - Đăng ký kinh doanh số 0108886908 - do Sở Kế hoạch và Đầu tư thành phố Hà Nội cấp lần đầu ngày 04 tháng 09 năm 2019",
  IDS_MT_TEXT_BEST_PRICE_OF_DAY: "Giá tốt sát ngày",
  IDS_MT_TEXT_COMMIT_BEST_PRICE_OF_DAY:
    "Cam kết giá tốt nhất khi đặt gần ngày cho chuyến đi của bạn.",
  IDS_MT_TEXT_SPEED_ORDER_MANY_PAYMENT:
    "Bao gồm thêm chuyển khoản ngân hàng và tiền mặt tại cửa hàng.",
  IDS_MT_TEXT_BOOK_FLY_LOW_PRICE: "Đặt vé máy bay giá rẻ",
  IDS_MT_TEXT_BEST_PRICE_REFUND: "Giá tốt nhất, hoàn tiền",
  IDS_MT_TEXT_COMMIT_BEST_PRICE_REFUND:
    "Cam kết đưa đầy đủ và chính xác thông tin khách sạn trên website với chính sách “Đảm bảo Hoàn tiền”",
  IDS_MT_TEXT_PAYMENT_EASY: "Thanh toán dễ dàng, đa dạng",
  IDS_MT_HOT_HOTEL_TEXT: "Các khách sạn hàng đầu",
  IDS_MT_HOT_FLIGHT_TEXT: "Các chặng bay & hãng bay hàng đầu",
  IDS_MT_TEXT_YOU_WANT_TO_SAVE:
    "Bạn muốn tiết kiệm tới 50% khi đặt phòng khách sạn, vé máy bay?",
  IDS_MT_TEXT_YOU_PRESS_PHONE_NUMBER:
    "Nhập số điện thoại để Mytour có thể gửi đến bạn những chương trình khuyến mại mới nhất!",
  IDS_MT_TEXT_ORDER_HOTEL: "Đơn phòng",
  IDS_MT_DOWNLOAD_TEXT_1:
    "Giá tốt hơn so với đặt phòng trực tiếp tại khách sạn",
  IDS_MT_DOWNLOAD_TEXT_2: "Nhân viên chăm sóc, tư vấn nhiều kinh nghiệm",
  IDS_MT_DOWNLOAD_TEXT_3: "Hơn 5000 khách sạn tại Việt Nam với đánh giá thực",
  IDS_MT_DOWNLOAD_TEXT_4: "Nhiều chương trình khuyến mãi và tích lũy điểm",
  IDS_MT_DOWNLOAD_TEXT_5: "Thanh toán dễ dàng, đa dạng",
  IDS_MT_TEXT_DES_ODER_HOTEL: "Tất cả đơn phòng của bạn sẽ hiện ở đây",
  IDS_MT_TEXT_DES_ODER_FLIGHT: "Vé máy bay của bạn sẽ hiện ở đây",
  IDS_MT_TEXT_NO_RESULT: "Không có kết quả",
  IDS_MT_TEXT_FIND_FLIGHT: "Tìm chuyến bay",
  IDS_MT_TEXT_INSPIRE_HOTEL: "Cảm hứng cho những chuyến đi",
  IDS_MT_TEXT_INSPIRE_DESCRIPTION_HOTEL:
    "Bí quyết du lịch, những câu chuyện thú vị đang chờ đón bạn",
  IDS_MT_TOP_PRICE_HOTEL: "Khách sạn giá tốt",
  IDS_MT_TOP_PRICE_HOTEL_ONLY_MYTOUR: "Khách sạn giá tốt chỉ có trên Mytour",
  IDS_TEXT_EXPIRED_HOLDING: "Hết hạn giữ chỗ",
  IDS_TEXT_CHANGE_FLIGHT_TIME: "Đổi giờ bay",
  IDS_TEXT_FLIGHT_CHANGE_BAGGAGE: "Đổi hành lý",
  IDS_TEXT_FLIGHT_SPLIT_CODE: "Tách code",
  IDS_TEXT_FLIGHT_REFUND: "Hoàn hủy",
  IDS_MT_FAVORITE_HOTEL: "Có thể bạn sẽ thích",
  IDS_MT_FAVORITE_HOTEL_DESCRIPTION: "Khách sạn HOT nhất do Mytour đề xuất",
  IDS_MT_MYTOUR_MALL: "Mytour Mall",
  IDS_MT_TOP_BRANDING_HOTEL: "Các thương hiệu khách sạn đối tác hàng đầu",
  IDS_MT_TEXT_INPUT_SEARCH: "Thành phố, khách sạn, điểm đến",
  IDS_MT_TEXT_DATE_CHECKIN: "Ngày đến",
  IDS_MT_TEXT_DATE_CHECKOUT: "Ngày về",
  IDS_MT_TEXT_NUM_ROOMS_OF_GUEST: "Số phòng, số khách",
  IDS_MT_ONLY_MYTOUR: "Chỉ có trên Mytour",
  IDS_MT_MY_COUPON: "Mã giảm giá cho bạn",
  IDS_MT_HOTEL_FOLLOW_ADDRESS: "Khách sạn theo địa điểm",
  IDS_MT_TEXT_ORDER_STATUS: "Trạng thái đơn hàng",
  IDS_MT_TEXT_STATUS: "Trạng thái",
  IDS_MT_TEXT_PAYMENT_NOW: "Thanh toán ngay",
  IDS_MT_INPUT_CHILDREN: "Nhập tuổi trẻ em:",
  IDS_MT_AGE: "Tuổi",
  IDS_MT_TEXT_PRICE_OF_FREE_CANCEL: "Giá gồm hủy phòng miễn phí",
  IDS_MT_TEXT_BOOKING_ON_APP: "Đặt trên App",
  IDS_MT_TEXT_SALING_TOP: "Đang bán chạy",
  IDS_MT_TEXT_AIRPORT_SHUTTER: "Xe đưa đón sân bay",
  IDS_MT_TEXT_YOU_MISS_ALLOTMENT:
    "Bạn vừa bỏ lỡ mất rồi! Phòng cuối cùng đã được bán hết với ngày bạn chọn.",
  IDS_MT_TEXT_LOGIN_USE_PROMO: "Đăng nhập để",
  IDS_MT_TEXT_DISCOUNT_ON_MEMBER: "Giảm giá cho thành viên",
  IDS_MT_FILTER_SELECTED: "Bộ lọc đã chọn",
  IDS_MT_CLEAR_FILTER: "Xóa tất cả lọc",
  IDS_MT_RANGE_PRICE: "Khoảng giá (1 đêm)",
  IDS_MT_HOTEL_LEVEL: "Hạng khách sạn",
  IDS_MT_NO_RESULT_FILTER_HOTEL:
    "Không tìm thấy kết quả nào với các tiêu chí tìm kiếm của bạn.",
  IDS_MT_CHANGE_OPTION_FILTER: "Vui lòng thay đổi tiêu chí tìm kiếm",
  IDS_TEXT_FLIGHT_BOOKING_BANK_TRANSFER_1:
    "Hướng dẫn chuyển khoản sẽ được gửi tới email",
  IDS_TEXT_FLIGHT_BOOKING_BANK_TRANSFER_2: "Vui lòng thanh toán trước",
  IDS_MT_FIT_TEXT: "Phù hợp nhất",
  IDS_MT_PRICE_LOWEST: "Giá thấp nhất",
  IDS_MT_STAR_HIGHEST: "Nhiều sao nhất",
  IDS_MT_VOTE_HIGHEST: "Đánh giá cao nhất",
  IDS_MT_TEXT_REGISTER_PHONE: "Hoặc đăng ký bằng số điện thoại",
  IDS_MT_TEXT_PLEASE_ENTER_PHONE: "Vui lòng nhập số điện thoại",
  IDS_MT_TEXT_OTP_SEND_PHONE: "Mã xác nhận đã được gửi tới số điện thoại",
  IDS_MT_TEXT_RE_PASSWORD: "Nhập lại mật khẩu",
  IDS_MT_FIND_WHEN_MOVE_MAP: "Tìm kiếm khi di chuyển bản đồ",
  IDS_MT_HOTEL_AROUND: "Khách sạn xung quanh",
  IDS_MT_TEXT_REFUND_CANCEL_0_DONG: "Hoàn hủy 0 đồng",
  IDS_MT_TEXT_HOTEL_TOP_SALING:
    "Khách sạn này đang được rất nhiều khách hàng lựa chọn, chỉ còn rất ít chỗ ở này trên Mytour.vn",
  IDS_MT_TEXT_HOTEL_SEE_REVIEW: "Xem đánh giá",
  IDS_MT_TEXT_HOTEL_VIEW_MAP: "Xem bản đồ",
  IDS_MT_TEXT_VIEW_IMAGE_CONVIENT: "Xem hình ảnh & tiện nghi",
  IDS_MT_TEXT_VIEW_ALL_CONVIENT: "Xem tất cả tiện nghi",
  IDS_MT_TEXT_VIEW_ALL_EVALUATE: "Xem tất cả đánh giá",
  IDS_MT_TIME_BOOKING_OUT_TEXT: "Thời gian hoàn tất thanh toán đã hết!",
  IDS_MT_TIME_BOOKING_OUT_DESCRIPTION:
    "Bạn có muốn tiếp tục đặt phòng này? Giá có thể sẽ tăng nếu bạn rời đi bây giờ.",
  IDS_MT_SELECT_OTHER_ROOM: "Chọn phòng khác",
  IDS_MT_CONTINUE_BOOKING: "Tiếp tục đặt phòng",
  IDS_MT_TEXT_HOTEL_NEAR_YOUR: "Khách sạn gần bạn",
  IDS_MT_TEXT_ENTER_PHONE_OR_EMAIL: "Nhập số điện thoại hoặc email",
  IDS_MT_TEXT_LOGIN_BY_PHONE_OR_EMAIL:
    "Hoặc đăng nhập bằng số điện thoại, email",
  IDS_MT_ORDER_ROOM_FOR_OTHER_CUSTOMER: "Tôi đặt phòng giúp cho người khác.",
  IDS_MT_OTHER_CUSTOMER_INFO: "Thông tin khách nhận phòng",
  IDS_MT_FREE_CANCEL: "Miễn phí hoàn huỷ",
  IDS_MT_SEND_ORDER_ROOM: "Gửi yêu cầu đặt phòng",
  IDS_MT_PAYMENT_SUCCESS_DESCRIPTION:
    "Mytour sẽ chủ động liên hệ với quý khách trong vòng 30 phút để xác nhận phòng trống và hướng dẫn thanh toán.",
  IDS_MT_FLIGHT_PARTNER_TITLE: "Đối tác hàng không",
  IDS_MT_TEXT_PRICE_FROM: "Chỉ từ",
  IDS_MT_TEXT_PLACEHOLDER_PHONE_NUMBER: "Số điện thoại",
  IDS_MT_SELECT_BED_TYPE: "Chọn loại giường",
  IDS_MT_TEXT_BOOKING_LAST_TIME: "Vừa đặt cách đây",
  IDS_MT_NO_SMOKE_ROOM: "Phòng không hút thuốc",
  IDS_MT_ROOM_HIGH_LEVEL: "Phòng ở tầng cao",
  IDS_MT_INVOICE_REQUIRE: "Yêu cầu xuất hóa đơn",
  IDS_MT_REQUIRE_INVOICE_DES_1:
    "Quý khách hàng cần cung cấp thông tin và địa chỉ xuất hóa đơn trước hoặc ngay sau khi thanh toán đơn phòng.",
  IDS_MT_REQUIRE_INVOICE_DES_2:
    "Mytour sẽ xuất hóa đơn bằng với số tiền thực tế khách hàng đã thanh toán cho Mytour.",
  IDS_MT_REQUIRE_INVOICE_DES_3:
    "Mytour sẽ không thể xuất hóa đơn nếu Quý khách gửi thông tin chậm hơn thời gian trên.",
  IDS_MT_REQUIRE_INVOICE_DES_4:
    "Mytour sẽ gửi hóa đơn đến Quý khách trong vòng một tuần sau ngày trả phòng.",
  IDS_MT_VN_PAY_GATE: "Thanh toán an toàn qua cổng VNPay, miễn phí giao dịch.",
  IDS_MT_CONFIRM_VIA_EMAIL_SMS: "Nhận xác nhận ngay qua Email và SMS.",
  IDS_MT_TEXT_AGREE_ROOM_EARLY: "Phòng trống sẽ được xác nhận sớm.",
  IDS_TEXT_PAYMENT_STEP_1: "Thông tin hành khách",
  IDS_TEXT_PAYMENT_STEP_2: "Thanh toán",
  IDS_TEXT_PAYMENT_STEP_3: "Hoàn thành",
  IDS_MT_TEXT_PLEASE_ENTER_PROMOTION_CODE: "Vui lòng nhập mã giảm giá",
  IDS_MT_TEXT_PLEASE_TOTAL_MONEY: "Tổng tiền",
  IDS_MT_USE_COUPON_TEXT: "Sử dụng mã khi thanh toán",
  IDS_MT_TERM_CONDITION_COUPON_TEXT: "Điều kiện & thể lệ chương trình",
  IDS_MT_TEXT_DOWNLOAD_APP_MYTOUR: "Tải app Mytour",
  IDS_MT_TEXT_CHANGE_PASSWORD: "Đổi mật khẩu",
  IDS_MT_TEXT_FORGOT_PASSWORD_DES:
    "Vui lòng nhập số điện thoại khách hàng đã đăng kí. Chúng tôi sẽ gửi mã OTP tới điện thoại của khách hàng để tạo mật khẩu mới",
  IDS_MT_TEXT_FORGET_PASSWORD_SUCCESS: "Lấy lại mật khẩu thành công",
  IDS_MT_REVIEW_HOTEL_BY_MYTOUR: "Đánh giá từ Mytour",
  IDS_MT_REVIEW_HOTEL_BY_TRIPADVISOR: "Đánh giá từ TripAdvisor",
  IDS_MT_ALL_REVIEW_FORM_MYTOUR:
    "100% đánh giá từ khách hàng đặt phòng trên Mytour",
  IDS_MT_TEXT_VALID_MAX_LENGTH_DEFAULT:
    "Vui lòng không nhập quá số kí tự giới hạn",
  IDS_MT_MORE_ADDRESS: "Xem thêm các địa điểm",
  IDS_MT_MY_TOUR_MALL_HEADER_TEXT: "Đẳng cấp. Sang trọng. Và hơn thế nữa.",
  IDS_MT_MALL_BRAND: "Thương hiệu",
  IDS_MT_MALL_SELECT_BRAND: "Chọn thương hiệu",
  IDS_MT_MALL_INPUT_BRAND_NAME: "Nhập tên thương hiệu",
  IDS_MT_MALL_TOP_HOTEL_BRAND:
    "Những thương hiệu khách sạn, resort đẳng cấp hàng đầu",
  IDS_MT_MALL_TOP_HOTEL_BRAND_DESCRIPTION:
    "Thương hiệu thể hiện đẳng cấp, được thiết kế chuyên nghiệp với các dịch vụ, tiện nghi sang trọng với tiêu chuẩn dịch vụ cao nhất.",
  IDS_MT_MALL_DESIGN_SPECICAL: "Được thiết kế chuyên nghiệp",
  IDS_MT_MALL_DESIGN_SPECICAL_DETAILS:
    "Những thương hiệu được thiết kế theo một phong cách riêng, định hình cho mỗi thương hiệu nhưng tất cả đều thể hiển sự đẳng cấp và chuyên nghiệp của mình, mang tới cho bạn sự thoải mái và thư giãn.",
  IDS_MT_MALL_CONVIENT_ROYAL: "Tiện nghi sang trọng",
  IDS_MT_MALL_CONVIENT_ROYAL_DETAILS:
    "Được trang bị đầy đủ cơ sở vật chất, đáp ứng mọi nhu cầu của bạn khi đi du lịch, nghỉ dưỡng, với không gian đẳng cấp và riêng tư.",
  IDS_MT_MALL_REQUEST_SERVICE: "Dịch vụ theo yêu cầu",
  IDS_MT_MALL_REQUEST_SERVICE_DETAILS:
    "Từ đầu bếp riêng, đến cơ sở mát-xa, khu vui chơi trẻ em và dịch vụ chăm sóc trẻ, không gian dành cho thư giãn và thể thao chuyên nghiệp.",
  IDS_MT_MALL_TOP_LOCATION: "Vị trí đắc địa",
  IDS_MT_MALL_TOP_LOCATION_DETAILS:
    "Tất cả các cơ sở lưu trú đều nằm ở các vị trí đắc địa tại mỗi địa phương, thuận tiện cho mọi hoạt động vui chơi, mua sắm, giải trí và thư giãn.",
  IDS_MT_MALL_AIRPORT_SHUTTLE: "Dịch vụ xe đưa đón sân bay",
  IDS_MT_MALL_AIRPORT_SHUTTLE_DETAILS:
    "Bạn luôn luôn được chào đón từ sự tận tâm của khách sạn, không phải lo lắng khi phải đặt thêm các dịch vụ khác",
  IDS_MT_MALL_CHECK_QUALITY_REAL: "Kiểm tra chất lượng thực tế",
  IDS_MT_MALL_CHECK_QUALITY_REAL_DETAILS:
    "Mỗi thương hiệu đều được các tập đoàn hàng đầu xác minh về chất lượng và thiết kế nhằm đảm bảo có tất cả các yếu tố cần thiết để khiến bạn cảm thấy hài lòng, thoải mái và như được ở nhà - dù bạn đang ở bất kỳ đâu, bất kỳ nơi nào.",
  IDS_MT_TEXT_BACK_MY_ORDER_HOTEL: "Quay lại đơn phòng",
  IDS_MT_TEXT_CUSTOMER_NAME: "Họ và tên",
  IDS_MT_TEXT_INVOICE_OUTBOUND: "Hóa đơn chiều đi",
  IDS_MT_TEXT_INVOICE_INBOUND: "Hóa đơn chiều về",
  IDS_MT_TEXT_INVOICE_ATTACH_OUTBOUND_NAME: "HDCD.pdf",
  IDS_MT_TEXT_INVOICE_ATTACH_INBOUND_NAME: "HDCV.pdf",
  IDS_MT_TEXT_DISCOVER_NOW: "Khám phá ngay",
  IDS_MT_CONDITION_COUPON: "Điều kiện và thể lệ chương trình",
  IDS_MT_VIEW_ALL_CHAINS: "Xem tất cả thương hiệu",
  IDS_MT_ALL_BRANDS: "Tất cả thương hiệu",
  IDS_MT_DES_LOGIN_FLIGHT: "để đặt vé nhanh hơn, không cần nhập thông tin!",
  IDS_MT_EMPTY_ROOM_BOOKING:
    "Giá phòng này đã vừa có khách hàng nhanh tay đặt hết. Quý khách vui lòng lựa chọn phòng khác.",
  IDS_MT_VIEW_MORE_REVIEW_TRIPADVISOR:
    "Xem thêm bình luận trên trang TripAdvisor",
  IDS_MT_SEND_REVIEW: "Gửi đánh giá",
  IDS_MT_CONTENT_REVIEW: "Nội dung đánh giá",
  IDS_MT_DETAIL_REVIEW: "Đánh giá chi tiết",
  IDS_MT_UPLOAD_IMAGE: "Upload ảnh",
  IDS_MT_LIMIT_UPLOAD: "(tối đa 5 ảnh)",
  IDS_MT_UPLOAD: "Upload",
  IDS_MT_YOU_SAVE_MONEY: "Chúc mừng! Bạn đã tiết kiệm được",
  IDS_MT_ONLY_ONE_ALLOTMENT:
    "Nhanh lên! Phòng cuối cùng của chúng tôi ở mức giá này cho ngày bạn chọn",
  IDS_MT_TEXT_NOT_FOUND_TAX_CODE:
    "Không tìm thấy thông tin doanh nghiệp. Vui lòng nhập mới.",
};

export const prefixUrlIcon =
  "https://storage.googleapis.com/tripi-assets/mytour/icons/";
export const listIcons = {
  iconPlus: "icon_plus.svg",
  IconAirPlane: "icon_air_plane.svg",
  IconBestPrice: "icon_best_price.svg",
  IconHotelBox: "icon_hotel_box.svg",
  IconMPlus: "icon_m_plus.svg",
  IconBoxMail: "icon_mail.svg",
  IconMallBackground: "icon_mall_background.svg",
  IconMytourBackground: "icon_mytour_background.svg",
  IconMytourMallWhite: "icon_mytour_mall_white.svg",
  IconPiggyDollar: "icon_piggy_dollar.svg",
  IconRatingMall: "icon_rating_mall.svg",
  IconRecommendRoom: "icon_recommend_room.svg",
  IconStatusOrderHotel: "icon_status_order_hotel.svg",
  IconSupport247: "icon_support_247.svg",
  IconTagSale: "icon_tag_sale.svg",
  IconBeach: "icon_beach.svg",
  IconCalenderOrder: "icon_calendar_order.svg",
  IconFlightBar: "icon_flight_bar.svg",
  IconFlightBox: "icon_flight_box.svg",
  IconPartnerDiscount: "icon_partner_discount.svg",
  IconPartnerGuide: "icon_partner_guide.svg",
  IconPartnerRegister: "icon_partner_register.svg",
  IconPartnerStep1: "icon_partner_step_1.svg",
  IconPartnerStep2: "icon_partner_step_2.svg",
  IconPartnerStep3: "icon_partner_step_3.svg",
  IconPartnerStep4: "icon_partner_step_4.svg",
  IconPromo: "icon_promo.svg",
  iconInstagram: "icon_instagram.svg",
  iconMessenger: "icon_messenger.svg",
  iconFacebook: "icon_facebook.svg",
  IconPayment: "icon_payment.svg",
  IconTotalHotel: "icon_total_hotel.svg",
  IconFiveStar: "icon_5_star.svg",
  IconBgLogout: "icon_bg_logout.svg",
  IconBoxGif: "icon_gif_box.svg",
  IconHiddenInstall: "icon_install_logo_hidden.svg",
  IconMethodVisa: "icon_method_visa.svg",
  IconPartnerInfo: "icon_partner_info.svg",
  IconPriceTag2: "icon_price_tag_2.svg",
  IconRefreshOnlyMytour: "icon_refresh_only_mytour.svg",
  IconSynchronize: "icon_synchronize.svg",
  IconTransportTicket: "icon_transport_ticket.svg",
  IconTripadvisorMall: "icon_trip_advisor_mall.svg",
  IconTripadvisor: "icon_tripadvisor.svg",
  IconTripAdvisorReview: "icon_tripadvisor_review.svg",
  IconDoubleSingleBed: "icon__double_single_bed.svg",
  Icon3SingleBed: "icon_3 single bed.svg",
};

export const STEP_NUMBER = {
  PAYMENT_STEP_1: 1,
  PAYMENT_STEP_2: 2,
  PAYMENT_STEP_3: 3,
};
