import { Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import {
  IconEat,
  IconFrame,
  IconInfo,
  IconLuggage,
  IconNoRefundTicket,
} from "@public/icons";
import {
  airlineColor,
  flightPaymentMethodCode,
  flightPaymentStatus,
  formatDate,
  HASH_KEY,
  LAST_HOTEL_BROWSER_ITEMS,
  LAST_LOCATION_RECENT_SEARCH_HISTORY_ITEMS,
  LIFT_TIME_LIST,
  listString,
  CUSTOMER_TYPE,
} from "@utils/constants";
import CryptoJS from "crypto-js";
import moment from "moment";
import "moment-timezone";
import "moment/locale/vi";
import DeviceDetector from "ua-parser-js";
import { DATE_FORMAT, HOUR_MINUTE } from "./moment";

const has = Object.prototype.hasOwnProperty;

export const isDiff = (A, B) => JSON.stringify(A) !== JSON.stringify(B);

export const isEmpty = (prop) => {
  return (
    prop === null ||
    prop === undefined ||
    (has.call(prop, "length") && prop.length === 0) ||
    (prop.constructor === Object && Object.keys(prop).length === 0)
  );
};

export const areEqualItemRoom = (prevProps, nextProps) => {
  return (
    JSON.stringify(prevProps.item) === JSON.stringify(nextProps.item) &&
    JSON.stringify(prevProps.hotelDetail) ===
      JSON.stringify(nextProps.hotelDetail)
  );
};

export const areEqualHotelItemListing = (prevProps, nextProps) => {
  return (
    JSON.stringify(prevProps.item) === JSON.stringify(nextProps.item) &&
    JSON.stringify(prevProps.paramsFilterInit) ===
      JSON.stringify(nextProps.paramsFilterInit)
  );
};

export const areEqualItem = (prevProps, nextProps) => {
  return JSON.stringify(prevProps.item) === JSON.stringify(nextProps.item);
};

export const unEscape = (str) =>
  str
    .replace(/&amp;/g, "&")
    .replace(/&lt;/g, "<")
    .replace(/&gt;/g, ">")
    .replace(/&quot;/g, '"')
    .replace(/&#x27;/g, "'")
    .replace(/&#x2F;/g, "/")
    .replace(/&nbsp;/g, " ");

export const adapterBlogNear = (listItem = []) => {
  let result = [];
  listItem.forEach((el, index) => {
    if (!isEmpty(el)) {
      result.push({
        ...el,
        title: el.name,
      });
    }
  });
  return result;
};

export const getMessageHotelReviews = (score) => {
  if (score >= 4.5) return "Tuyệt vời";
  if (score >= 4) return "Rất tốt";
  if (score >= 3.5) return "Tốt";
  if (score >= 2.5) return "Trung bình";
  return "Kém ";
};

export const getFlightTimeText = (time) => {
  let strTime = "";
  if (time < 60) {
    strTime = "0h" + time + "'";
  } else {
    if (time < 120) {
      strTime = "1h" + (time - 60) + "'";
    } else {
      strTime = "2h" + (time - 120) + "'";
    }
  }

  return strTime;
};

export const sortRooms = () => {
  return [];
};

export const genderDate = (start, end) => {
  let listDate = [];
  for (let index = start; index <= end; index++) {
    listDate.push({
      id: index < 10 ? `0${index}` : `${index}`,
      label: index < 10 ? `0${index}` : `${index}`,
    });
  }
  return listDate;
};

export const checkIsMobile = (userAgent = "") => {
  const device = DeviceDetector(userAgent);
  if (
    device.device.type &&
    (device.device.type === "mobile" || device.device.type === "tablet")
  ) {
    return true;
  }
  return false;
};

export const getDeviceInfo = (userAgent = "") => {
  if (!(typeof window === "undefined")) {
    const device = DeviceDetector(navigator.userAgent);
    if (device.device.type && device.device.type === "mobile") {
      return "Mobile-Web";
    } else {
      return "PC-Web";
    }
  } else {
    return "Server-web";
  }
};

export const getPlatform = (userAgent = "") => {
  if (!(typeof window === "undefined")) {
    const device = DeviceDetector(navigator.userAgent);
    if (device.device.type && device.device.type === "mobile") {
      return "mobile_web";
    } else {
      return "website";
    }
  } else {
    return "website";
  }
};

///////////////adapter for home page////////////

//// adapter for api hotel-hot-deal view Hotel Card

export const adapterHotelHotDeal = (listItem = []) => {
  let result = [];
  listItem.forEach((el, index) => {
    if (!isEmpty(el)) {
      result.push({
        id: el.id,
        slug: el.slug,
        hotelName: el.name,
        discountPercent: !isEmpty(el.promotionInfo)
          ? el.promotionInfo.discountPercentage
          : null,
        subPrice: !isEmpty(el.promotionInfo)
          ? el.promotionInfo.priceBeforePromotion
          : null,
        mainPrice: el.price || null,
        rating: el.starNumber,
        hiddenPrice: el.hiddenPrice,
        point: 2 * el?.rating?.hotel?.rating,
        rateLevel: getMessageHotelReviews(el?.rating?.hotel?.rating),
        taRating: el?.rating?.tripadvisor?.rating,
        countRating: el?.rating?.hotel?.count,
        location: el?.address?.provinceName,
        countBooking: 554, // fake data
        imageHotel: el?.thumbnail?.image?.small,
        flagTopHotel: "Bán chạy", // fake data
        isFavoriteHotel: el.isFavoriteHotel,
      });
    }
  });
  return result;
};
export const adapterHotelHotDealPc = (listItem = []) => {
  let result = [];
  listItem.forEach((el, index) => {
    if (!isEmpty(el)) {
      result.push({
        id: el.id,
        slug: el.slug,
        hotelName: el.name,
        discountPercent: !isEmpty(el.basePromotionInfo)
          ? el.basePromotionInfo.discountPercentage
          : null,
        subPrice: !isEmpty(el.basePromotionInfo)
          ? el.basePromotionInfo.priceBeforePromotion
          : null,
        basePromotionInfo: el.basePromotionInfo || {},
        mainPrice: el.price || null,
        hiddenPrice: el?.hiddenPrice,
        lastBookedTime: el?.lastBookedTime,
        rating: el.starNumber,
        tags: el?.tags,
        point: 2 * el?.rating?.hotel?.rating,
        rateLevel: getMessageHotelReviews(el?.rating?.hotel?.rating),
        countRating: el?.rating?.hotel?.count,
        location: !isEmpty(el.address) ? getLocation(el.address) : "",
        imageHotel: el?.thumbnail?.image?.small,
        isFavoriteHotel: el.isFavoriteHotel,
        srcImage: el?.thumbnail?.image.small,
        srcImageAvailability: el?.image?.small,
        ratingStar: el.starNumber || 0,
        ratingPoint:
          2 * el?.rating?.hotel?.rating === 10
            ? 10
            : (2 * el?.rating?.hotel?.rating).toFixed(1),
        taRating: el?.rating?.tripadvisor?.rating,
        countComment: el?.rating?.hotel?.count,
      });
    }
  });
  return result;
};

//// adapter for api /v3/hotels/get-hotel-availability view Hotel Card

export const adapterHotelHotNear = (listItem = []) => {
  let result = [];
  listItem.forEach((el, index) => {
    if (!isEmpty(el)) {
      result.push({
        id: el.id,
        hotelName: el?.name || "",
        discount: !isEmpty(el.basePromotionInfo)
          ? `-${el.basePromotionInfo.discountPercentage}`
          : null,
        subPrice: !isEmpty(el.basePromotionInfo)
          ? el.basePromotionInfo.priceBeforePromotion
          : 0,
        mainPrice: el.basePrice,
        rating: el.starNumber,
        point: 2 * el?.ratingLevel,
        rateLevel: getMessageHotelReviews(el?.ratingLevel),
        countRating: el?.numberOfReviews,
        taRating: el?.taRating,
        location: el?.address?.address,
        countBooking: 554, // fake data
        imageHotel: el?.image.small,
        flagTopHotel: "Bán chạy", // fake data
        isFavoriteHotel: el.isFavoriteHotel,
        distanceToSearchLocation: el?.distanceToSearchLocation || null,
      });
    }
  });
  return result;
};

const getLocation = (address = {}) => {
  if (!isEmpty(address.interestingPlace)) {
    return `${address.interestingPlace.name}${`, ${
      !isEmpty(address.districtName)
        ? address.districtName
        : address.provinceName
    }`}`;
  } else {
    return `${
      !isEmpty(address.districtName)
        ? `${address.districtName}`
        : address.provinceName
    }`;
  }
};

export const getMemberDeal = (promotionInfo = {}, price = 0) => {
  let result = "";
  // if (promotionInfo.discountPercentage >= 10) {
  //   result = `${promotionInfo.discountPercentage}%`;
  // } else {
  //   result = `${(promotionInfo?.discountFixed).formatMoney()}đ`;
  // }
  result = `${promotionInfo.discountPercentage}%`;
  return result;
};

//// adapter for /v3/hotels/get-hotel-availability view Hotel Item Listing
export const adapterHotelAvailability = (listItem = []) => {
  let result = [];
  listItem.forEach((el, index) => {
    if (!isEmpty(el)) {
      result.push({
        id: el.id,
        srcImage: el?.image?.small,
        hotelName: el?.name || "",
        ratingStar: el.starNumber || 0,
        ratingPoint:
          2 * el?.ratingLevel === 10 ? 10 : (2 * el?.ratingLevel).toFixed(1),
        ratingText: getMessageHotelReviews(el.ratingLevel),
        countComment: el?.numberOfReviews,
        lastBookedTime: el?.lastBookedTime,
        location: !isEmpty(el.address) ? getLocation(el.address) : "",
        freeBreakfast: el.includedBreakfast,
        allotment: el.numberOfRemainingRooms,
        outOfRoom: el.outOfRoom,
        hiddenPrice: el.hiddenPrice,
        roomName: el.roomName,
        discountPercent: !isEmpty(el.basePromotionInfo)
          ? el.basePromotionInfo.discountPercentage
          : null,
        subPrice: !isEmpty(el.basePromotionInfo)
          ? el.basePromotionInfo.priceBeforePromotion
          : 0,
        priceKey: el.priceKey,
        memberDeal: !isEmpty(el.basePromotionInfo)
          ? getMemberDeal(el.basePromotionInfo, el.basePrice)
          : "10%",
        mainPrice: el.basePrice || 0,
        category: el?.category,
        isFavoriteHotel: el?.isFavoriteHotel,
        promotions: el?.promotions,
        taRating: el?.taRating,
        freeCancellation: el?.freeCancellation,
        specialOffer: el?.specialOffer,
        tags: el?.tags,
        amenities: el?.amenities,
        codeMobile: "MYTOUR", // fake data
        mainPriceOnPromo: 12500000, // fake data
        isMember: true, // fake data
        missAllotment: true, // fake data
        discountBookingApp: 20, // fake data
        discountPromoCode: 20, // fake data
        loginUsePromo: true, // fake data
        disountLogin: 25, // fake data
      });
    }
  });
  return result;
};

//// adapter for /v3/favorites/hotels view Hotel Item Listing
export const adapterHotelFavorite = (listItem = []) => {
  let result = [];
  listItem.forEach((el, index) => {
    if (!isEmpty(el)) {
      result.push({
        id: el.id,
        srcImage: el?.thumbnail?.src,
        hotelName: el.name,
        ratingStar: el.starNumber,
        ratingPoint: (2 * el?.rating?.hotel?.rating).toFixed(1),
        ratingText: getMessageHotelReviews(el?.rating?.hotel?.rating),
        countComment: el?.rating?.hotel?.count,
        location: !isEmpty(el.address) ? getLocation(el.address) : "",
        freeBreakfast: el.includedBreakfast,
        allotment: el.numberOfRemainingRooms,
        discountPercent: !isEmpty(el.promotionInfo)
          ? el.promotionInfo.discountPercentage
          : null,
        subPrice: !isEmpty(el.promotionInfo)
          ? el.promotionInfo.priceBeforePromotion
          : 0,
        mainPrice: el.price || null,
        category: el?.category,
        isFavoriteHotel: el?.isFavoriteHotel,
        promotions: el?.promotions,
        taRating: el?.taRating,
        freeCancellation: el?.freeCancellation,
        specialOffer: el?.specialOffer,
        tags: el?.tags,
        amenities: el?.amenities,
        codeMobile: "MYTOUR", // fake data
        mainPriceOnPromo: 12500000, // fake data
        isMember: true, // fake data
        missAllotment: true, // fake data
        discountBookingApp: 20, // fake data
        discountPromoCode: 20, // fake data
        loginUsePromo: true, // fake data
        disountLogin: 25, // fake data
      });
    }
  });
  return result;
};

/// adapter create Amenities group
export const adapterAmenitiesGroup = (groupAmenities = {}) => {
  let result = [];
  const arrayGroupAmenities = Object.values(groupAmenities);

  arrayGroupAmenities.forEach((el) => {
    if (!isEmpty(el.amenities)) {
      result = [...result, ...el.amenities];
    }
  });
  return result;
};

//////////// handle save item when search location or hotel for Storage

export const handleSaveItemStorage = (item = {}) => {
  let resultListItem = [];
  const listItems =
    JSON.parse(
      localStorage.getItem(LAST_LOCATION_RECENT_SEARCH_HISTORY_ITEMS)
    ) || [];
  // remove item is have in storage
  resultListItem = listItems.filter((el) => el.name !== item.name);
  resultListItem.unshift(item);
  // remove length > 5
  if (resultListItem.length > 5) {
    resultListItem.splice(5, 1);
  }
  localStorage.setItem(
    LAST_LOCATION_RECENT_SEARCH_HISTORY_ITEMS,
    JSON.stringify(resultListItem)
  );
};

//////////// handle save item  hotel when view hotel detail
export const handleSaveHotelDetailToStorage = (hotelDetail = {}) => {
  const listHotelLastViewTemp =
    JSON.parse(localStorage.getItem(LAST_HOTEL_BROWSER_ITEMS)) || [];
  let listHotelLastView = listHotelLastViewTemp.filter(
    (el) => el.id !== hotelDetail.id
  );
  listHotelLastView.unshift({
    id: hotelDetail.id,
    hotelName: hotelDetail.name || "",
    localtion: hotelDetail?.address.provinceName,
    hotelImage: hotelDetail?.thumbnail?.image?.small,
    hotelStar: hotelDetail?.starNumber,
  });
  if (listHotelLastView.length > 10) {
    listHotelLastView.splice(10, 1);
  }
  localStorage.setItem(
    LAST_HOTEL_BROWSER_ITEMS,
    JSON.stringify(listHotelLastView)
  );
};
///
export const paramsDefaultHotel = () => ({
  checkIn: moment()
    .tz("Asia/Ho_Chi_Minh")
    .format(formatDate.firstDay),
  checkOut: moment()
    .tz("Asia/Ho_Chi_Minh")
    .add(1, "days")
    .format(formatDate.firstDay),
  adults: 2,
  rooms: 1,
  children: 0,
});

export const checkAndChangeCheckIO = (dateString = "", type = "checkIn") => {
  if (isEmpty(dateString)) {
    return paramsDefaultHotel()[type];
  } else {
    const todayText = moment().format(formatDate.firstYear);
    const date = moment(dateString, formatDate.firstDay);
    const dateText = date.format(formatDate.firstYear);
    if (moment(dateText).isBefore(todayText) && type === "checkIn") {
      return paramsDefaultHotel()[type];
    }
    if (moment(dateText).isSameOrBefore(todayText) && type === "checkOut") {
      return paramsDefaultHotel()[type];
    }
  }
  return dateString;
};

export const getAppHash = () => {
  let timeStamp = new Date().getTime();
  timeStamp = timeStamp / 1000 - ((timeStamp / 1000) % 300);
  let str = `${timeStamp}:${process.env.NEXT_PUBLIC_HASH_KEY}`;
  const hash = CryptoJS.SHA256(str);
  const hashStr = CryptoJS.enc.Base64.stringify(hash);
  return hashStr;
};

export function getSurchargeMoney(
  totalPrice,
  confirmFee,
  percentFee,
  fixedFee
) {
  const finalPrice = totalPrice - confirmFee;
  const fee = (finalPrice * percentFee) / 100 + fixedFee;

  return parseInt(fee);
}

export function durationMillisecondToHour(millisecond) {
  const second = millisecond / 1000;
  const hours = Math.floor(second / 3600);
  const minutes = Math.floor((second - hours * 3600) / 60);

  if (minutes) {
    return `${hours}h ${minutes}m`;
  }

  return `${hours}h`;
}

export function secondToString(second) {
  let result = "";
  const minutes = Math.floor(second / 60);
  if (minutes < 60) {
    return minutes > 1 ? `${minutes}m` : `1m`;
  } else if (minutes >= 60 && minutes <= 1440) {
    const hours = Math.floor(minutes / 60);
    const surPlus = minutes % 60;
    let surPlusStr = "";
    if (surPlus > 0) {
      surPlusStr = secondToString(surPlus * 60);
    }
    result = hours > 1 ? `${hours}h` : "1h";
    if (surPlusStr) {
      result = `${result} ${surPlusStr}`;
    }
  } else {
    const day = Math.floor(minutes / 1440);
    const surPlus = minutes % 1440;
    let surPlusStr = "";
    if (surPlus > 0) {
      surPlusStr = secondToString(surPlus * 60);
    }
    result = day > 1 ? `${day} ngày` : "1 ngày";
    if (surPlusStr) {
      result = `${result} ${surPlusStr}`;
    }
  }
  return result;
}

export function getExpiredTimeMillisecond(millisecond) {
  const second = millisecond / 1000;
  const curTimeStamp = moment().unix();
  if (second < curTimeStamp) {
    return "0m";
  } else {
    const diffSecond = second - curTimeStamp;
    return secondToString(diffSecond);
  }
}

export function durationFlightTime(millisecond) {
  const second = millisecond / 1000;
  const hours = Math.floor(second / 3600);
  const minutes = Math.floor((second - hours * 3600) / 60);

  if (minutes) {
    return `${hours}h${minutes}'`;
  }

  return `${hours}h`;
}

export function convertUrlFlight(url) {
  if (url && url[1]) {
    var afterDot = url[1].substr(url[1].indexOf(".") + 1);
    const arrayCode = afterDot.split(".");
    return `${arrayCode[arrayCode.length - 2]}-${
      arrayCode[arrayCode.length - 1]
    }`;
  }
}

export const flightCheapestRoundtripCmp = (a, b) =>
  a.outbound.ticketdetail.bestGroupPriceAdultUnit -
  b.outbound.ticketdetail.bestGroupPriceAdultUnit;
export const flightCheapestCmp = (a, b) =>
  a.outbound.ticketdetail.priceAdultUnit -
  b.outbound.ticketdetail.priceAdultUnit;
export const flightFastestCmp = (a, b) =>
  a.outbound.duration - b.outbound.duration;
export const flightTimeTakeOffCmp = (a, b) =>
  a.outbound.departureTime - b.outbound.departureTime;

export function filterAndSort(tickets, filter, cmp = () => {}) {
  if (!filter || !tickets) return;
  const filteredTickets = tickets?.filter((ticket) => {
    if (filter.airlines?.length > 0) {
      if (!filter.airlines.find((one) => one === ticket.outbound.aid)) {
        return false;
      }
    }
    if (filter.flyTimeFilter) {
      if (ticket.outbound.duration > filter.flyTimeFilter * 1000 * 60) {
        return false;
      }
    }

    if (filter.numStops && filter.numStops.length > 0) {
      const stopCount = ticket.outbound.ticketdetail.numStops;
      if (filter.numStops.indexOf(stopCount) === -1) {
        return false;
      }
    }

    // if (
    //   ticket.outbound.ticketdetail.bestGroupPriceAdultUnit < filter.price[0] ||
    //   ticket.outbound.ticketdetail.bestGroupPriceAdultUnit > filter.price[1]
    // ) {
    //   return false;
    // }
    if (filter.priceFilter) {
      // if (filter.priceFilter) {
      if (ticket.outbound.ticketdetail.priceAdultUnit > filter.priceFilter) {
        return false;
      }
      // }
      // else if (
      //   ticket.outbound.ticketdetail.priceAdultUnit < filter.price[0] ||
      //   ticket.outbound.ticketdetail.priceAdultUnit > filter.price[1]
      // ) {
      //   return false;
      // }
    }

    if (filter.seatFilter?.length > 0) {
      const find = filter.seatFilter.find(
        (one) => ticket.outbound.ticketdetail.ticketClassCode === one
      );
      if (!find) {
        return false;
      }
    }

    if (filter.timeLand && filter.timeLand.length > 0) {
      const find = filter.timeLand.find((one) => {
        const ticketTime = moment(ticket.outbound.arrivalTimeStr, "HH:mm");
        const lowerTime = moment(one.value[0], "HH:mm");
        const upperTime = moment(one.value[1], "HH:mm");

        return (
          !ticketTime.isBefore(lowerTime) && !ticketTime.isAfter(upperTime)
        );
      });
      if (!find) {
        return false;
      }
    }
    if (filter.liftTimeFilter && filter.liftTimeFilter.length > 0) {
      const find = filter.liftTimeFilter.find((one) => {
        const ticketTime = moment(ticket.outbound.departureTimeStr, "HH:mm");
        const lowerTime = moment(LIFT_TIME_LIST[one - 1]?.from, "HH:mm");
        const upperTime = moment(LIFT_TIME_LIST[one - 1]?.to, "HH:mm");
        return (
          !ticketTime.isBefore(lowerTime) && !ticketTime.isAfter(upperTime)
        );
      });
      if (!find) {
        return false;
      }
    }
    if (filter.transitDuration) {
      const transitDuration = getTransitDuration(ticket);
      if (filter.transitDuration) {
        if (
          transitDuration < filter.transitDuration[0] ||
          transitDuration > filter.transitDuration[1]
        ) {
          return false;
        }
      }
    }

    return true;
  });
  if (cmp) {
    return filteredTickets?.sort(cmp);
  }

  return filteredTickets;
}

export function getInfoGuestTemp(customerInfo, router, totalGuest) {
  const tempGuests = [];

  if (customerInfo?.nameContact) {
    for (var i = 0; i < totalGuest; i++) {
      tempGuests[i] = {
        lastName: customerInfo[`name_${i}`]
          .substr(0, customerInfo[`name_${i}`].indexOf(" "))
          .trim(),
        firstName: customerInfo[`name_${i}`]
          .substr(
            customerInfo[`name_${i}`].indexOf(" "),
            customerInfo[`name_${i}`].length
          )
          .trim(),
        gender: customerInfo[`gender_${i}`],
        outboundBaggageId: customerInfo[`outbound_baggage_${i}`]
          ? customerInfo[`outbound_baggage_${i}`]
          : undefined,
        inboundBaggageId: customerInfo[`inbound_baggage_${i}`]
          ? customerInfo[`inbound_baggage_${i}`]
          : undefined,
        insuranceInfo: customerInfo?.checkValidContactMail
          ? {
              insurancePackageCode: customerInfo?.insurancePackageCode,
              fromDate: `${router?.query?.departureDate}` + " 00:00:00",
              toDate: router?.query?.returnDate
                ? `${router?.query?.returnDate}` + " 00:00:00"
                : `${router?.query?.departureDate}` + " 03:00:00",
            }
          : null,
        passport: customerInfo[`passport_${i}`],
        passportExpiredDate: customerInfo[`passport_expired_${i}`],
        passportCountryId: customerInfo[`passport_country_${i}`],
        nationalityCountryId: customerInfo[`passport_country_${i}`],
        dob: customerInfo[`dob_${i}`],
      };
    }
  }
  return tempGuests;
}

export function getContactInfoTemp(customerInfo) {
  let contactInfo = {};
  if (customerInfo?.nameContact) {
    contactInfo = {
      addr1: customerInfo?.emailContact,
      email: customerInfo?.emailContact,
      lastName: customerInfo?.nameContact
        .substr(0, customerInfo?.nameContact.indexOf(" "))
        .trim(),
      firstName: customerInfo?.nameContact
        .substr(
          customerInfo?.nameContact.indexOf(" "),
          customerInfo?.nameContact.length
        )
        .trim(),
      phone1: customerInfo?.phoneNumberContact,
      gender: customerInfo?.genderContact === "M" ? "M" : "F",
      title: customerInfo?.genderContact === "M" ? "Mr" : "Mrs",
    };
  }
  return contactInfo;
}

export function formatDateOfBirth(str, type = 1) {
  let errorMsg = "";
  if (str.length >= 8) {
    let d = 1;
    let m = 1;
    let y = 1970;
    if (str.length === 8) {
      d = parseInt(str.slice(0, 2));
      m = parseInt(str.slice(2, 4));
      y = parseInt(str.slice(4, 8));
    }
    if (str.indexOf("-") > 0 || str.indexOf("/") > 0) {
      let splString = [];
      if (str.indexOf("-") > 0) {
        splString = str.split("-");
      } else {
        splString = str.split("/");
      }
      d = parseInt(splString[0]);
      m = parseInt(splString[1]);
      y = parseInt(splString[2]);
    }
    let date = new Date(y, m - 1, d);
    let currentDate = new Date();
    let fromAge = 0;
    let toAge = 2;
    if (type === CUSTOMER_TYPE.CHILDREN) {
      fromAge = 2;
      toAge = 11;
    }

    let minDateChild = new Date(
      currentDate.getFullYear() - toAge,
      currentDate.getMonth(),
      currentDate.getDate()
    );
    let maxDateChild = new Date(
      currentDate.getFullYear() - fromAge,
      currentDate.getMonth(),
      currentDate.getDate()
    );
    if (minDateChild < date && date < maxDateChild) {
      errorMsg = "";
    } else {
      if (type === CUSTOMER_TYPE.CHILDREN) {
        errorMsg = `Trẻ em phải từ ${fromAge}-${toAge} tuổi`;
      } else {
        errorMsg = `Em bé phải từ ${fromAge}-${toAge} tuổi`;
      }
    }
  } else {
    errorMsg = "Ngày sinh không hợp lệ";
  }
  return errorMsg;
}

export const paymentStatus = (bookingDetail = {}) => {
  let result = {};
  switch (bookingDetail.paymentStatus) {
    case "success":
    case "completed":
      result = {
        title: "Thành công",
        color: "#48BB78",
        isShowCountDown: false,
        paymentStatus: "Thành công",
        paymentColor: "#48BB78",
        bgPaymentColor: "rgba(72, 187, 120, 0.1)",
      };
      break;
    case "waiting": {
      const expiredTime = !isEmpty(bookingDetail?.expiredTime)
        ? moment(bookingDetail?.expiredTime, "YYYY-MM-DD HH:mm:ss")
        : moment();
      const currentTime = moment();
      const diffTime = expiredTime.valueOf() - currentTime.valueOf();
      if (diffTime > 0) {
        result = {
          title: "Chờ thanh toán",
          color: "#ED8936",
          isShowCountDown: true,
          paymentStatus: "Chờ thanh toán",
          paymentColor: "#ED8936",
          bgPaymentColor: "rgba(237, 137, 54, 0.1)",
        };
      } else {
        result = {
          title: "Hết hạn thanh toán",
          color: "#718096",
          isShowCountDown: false,
          paymentStatus: "Hết hạn thanh toán",
          paymentColor: "#718096",
          bgPaymentColor: "rgba(113, 128, 150, 0.1)",
        };
      }
      break;
    }
    case "fail":
      result = {
        title: "Thất bại",
        color: "#FF1284",
        isShowCountDown: false,
        paymentStatus: "Thất bại",
        paymentColor: "#FF1284",
        bgPaymentColor: "rgba(255, 18, 132, 0.1)",
      };
      break;
    case "awaiting-payment": {
      if (bookingDetail.paymentMethodCode === "PR") {
        result = {
          title: "Thành công",
          color: "#48BB78",
          isShowCountDown: false,
          paymentStatus: "Chờ thanh toán",
          paymentColor: "#ED8936",
          bgPaymentColor: "rgba(237, 137, 54, 0.1)",
        };
      } else if (bookingDetail.paymentMethodCode === "DBT") {
        const expiredTime = !isEmpty(bookingDetail?.expiredTime)
          ? moment(bookingDetail?.expiredTime, "YYYY-MM-DD HH:mm:ss")
          : moment();
        const currentTime = moment();
        const diffTime = expiredTime.valueOf() - currentTime.valueOf();
        if (diffTime > 0) {
          result = {
            title: "Thành công",
            color: "#48BB78",
            isShowCountDown: true,
            paymentStatus: "Chờ thanh toán",
            paymentColor: "#ED8936",
            bgPaymentColor: "rgba(237, 137, 54, 0.1)",
          };
        } else {
          result = {
            title: "Hết hạn thanh toán",
            color: "#718096",
            isShowCountDown: false,
            paymentStatus: "Hết hạn thanh toán",
            paymentColor: "#718096",
            bgPaymentColor: "rgba(113, 128, 150, 0.1)",
          };
        }
      } else {
        const expiredTime = !isEmpty(bookingDetail?.expiredTime)
          ? moment(bookingDetail?.expiredTime, "YYYY-MM-DD HH:mm:ss")
          : moment();
        const currentTime = moment();
        const diffTime = expiredTime.valueOf() - currentTime.valueOf();
        if (diffTime > 0) {
          result = {
            title: "Chờ thanh toán",
            color: "#ED8936",
            isShowCountDown: true,
            paymentStatus: "Chờ thanh toán",
            paymentColor: "#ED8936",
            bgPaymentColor: "rgba(237, 137, 54, 0.1)",
          };
        } else {
          result = {
            title: "Hết hạn thanh toán",
            color: "#718096",
            isShowCountDown: false,
            paymentStatus: "Hết hạn thanh toán",
            paymentColor: "#718096",
            bgPaymentColor: "rgba(113, 128, 150, 0.1)",
          };
        }
      }
      break;
    }
    case "holding": {
      const expiredTime = !isEmpty(bookingDetail?.expiredTime)
        ? moment(bookingDetail?.expiredTime, "YYYY-MM-DD HH:mm:ss")
        : moment();
      const currentTime = moment();
      const diffTime = expiredTime.valueOf() - currentTime.valueOf();
      if (diffTime > 0) {
        result = {
          title: "Đang giữ phòng",
          color: "#ED8936",
          isShowCountDown: true,
          paymentStatus: "Chờ thanh toán",
          paymentColor: "#ED8936",
          bgPaymentColor: "rgba(237, 137, 54, 0.1)",
        };
      } else {
        result = {
          title: "Hết hạn thanh toán",
          color: "#718096",
          isShowCountDown: false,
          paymentStatus: "Hết hạn thanh toán",
          paymentColor: "#718096",
          bgPaymentColor: "rgba(113, 128, 150, 0.1)",
        };
      }
      break;
    }
    case "refunded":
      result = {
        title: "Đã hoàn tiền",
        color: "#6260DF",
        isShowCountDown: false,
        paymentStatus: "Đã hoàn tiền",
        paymentColor: "#6260DF",
        bgPaymentColor: "rgba(98, 96, 223, 0.1)",
      };
      break;
    default:
      result = {
        title: "Hết hạn thanh toán",
        color: "#718096",
        isShowCountDown: false,
        paymentStatus: "Hết hạn thanh toán",
        paymentColor: "#718096",
        bgPaymentColor: "rgba(113, 128, 150, 0.1)",
      };
  }
  return result;
};

export const formatDetailPriceTicketData = (
  ticketOutBound,
  ticketInBound,
  customerInfo,
  dataByType
) => {
  let detailPriceTicket = {};
  if (!isEmpty(ticketOutBound) && !isEmpty(customerInfo)) {
    const dataBagIn = ticketInBound?.ticket?.outbound?.baggages;
    const dataBagOut = ticketOutBound?.ticket?.outbound?.baggages;
    let totalInboundBagMoney = 0;
    let totalOutboundBagMoney = 0;
    for (let index = 0; index < dataByType.totalGuest; index++) {
      let bagInfoById = dataBagOut.filter(
        (el) => el.id === customerInfo[`outbound_baggage_${index}`]
      );

      if (!isEmpty(bagInfoById)) {
        totalOutboundBagMoney += bagInfoById[0]?.price;
      }
      if (ticketInBound) {
        let bagInfoByIdInbound = dataBagIn.filter(
          (el) => el.id === customerInfo[`inbound_baggage_${index}`]
        );
        if (!isEmpty(bagInfoByIdInbound)) {
          totalInboundBagMoney += bagInfoByIdInbound[0]?.price;
        }
      }
    }

    detailPriceTicket = {
      outbound: {
        numAdult: ticketOutBound?.searchRequest?.numAdults,
        adultMoney:
          parseInt(ticketOutBound?.ticket?.outbound?.ticketdetail?.farePrice) *
          parseInt(ticketOutBound?.searchRequest?.numAdults),
        numChildren: ticketOutBound?.searchRequest?.numChildren,
        childMoney:
          parseInt(ticketOutBound?.ticket?.outbound?.ticketdetail?.farePrice) *
          parseInt(ticketOutBound?.searchRequest?.numChildren),
        numInfants: ticketOutBound?.searchRequest?.numInfants,
        babyMoney: ticketOutBound?.ticket?.outbound?.ticketdetail?.priceInfants,
        taxFee:
          parseInt(ticketOutBound?.ticket?.totalPrice) -
          parseInt(
            ticketOutBound?.ticket?.outbound?.ticketdetail?.farePrice *
              (ticketOutBound?.searchRequest?.numAdults +
                ticketOutBound?.searchRequest?.numChildren)
          ),
        baggageMoney: totalOutboundBagMoney,
        total: ticketOutBound?.ticket?.totalPrice + totalOutboundBagMoney,
      },
      inbound: ticketInBound
        ? {
            numAdult: ticketInBound?.searchRequest?.numAdults,
            adultMoney:
              parseInt(
                ticketInBound?.ticket?.outbound?.ticketdetail?.farePrice
              ) * parseInt(ticketInBound?.searchRequest?.numAdults),
            numChildren: ticketInBound?.searchRequest?.numChildren,
            childMoney:
              parseInt(
                ticketInBound?.ticket?.outbound?.ticketdetail?.farePrice
              ) * parseInt(ticketInBound?.searchRequest?.numChildren),
            numInfants: ticketInBound?.searchRequest?.numInfants,
            babyMoney:
              ticketInBound?.ticket?.outbound?.ticketdetail?.priceInfants,
            taxFee:
              parseInt(ticketInBound?.ticket?.totalPrice) -
              parseInt(
                ticketInBound?.ticket?.outbound?.ticketdetail?.farePrice *
                  (ticketInBound?.searchRequest?.numAdults +
                    ticketInBound?.searchRequest?.numChildren)
              ),
            baggageMoney: totalInboundBagMoney,
            total: ticketInBound?.ticket?.totalPrice + totalInboundBagMoney,
          }
        : null,
      insurance: dataByType.totalMoneyInsurance,
      voucherCode: dataByType?.discountCode,
      voucherDescription: dataByType?.discountDesc,
      voucherMoney: dataByType.discountMoney,
      surchargeMoney: dataByType.surchargeMoney,
      totalPayment: dataByType.totalMoneyBoxPrice,
    };
  }
  return detailPriceTicket;
};

export const formatDetailPriceTicketBookingData = (orderDetail) => {
  let detailPriceTicket = {};
  if (!isEmpty(orderDetail)) {
    let totalInboundBagMoney = 0;
    let totalOutboundBagMoney = 0;
    if (orderDetail?.guests) {
      orderDetail?.guests.forEach((guest, idx) => {
        totalInboundBagMoney += guest?.inboundBaggage?.price;
        totalOutboundBagMoney += guest?.outboundBaggage?.price;
      });
    }

    const ticketOutBound = orderDetail.outbound;
    const ticketInBound = orderDetail.inbound;

    detailPriceTicket = {
      outbound: {
        baggageMoney: totalOutboundBagMoney,
        total: ticketOutBound?.price,
      },
      inbound: ticketInBound
        ? {
            baggageMoney: totalInboundBagMoney,
            total: ticketInBound?.price,
          }
        : null,
      insurance: orderDetail?.insuranceAmount || "",
      voucherCode: orderDetail?.promotionCode || "",
      voucherMoney: orderDetail?.discount || 0,
      totalPayment: orderDetail.finalPrice,
    };
  }
  return detailPriceTicket;
};

export const getDetailBaggageByCustomer = (data) => {
  let dataDetail = [];
  data.forEach((element, index) => {
    let dataTemp = {
      name: element.name,
      weight: (element.departureWeight || 0) + (element.returnWeight || 0),
      money: (element.departureMoney || 0) + (element.returnMoney || 0),
    };
    if (dataTemp.weight > 0) {
      dataDetail.push(dataTemp);
    }
  });

  return dataDetail;
};

export const resetDataAfterBookTicket = (dataStorage, name, totalGuest) => {
  let result = [];
  if (!isEmpty(dataStorage)) {
    result = {
      ...result,
      genderContact: "M",
      nameContact: "",
      phoneNumberContact: "",
      emailContact: "",
      checkValidContactMail: false,
      checkedSame: false,
    };
    for (var index = 0; index < totalGuest; index++) {
      result = {
        ...result,
        [`name_${index}`]: "",
        [`gender_${index}`]: "M",
        [`outbound_baggage_${index}`]: undefined,
        [`inbound_baggage_${index}`]: undefined,
        [`passport_${index}`]: "",
        [`passport_expired_${index}`]: "",
        [`passport_residence_${index}`]: "",
        [`passport_country_${index}`]: "",
        [`insurance_${index}`]: "",
        [`dob_${index}`]: "",
      };
    }

    localStorage.setItem(name, JSON.stringify(result));
  }
};

export const getPaymentStatusBookingFlight = (
  status,
  paymentMethodCode,
  expiredTime
) => {
  const theme = useTheme();
  if (
    (paymentMethodCode === flightPaymentMethodCode.BANK_TRANSFER ||
      paymentMethodCode === flightPaymentMethodCode.BANK_TRANSFER_2) &&
    status === flightPaymentStatus.HOLDING
  ) {
    return {
      title: listString.IDS_TEXT_ORDER_PAYMENT_STATUS_WAITING_PAYMENT,
      color: theme.palette.orange.main,
      bgColor: theme.palette.orange.orangeLight2,
      type: flightPaymentStatus.PENDING,
    };
  }

  if (
    (status === flightPaymentStatus.HOLDING &&
      expiredTime / 1000 <= moment().unix()) ||
    status === flightPaymentStatus.CANCELLING_HOLDING
  ) {
    return {
      title: listString.IDS_TEXT_EXPIRED_HOLDING,
      color: theme.palette.gray.grayDark7,
      bgColor: theme.palette.gray.grayLight27,
      type: flightPaymentStatus.CANCELLING_HOLDING,
    };
  }

  switch (status) {
    case flightPaymentStatus.SUCCESS:
    case flightPaymentStatus.COMPLETED:
    case flightPaymentStatus.PAYMENT_SUCCESS:
      return {
        title: listString.IDS_TEXT_ORDER_PAYMENT_STATUS_SUCCESS,
        color: theme.palette.green.greenLight7,
        bgColor: theme.palette.green.greenLight8,
        type: flightPaymentStatus.SUCCESS,
      };
    case flightPaymentStatus.FAIL:
      return {
        title: listString.IDS_TEXT_ORDER_PAYMENT_STATUS_FAIL,
        color: theme.palette.red.redLight5,
        bgColor: theme.palette.red.redLight6,
        type: flightPaymentStatus.FAIL,
      };
    case flightPaymentStatus.PENDING:
    case flightPaymentStatus.AWAITING_PAYMENT:
    case flightPaymentStatus.WAITING_PAYMENT:
      return {
        title: listString.IDS_TEXT_ORDER_PAYMENT_STATUS_WAITING_PAYMENT,
        color: theme.palette.orange.main,
        bgColor: theme.palette.orange.orangeLight2,
        type: flightPaymentStatus.PENDING,
      };
    case flightPaymentStatus.CANCELLED_BY_PROVIDER:
      return {
        title: listString.IDS_TEXT_ORDER_PAYMENT_STATUS_CANCELLED,
        color: theme.palette.gray.grayDark7,
        bgColor: theme.palette.gray.grayLight27,
        type: flightPaymentStatus.CANCELLED_BY_PROVIDER,
      };
    case flightPaymentStatus.REFUNDED_POSTED:
    case flightPaymentStatus.REFUNDED:
      return {
        title: listString.IDS_TEXT_ORDER_PAYMENT_STATUS_REFUNDED,
        color: theme.palette.purple.main,
        bgColor: theme.palette.purple.purpleLight1,
        type: flightPaymentStatus.REFUNDED,
      };
    case flightPaymentStatus.DISPUTED:
      return {
        title: listString.IDS_TEXT_ORDER_PAYMENT_STATUS_DISPUTED,
        color: theme.palette.gray.grayDark7,
        bgColor: theme.palette.gray.grayLight27,
        type: flightPaymentStatus.DISPUTED,
      };
    case flightPaymentStatus.HOLDING:
      return {
        title: listString.IDS_TEXT_ORDER_PAYMENT_STATUS_HOLDING,
        color: theme.palette.orange.main,
        bgColor: theme.palette.orange.orangeLight2,
        type: flightPaymentStatus.HOLDING,
      };
    default:
      return {
        title: listString.IDS_TEXT_ORDER_PAYMENT_STATUS_SUCCESS,
        color: theme.palette.green.greenLight7,
        bgColor: theme.palette.green.greenLight8,
        type: flightPaymentStatus.SUCCESS,
      };
  }
};

export const getInvoiceStatus = (
  paymentStatus = flightPaymentStatus.FAIL,
  invoiceStatus = ""
) => {
  const theme = useTheme();
  if (paymentStatus === flightPaymentStatus.SUCCESS) {
    switch (invoiceStatus) {
      case "handling":
        return {
          text: "Đang xử lý",
          textColor: theme.palette.gray.grayDark7,
        };
      case "completed":
        return {
          text: "Đã xử lý",
          textColor: theme.palette.green.greenLight7,
        };
      default:
        return {
          text: "Chờ xử lý",
          textColor: theme.palette.blue.blueLight8,
        };
    }
  } else {
    return {
      text: "Chưa thanh toán",
      textColor: theme.palette.blue.blueLight8,
    };
  }
};

export const CLASS_CODE = [
  {
    code: "economy",
    v_name: "Phổ Thông",
  },
  {
    code: "premium_economy",
    v_name: "Phổ Thông Đặc Biệt",
  },
  {
    code: "business",
    v_name: "Thương Gia",
  },
  {
    code: "first",
    v_name: "Hạng Nhất",
  },
];

export const getTicketClass = (ticket, ticketClass) => {
  const tempTicketClassCode = ticketClass ? ticketClass : CLASS_CODE;
  return tempTicketClassCode?.find(
    (element) => element.code === ticket.ticketdetail?.ticketClassCode
  )?.v_name;
};

export const getPolicyIcon = (code, colorTone = "#4A5568") => {
  if (code === "LUGGAGE_INFO") {
    return (
      <IconLuggage
        className="svgFillAll"
        style={{
          height: "16px",
          width: "auto",
          marginRight: "4px",
          stroke: colorTone,
          marginTop: 2,
        }}
      />
    );
  }

  if (code === "CHANGE_ROUTE") {
    return (
      <IconFrame
        className="svgFillAll"
        style={{
          height: "16px",
          width: "auto",
          marginRight: "4px",
          stroke: colorTone,
          marginTop: 3,
        }}
      />
    );
  }
  if (code === "VOID_TICKET") {
    return (
      <IconNoRefundTicket
        className="svgFillAll"
        style={{
          height: "16px",
          width: "auto",
          marginRight: "4px",
          stroke: colorTone,
          marginTop: 3,
        }}
      />
    );
  }
  if (code === "INCLUDED_ANCILLARY") {
    return (
      <IconEat
        className="svgFillAll"
        style={{
          height: "16px",
          width: "auto",
          marginRight: "4px",
          stroke: colorTone,
          marginTop: 3,
        }}
      />
    );
  }
  if (code === "OTHERS") {
    return (
      <IconInfo
        className="svgFillAll"
        style={{
          height: "18px",
          width: "auto",
          marginRight: "2px",
          stroke: colorTone,
        }}
      />
    );
  }
  return (
    <IconFrame
      className="svgFillAll"
      style={{
        height: "16px",
        width: "auto",
        marginRight: "4px",
        color: colorTone,
        marginTop: 3,
      }}
    />
  );
};

export const getFlightInfomation = (query, domesticInBound) => {
  const codeTemp = convertUrlFlight(query?.slug);
  let tempCodeFlight = null;
  tempCodeFlight = domesticInBound.find((element) => {
    return element?.code === codeTemp;
  });
  if (
    ((query?.slug && query?.slug[0] === "inbound") ||
      (query?.slug && query?.slug[0] === "outbound")) &&
    query?.origin_code &&
    query?.destination_code
  ) {
    const code = `${query.origin_code}-${query.destination_code}`;
    tempCodeFlight = {
      code,
      depart: query?.origin_location?.replace("Thành phố ", ""),
      arrival: query?.destination_location,
    };
  } else if (isEmpty(tempCodeFlight)) {
    if (query?.slug && query?.slug[0] === "tu") {
      tempCodeFlight = domesticInBound.find((element) => {
        return element?.code.split("-")[0] === codeTemp.split("-")[1];
      });
    }

    if (query?.slug && query?.slug[0] === "den") {
      tempCodeFlight = domesticInBound.find((element) => {
        return element?.code.split("-")[1] === codeTemp.split("-")[1];
      });
    }
  }
  return tempCodeFlight;
};

export const getFlightListInfomation = (query, domesticInBound) => {
  const codeTemp = convertUrlFlight(query?.slug);
  let tempCodeFlight = null;
  tempCodeFlight = domesticInBound.filter((element) => {
    return element?.code?.split("-")[1] === codeTemp?.split("-")[1];
  });
  return tempCodeFlight.slice(0, 5)?.map((item) => ({
    arrivalCity: item?.arrival,
    departCity: item?.depart,
    fromAirport: item?.code?.split("-")[0],
    toAirport: item?.code?.split("-")[1],
  }));
};

export const generateSlugFromFlightInfo = (flight, slug) => {
  if (slug === "den") {
    return `${flight?.arrival?.replace(/ /g, "-")}.${
      flight?.code?.split("-")[1]
    }`;
  }
  return `${flight?.depart?.replace(/ /g, "-")}-${flight?.arrival?.replace(
    / /g,
    "-"
  )}.${flight?.code?.replace(/-/g, ".")}`;
};

export const handleColorAirlineFrom = (id) => {
  return airlineColor.find((item) => item.id === id)?.color;
};

export const getFirstName = (name = "") => {
  let result = "";
  if (isEmpty(name)) {
    return "KT";
  }
  const listName = name.trim().split(" ");
  if (listName.length === 1) {
    result = listName[0].split("").shift();
  } else if (listName.length > 1) {
    result = `${listName[0].split("").shift()}${listName[listName.length - 1]
      .split("")
      .shift()}`;
  }

  return result;
};

export const handleTitleFlight = (query, codeFlight) => {
  if (query && !query?.slug?.length) {
    return {
      title: `Đặt vé máy bay giá rẻ - giá tốt nhất trên Mytour.vn | Bạn đang tìm vé máy bay khuyến mãi?`,
      description: `Đặt vé máy bay giá rẻ trên Mytour. Đặt vé máy bay Vietnam Airlines, Vietjet, Bamboo Airways, Jetstar nhiều khuyến mãi. Giá trung thực, không phí ẩn!`,
      keywords: `Vé máy bay, đặt vé máy bay, vé vietnam airlines, jetstar, vietjet, bamboo airways, hỗ trợ dịch vụ khách hàng 24/7 đặt vé máy bay`,
    };
  } else if (query?.slug[0] === "outbound" || query?.slug[0] === "inbound") {
    return {
      title: `Vé máy bay giá rẻ từ ${codeFlight?.depart} đi ${codeFlight?.arrival} trên Mytour.vn`,
      description: `Đặt vé máy bay giá rẻ trên Mytour. Đặt vé máy bay Vietnam Airlines, Vietjet, Bamboo Airways, Jetstar nhiều khuyến mãi. Giá trung thực, không phí ẩn!`,
      keywords: `Vé máy bay, đặt vé máy bay, vé vietnam airlines, jetstar, vietjet, bamboo airways, hỗ trợ dịch vụ khách hàng 24/7 đặt vé máy bay`,
    };
  }
  const afterDot = query?.slug[1].substr(query.slug[1].indexOf(".") + 1);
  const arrayCode = afterDot.split(".");
  if (arrayCode[arrayCode.length - 2] && arrayCode[arrayCode.length - 1]) {
    return {
      title: `Vé máy bay từ ${codeFlight?.depart} đi ${codeFlight?.arrival} giá rẻ nhất trên Mytour.vn`,
      description: `Đặt vé máy bay giá rẻ trên Mytour. Đặt vé máy bay Vietnam Airlines, Vietjet, Bamboo Airways, Jetstar nhiều khuyến mãi. Giá trung thực, không phí ẩn!`,
      keywords: `Vé máy bay, đặt vé máy bay, vé vietnam airlines, jetstar, vietjet, bamboo airways, hỗ trợ dịch vụ khách hàng 24/7 đặt vé máy bay`,
    };
  } else if (query?.slug && query?.slug[0] === "tu") {
    return {
      title: `Vé máy bay giá rẻ nhất khởi hành từ ${codeFlight?.depart} (${
        codeFlight?.code?.split("-")[0]
      }) 2021 trên Mytour.vn`,
      description: `Đặt vé máy bay giá rẻ trên Mytour. Đặt vé máy bay Vietnam Airlines, Vietjet, Bamboo Airways, Jetstar nhiều khuyến mãi. Giá trung thực, không phí ẩn!`,
      keywords: `Vé máy bay, đặt vé máy bay, vé vietnam airlines, jetstar, vietjet, bamboo airways, hỗ trợ dịch vụ khách hàng 24/7 đặt vé máy bay `,
    };
  } else if (query?.slug && query?.slug[0] === "result") {
    return {
      title: `Vé máy bay giá rẻ từ ${codeFlight?.depart} đi ${codeFlight?.arrival} trên Mytour.vn`,
      description: `Đặt vé máy bay giá rẻ trên Mytour. Đặt vé máy bay Vietnam Airlines, Vietjet, Bamboo Airways, Jetstar nhiều khuyến mãi. Giá trung thực, không phí ẩn!`,
      keywords: `Vé máy bay, đặt vé máy bay, vé vietnam airlines, jetstar, vietjet, bamboo airways, hỗ trợ dịch vụ khách hàng 24/7 đặt vé máy bay`,
    };
  } else if (query?.slug && query?.slug[0] === "den") {
    return {
      title: `Vé máy bay giá rẻ đi ${codeFlight?.arrival} (${
        codeFlight?.code?.split("-")[1]
      }) 2021 trên Mytour.vn`,
      description: `Đặt vé máy bay giá rẻ trên Mytour. Đặt vé máy bay Vietnam Airlines, Vietjet, Bamboo Airways, Jetstar nhiều khuyến mãi. Giá trung thực, không phí ẩn!`,
      keywords: `Vé máy bay, đặt vé máy bay, vé vietnam airlines, jetstar, vietjet, bamboo airways, hỗ trợ dịch vụ khách hàng 24/7 đặt vé máy bay`,
    };
  } else {
    return {
      title: `Đặt vé máy bay giá rẻ - giá tốt nhất trên Mytour.vn | Bạn đang tìm vé máy bay khuyến mãi?`,
      description: `Đặt vé máy bay giá rẻ trên Mytour. Đặt vé máy bay Vietnam Airlines, Vietjet, Bamboo Airways, Jetstar nhiều khuyến mãi. Giá trung thực, không phí ẩn!`,
      keywords: `Vé máy bay, đặt vé máy bay, vé vietnam airlines, jetstar, vietjet, bamboo airways, hỗ trợ dịch vụ khách hàng 24/7 đặt vé máy bay`,
    };
  }
};

export const handleTitleFlightSearchForm = (query, codeFlight) => {
  const afterDot =
    query?.slug &&
    query?.slug.length > 1 &&
    query?.slug[1].substr(query.slug[1].indexOf(".") + 1);
  const arrayCode = afterDot?.split(".");
  if (
    arrayCode &&
    arrayCode[arrayCode.length - 2] &&
    arrayCode[arrayCode.length - 1]
  ) {
    // nếu slug có điểm đi, điểm đến cụ thể thì trả về chặng có điểm đi điểm đến
    return ` từ ${codeFlight?.depart}  (${
      codeFlight?.code?.split("-")[0]
    }) đi ${codeFlight?.arrival}  (${codeFlight?.code?.split("-")[1]})`;
  } else if (query?.slug && query?.slug[0] === "den") {
    // nếu slug chỉ có điểm đến (dựa vào slug="den") thì trả về chặng bay đi tới điểm đến
    return ` đến ${codeFlight?.arrival} (${codeFlight?.code?.split("-")[1]})`;
  } else {
    return ``;
  }
};

export const Policy = (props) => {
  const { item, isMobile } = props;
  const theme = useTheme();
  const colorTone =
    item.description.indexOf("Không") !== -1
      ? theme.palette.red.redLight5
      : item.description.indexOf("Miễn phí") !== -1
      ? theme.palette.green.greenLight7
      : undefined;
  return (
    <div
      style={{
        display: "flex",
        minWidth: "125px",
        alignItems: "flex-start",
        marginBottom: 8,
      }}
    >
      <div
        style={{
          flexShrink: 0,
        }}
      >
        {getPolicyIcon(item.code, colorTone)}
      </div>
      <Typography>
        <Typography
          variant={isMobile ? "body2" : "caption"}
          component="span"
          style={{
            color: colorTone,
          }}
        >
          {item.description}
        </Typography>
        {item.code === "TECHNICAL_STOP" && (
          <Typography color="error" variant="subtitle2" component="span">
            &nbsp; result.needVisa
          </Typography>
        )}
      </Typography>
    </div>
  );
};
export const stringSlug = (string = "") => {
  const separator = "-";
  const includeDot = false;
  let text = string;
  if (!text) text = "";
  text = text
    .toString()
    .toLowerCase()
    .trim();
  const sets = [
    { to: "a", from: "[ÀÁÂÃÄÅÆĀĂĄẠẢẤẦẨẪẬẮẰẲẴẶ]" },
    { to: "c", from: "[ÇĆĈČ]" },
    { to: "d", from: "[ÐĎĐÞ]" },
    { to: "e", from: "[ÈÉÊËĒĔĖĘĚẸẺẼẾỀỂỄỆ]" },
    { to: "g", from: "[ĜĞĢǴ]" },
    { to: "h", from: "[ĤḦ]" },
    { to: "i", from: "[ÌÍÎÏĨĪĮİỈỊ]" },
    { to: "j", from: "[Ĵ]" },
    { to: "ij", from: "[Ĳ]" },
    { to: "k", from: "[Ķ]" },
    { to: "l", from: "[ĹĻĽŁ]" },
    { to: "m", from: "[Ḿ]" },
    { to: "n", from: "[ÑŃŅŇ]" },
    { to: "o", from: "[ÒÓÔÕÖØŌŎŐỌỎỐỒỔỖỘỚỜỞỠỢǪǬƠ]" },
    { to: "oe", from: "[Œ]" },
    { to: "p", from: "[ṕ]" },
    { to: "r", from: "[ŔŖŘ]" },
    { to: "s", from: "[ßŚŜŞŠ]" },
    { to: "t", from: "[ŢŤ]" },
    { to: "u", from: "[ÙÚÛÜŨŪŬŮŰŲỤỦỨỪỬỮỰƯ]" },
    { to: "w", from: "[ẂŴẀẄ]" },
    { to: "x", from: "[ẍ]" },
    { to: "y", from: "[ÝŶŸỲỴỶỸ]" },
    { to: "z", from: "[ŹŻŽ]" },
  ];

  if (includeDot) sets.push({ to: "-", from: "[·/_.,:;']" });
  else sets.push({ to: "-", from: "[·/_,:;']" });

  sets.forEach((set) => {
    text = text.replace(new RegExp(set.from, "gi"), set.to);
  });

  text = text
    .toString()
    .toLowerCase()
    .replace(/\s+/g, "-") // Replace spaces with -
    .replace(/&/g, "-and-") // Replace & with 'and'
    .replace(/[^\w-]+/g, "") // Remove all non-word chars
    .replace(/--+/g, "-") // Replace multiple - with single -
    .replace(/^-+/, "") // Trim - from start of text
    .replace(/-+$/, ""); // Trim - from end of text

  if (typeof separator !== "undefined" && separator !== "-") {
    text = text.replace(/-/g, separator);
  }

  return text ? text : "a";
};

export const removeAccent = (str) => {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
  str = str.replace(/Đ/g, "D");
  return str;
};

export const getTechnicalStop = (policies) => {
  const stop = policies.find((policy) => {
    if (policy.code === "TECHNICAL_STOP") {
      return true;
    }
    return false;
  });
  if (stop) {
    const { moreInfo } = stop;
    if (moreInfo) {
      return moreInfo.airportCode;
    }
  }
  return null;
};

export function getStops(ticket) {
  const value = [];
  if (ticket.transitTickets) {
    for (let i = 0; i < ticket.transitTickets.length; i += 1) {
      const transitTicket = ticket.transitTickets[i];
      const tsTop = getTechnicalStop(transitTicket.polices);
      if (tsTop) {
        value.push({ code: tsTop, city: "", technical: true });
      }

      if (i !== ticket.transitTickets.length - 1) {
        value.push({
          code: transitTicket.arrivalAirport,
          city: transitTicket.arrivalCity,
        });
      }
    }
  } else {
    const tsTop = getTechnicalStop(ticket.ticketdetail.polices);
    if (tsTop) {
      value.push({ code: tsTop, city: "" });
    }
  }
  return value;
}

export const getHeaderInServer = (headers = {}) => {
  let result = {
    token: "",
  };
  if (!isEmpty(headers.cookie)) {
    const listCookie = headers.cookie.split("; ") || [];
    listCookie.forEach((el) => {
      const cookies = el.split("=");
      if (cookies[0] === "token") {
        result = {
          ...result,
          token: cookies[1],
        };
      }
    });
  }
  return result;
};

export const checkFlightTime2Ways = (tempTicketOutBound, item) => {
  if (tempTicketOutBound) {
    if (
      moment(item?.arrivalDayStr, DATE_FORMAT, true).diff(
        moment(tempTicketOutBound?.outbound?.arrivalDayStr, DATE_FORMAT, true),
        "days"
      ) < 0
    ) {
      console.log("<0");
      return;
    } else if (
      moment(item?.arrivalDayStr, DATE_FORMAT, true).diff(
        moment(tempTicketOutBound?.outbound?.arrivalDayStr, DATE_FORMAT, true),
        "days"
      ) === 0
    ) {
      if (
        moment(item?.departureTimeStr, HOUR_MINUTE, true).diff(
          moment(
            tempTicketOutBound?.outbound?.departureTimeStr,
            HOUR_MINUTE,
            true
          ),
          "hour"
        ) < 3
      ) {
        return;
      } else {
        return true;
      }
    } else {
      return true;
    }
  } else {
    return true;
  }
};
