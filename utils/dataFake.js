import { IconTotalHotel, IconPayment } from "@public/icons";
import { listString, prefixUrlIcon, listIcons } from "@utils/constants";

export const dataItemStar = [
  {
    id: 2,
    star: "≤2",
  },
  {
    id: 3,
    star: 3,
  },
  {
    id: 4,
    star: 4,
  },
  {
    id: 5,
    star: 5,
  },
];

export const dataUserVote = [
  {
    id: 0,
    value: { minRating: 4.5, maxRating: 5 },
    name: listString.IDS_MT_TEXT_USER_VOTE_GREAT,
  },
  {
    id: 1,
    value: { minRating: 4, maxRating: 5 },
    name: listString.IDS_MT_TEXT_USER_VOTE_VERY_GOOD,
  },
  {
    id: 2,
    value: { minRating: 3.5, maxRating: 5 },
    name: listString.IDS_MT_TEXT_USER_VOTE_GOOD,
  },
];

export const listFieldFilterHotel = [
  "minPrice",
  "maxPrice",
  "stars",
  "services",
  "ratingRanges",
  "types",
  "subLocations",
  "amenities",
];

export const listInfoDescriptionHome = [
  {
    id: 0,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconSupport247}`}
        alt="logo_support_247"
        style={{ width: 52, height: 52 }}
      />
    ),
    title: listString.IDS_MT_TEXT_SUPPORT_247,
    description: listString.IDS_TEXT_WHY_BOOK_HOTEL_SUPPORT_247_DESC,
  },
  {
    id: 1,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconBestPrice}`}
        alt="logo_best_price"
        style={{ width: 52, height: 52 }}
      />
    ),
    title: listString.IDS_MT_TEXT_BEST_PRICE_OF_DAY,
    description: listString.IDS_MT_TEXT_COMMIT_BEST_PRICE_OF_DAY,
  },
  {
    id: 2,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconPayment}`}
        alt="logo_payment"
        style={{ width: 52, height: 52 }}
      />
    ),
    title: listString.IDS_MT_TEXT_PAYMENT_EASY,
    description: listString.IDS_MT_TEXT_SPEED_ORDER_MANY_PAYMENT,
  },
  {
    id: 3,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconTotalHotel}`}
        alt="logo_total_hotel"
        style={{ width: 52, height: 52 }}
      />
    ),
    title: listString.IDS_TEXT_WHY_BOOK_HOTEL_TICKET_TITLE,
    description: listString.IDS_TEXT_WHY_BOOK_HOTEL_TICKET_DESC,
  },
];

export const listInfoDescriptionHotel = [
  {
    id: 0,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconSupport247}`}
        alt="logo_support_247"
        style={{ width: 52, height: 52 }}
      />
    ),
    title: listString.IDS_MT_TEXT_SUPPORT_247,
    description: listString.IDS_TEXT_WHY_BOOK_HOTEL_SUPPORT_247_DESC,
  },
  {
    id: 1,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconBestPrice}`}
        alt="logo_best_price"
        style={{ width: 52, height: 52 }}
      />
    ),
    title: listString.IDS_MT_TEXT_BEST_PRICE_OF_DAY,
    description: listString.IDS_MT_TEXT_COMMIT_BEST_PRICE_OF_DAY,
  },
  {
    id: 2,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconPayment}`}
        alt="logo_payment"
        style={{ width: 52, height: 52 }}
      />
    ),
    title: listString.IDS_MT_TEXT_PAYMENT_EASY,
    description: listString.IDS_MT_TEXT_SPEED_ORDER_MANY_PAYMENT,
  },
  {
    id: 3,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconTotalHotel}`}
        alt="logo_total_hotel"
        style={{ width: 52, height: 52 }}
      />
    ),
    title: listString.IDS_TEXT_WHY_BOOK_HOTEL_TICKET_TITLE,
    description: listString.IDS_TEXT_WHY_BOOK_HOTEL_TICKET_DESC,
  },
];

export const listWhyFlight = [
  {
    id: "0",
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconSupport247}`}
        alt="logo_support_247"
        style={{ width: 52, height: 52 }}
      />
    ),
    title: listString.IDS_MT_TEXT_SUPPORT_247,
    desc: listString.IDS_TEXT_WHY_BOOK_FLIGHT_SUPPORT_DESC,
  },
  {
    id: "1",
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconPayment}`}
        alt="logo_payment"
        style={{ width: 52, height: 52 }}
      />
    ),
    title: listString.IDS_MT_TEXT_PAYMENT_EASY,
    desc: listString.IDS_MT_TEXT_SPEED_ORDER_MANY_PAYMENT,
  },
  {
    id: "2",
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconAirPlane}`}
        alt="logo_air_plane"
        style={{ width: 52, height: 52 }}
      />
    ),
    title: listString.IDS_TEXT_WHY_BOOK_FLIGHT_TICKET_TITLE,
    desc: listString.IDS_TEXT_WHY_BOOK_FLIGHT_TICKET_DESC,
  },
];
export const topAddressLocation = [
  { id: 21, name: "Sapa, Lào Cai", aliasCode: "td503" }, // id chưa đúng
  { id: 23, name: "Phan Thiết", aliasCode: "td433" }, // id chưa đúng
  { id: 23, name: "Mũi Né, Bình Thuận", aliasCode: "ts28086" }, // check lại aliasCode
  { id: 10, name: "Hạ Long, Quảng Ninh", aliasCode: "td235" },
  { id: 28, name: "Hội An, Quảng Nam", aliasCode: "td269" },
  { id: 43, name: "Nha Trang, Khánh Hòa", aliasCode: "td414" },
  { id: 20, name: "Đà Lạt, Lâm Đồng", aliasCode: "td155" },
  { id: 2, name: "Phú Quốc, Kiên Giang", aliasCode: "td446" },
];
export const cityList = [
  { id: 1, name: "Thừa Thiên - Huế", aliasCode: "tp1" },
  { id: 2, name: "Kiên Giang", aliasCode: "tp2" },
  { id: 3, name: "Hải Phòng", aliasCode: "tp3" },
  { id: 4, name: "Gia Lai", aliasCode: "tp4" },
  { id: 5, name: "Bình Định", aliasCode: "tp5" },
  { id: 6, name: "An Giang", aliasCode: "tp6" },
  { id: 7, name: "Nghệ An", aliasCode: "tp7" },
  { id: 8, name: "Hưng Yên", aliasCode: "tp8" },
  { id: 9, name: "Bắc Kạn", aliasCode: "tp9" },
  { id: 10, name: "Quảng Ninh", aliasCode: "tp10" },
  { id: 11, name: "Hà Nội", aliasCode: "tp11" },
  { id: 12, name: "Quảng Bình", aliasCode: "tp12" },
  { id: 13, name: "Quảng Ngãi", aliasCode: "tp13" },
  { id: 14, name: "Bến Tre", aliasCode: "tp14" },
  { id: 15, name: "Bà Rịa - Vũng Tàu", aliasCode: "tp15" },
  { id: 16, name: "Thanh Hóa", aliasCode: "tp16" },
  { id: 17, name: "Ninh Thuận", aliasCode: "tp17" },
  { id: 18, name: "Bạc Liêu", aliasCode: "tp18" },
  { id: 19, name: "Cao Bằng", aliasCode: "tp19" },
  { id: 20, name: "Lâm Đồng", aliasCode: "tp20" },
  { id: 21, name: "Lào Cai", aliasCode: "tp21" },
  { id: 22, name: "Bình Dương", aliasCode: "tp22" },
  { id: 23, name: "Bình Thuận", aliasCode: "tp23" },
  { id: 24, name: "Bắc Giang", aliasCode: "tp24" },
  { id: 25, name: "Hà Giang", aliasCode: "tp25" },
  { id: 26, name: "Bắc Ninh", aliasCode: "tp26" },
  { id: 27, name: "Lạng Sơn", aliasCode: "tp27" },
  { id: 28, name: "Quảng Nam", aliasCode: "tp28" },
  { id: 29, name: "Sơn La", aliasCode: "tp29" },
  { id: 30, name: "Tây Ninh", aliasCode: "tp30" },
  { id: 31, name: "Long An", aliasCode: "tp31" },
  { id: 32, name: "Đồng Nai", aliasCode: "tp32" },
  { id: 33, name: "Hồ Chí Minh", aliasCode: "tp33" },
  { id: 34, name: "Hải Dương", aliasCode: "tp34" },
  { id: 35, name: "Bình Phước", aliasCode: "tp35" },
  { id: 36, name: "Hà Nam", aliasCode: "tp36" },
  { id: 37, name: "Vĩnh Long", aliasCode: "tp37" },
  { id: 38, name: "Cần Thơ", aliasCode: "tp38" },
  { id: 39, name: "Vĩnh Phúc", aliasCode: "tp39" },
  { id: 40, name: "Đắk Lắk", aliasCode: "tp40" },
  { id: 41, name: "Cà Mau", aliasCode: "tp41" },
  { id: 42, name: "Tiền Giang", aliasCode: "tp42" },
  { id: 43, name: "Khánh Hòa", aliasCode: "tp43" },
  { id: 44, name: "Quảng Trị", aliasCode: "tp44" },
  { id: 45, name: "Hà Tĩnh", aliasCode: "tp45" },
  { id: 46, name: "Trà Vinh", aliasCode: "tp46" },
  { id: 47, name: "Đồng Tháp", aliasCode: "tp47" },
  { id: 48, name: "Hòa Bình", aliasCode: "tp48" },
  { id: 49, name: "Phú Thọ", aliasCode: "tp49" },
  { id: 50, name: "Đà Nẵng", aliasCode: "tp50" },
  { id: 51, name: "Hậu Giang", aliasCode: "tp51" },
  { id: 52, name: "Sóc Trăng", aliasCode: "tp52" },
  { id: 53, name: "Tuyên Quang", aliasCode: "tp53" },
  { id: 54, name: "Đắk Nông", aliasCode: "tp54" },
  { id: 55, name: "Thái Nguyên", aliasCode: "tp55" },
  { id: 56, name: "Kon Tum", aliasCode: "tp56" },
  { id: 57, name: "Điện Biên", aliasCode: "tp57" },
  { id: 58, name: "Phú Yên", aliasCode: "tp58" },
  { id: 59, name: "Thái Bình", aliasCode: "tp59" },
  { id: 60, name: "Ninh Bình", aliasCode: "tp60" },
  { id: 61, name: "Nam Định", aliasCode: "tp61" },
  { id: 62, name: "Lai Châu", aliasCode: "tp62" },
  { id: 63, name: "Yên Bái", aliasCode: "tp63" },
];

export const listDestinationFirst = [
  {
    code: "HAN-SGN",
    depart: "Hà Nội",
    arrival: "Hồ Chí Minh",
    provinceId: 33,
    country: "Việt Nam",
    thumb:
      "https://content.skyscnr.com/0012f421f0682d22815185912d6e105c/GettyImages-481711830.jpg",
  },
  {
    code: "SGN-HAN",
    depart: "Hồ Chí Minh",
    arrival: "Hà Nội",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://content.skyscnr.com/02f6535440ff778b7292a2c90cf4e027/GettyImages-476258421.jpg",
  },
  {
    code: "HAN-DAD",
    depart: "Hà Nội",
    arrival: "Đà Nẵng",
    provinceId: 50,
    country: "Việt Nam",
    thumb:
      "https://content.skyscnr.com/0ccd022e5d074467344ac3d01d5d1df7/GettyImages-178793470.jpg",
  },
  {
    code: "SGN-VII",
    depart: "Hồ Chí Minh",
    arrival: "Thành phố Vinh",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://pix10.agoda.net/hotelImages/5663774/-1/00f12730a33df7f9a31657bcdf749e93.jpg?s=1024x768",
  },
  {
    code: "HAN-CXR",
    depart: "Hà Nội",
    arrival: "Nha Trang",
    provinceId: 43,
    country: "Việt Nam",
    thumb:
      "https://content.skyscnr.com/0f7d6f8d81e0be0d501901eb49758537/GettyImages-178392880.jpg",
  },
  {
    code: "SGN-HPH",
    depart: "Hồ Chí Minh",
    arrival: "Hải Phòng",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://moitruong.net.vn/wp-content/uploads/2019/10/chuan-bi-thong-xe-co-hinh-canh-chim-bien-o-hai-phong.jpg",
  },
];

export const listDestinationSecond = [
  {
    code: "HAN-PQC",
    depart: "Hà Nội",
    arrival: "Phú Quốc",
    provinceId: 2,
    country: "Việt Nam",
    thumb:
      "https://content.skyscnr.com/b08a6baae567f5c74243d9d01de13498/GettyImages-491610663.jpg",
  },
  {
    code: "SGN-THD",
    depart: "Hồ Chí Minh",
    arrival: "Thanh Hóa",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://moitruong.net.vn/wp-content/uploads/2019/10/chuan-bi-thong-xe-co-hinh-canh-chim-bien-o-hai-phong.jpg",
  },
  {
    code: "HAN-DLI",
    depart: "Hà Nội",
    arrival: "Đà Lạt",
    provinceId: 20,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/da-lat.png",
  },
  {
    code: "HAN-UIH",
    depart: "Hà Nội",
    arrival: "Quy Nhơn",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://content.skyscnr.com/6a70006a89670095c95f96624f3f06d3/eyeem-26693940-117794071.jpg",
  },
  {
    code: "SGN-HUI",
    depart: "Hồ Chí Minh",
    arrival: "Huế",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://content.skyscnr.com/382ad5d171134d7a9eb3f77d892e2356/GettyImages-521132331.jpg",
  },
  {
    code: "HAN-BMV",
    depart: "Hà Nội",
    arrival: "Ban Mê Thuột",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://www.yong.vn/Content/images/travels/cum-thac-gia-long-dray-nur-dray-sap.jpg",
  },
];
export const listFlightsFirst = [
  {
    code: "SGN-HAN",
    depart: "Hồ Chí Minh",
    arrival: "Hà Nội",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://content.skyscnr.com/02f6535440ff778b7292a2c90cf4e027/GettyImages-476258421.jpg",
  },
  {
    code: "HAN-SGN",
    depart: "Hà Nội",
    arrival: "Hồ Chí Minh",
    provinceId: 33,
    country: "Việt Nam",
    thumb:
      "https://content.skyscnr.com/0012f421f0682d22815185912d6e105c/GettyImages-481711830.jpg",
  },
  {
    code: "SGN-VII",
    depart: "Hồ Chí Minh",
    arrival: "Thành phố Vinh",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://pix10.agoda.net/hotelImages/5663774/-1/00f12730a33df7f9a31657bcdf749e93.jpg?s=1024x768",
  },
  {
    code: "HAN-DAD",
    depart: "Hà Nội",
    arrival: "Đà Nẵng",
    provinceId: 50,
    country: "Việt Nam",
    thumb:
      "https://content.skyscnr.com/0ccd022e5d074467344ac3d01d5d1df7/GettyImages-178793470.jpg",
  },
  {
    code: "SGN-HPH",
    depart: "Hồ Chí Minh",
    arrival: "Hải Phòng",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://moitruong.net.vn/wp-content/uploads/2019/10/chuan-bi-thong-xe-co-hinh-canh-chim-bien-o-hai-phong.jpg",
  },
  {
    code: "SGN-THD",
    depart: "Hồ Chí Minh",
    arrival: "Thanh Hóa",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://moitruong.net.vn/wp-content/uploads/2019/10/chuan-bi-thong-xe-co-hinh-canh-chim-bien-o-hai-phong.jpg",
  },
];
export const listFlightsSecond = [
  {
    code: "DAD-SGN",
    depart: "Đà Nẵng",
    arrival: "Hồ Chí Minh",
    provinceId: 33,
    country: "Việt Nam",
    thumb:
      "https://content.skyscnr.com/0012f421f0682d22815185912d6e105c/GettyImages-481711830.jpg",
  },
  {
    code: "HPH-SGN",
    depart: "Hải Phòng",
    arrival: "Hồ Chí Minh",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://moitruong.net.vn/wp-content/uploads/2019/10/chuan-bi-thong-xe-co-hinh-canh-chim-bien-o-hai-phong.jpg",
  },
  {
    code: "VII-SGN",
    depart: "Thành phố Vinh",
    arrival: "Hồ Chí Minh",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://pix10.agoda.net/hotelImages/5663774/-1/00f12730a33df7f9a31657bcdf749e93.jpg?s=1024x768",
  },
  {
    code: "HAN-CXR",
    depart: "Hà Nội",
    arrival: "Nha Trang",
    provinceId: 43,
    country: "Việt Nam",
    thumb:
      "https://content.skyscnr.com/0f7d6f8d81e0be0d501901eb49758537/GettyImages-178392880.jpg",
  },
  {
    code: "DAD-HAN",
    depart: "Đà Nẵng",
    arrival: "Hà Nội",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://content.skyscnr.com/02f6535440ff778b7292a2c90cf4e027/GettyImages-476258421.jpg",
  },
  {
    code: "SGN-HUI",
    depart: "Hồ Chí Minh",
    arrival: "Huế",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://content.skyscnr.com/382ad5d171134d7a9eb3f77d892e2356/GettyImages-521132331.jpg",
  },
];
