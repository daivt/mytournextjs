export const domesticInBound = [
  {
    code: "HAN-SGN",
    depart: "Hà Nội",
    arrival: "Hồ Chí Minh",
    provinceId: 33,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hochiminh.png",
  },
  {
    code: "SGN-HAN",
    depart: "Hồ Chí Minh",
    arrival: "Hà Nội",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hanoi.png",
  },
  {
    code: "HAN-DAD",
    depart: "Hà Nội",
    arrival: "Đà Nẵng",
    provinceId: 50,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/danang.png",
  },
  {
    code: "DAD-HAN",
    depart: "Đà Nẵng",
    arrival: "Hà Nội",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hanoi.png",
  },
  {
    code: "SGN-DAD",
    depart: "Hồ Chí Minh",
    arrival: "Đà Nẵng",
    provinceId: 50,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/danang.png",
  },
  {
    code: "DAD-SGN",
    depart: "Đà Nẵng",
    arrival: "Hồ Chí Minh",
    provinceId: 33,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hochiminh.png",
  },
  {
    code: "HAN-PQC",
    depart: "Hà Nội",
    arrival: "Phú Quốc",
    provinceId: 2,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/phu-quoc.png",
  },
  {
    code: "PQC-HAN",
    depart: "Phú Quốc",
    arrival: "Hà Nội",
    provinceId: 2,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hanoi.png",
  },
  {
    code: "HAN-CXR",
    depart: "Hà Nội",
    arrival: "Nha Trang",
    provinceId: 43,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/nha-trang.png",
  },
  {
    code: "CXR-HAN",
    depart: "Nha Trang",
    arrival: "Hà Nội",
    provinceId: 43,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hanoi.png",
  },
  {
    code: "HAN-DLI",
    depart: "Hà Nội",
    arrival: "Đà Lạt",
    provinceId: 20,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/da-lat.png",
  },
  {
    code: "DLI-HAN",
    depart: "Đà Lạt",
    arrival: "Hà Nội",
    provinceId: 20,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hanoi.png",
  },
  {
    code: "HAN-VCS",
    depart: "Hà Nội",
    arrival: "Côn Đảo",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/vung-tau.png",
  },
  {
    code: "VCS-HAN",
    depart: "Côn Đảo",
    arrival: "Hà Nội",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hanoi.png",
  },
  {
    code: "HAN-HUI",
    depart: "Hà Nội",
    arrival: "Huế",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hue.png",
  },
  {
    code: "HUI-HAN",
    depart: "Huế",
    arrival: "Hà Nội",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hanoi.png",
  },
  {
    code: "UIH-HAN",
    depart: "Quy Nhơn",
    arrival: "Hà Nội",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hanoi.png",
  },
  {
    code: "HAN-UIH",
    depart: "Hà Nội",
    arrival: "Quy Nhơn",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/binh-dinh.png",
  },
  {
    code: "HAN-VCA",
    depart: "Hà Nội",
    arrival: "Cần Thơ",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/can-tho.png",
  },
  {
    code: "VCA-HAN",
    depart: "Cần Thơ",
    arrival: "Hà Nội",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hanoi.png",
  },
  {
    code: "HAN-TBB",
    depart: "Hà Nội",
    arrival: "Tuy Hoà",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/phu-yen.png",
  },
  {
    code: "TBB-HAN",
    depart: "Tuy Hoà",
    arrival: "Hà Nội",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hanoi.png",
  },
  {
    code: "HAN-VII",
    depart: "Hà Nội",
    arrival: "Thành phố Vinh",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/nghe-an.png",
  },
  {
    code: "VII-HAN",
    depart: "Thành phố Vinh",
    arrival: "Hà Nội",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hanoi.png",
  },
  {
    code: "HAN-PXU",
    depart: "Hà Nội",
    arrival: "Pleiku",
    provinceId: 11,
    country: "Việt Nam",
    thumb: "https://cf.bstatic.com/images/hotel/max1024x768/282/282657139.jpg",
  },
  {
    code: "PXU-HAN",
    depart: "Pleiku",
    arrival: "Hà Nội",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hanoi.png",
  },
  {
    code: "HAN-BMV",
    depart: "Hà Nội",
    arrival: "Ban Mê Thuột",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://luhanhxanhvietnam.com.vn/wp-content/uploads/2019/03/3-1-2.jpg",
  },
  {
    code: "BMV-HAN",
    depart: "Ban Mê Thuột",
    arrival: "Hà Nội",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hanoi.png",
  },
  {
    code: "DAD-PQC",
    depart: "Đà Nẵng",
    arrival: "Phú Quốc",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/phu-quoc.png",
  },
  {
    code: "PQC-DAD",
    depart: "Phú Quốc",
    arrival: "Đà Nẵng",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/danang.png",
  },
  {
    code: "SGN-PQC",
    depart: "Hồ Chí Minh",
    arrival: "Phú Quốc",
    provinceId: 2,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/phu-quoc.png",
  },
  {
    code: "PQC-SGN",
    depart: "Phú Quốc",
    arrival: "Hồ Chí Minh",
    provinceId: 2,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hochiminh.png",
  },
  {
    code: "HPH-PQC",
    depart: "Hải Phòng",
    arrival: "Phú Quốc",
    provinceId: 2,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/phu-quoc.png",
  },
  {
    code: "PQC-HPH",
    depart: "Phú Quốc",
    arrival: "Hải Phòng",
    provinceId: 2,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hai-phong.png",
  },
  {
    code: "DAD-HPH",
    depart: "Đà Nẵng",
    arrival: "Hải Phòng",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hai-phong.png",
  },
  {
    code: "HPH-DAD",
    depart: "Hải Phòng",
    arrival: "Đà Nẵng",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/danang.png",
  },
  {
    code: "DAD-DLI",
    depart: "Đà Nẵng",
    arrival: "Đà Lạt",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/da-lat.png",
  },
  {
    code: "DLI-DAD",
    depart: "Đà Lạt",
    arrival: "Đà Nẵng",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/danang.png",
  },
  {
    code: "DAD-CXR",
    depart: "Đà Nẵng",
    arrival: "Nha Trang",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/nha-trang.png",
  },
  {
    code: "DAD-VCA",
    depart: "Đà Nẵng",
    arrival: "Cần Thơ",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/can-tho.png",
  },
  {
    code: "DAD-TBB",
    depart: "Đà Nẵng",
    arrival: "Tuy Hoà",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/phu-yen.png",
  },
  {
    code: "SGN-HPH",
    depart: "Hồ Chí Minh",
    arrival: "Hải Phòng",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hai-phong.png",
  },
  {
    code: "SGN-THD",
    depart: "Hồ Chí Minh",
    arrival: "Thanh Hóa",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/thanh-hoa.png",
  },
  {
    code: "THD-SGN",
    depart: "Thanh Hóa",
    arrival: "Hồ Chí Minh",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hochiminh.png",
  },
  {
    code: "HPH-SGN",
    depart: "Hải Phòng",
    arrival: "Hồ Chí Minh",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hochiminh.png",
  },
  {
    code: "SGN-VII",
    depart: "Hồ Chí Minh",
    arrival: "Thành phố Vinh",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/nghe-an.png",
  },
  {
    code: "VII-SGN",
    depart: "Thành phố Vinh",
    arrival: "Hồ Chí Minh",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hochiminh.png",
  },
  {
    code: "SGN-DLI",
    depart: "Hồ Chí Minh",
    arrival: "Đà Lạt",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/da-lat.png",
  },
  {
    code: "DLI-SGN",
    depart: "Đà Lạt",
    arrival: "Hồ Chí Minh",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hochiminh.png",
  },
  {
    code: "SGN-VDO",
    depart: "Hồ Chí Minh",
    arrival: "Quảng Ninh",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/ha-long.png",
  },
  {
    code: "SGN-HUI",
    depart: "Hồ Chí Minh",
    arrival: "Huế",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/hue.png",
  },
  {
    code: "SGN-PXU",
    depart: "Hồ Chí Minh",
    arrival: "Pleiku",
    provinceId: 11,
    country: "Việt Nam",
    thumb: "https://cf.bstatic.com/images/hotel/max1024x768/282/282657139.jpg",
  },
  {
    code: "SGN-BMV",
    depart: "Hồ Chí Minh",
    arrival: "Ban Mê Thuột",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://luhanhxanhvietnam.com.vn/wp-content/uploads/2019/03/3-1-2.jpg",
  },
  {
    code: "SGN-TBB",
    depart: "Hồ Chí Minh",
    arrival: "Tuy Hoà",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/phu-yen.png",
  },
  {
    code: "SGN-UIH",
    depart: "Hồ Chí Minh",
    arrival: "Quy Nhơn",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/binh-dinh.png",
  },
  {
    code: "SGN-VDH",
    depart: "Hồ Chí Minh",
    arrival: "Quảng Bình",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://pix10.agoda.net/hotelImages/5663774/-1/00f12730a33df7f9a31657bcdf749e93.jpg?s=1024x768",
  },
  {
    code: "DAD-VII",
    depart: "Đà Nẵng",
    arrival: "Thành phố Vinh",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/nghe-an.png",
  },
  {
    code: "VII-DAD",
    depart: "Thành phố Vinh",
    arrival: "Đà Nẵng",
    provinceId: 11,
    country: "Việt Nam",
    thumb:
      "https://storage.googleapis.com/tripi-assets/mytour/images/locations/danang.png",
  },
];
