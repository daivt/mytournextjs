import PropTypes from "prop-types";
import { Slider, Tooltip } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";

const SliderCustomer = withStyles({
  root: {
    color: "#E2E8F0",
    height: 4,
    width: "100%",
    borderRadius: 100,
  },
  thumb: {
    height: 28,
    width: 28,
    backgroundColor: "#FFFFFF",
    marginTop: -12,
    marginLeft: -12,
    border: "none",
    boxShadow:
      " 0px 0.5px 4px rgba(0, 0, 0, 0.12), 0px 6px 13px rgba(0, 0, 0, 0.12)",
    borderRadius: 100,
    "&:focus, &:hover, &$active": {
      boxShadow:
        " 0px 0.5px 4px rgba(0, 0, 0, 0.12), 0px 6px 13px rgba(0, 0, 0, 0.12)",
    },
  },
  active: {},
  valueLabel: {
    left: "calc(-50% + 4px)",
  },
  track: {
    height: 6,
    borderRadius: 3,
    color: "#00B6F3",
  },
  rail: {
    height: 6,
    borderRadius: 3,
    color: "#eeeeee",
    boxShadow: "inset 0 0 2px 0 rgba(0, 0, 0, 0.15)",
  },
})(Slider);

const RangerSliderPrice = ({
  min = 0,
  max = 5000000,
  step = 100000,
  valueRange = [0, 5000000],
  handleChangeRange = () => {},
  ...other
}) => {
  return (
    <SliderCustomer
      min={min}
      max={max}
      step={step}
      value={valueRange}
      onChange={handleChangeRange}
      {...other}
    />
  );
};

RangerSliderPrice.propTypes = {
  min: PropTypes.number,
  max: PropTypes.number,
  step: PropTypes.number,
  ariaLabelledby: PropTypes.string,
  valueRange: PropTypes.array,
  handleChangeRange: PropTypes.func,
};

export default RangerSliderPrice;
