import { memo } from "react";
import { FormControl, FormHelperText, TextField } from "@material-ui/core";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import { MIN_WIDTH_FORM } from "./utils";

const useStyles = makeStyles((theme) => ({
  styleInputBase: {
    "& .MuiInputLabel-formControl": {
      marginBottom: 0,
      color: "#718096",
      fontSize: 16,
    },
    "& .MuiFormLabel-root.Mui-focused": {
      color: "#4A5568",
      fontSize: 12,
    },
  },
  textRoot: {
    marginRight: 0,
    marginTop: 5,
    "& .MuiInput-root": {
      marginTop: "0 !important",
      "& .MuiInputBase-input": {
        padding: "11px 0 9px",
      },
    },
  },
}));

const FormControlTextFieldBase = (props) => {
  const {
    id,
    label,
    formControlStyle,
    styleLabel,
    inputStyle,
    errorMessage,
    optional,
    focused,
    value,
    fullWidth,
    helpText,
    helperTextStyle,
    isView,
    hideHelperText,
    ...rest
  } = props;
  const classes = useStyles();
  return (
    <FormControl
      focused={focused}
      style={{ minWidth: MIN_WIDTH_FORM, ...formControlStyle }}
      error={focused ? false : !!errorMessage}
      fullWidth
      className={classes.styleInputBase}
    >
      <TextField
        id={id}
        value={value || ""}
        label={label || ""}
        {...rest}
        error={focused ? false : !!errorMessage}
        classes={{ root: classes.textRoot }}
        style={{ ...inputStyle }}
      />
      {!hideHelperText && (
        <FormHelperText
          id={id}
          style={{
            minHeight: 20,
            paddingTop: 8,
            color: !errorMessage ? "initial" : undefined,
            ...helperTextStyle,
          }}
        >
          {errorMessage || helpText}
        </FormHelperText>
      )}
    </FormControl>
  );
};

FormControlTextFieldBase.propTypes = {
  id: PropTypes.string,
  label: PropTypes.node,
  styleLabel: PropTypes.any,
  formControlStyle: PropTypes.any,
  inputStyle: PropTypes.any,
  helperTextStyle: PropTypes.any,
  errorMessage: PropTypes.string,
  optional: PropTypes.bool,
  focused: PropTypes.bool,
  helpText: PropTypes.string,
};

export default memo(FormControlTextFieldBase);
