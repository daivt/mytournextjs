import { memo } from "react";
import { useField, useFormikContext } from "formik";
import { TextField, makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  inputItem: {
    margin: 0,
    marginBottom: 20,
    "& > .MuiInput-root": { marginTop: 0 },
    "& > label": {
      color: theme.palette.gray.grayDark8,
    },
  },
}));
const FieldTextAutocomplete = (props) => {
  const classes = useStyles();
  const [field, meta, helpers] = useField(props);
  const { submitCount } = useFormikContext();
  return (
    <TextField
      fullWidth
      {...field}
      {...props}
      error={submitCount > 0 && meta.error ? meta.error : ""}
      className={classes.inputItem}
      style={{
        margin: 0,
        marginBottom: 20,
      }}
      className={classes.inputItem}
    />
  );
};

export default memo(FieldTextAutocomplete);
