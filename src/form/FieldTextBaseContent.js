import { memo } from "react";
import { useField, useFormikContext } from "formik";
import FormControlTextFieldBase from "@src/form/FormControlTextFieldBase";

const FieldTextBaseContent = (props) => {
  const [field, meta] = useField(props);
  const { submitCount } = useFormikContext();
  return (
    <FormControlTextFieldBase
      {...field}
      {...props}
      errorMessage={
        (meta.touched || submitCount > 0) && meta.error ? meta.error : undefined
      }
    />
  );
};

export default memo(FieldTextBaseContent);
