import MuiAccordion from "@material-ui/core/Accordion";
import { withStyles } from "@material-ui/styles";

const AccordionCustom = withStyles((theme) => ({
  root: {
    border: "none",
    boxShadow: "none",
    marginTop: "-16px",
    "&$expanded": {
      margin: 0,
    },
  },
  expanded: {
    margin: 0,
  },
}))(MuiAccordion);

export default AccordionCustom;
