import MuiAccordionSummary from "@material-ui/core/AccordionSummary";
import { withStyles } from "@material-ui/styles";

const AccordionSummaryCustom = withStyles({
  root: {
    padding: 0,
    marginTop: "-16px",
    "&$expanded": {
      minHeight: 48,
    },
  },
  content: {
    margin: 0,
    "&$expanded": {
      margin: 0,
    },
  },
  expanded: {},
})(MuiAccordionSummary);

export default AccordionSummaryCustom;
