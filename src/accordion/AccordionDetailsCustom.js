import MuiAccordionDetails from "@material-ui/core/AccordionDetails";
import { withStyles } from "@material-ui/styles";

const AccordionDetailsCustom = withStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    padding: 0,
  },
}))(MuiAccordionDetails);

export default AccordionDetailsCustom;
