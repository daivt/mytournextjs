import clsx from "clsx";
import { memo } from "react";
import PropTypes from "prop-types";
import makeStyles from "@material-ui/styles/makeStyles";
import { Box, Button, CircularProgress } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  customerButton: ({
    typeButton,
    backgroundColor,
    borderRadius,
    color,
    height,
    width,
    fontSize,
    fontWeight,
    borderColor,
    padding,
  }) => ({
    margin: 0,
    padding,
    backgroundColor:
      typeButton === "text"
        ? "initial"
        : backgroundColor || theme.palette.primary.main,
    color: color || theme.palette.white.white1,
    height,
    width,
    fontSize,
    fontWeight,
    textTransform: "none",
    borderRadius,
    border: typeButton === "outlined" ? `solid 1px ${borderColor}` : "none",
    "&:hover": {
      backgroundColor:
        typeButton === "text"
          ? "initial"
          : backgroundColor || theme.palette.primary.main,
    },
    minHeight: 20,
    "&:disabled": {
      backgroundColor: theme.palette.gray.grayLight25,
      color: theme.palette.white.main,
      // opacity: 0.5,
    },
  }),
}));

const ButtonComponent = ({
  children = null,
  className = "",
  color = "",
  backgroundColor = "",
  borderRadius = 4,
  height = 40,
  fontSize = 14,
  fontWeight = "normal",
  width = "100%",
  handleClick = () => {},
  typeButton = "contained",
  borderColor = "#cccccc",
  loading = false,
  disableRipple = false,
  loadingColor = "#fff",
  isShowCircularProgress = true,
  padding = "6px 8px",
  disabled = false,
  ...other
}) => {
  const propsStyle = {
    backgroundColor,
    borderRadius,
    color,
    height,
    width,
    fontSize,
    fontWeight,
    typeButton,
    borderColor,
    padding,
  };
  const classes = useStyles(propsStyle);

  return (
    <Button
      className={clsx(classes.customerButton, className)}
      {...other}
      onClick={!loading ? handleClick : undefined}
      disabled={loading || disabled}
      disableRipple={loading ? true : disableRipple}
    >
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        position="relative"
      >
        <Box display="flex" alignItems="center" opacity={loading ? 0.5 : 1}>
          {children || "OK"}
        </Box>
        {loading && isShowCircularProgress && (
          <CircularProgress
            size={24}
            color="secondary"
            style={{
              position: "absolute",
              top: "50%",
              left: "50%",
              marginTop: "-12px",
              marginLeft: "-12px",
            }}
          />
        )}
      </Box>
    </Button>
  );
};

ButtonComponent.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  handleClick: PropTypes.func,
  loading: PropTypes.bool,
  disableRipple: PropTypes.bool,
  isShowCircularProgress: PropTypes.bool,
};

export default memo(ButtonComponent);
