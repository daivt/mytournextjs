import clsx from "clsx";
import { memo } from "react";
import PropTypes from "prop-types";
import LazyLoad from "react-lazyload";
import { useRef, useState, useEffect } from "react";
import makeStyles from "@material-ui/styles/makeStyles";
import { IconLoadingHotel } from "@public/icons";
const useStyles = makeStyles((theme) => ({
  imageWrapper: {
    width: "100%",
    height: "100%",
    position: "relative",
    "& .lazyload-wrapper": {
      height: "100%",
    },
  },
  placeholder: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: "100%",
    height: "100%",
    backgroundColor: theme.palette.gray.grayLight22,
    opacity: 1,
    transition: "opacity 500ms",
    zIndex: 1,
  },
  lazyLoadImage: {
    height: "100%",
  },
  imgContainer: {
    width: "100%",
    height: "100%",
    position: "absolute",
    left: 0,
    top: 0,
    objectFit: "cover",
  },
  removePlaceholder: {
    opacity: 0,
    transition: "opacity 500ms",
  },
}));
const defaultBG =
  "https://test.mytourcdn.com/120x90,q90/themes/images/pc-listing-default.png";
const ImageComponent = (props) => {
  const { className, srcImage, borderRadiusProp = 0, ...other } = props;
  const refPlaceholder = useRef();
  const [imgSrc, setImgSrc] = useState(srcImage || defaultBG);
  const [loadImgError, setLoadImgError] = useState(false);
  const classes = useStyles();
  const removePlaceholder = () => {
    refPlaceholder.current.classList.add(classes.removePlaceholder);
  };
  const showErrorImage = () => {
    setLoadImgError(true);
  };
  useEffect(() => {
    setImgSrc(srcImage);
  }, [srcImage]);
  return (
    <div className={clsx(classes.imageWrapper, className)}>
      <div
        className={classes.placeholder}
        style={{ borderRadius: borderRadiusProp }}
        ref={refPlaceholder}
      >
        {loadImgError && <IconLoadingHotel />}
      </div>
      <LazyLoad>
        <img
          src={imgSrc}
          className={clsx(classes.imgContainer, className)}
          onLoad={removePlaceholder}
          onError={showErrorImage}
          {...other}
          alt=""
        />
      </LazyLoad>
    </div>
  );
};

ImageComponent.propTypes = {
  srcImage: PropTypes.string.isRequired,
};

export default memo(ImageComponent);
