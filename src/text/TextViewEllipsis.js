import PropTypes from "prop-types";
import { Box } from "@material-ui/core";
import makeStyles from "@material-ui/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
  boxText: ({ backgroundColor }) => ({
    overflow: "hidden",
    display: "block",
    position: "relative",
    paddingRight: 12,
    "&:before": {
      paddingLeft: 2,
      position: "absolute",
      content: "'...'",
      bottom: 0,
      right: 0,
      marginLeft: -6,
    },
    "&:after": {
      position: "absolute",
      content: "''",
      right: 0,
      width: "1em",
      height: "1em",
      marginTop: ".2em",
      background: backgroundColor,
    },
  }),
}));
const TextView = ({
  text = "",
  fontSize = 14,
  fontWeight = "inherit",
  lineHeight = "inherit",
  textAlign = "inherit",
  color = "inherit",
  margin = 0,
  maxHeight = 45,
  height = "auto",
  backgroundColor = "#fff",
}) => {
  const propStyle = {
    backgroundColor,
  };
  const classes = useStyles(propStyle);
  return (
    <Box
      className={classes.boxText}
      fontSize={fontSize}
      fontWeight={fontWeight}
      lineHeight={lineHeight}
      textAlign={textAlign}
      m={margin}
      maxHeight={maxHeight}
      height={height}
      color={color}
    >
      <Box component="span">{text}</Box>
    </Box>
  );
};

TextView.propTypes = {
  text: PropTypes.string.isRequired,
  fontSize: PropTypes.number,
  maxHeight: PropTypes.number,
  fontWeight: PropTypes.any,
  lineHeight: PropTypes.any,
  textAlign: PropTypes.any,
  margin: PropTypes.any,
  color: PropTypes.any,
};

export default TextView;
