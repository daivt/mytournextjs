import PaymentStep from "@components/desktop/flight/payment/PaymentStep";
import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconPhoneContact } from "@public/icons";
import Image from "@src/image/Image";
import Link from "@src/link/Link";
import { hotLine, listString, routeStatic } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  headerContainer: {
    height: 60,
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    margin: "0 auto",
  },
  textItem: { lineHeight: "17px", whiteSpace: "nowrap" },
  infoItem: {
    display: "flex",
    flexDirection: "column",
    padding: "0 12px",
  },
  headerRight: { display: "flex" },
  wrapperItem: { display: "flex", alignItems: "center" },
  logoMytour: {
    width: 120,
    height: 32,
  },
}));
const menu = [
  {
    name: listString.IDS_MT_TEXT_HOTLINE,
    phone: hotLine.flight,
    icon: <IconPhoneContact />,
  },
];
const HeaderBookingFlight = ({ currentStep }) => {
  const classes = useStyles();
  return (
    <Box
      style={{
        width: "100%",
        height: 60,
        background: "white",
        position: "sticky",
        top: 0,
        zIndex: 1,
      }}
    >
      <Box className={classes.headerContainer}>
        <Box display="flex" alignItems="center">
          <Link
            href={{ pathname: routeStatic.HOME.href }}
            style={{ cursor: "pointer" }}
          >
            <img
              src="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_logo_mytour.svg"
              className={classes.logoMytour}
              alt="logo_mytour"
            />
          </Link>
        </Box>
        <PaymentStep currentStep={currentStep} />
        <Box className={classes.headerRight}>
          <Box className={classes.wrapperItem} style={{ color: "#1A202C" }}>
            {menu.map((el, i) => (
              <Box key={i} style={{ display: "flex", alignItems: "center" }}>
                {el.icon}
                <Box className={classes.infoItem}>
                  <Typography variant="body2" className={classes.textItem}>
                    {el.name}
                  </Typography>
                  <Typography
                    variant="body2"
                    className={classes.textItem}
                    style={{ fontWeight: 600 }}
                  >
                    {el.phone}
                  </Typography>
                </Box>
              </Box>
            ))}
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default HeaderBookingFlight;
