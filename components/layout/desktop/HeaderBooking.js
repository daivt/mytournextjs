import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconPhoneContact } from "@public/icons";
import Image from "@src/image/Image";
import Link from "@src/link/Link";
import { routeStatic } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  headerContainer: {
    height: 60,
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    margin: "0 auto",
  },
  headerLeft: { display: "flex", alignItems: "center" },
  headerRight: { display: "flex" },

  wrapperItem: { display: "flex", alignItems: "center" },
  filterContainer: { textAlign: "center" },
  divider: {
    height: 3,
    width: 40,
    margin: "auto",
    marginTop: 12,
  },
  textItem: { fontSize: 14, lineHeight: "17px", whiteSpace: "nowrap" },
  infoItem: {
    display: "flex",
    flexDirection: "column",
    paddingLeft: 12,
  },
  logoMytour: {
    width: 120,
    height: 32,
    marginRight: 56,
  },
}));
const menu = [
  { name: "Hotline", phone: "1900 2083", icon: <IconPhoneContact /> },
];

const HeaderBooking = () => {
  const classes = useStyles();

  return (
    <Box
      style={{
        width: "100%",
        height: 60,
        background: "white",
        position: "sticky",
        top: 0,
        zIndex: 1,
      }}
    >
      <Box className={classes.headerContainer}>
        <Box className={classes.headerLeft} style={{ color: "#1A202C" }}>
          <Link
            href={{ pathname: routeStatic.HOME.href }}
            style={{ cursor: "pointer" }}
          >
            <img
              src="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_logo_mytour.svg"
              className={classes.logoMytour}
              alt="logo_mytour"
            />
          </Link>
        </Box>
        <Box className={classes.headerRight}>
          <Box className={classes.wrapperItem} style={{ color: "#1A202C" }}>
            {menu.map((el, i) => (
              <Box key={i} style={{ display: "flex", alignItems: "center" }}>
                {el.icon}
                <Box className={classes.infoItem}>
                  <Typography variant="body2" className={classes.textItem}>
                    {el.name}
                  </Typography>
                  <Typography
                    variant="body2"
                    className={classes.textItem}
                    style={{ fontWeight: 600 }}
                  >
                    {el.phone}
                  </Typography>
                </Box>
              </Box>
            ))}
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default HeaderBooking;
