import { makeStyles } from "@material-ui/styles";
import { Box, Typography, Divider } from "@material-ui/core";

import { useSystem } from "@contextProvider/ContextProvider";

import {
  IconClose,
  IconLoved,
  IconHomeFill,
  IconHotelYellow,
  IconFlightTicket,
  IconTourEvent,
  IconTravelGuide,
  IconDescMytour,
  IconHR,
  IconHeadPhoneSupport,
  IconHandShake,
  IconMobileDownload,
} from "@public/icons";
import { isEmpty } from "@utils/helpers";
import { routeStatic } from "@utils/constants";

import Link from "@src/link/Link";
import AuthWrapper from "@components/layout/desktop/components/AuthWrapper";

const useStyles = makeStyles((theme) => ({
  container: { height: "100%", width: 304, padding: "8px 16px" },
  content: { paddingTop: 10 },
  itemContent: {
    display: "flex",
    alignItems: "center",
    padding: "12px 8px",
    margin: "4px -8px",
    background: "white",
    borderRadius: 8,
    cursor: "pointer",
    "&:hover": {
      background: "#EDF2F7",
    },
  },
  textItem: {
    fontSize: 14,
    lineHeight: "17px",
    color: "#1A202C",
    marginLeft: 16,
  },
}));

const DrawerHomeContent = ({ onClose }) => {
  const classes = useStyles();
  const { systemReducer } = useSystem();
  const informationUser = systemReducer.informationUser || {};
  const isLogin = !isEmpty(informationUser);

  const menu = [
    {
      name: "Trang chủ",
      icon: <IconHomeFill />,
      url: routeStatic.HOME.href,
    },
    // { name: "Săn vé", icon: <IconHuntTicket /> },
    {
      name: "Yêu thích",
      icon: <IconLoved />,
      url: routeStatic.ACCOUNT_FAVORITES_HOTEL.href,
      isLast: true,
      code: "favorites",
    },
    // {
    //   name: "Nạp tiền điện thoại",
    //   icon: <IconCardPhone />,
    //   isLast: true,
    // },
    {
      name: "Khách sạn",
      icon: <IconHotelYellow />,
      url: routeStatic.HOTEL.href,
    },
    {
      name: "Vé máy bay",
      icon: <IconFlightTicket />,
      url: routeStatic.FLIGHT.href,
    },
    {
      name: "Tour & Sự kiện",
      icon: <IconTourEvent />,
      url: "https://mytourevent.vn/",
    },
    {
      name: "Cẩm nang du lịch",
      icon: <IconTravelGuide />,
      url: routeStatic.BLOG.href,
      isLast: true,
      code: "blog",
    },
    {
      name: "Tuyển dụng",
      icon: <IconHR />,
      url: "https://career.mytour.vn/",
    },
    {
      name: "Hỗ trợ",
      icon: <IconHeadPhoneSupport />,
      url: "/help/30-lien-he.html",
      isLast: true,
    },
    // { name: "Ưu đãi", icon: <IconPromo />, isLast: true },
    {
      name: "Hợp tác với chúng tôi",
      icon: <IconHandShake />,
      url: routeStatic.PARTNERSHIP.href,
    },
    // {
    //   name: "Giới thiệu về Mytour",
    //   icon: <IconDescMytour />,
    //   url: "/app/mytour-info",
    // },

    // { name: "Giới thiệu bạn bè", icon: <IconShareToFriend /> },
    {
      name: "Tải ứng dụng Mytour",
      icon: <IconMobileDownload />,
      url: routeStatic.DOWNLOAD_APP.href,
    },
  ];
  return (
    <Box>
      <Box className={classes.container}>
        <Box className={classes.content}>
          <IconClose
            style={{ cursor: "pointer", marginBottom: 20 }}
            onClick={onClose}
          />
          {menu.map((item, i) => {
            if (item.code === "favorites" && !isLogin) {
              return (
                <AuthWrapper typeModal="MODAL_LOGIN">
                  <Box className={classes.itemContent}>
                    {item.icon}
                    <Typography
                      variant="body2"
                      className={classes.textItem}
                      key={i}
                    >
                      {item.name}
                    </Typography>
                  </Box>
                  {item.isLast && <Divider />}
                </AuthWrapper>
              );
            }
            if (item.code === "blog") {
              return (
                <a
                  key={i}
                  href={item.url}
                  style={{
                    textDecoration: "none",
                  }}
                >
                  <Box className={classes.itemContent}>
                    {item.icon}
                    <Typography
                      variant="body2"
                      className={classes.textItem}
                      key={i}
                    >
                      {item.name}
                    </Typography>
                  </Box>
                  {item.isLast && <Divider />}
                </a>
              );
            }

            return (
              <Link
                key={i}
                href={{ pathname: item.url }}
                style={{
                  textDecoration: "none",
                }}
              >
                <Box className={classes.itemContent}>
                  {item.icon}
                  <Typography
                    variant="body2"
                    className={classes.textItem}
                    key={i}
                  >
                    {item.name}
                  </Typography>
                </Box>
                {item.isLast && <Divider />}
              </Link>
            );
          })}
        </Box>
      </Box>
    </Box>
  );
};

export default DrawerHomeContent;
