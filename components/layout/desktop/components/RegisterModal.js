import { useState, useEffect } from "react";
import {
  Box,
  IconButton,
  makeStyles,
  Modal,
  Fade,
  Backdrop,
} from "@material-ui/core";
import { useRouter } from "next/router";

import { listString } from "@utils/constants";
import { IconClose, IconBack } from "@public/icons";
import { useSystem } from "@contextProvider/ContextProvider";

import ButtonLoginViaGGFB from "@components/common/login/ButtonLoginViaGGFB";
import RegisterStep1 from "@components/layout/desktop/components/register/RegisterStep1";
import RegisterStep2 from "@components/layout/desktop/components/register/RegisterStep2";
import RegisterStep3 from "@components/layout/desktop/components/register/RegisterStep3";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    position: "relative",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: theme.palette.white.main,
    boxShadow: theme.shadows[5],
    width: 408,
    borderRadius: 8,
    padding: "32px 48px",
    outline: "none",
  },
  boxIconClose: {
    position: "absolute",
    right: 4,
    top: 4,
  },
  boxIconBack: {
    position: "absolute",
    left: 4,
    top: 4,
  },
}));

const RegisterModal = ({
  open = false,
  handleClose = () => {},
  handleSelectItem = () => {},
  phoneNumberInit = "",
}) => {
  const classes = useStyles();
  const router = useRouter();

  const { systemReducer } = useSystem();
  const [step, setStep] = useState(1);
  useEffect(() => {
    setStep(1);
  }, []);
  const handleChangeStep = (valueStep = 1) => {
    setStep(valueStep);
    if (valueStep === 4) {
      handleCloseModal();
      router.reload();
    }
  };
  const handleCloseModal = () => {
    setStep(1);
    handleClose();
  };
  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={open}
      onClose={handleCloseModal}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{ timeout: 500 }}
      disableBackdropClick
    >
      <Fade in={open}>
        <Box className={classes.paper}>
          <Box className={classes.boxIconBack}>
            {step !== 1 && (
              <IconButton onClick={() => handleChangeStep(step - 1)}>
                <IconBack />
              </IconButton>
            )}
          </Box>
          <Box className={classes.boxIconClose}>
            <IconButton onClick={handleCloseModal}>
              <IconClose />
            </IconButton>
          </Box>
          <Box
            component="span"
            fontWeight={600}
            fontSize={22}
            lineHeight="22px"
          >
            {step !== 3 ? listString.IDS_MT_TEXT_SIGNUP : "Nhập mật khẩu"}
          </Box>
          {step === 1 && (
            <>
              <ButtonLoginViaGGFB
                isLogin={false}
                heightBtn={44}
                fontSizeText={14}
                isPc
              />
              <Box
                lineHeight="14px"
                fontSize={12}
                display="flex"
                justifyContent="center"
                pt={3}
                pb={12 / 8}
              >
                {listString.IDS_MT_TEXT_REGISTER_PHONE}
              </Box>
            </>
          )}

          {step === 1 ? (
            <RegisterStep1
              handleChangeStep={handleChangeStep}
              handleSelectItem={handleSelectItem}
              phoneNumberInit={phoneNumberInit}
            />
          ) : step === 2 ? (
            <RegisterStep2 handleChangeStep={handleChangeStep} />
          ) : (
            <RegisterStep3 handleChangeStep={handleChangeStep} />
          )}
        </Box>
      </Fade>
    </Modal>
  );
};

export default RegisterModal;
