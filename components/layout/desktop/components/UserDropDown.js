import clsx from "clsx";
import cookie from "js-cookie";
import { useState } from "react";
import { useRouter } from "next/router";
import { Box, Avatar } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import OutsideClickHandler from "react-outside-click-handler";

import { LogoutAccount } from "@api/user";
import { isEmpty } from "@utils/helpers";
import { IconDropDown, IconUser } from "@public/icons";
import {
  listString,
  TOKEN,
  LAST_BOOKING_HOTEL,
  routeStatic,
} from "@utils/constants";

import * as actionTypes from "@contextProvider/system/ActionTypes";
import { useSystem, useDispatchSystem } from "@contextProvider/ContextProvider";

import LoginModal from "@components/layout/desktop/components/LoginModal";
import RegisterModal from "@components/layout/desktop/components/RegisterModal";

const useStyles = makeStyles((theme) => ({
  wrapperItem: {
    display: "flex",
    alignItems: "center",
    marginRight: 18,
    cursor: "pointer",
    position: "relative",
  },
  dropdownContainer: {
    position: "absolute",
    top: 32,
    right: -24,
    background: "white",
    border: "1px solid #EDF2F7",
    boxSizing: "border-box",
    boxShadow: "0px 8px 8px rgba(0, 0, 0, 0.1)",
    borderRadius: 8,
    padding: "10px 8px",
    width: 220,
    zIndex: 1,
  },
  dropdownContent: { width: "100%", position: "relative" },
  arrowBlock: {
    width: 13,
    height: 13,
    position: "absolute",
    transform: "rotate(45deg)",
    top: -15,
    right: 15,
    background: "white",
  },
  dropdownItem: {
    display: "flex",
    alignItems: "center",
    padding: "14px 0",
    paddingLeft: 8,
    borderRadius: 8,
    "&:hover": {
      backgroundColor: theme.palette.gray.grayLight22,
    },
  },
  addressText: { fontSize: 14, lineHeight: "17px", color: "#1A202C" },
  userAccount: {
    display: "flex",
    alignItems: "center",
    backgroundColor: theme.palette.white.main,
    borderRadius: 100,
    padding: 2,
  },
  avatarUser: {
    width: 20,
    height: 20,
  },
  avatarDefault: {
    width: 20,
    height: 20,
    color: theme.palette.white.main,
    backgroundColor: theme.palette.blue.blueLight8,
    fontSize: 12,
    fontWeight: 600,
  },
}));

const listTypeModal = {
  MODAL_LOGIN: "MODAL_LOGIN",
  MODAL_REGISTER: "MODAL_REGISTER",
  LOGOUT: "LOGOUT",
  ACCOUNT_PROFILE: "ACCOUNT_PROFILE",
  FAVORITE_HOTEL: "FAVORITE_HOTEL",
  HOTEL: "HOTEL",
  FLIGHT: "FLIGHT",
};
const menuNoneAccount = [
  {
    name: listString.IDS_MT_TEXT_LOGIN,
    id: 0,
    code: listTypeModal.MODAL_LOGIN,
  },
  {
    name: listString.IDS_MT_TEXT_SIGNUP,
    id: 1,
    code: listTypeModal.MODAL_REGISTER,
  },
];
const menuAccount = [
  {
    name: listString.IDS_MT_TEXT_ACCOUNT,
    id: 0,
    code: listTypeModal.ACCOUNT_PROFILE,
  },
  {
    name: listString.IDS_MT_TEXT_FAVORITE,
    id: 1,
    code: listTypeModal.FAVORITE_HOTEL,
  },
  {
    name: listString.IDS_MT_TEXT_ORDER_HOTEL,
    id: 2,
    code: listTypeModal.HOTEL,
  },
  {
    name: listString.IDS_MT_TEXT_FLIGHT,
    id: 3,
    code: listTypeModal.FLIGHT,
  },
  {
    name: listString.IDS_MT_TEXT_LOGOUT,
    id: 4,
    code: listTypeModal.LOGOUT,
  },
];
const UserDropDown = ({ type }) => {
  const router = useRouter();
  const classes = useStyles();
  const [visible, setVisible] = useState(false);
  const [visibleModal, setVisibleModal] = useState("");

  const { systemReducer } = useSystem();
  const dispatch = useDispatchSystem();

  const informationUser = systemReducer.informationUser || {};
  const nameUser = informationUser.name || "";
  const nameSplit = nameUser.split(" ") || [];
  const lastNameUSer = nameSplit.pop() || "";
  const isLogin = !isEmpty(informationUser);

  const getColor = () => {
    return type === "listing" || type === "detail" || type === "account"
      ? "#1A202C"
      : "white";
  };

  const handleSelectItem = async (code = "") => {
    if (code === listTypeModal.MODAL_LOGIN) {
      setVisibleModal(listTypeModal.MODAL_LOGIN);
    } else if (code === listTypeModal.MODAL_REGISTER) {
      setVisibleModal(listTypeModal.MODAL_REGISTER);
    } else if (code === listTypeModal.ACCOUNT_PROFILE) {
      router.push({
        pathname: routeStatic.ACCOUNT.href,
      });
    } else if (code === listTypeModal.FAVORITE_HOTEL) {
      router.push({
        pathname: routeStatic.ACCOUNT_FAVORITES_HOTEL.href,
      });
    } else if (code === listTypeModal.HOTEL) {
      router.push({
        pathname: routeStatic.ACCOUNT_ORDER_BOOKING.href,
      });
    } else if (code === listTypeModal.FLIGHT) {
      router.push({
        pathname: routeStatic.ACCOUNT_ORDER_FLIGHT.href,
      });
    } else if (code === listTypeModal.LOGOUT) {
      try {
        const { data } = await LogoutAccount(cookie.get(TOKEN));
        if (data.code === 200) {
          cookie.remove(TOKEN);
          localStorage.removeItem(LAST_BOOKING_HOTEL);
          dispatch({
            type: actionTypes.SET_INFORMATION_USER,
            payload: {},
          });
          if (router.route.includes("/tai-khoan")) {
            router.push({
              pathname: routeStatic.HOME.href,
            });
          } else {
            router.reload();
          }
        }
      } catch (error) {}
    }
  };

  const NoneLogin = (
    <Box
      display="flex"
      alignItems="center"
      onClick={() => setVisible(!visible)}
    >
      <IconUser className="svgFillAll" style={{ stroke: getColor() }} />
      <IconDropDown
        className="svgFillAll"
        style={{ stroke: getColor(), marginLeft: 6 }}
      />
    </Box>
  );
  const InfoLogin = (
    <Box
      className={classes.userAccount}
      style={{
        border:
          type === "listing" || type === "detail"
            ? "1px solid #1A202C"
            : "1px solid #FFFFFF",
      }}
      onClick={() => setVisible(!visible)}
    >
      {!isEmpty(informationUser?.profilePhoto) ? (
        <Avatar
          alt="Remy Sharp"
          src={informationUser?.profilePhoto}
          className={classes.avatarUser}
        />
      ) : (
        <Avatar className={classes.avatarDefault}>
          {lastNameUSer[0] || ""}
        </Avatar>
      )}
      <Box
        component="span"
        fontSize={12}
        lineHeight="14px"
        style={{ color: "#1A202C" }}
        fontWeight={600}
        pl={6 / 8}
      >
        {lastNameUSer || ""}
      </Box>
      <IconDropDown
        className="svgFillAll"
        style={{ stroke: "#1A202C", marginLeft: 6 }}
      />
    </Box>
  );

  const menu = isLogin ? menuAccount : menuNoneAccount;

  return (
    <Box>
      <Box className={classes.wrapperItem} style={{ color: getColor() }}>
        {isLogin ? InfoLogin : NoneLogin}
        {visible && (
          <OutsideClickHandler onOutsideClick={() => setVisible(false)}>
            <Box className={classes.dropdownContainer}>
              <Box className={classes.dropdownContent}>
                <Box className={clsx(classes.arrowBlock)} />
                {menu.map((el, i) => (
                  <Box
                    className={classes.dropdownItem}
                    key={i}
                    onClick={() => {
                      handleSelectItem(el.code);
                      setVisible(false);
                    }}
                  >
                    <Box className={classes.addressText}>{el.name}</Box>
                  </Box>
                ))}
              </Box>
            </Box>
          </OutsideClickHandler>
        )}
        <LoginModal
          open={visibleModal === listTypeModal.MODAL_LOGIN}
          handleClose={() => setVisibleModal("")}
          handleSelectItem={handleSelectItem}
        />
        <RegisterModal
          open={visibleModal === listTypeModal.MODAL_REGISTER}
          handleClose={() => setVisibleModal("")}
          handleSelectItem={handleSelectItem}
        />
      </Box>
    </Box>
  );
};

export default UserDropDown;
