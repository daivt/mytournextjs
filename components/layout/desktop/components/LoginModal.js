import { useState } from "react";
import {
  Box,
  InputAdornment,
  IconButton,
  makeStyles,
  Modal,
  Fade,
  Backdrop,
} from "@material-ui/core";
import * as yup from "yup";
import cookie from "js-cookie";
import { Formik, Form } from "formik";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";

import * as actionTypes from "@contextProvider/system/ActionTypes";
import { listString, TOKEN, LAST_BOOKING_HOTEL } from "@utils/constants";
import { IconClose, IconVisibility, IconVisibilityOff } from "@public/icons";
import { useSystem, useDispatchSystem } from "@contextProvider/ContextProvider";

import { LoginAccount } from "@api/user";

import snackbarSetting from "@src/alert/Alert";
import ButtonComponent from "@src/button/Button";
import FieldTextContent from "@src/form/FieldTextContent";
import ButtonLoginViaGGFB from "@components/common/login/ButtonLoginViaGGFB";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    position: "relative",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: theme.palette.white.main,
    boxShadow: theme.shadows[5],
    width: 408,
    borderRadius: 8,
    padding: "32px 48px",
    outline: "none",
  },
  boxIconClose: {
    position: "absolute",
    right: 4,
    top: 4,
  },
  boxBtnRegister: {
    display: "flex",
    alignItems: "center",
    fontSize: 14,
    lineHeight: "17px",
    paddingTop: 24,
    justifyContent: "center",
  },
}));

const LoginModal = ({
  open = false,
  handleClose = () => {},
  handleSelectItem = () => {},
}) => {
  const router = useRouter();
  const classes = useStyles();
  const dispatch = useDispatchSystem();
  const { systemReducer } = useSystem();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const storeSchema = yup.object().shape({
    account: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_PHONE),
    password: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_PASSWORD)
      .min(6, listString.IDS_MT_TEXT_VALIDATE_PASSWORD)
      .max(50, listString.IDS_MT_TEXT_VALIDATE_PASSWORD),
  });

  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{ timeout: 500 }}
    >
      <Fade in={open}>
        <Box className={classes.paper}>
          <Box className={classes.boxIconClose}>
            <IconButton onClick={handleClose}>
              <IconClose />
            </IconButton>
          </Box>
          <Box
            component="span"
            fontWeight={600}
            fontSize={22}
            lineHeight="22px"
          >
            {listString.IDS_MT_TEXT_LOGIN}
          </Box>
          <ButtonLoginViaGGFB heightBtn={44} fontSizeText={14} isPc />
          <Box
            lineHeight="14px"
            fontSize={12}
            display="flex"
            justifyContent="center"
            pt={3}
            pb={12 / 8}
          >
            {listString.IDS_MT_TEXT_LOGIN_BY_PHONE_OR_EMAIL}
          </Box>
          <Formik
            initialValues={{
              account: "",
              password: "",
              showPassword: false,
            }}
            onSubmit={async (values) => {
              try {
                const dataDTO = {
                  ...values,
                };
                delete dataDTO.showPassword;
                dispatch({
                  type: actionTypes.SET_VISIBLE_FETCHING,
                  payload: true,
                });
                const { data } = await LoginAccount(dataDTO);
                dispatch({
                  type: actionTypes.SET_VISIBLE_FETCHING,
                  payload: false,
                });
                if (data.code === 200) {
                  localStorage.removeItem(LAST_BOOKING_HOTEL);
                  cookie.set(TOKEN, data.data.loginToken, { expires: 365 });
                  router.reload();
                  data?.message &&
                    enqueueSnackbar(
                      listString.IDS_MT_TEXT_LOGIN_ACCOUNT_SUCCESS,
                      snackbarSetting((key) => closeSnackbar(key), {
                        color: "success",
                      })
                    );
                } else {
                  data?.message &&
                    enqueueSnackbar(
                      data?.message,
                      snackbarSetting((key) => closeSnackbar(key), {
                        color: "error",
                      })
                    );
                }
              } catch (error) {
                dispatch({
                  type: actionTypes.SET_VISIBLE_FETCHING,
                  payload: false,
                });
              }
            }}
            validationSchema={storeSchema}
          >
            {({ values, setFieldValue }) => {
              return (
                <Form>
                  <FieldTextContent
                    name="account"
                    optional
                    placeholder={listString.IDS_MT_TEXT_ENTER_PHONE_OR_EMAIL}
                    inputStyle={{
                      border: "none",
                      height: 44,
                      backgroundColor: "#EDF2F7",
                      borderRadius: 8,
                      paddingLeft: 16,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                  />
                  <FieldTextContent
                    name="password"
                    type={values.showPassword ? "text" : "password"}
                    optional
                    placeholder={listString.IDS_MT_TEXT_PASSWORD}
                    inputStyle={{
                      border: "none",
                      height: 44,
                      backgroundColor: "#EDF2F7",
                      borderRadius: 8,
                      paddingLeft: 16,
                      paddingRight: 16,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={() => {
                            setFieldValue("showPassword", !values.showPassword);
                          }}
                          edge="end"
                        >
                          {values.showPassword ? (
                            <IconVisibility />
                          ) : (
                            <IconVisibilityOff />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                  <ButtonComponent
                    backgroundColor="#FF1284"
                    borderRadius={8}
                    height={44}
                    padding="10px 40px"
                    fontSize={16}
                    fontWeight={600}
                    type="submit"
                  >
                    {listString.IDS_MT_TEXT_LOGIN}
                  </ButtonComponent>
                  <Box className={classes.boxBtnRegister}>
                    <Box component="span">Chưa có tài khoản?</Box>
                    <ButtonComponent
                      padding="4px"
                      fontSize={16}
                      height={17}
                      typeButton="text"
                      color="#00B6F3"
                      width="fit-content"
                      handleClick={() => handleSelectItem("MODAL_REGISTER")}
                    >
                      Đăng ký tài khoản
                    </ButtonComponent>
                  </Box>
                </Form>
              );
            }}
          </Formik>
        </Box>
      </Fade>
    </Modal>
  );
};

export default LoginModal;
