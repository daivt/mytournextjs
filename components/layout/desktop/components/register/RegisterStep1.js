import { useState } from "react";
import { Box, makeStyles } from "@material-ui/core";
import * as yup from "yup";
import { Formik, Form } from "formik";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";

import * as actionTypes from "@contextProvider/system/ActionTypes";
import { listString, LAST_REGISTER_INFO } from "@utils/constants";
import { useSystem, useDispatchSystem } from "@contextProvider/ContextProvider";

import { sendSignupOTP } from "@api/user";

import snackbarSetting from "@src/alert/Alert";
import ButtonComponent from "@src/button/Button";
import FieldTextContent from "@src/form/FieldTextContent";

const useStyles = makeStyles((theme) => ({
  boxBtnRegister: {
    display: "flex",
    alignItems: "center",
    fontSize: 14,
    lineHeight: "17px",
    paddingTop: 24,
    justifyContent: "center",
  },
}));
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

const Register = ({
  handleChangeStep = () => {},
  handleSelectItem = () => {},
  phoneNumberInit = "",
}) => {
  const router = useRouter();
  const classes = useStyles();
  const dispatch = useDispatchSystem();
  const { systemReducer } = useSystem();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const storeSchema = yup.object().shape({
    phone: yup
      .string()
      .matches(phoneRegExp, listString.IDS_MT_TEXT_INVALID_PHONE_NUMBER)
      .trim()
      .min(9, listString.IDS_MT_TEXT_INVALID_PHONE_NUMBER)
      .max(11, listString.IDS_MT_TEXT_INVALID_PHONE_NUMBER)
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_PHONE),
  });

  return (
    <Formik
      initialValues={{
        phone: phoneNumberInit,
      }}
      onSubmit={async (values) => {
        try {
          dispatch({
            type: actionTypes.SET_VISIBLE_FETCHING,
            payload: true,
          });
          const { data } = await sendSignupOTP(values);
          dispatch({
            type: actionTypes.SET_VISIBLE_FETCHING,
            payload: false,
          });
          if (data.code === 200) {
            localStorage.setItem(LAST_REGISTER_INFO, JSON.stringify(values));
            handleChangeStep(2);
          } else {
            data?.message &&
              enqueueSnackbar(
                data?.message,
                snackbarSetting((key) => closeSnackbar(key), {
                  color: "error",
                })
              );
          }
        } catch (error) {
          dispatch({
            type: actionTypes.SET_VISIBLE_FETCHING,
            payload: false,
          });
        }
      }}
      validationSchema={storeSchema}
    >
      {() => {
        return (
          <Form>
            <FieldTextContent
              name="phone"
              optional
              placeholder={
                listString.IDS_MT_TEXT_PLACEHOLDER_ENTER_PHONE_NUMBER
              }
              inputStyle={{
                border: "none",
                height: 52,
                backgroundColor: "#EDF2F7",
                borderRadius: 8,
                paddingLeft: 16,
              }}
              inputProps={{
                autoComplete: "off",
              }}
            />
            <ButtonComponent
              backgroundColor="#FF1284"
              borderRadius={8}
              height={44}
              padding="10px 40px"
              fontSize={16}
              fontWeight={600}
              type="submit"
            >
              {listString.IDS_MT_TEXT_SIGNUP}
            </ButtonComponent>
            <Box className={classes.boxBtnRegister}>
              <Box component="span">Bạn đã có tài khoản?</Box>
              <ButtonComponent
                padding="4px"
                fontSize={16}
                height={17}
                typeButton="text"
                color="#00B6F3"
                width="fit-content"
                handleClick={() => handleSelectItem("MODAL_LOGIN")}
              >
                {listString.IDS_MT_TEXT_LOGIN}
              </ButtonComponent>
            </Box>
          </Form>
        );
      }}
    </Formik>
  );
};

export default Register;
