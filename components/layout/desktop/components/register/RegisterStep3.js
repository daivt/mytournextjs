import * as yup from "yup";
import cookie from "js-cookie";
import { Formik, Form } from "formik";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { makeStyles } from "@material-ui/styles";
import { Box, InputAdornment, IconButton } from "@material-ui/core";

import * as actionTypes from "@contextProvider/system/ActionTypes";
import { useDispatchSystem } from "@contextProvider/ContextProvider";

import { signupAccount } from "@api/user";
import ButtonComponent from "@src/button/Button";
import { IconVisibility, IconVisibilityOff } from "@public/icons";
import {
  listString,
  routeStatic,
  LAST_REGISTER_INFO,
  TOKEN,
  LAST_BOOKING_HOTEL,
} from "@utils/constants";

import snackbarSetting from "@src/alert/Alert";
import FieldTextContent from "@src/form/FieldTextContent";

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
  },
  iconPhone: {
    fill: theme.palette.primary.main,
  },
}));

const RegisterStep3 = ({ handleChangeStep = () => {} }) => {
  const router = useRouter();
  const classes = useStyles();
  const dispatch = useDispatchSystem();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const storeSchema = yup.object().shape({
    name: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_PHONE),
    email: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_EMAIL)
      .email(listString.IDS_MT_TEXT_EMAIL_INVALIDATE),
    password: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_PASSWORD)
      .min(6, listString.IDS_MT_TEXT_VALIDATE_PASSWORD)
      .max(20, listString.IDS_MT_TEXT_VALIDATE_PASSWORD),
    rePassword: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_CONFIRM_PASSWORD)
      .oneOf(
        [yup.ref("password"), null],
        listString.IDS_MT_TEXT_PASSWORD_CONFIRM_NOT_MATCH_PASSWORD
      )
      .min(6, listString.IDS_MT_TEXT_VALIDATE_PASSWORD)
      .max(20, listString.IDS_MT_TEXT_VALIDATE_PASSWORD),
  });
  return (
    <Box className={classes.container}>
      <Formik
        initialValues={{
          name: "",
          email: "",
          password: "",
          rePassword: "",
          showPassword: false,
          showRePassword: false,
        }}
        onSubmit={async (values) => {
          const lastRegisterInfo =
            JSON.parse(localStorage.getItem(LAST_REGISTER_INFO)) || {};
          const dataDTO = {
            ...values,
            ...lastRegisterInfo,
          };
          delete dataDTO.showPassword;
          delete dataDTO.showRePassword;
          try {
            const { data } = await signupAccount(dataDTO);
            if (data.code === 200) {
              localStorage.removeItem(LAST_REGISTER_INFO);
              localStorage.removeItem(LAST_BOOKING_HOTEL);
              cookie.set(TOKEN, data.access_token, { expires: 365 });
              handleChangeStep(4);
              dispatch({
                type: actionTypes.SET_INFORMATION_USER,
                payload: data.data,
              });
              data?.message &&
                enqueueSnackbar(
                  listString.IDS_MT_TEXT_CREATE_ACCOUNT_SUCCESS,
                  snackbarSetting((key) => closeSnackbar(key), {
                    color: "success",
                  })
                );
            } else {
              data?.message &&
                enqueueSnackbar(
                  data?.message,
                  snackbarSetting((key) => closeSnackbar(key), {
                    color: "error",
                  })
                );
            }
          } catch (error) {}
        }}
        validationSchema={storeSchema}
      >
        {({ values, setFieldValue }) => {
          return (
            <Form>
              <Box pt={2}>
                <FieldTextContent
                  name="name"
                  optional
                  placeholder={listString.IDS_MT_TEXT_NAME}
                  inputStyle={{
                    border: "none",
                    height: 44,
                    backgroundColor: "#EDF2F7",
                    borderRadius: 8,
                    paddingLeft: 16,
                  }}
                  inputProps={{
                    autoComplete: "off",
                  }}
                />
                <FieldTextContent
                  name="email"
                  optional
                  placeholder={listString.IDS_MT_TEXT_EMAIL}
                  inputStyle={{
                    border: "none",
                    height: 44,
                    backgroundColor: "#EDF2F7",
                    borderRadius: 8,
                    paddingLeft: 16,
                  }}
                  inputProps={{
                    autoComplete: "off",
                  }}
                />
                <FieldTextContent
                  name="password"
                  type={values.showPassword ? "text" : "password"}
                  optional
                  placeholder={listString.IDS_MT_TEXT_PASSWORD}
                  inputStyle={{
                    border: "none",
                    height: 44,
                    backgroundColor: "#EDF2F7",
                    borderRadius: 8,
                    paddingLeft: 16,
                    paddingRight: 16,
                  }}
                  inputProps={{
                    autoComplete: "off",
                  }}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={() => {
                          setFieldValue("showPassword", !values.showPassword);
                        }}
                        edge="end"
                      >
                        {values.showPassword ? (
                          <IconVisibility />
                        ) : (
                          <IconVisibilityOff />
                        )}
                      </IconButton>
                    </InputAdornment>
                  }
                />
                <FieldTextContent
                  name="rePassword"
                  optional
                  placeholder={listString.IDS_MT_TEXT_RE_PASSWORD}
                  type={values.showRePassword ? "text" : "password"}
                  inputStyle={{
                    border: "none",
                    height: 44,
                    backgroundColor: "#EDF2F7",
                    borderRadius: 8,
                    paddingLeft: 16,
                    paddingRight: 16,
                  }}
                  inputProps={{
                    autoComplete: "off",
                  }}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={() => {
                          setFieldValue(
                            "showRePassword",
                            !values.showRePassword
                          );
                        }}
                        edge="end"
                      >
                        {values.showRePassword ? (
                          <IconVisibility />
                        ) : (
                          <IconVisibilityOff />
                        )}
                      </IconButton>
                    </InputAdornment>
                  }
                />
                <ButtonComponent
                  backgroundColor="#FF1284"
                  borderRadius={8}
                  height={44}
                  padding="10px 40px"
                  fontSize={16}
                  fontWeight={600}
                  type="submit"
                >
                  Hoàn thành
                </ButtonComponent>
              </Box>
            </Form>
          );
        }}
      </Formik>
    </Box>
  );
};

export default RegisterStep3;
