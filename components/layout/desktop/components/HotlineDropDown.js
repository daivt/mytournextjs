import { makeStyles } from "@material-ui/styles";
import { Typography, Box } from "@material-ui/core";

import { routeStatic } from "@utils/constants";
import { IconPhoneContact, IconPartnership } from "@public/icons";

import Link from "@src/link/Link";

const useStyles = makeStyles((theme) => ({
  wrapperItem: {
    display: "flex",
    alignItems: "center",
    marginRight: 18,
    position: "relative",
    "&:hover": {
      textDecoration: "none",
    },
  },
}));

const HotlineDropDown = ({ type }) => {
  const classes = useStyles();
  const getColor = () => {
    return type === "listing" || type === "detail" || type === "account"
      ? "#1A202C"
      : "white";
  };
  return (
    <Box style={{ color: getColor() }} display="flex" alignItems="center">
      <Link href={routeStatic.PARTNERSHIP.href} className={classes.wrapperItem}>
        <IconPartnership
          className="svgFillAll"
          style={{ stroke: getColor(), marginRight: 8 }}
        />
        <Typography
          variant="body2"
          style={{ fontSize: 14, lineHeight: "17px", color: getColor() }}
        >
          {`Hợp tác với chúng tôi`}
        </Typography>
      </Link>
      <Box className={classes.wrapperItem}>
        <IconPhoneContact
          className="svgFillAll"
          style={{ stroke: getColor(), marginRight: 8 }}
        />
        <Typography
          variant="body2"
          style={{ fontSize: 14, lineHeight: "17px" }}
        >
          {`1900 2083`}
        </Typography>
      </Box>
    </Box>
  );
};

export default HotlineDropDown;
