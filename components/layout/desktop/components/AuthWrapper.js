import { useState } from "react";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import LoginModal from "@components/layout/desktop/components/LoginModal";
import RegisterModal from "@components/layout/desktop/components/RegisterModal";

const listTypeModal = {
  MODAL_LOGIN: "MODAL_LOGIN",
  MODAL_REGISTER: "MODAL_REGISTER",
};

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
  },
}));

const AuthWrapper = ({
  children = null,
  typeModal = "MODAL_LOGIN",
  isScrollTop = false,
  phoneNumberRegister = "",
}) => {
  const classes = useStyles();
  const [visibleModal, setVisibleModal] = useState("");

  const handleSelectItem = (code = "") => {
    if (code === listTypeModal.MODAL_LOGIN) {
      setVisibleModal(listTypeModal.MODAL_LOGIN);
    } else if (code === listTypeModal.MODAL_REGISTER) {
      setVisibleModal(listTypeModal.MODAL_REGISTER);
    }
  };

  return (
    <>
      <Box
        className={classes.content}
        onClick={(e) => {
          if (isScrollTop) {
            window.scrollTo({ top: 0, behavior: "smooth" });
          }
          e.preventDefault();
          setVisibleModal(typeModal);
        }}
      >
        {children || null}
      </Box>
      <LoginModal
        open={visibleModal === listTypeModal.MODAL_LOGIN}
        handleClose={() => setVisibleModal("")}
        handleSelectItem={handleSelectItem}
      />
      <RegisterModal
        open={visibleModal === listTypeModal.MODAL_REGISTER}
        handleClose={() => setVisibleModal("")}
        handleSelectItem={handleSelectItem}
        phoneNumberInit={phoneNumberRegister}
      />
    </>
  );
};

export default AuthWrapper;
