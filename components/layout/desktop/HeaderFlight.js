import SectionSearchFlight from "@components/common/desktop/searchBox/flight/SectionSearchFlight";
import { Box, Drawer } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconMenu } from "@public/icons";
import Image from "@src/image/Image";
import Link from "@src/link/Link";
import {
  bannerFlight,
  bannerHome,
  bannerHotel,
  listString,
  routeStatic,
} from "@utils/constants";
import { paramsDefaultHotel } from "@utils/helpers";
import { useState } from "react";
import DrawerHomeContent from "./components/DrawerHomeContent";
import HotlineDropDown from "./components/HotlineDropDown";
import UserDropDown from "./components/UserDropDown";

const useStyles = makeStyles((theme) => ({
  headerContainer: {
    height: 48,
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    margin: "0 auto",
  },
  headerLeft: { display: "flex", alignItems: "center" },
  headerRight: { display: "flex" },
  textItem: {
    fontWeight: 600,
    fontSize: 14,
    lineHeight: "17px",
    marginRight: 24,
  },
  wrapperItem: { display: "flex", alignItems: "center", marginRight: 18 },
  filterContainer: { textAlign: "center" },
  divider: {
    height: 3,
    width: 40,
    margin: "auto",
    marginTop: 12,
  },
  logoMytour: {
    width: 120,
    height: 32,
    marginRight: 55,
  },
}));

const HeaderFlight = ({
  type,
  topLocation = [],
  paramsUrl = paramsDefaultHotel(),
  hotelDetail = {},
  query = {},
}) => {
  const classes = useStyles();
  const [visible, setVisible] = useState(false);

  const getColor = () => {
    return type === "listing" || type === "detail" ? "#1A202C" : "white";
  };
  const menu = [
    {
      name: listString.IDS_MT_TEXT_HOTEL,
      url: routeStatic.HOTEL.href,
      type: "hotel",
    },
    {
      name: listString.IDS_MT_TEXT_FLIGHT,
      url: routeStatic.FLIGHT.href,
      type: "flight",
    },
    {
      name: listString.IDS_MT_TOUR_AND_EVENT,
      url: routeStatic.HOTEL.href,
      type: "tour",
    },
    {
      name: listString.IDS_MT_TEXT_TRAVEL_GUIDE,
      url: routeStatic.HOTEL.href,
      type: "guide",
    },
  ];
  let containerStyle = { width: "100%", height: 164, background: "white" };
  if (type !== "listing" && type !== "detail") {
    containerStyle = {
      ...containerStyle,
      height: 488,
      backgroundImage: `url(${
        // eslint-disable-next-line no-nested-ternary
        type === "hotel"
          ? bannerHotel
          : type === "flight"
          ? bannerFlight
          : bannerHome
      })`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
    };
  } else {
    containerStyle = {
      ...containerStyle,
      position: "sticky",
      top: 0,
      background: "white",
      zIndex: 2,
    };
  }
  return (
    <Box style={containerStyle}>
      <Box className={classes.headerContainer}>
        <Box className={classes.headerLeft} style={{ color: getColor() }}>
          <Link
            href={{ pathname: routeStatic.HOME.href }}
            style={{ cursor: "pointer" }}
          >
            {type === "listing" || type === "detail" ? (
              <img
                src="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_logo_mytour.svg"
                className={classes.logoMytour}
                style={{ marginRight: 56 }}
                alt="logo_mytour"
              />
            ) : (
              <img
                src="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_logo_mytour_white.svg"
                className={classes.logoMytour}
                style={{ marginRight: 56 }}
                alt="logo_mytour"
              />
            )}
          </Link>

          {menu.map((item, i) => (
            <Link
              key={i}
              href={{ pathname: item.url }}
              style={{
                textDecoration: "none",
                color: getColor(),
                marginTop: 20,
              }}
            >
              <div className={classes.textItem}>
                {item.name}
                <div
                  className={classes.divider}
                  style={{
                    background: type === item.type ? "white" : " transparent",
                  }}
                />
              </div>
            </Link>
          ))}
        </Box>
        <Box className={classes.headerRight}>
          <Box className={classes.wrapperItem} style={{ color: getColor() }}>
            <HotlineDropDown type={type} />
            <UserDropDown type={type} />
          </Box>
          <IconMenu
            className="svgFillAll"
            style={{ stroke: getColor(), cursor: "pointer" }}
            onClick={() => setVisible(true)}
          />
        </Box>
      </Box>
      <div
        className={classes.filterContainer}
        style={{
          padding:
            type === "listing" || type === "detail" ? 8 : "64px 0 240px 0",
          color: getColor(),
        }}
      >
        <SectionSearchFlight
          type={type}
          topLocation={topLocation}
          dataQuery={paramsUrl}
          query={query}
        />
      </div>
      <Drawer anchor="right" open={visible} onClose={() => setVisible(false)}>
        <DrawerHomeContent onClose={() => setVisible(false)} />
      </Drawer>
    </Box>
  );
};

export default HeaderFlight;
