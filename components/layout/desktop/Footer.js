import { Box, Typography, Grid } from "@material-ui/core";
import { useRouter } from "next/router";
import { makeStyles } from "@material-ui/styles";
import YouWantToSave from "@components/common/desktop/itemView/YouWantToSave";
import Link from "@src/link/Link";
import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  footerContainer: {
    width: "100%",
    paddingTop: 40,
    background: theme.palette.gray.grayLight26,
  },
  footerContent: { maxWidth: 1188, width: "100%", margin: "0 auto" },
  columnTitle: {
    fontSize: 14,
    lineHeight: "17px",
    fontWeight: 600,
    marginBottom: 8,
  },
  columnTextItem: { fontSize: 12, lineHeight: "24px", color: "#4A5568" },
  divider: {
    height: 1,
    background: "#DBE8ED",
    width: "100%",
    marginTop: 20,
  },
  logoGroup: { display: "flex", width: "100%", marginTop: 15 },
  linkPolicyInfo: {
    color: theme.palette.gray.grayDark8,
    "&:hover": {
      textDecoration: "none",
      color: theme.palette.primary.main,
    },
  },
  logoMytourLarge: {
    width: 165,
    height: 44,
  },
}));

const companyInfo = [
  "Tổng đài chăm sóc: 1900 2083",
  "Email: hotro@mytour.vn",
  "Văn phòng Hà Nội: Tầng 11, Tòa Peakview, 36 Hoàng Cầu, Đống Đa",
  "Văn phòng HCM: Tầng 6, Tòa Nhà Central Park, 117 Nguyễn Du, Q.1",
  "Mytour Store: 168 Xã Đàn, Đống Đa, Hà Nội",
];
const policyInfo = [
  {
    title: "Chính sách và quy định chung",
    link: "/news/135152-chinh-sach-va-quy-dinh-chung.html",
  },
  {
    title: "Quy định về thanh toán",
    link: "/news/135633-quy-dinh-ve-thanh-toan.html",
  },
  {
    title: "Quy định về xác nhận thông tin đặt phòng",
    link: "/news/135634-quy-dinh-ve-xac-nhan-thong-tin-dat-phong.html",
  },
  {
    title: "Chính sách về hủy đặt phòng và hoàn trả tiền",
    link: "/news/135154-chinh-sach-huy-phong-va-hoan-tien.html",
  },
  {
    title: "Chính sách bảo mật thông tin",
    link:
      "/news/135636-chinh-sach-bao-mat-thong-tin-danh-cho-website-tmdt.html",
  },
  {
    title: "Quy chế hoạt động",
    link: "/news/135155-quy-che-hoat-dong.html",
  },
  {
    title: "Chính sách bảo mật",
    link: "/news/135156-chinh-sach-bao-mat-thong-tin-danh-cho-san-gdtmdt.html",
  },
  {
    title: "Quy trình giải quyết tranh chấp, khiếu nại",
    link: "/news/135420-quy-trinh-giai-quyet-tranh-chap-khieu-nai.html",
  },
];
const customerInfo = [
  {
    title: "Đăng nhập HMS",
    link: "https://hms.mytour.vn/",
  },
  {
    title: "Tuyển dụng",
    link: "https://career.mytour.vn/",
  },
  {
    title: "Liên hệ",
    link: "/help/30-lien-he.html",
  },
];
const Footer = ({ isSave = false, isServerLoaded = true }) => {
  const classes = useStyles();
  const router = useRouter();

  return (
    <Box className={classes.footerContainer}>
      <Box className={classes.footerContent}>
        {isSave && isServerLoaded && (
          <Box style={{ marginTop: -94, marginBottom: 40 }}>
            <YouWantToSave />
          </Box>
        )}
        <span>
          <img
            src="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_mytour_blue_large.svg"
            className={classes.logoMytourLarge}
            alt="logo_mytour_footer"
          />
        </span>
        <Grid container spacing={3} style={{ marginTop: 24 }}>
          <Grid item xs>
            <Typography variant="body2" className={classes.columnTitle}>
              Công ty cổ phần du lịch Việt Nam VNTravel
            </Typography>
            {companyInfo.map((el, i) => (
              <Typography
                variant="body2"
                key={i}
                className={classes.columnTextItem}
              >
                {el}
              </Typography>
            ))}
            {isServerLoaded && (
              <Box className={classes.logoGroup}>
                <Link
                  href="http://online.gov.vn/Home/WebDetails/75906"
                  target="_blank"
                >
                  <img
                    src="https://staticproxy.mytourcdn.com/0x0,q90/themes/images/logo-dathongbao-bocongthuong-w165.png"
                    alt=""
                    style={{ width: 160 }}
                  />
                </Link>
                <Link
                  href="http://online.gov.vn/Home/WebDetails/76269"
                  target="_blank"
                >
                  <img
                    src="https://staticproxy.mytourcdn.com/0x0,q90/themes/images/logo-congthuong-w165.png"
                    alt=""
                    style={{ width: 160 }}
                  />
                </Link>
              </Box>
            )}
          </Grid>
          <Grid item xs>
            <Typography variant="body2" className={classes.columnTitle}>
              Chính sách & Quy định
            </Typography>
            <Box display="flex" flexDirection="column">
              {policyInfo.map((el, i) => (
                <Link
                  href={el.link}
                  target="_blank"
                  className={classes.linkPolicyInfo}
                  key={i.toString()}
                >
                  <Box component="span" fontSize={12} lineHeight="24px">
                    {el.title}
                  </Box>
                </Link>
              ))}
            </Box>
          </Grid>
          <Grid item xs>
            <Typography variant="body2" className={classes.columnTitle}>
              Khách hàng và đối tác
            </Typography>
            <Box display="flex" flexDirection="column">
              {customerInfo.map((el, i) => (
                <Link
                  href={el.link}
                  target="_blank"
                  className={classes.linkPolicyInfo}
                  key={i.toString()}
                >
                  <Box component="span" fontSize={12} lineHeight="24px">
                    {el.title}
                  </Box>
                </Link>
              ))}
            </Box>
          </Grid>
        </Grid>
        <div className={classes.divider} />
      </Box>
    </Box>
  );
};

export default Footer;
