import Head from "next/head";
import cookie from "js-cookie";
import { Box } from "@material-ui/core";
import PropTypes from "prop-types";
import { memo, useEffect, useState } from "react";

import { isEmpty, paramsDefaultHotel } from "@utils/helpers";
import { validateAccessToken, addFavorite } from "@api/user";
import { TOKEN, LAST_HOTEL_FAVORITES } from "@utils/constants";
import * as actionTypes from "@contextProvider/system/ActionTypes";
import { useDispatchSystem, useSystem } from "@contextProvider/ContextProvider";

import Footer from "@components/layout/desktop/Footer";
import Header from "@components/layout/desktop/Header";
import LoadingApp from "@components/common/loading/LoadingApp";
import HeaderBooking from "@components/layout/desktop/HeaderBooking";
import HeaderMyTourMall from "@components/layout/desktop/HeaderMyTourMall";
import HeaderBookingFlight from "@components/layout/desktop/HeaderBookingFlight";
import FooterBottom from "./FooterBottom";

const Layout = ({
  children,
  isHeader = false,
  isFooter = false,
  isFooterBottom = true,
  type,
  topLocation = [],
  location = {},
  hotelDetail = {},
  paramsUrl = paramsDefaultHotel(),
  paramsQuery = {},
  queryCodeFlight = {},
  query = {},
  cityInfo = {},
  csrfToken = "kh1ez4WH3sozbpbLC7tOumNr81Ut9n2kVwAu1g1S",
  iconOgImage = "https://mytour.vn/themes/images/logo-ss-facebook.png",
  title = "Đặt phòng khách sạn, vé máy bay giá rẻ hàng đầu Việt Nam | Mytour.vn",
  iconImageWeb = "https://staticproxy.mytourcdn.com/0x0,q90/themes/images/favicon.ico",
  description = "Đặt phòng khách sạn giá rẻ trên Mytour. Đặt càng sát ngày, giá càng tốt. Đặt phòng khuyến mãi giá rẻ nhất ở Việt Nam và các nước khác chỉ trong 3 bước đơn giản. Giá trung thực, không phí ẩn!",
  keywords = "khách sạn, đặt phòng khách sạn, đặt phòng giá rẻ, đặt khách sạn hạng sang giá tốt, đặt khách sạn sát ngày giá tốt, đặt phòng khách sạn thuận tiện, hỗ trợ dịch vụ khách hàng 24/7 đặt phòng khách sạn",
  currentStep = 0,
}) => {
  const { systemReducer } = useSystem();
  const dispatch = useDispatchSystem();
  const [isServerLoaded, setServerLoaded] = useState(false);

  const handleAddFavorite = async (listHotelFavorites = []) => {
    try {
      localStorage.removeItem(LAST_HOTEL_FAVORITES);
      listHotelFavorites.forEach((el) => {
        addFavorite({ hotelId: el });
      });
    } catch (error) {}
  };
  const fetUserInfo = async () => {
    try {
      const { data } = await validateAccessToken();
      if (data.code === 200) {
        dispatch({
          type: actionTypes.SET_INFORMATION_USER,
          payload: data.data,
        });
        const listHotelFavorites =
          JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
        if (!isEmpty(listHotelFavorites)) {
          handleAddFavorite(listHotelFavorites);
        }
      } else {
        cookie.remove(TOKEN);
      }
    } catch (error) {
      cookie.remove(TOKEN);
    }
  };
  useEffect(() => {
    setServerLoaded(true);
    const informationUser = systemReducer.informationUser || {};
    if (isEmpty(informationUser) && !isEmpty(cookie.get(TOKEN))) {
      fetUserInfo();
    } // eslint-disable-next-line
  }, []);
  const getHeaderContent = () => {
    if (type === "bookingFlight") {
      return <HeaderBookingFlight currentStep={currentStep} />;
    }
    if (type === "booking") {
      return (
        <HeaderBooking
          type={type}
          topLocation={topLocation}
          paramsUrl={query}
        />
      );
    }
    if (type === "myTourMall") {
      return (
        <HeaderMyTourMall
          type={type}
          topLocation={topLocation}
          location={location}
          hotelDetail={hotelDetail}
          paramsUrl={!isEmpty(paramsUrl) ? paramsUrl : paramsDefaultHotel()}
          paramsQuery={paramsQuery}
          cityInfo={cityInfo}
          isServerLoaded={isServerLoaded}
        />
      );
    }

    return (
      <Header
        type={type}
        topLocation={topLocation}
        location={location}
        hotelDetail={hotelDetail}
        paramsUrl={!isEmpty(paramsUrl) ? paramsUrl : paramsDefaultHotel()}
        paramsQuery={paramsQuery}
        cityInfo={cityInfo}
        isServerLoaded={isServerLoaded}
        queryCodeFlight={queryCodeFlight}
        query={query}
      />
    );
  };

  return (
    <Box height="auto" bgcolor="gray.grayLight14" minHeight="100vh">
      <Head>
        <title>{title}</title>
        <meta
          name="viewport"
          content="initial-scale=1.0, maximum-scale=1.0, width=device-width, user-scalable=no"
        />
        <meta name="description" content={description} />
        <meta name="keywords" content={keywords} />
        <meta name="csrf-token" content={csrfToken} />
        <meta httpEquiv="x-dns-prefetch-control" content="on" />
        <link rel="dns-prefetch" href="https://mytourcdn.com/" />
        <meta name="robots" content="index, follow" />
        <link rel="alternate" hrefLang="vi" href="https://mytour.vn/" />
        <link href={iconImageWeb} rel="icon" type="image/x-icon" />
        <meta property="al:ios:app_name" content="Mytour.vn" />
        <meta property="al:ios:app_store_id" content="1149730203" />
        <meta property="al:android:app_name" content="Mytour.vn" />
        <meta property="al:android:package" content="vn.mytour.apps.android" />
        <meta property="fb:app_id" content="857393964278669" />
        <meta property="fb:pages" content="180719541988518" />
        <meta property="og:image" content={iconOgImage} />
        <meta property="og:url" content="https://mytour.vn" />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="Mytour.vn" />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
      </Head>
      {isHeader && getHeaderContent()}
      <main>{children}</main>
      {isFooter && (
        <Footer
          isSave={
            type === "home" ||
            type === "hotel" ||
            type === "top_hotel" ||
            type === "flight"
          }
          isServerLoaded={isServerLoaded}
        />
      )}
      {isFooterBottom && <FooterBottom />}
      <LoadingApp />
    </Box>
  );
};

Layout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default memo(Layout);
