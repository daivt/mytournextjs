import SectionSearchFlight from "@components/common/desktop/searchBox/flight/SectionSearchFlight";
import SectionSearchHotel from "@components/common/desktop/searchBox/hotel/SectionSearchHotel";
import { Box, Drawer, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconMenu } from "@public/icons";
import Image from "@src/image/Image";
import Link from "@src/link/Link";
import {
  bannerFlight,
  bannerHome,
  bannerHotel,
  listString,
  routeStatic,
} from "@utils/constants";
import {
  handleTitleFlightSearchForm,
  paramsDefaultHotel,
} from "@utils/helpers";
import { useState } from "react";
import DrawerHomeContent from "./components/DrawerHomeContent";
import HotlineDropDown from "./components/HotlineDropDown";
import UserDropDown from "./components/UserDropDown";
import * as gtm from "@utils/gtm";
const useStyles = makeStyles((theme) => ({
  headerContainer: {
    height: 48,
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    margin: "0 auto",
  },
  headerLeft: { display: "flex", alignItems: "center" },
  headerRight: { display: "flex" },
  textItem: {
    fontWeight: 600,
    fontSize: 14,
    lineHeight: "17px",
    marginRight: 24,
  },
  wrapperItem: { display: "flex", alignItems: "center", marginRight: 18 },
  filterContainer: { textAlign: "center" },
  divider: {
    height: 3,
    width: 40,
    margin: "auto",
    marginTop: 12,
  },
  headerSearchBox: {
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    flexDirection: "column",
    margin: "0 auto",
  },
  textBoxItem: { marginLeft: 24, marginBottom: 12, cursor: "pointer" },
  logoMytour: {
    width: 120,
    height: 32,
    marginRight: 56,
  },
}));

const Header = ({
  type,
  topLocation = [],
  paramsUrl = paramsDefaultHotel(),
  location = {},
  hotelDetail = {},
  paramsQuery = {},
  cityInfo = {},
  queryCodeFlight = {},
  query = {},
}) => {
  const classes = useStyles();
  const [visible, setVisible] = useState(false);
  const [active, setActive] = useState("hotel");

  const getColor = () => {
    return type === "listing" || type === "detail" || type === "account"
      ? "#1A202C"
      : "white";
  };
  const menu = [
    {
      name: listString.IDS_MT_TEXT_HOTEL,
      url: routeStatic.HOTEL.href,
      type: ["hotel", "top_hotel"],
      code: "hotel",
    },
    {
      name: listString.IDS_MT_TEXT_FLIGHT,
      url: routeStatic.FLIGHT.href,
      type: ["flight"],
      code: "flight",
    },
    {
      name: listString.IDS_MT_TOUR_AND_EVENT,
      url: "https://mytourevent.vn/",
      type: ["tour"],
      code: "tour",
    },
    {
      name: listString.IDS_MT_TEXT_TRAVEL_GUIDE,
      url: routeStatic.BLOG.href,
      type: ["blog"],
      code: "blog",
    },
  ];
  const menuSearch = [
    {
      name: listString.IDS_MT_TEXT_HOTEL,
      isShow: type !== "listing" && type !== "detail" && type !== "flight",
      type: "hotel",
    },
    {
      name: listString.IDS_MT_TEXT_FLY,
      isShow: type === "flight" || type === "home",
      type: "flight",
    },
  ];
  let containerStyle = {
    width: "100%",
    height: type === "account" ? 60 : 140,
    background: "white",
  };

  if (type !== "listing" && type !== "detail" && type !== "account") {
    containerStyle = {
      ...containerStyle,
      height: 488,
      backgroundImage: `url(${
        // eslint-disable-next-line no-nested-ternary
        type === "hotel"
          ? bannerHotel
          : type === "flight" || active === "flight"
          ? bannerFlight
          : bannerHome
      })`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      transition: "background-image 1s ease-in-out",
    };
  } else {
    containerStyle = {
      ...containerStyle,
      position: "sticky",
      top: 0,
      background: "white",
      zIndex: 2,
    };
  }
  const isLargeText =
    type === "flight" || type === "hotel" || type === "top_hotel";
  return (
    <Box style={containerStyle}>
      <Box
        className={classes.headerContainer}
        style={
          type === "home" || type === "hotel"
            ? {
                maxWidth: "100%",
                padding: "0 calc(50% - 594px)",
                background:
                  "linear-gradient(180deg, rgba(0, 0, 0, 0.1) 50%, rgba(0, 0, 0, 0.02) 100%)",
              }
            : {}
        }
      >
        <Box className={classes.headerLeft} style={{ color: getColor() }}>
          <Link
            href={{ pathname: routeStatic.HOME.href }}
            style={{ cursor: "pointer" }}
          >
            {type === "listing" || type === "detail" || type === "account" ? (
              <img
                src="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_logo_mytour.svg"
                className={classes.logoMytour}
                style={{ marginRight: 56 }}
                alt="logo_mytour"
              />
            ) : (
              <img
                src="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_logo_mytour_white.svg"
                className={classes.logoMytour}
                style={{ marginRight: 56 }}
                alt="logo_mytour"
              />
            )}
          </Link>

          {menu.map((item, i) => {
            if (item.code === "blog") {
              return (
                <a
                  key={i}
                  href={item.url}
                  style={{
                    textDecoration: "none",
                    color: getColor(),
                    marginTop: 20,
                  }}
                  onClick={() => {
                    if (item.code === "blog") {
                      gtm.addEventGtm("BlogPageDirect");
                    }
                  }}
                >
                  <div className={classes.textItem}>
                    {item.name}
                    <div
                      className={classes.divider}
                      style={{
                        background: item.type.includes(type)
                          ? "white"
                          : " transparent",
                      }}
                    />
                  </div>
                </a>
              );
            }

            return (
              <Link
                key={i}
                href={{ pathname: item.url }}
                style={{
                  textDecoration: "none",
                  color: getColor(),
                  marginTop: 20,
                }}
                onClick={() => {
                  if (item.code === "tour") {
                    gtm.addEventGtm("TourEventPageDirect");
                  }
                }}
              >
                <div className={classes.textItem}>
                  {item.name}
                  <div
                    className={classes.divider}
                    style={{
                      background: item.type.includes(type)
                        ? "white"
                        : " transparent",
                    }}
                  />
                </div>
              </Link>
            );
          })}
        </Box>
        <Box className={classes.headerRight}>
          <Box className={classes.wrapperItem} style={{ color: getColor() }}>
            <HotlineDropDown type={type} />
            <UserDropDown type={type} />
          </Box>
          <IconMenu
            className="svgFillAll"
            style={{ stroke: getColor(), cursor: "pointer" }}
            onClick={() => setVisible(true)}
          />
        </Box>
      </Box>
      {type !== "account" && (
        <div
          className={classes.filterContainer}
          style={{
            padding:
              type === "listing" || type === "detail" ? 8 : "64px 0 240px 0",
            color: getColor(),
          }}
        >
          <Box className={classes.headerSearchBox}>
            <Box className={classes.headerLeft}>
              {menuSearch.map((item, i) =>
                item.isShow ? (
                  <Box
                    className={classes.textBoxItem}
                    key={i}
                    onClick={() => setActive(item.type)}
                  >
                    <Typography
                      variant="body2"
                      style={{
                        fontWeight: 600,
                        fontSize: isLargeText ? 30 : 18,
                        lineHeight: isLargeText ? "36px" : "21px",
                      }}
                    >
                      {item.name}{" "}
                      {type === "top_hotel" ? `tại ${cityInfo.name}` : ""}
                      {type === "flight" && query?.slug?.length > 1
                        ? handleTitleFlightSearchForm(query, queryCodeFlight)
                        : ""}
                      {/* {type === "flight" &&
                      queryCodeFlight?.depart &&
                      queryCodeFlight?.arrival
                        ? `từ ${queryCodeFlight?.depart} (${
                            queryCodeFlight?.code?.split("-")[0]
                          }) đi ${queryCodeFlight?.arrival} (${
                            queryCodeFlight?.code?.split("-")[1]
                          })`
                        : ""} */}
                    </Typography>
                    {type === "home" && (
                      <div
                        className={classes.divider}
                        style={{
                          background:
                            active === item.type ? "white" : " transparent",
                        }}
                      />
                    )}
                  </Box>
                ) : null
              )}
            </Box>
            {type === "flight" || active === "flight" ? (
              <SectionSearchFlight
                type={type}
                topLocation={topLocation}
                dataQuery={paramsUrl}
                codeFlight={queryCodeFlight}
                query={query}
              />
            ) : (
              <SectionSearchHotel
                type={type}
                topLocation={topLocation}
                paramsUrl={paramsUrl}
                location={location}
                hotelDetail={hotelDetail}
                paramsQuery={paramsQuery}
                cityInfo={cityInfo}
              />
            )}
          </Box>
        </div>
      )}

      <Drawer anchor="right" open={visible} onClose={() => setVisible(false)}>
        <DrawerHomeContent onClose={() => setVisible(false)} />
      </Drawer>
    </Box>
  );
};

export default Header;
