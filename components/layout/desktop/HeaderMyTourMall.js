import SectionSearchMyTourMall from "@components/common/desktop/searchBox/hotel/SectionSearchMyTourMall";
import { Box, Drawer, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconMenu } from "@public/icons";
import Image from "@src/image/Image";
import Link from "@src/link/Link";
import {
  listIcons,
  listString,
  prefixUrlIcon,
  routeStatic,
} from "@utils/constants";
import { paramsDefaultHotel } from "@utils/helpers";
import { useState } from "react";
import DrawerHomeContent from "./components/DrawerHomeContent";
import HotlineDropDown from "./components/HotlineDropDown";
import UserDropDown from "./components/UserDropDown";

const bannerHome =
  "https://storage.googleapis.com/tripi-assets/mytour/images/banner%20mall.png";

const useStyles = makeStyles((theme) => ({
  headerContainer: {
    height: 48,
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    margin: "0 auto",
  },
  headerLeft: { display: "flex", alignItems: "center" },
  headerRight: { display: "flex" },
  textItem: {
    fontWeight: 600,
    fontSize: 14,
    lineHeight: "17px",
    marginRight: 24,
  },
  wrapperItem: { display: "flex", alignItems: "center", marginRight: 18 },
  filterContainer: { textAlign: "center" },
  divider: {
    height: 3,
    width: 40,
    margin: "auto",
    marginTop: 12,
  },
  headerSearchBox: {
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    flexDirection: "column",
    margin: "0 auto",
  },
  textBoxItem: { marginBottom: 24, cursor: "pointer" },
  iconMytourMallWhite: {
    width: 151,
    height: 40,
  },
}));

const HeaderMyTourMall = ({
  type,
  topLocation = [],
  paramsUrl = paramsDefaultHotel(),
  location = {},
  hotelDetail = {},
  paramsQuery = {},
  cityInfo = {},
  isServerLoaded = true,
}) => {
  const classes = useStyles();
  const [visible, setVisible] = useState(false);

  let containerStyle = { width: "100%", height: 140, background: "white" };
  if (isServerLoaded) {
    containerStyle = {
      ...containerStyle,
      height: 488,
      backgroundImage: `url(${bannerHome})`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
    };
  }
  return (
    <Box style={containerStyle}>
      <Box className={classes.headerContainer}>
        <Box className={classes.headerLeft} style={{ color: "white" }}>
          <Link
            href={{ pathname: routeStatic.HOME.href }}
            style={{ cursor: "pointer" }}
          >
            {/* <Image
              srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_mytour_mall.svg"
              style={{
                marginRight: 56,
                width: 151,
                height: 40,
              }}
            /> */}
            <Image
              srcImage={`${prefixUrlIcon}${listIcons.IconMytourMallWhite}`}
              className={classes.iconMytourMallWhite}
            />
          </Link>
        </Box>
        <Box className={classes.headerRight}>
          {/* <Box className={classes.wrapperItem} style={{ color: "white" }}>
            <IconDownloadApp
              className="svgFillAll"
              style={{ stroke: "white", marginRight: 8 }}
            />
            <Typography
              variant="body2"
              style={{ fontSize: 14, lineHeight: "17px" }}
            >
              {listString.IDS_MT_TEXT_DOWNLOAD_APP}
            </Typography>
          </Box> */}
          <Box className={classes.wrapperItem} style={{ color: "white" }}>
            <HotlineDropDown type={type} />
            <UserDropDown type={type} />
          </Box>
          <IconMenu
            className="svgFillAll"
            style={{ stroke: "white", cursor: "pointer" }}
            onClick={() => setVisible(true)}
          />
        </Box>
      </Box>
      <div
        className={classes.filterContainer}
        style={{
          padding: "64px 0 240px 0",
          color: "white",
        }}
      >
        <Box className={classes.headerSearchBox}>
          <Box className={classes.headerLeft}>
            <Box className={classes.textBoxItem}>
              <Typography
                variant="body2"
                style={{
                  fontWeight: 600,
                  fontSize: 36,
                  lineHeight: "52px",
                  width: 394,
                  textAlign: "start",
                }}
              >
                {listString.IDS_MT_MY_TOUR_MALL_HEADER_TEXT}
              </Typography>
            </Box>
          </Box>
          <SectionSearchMyTourMall
            type={type}
            topLocation={topLocation}
            paramsUrl={paramsUrl}
            location={location}
            hotelDetail={hotelDetail}
            paramsQuery={paramsQuery}
            cityInfo={cityInfo}
          />
        </Box>
      </div>
      <Drawer anchor="right" open={visible} onClose={() => setVisible(false)}>
        <DrawerHomeContent onClose={() => setVisible(false)} />
      </Drawer>
    </Box>
  );
};

export default HeaderMyTourMall;
