import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconCompanyGroup } from "@public/icons";
import Image from "@src/image/Image";
import { listString } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  footerContainer: {
    width: "100%",
    paddingTop: 24,
    paddingBottom: 24,
    background: theme.palette.gray.grayLight26,
  },
  footerContent: { maxWidth: 1188, width: "100%", margin: "0 auto" },
  columnTextItem: { fontSize: 12, lineHeight: "24px", color: "#4A5568" },
  companyInfoWrapper: { textAlign: "center" },
  companyIconGroup: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    margin: "12px 0 20px 0",
  },
  logoGroup: { display: "flex", width: "100%", marginTop: 15 },
  imageCompany: {
    width: 392,
    height: 32,
  },
}));

const FooterBottom = () => {
  const classes = useStyles();

  return (
    <Box className={classes.footerContainer}>
      <Box className={classes.footerContent}>
        <Box className={classes.companyInfoWrapper}>
          <Typography variant="body2" className={classes.columnTextItem}>
            {listString.IDS_MT_MYTOUR_DESCRIPTION}
          </Typography>
          <Box className={classes.companyIconGroup}>
            <Image
              srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_company_group.svg"
              className={classes.imageCompany}
              alt="vntravel_company_logo"
            />
          </Box>
          <Typography variant="body2" className={classes.columnTextItem}>
            {listString.IDS_MT_COPYRIGHT}
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default FooterBottom;
