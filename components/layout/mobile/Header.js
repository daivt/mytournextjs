import { useState } from "react";
import { useRouter } from "next/router";

import { makeStyles } from "@material-ui/styles";
import { Box, IconButton } from "@material-ui/core";

import * as gtm from "@utils/gtm";
import { isEmpty } from "@utils/helpers";
import { IconMenu, IconSearchBig } from "@public/icons";
import { listEventHotel, routeStatic } from "@utils/constants";

import MenuDrawer from "@components/layout/mobile/MenuDrawer";
import DestinationSearchModal from "@components/common/modal/hotel/DestinationSearchModal";

const useStyles = makeStyles((theme) => ({
  headerContainer: {
    height: 48,
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  logoMytour: {
    width: 120,
    height: 32,
  },
}));

const listTypeModal = {
  MODAL_DESTINATION_SEARCH: "MODAL_DESTINATION_SEARCH",
  MODAL_DRAWER_MENU: "MODAL_DRAWER_MENU",
};

const Header = () => {
  const router = useRouter();
  const classes = useStyles();
  const [visibleDrawerType, setVisibleDrawerType] = useState("");

  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };
  const handleSelectedItem = (item) => {
    setVisibleDrawerType("");
    if (item.type === "HOTEL") {
      gtm.addEventGtm(listEventHotel.HotelSearchByName, {
        hotelId: item.hotelId || "",
      });
      router.push({
        pathname: routeStatic.HOTEL_DETAIL.href,
        query: {
          alias: `${item.hotelId}-${item?.name.stringSlug()}.html`,
        },
      });
    } else {
      if (!isEmpty(item.address)) {
        pushGtm(item.address);
      } else {
        pushGtm(item);
      }
      router.push({
        pathname: routeStatic.LISTING_HOTEL.href,
        query: {
          slug: [
            `${item.aliasCode}`,
            `khach-san-tai-${item?.name.stringSlug()}.html`,
          ],
        },
      });
    }
  };

  const pushGtm = (location = {}) => {
    if (!isEmpty(location)) {
      let addressGtm = {};
      if (!isEmpty(location.districtId)) {
        addressGtm = {
          ...addressGtm,
          districtId: location?.districtId || "",
          districtName: location?.districtName || "",
        };
      }
      if (!isEmpty(location.provinceId)) {
        addressGtm = {
          ...addressGtm,
          provinceId: location?.provinceId || "",
          provinceName: location?.provinceName || location?.name || "",
        };
      }
      gtm.addEventGtm(listEventHotel.HotelSearchBySlug, {
        address: addressGtm,
      });
    }
  };

  return (
    <Box className={classes.headerContainer}>
      <IconButton onClick={toggleDrawer(listTypeModal.MODAL_DRAWER_MENU)}>
        <IconMenu />
      </IconButton>
      <img
        src="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_logo_mytour.svg"
        className={classes.logoMytour}
        alt="logo_mytour"
      />
      <IconButton
        onClick={toggleDrawer(listTypeModal.MODAL_DESTINATION_SEARCH)}
      >
        <IconSearchBig />
      </IconButton>
      <MenuDrawer
        open={visibleDrawerType === listTypeModal.MODAL_DRAWER_MENU}
        toggleDrawer={toggleDrawer}
      />
      <DestinationSearchModal
        open={visibleDrawerType === listTypeModal.MODAL_DESTINATION_SEARCH}
        toggleDrawer={toggleDrawer}
        handleSelectedItem={handleSelectedItem}
      />
    </Box>
  );
};

export default Header;
