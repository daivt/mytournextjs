import clsx from "clsx";
import PropTypes from "prop-types";
import { useRouter } from "next/router";
import makeStyles from "@material-ui/styles/makeStyles";
import { Box, Drawer, IconButton } from "@material-ui/core";

import { useSystem } from "@contextProvider/ContextProvider";
import Link from "@src/link/Link";
import { routeStatic, listString } from "@utils/constants";
import {
  IconClose,
  IconBlog,
  IconFavorite,
  IconHome,
  IconOrder,
  IconPhoneContact,
  IconUser,
} from "@public/icons";
import { isEmpty } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  mainDrawerFilter: {
    width: "100vw",
    color: theme.palette.black.black3,
    fontSize: 16,
    height: "100vh",
    overflow: "auto",
  },
  listMenu: {
    padding: "0 16px",
  },
  itemMenu: {
    display: "flex",
    alignItems: "center",
    height: 54,
    borderBottom: "1px solid #EDF2F7",
    textDecoration: "none !important",
    color: theme.palette.black.black3,
  },
  hideBorder: {
    borderBottom: "none",
  },
  infoContact: {
    display: "flex",
    flexDirection: "column",
    paddingLeft: 48,
  },
  paperAnchorLeft: {
    borderRadius: 0,
  },
}));

const listMenu = [
  {
    id: "0",
    name: listString.IDS_MT_TEXT_PAGE_HOME,
    icon: <IconHome />,
    isLink: false,
    code: "home",
  },
  {
    id: "1",
    name: listString.IDS_MT_TEXT_FAVORITE,
    icon: <IconFavorite />,
    link: routeStatic.ACCOUNT_FAVORITES_HOTEL.href,
    code: "favorites",
  },
  {
    id: "2",
    name: listString.IDS_MT_TEXT_ODER,
    icon: <IconOrder />,
    isLink: true,
    link: routeStatic.ACCOUNT_ORDER_BOOKING.href,
    code: "oder",
  },
  {
    id: "3",
    name: listString.IDS_MT_TEXT_TRAVEL_GUIDE,
    icon: <IconBlog />,
    link: routeStatic.BLOG.href,
    isLink: true,
    code: "blog",
  },
  {
    id: "4",
    name: listString.IDS_MT_TEXT_ACCOUNT,
    icon: <IconUser />,
    isLink: true,
    link: routeStatic.ACCOUNT.href,
    code: "account",
  },
  {
    id: "5",
    name: listString.IDS_MT_TEXT_SUPPORT_247,
    icon: <IconPhoneContact />,
    isLink: false,
    code: "support",
  },
];
const MenuDrawer = ({ open = false, toggleDrawer = () => {} }) => {
  const classes = useStyles();
  const router = useRouter();
  const { systemReducer } = useSystem();
  const informationUser = systemReducer.informationUser || {};
  const isLogin = !isEmpty(informationUser);

  const handleClick = (code = "") => {
    switch (code) {
      case "favorites": {
        if (isLogin) {
          router.push({
            pathname: routeStatic.ACCOUNT_FAVORITES_HOTEL.href,
          });
        } else {
          router.push({
            pathname: routeStatic.LOGIN.href,
          });
        }

        break;
      }
    }
  };
  return (
    <Drawer
      anchor="left"
      open={open}
      onClose={toggleDrawer("")}
      classes={{ paperAnchorLeft: classes.paperAnchorLeft }}
    >
      <Box className={classes.mainDrawerFilter}>
        <IconButton onClick={toggleDrawer("")}>
          <IconClose />
        </IconButton>
        <Box className={classes.listMenu}>
          {listMenu.map((el, index) => {
            if (el.isLink) {
              if (el.code === "blog") {
                return (
                  <a
                    className={clsx(
                      classes.itemMenu,
                      index === listMenu.length - 1 && classes.hideBorder
                    )}
                    key={el.id}
                    href={el.link}
                  >
                    <Box>{el.icon}</Box>
                    <Box component="span" pl={2}>
                      {el.name}
                    </Box>
                  </a>
                );
              }
              return (
                <Link
                  className={clsx(
                    classes.itemMenu,
                    index === listMenu.length - 1 && classes.hideBorder
                  )}
                  key={el.id}
                  href={el.link}
                >
                  <Box>{el.icon}</Box>
                  <Box component="span" pl={2}>
                    {el.name}
                  </Box>
                </Link>
              );
            }
            return (
              <Box
                className={clsx(
                  classes.itemMenu,
                  index === listMenu.length - 1 && classes.hideBorder
                )}
                key={el.id}
                onClick={() => handleClick(el.code)}
              >
                <Box>{el.icon}</Box>
                <Box component="span" pl={2}>
                  {el.name}
                </Box>
              </Box>
            );
          })}
        </Box>
        <Box className={classes.infoContact}>
          <Box component="span" pt={1}>
            Hà Nội:{" "}
            <Box component="span" fontWeight={600}>
              {listString.IDS_MT_TEXT_HOTLINE_HN}
            </Box>
          </Box>
          <Box component="span" pt={1}>
            TP Hồ Chí Minh:{" "}
            <Box component="span" fontWeight={600}>
              {listString.IDS_MT_TEXT_HOTLINE_HCM}
            </Box>
          </Box>
          <Box component="span" pt={1}>
            Hỗ trợ đặt vé máy bay:{" "}
            <Box component="span" fontWeight={600}>
              {listString.IDS_MT_TEXT_HOTLINE_SUPPORT_ODER_FLIGHT}
            </Box>
          </Box>
        </Box>
      </Box>
    </Drawer>
  );
};

export default MenuDrawer;

MenuDrawer.propTypes = {
  open: PropTypes.bool.isRequired,
  toggleDrawer: PropTypes.func,
};
