import Head from "next/head";
import cookie from "js-cookie";
import { memo, useEffect } from "react";
import { Box } from "@material-ui/core";

import { TOKEN, LAST_HOTEL_FAVORITES } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import { validateAccessToken, addFavorite } from "@api/user";
import * as actionTypes from "@contextProvider/system/ActionTypes";
import { useSystem, useDispatchSystem } from "@contextProvider/ContextProvider";

import Header from "@components/layout/mobile/Header";
import Footer from "@components/layout/mobile/Footer";
import HeaderBack from "@components/layout/mobile/HeaderBack";
import LoadingApp from "@components/common/loading/LoadingApp";

const Layout = ({
  children,
  isHeader = false,
  isFooter = false,
  isHeaderBack = false,
  isShowTitleInit = false,
  titleHeaderBack = "",
  csrfToken = "kh1ez4WH3sozbpbLC7tOumNr81Ut9n2kVwAu1g1S",
  iconOgImage = "https://mytour.vn/themes/images/logo-ss-facebook.png",
  title = "Đặt phòng khách sạn, vé máy bay giá rẻ hàng đầu Việt Nam | Mytour.vn",
  iconImageWeb = "https://staticproxy.mytourcdn.com/0x0,q90/themes/images/favicon.ico",
  description = "Đặt phòng khách sạn giá rẻ trên Mytour. Đặt càng sát ngày, giá càng tốt. Đặt phòng khuyến mãi giá rẻ nhất ở Việt Nam và các nước khác chỉ trong 3 bước đơn giản. Giá trung thực, không phí ẩn!",
  keywords = "khách sạn, đặt phòng khách sạn, đặt phòng giá rẻ, đặt khách sạn hạng sang giá tốt, đặt khách sạn sát ngày giá tốt, đặt phòng khách sạn thuận tiện, hỗ trợ dịch vụ khách hàng 24/7 đặt phòng khách sạn",
}) => {
  const { systemReducer } = useSystem();
  const dispatch = useDispatchSystem();
  useEffect(() => {
    const informationUser = systemReducer.informationUser || {};
    if (isEmpty(informationUser) && !isEmpty(cookie.get(TOKEN))) {
      fetUserInfo();
    }
  }, []);

  const fetUserInfo = async () => {
    try {
      const { data } = await validateAccessToken();
      if (data.code === 200) {
        dispatch({
          type: actionTypes.SET_INFORMATION_USER,
          payload: data.data,
        });
        const listHotelFavorites =
          JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
        if (!isEmpty(listHotelFavorites)) {
          handleAddFavorite(listHotelFavorites);
        }
      } else {
        cookie.remove(TOKEN);
      }
    } catch (error) {
      cookie.remove(TOKEN);
    }
  };

  const handleAddFavorite = async (listHotelFavorites = []) => {
    try {
      localStorage.removeItem(LAST_HOTEL_FAVORITES);
      listHotelFavorites.forEach((el) => {
        addFavorite({ hotelId: el });
      });
    } catch (error) {}
  };

  return (
    <Box height="auto" bgcolor="white.main" minHeight="100vh">
      <Head>
        <title>{title}</title>
        <meta
          name="viewport"
          content="initial-scale=1.0, maximum-scale=1.0, width=device-width, user-scalable=no"
        />
        <meta name="description" content={description} />
        <meta name="keywords" content={keywords} />
        <meta name="csrf-token" content={csrfToken} />
        <meta httpEquiv="x-dns-prefetch-control" content="on" />
        <link rel="dns-prefetch" href="https://mytourcdn.com/" />
        <meta name="robots" content="index, follow" />
        <link rel="alternate" hrefLang="vi" href="https://mytour.vn/" />
        <link href={iconImageWeb} rel="icon" type="image/x-icon" />
        <meta property="al:ios:app_name" content="Mytour.vn" />
        <meta property="al:ios:app_store_id" content="1149730203" />
        <meta property="al:android:app_name" content="Mytour.vn" />
        <meta property="al:android:package" content="vn.mytour.apps.android" />
        <meta property="fb:app_id" content="857393964278669" />
        <meta property="fb:pages" content="180719541988518" />
        <meta property="og:image" content={iconOgImage} />
        <meta property="og:url" content="https://mytour.vn" />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="Mytour.vn" />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
      </Head>
      {isHeader && <Header />}
      {isHeaderBack && (
        <HeaderBack
          titleHeaderBack={titleHeaderBack}
          isShowTitleInit={isShowTitleInit}
        />
      )}
      <main>{children}</main>
      {isFooter && <Footer />}
      <LoadingApp />
    </Box>
  );
};

export default memo(Layout);
