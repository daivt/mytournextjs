import { Box } from "@material-ui/core";
import MuiAccordion from "@material-ui/core/Accordion";
import { makeStyles, withStyles } from "@material-ui/styles";
import MuiAccordionSummary from "@material-ui/core/AccordionSummary";
import MuiAccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import { prefixUrlIcon, listIcons, listString } from "@utils/constants";

import Image from "@src/image/Image";
import Link from "@src/link/Link";

const useStyles = makeStyles((theme) => ({
  footerContainer: {
    backgroundColor: theme.palette.gray.grayLight26,
    padding: "30px 16px",
    display: "flex",
    flexDirection: "column",
    color: theme.palette.black.black3,
    fontSize: 14,
    lineHeight: "18px",
  },
  logoMytour: {
    width: 120,
    height: 32,
  },
  imageCompany: {
    width: "100%",
    height: 32,
  },
  linkPolicyInfo: {
    color: theme.palette.gray.grayDark8,
    "&:hover": {
      textDecoration: "none",
      color: theme.palette.primary.main,
    },
  },
  linkSocial: {
    paddingLeft: 16,
  },
  socialIcon: {
    height: 32,
    width: 32,
  },
}));

const Accordion = withStyles({
  root: {
    borderBottom: "2px solid #EDF2F7",
    boxShadow: "none",
    "&:before": {
      display: "none",
    },
  },
  expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
  root: {
    backgroundColor: "#F7FAFC",
    marginBottom: -1,
    minHeight: 56,
    padding: 0,
    "&$expanded": {
      minHeight: 56,
    },
  },
  content: {
    "&$expanded": {
      margin: "12px 0",
    },
  },
  expanded: {},
})(MuiAccordionSummary);

const AccordionDetails = withStyles((theme) => ({
  root: {
    padding: 0,
    backgroundColor: "#F7FAFC",
    display: "flex",
    flexDirection: "column",
  },
}))(MuiAccordionDetails);

const companyInfo = [
  "Hà Nội: (024) 7109 9999",
  "TP Hồ Chí Minh: (024) 7109 9998",
  "Hỗ trợ đặt vé máy bay: 1900 2083",
  "Email: hotro@mytour.vn",
  "Văn phòng Hà Nội: Tầng 11, Tòa Peakview, 36 Hoàng Cầu, Đống Đa",
  "Văn phòng HCM: Tầng 6, Tòa Nhà Central Park, 117 Nguyễn Du, Q.1",
  "Mytour Store: 168 Xã Đàn, Đống Đa, Hà Nội",
];

const policyInfo = [
  {
    title: "Chính sách và quy định chung",
    link: "/news/135152-chinh-sach-va-quy-dinh-chung.html",
  },
  {
    title: "Quy định về thanh toán",
    link: "/news/135633-quy-dinh-ve-thanh-toan.html",
  },
  {
    title: "Quy định về xác nhận thông tin đặt phòng",
    link: "/news/135634-quy-dinh-ve-xac-nhan-thong-tin-dat-phong.html",
  },
  {
    title: "Chính sách về hủy đặt phòng và hoàn trả tiền",
    link: "/news/135154-chinh-sach-huy-phong-va-hoan-tien.html",
  },
  {
    title: "Chính sách bảo mật thông tin",
    link:
      "/news/135636-chinh-sach-bao-mat-thong-tin-danh-cho-website-tmdt.html",
  },
  {
    title: "Quy chế hoạt động",
    link: "/news/135155-quy-che-hoat-dong.html",
  },
  {
    title: "Chính sách bảo mật",
    link: "/news/135156-chinh-sach-bao-mat-thong-tin-danh-cho-san-gdtmdt.html",
  },
  {
    title: "Quy trình giải quyết tranh chấp, khiếu nại",
    link: "/news/135420-quy-trinh-giai-quyet-tranh-chap-khieu-nai.html",
  },
];

const customerInfo = [
  {
    title: "Đăng nhập HMS",
    link: "https://hms.mytour.vn/",
  },
  {
    title: "Tuyển dụng",
    link: "https://career.mytour.vn/",
  },
  {
    title: "Liên hệ",
    link: "/help/30-lien-he.html",
  },
];

const socialList = [
  {
    icon: `${prefixUrlIcon}${listIcons.iconFacebook}`,
    link: "https://www.facebook.com/mytour.vn/",
  },
  {
    icon: `${prefixUrlIcon}${listIcons.iconMessenger}`,
    link: "https://plus.google.com/+MytourVndulichtietkiem",
  },
  {
    icon: `${prefixUrlIcon}${listIcons.iconInstagram}`,
    link: "https://www.instagram.com/mytour.vn/",
  },
];

const Footer = () => {
  const classes = useStyles();

  return (
    <Box className={classes.footerContainer}>
      <img
        src="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_logo_mytour.svg"
        className={classes.logoMytour}
        alt="logo_mytour"
      />
      <Box
        component="span"
        fontWeight={600}
        p="14px 0 8px"
      >{`Công ty cổ phần du lịch Việt Nam VNTravel`}</Box>
      {companyInfo.map((el, index) => (
        <Box
          component="span"
          fontSize={12}
          color="gray.grayDark8"
          pb={2 / 8}
          key={index.toString()}
        >
          {el}
        </Box>
      ))}
      <Accordion square>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1d-footer"
          id="panel1d-footer"
        >
          <Box component="span" fontWeight={600}>
            Chính sách & Quy định
          </Box>
        </AccordionSummary>
        <AccordionDetails>
          {policyInfo.map((el, i) => (
            <Link
              href={el.link}
              target="_blank"
              className={classes.linkPolicyInfo}
              key={i.toString()}
            >
              <Box component="span" fontSize={14} lineHeight="36px">
                {el.title}
              </Box>
            </Link>
          ))}
        </AccordionDetails>
      </Accordion>
      <Accordion square>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1d-footer"
          id="panel1d-footer"
        >
          <Box component="span" fontWeight={600}>
            Khách hàng và đối tác
          </Box>
        </AccordionSummary>
        <AccordionDetails>
          {customerInfo.map((el, i) => (
            <Link
              href={el.link}
              target="_blank"
              className={classes.linkPolicyInfo}
              key={i.toString()}
            >
              <Box component="span" fontSize={14} lineHeight="36px">
                {el.title}
              </Box>
            </Link>
          ))}
        </AccordionDetails>
      </Accordion>
      <Box
        display="flex"
        alignItems="center"
        flexDirection="row"
        justifyContent="space-between"
        p="12px 0"
      >
        <Box component="span" fontWeight={600}>
          Kết nối với Mytour.vn
        </Box>
        <Box display="flex" flexDirection="row">
          {socialList.map((el, i) => (
            <Link
              href={el.link}
              target="_blank"
              className={classes.linkSocial}
              key={i.toString()}
            >
              <Image
                src={el.icon}
                className={classes.socialIcon}
                alt="logo_mytour"
              />
            </Link>
          ))}
        </Box>
      </Box>
      <Box component="span" fontSize={12} color="gray.grayDark8">
        {listString.IDS_MT_MYTOUR_DESCRIPTION}
      </Box>
      <Box p="14px 4px" width="100%">
        <img
          src="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_company_group.svg"
          className={classes.imageCompany}
          alt="vntravel_company_logo"
        />
      </Box>

      <Box component="span" fontSize={12} color="gray.grayDark8">
        {listString.IDS_MT_COPYRIGHT}
      </Box>
    </Box>
  );
};

export default Footer;
