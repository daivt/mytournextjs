import clsx from "clsx";
import { useRouter } from "next/router";
import { makeStyles } from "@material-ui/styles";
import { useRef, useState, useEffect } from "react";
import { Box, IconButton } from "@material-ui/core";

import { IconBack } from "@public/icons";

const useStyles = makeStyles((theme) => ({
  container: {
    height: 48,
    position: "relative",
    backgroundColor: theme.palette.white.main,
  },
  searchWrapper: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    height: 48,
    position: "relative",
    color: theme.palette.black.black3,
    fontSize: 16,
    fontWeight: 600,
  },
  headerContent: {
    width: "100%",
    position: "relative",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  sticky: {
    zIndex: 10,
    top: 0,
    left: 0,
    position: "fixed",
    backgroundColor: theme.palette.white.main,
    transition: "all 0.4s ease-in-out",
    boxShadow: "0 0 2px 0 rgba(0, 0, 0, 0.1)",
  },
  btnBack: {
    position: "absolute",
    left: 0,
  },
  textHeader: {
    opacity: 0,
    transition: "all 0.4s",
    textAlign: "center",
  },
  textHeaderShow: {
    opacity: 1,
    transition: "all 0.4s",
  },
}));
const HeaderBack = ({ titleHeaderBack = "", isShowTitleInit = false }) => {
  const classes = useStyles();
  const ref = useRef(null);
  const router = useRouter();
  const [isSticky, setSticky] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", () => handleScroll);
    }; // eslint-disable-next-line
  }, []);

  const handleScroll = () => {
    if (ref && ref.current) {
      if (ref.current.getBoundingClientRect().top <= -20) {
        setSticky(true);
      } else {
        setSticky(false);
      }
    }
  };
  const _handleBackRoute = () => {
    router.back();
  };
  return (
    <div className={classes.container} ref={ref}>
      <Box className={clsx(classes.searchWrapper, isSticky && classes.sticky)}>
        <Box className={classes.headerContent}>
          <IconButton className={classes.btnBack} onClick={_handleBackRoute}>
            <IconBack />
          </IconButton>
          <Box
            component="span"
            className={clsx(
              classes.textHeader,
              (isSticky || isShowTitleInit) && classes.textHeaderShow
            )}
          >
            {titleHeaderBack}
          </Box>
        </Box>
      </Box>
    </div>
  );
};

export default HeaderBack;
