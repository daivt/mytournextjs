import { FormControl, FormHelperText, InputLabel } from "@material-ui/core";
import React from "react";
import { RED } from "../assets/theme/colors";
import { BootstrapInput, redMark, useStylesForm } from "./Form";

const FormControlTextField = (props) => {
  const classes = useStylesForm();
  const {
    id,
    label,
    formControlStyle,
    errorMessage,
    optional,
    focused,
    value,
    fullWidth,
    helpText,
    className,
    disableError,
    ...rest
  } = props;
  return (
    <FormControl
      focused={focused}
      className={classes.margin}
      style={{ minWidth: 200, ...formControlStyle }}
      fullWidth
    >
      {label && (
        <InputLabel shrink htmlFor={id}>
          {label}
          {!optional && (
            <>
              &nbsp;
              {redMark}
            </>
          )}
        </InputLabel>
      )}
      <BootstrapInput
        className={className}
        id={id}
        value={value || ""}
        {...rest}
        error={focused ? false : !!errorMessage}
      />
      <FormHelperText
        id={id}
        style={{
          minHeight: 20,
          fontSize: helpText ? 12 : 14,
          color: errorMessage ? RED : undefined,
        }}
      >
        {!disableError && (errorMessage || helpText)}
      </FormHelperText>
    </FormControl>
  );
};

export default React.memo(FormControlTextField);
