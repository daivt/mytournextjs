import React from "react";
import { useField, useFormikContext } from "formik";
import FormControlTextField from "./FormControlTextField";
import SingleSelect from "./SingleSelect";

export const FieldTextContent = React.memo((props) => {
  const [field, meta] = useField(props);
  const { submitCount } = useFormikContext();
  return (
    <FormControlTextField
      {...field}
      {...props}
      errorMessage={submitCount > 0 && meta.error ? meta.error : undefined}
    />
  );
});

export const FieldSelectContent = (props) => {
  const [field, meta] = useField(props);
  const { submitCount } = useFormikContext();
  return (
    <SingleSelect
      {...field}
      {...props}
      errorMessage={submitCount > 0 && meta.error ? meta.error : undefined}
    />
  );
};
