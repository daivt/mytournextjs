import React from "react";
import { CircularProgress } from "@material-ui/core";

const LoadingIcon = (props) => {
  const { children, loadingColor, style, ...rest } = props;

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        padding: 16,
        height: 320,
        ...style,
      }}
    >
      <CircularProgress
        size={48}
        color={
          loadingColor || loadingColor === "default" ? "primary" : rest.color
        }
        {...rest}
      />
    </div>
  );
};

export default LoadingIcon;
