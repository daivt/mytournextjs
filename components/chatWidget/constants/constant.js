export const ACCESS_TOKEN = "token";
export const SUCCESS_CODE = 200;
export const DEVICE_ID = "DEVICE_ID";
export const UUID = "UUID";
export const ANONYMOUS = "ANONYMOUS";
export const NORMAL_ACCOUNT = "NORMAL_ACCOUNT";
export const ACCOUNT = "ACCOUNT";
export const STATUS_MESSAGE = {
  SENT: "SENT",
  DELIVERED: "DELIVERED",
  SEEN: "SEEN",
  TYPING: "TYPING",
  STOPPED_TYPING: "STOPPED_TYPING",
};
export const TYPE_MESSAGE = {
  TEXT: "TEXT",
  // IMAGE: 'IMAGE',
  FILE: "FILE",
  // AUDIO: 'AUDIO',
  // VIDEO: 'VIDEO',
  // GIF: 'GIF',
  STICKER: "STICKER",
  EVENT: "EVENT",
};
export const TYPE_EVENT = {
  REQUEST_VOTE: "REQUEST_VOTE",
  VOTED: "VOTED",
  ADD_MEMBER: "ADD_MEMBER",
  REMOVE_MEMBER: "REMOVE_MEMBER",
  NEW_TICKET: "NEW_TICKET",
  WELCOME: "WELCOME",
};
export const SOCKET_STATUS = {
  CONNECTING: 0,
  OPEN: 1,
  CLOSING: 2,
  CLOSED: 3,
};
export const HOUR_MINUTE = "HH:mm";
export const DATE_FORMAT = "DD/MM/YYYY";
export const DATE_FORMAT_ALL = `${DATE_FORMAT} ${HOUR_MINUTE}`;
export const PAGE_SIZE = 15;
