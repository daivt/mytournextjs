import { TOKEN } from "@utils/constants";
import axios from "axios";
import { get } from "js-cookie";
import { sha256 } from "js-sha256";
import JSONbig from "json-bigint";
import { configs } from "../../constants/config";
import { UUID, ACCESS_TOKEN } from "../../constants/constant";
import { isEmpty } from "./helpers";
import { getAppHash } from "../../../../utils/helpers";

const IS_SERVER = typeof window === "undefined";
const timeStamp = new Date().getTime();
const AppHash = Buffer.from(
  sha256(
    `${timeStamp / 1000 - ((timeStamp / 1000) % 300)}:${configs().APP_KEY}`
  ),
  "hex"
).toString("base64");

const request = axios.create({
  baseURL: configs().BASE_URL,
  headers: {
    "accept-language": "vi",
    "Content-Type": "application/json",
    // "login-token": `${get(ACCESS_TOKEN) ||
    //   localStorage.getItem(ACCESS_TOKEN) ||
    //   ""}`,
    // "device-id": `${localStorage.getItem(UUID) || ""}`,
    version: configs().VERSION,
    appHash: getAppHash(),
    appId: configs().APP_ID,
  },
});

request.interceptors.request.use(
  (config) => {
    let tempConfig = { ...config };
    if (!IS_SERVER) {
      tempConfig = {
        ...tempConfig,
        headers: {
          ...tempConfig.headers,
          "login-token": `${get(TOKEN) || localStorage.getItem(ACCESS_TOKEN)}`,
          "device-id": `${localStorage.getItem(UUID) || ""}`,
        },
      };
    }
    if (tempConfig?.url.includes("/account/login")) {
      delete tempConfig?.headers["device-id"];
      delete tempConfig?.headers["login-token"];
    }
    if (isEmpty(localStorage.getItem(ACCESS_TOKEN)) && isEmpty(get(TOKEN))) {
      delete tempConfig?.headers["login-token"];
    } else {
      delete tempConfig?.headers["device-id"];
    }
    const currentTime = new Date().getTime();
    const appHash = Buffer.from(
      sha256(
        `${currentTime / 1000 - ((currentTime / 1000) % 300)}:${
          configs().APP_KEY
        }`
      ),
      "hex"
    ).toString("base64");
    return {
      ...tempConfig,
      headers: {
        ...tempConfig?.headers,
        version: configs().VERSION,
        appHash: getAppHash(),
        appId: configs().APP_ID,
      },
    };
  },
  (error) => Promise.reject(error)
);

request.interceptors.response.use(
  (response) => {
    const res = JSONbig.parse(response?.request?.response);
    // if (res.code === 3003) {
    //   window.alert(
    //     "Hệ thống yêu cầu cài đặt thời gian chính xác để thực hiện tính năng chat hỗ trợ. Quý khách vui lòng mở phần Cài đặt của thiết bị này và chuyển Ngày Giờ sang chế độ Tự động"
    //   );
    // }
    return res;
  },
  (error) => {
    return Promise.reject(error.response);
  }
);

const api = (options) => {
  return request({
    baseURL: configs().BASE_URL,
    ...options,
    headers: { ...options.headers },
  });
};

export default api;
