import React from "react";
import { IntlProvider } from "react-intl";
// import 'moment/locale/vi';
import vi from "../intl/vi.json";
// import en from '../intl/en.json';
// moment.locale('vi');

const LocaleProvider = (props) => {
  const localeIntl = {
    locale: "vi",
    messages: vi,
  };
  return <IntlProvider {...localeIntl}>{props.children}</IntlProvider>;
};

export default LocaleProvider;
