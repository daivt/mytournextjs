import React, { useState } from "react";
import JSONbig from "json-bigint";
import moment from "moment";
import DescriptionOutlinedIcon from "@material-ui/icons/DescriptionOutlined";
import { Typography } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import {
  GREEN,
  GREEN_300,
  GREY_100,
  GREY_600,
  GREY_900,
  WHITE,
} from "../../assets/theme/colors";
import {
  DATE_FORMAT_ALL,
  HOUR_MINUTE,
  TYPE_MESSAGE,
  TYPE_EVENT,
  ACCOUNT,
  SUCCESS_CODE,
} from "../../constants/constant";
import { formatBytes, isEmpty } from "../../utils/helpers/helpers";
import { Row } from "../../common/Elements";
import { AvatarIcon } from "../../assets/icons";
import ChatEvaluate from "./ChatEvaluate";
import { actionGetAccount } from "../ChatAction";
import ChatCustomerInfo from "./ChatCustomerInfo";
import WelcomeMessage from "./WelcomeMessage";

const ChatItem = (props) => {
  const {
    align,
    item,
    isLast,
    isFirst,
    chatGroup,
    socket,
    accountInfo,
    fetchCurrentTicket,
  } = props;
  const [users, setUsers] = useState(
    JSONbig.parse(localStorage.getItem(ACCOUNT) || "{}")
  );

  const getTime = (timeInfo) => {
    const time = moment(new Date(timeInfo));
    if (time.isSame(moment(), "days")) return time.format(HOUR_MINUTE);
    return time.format(DATE_FORMAT_ALL);
    // return new Date(timeInfo).toLocaleString('en-US');
  };
  const getAccountDetail = async (userId) => {
    try {
      const res = await actionGetAccount(userId.filter((v) => v));
      if (res?.code === SUCCESS_CODE) {
        let temp = { ...users };
        (res?.data?.items || []).forEach((userItem) => {
          temp = { ...users, [userItem?.id]: userItem };
        });
        localStorage.setItem(ACCOUNT, JSONbig.stringify(temp));
        setUsers(temp);
      }
    } catch (error) {}
  };
  const renderContent = () => {
    if (item?.contentType === TYPE_MESSAGE.TEXT) {
      return (
        <Typography
          variant="body2"
          style={{
            backgroundColor: align === "left" ? "#EDF2F7" : "#00B6F3",
            color: align === "left" ? GREY_900 : WHITE,
            maxWidth: "calc(100% - 120px)",
            marginLeft: 36,
            padding: "8px 12px",
            borderRadius: 16,
            wordBreak: "break-word",
            fontSize: 14,
            whiteSpace: "pre-line",
          }}
        >
          {item?.content}
        </Typography>
      );
    }
    if (item?.contentType === TYPE_MESSAGE.FILE) {
      const contentMessage = JSON.parse(item?.content);
      return (
        <Row
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: align === "left" ? "flex-start" : "flex-end",
            marginLeft: align === "left" ? 36 : 0,
          }}
        >
          {!isEmpty(contentMessage) &&
            contentMessage.map((el, i) => {
              if (el?.contentType) {
                const isImage = el?.contentType.toLowerCase().includes("image");
                if (isImage) {
                  return (
                    <a
                      href={el.url}
                      download={el?.fileName || el?.url.split("/").pop()}
                      target="_blank"
                      rel="noopener noreferrer"
                      key={el?.url}
                    >
                      <img
                        alt=""
                        src={
                          el?.extension
                            ? el?.extension?.thumbnailUrl?.url
                            : el?.url
                        }
                        style={{
                          width: "100%",
                          maxWidth: 280,
                          marginBottom: 4,
                        }}
                      />
                    </a>
                  );
                }
              }
              return (
                <a
                  key={el.url}
                  href={el.url}
                  target="_blank"
                  download={el?.fileName || el.url.split("/").pop()}
                  rel="noopener noreferrer"
                  style={{ textDecoration: "none" }}
                >
                  <Row
                    style={{
                      marginTop: 4,
                      padding: "0 4px",
                      borderRadius: 16,
                      backgroundColor: align === "left" ? "#EDF2F7" : "#00B6F3",
                      color: align === "left" ? GREY_900 : WHITE,
                    }}
                  >
                    <DescriptionOutlinedIcon
                      style={{
                        width: 80,
                        height: 80,
                        color: align === "left" ? GREY_600 : GREY_100,
                      }}
                    />
                    <Row
                      style={{
                        flexDirection: "column",
                        alignItems: "flex-start",
                      }}
                    >
                      <span
                        style={{
                          maxWidth: 150,
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                          whiteSpace: "nowrap",
                        }}
                        title={el?.fileName || el.url.split("/").pop()}
                      >
                        {el?.fileName || el.url.split("/").pop()}
                      </span>
                      {el.size && <span>{formatBytes(el.size)}</span>}
                    </Row>
                  </Row>
                </a>
              );
            })}
        </Row>
      );
    }
    if (item?.contentType === TYPE_MESSAGE.EVENT) {
      const contentEvent = JSONbig.parse(item?.content);
      if (contentEvent?.type === TYPE_EVENT.REQUEST_VOTE) {
        return (
          <ChatEvaluate
            chatGroup={chatGroup}
            socket={socket}
            item={contentEvent}
            messageId={`${item?.id}`}
            fetchCurrentTicket={fetchCurrentTicket}
          />
        );
      }
      if (contentEvent?.type === TYPE_EVENT.VOTED) {
        return (
          <ChatEvaluate
            chatGroup={chatGroup}
            socket={socket}
            isVoted
            item={contentEvent}
            messageId={`${item?.id}`}
            fetchCurrentTicket={fetchCurrentTicket}
          />
        );
      }
      if (
        contentEvent?.type === TYPE_EVENT.ADD_MEMBER ||
        contentEvent?.type === TYPE_EVENT.REMOVE_MEMBER ||
        contentEvent?.type === TYPE_EVENT.WELCOME
      ) {
        let text = "";
        const totalUserItem = [...contentEvent?.controlled];
        const temp = totalUserItem.filter((u) => isEmpty(users[`${u}`]));
        if (!isEmpty(temp)) getAccountDetail(temp);
        const tempControlled = contentEvent?.controlled.map(
          (v) => users[`${v}`]?.name
        );
        if (contentEvent?.type === TYPE_EVENT.WELCOME) {
          return (
            <WelcomeMessage
              employeeName={tempControlled.join(", ")}
              accountInfo={accountInfo}
              timestamp={item?.createdTime}
              avatarUrl={users[`${contentEvent?.controlled[0]}`]?.profilePhoto}
            />
          );
        }
        text =
          contentEvent?.type === TYPE_EVENT.ADD_MEMBER
            ? `${tempControlled.join(", ")} đã được thêm vào cuộc trò chuyện`
            : `${tempControlled.join(", ")} đã rời khỏi cuộc trò chuyện`;
        return (
          <Typography
            variant="body2"
            component="p"
            style={{
              color: GREY_600,
              width: "100%",
              textAlign: "center",
              margin: "12px 0",
            }}
          >
            {text}
          </Typography>
        );
      }
      if (contentEvent?.type === TYPE_EVENT.NEW_TICKET) {
        return (
          <ChatCustomerInfo
            item={contentEvent?.data}
            accountInfo={accountInfo}
          />
        );
      }
      return null;
    }
    return null;
  };
  if (align === "left" && item?.contentType !== TYPE_MESSAGE.EVENT) {
    if (isEmpty(users[`${item?.sender?.id}`])) {
      getAccountDetail([item?.sender?.id]);
    }
    return (
      <>
        {isFirst ? (
          <Row style={{ marginBottom: 4 }}>
            {users[`${item?.sender?.id}`]?.profilePhoto ? (
              <img
                src={users[`${item?.sender?.id}`]?.profilePhoto}
                alt=""
                style={{ maxWidth: 24, maxHeight: 24 }}
              />
            ) : (
              <AvatarIcon />
            )}
            <Typography
              variant="body2"
              style={{ marginLeft: 4, color: GREY_600, fontSize: 14 }}
            >
              {users[`${item?.sender?.id}`]?.name}
              &nbsp;
              {getTime(item?.createdTime)}
            </Typography>
          </Row>
        ) : (
          <Row style={{ marginBottom: 4 }} />
        )}
        <Row style={{ marginBottom: 4 }}>{renderContent()}</Row>
      </>
    );
  }
  return (
    <>
      {isFirst ? (
        <Row style={{ marginBottom: 4, justifyContent: "flex-end" }}>
          <Typography
            variant="body2"
            style={{ marginLeft: 4, color: GREY_600 }}
          >
            {getTime(Number(item?.createdTime))}
          </Typography>
        </Row>
      ) : (
        <Row style={{ marginBottom: 4, justifyContent: "flex-end" }} />
      )}
      <Row style={{ justifyContent: "flex-end" }}>{renderContent()}</Row>
      {!isEmpty(item) && isLast && item?.contentType !== TYPE_MESSAGE.EVENT && (
        <Row style={{ marginBottom: 4, justifyContent: "flex-end" }}>
          <Typography
            variant="body2"
            style={{ marginLeft: 4, color: GREY_600 }}
          >
            <FormattedMessage
              id={`IDS_CHAT_${
                item?.status === "SEEN"
                  ? "DELIVERED"
                  : item?.status || "DELIVERED"
              }`}
            />
          </Typography>
        </Row>
      )}
    </>
  );
};

export default ChatItem;
