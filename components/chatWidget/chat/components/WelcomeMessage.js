import React from "react";
import { Typography, Box } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import moment from "moment";
import { DATE_FORMAT_ALL, HOUR_MINUTE } from "../../constants/constant";
import { GREY_100, GREY_600 } from "../../assets/theme/colors";
import { Row } from "../../common/Elements";
import { HandIcon, AvatarIcon } from "../../assets/icons";

const WelcomeMessage = (props) => {
  const { accountInfo, employeeName, timestamp, avatarUrl } = props;
  const getTime = () => {
    const time = timestamp ? moment(new Date(timestamp)) : moment();
    if (time.isSame(moment(), "days")) return time.format(HOUR_MINUTE);
    return time.format(DATE_FORMAT_ALL);
  };
  return (
    <Row
      style={{
        marginBottom: 12,
        flexDirection: "column",
        alignItems: "flex-start",
      }}
    >
      <Row>
        {avatarUrl ? (
          <img src={avatarUrl} alt="" style={{ maxWidth: 24, maxHeight: 24 }} />
        ) : (
          <AvatarIcon />
        )}
        <Typography
          variant="body2"
          style={{ marginLeft: 4, color: GREY_600, fontSize: 14 }}
        >
          {employeeName || ""}
          &nbsp;
          {getTime()}
        </Typography>
      </Row>
      <Row
        style={{
          marginBottom: 12,
          flexDirection: "column",
          alignItems: "flex-start",
        }}
      >
        <Box
          style={{
            backgroundColor: GREY_100,
            maxWidth: "calc(100% - 120px)",
            marginLeft: 36,
            padding: "8px 12px",
            borderRadius: 8,
          }}
        >
          <HandIcon style={{ marginBottom: 8 }} />
          <Typography
            variant="body2"
            component="p"
            style={{ fontSize: 14, lineHeight: "17px" }}
          >
            <FormattedMessage id="IDS_CHAT_WELCOME" />
            &nbsp;
            <b>{accountInfo?.name || "Quý khách"}!</b>
            <br />
            {/* <FormattedMessage
            id="IDS_CHAT_WELCOME_MESSAGE"
            values={{ name: employeeName || "" }}
          /> */}
            <span>
              Em là <b>{employeeName || "Mytour CSKH"}</b>, chuyên viên chăm sóc
              khách hàng. Em có thể giúp gì được cho anh/chị?
            </span>
          </Typography>
        </Box>
      </Row>
    </Row>
  );
};

export default WelcomeMessage;
