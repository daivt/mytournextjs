import React, { useState } from "react";
import { Button, InputBase, Typography } from "@material-ui/core";
import DescriptionOutlinedIcon from "@material-ui/icons/DescriptionOutlined";
import { FormattedMessage, useIntl } from "react-intl";
import { SUCCESS_CODE } from "../../constants/constant";
import { GREY_600, RED } from "../../assets/theme/colors";
import { Row } from "../../common/Elements";
import { isEmpty } from "../../utils/helpers/helpers";
import {
  SendIcon,
  AttachFileIcon,
  AddImageIcon,
  CloseFillIcon,
} from "../../assets/icons";
import { actionUploadFile } from "../ChatAction";

const ChatInput = (props) => {
  const intl = useIntl();
  const {
    type,
    fileUploads,
    isCloseTicket,
    message,
    sendMessage,
    handleTyping,
    handleDeleteFile,
    setMessage,
    setFileUploads,
    setType,
  } = props;
  const [uploadError, setError] = useState(false);

  const handleKeyPress = (e) => {
    if (e.keyCode === 13) sendMessage();
  };
  const handleUploadFile = async (e) => {
    const { files } = e.target;
    if (files && files.length > 0) {
      const temp = [];
      const fileList = Object.values(e.target.files);
      const fileUp = fileList.filter((v) => v?.size <= 25 * 1024 * 1024);
      if (fileList.length > fileUp.length) setError(true);
      else setError(false);
      if (!isEmpty(fileUp)) {
        fileUp.forEach((file, idx) => {
          if (fileUploads.length + idx < 5) {
            const formData = new FormData();
            formData.append("file", file);
            formData.append("config", "CHAT-FILE");
            temp.push(actionUploadFile(formData));
          } else {
            setError(true);
          }
        });
        const res = await Promise.all(temp);
        const fileUploaded = [];
        res.forEach((v) => {
          if (v?.code === SUCCESS_CODE) fileUploaded.push(v?.data);
        });
        setFileUploads([...fileUploads, ...fileUploaded]);
      }
    }
  };
  return (
    <>
      {type === "welcome" && (
        <Row>
          <Button
            style={{
              width: "100%",
              height: 36,
              backgroundColor: "#ff1284",
              margin: 12,
            }}
            type="submit"
            variant="contained"
            color="secondary"
            disableElevation
            onClick={() => setType("info")}
          >
            <Typography variant="subtitle2" style={{ fontSize: 14 }}>
              <FormattedMessage id="IDS_CHAT_NOW" />
            </Typography>
          </Button>
        </Row>
      )}
      {!isEmpty(fileUploads) && type !== "welcome" && (
        <div className="files-container">
          {fileUploads.map((el, index) => (
            <div className="file-wrapper" key={el?.url}>
              {el?.contentType.includes("image") ? (
                <img
                  src={
                    el?.extension ? el?.extension?.thumbnailUrl?.url : el?.url
                  }
                  alt=""
                />
              ) : (
                <DescriptionOutlinedIcon
                  style={{ width: 80, height: 80, color: GREY_600 }}
                />
              )}

              <CloseFillIcon
                style={{ cursor: "pointer" }}
                onClick={() => {
                  handleDeleteFile(index);
                  setError(false);
                }}
              />
            </div>
          ))}
          {fileUploads.length < 5 && (
            <div
              className="add-image-icon"
              onClick={() => document.getElementById("upload_file")?.click()}
              aria-hidden="true"
            >
              <AddImageIcon />
            </div>
          )}
        </div>
      )}
      {uploadError && type !== "welcome" && (
        <p
          style={{
            color: RED,
            margin: 0,
            textAlign: "start",
            paddingLeft: 16,
            fontSize: 12,
          }}
        >
          {intl.formatMessage({ id: "IDS_CHAT_FILE_MAX_MESSAGE" })}
        </p>
      )}
      {type === "chat" && !isCloseTicket && (
        <InputBase
          className="chat-input"
          startAdornment={
            <span>
              <input
                type="file"
                style={{ display: "none" }}
                id="upload_file"
                multiple
                onChange={handleUploadFile}
              />
              <AttachFileIcon
                onClick={() => document.getElementById("upload_file")?.click()}
                style={{ cursor: "pointer" }}
              />
            </span>
          }
          endAdornment={
            <SendIcon
              onClick={() => {
                setError(false);
                sendMessage();
              }}
              className={`${
                !isEmpty(message.trim()) || !isEmpty(fileUploads)
                  ? "active-icon"
                  : ""
              } `}
              style={{
                cursor:
                  !isEmpty(message.trim()) || !isEmpty(fileUploads)
                    ? "pointer"
                    : "not-allowed",
              }}
            />
          }
          inputProps={{ maxLength: 20000 }}
          placeholder={intl.formatMessage({ id: "IDS_CHAT_WRITE_MESSAGE" })}
          value={message}
          onChange={(e) => setMessage(e?.target?.value)}
          onKeyDown={handleKeyPress}
          onFocus={() => handleTyping(true)}
          onBlur={() => handleTyping(false)}
        />
      )}
    </>
  );
};

export default ChatInput;
