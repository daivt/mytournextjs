import React, { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroller";
import bigInt from "big-integer";
import { Button, Typography } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import {
  UUID,
  PAGE_SIZE,
  SUCCESS_CODE,
  TYPE_EVENT,
  TYPE_MESSAGE,
} from "../../constants/constant";
import { isEmpty } from "../../utils/helpers/helpers";
import ChatItem from "./ChatItem";
import { actionGetHistory } from "../ChatAction";
// import { GREY_600 } from '../../assets/theme/colors';
// import ChatEvaluate from './ChatEvaluate';

const ChatContent = (props) => {
  const {
    listMessage,
    chatGroup,
    socket,
    accountInfo,
    restartChat,
    fetchCurrentTicket,
  } = props;
  const [isLoadingMoreMsg, setLoadMoreMsg] = useState(true);
  const [historyMessage, setHistoryMessage] = useState([]);
  const fetchHistory = async (startMessageId, endMessageId) => {
    if (!isEmpty(chatGroup?.chatGroupId)) {
      try {
        const res = await actionGetHistory({
          page: 0,
          pageSize: PAGE_SIZE,
          chatGroupId: `${chatGroup?.chatGroupId}`,
          startMessageId,
          endMessageId,
        });
        if (res?.code === SUCCESS_CODE) {
          const historyMsg = res?.data?.items || [];
          if (historyMsg.length < PAGE_SIZE) setLoadMoreMsg(false);
          const tempHistory = historyMsg.filter(
            (v) => !v?.content.includes(TYPE_EVENT.REQUEST_VOTE)
          );
          setHistoryMessage([...tempHistory, ...historyMessage]);
        }
      } catch (error) {}
    }
  };
  const clearHistory = () => {
    setHistoryMessage([]);
    fetchHistory();
  };
  useEffect(() => {
    clearHistory();
    // eslint-disable-next-line
  }, [socket]);
  const scrollToBottom = () => {
    const chatBoxWrapper = document.querySelector(".bottom-chat-content");
    if (chatBoxWrapper) {
      chatBoxWrapper.scrollIntoView();
    }
  };
  useEffect(() => {
    if (historyMessage.length <= 15) scrollToBottom();
    // eslint-disable-next-line
  }, [historyMessage]);

  useEffect(() => {
    fetchHistory();
    // eslint-disable-next-line
  }, []);
  const tempHistory = historyMessage.filter(
    (v) =>
      !(
        v?.contentType === TYPE_MESSAGE.EVENT &&
        v.content.includes(TYPE_EVENT.REQUEST_VOTE)
      ) && !listMessage.find((u) => `${u?.id}` === `${v?.id}`)
  );
  const allMessage = [...tempHistory, ...listMessage];
  // console.log('allMessage', allMessage);

  return (
    <>
      <div className="chat-content-box">
        <InfiniteScroll
          isReverse
          initialLoad={false}
          pageStart={0}
          loadMore={() => {
            fetchHistory(
              !isEmpty(historyMessage)
                ? `${bigInt(historyMessage[0]?.id).minus(1)}`
                : undefined
            );
          }}
          hasMore={isLoadingMoreMsg}
          loader={<div className="loader" key={0} />}
          useWindow={false}
        >
          <div style={{ margin: "24px 0" }}>
            {!isEmpty(allMessage) &&
              allMessage.map((item, idx) => {
                const senderIDBefore = allMessage[idx - 1]?.sender?.id;
                const senderID = item?.sender?.id;
                const alignType =
                  `${senderID}` === `${accountInfo?.id}` ||
                  `${senderID}` === localStorage.getItem(UUID)
                    ? "right"
                    : "left";
                return (
                  <ChatItem
                    align={alignType}
                    item={item}
                    key={item?.id}
                    isLast={idx === allMessage.length - 1}
                    isFirst={
                      senderID !== senderIDBefore ||
                      idx === 0 ||
                      allMessage[idx - 1]?.contentType === TYPE_MESSAGE.EVENT
                    }
                    chatGroup={chatGroup}
                    socket={socket}
                    accountInfo={accountInfo}
                    fetchCurrentTicket={fetchCurrentTicket}
                  />
                );
              })}
            {isEmpty(chatGroup.id) &&
              !allMessage.find((v) =>
                v.content.includes(TYPE_EVENT.REQUEST_VOTE)
              ) && (
                <Button
                  style={{
                    width: "calc(100% - 24px)",
                    height: 36,
                    backgroundColor: "#ff1284",
                    margin: "0 12px",
                  }}
                  variant="contained"
                  color="secondary"
                  disableElevation
                  onClick={() => {
                    setHistoryMessage([]);
                    restartChat();
                  }}
                >
                  <Typography variant="subtitle2">
                    <FormattedMessage id="IDS_CHAT_START_AGAIN" />
                  </Typography>
                </Button>
              )}
            {/* {isTyping && (
              <div style={{ fontSize: 14, color: GREY_600, display: 'flex' }}>
                {users[`${chatGroup?.employee?.id}`]?.name}
                &nbsp;is typing&nbsp;
                <div className="dot-pulse" />
              </div>
            )} */}
          </div>
        </InfiniteScroll>
        <div className="bottom-chat-content" />
      </div>
    </>
  );
};

export default ChatContent;
