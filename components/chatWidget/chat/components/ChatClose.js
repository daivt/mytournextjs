import React from "react";
import { Popover, Typography } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import { MoreIcon, CloseCircleIcon } from "../../assets/icons";
import { Row } from "../../common/Elements";

const ChatClose = (props) => {
  const { openModal } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => setAnchorEl(null);

  const open = Boolean(anchorEl);
  const id = open ? "close-chat" : undefined;

  return (
    <>
      <MoreIcon
        style={{ cursor: "pointer", marginRight: 12 }}
        onClick={handleClick}
      />
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
        transformOrigin={{ vertical: "top", horizontal: "left" }}
        style={{ zIndex: 2147483639 }}
      >
        <Row
          className="popper-item"
          onClick={() => {
            handleClose();
            openModal();
          }}
        >
          <CloseCircleIcon />
          <Typography
            variant="body2"
            style={{ marginLeft: 4, fontSize: 14, lineHeight: "17px" }}
          >
            <FormattedMessage id="IDS_CHAT_STOP_SUPPORT" />
          </Typography>
        </Row>
      </Popover>
    </>
  );
};

export default ChatClose;
