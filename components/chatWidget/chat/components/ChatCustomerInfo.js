import React from "react";
import { Divider, Paper, Typography } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import { Row, Col } from "../../common/Elements";

const ChatCustomerInfo = (props) => {
  const { item, accountInfo } = props;
  return (
    <Paper className="chat-evaluate support-info" style={{ boxShadow: "none" }}>
      <Typography
        variant="body2"
        className="title-box"
        style={{ textAlign: "left" }}
      >
        <FormattedMessage id="IDS_CHAT_CONTENT_SUPPORT" />
      </Typography>
      <Typography variant="body2" className="title-item">
        <FormattedMessage id="IDS_CHAT_SUPPORT_CONTENT" />
      </Typography>
      <Typography variant="body2" style={{ marginBottom: 8, fontSize: 14 }}>
        {item?.teamName}
      </Typography>
      {/* <Typography variant="body2" className="title-item">
        <FormattedMessage id="IDS_CHAT_CONTENT_REQUIRE" />
      </Typography>
      <Typography variant="body2" style={{ marginBottom: 8, fontSize: 14 }}>
        {item?.customerContentRequest}
      </Typography> */}
      <Divider />
      <Row style={{ marginTop: 8 }}>
        <Col style={{ width: "100%" }}>
          <Typography variant="body2" className="title-item">
            {item?.customerTitle ||
              (accountInfo?.gender === "M" ? "Anh" : "Anh/Chị")}
          </Typography>
          <Typography
            variant="body2"
            className="text-content"
            title={item?.customerName || accountInfo?.name}
          >
            {item?.customerName || accountInfo?.name}
          </Typography>
          <Typography variant="body2" className="title-item">
            <FormattedMessage id="IDS_CHAT_PHONE_NUMBER" />
          </Typography>
          <Typography
            variant="body2"
            className="text-content"
            title={item?.customerPhone || accountInfo?.phoneInfo}
          >
            {item?.customerPhone || accountInfo?.phoneInfo}
          </Typography>
        </Col>
        {/* <Col style={{ width: "50%" }}>
          <Typography variant="body2" className="title-item">
            <FormattedMessage id="IDS_CHAT_ID_AGENCY" />
          </Typography>
          <Typography
            variant="body2"
            className="text-content"
            title={item?.agencyId || `${accountInfo?.id || ""}`}
          >
            {item?.agencyId || `${accountInfo?.id || ""}`}
          </Typography>
          <Typography variant="body2" className="title-item">
            <FormattedMessage id="IDS_CHAT_EMAIL" />
          </Typography>
          <Typography
            variant="body2"
            className="text-content"
            title={item?.customerEmail || accountInfo?.emailInfo}
          >
            {item?.customerEmail || accountInfo?.emailInfo}
          </Typography>
        </Col> */}
      </Row>
    </Paper>
  );
};

export default ChatCustomerInfo;
