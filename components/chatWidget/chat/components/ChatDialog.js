import React from "react";
import {
  Typography,
  Divider,
  DialogContent,
  DialogActions,
  Button,
} from "@material-ui/core";
import { FormattedMessage, useIntl } from "react-intl";
import LoadingButton from "../../common/LoadingButton";
import { actionCloseTicket, actionVoteTicket } from "../ChatAction";
import {
  SUCCESS_CODE,
  TYPE_EVENT,
  TYPE_MESSAGE,
} from "../../constants/constant";
import FormControlTextField from "../../common/FormControlTextField";
import { getSendMessage } from "../../utils/helpers/helpers";

const ChatDialog = (props) => {
  const intl = useIntl();
  const {
    chatGroup,
    handleClose,
    handleSuccess,
    type,
    typeLike,
    socket,
  } = props;
  const [loading, setLoading] = React.useState(false);
  const [customerComment, setComment] = React.useState("");
  const onSubmit = async () => {
    try {
      setLoading(true);
      if (type === "closed") {
        handleSuccess();
        const res = await actionCloseTicket({
          id: chatGroup?.id,
          status: 4,
        });
        if (res?.code === SUCCESS_CODE) handleClose();
      } else {
        const res = await actionVoteTicket({
          id: chatGroup?.id,
          customerRating: typeLike === "like" ? 1 : 0,
          customerComment,
        });
        if (res?.code === SUCCESS_CODE) {
          handleClose();
          if (socket && socket.readyState === 1) {
            socket.send(
              getSendMessage(
                JSON.stringify({
                  type: TYPE_EVENT.VOTED,
                  rate: typeLike === "like" ? 1 : 0,
                  comment: customerComment,
                }),
                TYPE_MESSAGE.EVENT,
                `${chatGroup?.chatGroupId}`,
                `${chatGroup?.id}`
              )
            );
          }
        }
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };
  return (
    <div className="chat-wrapper">
      <div className="chat-container">
        <Typography
          variant="subtitle2"
          className="chat-header-dialog"
          style={{ fontSize: 14 }}
        >
          <FormattedMessage
            id={
              type === "closed"
                ? "IDS_CHAT_CLOSE_TITLE"
                : "IDS_CHAT_ADD_COMMENT"
            }
          />
        </Typography>
        <Divider />
        <DialogContent>
          {type === "closed" ? (
            <Typography
              variant="body2"
              style={{ fontSize: 14, lineHeight: "17px" }}
            >
              <FormattedMessage id="IDS_CHAT_CLOSE_QUESTION" />
            </Typography>
          ) : (
            <>
              <Typography
                variant="body2"
                style={{ fontSize: 14, lineHeight: "17px" }}
              >
                <FormattedMessage id="IDS_CHAT_ADD_COMMENT_DESCRIPTION" />
              </Typography>
              <FormControlTextField
                formControlStyle={{
                  width: "100%",
                  marginRight: 0,
                  marginTop: 8,
                }}
                label=""
                multiline
                rows={4}
                placeholder={intl.formatMessage({
                  id: "IDS_CHAT_ENTER_CUSTOMER_COMMENT",
                })}
                inputProps={{ maxLength: 500, autoComplete: "none" }}
                value={customerComment}
                onChange={(e) => setComment(e.target.value)}
              />
            </>
          )}
        </DialogContent>
        <Divider />
        <DialogActions style={{ padding: 16, justifyContent: "flex-end" }}>
          <LoadingButton
            variant="contained"
            color="secondary"
            size="medium"
            style={{
              width: "50%",
              margin: 0,
              height: 36,
              backgroundColor: "#ff1284",
              fontSize: 14,
              borderRadius: 8,
            }}
            onClick={onSubmit}
            disableElevation
            loading={loading}
          >
            <FormattedMessage
              id={
                type === "closed" ? "IDS_CHAT_CLOSE" : "IDS_CHAT_COMMENT_BUTTON"
              }
            />
          </LoadingButton>
          <Button
            variant="outlined"
            size="medium"
            style={{
              width: "50%",
              height: 36,
              fontSize: 14,
              borderRadius: 8,
              backgroundColor: "#E2E8F0",
              borderColor: "#E2E8F0",
            }}
            onClick={handleClose}
            disableElevation
          >
            <Typography
              variant="body2"
              color="textSecondary"
              style={{ color: "#1A202C", fontWeight: 600 }}
            >
              <FormattedMessage id="IDS_CHAT_REJECT" />
            </Typography>
          </Button>
        </DialogActions>
      </div>
    </div>
  );
};

export default ChatDialog;
