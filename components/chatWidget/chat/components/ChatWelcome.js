import React, { useState, useEffect } from "react";
import InfiniteScroll from "react-infinite-scroller";
import bigInt from "big-integer";
import { Typography } from "@material-ui/core";
import { FormattedMessage } from "react-intl";
import {
  UUID,
  PAGE_SIZE,
  SUCCESS_CODE,
  TYPE_EVENT,
  TYPE_MESSAGE,
} from "../../constants/constant";
import { isEmpty } from "../../utils/helpers/helpers";
import ChatItem from "./ChatItem";
import { actionGetHistory } from "../ChatAction";
import WelcomeMessage from "./WelcomeMessage";
import { CustomerServiceIcon } from "../../assets/icons";

const ChatWelcome = (props) => {
  const {
    accountFree,
    chatGroup,
    socket,
    accountInfo,
    fetchCurrentTicket,
  } = props;
  const [isLoadingMoreMsg, setLoadMoreMsg] = useState(true);
  const [historyMessage, setHistoryMessage] = useState([]);
  const fetchHistory = async (startMessageId, endMessageId) => {
    if (!isEmpty(chatGroup?.chatGroupId)) {
      try {
        const res = await actionGetHistory({
          page: 0,
          pageSize: PAGE_SIZE,
          chatGroupId: `${chatGroup?.chatGroupId}`,
          startMessageId,
          endMessageId,
        });
        if (res?.code === SUCCESS_CODE) {
          const historyMsg = res?.data?.items || [];
          if (historyMsg.length < PAGE_SIZE) setLoadMoreMsg(false);
          const tempHistory = historyMsg.filter(
            (v) => !v?.content.includes(TYPE_EVENT.REQUEST_VOTE)
          );
          setHistoryMessage([...tempHistory, ...historyMessage]);
        }
      } catch (error) {}
    }
  };
  const clearHistory = () => {
    setHistoryMessage([]);
    fetchHistory();
  };
  useEffect(() => {
    clearHistory(); // eslint-disable-next-line
  }, [accountInfo]);

  const getAccountName = () => accountFree?.name || "Mytour CSKH";
  if (isEmpty(historyMessage)) {
    return (
      <div className="chat-content-box empty-content-welcome">
        <CustomerServiceIcon style={{ marginBottom: 24 }} />
        <Typography
          variant="body2"
          component="p"
          style={{ fontSize: 14, lineHeight: "22px" }}
        >
          <FormattedMessage id="IDS_CHAT_WELCOME" />
          &nbsp;
          <b>{accountInfo?.name || "Quý khách"}!</b>
          <br />
          <span>
            Em là <b>{getAccountName() || "Mytour CSKH"}</b>, chuyên viên chăm
            sóc khách hàng. Em có thể giúp gì được cho anh/chị?
          </span>
        </Typography>
      </div>
    );
  }
  return (
    <>
      <div className="chat-content-box">
        <InfiniteScroll
          isReverse
          initialLoad
          pageStart={0}
          loadMore={() => {
            fetchHistory(
              !isEmpty(historyMessage)
                ? `${bigInt(historyMessage[0]?.id).minus(1)}`
                : undefined
            );
          }}
          hasMore={isLoadingMoreMsg}
          loader={<div className="loader" key={0} />}
          useWindow={false}
        >
          <div style={{ margin: "12px 0" }}>
            {!isEmpty(chatGroup) &&
              !isEmpty(historyMessage) &&
              historyMessage.map((item, idx) => {
                const senderIDBefore = historyMessage[idx - 1]?.sender?.id;

                const senderID = item?.sender?.id;
                const alignType =
                  `${senderID}` === `${accountInfo?.id}` ||
                  `${senderID}` === localStorage.getItem(UUID)
                    ? "right"
                    : "left";
                return (
                  <ChatItem
                    align={alignType}
                    item={item}
                    key={item?.id}
                    isLast={idx === historyMessage.length - 1}
                    isFirst={
                      senderID !== senderIDBefore ||
                      idx === 0 ||
                      historyMessage[idx - 1]?.contentType ===
                        TYPE_MESSAGE.EVENT
                    }
                    chatGroup={chatGroup}
                    socket={socket}
                    accountInfo={accountInfo}
                    fetchCurrentTicket={fetchCurrentTicket}
                  />
                );
              })}
            <WelcomeMessage
              accountInfo={accountInfo}
              employeeName={getAccountName()}
              avatarUrl={accountFree?.profilePhoto}
            />
          </div>
        </InfiniteScroll>
      </div>
    </>
  );
};

export default ChatWelcome;
