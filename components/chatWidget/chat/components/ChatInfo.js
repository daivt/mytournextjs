import React, { useEffect, useState } from "react";
import { get } from "js-cookie";
import { Typography } from "@material-ui/core";
import { FormattedMessage, useIntl } from "react-intl";
import { Form, Formik } from "formik";
import * as yup from "yup";
import { actionCreateTicket, actionGetTeams } from "../ChatAction";
import { isEmpty } from "../../utils/helpers/helpers";
import { Col, Row } from "../../common/Elements";
import LoadingButton from "../../common/LoadingButton";
import {
  SUCCESS_CODE,
  ANONYMOUS,
  ACCESS_TOKEN,
  NORMAL_ACCOUNT,
} from "../../constants/constant";
import ChatInfoContent from "./ChatInfoContent";
// import { NumberFormatCustom } from '../../common/Form';

const ChatInfo = (props) => {
  const intl = useIntl();
  const {
    handleRegister,
    accountInfo,
    socket,
    handleUpdateForm,
    valueForm,
  } = props;
  const [loading, setLoading] = React.useState(false);
  const [teams, setTeams] = useState({});
  const [isAnonymous, setAnonymous] = useState(
    isEmpty(get(ACCESS_TOKEN) || localStorage.getItem(ACCESS_TOKEN))
  );
  const fetchData = async () => {
    try {
      const res = await actionGetTeams();
      if (res?.code === SUCCESS_CODE) {
        setTeams(res?.data);
      }
    } catch (error) {}
  };
  useEffect(() => {
    fetchData();
  }, []);
  useEffect(() => {
    setAnonymous(
      isEmpty(get(ACCESS_TOKEN) || localStorage.getItem(ACCESS_TOKEN))
    );
  }, [socket]);
  const schema = yup.object().shape({
    customerPhone: yup
      .string()
      .required(intl.formatMessage({ id: "IDS_CHAT_PHONE_REQUIRE" }))
      .matches(
        /^[0-9]*$/,
        intl.formatMessage({ id: "IDS_CHAT_PHONE_VALIDATE" })
      ),
    teamId: yup
      .number()
      .required(intl.formatMessage({ id: "IDS_CHAT_CHAT_TEAM_REQUIRE" })),
    // customerTitle: yup
    //   .string()
    //   .required(intl.formatMessage({ id: "IDS_CHAT_CHAT_TITLE_REQUIRE" })),
    customerName: yup
      .string()
      .required(intl.formatMessage({ id: "IDS_CHAT_NAME_REQUIRE" })),
    // agencyId: yup
    //   .string()
    //   .required(intl.formatMessage({ id: "IDS_CHAT_AGENCY_REQUIRE" })),
    // customerEmail: yup
    //   .string()
    //   .matches(
    //     /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
    //     intl.formatMessage({ id: "IDS_CHAT_EMAIL_VALIDATE" })
    //   )
    //   .required(intl.formatMessage({ id: "IDS_CHAT_EMAIL_REQUIRE" })),
  });
  return (
    <>
      <Formik
        initialValues={{
          customerPhone: accountInfo?.phoneInfo,
          // customerTitle: accountInfo?.gender
          //   ? accountInfo?.gender === "M"
          //     ? "Anh"
          //     : "Chị"
          //   : undefined,
          customerName: accountInfo?.name,
          // customerEmail: accountInfo?.emailInfo,
          // agencyId: `${accountInfo?.id || ""}`,
          teamId: null,
          ...valueForm,
        }}
        validationSchema={schema}
        onSubmit={async (values, { setSubmitting }) => {
          try {
            setLoading(true);
            const team = teams?.items.find(
              (v) => `${v?.id}` === `${values?.teamId}`
            );
            if (team) {
              const res = await actionCreateTicket({
                ...values,
                teamName: team?.name,
                customerType: isAnonymous ? ANONYMOUS : NORMAL_ACCOUNT,
              });

              if (res?.code === SUCCESS_CODE) {
                handleRegister(res?.data);
              }
            }
          } catch (error) {
          } finally {
            setLoading(false);
          }
        }}
      >
        {({ values, setFieldValue, errors, submitCount }) => {
          return (
            <Form>
              <Col style={{ padding: 12 }}>
                <ChatInfoContent
                  accountInfo={accountInfo}
                  teams={teams}
                  values={values}
                  setFieldValue={setFieldValue}
                  errors={errors}
                  submitCount={submitCount}
                  handleUpdateForm={handleUpdateForm}
                />
                <Row>
                  <LoadingButton
                    style={{
                      width: "100%",
                      height: 36,
                      backgroundColor: "#ff1284",
                      color: "white",
                      cursor: isEmpty(values?.teamId)
                        ? "not-allowed"
                        : "pointer",
                      pointerEvents: "all",
                    }}
                    type="submit"
                    variant="contained"
                    color="secondary"
                    disableElevation
                    disabled={isEmpty(values?.teamId)}
                    loading={loading}
                  >
                    <Typography variant="subtitle2">
                      <FormattedMessage id="IDS_CHAT_START" />
                    </Typography>
                  </LoadingButton>
                </Row>
              </Col>
            </Form>
          );
        }}
      </Formik>
    </>
  );
};

export default ChatInfo;
