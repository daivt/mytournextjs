import React, { useEffect } from "react";
import {
  FormControlLabel,
  InputLabel,
  Radio,
  RadioGroup,
  Typography,
} from "@material-ui/core";
import { FormattedMessage, useIntl } from "react-intl";
import { withStyles } from "@material-ui/core/styles";
import { Field } from "formik";
import { isEmpty } from "../../utils/helpers/helpers";
import { redMark } from "../../common/Elements";
import { GREEN, GREY_900, RED } from "../../assets/theme/colors";
import {
  // FieldSelectContent,
  FieldTextContent,
} from "../../common/FieldContent";
// import { NumberFormatCustom } from '../../common/Form';

const CustomRadio = withStyles({
  root: {
    color: "#ff1284",
    width: 20,
    height: 20,
    "&$checked": {
      color: "#ff1284",
    },
    "& .MuiSvgIcon-root": {
      height: 20,
      weight: 20,
    },
  },
  checked: {},
})((props) => <Radio color="default" {...props} />);

const ChatInfoContent = (props) => {
  const intl = useIntl();
  const {
    setFieldValue,
    accountInfo,
    teams,
    submitCount,
    errors,
    values,
    handleUpdateForm,
  } = props;
  useEffect(() => {
    handleUpdateForm(values); // eslint-disable-next-line
  }, [values]);
  return (
    <>
      <Typography
        variant="body2"
        style={{ marginBottom: 10, fontSize: 14, lineHeight: "22px" }}
      >
        <FormattedMessage id="IDS_CHAT_WELCOME_DESCRIPTION" />
      </Typography>
      {isEmpty(accountInfo?.name) && (
        <>
          <InputLabel
            style={{
              color: GREY_900,
              fontSize: 14,
              lineHeight: "17px",
              marginBottom: 6,
            }}
          >
            <FormattedMessage id="IDS_CHAT_FULL_NAME" />
            {redMark}
          </InputLabel>
          <FieldTextContent
            name="customerName"
            formControlStyle={{ width: "100%", marginRight: 0 }}
            label=""
            placeholder={intl.formatMessage({
              id: "IDS_CHAT_INSERT_FULL_NAME",
            })}
            inputProps={{ maxLength: 255, autoComplete: "none" }}
          />
        </>
      )}
      {isEmpty(accountInfo?.phoneInfo) && (
        <>
          <InputLabel
            style={{
              color: GREY_900,
              fontSize: 14,
              lineHeight: "17px",
              marginBottom: 6,
            }}
          >
            <FormattedMessage id="IDS_CHAT_PHONE_NUMBER" />
            {redMark}
          </InputLabel>

          <FieldTextContent
            name="customerPhone"
            formControlStyle={{ width: "100%", marginRight: 0 }}
            label=""
            disabled={!isEmpty(accountInfo?.phoneInfo)}
            placeholder={intl.formatMessage({
              id: "IDS_CHAT_INSERT_PHONE_NUMBER",
            })}
            inputProps={{ maxLength: 13, autoComplete: "none" }}
          />
        </>
      )}
      {/* <InputLabel style={{ color: GREY_900, fontSize: 14, lineHeight: "17px" }}>
        <FormattedMessage id="IDS_CHAT_SUPPORT_CONTENT" />
        {redMark}
      </InputLabel>
      <FieldSelectContent
        name="teamId"
        label=""
        placeholder={intl.formatMessage({ id: "IDS_CHAT_CHOOSE" })}
        options={teams?.items || []}
        getOptionLabel={(v) => (
          <span>
            <span>{v?.name}</span>
            &nbsp;
            <span
              style={{ color: v?.status === 0 ? RED : GREEN, fontSize: 14 }}
            >
              {`(${v?.status === 0 ? "offline" : "online"})`}
            </span>
          </span>
        )}
        renderValueInput={(v) => v?.name}
        onSelectOption={(value) => {
          setFieldValue("teamId", value);
        }}
        formControlStyle={{ width: "100%", marginRight: 0 }}
      /> */}
      {/* <InputLabel style={{ color: GREY_900, fontSize: 14, lineHeight: "20px" }}>
        <FormattedMessage id="IDS_CHAT_REQUIREMENT_DESCRIPTION" />
      </InputLabel>
      <FieldTextContent
        name="customerContentRequest"
        formControlStyle={{ width: "100%", marginRight: 0 }}
        label=""
        multiline
        rows={4}
        placeholder={intl.formatMessage({ id: "IDS_CHAT_INSERT_DESCRIPTION" })}
        inputProps={{ maxLength: 500, autoComplete: "none" }}
      /> */}
      <InputLabel style={{ color: GREY_900, fontSize: 14, lineHeight: "17px" }}>
        <FormattedMessage id="IDS_CHAT_SUPPORT_CONTENT" />
        {redMark}
      </InputLabel>
      <Field
        name="teamId"
        as={(propsField) => (
          <RadioGroup
            style={{ margin: "6px 0" }}
            name="teamId"
            {...propsField}
            onChange={(e) => {
              setFieldValue("teamId", e.target.value);
            }}
          >
            {(teams?.items || []).map((v, i) => {
              return (
                <FormControlLabel
                  key={v.id}
                  value={v.id}
                  style={{ margin: "6px 0" }}
                  control={
                    <CustomRadio checked={`${values.teamId}` === `${v.id}`} />
                  }
                  label={
                    <span
                      style={{
                        fontSize: 14,
                        lineHeight: "17px",
                        marginLeft: 8,
                      }}
                      onClick={() => {
                        setFieldValue("teamId", v.id);
                      }}
                    >
                      <span>{v?.name}</span>
                      &nbsp;
                      <span
                        style={{
                          color: v?.status === 0 ? RED : GREEN,
                          fontSize: 14,
                        }}
                      >
                        {`(${v?.status === 0 ? "offline" : "online"})`}
                      </span>
                    </span>
                  }
                />
              );
            })}
          </RadioGroup>
        )}
      />
      {submitCount > 0 && errors?.customerTitle ? (
        <Typography
          variant="body2"
          style={{
            color: RED,
            marginBottom: 8,
            marginTop: -8,
            fontSize: 13,
          }}
        >
          {errors?.customerTitle}
        </Typography>
      ) : (
        undefined
      )}

      {/* {isEmpty(accountInfo?.id) && (
        <>
          <InputLabel
            style={{ color: GREY_900, fontSize: 14, lineHeight: "20px" }}
          >
            <FormattedMessage id="IDS_CHAT_ID_AGENCY" />
            {redMark}
          </InputLabel>
          <FieldTextContent
            name="agencyId"
            formControlStyle={{ width: "100%", marginRight: 0 }}
            label=""
            placeholder={intl.formatMessage({
              id: "IDS_CHAT_INSERT_ID_AGENCY",
            })}
            inputProps={{ maxLength: 13, autoComplete: "none" }}
          />
        </>
      )}
      {isEmpty(accountInfo?.email) && (
        <InputLabel>
          <InputLabel
            style={{ color: GREY_900, fontSize: 14, lineHeight: "20px" }}
          >
            <FormattedMessage id="IDS_CHAT_EMAIL" />
            {redMark}
          </InputLabel>
          <FieldTextContent
            name="customerEmail"
            formControlStyle={{ width: "100%", marginRight: 0 }}
            label=""
            placeholder={intl.formatMessage({ id: "IDS_CHAT_INSERT_EMAIL" })}
            inputProps={{ maxLength: 50, autoComplete: "none" }}
          />
        </InputLabel>
      )} */}
    </>
  );
};

export default ChatInfoContent;
