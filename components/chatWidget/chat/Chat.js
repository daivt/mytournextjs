import React, { useState, useEffect } from "react";
import { get } from "js-cookie";
import JSONbig from "json-bigint";
import { FormattedMessage } from "react-intl";
import { Box, Typography } from "@material-ui/core";
import { getSendMessage, isEmpty } from "../utils/helpers/helpers";
import {
  RuleHorizontalIcon,
  // MoreHorizontalIcon,
  AvatarIcon,
  DislikeIcon,
  LikeIcon,
} from "../assets/icons";
import ChatContent from "./components/ChatContent";
import ChatInfo from "./components/ChatInfo";
import { Col, Row } from "../common/Elements";
import { RED, WHITE } from "../assets/theme/colors";
import {
  SUCCESS_CODE,
  TYPE_MESSAGE,
  STATUS_MESSAGE,
  ACCESS_TOKEN,
  TYPE_EVENT,
  ACCOUNT,
} from "../constants/constant";
import ChatWelcome from "./components/ChatWelcome";
import {
  actionGetAccountFree,
  actionGetAccountInfo,
  actionGetCurrentTicket,
  actionGetAccount,
} from "./ChatAction";
import ChatClose from "./components/ChatClose";
import ChatDialog from "./components/ChatDialog";
import ChatInput from "./components/ChatInput";
// import "./Chat.scss";

let valueForm;
const Chat = (props) => {
  const {
    socket,
    reconnectSocket,
    isOutSupport,
    isOnline,
    customClassName,
  } = props;
  const [isOpen, setOpen] = useState(false); // open chat dialog
  const [typeLike, setTypeLike] = useState("");
  const [type, setType] = useState("welcome"); // type screen
  const [message, setMessage] = useState(""); // message input
  const [listMessage, setListMessage] = useState([]); // list message chat by socket
  const [fileUploads, setFileUploads] = useState([]); // list file upload
  const [accountFree, setAccountFree] = useState({}); // account free when start
  const [accountInfo, setAccountInfo] = useState({}); // account when login success
  const [chatGroup, setChatGroup] = useState({}); // chat group information
  const [isClosed, setClosed] = useState(""); // type dialog
  const [users, setUsers] = useState(
    JSONbig.parse(localStorage.getItem(ACCOUNT) || "{}")
  ); // user info in system
  const [isTyping, setTyping] = useState(false); // detect typing

  const isCloseTicket = isEmpty(chatGroup?.id);
  const getAccountDetail = async (userId) => {
    try {
      const res = await actionGetAccount(userId.filter((v) => v));
      if (res?.code === SUCCESS_CODE) {
        let temp = { ...users };
        (res?.data?.items || []).forEach((userItem) => {
          temp = { ...users, [userItem?.id]: userItem };
        });
        localStorage.setItem(ACCOUNT, JSONbig.stringify(temp));
        setUsers(temp);
      }
    } catch (error) {}
  };
  const fetchAccount = async () => {
    try {
      const res = await actionGetAccountFree();
      if (res?.code === SUCCESS_CODE) {
        setAccountFree(res?.data);
        if (isEmpty(users[`${res?.data?.id}`]))
          getAccountDetail([res?.data?.id]);
      }
    } catch (error) {}
  };
  const fetchAccountInfo = async () => {
    try {
      if (get(ACCESS_TOKEN) || localStorage.getItem(ACCESS_TOKEN)) {
        const res = await actionGetAccountInfo();
        if (res?.code === SUCCESS_CODE) setAccountInfo(res?.data);
      }
    } catch (error) {}
  };
  const fetchCurrentTicket = async () => {
    try {
      const res = await actionGetCurrentTicket();
      if (res?.code === SUCCESS_CODE) {
        const ticket = res?.data;
        setChatGroup(ticket);
        setTypeLike(
          ticket?.customerRating === 1
            ? "like"
            : ticket?.customerRating === 0
            ? "dislike"
            : ""
        );
        if (res?.data && res?.data?.id) {
          setType("chat");
          if (isEmpty(users[`${res?.data?.employee?.id}`]))
            getAccountDetail([res?.data?.employee?.id]);
        }
      }
    } catch (error) {}
  };
  const resetChat = () => {
    setListMessage([]);
    setAccountInfo({});
    setChatGroup({});
    setType("welcome");
  };
  useEffect(() => {
    fetchAccountInfo();
    fetchCurrentTicket();
    resetChat();
    valueForm = {}; // eslint-disable-next-line
  }, [socket]);
  useEffect(() => {
    fetchAccount();
    fetchCurrentTicket(); // eslint-disable-next-line
  }, []);
  const handleCloseTicket = () => {
    if (!isCloseTicket && socket && socket.readyState === 1) {
      socket.send(
        getSendMessage(
          JSON.stringify({
            type: TYPE_EVENT.REQUEST_VOTE,
            id: chatGroup?.id,
            rate: chatGroup?.customerRating,
            comment: chatGroup?.customerComment,
            closed: true,
          }),
          TYPE_MESSAGE.EVENT,
          `${chatGroup?.chatGroupId}`,
          `${chatGroup?.id}`
        )
      );
    }
  };
  const restartChat = () => {
    setListMessage([]);
    setMessage("");
    setFileUploads([]);
    setType("info");
  };
  const handleDeleteFile = (idx) => {
    const result = fileUploads.filter((v, i) => i !== idx);
    setFileUploads(result);
  };
  const scrollToBottom = () => {
    const chatBoxWrapper = document.querySelector(".bottom-chat-content");
    if (chatBoxWrapper) chatBoxWrapper.scrollIntoView();
  };
  socket.onclose = (e) => {
    if (e?.code !== 1000 && e?.code !== 2001) reconnectSocket();
  };
  const updateStatusMessage = (messages, res) => {
    const temp = [];
    messages.forEach((element) => {
      if (`${element?.id}` === `${res?.content}`) {
        temp.push({ ...element, status: res?.contentType });
      } else {
        temp.push(element);
      }
    });
    return temp;
  };
  socket.onmessage = (e) => {
    const resFull = JSONbig.parse(e.data);
    const res = resFull?.body?.data || resFull?.body;
    const contentInfo = res?.content;
    if (
      resFull?.headers?.command === "sendCSGroupMessage" &&
      Object.values(TYPE_MESSAGE).includes(res?.contentType)
    ) {
      if (res?.contentType) {
        // console.log('tin nhắn mới do mình gửi đi', res);
        if (res?.contentType === TYPE_MESSAGE.EVENT) {
          const contentEvent = JSONbig.parse(res?.content);
          if (
            contentEvent?.type === TYPE_EVENT.REQUEST_VOTE ||
            contentEvent?.type === TYPE_EVENT.VOTED
          ) {
            setListMessage((messages) => [
              ...messages.filter(
                (v) =>
                  !(
                    v?.contentType === TYPE_MESSAGE.EVENT &&
                    v.content.includes(TYPE_EVENT.REQUEST_VOTE)
                  )
              ),
              res,
            ]);
            if (contentEvent?.closed) fetchCurrentTicket();
          } else setListMessage((messages) => [...messages, res]);
        } else if (
          res?.contentType !== STATUS_MESSAGE.TYPING &&
          res?.contentType !== STATUS_MESSAGE.STOPPED_TYPING
        ) {
          setListMessage((messages) => [...messages, res]);
        }
        if (contentInfo) setFileUploads([]);
        setMessage("");
        scrollToBottom();
      }
    } else if (resFull?.headers?.command === "messageArrived") {
      if (Object.values(TYPE_MESSAGE).includes(res?.contentType)) {
        // console.log('nhận được tin nhắn mới do người khác gửi', res);
        if (res?.contentType === TYPE_MESSAGE.EVENT) {
          const contentEvent = JSONbig.parse(res?.content);
          if (
            contentEvent?.type === TYPE_EVENT.ADD_MEMBER ||
            contentEvent?.type === TYPE_EVENT.REMOVE_MEMBER ||
            contentEvent?.type === TYPE_EVENT.WELCOME ||
            contentEvent?.type === TYPE_EVENT.NEW_TICKET ||
            contentEvent?.type === TYPE_EVENT.VOTED
          ) {
            fetchCurrentTicket();
          }
          if (contentEvent?.type !== TYPE_EVENT.NEW_TICKET) {
            if (
              contentEvent?.type === TYPE_EVENT.REQUEST_VOTE ||
              contentEvent?.type === TYPE_EVENT.VOTED
            ) {
              setListMessage((messages) => [
                ...messages.filter(
                  (v) =>
                    !(
                      v?.contentType === TYPE_MESSAGE.EVENT &&
                      v.content.includes(TYPE_EVENT.REQUEST_VOTE)
                    )
                ),
                res,
              ]);
              if (contentEvent?.closed) fetchCurrentTicket();
            } else setListMessage((messages) => [...messages, res]);
          } else setListMessage((messages) => [...messages, res]);
        } else {
          if (socket && socket.readyState === 1) {
            socket.send(
              getSendMessage(
                `${res?.id}`,
                STATUS_MESSAGE.DELIVERED,
                `${chatGroup?.chatGroupId}`,
                `${chatGroup?.id}`
              )
            );
          }
          setListMessage((messages) => [...messages, res]);
        }
        scrollToBottom();
      } else if (
        res?.contentType === STATUS_MESSAGE.TYPING ||
        res?.contentType === STATUS_MESSAGE.STOPPED_TYPING
      ) {
        setTyping(res?.contentType === STATUS_MESSAGE.TYPING);
      } else {
        // console.log('cập nhật trạng thái message', res, listMessage);
        setListMessage((messages) => [...updateStatusMessage(messages, res)]);
      }
    }
  };
  const sendMessage = () => {
    if (!isEmpty(fileUploads)) {
      if (socket && socket.readyState === 1) {
        fileUploads.forEach((it) => {
          socket.send(
            getSendMessage(
              JSON.stringify([it]),
              TYPE_MESSAGE.FILE,
              `${chatGroup?.chatGroupId}`,
              `${chatGroup?.id}`
            )
          );
        });
      }
    }
    if (!isEmpty(message.trim())) {
      if (socket && socket.readyState === 1)
        socket.send(
          getSendMessage(
            message.trim(),
            TYPE_MESSAGE.TEXT,
            `${chatGroup?.chatGroupId}`,
            `${chatGroup?.id}`
          )
        );
    }
  };
  const handleLike = (value) => {
    setClosed("comment");
    setTypeLike(value);
  };
  const handleTyping = (typing) => {
    if (socket && socket.readyState === 1) {
      socket.send(
        getSendMessage(
          "",
          typing ? STATUS_MESSAGE.TYPING : STATUS_MESSAGE.STOPPED_TYPING,
          `${chatGroup?.chatGroupId}`,
          `${chatGroup?.id}`
        )
      );
    }
  };
  const handleSeen = () => {
    const messageId = !isEmpty(listMessage)
      ? `${
          !isEmpty(listMessage[listMessage.length - 1]?.id)
            ? listMessage[listMessage.length - 1]?.id
            : ""
        }`
      : `${!isEmpty(chatGroup?.startMsgId) ? chatGroup?.startMsgId : ""}`;
    if (
      socket &&
      socket.readyState === 1 &&
      !isEmpty(messageId) &&
      !isEmpty(chatGroup?.id)
    ) {
      socket.send(
        getSendMessage(
          `${messageId}`,
          STATUS_MESSAGE.SEEN,
          `${chatGroup?.chatGroupId}`,
          `${chatGroup?.id}`
        )
      );
    }
  };
  const renderDisconnected = () => {
    if (socket && socket.readyState === 0) {
      return (
        <Typography
          variant="body2"
          component="p"
          className="disconnected-text connecting-text"
        >
          <FormattedMessage id="IDS_CHAT_CONNECTING" />
        </Typography>
      );
    }
    if (isOnline) return null;
    return (
      <Typography variant="body2" component="p" className="disconnected-text">
        <FormattedMessage id="IDS_CHAT_DISCONNECTED" />
      </Typography>
    );
  };
  return (
    <>
      {isOpen ? (
        <div
          className={`chat ${customClassName || ""}`}
          onClick={handleSeen}
          aria-hidden="true"
        >
          <div className="chat-content-container">
            <div className="chat-content">
              <div
                className="chat-header"
                style={{ backgroundColor: "#ff1284" }}
              >
                {(type === "welcome" || type === "info") && (
                  <>
                    {/* <MoreHorizontalIcon style={{ width: 24, height: 24 }} /> */}
                    <p className="header-text">Chat cùng Mytour</p>
                    <RuleHorizontalIcon
                      style={{ width: 24, height: 24, cursor: "pointer" }}
                      onClick={() => setOpen(false)}
                    />
                  </>
                )}
                {type === "chat" && (
                  <>
                    <Row style={{ margin: "6px 0", width: "100%" }}>
                      <Col
                        style={{
                          width: "100%",
                          fontSize: 14,
                          lineHeight: "17px",
                          flexDirection: "row",
                          alignItems: "center",
                        }}
                      >
                        {!isCloseTicket && (
                          <ChatClose openModal={() => setClosed("closed")} />
                        )}
                        Chat cùng Mytour
                      </Col>
                      <RuleHorizontalIcon
                        style={{
                          width: 24,
                          height: 24,
                          cursor: "pointer",
                          marginLeft: 8,
                        }}
                        onClick={() => setOpen(false)}
                      />
                    </Row>
                  </>
                )}
              </div>
              {isOutSupport && isCloseTicket && type !== "info" ? (
                <div className="sub-header" style={{ backgroundColor: RED }}>
                  <Typography
                    variant="body2"
                    component="p"
                    style={{ paddingRight: 10, color: WHITE, fontSize: 14 }}
                  >
                    <FormattedMessage id="IDS_CHAT_OUT_SUPPORT_MESSAGE" />
                  </Typography>
                  {renderDisconnected()}
                </div>
              ) : (
                type !== "info" && (
                  <div
                    className="sub-header"
                    style={{
                      backgroundColor:
                        type === "chat" &&
                        isEmpty(chatGroup?.employee) &&
                        !isCloseTicket
                          ? RED
                          : WHITE,
                    }}
                  >
                    {type === "chat" &&
                    isEmpty(chatGroup?.employee) &&
                    !isCloseTicket ? (
                      <Typography
                        variant="body2"
                        component="p"
                        style={{
                          paddingRight: 10,
                          color: WHITE,
                          fontSize: 14,
                          lineHeight: "17px",
                        }}
                      >
                        <FormattedMessage id="IDS_CHAT_CV_BUSY_MESSAGE" />
                      </Typography>
                    ) : (
                      <Row style={{ margin: "6px 0", width: "100%" }}>
                        {users[`${chatGroup?.employee?.id}`]?.profilePhoto ? (
                          <img
                            src={
                              users[`${chatGroup?.employee?.id}`]?.profilePhoto
                            }
                            alt=""
                            style={{ maxWidth: 40, maxHeight: 40 }}
                          />
                        ) : (
                          <Box className="avatar-box">
                            <AvatarIcon />
                            <Box className="dot-online" />
                          </Box>
                        )}
                        <Col style={{ marginLeft: 12 }}>
                          <Typography
                            variant="body2"
                            component="p"
                            style={{
                              fontSize: 14,
                              lineHeight: "17px",
                              color: "#000000",
                              fontWeight: 600,
                            }}
                          >
                            {type === "chat"
                              ? users[`${chatGroup?.employee?.id}`]?.name ||
                                users[`${accountFree?.id}`]?.name ||
                                "Mytour CSKH"
                              : users[`${accountFree?.id}`]?.name ||
                                "Mytour CSKH"}
                          </Typography>
                          <Typography
                            variant="body2"
                            component="p"
                            style={{
                              fontSize: 12,
                              lineHeight: "14px",
                              color: "#718096",
                            }}
                          >
                            <FormattedMessage id="IDS_CHAT_CV_CSKH" />
                          </Typography>
                        </Col>
                        <span
                          style={{
                            marginLeft: "auto",
                            whiteSpace: "nowrap",
                            display: "flex",
                            opacity: type === "chat" ? 1 : 0.5,
                          }}
                        >
                          <LikeIcon
                            style={{
                              cursor:
                                type === "chat" && !isCloseTicket
                                  ? "pointer"
                                  : "not-allowed",
                            }}
                            onClick={() =>
                              type === "chat" &&
                              !isCloseTicket &&
                              handleLike("like")
                            }
                            className={`like-icon ${
                              isCloseTicket ? "disable-icon" : ""
                            } ${typeLike === "like" ? "active-icon" : ""} `}
                          />
                          <DislikeIcon
                            style={{
                              marginLeft: 8,
                              cursor:
                                type === "chat" && !isCloseTicket
                                  ? "pointer"
                                  : "not-allowed",
                            }}
                            onClick={() =>
                              type === "chat" &&
                              !isCloseTicket &&
                              handleLike("dislike")
                            }
                            className={`dislike-icon ${
                              isCloseTicket ? "disable-icon" : ""
                            } ${typeLike === "dislike" ? "active-icon" : ""} `}
                          />
                        </span>
                      </Row>
                    )}
                    {renderDisconnected()}
                  </div>
                )
              )}

              <div style={{ overflow: "auto", padding: "0", height: "100%" }}>
                {type === "welcome" && (
                  <ChatWelcome
                    accountFree={{
                      ...accountFree,
                      name: users[`${accountFree?.id}`]?.name,
                      profilePhoto: users[`${accountFree?.id}`]?.profilePhoto,
                    }}
                    chatGroup={chatGroup}
                    socket={socket}
                    accountInfo={accountInfo}
                    fetchCurrentTicket={fetchCurrentTicket}
                  />
                )}
                {type === "info" && (
                  <ChatInfo
                    accountInfo={accountInfo}
                    valueForm={valueForm}
                    handleRegister={(group) => {
                      valueForm = {};
                      setChatGroup(group);
                      setTypeLike(
                        group?.customerRating === 1
                          ? "like"
                          : group?.customerRating === 0
                          ? "dislike"
                          : ""
                      );
                      if (isEmpty(users[`${group?.employee?.id}`]))
                        getAccountDetail([group?.employee?.id]);
                      setType("chat");
                    }}
                    handleUpdateForm={(value) => {
                      valueForm = { ...value };
                    }}
                    socket={socket}
                  />
                )}
                {type === "chat" && !isEmpty(chatGroup) && (
                  <ChatContent
                    listMessage={listMessage}
                    chatGroup={chatGroup}
                    socket={socket}
                    accountInfo={accountInfo}
                    restartChat={restartChat}
                    isTyping={isTyping}
                    users={users}
                    fetchCurrentTicket={fetchCurrentTicket}
                  />
                )}
              </div>
              <div className="chat-footer">
                <ChatInput
                  type={type}
                  fileUploads={fileUploads}
                  isCloseTicket={isCloseTicket}
                  message={message}
                  sendMessage={sendMessage}
                  handleTyping={handleTyping}
                  handleDeleteFile={handleDeleteFile}
                  setMessage={setMessage}
                  setFileUploads={setFileUploads}
                  setType={setType}
                />
              </div>

              {isClosed !== "" && (
                <ChatDialog
                  handleClose={() => {
                    setClosed("");
                    setTypeLike("");
                    fetchCurrentTicket();
                  }}
                  chatGroup={chatGroup}
                  handleSuccess={handleCloseTicket}
                  type={isClosed}
                  typeLike={typeLike}
                  socket={socket}
                />
              )}
            </div>
          </div>
        </div>
      ) : (
        <div className="chat-hidden">
          <div
            className="chat-hidden-header"
            onClick={() => setOpen(true)}
            aria-hidden="true"
          >
            <p className="header-hidden-text">Chat cùng Mytour</p>
            <RuleHorizontalIcon
              style={{ width: 24, height: 24, cursor: "pointer" }}
            />
          </div>
        </div>
      )}
    </>
  );
};
export default Chat;
