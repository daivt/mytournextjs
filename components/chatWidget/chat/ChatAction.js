import { configs } from "../constants/config";
import api from "../utils/helpers/api";

export const actionGetTeams = () => {
  return api({ method: "get", url: "/csp/team/list-team" });
};
export const actionCreateTicket = (data) => {
  return api({ method: "post", url: "/csp/tickets", data });
};
export const actionGetHistory = (params) => {
  return api({ method: "get", url: "/chat/messages/cs", params });
};
export const actionUploadFile = (data) => {
  return api({
    method: "post",
    url: `${configs().UPLOAD_URL}/assets/file/upload`,
    data,
  });
  // return api({ method: 'post', url: '/chat/file/upload', data });
};
export const actionVoteTicket = (data) => {
  return api({ method: "post", url: "/csp/tickets/votes", data });
};
export const actionCloseTicket = (data) => {
  return api({ method: "post", url: "/csp/tickets/close-ticket", data });
};
export const actionGetAccountFree = () => {
  return api({ method: "get", url: "/csp/employees/get-free-employee" });
};
export const actionGetAccountInfo = () => {
  return api({ method: "get", url: "/ams/account/get-detail" });
};
export const actionGetCurrentTicket = () => {
  return api({ method: "get", url: "/csp/tickets/current-ticket" });
};
export const actionGetAccount = (data) => {
  return api({
    method: "post",
    url: "/ams/account/get-user-avatar-info",
    data,
  });
};
