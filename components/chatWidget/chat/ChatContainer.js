import React, { useState, useEffect } from "react";
import { get } from "js-cookie";
import moment from "moment";
import { v4 as uuidv4 } from "uuid";
import DeviceDetector from "ua-parser-js";
import { UUID, ACCESS_TOKEN } from "../constants/constant";
import Chat from "./Chat";
import LocaleProvider from "../utils/localeProvider/LocaleProvider";
import { configs } from "../constants/config";
import { isEmpty } from "../utils/helpers/helpers";

let interval = null;
const ChatContainer = (props) => {
  const { caId, customClassName } = props;
  const [socket, setSocket] = useState(null);
  const checkOutSupport = () =>
    moment().isAfter(moment("23:30:00", "HH:mm:ss")) &&
    moment().isBefore(moment("8:00:00", "HH:mm:ss"));
  const [isOutSupport, setOutSupport] = useState(checkOutSupport());
  const [isOnline, setOnline] = useState(navigator.onLine);
  let online = navigator.onLine;
  let token = get(ACCESS_TOKEN) || localStorage.getItem(ACCESS_TOKEN);
  const fetchDeviceId = async () => {
    let uuidVal = localStorage.getItem(UUID);
    if (isEmpty(uuidVal)) {
      uuidVal = uuidv4();
      localStorage.setItem(UUID, uuidVal);
    }
    if (token) {
      setSocket(
        new WebSocket(
          `${configs().SOCKET}/chat/ws?login-token=${token}&ca-id=${caId}`
        )
      );
    } else {
      setSocket(
        new WebSocket(
          `${configs().SOCKET}/chat/ws?device-id=${uuidVal}&ca-id=${caId}`
        )
      );
    }
  };
  const checkCookie = () => {
    const newToken = get(ACCESS_TOKEN) || localStorage.getItem(ACCESS_TOKEN);
    if (online !== navigator.onLine) {
      if (!online) {
        setSocket((previewSocket) => {
          if (previewSocket && previewSocket.readyState === 1) {
            previewSocket.send(
              JSON.stringify({ headers: { command: "goodbye" }, body: {} })
            );
          }
          return new WebSocket(
            `${configs().SOCKET}/chat/ws?login-token=${newToken}&ca-id=${caId}`
          );
        });
      }
      online = navigator.onLine;
      setOnline(navigator.onLine);
    }
    if (token !== newToken) {
      token = newToken;
      if (token) {
        setSocket((previewSocket) => {
          if (previewSocket && previewSocket.readyState === 1) {
            previewSocket.send(
              JSON.stringify({ headers: { command: "goodbye" }, body: {} })
            );
          }
          return new WebSocket(
            `${configs().SOCKET}/chat/ws?login-token=${token}&ca-id=${caId}`
          );
        });
      } else {
        setSocket((previewSocket) => {
          if (previewSocket && previewSocket.readyState === 1) {
            previewSocket.send(
              JSON.stringify({ headers: { command: "goodbye" }, body: {} })
            );
          }
          return new WebSocket(
            `${configs().SOCKET}/chat/ws?device-id=${localStorage.getItem(
              UUID
            ) || ""}&ca-id=${caId}`
          );
        });
      }
    }
    // check time open support
    if (isOutSupport !== checkOutSupport()) setOutSupport(checkOutSupport());
  };
  const reconnectSocket = () => {
    if (token) {
      setSocket((previewSocket) => {
        if (previewSocket && previewSocket.readyState === 1) {
          previewSocket.send(
            JSON.stringify({ headers: { command: "goodbye" }, body: {} })
          );
        }
        return new WebSocket(
          `${configs().SOCKET}/chat/ws?login-token=${token}&ca-id=${caId}`
        );
      });
    } else {
      setSocket((previewSocket) => {
        if (previewSocket && previewSocket.readyState === 1) {
          previewSocket.send(
            JSON.stringify({ headers: { command: "goodbye" }, body: {} })
          );
        }
        return new WebSocket(
          `${configs().SOCKET}/chat/ws?device-id=${localStorage.getItem(UUID) ||
            ""}&ca-id=${caId}`
        );
      });
    }
  };
  useEffect(() => {
    fetchDeviceId();
    interval = setInterval(checkCookie, 100);
    return () => {
      if (interval) clearInterval(interval);
    }; // eslint-disable-next-line
  }, []);
  const device = DeviceDetector(navigator.userAgent);
  if (device.device.type && device.device.type === "mobile") {
    return null;
  }
  return (
    <LocaleProvider>
      {socket && (
        <Chat
          socket={socket}
          reconnectSocket={reconnectSocket}
          isOutSupport={isOutSupport}
          isOnline={isOnline}
          customClassName={customClassName}
        />
      )}
    </LocaleProvider>
  );
};

export default ChatContainer;
