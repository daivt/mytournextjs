import { makeStyles } from "@material-ui/styles";
import { Box, Typography } from "@material-ui/core";

import Link from "@src/link/Link";
import { cityList } from "@utils/dataFake";
import utilStyles from "@styles/utilStyles";
import { listString, routeStatic } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  headerContainer: {
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    flexDirection: "column",
    margin: "0 auto",
  },
  divider: {
    height: 3,
    background: "#00B6F3",
    width: 40,
    margin: "auto",
    marginTop: 12,
    marginBottom: 12,
  },
  cityText: {
    fontSize: 14,
    lineHeight: "32px",
    color: "#1A202C",
    minWidth: 152,
  },
  listItem: {
    ...theme.typography.fontSize,
    lineHeight: "32px",
    color: theme.palette.black.black3,
  },
}));

const listCityGroup = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

const CityList = () => {
  const classes = useStyles();
  const classesUtils = utilStyles();

  return (
    <Box
      style={{
        width: "100%",
        paddingTop: 20,
        paddingLeft: 16,
      }}
    >
      <Box
        style={{
          fontWeight: 600,
          fontSize: 16,
          lineHeight: "19px",
          paddingBottom: 12,
        }}
      >
        {listString.IDS_MT_HOT_HOTEL_TEXT}
      </Box>
      <Box
        className={classesUtils.scrollViewHorizontal}
        style={{ flexDirection: "column" }}
      >
        {listCityGroup.map((item) => {
          return (
            <Box display="flex" key={item}>
              {cityList.map((el, index) => {
                const column = Math.ceil(
                  cityList.length / listCityGroup.length
                );
                if (
                  index >= parseInt(item) * column &&
                  index <= (parseInt(item) + 1) * column - 1
                ) {
                  return (
                    <Box key={el.aliasCode} className={classes.cityText}>
                      <Link
                        href={{
                          pathname: routeStatic.TOP_PRICE_HOTEL.href,
                          query: {
                            slug: [
                              `${el.aliasCode}`,
                              `khach-san-tai-${el.name.stringSlug()}.html`,
                            ],
                          },
                        }}
                      >
                        <Typography
                          variant="caption"
                          className={classes.listItem}
                        >
                          {el.name}
                        </Typography>
                      </Link>
                    </Box>
                  );
                }
                return null;
              })}
            </Box>
          );
        })}
      </Box>
    </Box>
  );
};

export default CityList;
