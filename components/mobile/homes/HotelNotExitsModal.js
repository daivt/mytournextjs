import { useRouter } from "next/router";
import { makeStyles } from "@material-ui/styles";
import { useState, useEffect, forwardRef } from "react";
import { Box, Dialog, Slide, IconButton } from "@material-ui/core";

import { isEmpty } from "@utils/helpers";
import { IconClose } from "@public/icons";
import ButtonComponent from "@src/button/Button";

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  dialogContent: {
    display: "flex",
    flexDirection: "column",
    padding: "0 16px",
  },
}));

const HotelNotExitsModal = () => {
  const classes = useStyles();
  const [visible, setVisible] = useState(false);
  const router = useRouter();
  useEffect(() => {
    if (!isEmpty(router.query) && router.query.detail === "true") {
      setVisible(true);
    }
  }, []);

  const handleClose = () => {
    setVisible(false);
  };

  return (
    <Dialog
      open={visible}
      onClose={handleClose}
      TransitionComponent={Transition}
    >
      <Box display="flex" justifyContent="flex-end">
        <IconButton onClick={handleClose}>
          <IconClose />
        </IconButton>
      </Box>
      <Box className={classes.dialogContent}>
        <Box component="span" fontSize={14} lineHeight="17px">
          Khách sạn đã thay đổi thông tin. Vui lòng tìm kiếm lại khách sạn
        </Box>
        <ButtonComponent
          backgroundColor="#FF1284"
          height={44}
          fontSize={16}
          fontWeight={600}
          borderRadius={8}
          style={{ margin: "24px 0" }}
          handleClick={handleClose}
        >
          Đóng
        </ButtonComponent>
      </Box>
    </Dialog>
  );
};

export default HotelNotExitsModal;
