import clsx from "clsx";
import { useRouter } from "next/router";
import { Box, Typography } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/core/styles";

import Image from "@src/image/Image";
import ButtonComponent from "@src/button/Button";
import { listString, routeStatic } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  wrapResultSearch: {
    marginTop: 138,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
  textSearchAgain: {
    marginTop: 16,
  },
  imageEmpty: {
    width: 249,
    height: 210,
  },
}));

const NoHotelFavorite = ({ cusResultSearch = "" }) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();

  return (
    <>
      <Box className={clsx(classes.wrapResultSearch, cusResultSearch)}>
        <Box className={classes.wrapIconResult}>
          <Image
            srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_hotel_favorite.svg"
            className={classes.imageEmpty}
          />
        </Box>
        <Typography variant="subtitle1" className={classes.result}>
          {listString.IDS_MT_TEXT_NO_FAVORITE_HOTEL}
        </Typography>
        <Typography variant="subtitle2" className={classes.textSearchAgain}>
          <ButtonComponent
            backgroundColor={theme.palette.blue.blueLight8}
            height={41}
            fontSize={16}
            fontWeight={600}
            borderRadius={8}
            width={146}
            handleClick={() => {
              router.push({
                pathname: routeStatic.HOTEL.href,
              });
            }}
          >
            {listString.IDS_MT_TEXT_SEARCH}
          </ButtonComponent>
        </Typography>
      </Box>
    </>
  );
};
export default NoHotelFavorite;
