import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { useTheme } from "@material-ui/core/styles";

import utilStyles from "@styles/utilStyles";
import { adapterHotelHotDeal, isEmpty } from "@utils/helpers";
import { listString, routeStatic } from "@utils/constants";

import Link from "@src/link/Link";
import ButtonComponent from "@src/button/Button";
import HotelCard from "@components/common/card/hotel/HotelCard";

const useStyles = makeStyles((theme) => ({
  linkBtn: {
    color: theme.palette.black.black3,
    textDecoration: "none !important",
  },

  boxHotelDeal: {
    padding: "32px 0 16px 16px",
    display: "flex",
    flexDirection: "column",
  },
}));

const HotDeal = ({ hotelHotDeal = [] }) => {
  const theme = useTheme();
  const classes = useStyles();
  const classesUtils = utilStyles();

  if (isEmpty(hotelHotDeal)) {
    return null;
  }
  return (
    <Box>
      <Box className={classes.boxHotelDeal}>
        <Typography variant="h5">{listString.IDS_TEXT_CAN_YOU_LOVE}</Typography>
        <Box component="span" color="black.black4" lineHeight="17px" pt={4 / 8}>
          {listString.IDS_TEXT_TOP_HOTEL_SEARCH_AND_BOOKING}
        </Box>
      </Box>
      <Box pl={1} className={classesUtils.scrollViewHorizontal}>
        {adapterHotelHotDeal(hotelHotDeal).map((el) => (
          <Box key={el.id} px={1}>
            <HotelCard
              item={el}
              isHotelInterest={false}
              isCountBooking={false}
              isShowDiscountPercent={false}
              isShowPriceInImage={false}
            />
          </Box>
        ))}
      </Box>
      <Box display="flex" justifyContent="center" pt={2}>
        <Link
          href={{
            pathname: routeStatic.TOP_HOTEL.href,
          }}
          className={classes.linkBtn}
        >
          <ButtonComponent
            typeButton="outlined"
            backgroundColor="inherit"
            color="inherit"
            width="fit-content"
            borderColor={theme.palette.black.black5}
            borderRadius={8}
            padding="12px 50px"
          >
            {listString.IDS_TEXT_VIEW_ALL}
          </ButtonComponent>
        </Link>
      </Box>
    </Box>
  );
};

export default HotDeal;
