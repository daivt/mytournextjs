import { useState, useEffect } from "react";
import { Box, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { useTheme } from "@material-ui/core/styles";

import { isEmpty, paramsDefaultHotel } from "@utils/helpers";
import { getHotelsAvailability } from "@api/hotels";
import { getTopHotels, getHotelsHotDeal, getBannerNews } from "@api/homes";
import { useDispatchSystem } from "@contextProvider/ContextProvider";
import { SET_TOP_LOCATION } from "@contextProvider/hotel/ActionTypes";
import {
  listString,
  routeStatic,
  DELAY_TIMEOUT_POLLING,
} from "@utils/constants";

import Link from "@src/link/Link";
import Image from "@src/image/Image";
import ButtonComponent from "@src/button/Button";
import Layout from "@components/layout/mobile/Layout";
import HotDeal from "@components/mobile/homes/HotDeal";
import CityList from "@components/mobile/homes/CityList";
import TopHotel from "@components/mobile/homes/TopHotel";
import SlideShow from "@components/common/slideShow/SlideShow";
import FlyBestPrice from "@components/mobile/homes/FlyBestPrice";
import TopBannerView from "@components/mobile/homes/TopBannerView";
import ListHotelLastView from "@components/mobile/homes/ListHotelLastView";
import HotelNotExitsModal from "@components/mobile/homes/HotelNotExitsModal";
import LocationBannerCard from "@components/common/card/hotel/LocationBannerCard";

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
    paddingBottom: 32,
  },
  imageContent: {
    margin: "0 8px",
    borderRadius: 8,
    paddingTop: 24,
  },
  imageSlick: {
    width: "100%",
    height: 154,
    borderRadius: 8,
  },
  linkBtn: {
    color: theme.palette.black.black3,
    textDecoration: "none !important",
  },
  slickDot: {
    bottom: "-20px !important",
    "& li": {
      height: 2,
      width: 16,
      margin: "0 2px",
      "& div": {
        height: 2,
        width: 16,
        borderRadius: 100,
        backgroundColor: theme.palette.gray.grayLight22,
        border: "none",
      },
    },
    "&.slick-active": {
      "& div": {
        backgroundColor: theme.palette.blue.blueLight8,
      },
    },
  },
  boxHotelDeal: {
    padding: "32px 0 16px 16px",
    display: "flex",
    flexDirection: "column",
  },
  locationDestinationBetween: {
    padding: "16px 0 8px",
  },

  itemBannerSmall: {
    height: 136,
  },
}));

const settings = {
  dots: true,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  arrows: false,
};

const MobileContent = ({ topLocation = [] }) => {
  const classes = useStyles();
  const dispatch = useDispatchSystem();
  const theme = useTheme();
  let breakPolling = true;
  const [hotelHotDeal, setHotelHotDeal] = useState([]);
  const [topHotels, setTopHotels] = useState([]);
  const [listBanner, setListBanner] = useState([]);

  useEffect(() => {
    fetchData();
    fetBanner();
    dispatch({
      type: SET_TOP_LOCATION,
      payload: topLocation,
    });
    return () => {
      breakPolling = false;
    };
  }, []);

  const fetchData = async () => {
    try {
      const params = { page: 1, size: 20, ...paramsDefaultHotel() };
      const [resHotDeal, resTopHotel] = await Promise.all([
        getHotelsHotDeal(params),
        getTopHotels(params),
      ]);
      let temp = [];
      if (!isEmpty(resHotDeal.data) && resHotDeal.data.code === 200) {
        temp = [...temp, ...resHotDeal.data.data.items];
        setHotelHotDeal(
          !isEmpty(resHotDeal.data.data) ? resHotDeal.data.data.items : []
        );
      }
      if (!isEmpty(resTopHotel.data) && resTopHotel.data.code === 200) {
        temp = [...temp, ...resTopHotel.data.data.items];
        setTopHotels(
          !isEmpty(resTopHotel.data.data) ? resTopHotel.data.data.items : []
        );
      }
      fetchPriceHotel(
        temp.map((el) => el.id) || [],
        resHotDeal.data.data.items || [],
        resTopHotel.data.data.items || []
      );
    } catch (error) {}
  };

  const fetchPriceHotel = async (
    ids = [],
    dataOriginalHotDeal = [],
    dataOriginalTopHotel = []
  ) => {
    let polling = true;
    try {
      const dataDto = {
        ...paramsDefaultHotel(),
        hotelIds: ids,
        searchType: "hotel",
      };
      while (polling && breakPolling) {
        const { data } = await getHotelsAvailability(dataDto);
        if (data.code === 200) {
          mergerPriceToData(
            data.data.items,
            "hotelHotDeal",
            dataOriginalHotDeal
          );
          mergerPriceToData(data.data.items, "topHotels", dataOriginalTopHotel);
          if (data.data.completed) {
            polling = false;
            break;
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      polling = false;
    }
  };

  const mergerPriceToData = (data = [], type = "", dataOriginal = []) => {
    const dataResult = [...dataOriginal];
    data.forEach((el) => {
      dataResult.forEach((item, index) => {
        if (item.id === el.id) {
          dataResult[index] = {
            ...item,
            promotionInfo: el.basePromotionInfo,
            price: el.basePrice,
            hiddenPrice: el.hiddenPrice,
          };
          return;
        }
      });
    });

    if (type === "hotelHotDeal") setHotelHotDeal(dataResult);
    else if (type === "topHotels") setTopHotels(dataResult);
  };

  const fetBanner = async () => {
    try {
      const { data } = await getBannerNews();
      if (data.code === 200) {
        setListBanner(data?.data || []);
      }
    } catch (error) {}
  };
  return (
    <Layout isHeader isFooter>
      <Box className={classes.container}>
        <Box pt={12 / 8}>
          <TopBannerView />
        </Box>
        {!isEmpty(listBanner) && (
          <SlideShow
            settingProps={settings}
            className={classes.imageContent}
            slickDotStyle={classes.slickDot}
          >
            {listBanner
              .sort((a, b) => a.priority - b.priority)
              .map((el) => {
                if (el.action === "link") {
                  return (
                    <Box key={el.id} px={1}>
                      <Link href={el.content} target="_blank">
                        <Image
                          srcImage={el.thumb}
                          className={classes.imageSlick}
                          alt={el.description}
                          title={el.title}
                          borderRadiusProp="8px"
                        />
                      </Link>
                    </Box>
                  );
                }
                return (
                  <Box key={el.id} px={1}>
                    <Image
                      srcImage={el.thumb}
                      className={classes.imageSlick}
                      alt={el.description}
                      title={el.title}
                      borderRadiusProp="8px"
                    />
                  </Box>
                );
              })}
          </SlideShow>
        )}
        <TopHotel topHotels={topHotels} />
        <HotDeal hotelHotDeal={hotelHotDeal} />
        <FlyBestPrice />
        <Box className={classes.boxHotelDeal}>
          <Typography variant="h5">
            {listString.IDS_TEXT_DESTINATION_FAVORITE}
          </Typography>
          <Box
            component="span"
            color="black.black4"
            lineHeight="17px"
            pt={4 / 8}
          >
            {listString.IDS_TEXT_DESTINATION_HOT_MYTOUR_RECOMMENDED}
          </Box>
        </Box>
        <Box p="0 16px">
          <Grid container className={classes.itemBannerSmall}>
            {topLocation.length >= 1 && (
              <LocationBannerCard item={topLocation[0] || {}} />
            )}
          </Grid>
          <Grid
            container
            spacing={2}
            className={classes.locationDestinationBetween}
          >
            <Grid item xs={6}>
              {topLocation.length >= 2 && (
                <LocationBannerCard item={topLocation[1] || {}} />
              )}
            </Grid>
            <Grid item xs={6}>
              <Grid container spacing={2}>
                <Grid item xs={12} className={classes.itemBannerSmall}>
                  {topLocation.length >= 3 && (
                    <LocationBannerCard item={topLocation[2] || {}} />
                  )}
                </Grid>
                <Grid item xs={12} className={classes.itemBannerSmall}>
                  {topLocation.length >= 4 && (
                    <LocationBannerCard item={topLocation[3] || {}} />
                  )}
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6} className={classes.itemBannerSmall}>
              {topLocation.length >= 5 && (
                <LocationBannerCard item={topLocation[4] || {}} />
              )}
            </Grid>
            <Grid item xs={6} className={classes.itemBannerSmall}>
              {topLocation.length >= 6 && (
                <LocationBannerCard item={topLocation[5] || {}} />
              )}
            </Grid>
          </Grid>
        </Box>
        <Box display="flex" justifyContent="center" pt={2}>
          <Link
            href={{
              pathname: routeStatic.FAVORITE_DESTINATIONS_HOTEL_LIST.href,
            }}
            className={classes.linkBtn}
          >
            <ButtonComponent
              typeButton="outlined"
              backgroundColor="inherit"
              color="inherit"
              width="fit-content"
              borderColor={theme.palette.black.black5}
              borderRadius={8}
              padding="12px 50px"
            >
              {listString.IDS_TEXT_VIEW_ALL}
            </ButtonComponent>
          </Link>
        </Box>
        <ListHotelLastView />
        <CityList />
      </Box>
      <HotelNotExitsModal />
    </Layout>
  );
};

export default MobileContent;
