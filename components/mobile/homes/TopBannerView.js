import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import { IconHotelBox } from "@public/icons";

import {
  listString,
  routeStatic,
  prefixUrlIcon,
  listIcons,
} from "@utils/constants";

import Link from "@src/link/Link";

const useStyles = makeStyles((theme) => ({
  itemBoxTabMenu: {
    display: "flex",
    alignItems: "center",
    borderRadius: 8,
    boxShadow:
      "0px 0px 8px rgba(0, 0, 0, 0.08), 0px 8px 8px rgba(0, 0, 0, 0.05)",
    backgroundColor: theme.palette.white.main,
    padding: "10px 0 10px 12px",
    margin: "0 8px",
    width: "50%",
    color: theme.palette.black.black3,
    textDecoration: "none !important",
  },
}));
const listTabMenu = [
  {
    id: "0",
    name: listString.IDS_MT_TEXT_HOTEL,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconHotelBox}`}
        alt="icon_hotel_box"
        style={{ width: 44, height: 44 }}
      />
    ),
    link: routeStatic.HOTEL.href,
  },
  {
    id: "1",
    name: listString.IDS_MT_TEXT_FLY,
    icon: <img src={`${prefixUrlIcon}${listIcons.IconFlightBox}`} />,
    link: routeStatic.FLIGHT.href,
  },
];

const TopBannerView = ({}) => {
  const classes = useStyles();
  return (
    <Box px={1} display="flex" fontWeight={600}>
      {listTabMenu.map((el) => (
        <Link href={el.link} className={classes.itemBoxTabMenu} key={el.id}>
          <Box component="span" mr={1}>
            {el.icon}
          </Box>
          <Typography variant="subtitle2">{el.name}</Typography>
        </Link>
      ))}
    </Box>
  );
};

export default TopBannerView;
