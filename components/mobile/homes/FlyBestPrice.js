import { searchHotTickets } from "@api/flight";
import FlightBookingReviewCard from "@components/common/card/FlightBookingReviewCard";
import { Box, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import ButtonComponent from "@src/button/Button";
import Link from "@src/link/Link";
import { formatDate, listString, routeStatic } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import { DATE_FORMAT, DATE_FORMAT_BACK_END } from "@utils/moment";
import find from "lodash/find";
import minBy from "lodash/minBy";
import take from "lodash/take";
import moment from "moment";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { domesticInBound } from "@utils/domestic";

const useStyles = makeStyles((theme) => ({
  boxFlightCard: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: theme.palette.blue.blueLight8,
    marginTop: 32,
    padding: "32px 16px 24px 16px",
    color: theme.palette.white.main,
  },
  styleWrapperFlightCard: {
    margin: "8px 0",
  },
  linkBtn: {
    color: theme.palette.black.black3,
    "& hover": {
      textDecoration: "none",
    },
  },
}));
const FlyBestPrice = () => {
  const classes = useStyles();
  const theme = useTheme();
  const [listData, setListData] = useState([]);
  const router = useRouter();
  useEffect(() => {
    getBestFly();
  }, []);

  const getBestFly = async () => {
    try {
      let listInBound = [];
      domesticInBound.forEach((data, index) => {
        const airportCode = data.code.split("-");
        if (airportCode[0] === "SGN") {
          listInBound.push({
            arrivalCity: data?.arrival,
            departCity: "Hồ Chí Minh",
            fromAirport: "SGN",
            toAirport: airportCode[1],
          });
        }
      });

      const itinerariesParam = take(listInBound, 5);

      const dataDTO = {
        adults: 1,
        children: 0,
        fromDate: moment().format(formatDate.firstYear),
        groupByDate: true,
        infants: 0,
        itineraries: itinerariesParam,
        toDate: moment()
          .add(15, "days")
          .format(formatDate.firstYear),
      };
      let listData = [];
      const { data } = await searchHotTickets(dataDTO);
      data.data.hotTickets.forEach((el) => {
        if (!isEmpty(el.priceOptions)) {
          const tickets = minBy(el.priceOptions, "totalPrice");
          const itinerary = {
            fromAirport: find(
              data?.data?.airports,
              (o) => o.code === el.itinerary.fromAirport
            ),
            toAirport: find(
              data?.data?.airports,
              (o) => o.code === el.itinerary.toAirport
            ),
          };
          const airlines = find(
            data?.data?.airlines,
            (o) => o.id === tickets.airlineId
          );
          listData.push({
            origin: el.itinerary.fromAirport,
            destination: el.itinerary.toAirport,
            flightFrom: itinerary.fromAirport.name,
            flightTo: itinerary.toAirport.name,
            date: tickets.departureDate,
            airlineLogo: airlines.logo,
            airlineName: airlines.name,
            discountPercent: tickets.discountAdult,
            subPrice:
              (tickets.totalPrice * (100 + tickets.discountAdult)) / 100,
            mainPrice: tickets.totalPrice,
            farePrice: tickets.farePrice,
          });
        }
      });
      setListData(listData);
    } catch (error) {}
  };

  const handleClickTicket = (item) => () => {
    router.push({
      pathname: "/ve-may-bay/result/outbound",
      query: {
        adultCount: 1,
        childCount: 0,
        infantCount: 0,
        seatSearch: "economy",
        departureDate: item.date
          ? moment(item.date, DATE_FORMAT).format(DATE_FORMAT_BACK_END)
          : moment().format(DATE_FORMAT_BACK_END),
        origin_code: item.origin,
        origin_location: item.flightFrom,
        destination_code: item.destination,
        destination_location: item.flightTo,
      },
    });
  };
  if (isEmpty(listData)) return null;
  return (
    <Box className={classes.boxFlightCard}>
      <Typography variant="h5">{listString.IDS_TEXT_FLY_HOT_DEAL}</Typography>
      <Box
        component="span"
        fontWeight={400}
        lineHeight="17px"
        pt={4 / 8}
        pb={1}
      >
        {listString.IDS_TEXT_PEOPLE_CARE_THE_MOST}
      </Box>
      {listData.map((el, index) => (
        <FlightBookingReviewCard
          key={index}
          bgCircle={theme.palette.blue.blueLight8}
          item={el}
          styleWrapper={classes.styleWrapperFlightCard}
          handleClickTicket={handleClickTicket}
        />
      ))}
      {listData.length > 5 && (
        <Box display="flex" justifyContent="center" pt={2}>
          <Link
            href={{
              pathname: routeStatic.FLIGHT_GOOD_PRICES.href,
            }}
            className={classes.linkBtn}
          >
            <ButtonComponent
              backgroundColor={theme.palette.white.main}
              color="inherit"
              width="fit-content"
              borderRadius={8}
              padding="12px 50px"
            >
              {listString.IDS_TEXT_VIEW_ALL}
            </ButtonComponent>
          </Link>
        </Box>
      )}
    </Box>
  );
};

export default FlyBestPrice;
