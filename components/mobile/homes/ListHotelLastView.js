import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import { Box, Typography } from "@material-ui/core";

import { isEmpty } from "@utils/helpers";
import utilStyles from "@styles/utilStyles";
import { listString, LAST_HOTEL_BROWSER_ITEMS } from "@utils/constants";

import HotelLastViewCard from "@components/common/card/hotel/HotelLastViewCard";

const useStyles = makeStyles((theme) => ({
  boxHotelDeal: {
    padding: "32px 0 16px 16px",
    display: "flex",
    flexDirection: "column",
  },
}));

const ListHotelLastView = () => {
  const classes = useStyles();
  const classesUtils = utilStyles();
  const [hotelItems, setHotelItems] = useState([]);
  useEffect(() => {
    setHotelItems(
      JSON.parse(localStorage.getItem(LAST_HOTEL_BROWSER_ITEMS)) || []
    );
  }, []);
  if (isEmpty(hotelItems)) {
    return null;
  }
  return (
    <Box>
      <Box className={classes.boxHotelDeal}>
        <Typography variant="h5">
          {listString.IDS_TEXT_VIEW_LAST_HISTORY}
        </Typography>
        <Box
          component="span"
          color="gray.grayDark8"
          lineHeight="17px"
          pt={4 / 8}
        >
          {listString.IDS_TEXT_VIEW_LAST_HISTORY_HOTEL}
        </Box>
      </Box>
      <Box pl={1} className={classesUtils.scrollViewHorizontal}>
        {hotelItems.map((el) => (
          <Box key={el.id} px={1}>
            <HotelLastViewCard item={el} />
          </Box>
        ))}
      </Box>
    </Box>
  );
};

export default ListHotelLastView;
