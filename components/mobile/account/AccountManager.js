import cookie from "js-cookie";
import { useRouter } from "next/router";
import { Box, Avatar, IconButton } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import {
  IconBack,
  IconBgAccount,
  IconBgFavorite,
  IconBgMyTour,
  IconBgNotification,
  IconBgPolicy,
  IconBgSendRes,
  IconBgService,
  IconBgShare,
  IconBgLogoMyTour,
} from "@public/icons";
import { isEmpty } from "@utils/helpers";
import * as actionTypes from "@contextProvider/system/ActionTypes";
import { useSystem, useDispatchSystem } from "@contextProvider/ContextProvider";
import {
  listString,
  routeStatic,
  TOKEN,
  LAST_BOOKING_HOTEL,
  prefixUrlIcon,
  listIcons,
} from "@utils/constants";
import { LogoutAccount } from "@api/user";

import Link from "@src/link/Link";
import ButtonComponent from "@src/button/Button";
import Layout from "@components/layout/mobile/Layout";

const listBlockF = [
  {
    id: 0,
    title: listString.IDS_MT_TEXT_INFO_ACCOUNT,
    icon: <IconBgAccount />,
    link: routeStatic.ACCOUNT_INFO.href,
    isLink: true,
    code: "user-info",
  },
  // {
  //   id: 1,
  //   title: "Thông báo",
  //   icon: <IconBgNotification />,
  // },
  {
    id: 2,
    title: listString.IDS_MT_TEXT_HOTEL_FAVORITE,
    icon: <IconBgFavorite />,
    link: routeStatic.ACCOUNT_FAVORITES_HOTEL.href,
    code: "favorites",
  },
  // {
  //   id: 3,
  //   title: "Giới thiệu bạn bè",
  //   icon: <IconBgShare />,
  // },
];
const listBlockE = [
  {
    id: 0,
    title: "Dịch vụ khách hàng 24/7",
    icon: <IconBgService />,
    isLink: true,
    link: "/help/30-lien-he.html",
  },
  // {
  //   id: 1,
  //   title: "Về Mytour.vn",
  //   icon: <IconBgMyTour />,
  //   link: "/app/mytour-info",
  //   isLink: true,
  // },
  {
    id: 2,
    title: "Chính sách bảo mật",
    icon: <IconBgPolicy />,
    isLink: true,
    link: "/news/135156-chinh-sach-bao-mat-thong-tin-danh-cho-san-gdtmdt.html",
  },
  {
    id: 3,
    title: "Gửi phản hồi",
    icon: <IconBgSendRes />,
    isLink: true,
    link: "/feedback.html",
  },
  {
    id: 4,
    title: listString.IDS_MT_TEXT_LOGOUT,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconBgLogout}`}
        alt="icon_logout"
        style={{ width: 32, height: 32 }}
      />
    ),
    code: "logout",
  },
];

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
    position: "relative",
  },
  noneLoginContent: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    backgroundColor: theme.palette.blue.blueLight9,
    padding: "40px 0 16px",
  },
  iconBgLogo: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: 56,
    width: 56,
    backgroundColor: theme.palette.white.main,
    borderRadius: "50%",
  },
  linkHotelDetail: {
    textDecoration: "none !important",
    fontWeight: 600,
    color: theme.palette.primary.main,
  },
  itemAccount: {
    padding: "8px 0",
    fontSize: 16,
  },
  diver: {
    height: 1,
    width: "100%",
    backgroundColor: theme.palette.gray.grayLight22,
    margin: "8px 0",
  },
  infoLogin: {
    position: "relative",
    width: "100%",
    height: 70,
    backgroundColor: theme.palette.blue.blueLight9,
    marginBottom: 100,
  },
  ellipse: {
    position: "absolute",
    top: 15,
    left: 0,
    display: "flex",
    justifyContent: "center",
    height: 85,
    width: "100%",
    borderRadius: "40%",
    backgroundColor: theme.palette.blue.blueLight9,
  },
  avatarDefault: {
    width: 88,
    height: 88,
    backgroundColor: theme.palette.blue.blueLight8,
    fontSize: 40,
    fontWeight: 600,
  },
  userAccount: {
    marginTop: 30,
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
  },
  linkItem: {
    textDecoration: "none !important",
    color: theme.palette.black.black3,
  },
  avatarUser: {
    width: 88,
    height: 88,
  },
  btnBack: {
    position: "absolute",
    zIndex: 10,
    left: 0,
    top: 0,
  },
}));
const AccountManager = () => {
  const classes = useStyles();
  const { systemReducer } = useSystem();
  const router = useRouter();
  const dispatch = useDispatchSystem();

  const informationUser = systemReducer.informationUser || {};
  const nameUser = informationUser.name || "";
  const nameSplit = nameUser.split(" ") || [];
  const lastNameUSer = nameSplit.pop() || "";
  const isLogin = !isEmpty(informationUser);
  const NoneLogin = (
    <Box className={classes.noneLoginContent}>
      <Box className={classes.iconBgLogo}>
        <IconBgLogoMyTour />
      </Box>
      <Box component="span" fontSize={16} lineHeight="19px" pt={12 / 8}>
        Chào mừng bạn đến với Mytour.vn
      </Box>
      <Box component="span" fontSize={16} lineHeight="19px" pt={1} pb={2}>
        <Box component="span" fontWeight={600} pr={2 / 8}>
          {listString.IDS_MT_TEXT_LOGIN}
        </Box>
        để nhận được nhiều ưu đãi
      </Box>
      <ButtonComponent
        backgroundColor="#FF1284"
        borderRadius={8}
        width="fit-content"
        height={40}
        padding="10px 40px"
        fontSize={16}
        fontWeight={600}
        handleClick={() =>
          router.push({
            pathname: routeStatic.LOGIN.href,
          })
        }
      >
        {listString.IDS_MT_TEXT_LOGIN}
      </ButtonComponent>
      <Box display="flex" lineHeight="17px" pt={2}>
        <Box component="span" pr={4 / 8}>
          Chưa có tài khoản?
        </Box>
        <Link
          href={{
            pathname: routeStatic.REGISTER_ACCOUNT.href,
          }}
          className={classes.linkHotelDetail}
        >
          Đăng ký ngay
        </Link>
      </Box>
    </Box>
  );
  const InfoLogin = (
    <Box className={classes.infoLogin}>
      <Box className={classes.ellipse}>
        <Box className={classes.userAccount}>
          {!isEmpty(informationUser?.profilePhoto) ? (
            <Avatar
              alt="Remy Sharp"
              src={informationUser?.profilePhoto}
              className={classes.avatarUser}
            />
          ) : (
            <Avatar className={classes.avatarDefault}>
              {lastNameUSer[0] || ""}
            </Avatar>
          )}
          <Box
            component="span"
            fontSize={18}
            lineHeight="21px"
            fontWeight={600}
            pt={12 / 8}
          >
            {nameUser || ""}
          </Box>
        </Box>
      </Box>
    </Box>
  );
  const handleClick = async (code = "") => {
    if (code === "logout") {
      try {
        const { data } = await LogoutAccount(cookie.get(TOKEN));
        if (data.code === 200) {
          cookie.remove(TOKEN);
          localStorage.removeItem(LAST_BOOKING_HOTEL);
          router.push({
            pathname: routeStatic.HOME.href,
          });
          dispatch({
            type: actionTypes.SET_INFORMATION_USER,
            payload: {},
          });
        }
      } catch (error) {}
    } else if (code === "favorites") {
      if (isLogin) {
        router.push({
          pathname: routeStatic.ACCOUNT_FAVORITES_HOTEL.href,
        });
      } else {
        router.push({
          pathname: routeStatic.LOGIN.href,
        });
      }
    }
  };

  const _handleBackRoute = () => {
    router.back();
  };

  return (
    <Layout isHeader={false}>
      <Box className={classes.container}>
        <IconButton className={classes.btnBack} onClick={_handleBackRoute}>
          <IconBack />
        </IconButton>
        <Box className={classes.infoAccount}>
          {isLogin ? InfoLogin : NoneLogin}
        </Box>
        <Box p="12px 16px 0">
          {listBlockF.map((el, index) => {
            if (el.code === "user-info" && !isLogin) return null;
            if (el.isLink)
              return (
                <Link
                  href={{
                    pathname: el.link,
                  }}
                  className={classes.linkItem}
                  key={index.toString()}
                >
                  <Box className={classes.itemAccount} key={el.id}>
                    <Box display="flex" alignItems="center" lineHeight="19px">
                      {el.icon}
                      <Box component="span" pl={12 / 8}>
                        {el.title}
                      </Box>
                    </Box>
                  </Box>
                </Link>
              );
            return (
              <Box
                className={classes.itemAccount}
                key={el.id}
                onClick={() => handleClick(el.code)}
              >
                <Box display="flex" alignItems="center" lineHeight="19px">
                  {el.icon}
                  <Box component="span" pl={12 / 8}>
                    {el.title}
                  </Box>
                </Box>
              </Box>
            );
          })}
          <Box className={classes.diver} />
          {listBlockE.map((el, index) => {
            if (el.code === "logout" && !isLogin) return null;
            if (el.isLink)
              return (
                <Link
                  href={{
                    pathname: el.link,
                  }}
                  className={classes.linkItem}
                  key={index.toString()}
                >
                  <Box className={classes.itemAccount} key={el.id}>
                    <Box display="flex" alignItems="center" lineHeight="19px">
                      {el.icon}
                      <Box component="span" pl={12 / 8}>
                        {el.title}
                      </Box>
                    </Box>
                  </Box>
                </Link>
              );
            return (
              <Box
                className={classes.itemAccount}
                key={el.id}
                onClick={() => handleClick(el.code)}
              >
                <Box display="flex" alignItems="center" lineHeight="19px">
                  {el.icon}
                  <Box component="span" pl={12 / 8}>
                    {el.title}
                  </Box>
                </Box>
              </Box>
            );
          })}
        </Box>
      </Box>
    </Layout>
  );
};

export default AccountManager;
