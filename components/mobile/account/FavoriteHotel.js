import cookie from "js-cookie";
import { Box } from "@material-ui/core";
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import InfiniteScroll from "react-infinite-scroller";

import {
  isEmpty,
  paramsDefaultHotel,
  adapterHotelFavorite,
} from "@utils/helpers";
import { getListFavorite } from "@api/user";
import { TOKEN, listString } from "@utils/constants";
import * as actionTypes from "@contextProvider/system/ActionTypes";
import { useDispatchSystem } from "@contextProvider/ContextProvider";

import Layout from "@components/layout/mobile/Layout";
import LoadingMore from "@components/common/loading/LoadingMore";
import HotelItemListing from "@components/mobile/hotels/HotelItemListing";
import NoHotelFavorite from "@components/mobile/homes/NoHotelFavorite";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    color: theme.palette.black.black3,
    fontSize: 14,
    minHeight: "100%",
    borderTop: "1px solid #EDF2F7",
    paddingTop: 16,
  },
  styleImageBanner: {
    height: 164,
    borderRadius: 0,
  },
  divider: {
    height: 6,
    width: "calc(100% + 24px)",
    backgroundColor: theme.palette.gray.grayLight22,
    margin: "12px -12px",
  },
  loadingContent: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "calc(100vh - 94px)",
  },
}));

const pageSize = 40;
const listStatus = {
  INIT_STATE: "INIT_STATE",
  FIRST_REQUEST: "FIRST_REQUEST",
};
const HotelFavorites = ({}) => {
  const classes = useStyles();
  const [page, setPage] = useState(1);
  const [listHotel, setListHotel] = useState([]);
  const [totalHotel, setTotalHotel] = useState(0);
  const [loading, setLoading] = useState(true);
  const [status, setStatus] = useState(listStatus.INIT_STATE);
  const dispatch = useDispatchSystem();

  useEffect(() => {
    if (!isEmpty(cookie.get(TOKEN))) {
      fetHotelFavoritesAuth();
    }
  }, []);

  const fetHotelFavoritesAuth = async () => {
    try {
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: true,
      });
      const { data } = await getListFavorite({ page: 1, size: 40 });
      if (status === listStatus.INIT_STATE) {
        setStatus(listStatus.FIRST_REQUEST);
      }
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
      if (data.code === 200) {
        setListHotel(data.data.items || []);
        setTotalHotel(data.data.total || 0);
      } else {
      }
    } catch {
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
      if (status === listStatus.INIT_STATE) {
        setStatus(listStatus.FIRST_REQUEST);
      }
    }
  };

  const loadFunc = () => {};
  return (
    <Layout
      isHeaderBack
      isShowTitleInit
      titleHeaderBack={listString.IDS_MT_TEXT_HOTEL_FAVORITE}
    >
      <Box className={classes.container}>
        <Box position="relative">
          {status === listStatus.INIT_STATE ? (
            ""
          ) : !isEmpty(listHotel) ? (
            <InfiniteScroll
              pageStart={page}
              loadMore={loadFunc}
              hasMore={loading}
            >
              {adapterHotelFavorite(listHotel).map((el, index) => (
                <Box key={el.id} px={12 / 8}>
                  <HotelItemListing
                    styleHotelItem={classes.styleHotelItem}
                    item={el}
                    paramsFilterInit={paramsDefaultHotel()}
                  />
                  {index !== totalHotel - 1 && (
                    <Box className={classes.divider} />
                  )}
                </Box>
              ))}
              {!loading && page * pageSize < totalHotel && <LoadingMore />}
            </InfiniteScroll>
          ) : (
            <NoHotelFavorite />
          )}
        </Box>
      </Box>
    </Layout>
  );
};

export default HotelFavorites;
