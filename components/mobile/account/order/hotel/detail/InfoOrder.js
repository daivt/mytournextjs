import clsx from "clsx";
import moment from "moment";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { Box, Grid, makeStyles, Typography } from "@material-ui/core";

import { listString } from "@utils/constants";
import { paymentStatus, isEmpty } from "@utils/helpers";
import { IconArrowLeft, IconDot, IconClock } from "@public/icons";

import InfoPayment from "@components/mobile/account/order/hotel/detail/InfoPayment";
import InfoBookingDetails from "components/mobile/hotels/resultBooking/InfoBookingDetails";

const useStyles = makeStyles((theme) => ({
  divider: {
    height: 6,
    background: theme.palette.gray.grayLight22,
  },
  bookingDate: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    color: theme.palette.black.black3,
    marginTop: 16,
    display: "flex",
    justifyContent: "space-between",
  },
  orderCode: {
    border: `1px dashed ${theme.palette.gray.grayLight25}`,
    background: theme.palette.gray.grayLight26,
    padding: "12px 24px 12px 24px",
    marginTop: 12,
    borderRadius: 8,
    display: "flex",
    flexDirection: "column",
  },
  wrapBookingCode: {
    display: "flex",
    justifyContent: "space-between",
  },
  code: {
    fontSize: 18,
    lineHeight: "22px",
    fontWeight: 600,
    marginTop: 2,
    textAlign: "center",
  },

  wraperHeader: {
    padding: "12px 16px",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    height: 48,
    display: "flex",
    alignItems: "center",
  },
  wrapHeaderDraw: {
    display: "flex",
    alignItems: "center",
    height: 48,
    borderBottom: "1px solid #EDF2F7",
    width: "100%",
  },
  textFilter: {
    textAlign: "center",
    width: "calc(100% - 40px)",
  },
  paperAnchorBottom: {
    borderRadius: 8,
    maxHeight: "98%",
  },
  wrapUse: {
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    position: "fixed",
    bottom: 0,
  },
  wrapBtn: {
    margin: "7px 16px",
  },
  wrapCustomer: {
    margin: 16,
    fontWeight: 600,
    fontSize: 16,
    color: theme.palette.black.black3,
  },
  infoContact: {
    lineHeight: "19px",
  },
  userName: {
    marginTop: 8,
    lineHeight: "17px",
  },
  usePhone: {
    marginTop: 4,
    fontWweight: 400,
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark8,
    display: "flex",
    alignItems: "center",
  },
  wrapInvoice: {
    margin: 16,
    fontSize: 14,
    color: theme.palette.black.black3,
  },
  exportInvoice: {
    marginTop: 8,
    lineHeight: "17px",
    fontWeight: 600,
    fontSize: 16,
  },
  wrapBoxLeft: {
    fontWeight: "normal",
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark8,
  },

  wrapBoxRight: {
    fontWeight: 400,
    fontSize: 14,
    lineHeight: "17px",
    textAlign: "right",
    color: theme.palette.black.black3,
  },
  invoice: {
    marginTop: 12,
    justifyContent: "space-between",
    display: "flex",
  },
  wrapContainerTop: {
    marginTop: 12,
  },

  // ========= Start style of info details price ========= //
  itemPrice: {
    display: "flex",
    justifyContent: "space-between",
    lineHeight: "17px",
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
    padding: "14px 0 12px",
    position: "relative",
    fontSize: 14,
  },
  iconArrowRight: {
    transform: "rotate(270deg)",
    stroke: theme.palette.blue.blueLight8,
  },
  textUppercase: {
    textTransform: "uppercase",
  },
  totalPrice: {
    borderBottom: "none",
  },
  boxFotter: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    zIndex: 1000,
    bottom: 0,
    left: 0,
    position: "fixed",
    color: "#000",
    background: "#fff",
    width: "100%",
    padding: "8px 16px",
    borderTop: "2px solid #EDF2F7",
  },
  wrapPromo: {
    height: 24,
    whiteSpace: "nowrap",
    display: "flex",
    alignItems: "center",
  },
  promo: {
    border: `1px solid ${theme.palette.gray.grayLight24}`,
    padding: "4px 6px",
    marginLeft: 8,
    borderRadius: 4,
  },
  textPromo: {
    fontSize: 14,
    lineHeight: "17px",
    display: "flex",
  },
  wrapDetailsPrice: {
    margin: 16,
  },
  wrapBookingDate: {
    margin: "0 16px",
  },
  // ========= End style of info details price ========= //
  wrapHeader: {
    fontSize: 24,
    lineHeight: "26px",
    color: theme.palette.white.main,
    padding: 16,
  },
  wrapIcon: {
    display: "flex",
    justifyContent: "space-between",
  },
  icon: {
    stroke: theme.palette.white.main,
  },
  iconShare: {
    margin: "12px 16px 0 0 ",
  },
  requestSpecial: {
    display: "flex",
    flexDirection: "column",
  },
  iconClock: {
    stroke: theme.palette.white.main,
  },
}));
let interval = null;
const InfoOrder = ({ item = {}, infoPayment = {}, styleContainer = "" }) => {
  const router = useRouter();
  const classes = useStyles();
  const [timer, setTimer] = useState({
    hours: "00",
    minutes: "00",
    seconds: "00",
  });
  const [isShowCountdown, setIsShowCountdown] = useState(false);

  const roomBookings = !isEmpty(item.roomBookings) ? item.roomBookings : [];
  const roomBooking = !isEmpty(roomBookings) ? roomBookings[0] : {};

  let expiredTime = !isEmpty(item?.expiredTime)
    ? moment(item?.expiredTime, "YYYY-MM-DD HH:mm:ss")
    : moment();

  useEffect(() => {
    const startTime = moment();
    const endTime = !isEmpty(item?.expiredTime)
      ? moment(item?.expiredTime, "YYYY-MM-DD HH:mm:ss")
      : moment();
    const diffTime = endTime.valueOf() - startTime.valueOf();
    let duration = moment.duration(diffTime, "milliseconds");
    if (diffTime > 0) {
      setIsShowCountdown(true);
      interval = setInterval(() => {
        duration = moment.duration(duration - 1000, "milliseconds");
        setTimer({
          hours:
            duration.hours() < 10 ? `0${duration.hours()}` : duration.hours(),
          minutes:
            duration.minutes() < 10
              ? `0${duration.minutes()}`
              : duration.minutes(),
          seconds:
            duration.seconds() < 10
              ? `0${duration.seconds()}`
              : duration.seconds(),
        });
        if (
          duration.hours() === 0 &&
          duration.minutes() === 0 &&
          duration.seconds() === 0
        ) {
          setIsShowCountdown(false);
          clearInterval(interval);
        }
      }, 1000);
    }
    return () => {
      clearInterval(interval);
    };
  }, [infoPayment]);

  const bookingDate = !isEmpty(item.bookingDate)
    ? moment(item?.bookingDate, "YYYY-MM-DD HH:mm:ss").format(
        "DD/MM/YYYY HH:mm:ss"
      )
    : "";
  const _handleBack = () => {
    router.back();
  };

  const getNight = () => {
    let checkIn = moment(item.checkIn, "DD-MM-YYYY");
    let checkOut = moment(item.checkOut, "DD-MM-YYYY");
    if (checkIn && checkOut) {
      return Math.abs(
        moment(checkIn)
          .startOf("day")
          .diff(checkOut.startOf("day"), "days")
      );
    }
    return 1;
  };

  // tong gia chua app ma khuyen mai
  const totalPrice = () => {
    if (item.numRooms && item.price && getNight()) {
      return item.numRooms * getNight() * item.price;
    } else {
      return 0;
    }
  };

  // tong gia cac dem
  const nightPrice = () => {
    if (item.numRooms && item.basePrice && getNight()) {
      return item.numRooms * getNight() * item.basePrice;
    } else {
      return 0;
    }
  };

  // giam gia makup
  const getDiscount = () => {
    return roomBookings
      .map((el) => Math.abs(el.totalDiscount))
      .reduce((a, b) => a + b, 0);
  };

  // thue va phi dich vu khach san
  const vatPrice = () => {
    return totalPrice() - nightPrice() + getDiscount() - totalSurcharge();
  };

  // phu phi
  const totalSurcharge = () => {
    return roomBookings
      .map((el) => el.totalSurcharge)
      .reduce((a, b) => a + b, 0);
  };

  return (
    <>
      <Box className={classes.wrapHeader} bgcolor={paymentStatus(item).color}>
        <Box className={classes.wrapIcon}>
          <Box onClick={_handleBack}>
            <IconArrowLeft className={`svgFillAll ${clsx(classes.icon)}`} />
          </Box>
        </Box>
        <Box fontWeight={600} pt={2}>
          {paymentStatus(item).title}
        </Box>
        {paymentStatus(item).isShowCountDown && isShowCountdown && (
          <Box
            display="flex"
            alignItems="center"
            color="white.main"
            fontSize={14}
            lineHeight="17px"
            pt={4 / 8}
          >
            <IconClock className={`svgFillAll ${classes.iconClock}`} />
            <Box pl={2 / 8} component="span">
              <Box component="span">{`Còn `}</Box>
              <Box component="span">{timer.hours}</Box>
              <Box
                component="span"
                className={classes.itemBetween}
              >{` giờ `}</Box>
              <Box component="span">{timer.minutes}</Box>
              <Box
                component="span"
                className={classes.itemBetween}
              >{` phút `}</Box>
              <Box component="span">{timer.seconds}</Box>
              <Box
                component="span"
                className={classes.itemBetween}
              >{` giây `}</Box>
            </Box>
          </Box>
        )}
      </Box>
      <Box className={clsx(classes.wrapContainer, styleContainer)}>
        <InfoBookingDetails
          hotelDetail={item.hotel}
          bookingDetails={item}
          isDrawer={false}
        />
      </Box>
      <Box className={classes.divider} />
      <Box className={classes.wrapBookingDate}>
        <Box className={classes.bookingDate}>
          {listString.IDS_MT_TEXT_DATE_BOOKING}
          <Box component="span">{bookingDate}</Box>
        </Box>
        <Box className={classes.wrapBookingCode}>
          <Box className={classes.orderCode}>
            {listString.IDS_TEXT_FLIGHT_ORDER_CODE}
            <Typography
              component="span"
              color="secondary"
              className={classes.code}
            >
              {item?.orderCode}
            </Typography>
          </Box>
          <Box className={classes.orderCode}>
            {listString.IDS_TEXT_FLIGHT_ROOM_CODE}
            <Typography
              component="span"
              color="primary"
              className={classes.code}
            >
              {item?.partnerBookingCode || "Chưa có mã"}
            </Typography>
          </Box>
        </Box>
        {!isEmpty(infoPayment) && paymentStatus(item).isShowCountDown && (
          <Box pt={2}>
            <Box color="orange.main">
              <Box component="span">{`${
                listString.IDS_TEXT_FLIGHT_BOOKING_BANK_TRANSFER_2
              } ${expiredTime.format("HH:mm")}, ${expiredTime
                .locale("vi_VN")
                .format("ddd")
                .replace("T2", "Thứ hai")
                .replace("T3", "Thứ ba")
                .replace("T4", "Thứ tư")
                .replace("T5", "Thứ năm")
                .replace("T6", "Thứ sáu")
                .replace("T7", "Thứ bảy")
                .replace("CN", "Chủ Nhật")}, ngày ${expiredTime.format(
                "DD/MM/YYYY"
              )}`}</Box>
            </Box>
            <InfoPayment infoPayment={infoPayment} />
          </Box>
        )}
      </Box>
      <Box className={classes.divider} style={{ marginTop: 16 }} />
      <Box className={clsx(classes.wrapCustomer, styleContainer)}>
        <Box className={classes.infoContact}>
          {listString.IDS_MT_TEXT_INFO_CONTACT}
        </Box>
        <Box className={classes.userName}>{item?.customer?.name}</Box>
        <Box className={classes.usePhone}>
          {item?.customer?.phone}{" "}
          <Box mx={1}>
            {" "}
            <IconDot />
          </Box>{" "}
          {item?.customer?.email}
        </Box>
      </Box>
      <Box className={classes.divider} />
      <Box className={classes.wrapInvoice}>
        {(roomBooking.isHighFloor || roomBooking.noSmoking) && (
          <Box className={classes.requestSpecial}>
            <Box fontWeight={600} fontSize={16} lineHeight="19px" pb={1}>
              {listString.IDS_MT_TEXT_REQUEST_SPECIAL}
            </Box>
            {roomBooking.isHighFloor && (
              <Box display="flex" alignItems="center">
                <IconDot />
                <Box component="span" lineHeight="24px" pl={1}>
                  {listString.IDS_MT_ROOM_HIGH_LEVEL}
                </Box>
              </Box>
            )}
            {roomBooking.noSmoking && (
              <Box display="flex" alignItems="center">
                <IconDot />
                <Box component="span" lineHeight="24px" pl={1}>
                  {listString.IDS_MT_NO_SMOKE_ROOM}
                </Box>
              </Box>
            )}
          </Box>
        )}

        {!isEmpty(roomBooking.specialRequest) && (
          <Box className={classes.userName}>
            <Box fontWeight={600} fontSize={16} lineHeight="19px">
              {`${listString.IDS_MT_TEXT_BOOKING_REQUEST_PRIVATE}:`}
              <Box
                component="span"
                pl={4 / 8}
                fontWeight="normal"
                fontSize={14}
              >
                {roomBooking.specialRequest}
              </Box>
            </Box>
          </Box>
        )}

        {!isEmpty(item?.invoiceInfo?.taxNumber) && (
          <Box className={classes.exportInvoice}>
            {listString.IDS_MT_TEXT_INVOICE}:
            <Box>
              <Grid container className={classes.wrapContainerTop}>
                <Grid item xs={4}>
                  <Box className={classes.wrapBoxLeft}>
                    {listString.IDS_MT_TEXT_COMPANY_NAME}{" "}
                  </Box>
                </Grid>

                <Grid item xs={8}>
                  <Box className={classes.wrapBoxRight}>
                    {item?.invoiceInfo?.companyName}
                  </Box>
                </Grid>
              </Grid>

              <Grid container className={classes.wrapContainerTop}>
                <Grid item xs={4}>
                  <Box className={classes.wrapBoxLeft}>
                    {listString.IDS_MT_TEXT_CODE_INVOICE}{" "}
                  </Box>
                </Grid>

                <Grid item xs={8}>
                  <Box className={classes.wrapBoxRight}>
                    {item?.invoiceInfo?.taxNumber}
                  </Box>
                </Grid>
              </Grid>

              <Grid container className={classes.wrapContainerTop}>
                <Grid item xs={4}>
                  <Box className={classes.wrapBoxLeft}>
                    {listString.IDS_MT_TEXT_ADDRESS_COMPANY}{" "}
                  </Box>
                </Grid>

                <Grid item xs={8}>
                  <Box className={classes.wrapBoxRight}>
                    {item?.invoiceInfo?.companyAddress}
                  </Box>
                </Grid>
              </Grid>
              <Grid container className={classes.wrapContainerTop}>
                <Grid item xs={5}>
                  <Box className={classes.wrapBoxLeft}>
                    {listString.IDS_MT_TEXT_ADDRESS_INVOICE}
                  </Box>
                </Grid>

                <Grid item xs={7}>
                  <Box className={classes.wrapBoxRight}>
                    {item?.invoiceInfo?.recipientAddress}
                  </Box>
                </Grid>
              </Grid>
              <Grid container className={classes.wrapContainerTop}>
                <Grid item xs={5}>
                  <Box className={classes.wrapBoxLeft}>
                    {listString.IDS_TEXT_PLACEHOLDER_INVOICE_CUSTOMER_NAME}
                  </Box>
                </Grid>

                <Grid item xs={7}>
                  <Box className={classes.wrapBoxRight}>
                    {item?.invoiceInfo?.recipientName}
                  </Box>
                </Grid>
              </Grid>
              <Grid container className={classes.wrapContainerTop}>
                <Grid item xs={5}>
                  <Box className={classes.wrapBoxLeft}>
                    {listString.IDS_MT_TEXT_PHONE_NUMBER}
                  </Box>
                </Grid>

                <Grid item xs={7}>
                  <Box className={classes.wrapBoxRight}>
                    {item?.invoiceInfo?.recipientPhone}
                  </Box>
                </Grid>
              </Grid>
              <Grid container className={classes.wrapContainerTop}>
                <Grid item xs={5}>
                  <Box className={classes.wrapBoxLeft}>
                    {listString.IDS_MT_TEXT_EMAIL}
                  </Box>
                </Grid>

                <Grid item xs={7}>
                  <Box className={classes.wrapBoxRight}>
                    {item?.invoiceInfo?.recipientEmail}
                  </Box>
                </Grid>
              </Grid>
            </Box>
          </Box>
        )}
      </Box>
      <Box className={classes.divider} />

      <Box className={classes.wrapDetailsPrice}>
        <Box
          component="span"
          color="black.black3"
          lineHeight="19px"
          fontWeight={600}
          fontSize={16}
          pb={12 / 8}
        >
          {listString.IDS_MT_TEXT_PRICE_DETAIL}
        </Box>

        <Box className={classes.itemPrice}>
          <Box component="span">{`${
            item?.numRooms
          } phòng x ${getNight()} đêm`}</Box>
          <Box component="span">{`${nightPrice().formatMoney()}đ`}</Box>
        </Box>

        {totalSurcharge() > 0 && (
          <Box className={classes.itemPrice}>
            <Box component="span">{`Phụ phí`}</Box>
            {`${totalSurcharge().formatMoney()}đ`}
          </Box>
        )}

        {getDiscount() > 0 && (
          <Box className={classes.itemPrice} color="green.greenLight7">
            <Box component="span">{`Chúng tôi khớp giá, giảm thêm`}</Box>
            {`-${getDiscount().formatMoney()}đ`}
          </Box>
        )}

        {vatPrice() > 2 && (
          <Box className={classes.itemPrice}>
            <Box component="span">{`${
              item.includedVAT ? "Thuế và phí " : "Phí "
            }dịch vụ khách sạn`}</Box>
            {`${vatPrice().formatMoney()}đ`}
          </Box>
        )}

        {!isEmpty(item.paymentMethodFee) && item.paymentMethodFee > 0 && (
          <Box className={classes.itemPrice}>
            <Box component="span">{`Phí giao dịch ngân hàng`}</Box>
            <Box component="span">{`${item?.paymentMethodFee?.formatMoney()}đ`}</Box>
          </Box>
        )}

        <Box className={classes.itemPrice}>
          <Box component="span" className={classes.wrapPromo}>
            {listString.IDS_TEXT_VOUCHER}{" "}
            {!isEmpty(item.promotionCode) && (
              <Box component="span" className={classes.promo}>
                <Typography
                  component="span"
                  className={classes.textPromo}
                  style={{
                    color: "#1A202C",
                  }}
                >
                  {item?.promotionCode?.toUpperCase()}
                </Typography>
              </Box>
            )}
          </Box>
          <Box display="flex" alignItems="center" color="blue.blueLight8">
            <Typography
              component="span"
              className={classes.textPromo}
              style={{
                color: "#48BB78",
              }}
            >
              {`${
                item?.discount > 0 ? "-" : ""
              }${item?.discount?.formatMoney()}đ`}
            </Typography>
          </Box>
        </Box>

        <Box
          className={clsx(classes.itemPrice, classes.totalPrice)}
          fontWeight={600}
        >
          <Box component="span">{listString.IDS_MT_TEXT_TOTAL_PRICE}</Box>
          <Box component="span" fontSize={18}>
            {`${item?.finalPrice?.formatMoney()}đ`}
          </Box>
        </Box>
        {!item.includedVAT && (
          <Box
            fontSize={12}
            lineHeight="14px"
            color="gray.grayDark8"
            textAlign="end"
          >
            Giá chưa bao gồm VAT
          </Box>
        )}
      </Box>
      <Box className={classes.divider} />
      <Box className={classes.wrapDetailsPrice}>
        <Box
          component="span"
          color="black.black3"
          lineHeight="19px"
          fontWeight={600}
          fontSize={16}
          pb={12 / 8}
        >
          {listString.IDS_MT_TEXT_PAYMENT}
        </Box>
        <Box className={classes.itemPrice} style={{ border: "none" }}>
          <Box component="span">{`${listString.IDS_MT_TEXT_PAYMENT_MENTHODS}`}</Box>
          <Box component="span" textAlign="end">{`${item.paymentMethod}`}</Box>
        </Box>
        <Box className={classes.itemPrice} style={{ border: "none" }}>
          <Box component="span">{`${listString.IDS_MT_TEXT_STATUS}`}</Box>
          <Box
            p="4px 8px"
            color={paymentStatus(item).paymentColor}
            style={{ backgroundColor: paymentStatus(item).bgPaymentColor }}
          >{`${paymentStatus(item).paymentStatus}`}</Box>
        </Box>
      </Box>
    </>
  );
};
InfoOrder.propTypes = {};
export default InfoOrder;
