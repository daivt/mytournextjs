import { Box } from "@material-ui/core";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";

import { flightPaymentMethodCode } from "@utils/constants";
import { getBookingDetails, getPaymentInfo } from "@api/hotels";

import Layout from "@components/layout/mobile/Layout";
import InfoOrder from "@components/mobile/account/order/hotel/detail/InfoOrder";

const useStyles = makeStyles((theme) => ({
  styleContainer: {
    color: theme.palette.black.black3,
    fontSize: 14,
  },
  loadingContent: {
    position: "absolute",
    top: 0,
    zIndex: 2,
    width: "100%",
    height: "calc(100%)",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(255,255,255,0.4)",
  },
}));

const MobileContent = () => {
  const classes = useStyles();
  const router = useRouter();
  const [infoOrder, setInfoOrder] = useState({});
  const [infoPayment, setInfoPayment] = useState({});
  const [fetchingData, setFetchingData] = useState(true);
  useEffect(() => {
    fetchOrderDetail();
  }, []);

  const fetchOrderDetail = async () => {
    try {
      const { data } = await getBookingDetails({
        id: router.query.id,
      });

      if (data.code === 200) {
        setInfoOrder(data.data || {});
        if (
          data.data.paymentMethodCode ===
          flightPaymentMethodCode.BANK_TRANSFER_2
        ) {
          fetchPaymentInfo({
            bookingCode: data.data.orderCode,
            paymentMethodId: data.data.paymentMethodId,
          });
        } else {
          setFetchingData(false);
        }
      }
    } catch (error) {
      setFetchingData(false);
    }
  };

  const fetchPaymentInfo = async (dataQuery = {}) => {
    try {
      const { data } = await getPaymentInfo(dataQuery);
      if (data.code === 200) {
        setInfoPayment(data.data || {});
      }
      setFetchingData(false);
    } catch (error) {
      setFetchingData(false);
    }
  };

  const LoadingContent = (
    <Box className={classes.loadingContent}>
      <img src="https://staticproxy.mytourcdn.com/0x0/themes/images/mloading.gif" />
    </Box>
  );

  if (fetchingData) {
    return LoadingContent;
  }
  return (
    <Layout>
      <Box className={classes.container}>
        <InfoOrder
          infoPayment={infoPayment}
          item={infoOrder}
          styleContainer={classes.styleContainer}
        />
      </Box>
    </Layout>
  );
};
export default MobileContent;
