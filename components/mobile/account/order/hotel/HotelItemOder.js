import clsx from "clsx";
import moment from "moment";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import { Box, Typography, IconButton } from "@material-ui/core";

import { listString, routeStatic } from "@utils/constants";
import { isEmpty, paymentStatus } from "@utils/helpers";
import { IconUser2, IconArrowDownToggle, IconMoonlight } from "@public/icons";

import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: theme.palette.white.main,
    borderRadius: 8,
    boxShadow: "0px 3px 3px rgba(0, 0, 0, 0.05)",
    padding: "12px 16px",
    color: theme.palette.black.black3,
    fontSize: 14,
  },
  header: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginRight: -12,
  },
  iconArrowDownToggle: {
    stroke: theme.palette.black.black3,
    transform: "rotate(270deg)",
  },
  diver: {
    height: 1,
    backgroundColor: theme.palette.gray.grayLight22,
    width: "100%",
    margin: "12px 0",
  },
  infoHotel: {
    display: "flex",
    justifyContent: "space-between",
  },
  imageHotel: {
    width: 80,
    height: 80,
    borderRadius: 8,
  },
  iconUser2: {
    height: 12,
    width: 12,
  },
  diverFull: {
    width: "calc(100% + 32px)",
    margin: "12px -16px",
  },
  infoBooking: {
    display: "flex",
    flexDirection: "column",
  },
  infoFooter: {
    display: "flex",
    justifyContent: "space-between",
  },
  status: {
    display: "flex",
    alignItems: "center",
    borderRadius: 4,
    height: 22,
    padding: " 0 6px",
    textTransform: "uppercase",
    color: theme.palette.white.main,
    fontSize: 12,
    fontWeight: 600,
    lineHeight: "14px",
  },
  timeCheckIn: {
    fontSize: 14,
    fontWeight: 600,
    lineHeight: "17px",
  },
  nightDay: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 34,
    height: 28,
    borderRadius: "100px",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    backgroundColor: theme.palette.white.main,
  },
}));

let interval = null;

const HotelItemOder = ({ item = {} }) => {
  const classes = useStyles();
  const hotel = item.hotel || {};
  const router = useRouter();
  const roomBookings = item.roomBookings || [];
  const checkIn = moment(item.checkIn, "DD-MM-YYYY");
  const checkOut = moment(item.checkOut, "DD-MM-YYYY");
  const [timer, setTimer] = useState({
    hours: "00",
    minutes: "00",
    seconds: "00",
  });
  const [isShowCountdown, setIsShowCountdown] = useState(false);

  useEffect(() => {
    const startTime = moment();
    const endTime = !isEmpty(item?.expiredTime)
      ? moment(item?.expiredTime, "YYYY-MM-DD HH:mm:ss")
      : moment();
    const diffTime = endTime.valueOf() - startTime.valueOf();
    let duration = moment.duration(diffTime, "milliseconds");

    if (diffTime > 0) {
      setIsShowCountdown(true);
      interval = setInterval(() => {
        duration = moment.duration(duration - 1000, "milliseconds");
        setTimer({
          hours:
            duration.hours() < 10 ? `0${duration.hours()}` : duration.hours(),
          minutes:
            duration.minutes() < 10
              ? `0${duration.minutes()}`
              : duration.minutes(),
          seconds:
            duration.seconds() < 10
              ? `0${duration.seconds()}`
              : duration.seconds(),
        });
        if (
          duration.hours() === 0 &&
          duration.minutes() === 0 &&
          duration.seconds() === 0
        ) {
          setIsShowCountdown(false);
          clearInterval(interval);
        }
      }, 1000);
    }
    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <Box
      className={classes.container}
      onClick={() => {
        router.push({
          pathname: routeStatic.ACCOUNT_ORDER_BOOKING_DETAIL.href,
          query: {
            id: item.id,
          },
        });
      }}
    >
      <Box className={classes.header}>
        <Box display="flex" alignItems="center" lineHeight="17px">
          <Box component="span">{`${listString.IDS_TEXT_FLIGHT_ORDER_CODE}: `}</Box>
          <Box
            component="span"
            color="blue.blueLight8"
            pl={2 / 8}
          >{`${item.orderCode}`}</Box>
        </Box>
        <IconButton>
          <IconArrowDownToggle
            className={`svgFillAll ${classes.iconArrowDownToggle}`}
          />
        </IconButton>
      </Box>
      <Box className={clsx(classes.diver, classes.diverFull)} />
      <Box className={classes.infoHotel}>
        <Box display="flex" flexDirection="column" pr={1}>
          <Box
            component="span"
            fontSize={16}
            fontWeight={600}
            lineHeight="19px"
          >
            {hotel?.name}
          </Box>
          <Box component="span" fontWeight={600} lineHeight="17px" py={1}>{`${
            item.numRooms
          }x ${!isEmpty(roomBookings) ? roomBookings[0].name : ""}`}</Box>
          <Box display="flex">
            <IconUser2 className={classes.iconUser2} />
            <Box component="span" pl={4 / 8} fontSize={12}>{`${item.numAdult +
              item.numChildren} người`}</Box>
          </Box>
        </Box>
        <Box>
          <Image
            srcImage={hotel?.thumbnail?.image.small}
            className={classes.imageHotel}
            borderRadiusProp="8px"
          />
        </Box>
      </Box>
      <Box className={classes.diver} />
      <Box className={classes.infoBooking}>
        <Box
          display="flex"
          justifyContent="space-between"
          lineHeight="17px"
          color="gray.grayDark8"
        >
          <Box component="span">{listString.IDS_MT_TEXT_CHECK_IN_ROOM}</Box>
          <Box component="span">{listString.IDS_MT_TEXT_CHECK_OUT_ROOM}</Box>
        </Box>
        <Box
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          py={2 / 8}
        >
          <Box>
            <Typography className={classes.timeCheckIn}>
              {checkIn
                ? checkIn
                    .locale("vi_VN")
                    .format("ddd")
                    .replace("T", "Thứ ")
                    .replace("CN", "Chủ Nhật")
                : "-"}
              <Box component="span">
                {", "}
                {checkIn.format("DD")} Tháng {checkIn.format("MM")}
              </Box>
            </Typography>
          </Box>
          <Box className={classes.nightDay}>
            <Box component="span" fontSize={11} lineHeight="13px" pr={2 / 8}>
              {checkIn && checkOut
                ? Math.abs(
                    moment(checkIn)
                      .startOf("day")
                      .diff(checkOut.startOf("day"), "days")
                  )
                : "-"}
            </Box>

            <IconMoonlight />
          </Box>
          <Box>
            <Typography className={classes.timeCheckIn}>
              {checkOut
                ? checkOut
                    .locale("vi_VN")
                    .format("ddd")
                    .replace("T", "Thứ ")
                    .replace("CN", "Chủ Nhật")
                : "-"}
              <Box component="span">
                {", "}
                {checkOut.format("DD")} Tháng {checkOut.format("MM")}
              </Box>
            </Typography>
          </Box>
        </Box>
        <Box
          display="flex"
          justifyContent="space-between"
          lineHeight="17px"
          color="gray.grayDark8"
        >
          <Box component="span">{hotel.checkInTime}</Box>
          <Box component="span">{hotel.checkOutTime}</Box>
        </Box>
      </Box>
      <Box className={classes.diver} />
      <Box className={classes.infoFooter}>
        <Box display="flex" lineHeight="17px">
          <Box component="span">{`${listString.IDS_MT_TEXT_TOTAL_PRICE}: `}</Box>
          <Box
            component="span"
            fontWeight={600}
            pl={2 / 8}
          >{`${item?.finalPrice?.formatMoney()}đ`}</Box>
        </Box>
        <Box className={classes.status} bgcolor={paymentStatus(item).color}>
          {paymentStatus(item).title}
        </Box>
      </Box>
      {paymentStatus(item).isShowCountDown && isShowCountdown && (
        <Box
          display="flex"
          justifyContent="flex-end"
          color="orange.main"
          pt={4 / 8}
        >
          <Box pl={2 / 8} component="span">
            <Box component="span">{`Còn `}</Box>
            <Box component="span">{timer.hours}</Box>
            <Box
              component="span"
              className={classes.itemBetween}
            >{` giờ `}</Box>
            <Box component="span">{timer.minutes}</Box>
            <Box
              component="span"
              className={classes.itemBetween}
            >{` phút `}</Box>
            <Box component="span">{timer.seconds}</Box>
            <Box
              component="span"
              className={classes.itemBetween}
            >{` giây `}</Box>
          </Box>
        </Box>
      )}
    </Box>
  );
};
export default HotelItemOder;
