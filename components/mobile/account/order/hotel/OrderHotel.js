import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import InfiniteScroll from "react-infinite-scroller";
import { Box, Typography } from "@material-ui/core";

import { isEmpty } from "@utils/helpers";
import { getMyBooking } from "@api/hotels";
import { listString, routeStatic } from "@utils/constants";
import { IconArrowDownToggle } from "@public/icons";

import Image from "@src/image/Image";
import ButtonComponent from "@src/button/Button";
import LoadingMore from "@components/common/loading/LoadingMore";
import HotelItemOder from "@components/mobile/account/order/hotel/HotelItemOder";
import FilterOderPopup from "@components/mobile/account/order/hotel/FilterOderPopup";
import OrderManager from "@components/mobile/account/order/OrderManager";

const useStyles = makeStyles((theme) => ({
  orderEmptyFirst: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "calc(100vh - 48px)",
    color: theme.palette.black.black3,
    fontSize: 14,
  },
  iconArrowDownToggle: {
    stroke: theme.palette.black.black3,
    marginLeft: 5,
  },
  viewMessageReview: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.black.black3,
    display: "flex",
    alignItems: "center",
  },
  loadingContent: {
    position: "absolute",
    top: 48,
    zIndex: 2,
    width: "100%",
    height: "calc(100% - 48px)",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(255,255,255,0.4)",
  },
  imageEmpty: {
    width: 249,
    height: 210,
  },
}));

let isFirstFetChing = true;
const listFilter = [
  {
    code: "all",
    label: "Tất cả",
  },
  {
    code: "success",
    label: "Thành công",
  },
  {
    code: "waiting",
    label: "Đang chờ thanh toán",
  },
  {
    code: "holding",
    label: "Đang giữ chỗ",
  },
  {
    code: "refunded",
    label: "Hoàn tiền",
  },
  {
    code: "fail",
    label: "Thất bại",
  },
];
const listTypeModal = {
  MODAL_FILTER_ODER: "MODAL_FILTER_ODER",
};
const pageSize = 10;
const OrderHotel = () => {
  const classes = useStyles();
  const [listBookingOrder, setListBookingOrder] = useState([]);
  const [valueFilter, setValueFilter] = useState("all");
  const [visibleDrawerType, setVisibleDrawerType] = useState("");
  const [totalHotel, setTotalHotel] = useState(0);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(true);
  const [fetchingData, setFetchingData] = useState(true);
  const router = useRouter();
  useEffect(() => {
    const params = {
      page: 1,
      size: pageSize,
    };
    fetMyBookingHotel(params);
    isFirstFetChing = true;
  }, []);

  const fetMyBookingHotel = async (params) => {
    isFirstFetChing = false;
    try {
      setFetchingData(true);
      const { data } = await getMyBooking(params);
      if (data.code === 200) {
        setListBookingOrder(data.data.items);
        setTotalHotel(data.data.total);
      }
      setFetchingData(false);
    } catch (error) {
      setFetchingData(false);
    }
  };

  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };

  const handleSelectedItemSort = (code = "") => (e) => {
    // let valueFilterTemp = [...valueFilter];
    // if (code === "all") {
    //   if (!valueFilter.includes("all")) {
    //     valueFilterTemp = ["all"];
    //   }
    // } else {
    //   if (valueFilterTemp.includes(code)) {
    //     valueFilterTemp = valueFilter.filter((el) => el !== code);
    //   } else {
    //     valueFilterTemp.push(code);
    //   }
    //   valueFilterTemp = valueFilterTemp.filter((el) => el !== "all");
    //   if (valueFilterTemp.length === 4 || valueFilterTemp.length === 0) {
    //     valueFilterTemp = ["all"];
    //   }
    // }
    // setValueFilter(valueFilterTemp);
    setValueFilter(code);
    setVisibleDrawerType("");
    let params = {
      page: 1,
      size: pageSize,
    };
    if (code !== "all") {
      params = {
        ...params,
        filter: {
          status: [code],
        },
      };
    }
    fetMyBookingHotel(params);
  };

  const loadFunc = async () => {
    const pageTemp = page + 1;
    try {
      setLoading(false);
      let params = {
        page: pageTemp,
        size: pageSize,
      };
      if (!valueFilter.includes("all")) {
        params = {
          ...params,
          filter: {
            status: [valueFilter],
          },
        };
      }
      const { data } = await getMyBooking(params);
      if (data.code === 200) {
        setPage(pageTemp);
        setListBookingOrder([...listBookingOrder, ...data.data.items]);
        setTotalHotel(data.data.total);
        if (pageTemp * pageSize < data?.data.total) {
          setLoading(true);
        }
      }
    } catch (error) {}
  };

  const getNameFilter = () => {
    const filterSelected = listFilter.find((el) => el.code === valueFilter);
    return filterSelected.label;
  };

  const OrderEmptyFirst = (
    <Box className={classes.orderEmptyFirst}>
      <Image
        srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_empty_order_hotel.svg"
        className={classes.imageEmpty}
      />
      <Box component="span" pb={2} pt={12 / 8} lineHeight="19px" fontSize={16}>
        {listString.IDS_MT_TEXT_DES_ODER_HOTEL}
      </Box>
      <ButtonComponent
        backgroundColor="#FF1284"
        borderRadius={8}
        height={48}
        padding="16px 30px"
        fontSize={16}
        fontWeight={600}
        width="fit-content"
        handleClick={() => {
          router.push({
            pathname: routeStatic.HOTEL.href,
          });
        }}
      >
        {listString.IDS_MT_TEXT_SEARCH_HOTEL}
      </ButtonComponent>
    </Box>
  );

  const OrderEmptyFilter = (
    <Box className={classes.orderEmptyFirst}>
      <Image
        srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_empty_order_hotel.svg"
        className={classes.imageEmpty}
      />
      <Box
        component="span"
        color="gray.grayDark7"
        pb={2}
        pt={12 / 8}
        lineHeight="19px"
        fontSize={16}
      >
        Không có kết quả
      </Box>
    </Box>
  );
  const LoadingContent = (
    <Box className={classes.loadingContent}>
      <img
        src="https://staticproxy.mytourcdn.com/0x0/themes/images/mloading.gif"
        className={classes.imgLoading}
      />
    </Box>
  );

  if (fetchingData) {
    return <OrderManager>{LoadingContent}</OrderManager>;
  }
  if (isEmpty(listBookingOrder) && isFirstFetChing) {
    return <OrderManager>{OrderEmptyFirst}</OrderManager>;
  }

  return (
    <OrderManager>
      <Box className={classes.container}>
        <Box pl={1} pt={4 / 8}>
          <ButtonComponent
            typeButton="text"
            color="#1A202C"
            width="fit-content"
            handleClick={() =>
              setVisibleDrawerType(listTypeModal.MODAL_FILTER_ODER)
            }
          >
            <Typography className={classes.viewMessageReview}>
              {getNameFilter()}
              <IconArrowDownToggle
                className={`svgFillAll ${classes.iconArrowDownToggle}`}
              />
            </Typography>
          </ButtonComponent>
        </Box>
        {isEmpty(listBookingOrder) ? (
          OrderEmptyFilter
        ) : (
          <Box px={12 / 8} position="relative">
            <InfiniteScroll
              pageStart={page}
              loadMore={loadFunc}
              hasMore={loading}
            >
              {listBookingOrder.map((el, index) => (
                <Box key={el.id} pb={12 / 8}>
                  <HotelItemOder item={el} />
                </Box>
              ))}
              {!loading && page * pageSize < totalHotel && <LoadingMore />}
            </InfiniteScroll>
          </Box>
        )}

        <FilterOderPopup
          openModal={visibleDrawerType === listTypeModal.MODAL_FILTER_ODER}
          toggleDrawerReview={toggleDrawer}
          handleSelectedItem={handleSelectedItemSort}
          valueFilter={valueFilter}
          listFilter={listFilter || []}
        />
      </Box>
    </OrderManager>
  );
};
export default OrderHotel;
