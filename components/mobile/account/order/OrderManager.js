import { Box, IconButton, Tabs, Tab } from "@material-ui/core";
import { useState, useEffect } from "react";
import { makeStyles, withStyles } from "@material-ui/styles";
import { useRouter } from "next/router";

import { IconBack } from "@public/icons";
import { listString, routeStatic } from "@utils/constants";
import { handleTitleFlight } from "@utils/helpers";

import Layout from "@components/layout/mobile/Layout";

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
  },
  content: {
    backgroundColor: theme.palette.gray.grayLight22,
    minHeight: "calc(100vh - 48px)",
  },
  header: {
    display: "flex",
    alignItems: "center",
  },
  tabItem: {
    textTransform: "none",
    fontWeight: 600,
    lineHeight: "17px",
    color: theme.palette.black.black3,
    opacity: 1,
    width: 120,
    "&.Mui-selected": {
      color: theme.palette.blue.blueLight8,
    },
  },
  indicator: {
    backgroundColor: theme.palette.gray.grayDark1,
  },
  tabPanel: {
    height: "100%",
  },
}));

export const TabsInient = withStyles((theme) => ({
  indicator: {
    background: theme.palette.blue.blueLight8,
  },
}))(Tabs);

const getValueTab = (router) => {
  switch (router.route) {
    case routeStatic.ACCOUNT_ORDER_BOOKING.href:
      return 0;
    case routeStatic.ACCOUNT_ORDER_FLIGHT.href:
      return 1;
    default:
      return 0;
  }
};

const MobileContent = ({ children = null, query = {}, codeFlight = {} }) => {
  const classes = useStyles();
  const router = useRouter();
  const [currentTab, setCurrentTab] = useState(getValueTab(router));

  useEffect(() => {
    setCurrentTab(getValueTab(router));
  }, [router]);

  const _handleBackRoute = () => {
    router.push({
      pathname: routeStatic.HOME.href,
    });
  };
  const handleChangeTab = (event, newValue) => {
    setCurrentTab(newValue);
    switch (newValue) {
      case 0: {
        router.push({
          pathname: routeStatic.ACCOUNT_ORDER_BOOKING.href,
        });
        break;
      }
      case 1: {
        router.push({
          pathname: routeStatic.ACCOUNT_ORDER_FLIGHT.href,
        });
        break;
      }
    }
  };

  return (
    <Layout {...handleTitleFlight(query, codeFlight)}>
      <Box className={classes.container}>
        <Box className={classes.header}>
          <Box pr={2}>
            <IconButton className={classes.btnBack} onClick={_handleBackRoute}>
              <IconBack />
            </IconButton>
          </Box>
          <Box>
            <TabsInient
              value={currentTab}
              onChange={handleChangeTab}
              classes={{ indicator: classes.indicator }}
              aria-label="full width tabs example"
            >
              <Tab
                label={listString.IDS_MT_TEXT_ORDER_HOTEL}
                className={classes.tabItem}
              />
              <Tab
                label={listString.IDS_MT_TEXT_FLIGHT}
                className={classes.tabItem}
              />
            </TabsInient>
          </Box>
        </Box>
        <Box className={classes.content}>{children || null}</Box>
      </Box>
    </Layout>
  );
};
export default MobileContent;
