import { Box, Typography, useTheme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconArrowFlightOrder, IconDot } from "@public/icons";
import Image from "@src/image/Image";
import { DATE_FORMAT } from "@utils/moment";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  airlineLogo: {
    width: 20,
    height: 20,
  },
  iconDot: {
    margin: 6,
  },
  airlineName: {
    color: theme.palette.black.black3,
    whiteSpace: "nowrap",
  },
  flightCode: {
    color: theme.palette.black.black3,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  warpAirBrand: {
    display: "flex",
    alignItems: "center",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    overflow: "hidden",
    marginLeft: 8,
    color: theme.palette.black.black3,
  },
}));

const WayFlightItemDetail = ({ item = {}, airlineInfo = {} }) => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Box display="flex" flexDirection="column">
      <Box display="flex" alignItems="center" justifyContent="space-between">
        <Typography variant="subtitle1">{item?.fromAirport}</Typography>
        <Box display="flex" alignItems="center" justifyContent="space-between">
          <div
            style={{
              background:
                "linear-gradient(270deg, #A0AEC0 0%, rgba(160, 174, 192, 0) 100%)",
              width: 24,
              height: 1,
              marginRight: 10,
            }}
          ></div>
          <IconArrowFlightOrder />
          <div
            style={{
              background:
                "linear-gradient(270deg, rgba(160, 174, 192, 0) 0%, #A0AEC0 100%)",
              width: 24,
              height: 1,
              marginLeft: 10,
            }}
          ></div>
        </Box>
        <Typography variant="subtitle1">{item?.toAirport}</Typography>
      </Box>
      <Box display="flex" alignItems="center" justifyContent="space-between">
        <Typography
          variant="body2"
          style={{ color: theme.palette.gray.grayDark8 }}
        >
          {item?.departureCity}
        </Typography>
        <Typography
          variant="body2"
          style={{ color: theme.palette.gray.grayDark8 }}
        >
          {item?.arrivalCity}
        </Typography>
      </Box>
      <Box display="flex" alignItems="center" justifyContent="space-between">
        <Typography variant="caption" style={{ lineHeight: "17px" }}>
          {`${item?.departureTime}, ${moment(
            item?.departureDate,
            DATE_FORMAT
          ).format("DD")} tháng ${moment(item?.arrivalDate, DATE_FORMAT).format(
            "MM"
          )}`}
        </Typography>
        <Typography variant="caption" style={{ lineHeight: "17px" }}>
          {`${item?.arrivalTime}, ${moment(
            item?.arrivalDate,
            DATE_FORMAT
          ).format("DD")} tháng ${moment(item?.arrivalDate, DATE_FORMAT).format(
            "MM"
          )}`}
        </Typography>
      </Box>
      <Box display="flex" alignItems="center">
        <Image srcImage={airlineInfo.logo} className={classes.airlineLogo} />
        <Box className={classes.warpAirBrand}>
          <Typography variant="body2" className={classes.airlineName}>
            {airlineInfo.name}
          </Typography>
          <IconDot className={classes.iconDot} />
          <Typography variant="body2" className={classes.flightCode}>
            {item?.flightCode}
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};
export default WayFlightItemDetail;
