import { getFlightBookings, getGeneralInformation } from "@api/flight";
import LoadingMore from "@components/common/loading/LoadingMore";
import FilterModal from "@components/mobile/account/order/flight/FilterModal";
import FlightBookingItem from "@components/mobile/account/order/flight/FlightBookingItem";
import { Box, Typography, useTheme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconArrowDownToggle } from "@public/icons";
import ButtonComponent from "@src/button/Button";
import { listString, routeStatic } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroller";
import OrderManager from "@components/mobile/account/order/OrderManager";
import Image from "@src/image/Image";
const useStyles = makeStyles((theme) => ({
  orderEmptyFirst: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "calc(100vh - 48px)",
    color: theme.palette.black.black3,
  },
  iconArrowDownToggle: {
    stroke: theme.palette.black.black3,
    marginLeft: 5,
  },
  viewMessageReview: {
    lineHeight: "17px",
    color: theme.palette.black.black3,
    display: "flex",
    alignItems: "center",
  },
  loadingContent: {
    position: "absolute",
    top: 48,
    zIndex: 2,
    width: "100%",
    height: "calc(100% - 48px)",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(255,255,255,0.4)",
  },
  imageEmpty: {
    width: 249,
    height: 210,
  },
}));

let firstTimeFetching = true;
const listFilter = [
  {
    code: "all",
    label: "Tất cả",
  },
  {
    code: "success",
    label: "Thành công",
  },
  {
    code: "holding",
    label: "Đang giữ chỗ",
  },
  {
    code: "fail",
    label: "Thất bại",
  },
];
const listTypeModal = {
  MODAL_FILTER_ODER: "MODAL_FILTER_ODER",
};
const firstPage = 1;
const pageSize = 20;
const OrderFlight = ({ query = {}, codeFlight = {} }) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [orderFlights, setOrderFlights] = useState([]);
  const [valueFilter, setValueFilter] = useState(["all"]);
  const [visibleDrawerType, setVisibleDrawerType] = useState("");
  const [totalFlights, setTotalFlights] = useState(0);
  const [pageNumber, setPageNumber] = useState(1);
  const [loading, setLoading] = useState(true);
  const [fetchingData, setFetchingData] = useState(true);
  const [airlines, setAirlines] = useState();
  const actionsGeneralInformation = async () => {
    const { data } = await getGeneralInformation();
    setAirlines(data?.data?.airlines);
  };

  const fetchBookingFlight = async (params) => {
    firstTimeFetching = false;
    try {
      setFetchingData(true);
      const { data } = await getFlightBookings(params);
      if (data?.code !== 200) {
        data?.message &&
          enqueueSnackbar(
            data?.message,
            snackbarSetting((key) => closeSnackbar(key), { color: "error" })
          );
      }
      setOrderFlights(data?.data);
      setTotalFlights(data?.total);
      setFetchingData(false);
    } catch (error) {
      setFetchingData(false);
    }
  };

  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };

  const handleSelectedItemSort = (code = "") => (e) => {
    let valueFilterTemp = [];
    valueFilterTemp.push(code);
    if (code === "success") {
      valueFilterTemp.push("completed");
    }
    setValueFilter(valueFilterTemp);
    setVisibleDrawerType("");
    let params = {
      paging: {
        page: firstPage,
        size: pageSize,
      },
    };
    if (!valueFilterTemp.includes("all")) {
      params = {
        ...params,
        filters: {
          paymentStatuses: valueFilterTemp,
        },
      };
    }
    fetchBookingFlight(params);
  };

  const loadFunc = async () => {
    const pageTemp = pageNumber + 1;
    try {
      setLoading(false);
      let params = {
        paging: {
          page: pageTemp,
          size: pageSize,
        },
      };
      if (!valueFilter.includes("all")) {
        params = {
          ...params,
          filter: {
            paymentStatuses: valueFilter,
          },
        };
      }
      const { data } = await getFlightBookings(params);
      if (data.code === 200) {
        setPageNumber(pageTemp);
        setOrderFlights([...orderFlights, ...data?.data]);
        setTotalFlights(data?.total);
        if (pageTemp * pageSize < data?.total) {
          setLoading(true);
        }
      }
    } catch (error) {}
  };

  useEffect(() => {
    const params = {
      filters: {
        paymentStatuses: ["all"],
      },
      paging: {
        page: pageNumber,
        size: pageSize,
      },
    };
    fetchBookingFlight(params);
    firstTimeFetching = true;
    actionsGeneralInformation();
  }, []);

  const getNameFilter = () => {
    const filterSelected = listFilter.find((el) =>
      valueFilter.includes(el.code)
    );
    return filterSelected.label;
  };

  const OrderEmptyFirst = (
    <OrderManager query={query} codeFlight={codeFlight}>
      <Box className={classes.orderEmptyFirst}>
        <Image
          srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_empty_order_flight.svg"
          className={classes.imageEmpty}
        />
        <Typography variant="body1" style={{ padding: "12px 0 16px 0" }}>
          {listString.IDS_MT_TEXT_DES_ODER_FLIGHT}
        </Typography>
        <ButtonComponent
          backgroundColor={theme.palette.pink.main}
          borderRadius={8}
          height={48}
          padding="0 12px"
          width="fit-content"
          handleClick={() => {
            router.push({
              pathname: routeStatic.FLIGHT.href,
            });
          }}
        >
          <Typography variant="subtitle1" component="span">
            {listString.IDS_MT_TEXT_FIND_FLIGHT}
          </Typography>
        </ButtonComponent>
      </Box>
    </OrderManager>
  );

  const OrderEmptyFilter = (
    <Box className={classes.orderEmptyFirst}>
      <Image
        srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_empty_order_flight.svg"
        className={classes.imageEmpty}
      />
      <Box component="span" color="gray.grayDark7" pb={2} pt={12 / 8}>
        <Typography variant="body1" component="span">
          {listString.IDS_MT_TEXT_NO_RESULT}
        </Typography>
      </Box>
    </Box>
  );

  const LoadingContent = (
    <Box className={classes.loadingContent}>
      <img
        src="https://staticproxy.mytourcdn.com/0x0/themes/images/mloading.gif"
        className={classes.imgLoading}
      />
    </Box>
  );

  if (fetchingData) {
    return <OrderManager>{LoadingContent}</OrderManager>;
  }

  if (isEmpty(orderFlights) && firstTimeFetching) return OrderEmptyFirst;

  return (
    <OrderManager>
      <Box className={classes.container}>
        <Box pl={1} pt={4 / 8}>
          <ButtonComponent
            typeButton="text"
            color={theme.palette.black.black3}
            width="fit-content"
            handleClick={() =>
              setVisibleDrawerType(listTypeModal.MODAL_FILTER_ODER)
            }
          >
            <Typography variant="caption" className={classes.viewMessageReview}>
              {getNameFilter()}
              <IconArrowDownToggle
                className={`svgFillAll ${classes.iconArrowDownToggle}`}
              />
            </Typography>
          </ButtonComponent>
        </Box>
        {isEmpty(orderFlights) ? (
          OrderEmptyFilter
        ) : (
          <Box px={12 / 8} position="relative">
            <InfiniteScroll
              pageStart={pageNumber}
              loadMore={loadFunc}
              hasMore={loading}
            >
              {orderFlights.map((el, index) => (
                <Box key={index} pb={12 / 8}>
                  <FlightBookingItem item={el} airlines={airlines} />
                </Box>
              ))}
              {!loading && pageNumber * pageSize < totalFlights && (
                <LoadingMore />
              )}
            </InfiniteScroll>
          </Box>
        )}
        <FilterModal
          openModal={visibleDrawerType === listTypeModal.MODAL_FILTER_ODER}
          toggleDrawerReview={toggleDrawer}
          handleSelectedItem={handleSelectedItemSort}
          valueFilter={valueFilter}
          listFilter={listFilter || []}
        />
      </Box>
    </OrderManager>
  );
};
export default OrderFlight;
