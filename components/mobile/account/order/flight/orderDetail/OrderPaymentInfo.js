import { Box, makeStyles, Typography, useTheme } from "@material-ui/core";
import { IconInfo } from "@public/icons";
import ButtonComponent from "@src/button/Button";
import { flightPaymentStatus, listString, routeStatic } from "@utils/constants";
import { useRouter } from "next/router";
import { useState } from "react";
const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    justifyContent: "space-between",
    textAlign: "center",
  },
  container: {
    backgroundColor: theme.palette.white.main,
    bottom: 0,
    width: "100vw",
    position: "fixed",
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    padding: "8px 16px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    zIndex: 99,
  },
  leftBox: {
    display: "flex",
    flexDirection: "column",
    width: "50%",
  },
  numGuests: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    color: theme.palette.gray.grayDark8,
  },
  mainPrice: {
    textDecorationLine: "line-through",
    color: theme.palette.gray.grayDark8,
    lineHeight: "16px",
  },
  rightBox: {
    width: "50%",
  },
}));

const OrderPaymentInfo = ({ orderDetail = {}, paymentStatus = {} }) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const handleClickPayment = () => {
    router.push({
      pathname: routeStatic.REPAY_BOOKING_FLIGHT.href,
      query: {
        bookingId: orderDetail?.orderCode.substr(1),
      },
    });
  };
  const [visiblePayment, setVisiblePayment] = useState(false);
  window.onscroll = function() {
    setVisiblePayment(window.pageYOffset === 0);
  };
  return (
    <>
      <Box
        display="flex"
        flexDirection="column"
        pt={2}
        pr={2}
        pb={5}
        pl={2}
        marginBottom={
          paymentStatus.type === flightPaymentStatus.HOLDING ||
          paymentStatus.type === flightPaymentStatus.PENDING
            ? "60px"
            : "0"
        }
      >
        <Typography variant="subtitle1">
          {listString.IDS_MT_TEXT_PAYMENT_ACTION}
        </Typography>
        <Box className={classes.content}>
          <Typography variant="caption">
            {listString.IDS_TEXT_ORDER_PAYMENT_TOTAL_PRICE}
          </Typography>
          <Typography variant="caption">
            {orderDetail.finalPriceFormatted}
          </Typography>
        </Box>
        <Box className={classes.content}>
          <Typography variant="caption">
            {listString.IDS_TEXT_ORDER_PAYMENT_METHOD}
          </Typography>
          <Typography
            variant="caption"
            style={{
              width: "50%",
              textAlign: "end",
            }}
          >
            {orderDetail.paymentMethod}
          </Typography>
        </Box>
      </Box>
      {(paymentStatus.type === flightPaymentStatus.HOLDING ||
        paymentStatus.type === flightPaymentStatus.PENDING) &&
        !visiblePayment && (
          <Box className={classes.container}>
            <Box className={classes.leftBox}>
              <Box className={classes.numGuests}>
                <Typography variant="body2">
                  {`Tổng (${orderDetail.numGuests} khách)`}
                </Typography>
                <IconInfo
                  className="svgFillAll"
                  style={{
                    stroke: theme.palette.blue.blueLight8,
                    marginLeft: 4,
                  }}
                />
              </Box>
              <Typography variant="h6">
                {orderDetail.finalPriceFormatted}
              </Typography>
              {orderDetail?.discount > 0 && (
                <Typography variant="caption" className={classes.mainPrice}>
                  {`${parseInt(
                    orderDetail.discount + orderDetail.finalPrice
                  ).formatMoney()} đ`}
                </Typography>
              )}
            </Box>
            <ButtonComponent
              type="submit"
              backgroundColor={theme.palette.secondary.main}
              height={48}
              borderRadius={8}
              handleClick={handleClickPayment}
              className={classes.rightBox}
            >
              <Typography variant="subtitle1">
                {listString.IDS_MT_TEXT_PAYMENT}
              </Typography>
            </ButtonComponent>
          </Box>
        )}
      {(paymentStatus.type === flightPaymentStatus.HOLDING ||
        paymentStatus.type === flightPaymentStatus.PENDING) &&
        visiblePayment && (
          <Box className={classes.container}>
            <ButtonComponent
              type="submit"
              backgroundColor={theme.palette.secondary.main}
              height={48}
              borderRadius={8}
              handleClick={handleClickPayment}
            >
              <Typography variant="subtitle1">
                {listString.IDS_MT_TEXT_PAYMENT_NOW}
              </Typography>
            </ButtonComponent>
          </Box>
        )}
    </>
  );
};

export default OrderPaymentInfo;
