import { Box, makeStyles, Typography } from "@material-ui/core";
import { listString } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    justifyContent: "space-between",
    textAlign: "center",
  },
  rightContent: {
    wordWrap: "break-word",
    maxWidth: "60%",
    textAlign: "end",
  },
}));

const OrderContact = ({ item = {} }) => {
  const classes = useStyles();
  return (
    <Box display="flex" flexDirection="column" pt={2} pr={2} pb={12 / 8} pl={2}>
      <Typography variant="subtitle1">
        {listString.IDS_MT_TEXT_INFO_CONTACT}
      </Typography>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_MT_TEXT_CONTACT_NAME_SIMPLE}
        </Typography>
        <Typography variant="caption" className={classes.rightContent}>
          {item?.fullName.toUpperCase()}
        </Typography>
      </Box>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_MT_TEXT_PHONE_NUMBER}
        </Typography>
        <Typography variant="caption" className={classes.rightContent}>
          {item?.phone1 ? item.phone1 : item?.phone2 ? item.phone2 : null}
        </Typography>
      </Box>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_MT_TEXT_EMAIL}
        </Typography>
        <Typography variant="caption" className={classes.rightContent}>
          {item.email}
        </Typography>
      </Box>
    </Box>
  );
};

export default OrderContact;
