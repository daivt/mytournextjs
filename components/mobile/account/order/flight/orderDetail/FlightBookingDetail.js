import { getBankTransferInfo, getGeneralInformation } from "@api/flight";
import FlightTicketDetailPayment from "@components/common/card/flight/FlightTicketDetailPayment";
import OrderContact from "@components/mobile/account/order/flight/orderDetail/OrderContact";
import OrderCustomerInfo from "@components/mobile/account/order/flight/orderDetail/OrderCustomerInfo";
import OrderInfo from "@components/mobile/account/order/flight/orderDetail/OrderInfo";
import OrderInsurance from "@components/mobile/account/order/flight/orderDetail/OrderInsurance";
import OrderPaymentInfo from "@components/mobile/account/order/flight/orderDetail/OrderPaymentInfo";
import OrderExportInvoice from "@components/mobile/flight/payment/orderDetail/OrderExportInvoice";
import {
  Box,
  Grid,
  IconButton,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import {
  IconBack,
  IconChangeBaggage,
  IconChangeFlightTime,
  IconClock,
  IconRefundFlight,
  IconShare,
  IconSplitFlightCode,
} from "@public/icons";
import utilStyles from "@styles/utilStyles";
import {
  flightPaymentMethodCode,
  flightPaymentStatus,
  listString,
} from "@utils/constants";
import {
  getExpiredTimeMillisecond,
  getPaymentStatusBookingFlight,
  isEmpty,
} from "@utils/helpers";
import clsx from "clsx";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  wraper: {
    display: "flex",
    flexDirection: "column",
    background: theme.palette.gray.grayLight22,
  },
  wraperHeader: {
    position: "fixed",
    width: "100vw",
    zIndex: 99,
  },
  boxTicket: {
    padding: 16,
    background: theme.palette.gray.grayLight22,
    boxShadow: "0px 8px 8px rgba(0, 0, 0, 0.08)",
  },
  ticketDetail: {
    background: theme.palette.white.main,
    borderRadius: 8,
    padding: 16,
  },
  slideItem: {
    minWidth: "90%",
    marginRight: 16,
  },
  titleTicket: {
    height: "18px",
    width: "fit-content",
    overflow: "hidden",
    alignItems: "center",
    borderRadius: "4px",
    display: "flex",
    justifyContent: "center",
    color: "white",
    backgroundColor: theme.palette.primary.main,
    fontWeight: 600,
    fontSize: "12px",
    padding: "2px 4px",
    marginBottom: 10,
  },
  boxStatus: {
    display: "flex",
    flexDirection: "column",
    minHeight: 92,
    color: theme.palette.white.main,
    padding: "12px 16px",
  },
  iconBack: {
    stroke: theme.palette.white.main,
  },
  iconClock: { stroke: theme.palette.white.main },
  navbarSuccess: {
    background: theme.palette.white.main,
    padding: 10,
    height: 72,
  },
  navbarItem: {
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
}));

const FlightBookingDetail = ({ orderDetail = {} }) => {
  const theme = useTheme();
  const classes = useStyles();
  const classesUtils = utilStyles();
  const router = useRouter();
  const [bankTransferInfo, setBankTransferInfo] = useState();

  const [airlines, setAirlines] = useState();
  const actionsGeneralInformation = async () => {
    const { data } = await getGeneralInformation();
    setAirlines(data?.data?.airlines);
  };

  const actionGetBankTransfer = async () => {
    const { data } = await getBankTransferInfo(orderDetail.bankTransferCode);
    if (data.code == 200) {
      setBankTransferInfo(data.data);
    }
  };

  const paymentStatus = getPaymentStatusBookingFlight(
    orderDetail?.paymentStatus,
    orderDetail?.paymentMethodCode,
    orderDetail?.expiredTime || 0
  );

  useEffect(() => {
    actionsGeneralInformation();
    if (
      !isEmpty(orderDetail?.bankTransferCode) &&
      (orderDetail?.paymentMethodCode ===
        flightPaymentMethodCode.BANK_TRANSFER ||
        orderDetail?.paymentMethodCode ===
          flightPaymentMethodCode.BANK_TRANSFER_2)
    ) {
      actionGetBankTransfer();
    }
  }, []);

  return (
    <Box className={classes.wraper}>
      <Box className={classes.wraperHeader}>
        <Box className={classes.boxStatus} bgcolor={paymentStatus.color}>
          <Box display="flex" justifyContent="space-between">
            <IconButton
              style={{ padding: 0 }}
              onClick={() => {
                router.back();
              }}
            >
              <IconBack className={`svgFillAll ${classes.iconBack}`} />
            </IconButton>
            <IconButton style={{ padding: 0 }}>
              <IconShare className={`svgFillAll ${classes.iconBack}`} />
            </IconButton>
          </Box>
          <Typography variant="h4" style={{ marginTop: "12px" }}>
            {paymentStatus.title}
          </Typography>
          {paymentStatus.type === flightPaymentStatus.HOLDING && (
            <Box mt={1} display="flex" alignItems="center">
              <IconClock className={`svgFillAll ${classes.iconClock}`} />
              <Box component="span" fontSize="14px" lineHeight="17px">
                {`Còn ${getExpiredTimeMillisecond(orderDetail.expiredTime)}`}
              </Box>
            </Box>
          )}
        </Box>
        {paymentStatus.type === flightPaymentStatus.SUCCESS && (
          <Grid container className={classes.navbarSuccess}>
            <Grid item xs={3}>
              <Box className={classes.navbarItem}>
                <IconButton style={{ padding: 0 }}>
                  <IconChangeFlightTime />
                </IconButton>
                <Box component="span" fontSize="14px" lineHeight="17px" mt={1}>
                  {listString.IDS_TEXT_CHANGE_FLIGHT_TIME}
                </Box>
              </Box>
            </Grid>
            <Grid item xs={3}>
              <Box className={classes.navbarItem}>
                <IconButton style={{ padding: 0 }}>
                  <IconChangeBaggage />
                </IconButton>
                <Box component="span" fontSize="14px" lineHeight="17px" mt={1}>
                  {listString.IDS_TEXT_FLIGHT_CHANGE_BAGGAGE}
                </Box>
              </Box>
            </Grid>
            <Grid item xs={3}>
              <Box className={classes.navbarItem}>
                <IconButton style={{ padding: 0 }}>
                  <IconSplitFlightCode />
                </IconButton>
                <Box component="span" fontSize="14px" lineHeight="17px" mt={1}>
                  {listString.IDS_TEXT_FLIGHT_SPLIT_CODE}
                </Box>
              </Box>
            </Grid>
            <Grid item xs={3}>
              <Box className={classes.navbarItem}>
                <IconButton style={{ padding: 0 }}>
                  <IconRefundFlight />
                </IconButton>
                <Box component="span" fontSize="14px" lineHeight="17px" mt={1}>
                  {listString.IDS_TEXT_FLIGHT_REFUND}
                </Box>
              </Box>
            </Grid>
          </Grid>
        )}
      </Box>
      <Box
        marginTop={
          paymentStatus.type === flightPaymentStatus.SUCCESS
            ? "164px"
            : paymentStatus.type === flightPaymentStatus.HOLDING
            ? "120px"
            : "92px"
        }
      >
        <Box className={classes.boxTicket}>
          {orderDetail.isTwoWay ? (
            <Box className={classesUtils.scrollViewHorizontal}>
              <Box className={clsx(classes.ticketDetail, classes.slideItem)}>
                <Box className={classes.titleTicket}>
                  {listString.IDS_MT_TEXT_DEPARTURE.toUpperCase()}
                </Box>
                <FlightTicketDetailPayment
                  ticket={orderDetail?.outbound}
                  airlines={airlines}
                />
              </Box>
              <Box className={clsx(classes.ticketDetail, classes.slideItem)}>
                <Box className={classes.titleTicket}>
                  {listString.IDS_MT_TEXT_RETURN.toUpperCase()}
                </Box>
                <FlightTicketDetailPayment
                  ticket={orderDetail?.inbound}
                  airlines={airlines}
                />
              </Box>
            </Box>
          ) : (
            <Box className={classes.ticketDetail}>
              <FlightTicketDetailPayment
                ticket={orderDetail?.outbound}
                airlines={airlines}
              />
            </Box>
          )}
        </Box>
        <Box
          mb={6 / 8}
          className={classes.boxOrderInfo}
          bgcolor={theme.palette.white.main}
        >
          <OrderInfo item={orderDetail} bankTransferInfo={bankTransferInfo} />
        </Box>
        <Box
          mb={6 / 8}
          className={classes.boxContact}
          bgcolor={theme.palette.white.main}
        >
          <OrderContact item={orderDetail?.mainContact} />
        </Box>
        {!isEmpty(orderDetail?.vatInvoiceInfo) && (
          <>
            <Box mb={6 / 8} bgcolor={theme.palette.white.main}>
              <OrderExportInvoice
                invoice={orderDetail.vatInvoiceInfo}
                paymentStatus={paymentStatus}
              />
            </Box>
          </>
        )}
        <Box
          mb={6 / 8}
          className={classes.boxCustomer}
          bgcolor={theme.palette.white.main}
        >
          <OrderCustomerInfo guests={orderDetail.guests} />
        </Box>
        <Box
          mb={6 / 8}
          className={classes.boxInsurance}
          bgcolor={theme.palette.white.main}
        >
          <OrderInsurance
            insuranceContact={orderDetail.insuranceContact}
            insuranceInfo={orderDetail.guests[0].insuranceInfo}
            numberInsurance={orderDetail.numGuests}
          />
        </Box>
        <Box className={classes.boxPayment} bgcolor={theme.palette.white.main}>
          <OrderPaymentInfo
            orderDetail={orderDetail}
            paymentStatus={paymentStatus}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default FlightBookingDetail;
