import { Box, makeStyles, Typography } from "@material-ui/core";
import { listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import { DATE_FORMAT_BACK_END } from "@utils/moment";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    justifyContent: "space-between",
    textAlign: "center",
  },
}));

const OrderInsurance = ({
  insuranceContact = {},
  insuranceInfo = {},
  numberInsurance = 0,
}) => {
  const classes = useStyles();
  if (isEmpty(insuranceContact)) return null;
  return (
    <Box display="flex" flexDirection="column" pt={2} pr={2} pb={12 / 8} pl={2}>
      <Typography variant="subtitle1">
        {listString.IDS_TEXT_ORDER_INSURANCE}
      </Typography>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_TEXT_ORDER_INSURANCE_PACKAGE}
        </Typography>
        <Typography variant="caption">
          {insuranceInfo?.insurancePackageName}
        </Typography>
      </Box>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_TEXT_ORDER_INSURANCE_START}
        </Typography>
        <Typography variant="caption">
          {moment(insuranceInfo?.fromDate, DATE_FORMAT_BACK_END).format("L")}
        </Typography>
      </Box>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_TEXT_ORDER_INSURANCE_END}
        </Typography>
        <Typography variant="caption">
          {moment(insuranceInfo?.toDate, DATE_FORMAT_BACK_END).format("L")}
        </Typography>
      </Box>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_TEXT_ORDER_INSURANCE_NUMBER}
        </Typography>
        <Typography variant="caption">{numberInsurance}</Typography>
      </Box>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_TEXT_ORDER_INSURANCE_PRICE}
        </Typography>
        <Typography variant="caption">{`${insuranceContact?.totalPrice.formatMoney()}đ`}</Typography>
      </Box>
    </Box>
  );
};

export default OrderInsurance;
