/* eslint-disable no-nested-ternary */
import {
  Box,
  Divider,
  makeStyles,
  Typography,
  useTheme,
} from "@material-ui/core";
import { IconCopy } from "@public/icons";
import Image from "@src/image/Image";
import { flightPaymentStatus, listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import { DATE_FORMAT_ALL_ISOLATE, DATE_TIME_FORMAT } from "@utils/moment";
import clsx from "clsx";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  orderInfo: {
    padding: 16,
  },
  orderDate: {
    display: "flex",
    justifyContent: "space-between",
  },
  orderCode: {
    display: " flex",
    flexDirection: "column",
    justifyContent: "center",
    border: `1px dashed ${theme.palette.gray.grayLight25}`,
    background: theme.palette.gray.grayLight26,
    borderRadius: 8,
    padding: "12px 0",
    width: 164,
  },
  codeLeft: {
    color: theme.palette.blue.blueLight8,
    fontSize: 18,
    lineHeight: "22px",
    fontWeight: 600,
  },
  codeRight: {
    fontSize: 18,
    lineHeight: "22px",
    fontWeight: 600,
  },
  textSuccess: {
    color: theme.palette.pink.main,
  },
  textFail: {
    color: theme.palette.red.redLight5,
  },
  textProcessing: {
    color: theme.palette.orange.main,
  },
  divider: {
    height: 1,
    color: theme.palette.gray.grayLight22,
    margin: "12px 0px",
  },
  copy: {
    display: "flex",
    justifyContent: "flex-end",
    color: theme.palette.blue.blueLight8,
    alginItems: "center",
    fontSize: "14px",
    lineHeight: "17px",
  },
  iconBank: {
    width: "100%",
    height: "100%",
    objectFit: "contain",
  },
  bank: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
  },
  bankItem: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.gray.grayLight23}`,
    height: 24,
    width: 48,
    backgroundColor: theme.palette.white.main,
  },
  wrapBank: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    whiteSpace: "nowrap",
    fontSize: 14,
    lineHeight: "17px",
    marginTop: 12,
  },
  bankText: {
    fontWeight: 600,
    whiteSpace: "nowrap",
    overflow: "hidden !important",
    textOverflow: "ellipsis",
    width: 150,
  },
  infoBank: {
    borderBottom: `1px solid ${theme.palette.gray.grayLight23}`,
    marginBottom: 12,
  },
}));

const OrderInfo = ({ item = {}, bankTransferInfo = {} }) => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Box className={classes.orderInfo}>
      <Box className={classes.orderDate} mb={12 / 8}>
        <Typography variant="caption">
          {listString.IDS_MT_TEXT_DATE_BOOKING}
        </Typography>
        <Typography variant="caption">
          {moment(item.bookedDate, DATE_TIME_FORMAT).format(
            DATE_FORMAT_ALL_ISOLATE
          )}
        </Typography>
      </Box>
      <Box display="flex" justifyContent="space-between" textAlign="center">
        <Box className={classes.orderCode}>
          <Typography variant="caption">
            {listString.IDS_TEXT_FLIGHT_ORDER_CODE}
          </Typography>
          <Typography className={classes.codeLeft}>{item.orderCode}</Typography>
        </Box>
        {item.isTwoWay ? (
          <Box className={classes.twoWay}>
            <Box className={classes.orderCode}>
              <Typography variant="caption">
                {`${listString.IDS_TEXT_FLIGHT_BOOK_CODE_OUTBOUND} `}
              </Typography>
              {item.outboundPnrCode ? (
                item.paymentStatus === flightPaymentStatus.REFUNDED ||
                item.paymentStatus === flightPaymentStatus.REFUNDED_POSTED ? (
                  <Typography
                    className={clsx(classes.codeRight, classes.textFail)}
                  >
                    {listString.IDS_TEXT_ORDER_PAYMENT_STATUS_CANCELLED}
                  </Typography>
                ) : (
                  <Typography
                    className={clsx(classes.codeRight, classes.textSuccess)}
                  >
                    {item.outboundPnrCode}
                  </Typography>
                )
              ) : (
                <Typography
                  className={clsx(classes.codeRight, classes.textProcessing)}
                >
                  {listString.IDS_TEXT_ORDER_PAYMENT_STATUS_PROCESSING}
                </Typography>
              )}
              <Typography variant="caption" style={{ marginTop: 12 }}>
                {`${listString.IDS_TEXT_FLIGHT_BOOK_CODE_INBOUND} `}
              </Typography>
              {item.inboundPnrCode ? (
                item.paymentStatus === flightPaymentStatus.REFUNDED ||
                item.paymentStatus === flightPaymentStatus.REFUNDED_POSTED ? (
                  <Typography
                    className={clsx(classes.codeRight, classes.textFail)}
                  >
                    {listString.IDS_TEXT_ORDER_PAYMENT_STATUS_CANCELLED}
                  </Typography>
                ) : (
                  <Typography
                    className={clsx(classes.codeRight, classes.textSuccess)}
                  >
                    {item.inboundPnrCode}
                  </Typography>
                )
              ) : (
                <Typography
                  className={clsx(classes.codeRight, classes.textProcessing)}
                >
                  {listString.IDS_TEXT_ORDER_PAYMENT_STATUS_PROCESSING}
                </Typography>
              )}
            </Box>
          </Box>
        ) : (
          <Box className={classes.orderCode}>
            <Typography variant="caption">
              {`${listString.IDS_TEXT_FLIGHT_BOOK_CODE} `}
            </Typography>
            {item.outboundPnrCode ? (
              item.paymentStatus === flightPaymentStatus.REFUNDED ||
              item.paymentStatus === flightPaymentStatus.REFUNDED_POSTED ? (
                <Typography
                  className={clsx(classes.codeRight, classes.textFail)}
                >
                  {listString.IDS_TEXT_ORDER_PAYMENT_STATUS_CANCELLED}
                </Typography>
              ) : (
                <Typography
                  className={clsx(classes.codeRight, classes.textSuccess)}
                >
                  {item.outboundPnrCode}
                </Typography>
              )
            ) : (
              <Typography
                className={clsx(classes.codeRight, classes.textProcessing)}
              >
                {listString.IDS_TEXT_ORDER_PAYMENT_STATUS_PROCESSING}
              </Typography>
            )}
          </Box>
        )}
      </Box>
      {!isEmpty(bankTransferInfo) && (
        <Box
          mt={2}
          borderRadius="8px"
          border={`1.5px solid ${theme.palette.blue.blueLight8}`}
          borderTop={`4px solid ${theme.palette.blue.blueLight8}`}
          padding="16px 12px"
        >
          <Typography variant="subtitle1">
            {listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER}
          </Typography>
          {bankTransferInfo?.transferOptions.map((el, index) => (
            <Box key={index}>
              <Box className={classes.wrapBank}>
                <Typography variant="caption" className={classes.bank}>
                  {listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_BANK}
                </Typography>
                <Box display="flex" alignItems="center">
                  <Box component="span" className={classes.bankText}>
                    {el?.bankName}
                  </Box>
                  <Box className={classes.bankItem}>
                    <Image
                      srcImage={el?.bankLogo}
                      className={classes.iconBank}
                    />
                  </Box>
                </Box>
              </Box>
              <Box display="flex" justifyContent="space-between" mt={12 / 8}>
                <Typography variant="caption" style={{ lineHeight: "16px" }}>
                  {listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_NUMBER_ACCOUNT}
                </Typography>
                <Box display="flex" flexDirection="column">
                  <Typography variant="subtitle2">
                    {el?.accountNumber}
                  </Typography>
                  <Box
                    className={classes.copy}
                    onClick={() => {
                      navigator.clipboard.writeText(el?.accountNumber);
                    }}
                  >
                    <IconCopy />
                    {listString.IDS_MT_TEXT_INFO_COPPY}
                  </Box>
                </Box>
              </Box>
            </Box>
          ))}
          <Divider className={classes.divider} />
          <Box display="flex" justifyContent="space-between" mt={12 / 8}>
            <Typography variant="caption" style={{ lineHeight: "16px" }}>
              {listString.IDS_MT_TEXT_AMOUNT_MONEY}
            </Typography>
            <Box display="flex" flexDirection="column">
              <Typography variant="subtitle2">
                {`${bankTransferInfo?.transferInfo?.totalAmount.formatMoney()}đ`}
              </Typography>
              <Box
                className={classes.copy}
                onClick={() => {
                  navigator.clipboard.writeText(
                    `${bankTransferInfo?.transferInfo?.totalAmount.formatMoney()}đ`
                  );
                }}
              >
                <IconCopy />
                {listString.IDS_MT_TEXT_INFO_COPPY}
              </Box>
            </Box>
          </Box>
          <Box display="flex" justifyContent="space-between" mt={12 / 8}>
            <Typography variant="caption" style={{ lineHeight: "16px" }}>
              {listString.IDS_MT_TEXT_CONTENT_TRANSFER}
            </Typography>
            <Box display="flex" flexDirection="column">
              <Typography
                variant="subtitle2"
                style={{ textAlign: "end", whiteSpace: "break-spaces" }}
              >
                {bankTransferInfo?.transferInfo?.message}
              </Typography>
              <Box
                className={classes.copy}
                onClick={() => {
                  navigator.clipboard.writeText(
                    bankTransferInfo?.transferInfo?.message
                  );
                }}
              >
                <IconCopy />
                {listString.IDS_MT_TEXT_INFO_COPPY}
              </Box>
            </Box>
          </Box>
        </Box>
      )}
    </Box>
  );
};

export default OrderInfo;
