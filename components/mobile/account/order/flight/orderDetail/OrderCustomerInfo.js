import OrderCustomerDetailModal from "@components/common/modal/flight/OrderCustomerDetailModal";
import { Box, makeStyles, Typography, useTheme } from "@material-ui/core";
import ButtonComponent from "@src/button/Button";
import { CUSTOMER_TYPE_TEXT, listString } from "@utils/constants";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    justifyContent: "space-between",
    textAlign: "center",
  },
  title: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  numGuest: {
    color: theme.palette.gray.grayDark7,
  },
  ellipsisText: {
    wordWrap: "break-word",
    maxWidth: "60%",
    textAlign: "end",
  },
}));

const OrderCustomerInfo = ({ guests = {} }) => {
  const theme = useTheme();
  const classes = useStyles();
  const [openViewDetail, setOpenViewDetail] = useState(false);
  const handleViewDetail = (open) => () => {
    setOpenViewDetail(open);
  };
  let adultNo = 0;
  let childrenNo = 0;
  let infantNo = 0;
  return (
    <Box display="flex" flexDirection="column" pt={2} pr={2} pb={12 / 8} pl={2}>
      <Box className={classes.title}>
        <Box className={classes.titleLeft}>
          <Typography component="span" variant="subtitle1">
            {listString.IDS_MT_TEXT_INFO_CUSTOMER_FLIGHT}
          </Typography>
          <Typography
            component="span"
            variant="caption"
            className={classes.numGuest}
          >
            {` (${guests?.length || 0} ${
              listString.IDS_MT_TEXT_SINGLE_CUSTOMER
            })`}
          </Typography>
        </Box>
        <ButtonComponent
          fontSize={14}
          fontWeight="normal"
          color={theme.palette.blue.blueLight8}
          typeButton="text"
          width="fit-content"
          padding={0}
          handleClick={handleViewDetail(true)}
        >
          <Typography component="span" variant="caption">
            {listString.IDS_MT_TEXT_VIEW_DETAIL}
          </Typography>
        </ButtonComponent>
        <OrderCustomerDetailModal
          open={openViewDetail}
          handleClose={handleViewDetail}
          guests={guests}
        />
      </Box>
      {guests.length
        ? guests.map((item, index) => {
            if (item.ageCategory.toLowerCase() === CUSTOMER_TYPE_TEXT.ADULT) {
              return (
                <Box className={classes.content} key={index}>
                  <Typography variant="caption">
                    {`${listString.IDS_MT_TEXT_ADULT} ${++adultNo}`}
                  </Typography>
                  <Typography
                    variant="caption"
                    className={classes.ellipsisText}
                  >
                    {item?.fullName.toUpperCase()}
                  </Typography>
                </Box>
              );
            }
            if (
              item.ageCategory.toLowerCase() === CUSTOMER_TYPE_TEXT.CHILDREN
            ) {
              return (
                <Box className={classes.content} key={index}>
                  <Typography variant="caption">
                    {`${listString.IDS_MT_TEXT_CHILDREN} ${++childrenNo}`}
                  </Typography>
                  <Typography
                    variant="caption"
                    className={classes.ellipsisText}
                  >
                    {item?.fullName.toUpperCase()}
                  </Typography>
                </Box>
              );
            }
            if (item.ageCategory.toLowerCase() === CUSTOMER_TYPE_TEXT.INFANT) {
              return (
                <Box className={classes.content} key={index}>
                  <Typography variant="caption">
                    {`${listString.IDS_MT_TEXT_BABY} ${++infantNo}`}
                  </Typography>
                  <Typography
                    variant="caption"
                    className={classes.ellipsisText}
                  >
                    {item?.fullName.toUpperCase()}
                  </Typography>
                </Box>
              );
            }
          })
        : null}
    </Box>
  );
};

export default OrderCustomerInfo;
