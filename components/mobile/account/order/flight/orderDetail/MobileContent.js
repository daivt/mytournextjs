import { getFlightBookingDetail } from "@api/flight";
import Layout from "@components/layout/mobile/Layout";
import FlightBookingDetail from "@components/mobile/account/order/flight/orderDetail/FlightBookingDetail";
import { makeStyles } from "@material-ui/styles";
import { isEmpty } from "@utils/helpers";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  styleContainer: {
    color: theme.palette.black.black3,
    fontSize: 14,
    margin: 16,
  },
}));

const MobileContent = () => {
  const classes = useStyles();
  const router = useRouter();
  const [infoOrder, setInfoOrder] = useState({});
  const fetchOrderDetail = async () => {
    try {
      const { data } = await getFlightBookingDetail({
        id: router.query.id,
      });
      if (data.code === 200) {
        setInfoOrder(data.data || {});
      }
    } catch (error) {}
  };

  useEffect(() => {
    fetchOrderDetail();
  }, []);
  if (isEmpty(infoOrder)) return null;
  return (
    <Layout>
      <FlightBookingDetail orderDetail={infoOrder} />
    </Layout>
  );
};
export default MobileContent;
