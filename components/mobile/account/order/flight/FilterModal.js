import { Box, Drawer, makeStyles } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { IconActive } from "@public/icons";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  paperAnchorBottom: {
    borderRadius: "16px 16px 0 0",
  },
  mainDrawerFilter: {
    width: "100vw",
  },
  wrapHeader: {
    margin: "0 16px",
  },
  boxItem: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    height: 48,
    justifyContent: "space-between",
  },
  boxItemActive: {
    color: theme.palette.blue.blueLight8,
  },
  hideBorder: {
    borderBottom: "none",
  },
  swipeDown: {
    height: 6,
    width: 42,
    display: "flex",
    justifyContent: "center",
    background: theme.palette.gray.grayLight23,
    margin: "8px auto 14px auto",
    borderRadius: 100,
  },
  iconActive: {},
}));

const FilterModal = ({
  openModal = true,
  toggleDrawerReview = () => {},
  valueFilter = [],
  handleSelectedItem = () => {},
  listFilter = [],
}) => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Drawer
      anchor="bottom"
      open={openModal}
      onClose={toggleDrawerReview("")}
      classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
    >
      <Box className={classes.mainDrawerFilter}>
        <Box className={classes.wrapHeader}>
          <Box width="100%" textAlign="center">
            <Box className={classes.wrapListSort}>
              <Box className={classes.swipeDown} />
              {listFilter.map((item, index) => (
                <Box
                  className={clsx(
                    classes.boxItem,
                    index === listFilter.length - 1 && classes.hideBorder
                  )}
                  key={item.code}
                  onClick={handleSelectedItem(item.code)}
                  color={
                    valueFilter.includes(item.code)
                      ? theme.palette.blue.blueLight8
                      : theme.palette.black.black3
                  }
                >
                  <Typography variant="body1">{item.label}</Typography>
                  <Box className={classes.iconActive}>
                    {valueFilter.includes(item.code) && <IconActive />}
                  </Box>
                </Box>
              ))}
            </Box>
          </Box>
          <Box />
        </Box>
      </Box>
    </Drawer>
  );
};

export default FilterModal;
