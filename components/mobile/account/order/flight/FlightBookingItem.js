import WayFlightItemDetail from "@components/mobile/account/order/flight/WayFlightItemDetail";
import { Box, Typography, useTheme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconArrowDownToggle } from "@public/icons";
import { flightPaymentStatus, listString, routeStatic } from "@utils/constants";
import {
  getExpiredTimeMillisecond,
  getPaymentStatusBookingFlight,
} from "@utils/helpers";
import clsx from "clsx";
import moment from "moment";
import { useRouter } from "next/router";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: theme.palette.white.main,
    borderRadius: 8,
    boxShadow: "0px 3px 3px rgba(0, 0, 0, 0.05)",
    padding: "12px 16px",
    color: theme.palette.black.black3,
  },
  header: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  iconArrowDownToggle: {
    stroke: theme.palette.black.black3,
    transform: "rotate(270deg)",
  },
  divider: {
    height: 1,
    backgroundColor: theme.palette.gray.grayLight22,
    width: "100%",
    margin: "12px 0",
  },
  dividerFull: {
    width: "calc(100% + 32px)",
    margin: "12px -16px",
  },
  infoBooking: {
    display: "flex",
    flexDirection: "column",
  },
  infoFooter: {
    display: "flex",
    justifyContent: "space-between",
  },
  status: {
    display: "flex",
    alignItems: "center",
    borderRadius: 4,
    height: 22,
    padding: "0 6px",
    textTransform: "uppercase",
    color: theme.palette.white.main,
  },
}));

const FlightBookingItem = ({ item = {}, airlines = {} }) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();

  const paymentStatus = getPaymentStatusBookingFlight(
    item.paymentStatus,
    item.paymentMethodCode,
    item?.expiredTime || 0
  );

  let airlineOutBoundInfo = {};
  let airlineInBoundInfo = {};
  if (airlines) {
    airlineOutBoundInfo = airlines.find(
      (al) => al.aid === item.outbound?.airlineId
    );
    if (item.isTwoWay) {
      airlineInBoundInfo = airlines.find(
        (al) => al.aid === item.inbound?.airlineId
      );
    }
  }

  return (
    <Box
      className={classes.container}
      onClick={() => {
        router.push({
          pathname: routeStatic.ACCOUNT_ORDER_FLIGHT_DETAIL.href,
          query: {
            id: item?.orderCode.substr(1),
          },
        });
      }}
    >
      <Box className={classes.header}>
        <Box display="flex" alignItems="center">
          <Typography
            variant="caption"
            style={{ lineHeight: "16px" }}
          >{`${listString.IDS_TEXT_FLIGHT_ORDER_CODE}: `}</Typography>
          <Typography
            variant="subtitle2"
            style={{ color: theme.palette.blue.blueLight8, paddingLeft: "2px" }}
          >{`${item.orderCode}`}</Typography>
        </Box>
        <IconArrowDownToggle
          className={`svgFillAll ${classes.iconArrowDownToggle}`}
        />
      </Box>
      <Box className={clsx(classes.divider, classes.dividerFull)} />
      <WayFlightItemDetail
        item={item.outbound}
        airlineInfo={airlineOutBoundInfo}
      />
      {item.isTwoWay && (
        <Box mt={2}>
          <WayFlightItemDetail
            item={item?.inbound}
            airlineInfo={airlineInBoundInfo}
          />
        </Box>
      )}
      <Box className={classes.divider} />
      <Box className={classes.infoFooter}>
        <Box display="flex">
          <Typography
            variant="caption"
            style={{ lineHeight: "16px" }}
          >{`${listString.IDS_MT_TEXT_TOTAL_PRICE}: `}</Typography>
          <Typography
            variant="subtitle2"
            style={{ paddingLeft: "2px" }}
          >{`${item.finalPriceFormatted}`}</Typography>
        </Box>
        <Box display="flex" flexDirection="column">
          <Typography
            variant="subtitle2"
            className={classes.status}
            style={{ background: paymentStatus.color }}
          >
            {paymentStatus.title}
          </Typography>
          {(paymentStatus.type === flightPaymentStatus.HOLDING ||
            paymentStatus.type === flightPaymentStatus.PENDING) &&
            item.expiredTime > 0 &&
            item.expiredTime / 1000 > moment().unix() && (
              <Typography
                variant="body2"
                style={{ marginTop: 8, color: paymentStatus.color }}
              >
                {`Còn ${getExpiredTimeMillisecond(item.expiredTime)}`}
              </Typography>
            )}
        </Box>
      </Box>
    </Box>
  );
};
export default FlightBookingItem;
