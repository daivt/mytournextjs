import {
  Box,
  Drawer,
  IconButton,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { IconClose } from "@public/icons";
import { listString, optionClean } from "@utils/constants";
import { isEmpty, unEscape } from "@utils/helpers";
import sanitizeHtml from "sanitize-html";

const useStyles = makeStyles((theme) => ({
  paperAnchorBottom: {
    borderRadius: "16px 16px 0 0",
    maxHeight: "48%",
    paddingBottom: 20,
  },
  wrapButtonClose: { padding: 0 },
  wraperHeader: {
    padding: "12px 16px",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    height: 48,
    display: "flex",
    alignItems: "center",
  },
  wrapHeaderDraw: {
    display: "flex",
    alignItems: "center",
    height: 48,
    borderBottom: "1px solid #EDF2F7",
    width: "100%",
  },
  textFilter: {
    textAlign: "center",
    width: "calc(100% - 40px)",
  },
  wraperContent: {
    margin: 16,
    wordBreak: "break-all",
  },
  wrapGrid: {
    display: "flex",
    marginTop: 12,
    wordBreak: "break-word",
  },
  iconDot: {
    marginRight: 12,
    marginTop: 8,
  },
  policyItem: {
    wordBreak: "break-word",
    "& p": {
      margin: 0,
    },
  },
}));

const PoliciesModal = ({ open = true, item = {}, toggleDrawer = () => {} }) => {
  const classes = useStyles();
  return (
    <>
      <Drawer
        anchor="bottom"
        open={open}
        onClose={toggleDrawer("")}
        classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
      >
        <Box className={classes.wraperHeader}>
          <Box className={classes.wrapHeaderDraw}>
            <IconButton
              onClick={toggleDrawer("")}
              className={classes.wrapButtonClose}
            >
              <IconClose />
            </IconButton>
            <Typography variant="subtitle1" className={classes.textFilter}>
              {listString.IDS_MT_TEXT_ROOM_POLICY}
            </Typography>
          </Box>
        </Box>

        {!isEmpty(item) &&
          item.map((el, index) => (
            <Box key={index}>
              <Box className={classes.wraperContent}>
                <Typography variant="subtitle2">{el.title}</Typography>
                <Box
                  fontSize={14}
                  color="gray.grayDark1"
                  lineHeight="22px"
                  mt={1}
                  className={classes.policyItem}
                  dangerouslySetInnerHTML={{
                    __html: sanitizeHtml(
                      unEscape(unescape(el.description || "")),
                      optionClean
                    ),
                  }}
                />
              </Box>
            </Box>
          ))}
      </Drawer>
    </>
  );
};
PoliciesModal.propTypes = {};
export default PoliciesModal;
