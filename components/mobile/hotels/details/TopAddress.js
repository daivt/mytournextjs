import { Fragment } from "react";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import { isEmpty } from "@utils/helpers";
import { IconBeach } from "@public/icons";
import { listIcons, listString, prefixUrlIcon } from "@utils/constants";

import ButtonComponent from "@src/button/Button";
import MapDetail from "@components/common/map/MapDetail";
import MapDetailDialog from "@components/common/map/MapDetailDialog";

const useStyles = makeStyles((theme) => ({
  container: {
    padding: 16,
    fontSize: 14,
    color: theme.palette.black.black3,
  },
  addressContentContainer: {
    padding: "6px 0",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
}));

const TopAddress = ({
  hotelDetail = {},
  listInterestingPlaces = {},
  handleScrollToRate = () => {},
}) => {
  const classes = useStyles();
  const coordinate = !isEmpty(hotelDetail.address)
    ? hotelDetail.address.coordinate
    : {};
  if (isEmpty(listInterestingPlaces)) return null;
  return (
    <Box className={classes.container}>
      <Box component="span" fontSize={18} lineHeight="21px" fontWeight={600}>
        {listString.IDS_MT_HIGHT_LIGHT_ADDRESS}
      </Box>
      <Box
        width="100%"
        height={96}
        borderRadius={8}
        m="12px 0 6px"
        overflow="hidden"
      >
        <MapDetail position={coordinate} places={listInterestingPlaces} />
      </Box>

      {!isEmpty(listInterestingPlaces) &&
        listInterestingPlaces.items.map((el, index) => {
          const iconInfo = (listInterestingPlaces.types || []).find(
            (v) => el.typeId === v.id
          );
          if (index > 2) return null;
          return (
            <Fragment key={index.toString()}>
              <Box pt={12 / 8} className={classes.addressContentContainer}>
                <Box display="flex" alignItems="center">
                  {!isEmpty(iconInfo.icon) ? (
                    <img
                      src={iconInfo.icon}
                      style={{ width: 24, height: 24 }}
                      alt=""
                    />
                  ) : (
                    <img
                      src={`${prefixUrlIcon}${listIcons.IconBeach}`}
                      alt=""
                      style={{ width: 32, height: 32 }}
                    />
                  )}
                  <Box display="flex" flexDirection="column" pl={1}>
                    <Box component="span" lineHeight="17px">
                      {el.name}
                    </Box>
                    <Box
                      component="span"
                      fontSize={12}
                      lineHeight="14px"
                      pt={4 / 8}
                      color="gray.grayDark8"
                    >
                      {iconInfo.name}
                    </Box>
                  </Box>
                </Box>
                <Box component="span" lineHeight="17px">{`${(
                  el.distanceMeters / 1000
                ).toFixed(2)}km`}</Box>
              </Box>
            </Fragment>
          );
        })}
      {!isEmpty(listInterestingPlaces.items) &&
        listInterestingPlaces.items.length > 3 && (
          <MapDetailDialog
            child={
              <Box display="flex" justifyContent="center">
                <ButtonComponent
                  typeButton="outlined"
                  borderColor="#A0AEC0"
                  backgroundColor="initial"
                  width="fit-content"
                  borderRadius={8}
                  color="1A202C"
                  padding="8px 22px"
                >
                  {listString.IDS_MT_TEXT_LOAD_MORE}
                </ButtonComponent>
              </Box>
            }
            listInterestingPlaces={listInterestingPlaces}
            handleScrollToRate={handleScrollToRate}
            coordinate={coordinate}
          />
        )}
    </Box>
  );
};

export default TopAddress;
