import clsx from "clsx";
import { useState, useEffect } from "react";
import { Box, makeStyles, Drawer } from "@material-ui/core";

import { listString } from "@utils/constants";

import ButtonComponent from "@src/button/Button";

const useStyles = makeStyles((theme) => ({
  paperAnchorBottom: {
    minHeight: "55vh",
    borderRadius: "16px 16px 0 0",
    color: theme.palette.black.black3,
    fontSize: 14,
  },
  listItem: {
    display: "flex",
    flexFlow: "wrap",
    margin: "0 -8px",
  },
  itemBtn: {
    height: 56,
    width: 56,
    borderRadius: "100px",
    backgroundColor: theme.palette.gray.grayLight22,
    margin: 8,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    fontSize: 18,
  },
  itemBtnSelect: {
    backgroundColor: theme.palette.blue.blueLight8,
    color: theme.palette.white.main,
    fontWeight: 600,
  },
}));

let listRoom = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const SelectRoomUserModal = ({
  open = true,
  toggleDrawer = () => () => {},
  handleSelectParams = () => () => {},
  paramsSearch = {},
}) => {
  const classes = useStyles();
  const [params, setParams] = useState({
    customer: paramsSearch.adults + paramsSearch.children,
    rooms: paramsSearch.rooms,
  });

  useEffect(() => {
    setParams({
      customer: paramsSearch.adults + paramsSearch.children,
      rooms: paramsSearch.rooms,
    });
  }, [paramsSearch]);

  const handleSelectCustomer = (typePrams = "", number = 1) => {
    if (typePrams === "customer" && number <= params.rooms) {
      setParams({
        rooms: number,
        customer: number,
      });
    } else {
      setParams({
        ...params,
        [typePrams]: number,
      });
    }
  };
  return (
    <>
      <Drawer
        anchor="bottom"
        open={open}
        onClose={toggleDrawer("")}
        classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
      >
        <Box px={2} pt={3}>
          <Box display="flex" flexDirection="column">
            <Box fontSize={16} lineHeight="20px" component="span" pb={4 / 8}>
              {listString.IDS_TEXT_COUNT_CUSTOMER}
            </Box>
            <Box className={classes.listItem}>
              {listRoom.map((el) => (
                <Box
                  key={el.toString()}
                  className={clsx(
                    classes.itemBtn,
                    el === params.customer && classes.itemBtnSelect
                  )}
                  onClick={() => handleSelectCustomer("customer", el)}
                >
                  {el}
                </Box>
              ))}
            </Box>
          </Box>
          <Box display="flex" flexDirection="column" pt={1}>
            <Box fontSize={16} lineHeight="20px" component="span" pb={4 / 8}>
              {listString.IDS_MT_TEXT_ROOM_NUMBER}
            </Box>
            <Box
              fontSize={12}
              lineHeight="14px"
              component="span"
              pb={4 / 8}
              color="gray.grayDark7"
            >
              {listString.IDS_MT_TEXT_ROOM_NUMBER_POLICY}
            </Box>
            <Box className={classes.listItem}>
              {listRoom.map((el) => {
                if (el > params.customer) return null;
                return (
                  <Box
                    key={el.toString()}
                    className={clsx(
                      classes.itemBtn,
                      el === params.rooms && classes.itemBtnSelect
                    )}
                    onClick={() => handleSelectCustomer("rooms", el)}
                  >
                    {el}
                  </Box>
                );
              })}
            </Box>
          </Box>
          <Box py={2}>
            <ButtonComponent
              backgroundColor="#FF1284"
              borderRadius={8}
              height={48}
              handleClick={() => handleSelectParams(params)}
            >
              {listString.IDS_MT_TEXT_CONFIRM}
            </ButtonComponent>
          </Box>
        </Box>
      </Drawer>
    </>
  );
};
export default SelectRoomUserModal;
