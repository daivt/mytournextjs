import clsx from "clsx";
import moment from "moment";
import { useState } from "react";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import { IconMoonlight } from "@public/icons";
import { listString } from "@utils/constants";

import CalendarDialog from "@components/common/calendar/CalendarDialog";
// import SelectRoomUserModal from "@components/mobile/hotels/details/SelectRoomUserModal";
import GuestInfoBoxModal from "@components/common/modal/hotel/GuestInfoBoxModal";

const useStyles = makeStyles((theme) => ({
  boxSelectOption: {
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
    padding: "12px 0",
    margin: "0 -16px",
    width: "calc(100% + 32px)",
  },
  nightDay: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    width: 28,
    height: 28,
    borderRadius: "50%",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    backgroundColor: theme.palette.white.main,
  },
  titleParams: {
    lineHeight: "18px",
    color: theme.palette.gray.grayDark7,
  },
  valueParams: {
    fontSize: 16,
    lineHeight: "20px",
    fontWeight: 600,
    paddingTop: 4,
  },
  boxRight: {
    marginLeft: 12,
    paddingLeft: 12,
    textAlign: "center",
    display: "flex",
    borderLeft: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  sticky: {
    zIndex: 1000,
    top: 68,
    left: 0,
    position: "fixed",
    transition: "all 0.1s ease-in-out",
    color: "#000",
    background: "#fff",
    width: "100%",
    margin: 0,
    // boxShadow: "0 0 10px rgba(0,0,0,0.5)",
  },
}));

const listTypeModal = {
  MODAL_GUEST_INFO: "MODAL_GUEST_INFO",
};

const SelectParamsDetail = ({
  checkIn = null,
  checkOut = null,
  // setCheckIn = () => {},
  // setCheckOut = () => {},
  handleSelectParams = () => {},
  setFullDate = () => {},
  paramsSearch = {},
  sticky = "",
  listPosition = {},
}) => {
  const classes = useStyles();
  const [visibleDrawerType, setVisibleDrawerType] = useState("");
  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };
  const handleSelectParam = (values) => {
    setVisibleDrawerType("");
    handleSelectParams(values);
  };
  return (
    <Box
      className={clsx(
        classes.boxSelectOption,
        sticky === listPosition.POSITION_DATE_ROOM && classes.sticky
      )}
    >
      <Box width="100%" display="flex" justifyContent="space-between" px={2}>
        <CalendarDialog
          child={
            <Box display="flex" alignItems="center">
              <Box display="flex" flexDirection="column">
                <Box component="span" className={classes.titleParams}>
                  {listString.IDS_MT_TEXT_CHECK_IN_ROOM}
                </Box>
                <Box component="span" className={classes.valueParams}>
                  {`${checkIn.format("DD")} tháng ${checkIn.format("MM")}`}
                </Box>
              </Box>
              <Box
                mx={3}
                position="relative"
                display="flex"
                justifyContent="center"
                alignItems="center"
              >
                <Box className={classes.nightDay}>
                  <Box
                    component="span"
                    fontSize={11}
                    lineHeight="13px"
                    pr={2 / 8}
                  >
                    {checkIn && checkOut
                      ? Math.abs(
                          moment(checkIn)
                            .startOf("day")
                            .diff(checkOut.startOf("day"), "days")
                        )
                      : "-"}
                  </Box>
                  <IconMoonlight />
                </Box>
              </Box>
              <Box display="flex" flexDirection="column" alignItems="flex-end">
                <Box component="span" className={classes.titleParams}>
                  {listString.IDS_MT_TEXT_CHECK_OUT_ROOM}
                </Box>
                <Box component="span" className={classes.valueParams}>
                  {`${checkOut.format("DD")} tháng ${checkOut.format("MM")}`}
                </Box>
              </Box>
            </Box>
          }
          type="hotel"
          initStartDate={checkIn}
          initEndDate={checkOut}
          // updateStartDate={setCheckIn}
          // updateEndDate={setCheckOut}
          updateFullDate={setFullDate}
        />
        <Box
          className={classes.boxRight}
          onClick={toggleDrawer(listTypeModal.MODAL_GUEST_INFO)}
        >
          <Box display="flex" flexDirection="column" pr={1}>
            <Box component="span" className={classes.titleParams}>
              {listString.IDS_MT_TEXT_SINGLE_ROOM}
            </Box>
            <Box component="span" className={classes.valueParams}>
              {paramsSearch.rooms}
            </Box>
          </Box>
          <Box display="flex" flexDirection="column">
            <Box component="span" className={classes.titleParams}>
              {listString.IDS_MT_TEXT_SINGLE_CUSTOMER}
            </Box>
            <Box component="span" className={classes.valueParams}>
              {paramsSearch.adults + paramsSearch.children}
            </Box>
          </Box>
        </Box>
        {/* <SelectRoomUserModal
          open={visibleDrawerType === listTypeModal.MODAL_GUEST_INFO}
          toggleDrawer={toggleDrawer}
          paramsSearch={paramsSearch}
          handleSelectParams={handleSelectParam}
        /> */}
        <GuestInfoBoxModal
          paramsSearch={paramsSearch}
          open={visibleDrawerType === listTypeModal.MODAL_GUEST_INFO}
          toggleDrawer={toggleDrawer}
          handleOk={handleSelectParam}
        />
      </Box>
    </Box>
  );
};

export default SelectParamsDetail;
