import moment from "moment";
import { useRouter } from "next/router";
import sanitizeHtml from "sanitize-html";
import { makeStyles } from "@material-ui/styles";
import { Box, Typography } from "@material-ui/core";
import { useRef, useState, useEffect } from "react";

import {
  getHotelsRoom,
  getHotelReviews,
  getTripAdvisorReviews,
  getInterestingPlaces,
} from "@api/hotels";

import * as gtm from "@utils/gtm";
import {
  listString,
  formatDate,
  optionClean,
  routeStatic,
  listEventHotel,
  DELAY_TIMEOUT_POLLING,
  LAST_SEARCH_HISTORY,
} from "@utils/constants";
import {
  handleSaveHotelDetailToStorage,
  isEmpty,
  unEscape,
  checkAndChangeCheckIO,
} from "@utils/helpers";

import ButtonComponent from "@src/button/Button";
import Layout from "@components/layout/mobile/Layout";
import BannerHotel from "@components/mobile/hotels/details/BannerHotel";
import TopAddress from "@components/mobile/hotels/details/TopAddress";
import ListRoomHotel from "@components/mobile/hotels/details/ListRoomHotel";
import ListHotelRecent from "@components/mobile/hotels/details/ListHotelRecent";
import ConvenientPopup from "@components/mobile/hotels/details/ConvenientPopup";
import ConvenientHotel from "@components/mobile/hotels/details/ConvenientHotel";
import InfoHotelDetails from "@components/mobile/hotels/details/InfoHotelDetails";
import ReviewHotelPopup from "@components/mobile/hotels/details/ReviewHotelPopup";
import WriteReviewModal from "@components/mobile/hotels/details/WriteReviewModal";
import ReviewCommentHotel from "@components/mobile/hotels/details/ReviewCommentHotel";
import SelectParamsDetail from "@components/mobile/hotels/details/SelectParamsDetail";
import EvaluateHotelDetails from "@components/mobile/hotels/details/EvaluateHotelDetails";
import FooterSelectBestPrice from "@components/mobile/hotels/details/FooterSelectBestPrice";

const useStyles = makeStyles((theme) => ({
  boxSearchContent: {
    position: "absolute",
    width: "100%",
    marginTop: -20,
    backgroundColor: theme.palette.white.main,
    borderRadius: "16px 16px 0px 0px",
    paddingBottom: 34,
  },
  divider: {
    height: 8,
    backgroundColor: theme.palette.gray.grayLight22,
  },
  convenientHotel: {
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
    color: theme.palette.black.black3,
    paddingBottom: 12,
  },
  wrapCheckIn: {
    background: theme.palette.gray.grayLight22,
    color: theme.palette.black.black3,
    display: "flex",
    justifyContent: "space-around",
    borderRadius: 8,
    marginBottom: 12,
  },
  textCheckIn: {
    fontWeight: "normal",
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark8,
    marginTop: 8,
    textAlign: "center",
  },
  dividerTime: {
    width: 1,
    height: 28,
    background: theme.palette.gray.grayLight23,
    margin: "12px 0",
  },
  timeCheckIn: {
    marginBottom: 8,
    lineHeight: "17px",
  },
  bannerImg: {
    width: "calc(100% - 24px)",
    maxHeight: 84,
    borderRadius: 8,
    margin: "0 12px 20px",
  },
  itemPolices: {
    "& p": {
      margin: 0,
    },
  },
}));

const listTypeModal = {
  MODAL_INFO_HOTEL: "MODAL_INFO_HOTEL",
  MODAL_REVIEW_HOTEL: "MODAL_REVIEW_HOTEL",
};

const listTab = {
  TAB_CONVENIENT: "TAB_CONVENIENT",
  TAB_POLICY: "TAB_POLICY",
  TAB_DESCRIPTION: "TAB_DESCRIPTION",
};

const listPosition = {
  POSITION_MENU: "POSITION_MENU",
  POSITION_IMAGE: "POSITION_IMAGE",
  POSITION_DATE_ROOM: "POSITION_DATE_ROOM",
};

const HotelDetailMobile = ({ paramsInit = {}, hotelDetail = {} }) => {
  const ref = useRef(null);
  const refStart = useRef(null);
  const refEnd = useRef(null);
  const router = useRouter();
  const classes = useStyles();
  const [sticky, setSticky] = useState("");
  const [isShowSelectPrice, setIsShowSelectPrice] = useState(true);

  const [paramsSearch, setParamsSearch] = useState({
    checkIn: checkAndChangeCheckIO(paramsInit.checkIn, "checkIn"),
    checkOut: checkAndChangeCheckIO(paramsInit.checkOut, "checkOut"),
    rooms: paramsInit.rooms || 1,
    adults: paramsInit.adults || 2,
    children: paramsInit.children || 0,
    childrenAges: paramsInit.childrenAges || [],
  });
  const [visibleDrawerType, setVisibleDrawerType] = useState("");
  const [initialTab, setInitialTab] = useState("");
  const [listRoom, setListRoom] = useState([]);
  const [isFetchingRoom, setIsFetchingRoom] = useState(false);
  const [reviews, setReviews] = useState({});
  const [reviewsTripadvisor, setReviewsTripadvisor] = useState({});
  const [listInterestingPlaces, setListInterestingPlaces] = useState({});
  const [isFirst, setIsFirst] = useState(true);

  let breakPolling = true;

  useEffect(() => {
    if (checkIsEmptyParamsQuery()) {
      const lastSearchHistory =
        JSON.parse(localStorage.getItem(LAST_SEARCH_HISTORY)) || {};
      if (!isEmpty(lastSearchHistory)) {
        const paramsSearchTemp = {
          checkIn: checkAndChangeCheckIO(lastSearchHistory.checkIn, "checkIn"),
          checkOut: checkAndChangeCheckIO(
            lastSearchHistory.checkOut,
            "checkOut"
          ),
          adults: parseInt(lastSearchHistory.adults) || 2,
          rooms: parseInt(lastSearchHistory.rooms) || 1,
          children: parseInt(lastSearchHistory.children) || 0,
          childrenAges: lastSearchHistory.childrenAges || [],
        };
        setParamsSearch(paramsSearchTemp);
        getRoomHotel(paramsSearchTemp);
      } else {
        getRoomHotel(paramsSearch);
      }
    }
  }, []);

  useEffect(() => {
    if (!checkIsEmptyParamsQuery()) {
      const paramsSearchTemp = {
        checkIn: paramsInit.checkIn,
        checkOut: paramsInit.checkOut,
        rooms: paramsInit.rooms || 1,
        adults: paramsInit.adults || 2,
        children: paramsInit.children || 0,
        childrenAges: paramsInit.childrenAges || [],
      };
      setParamsSearch(paramsSearchTemp);
      getRoomHotel(paramsSearchTemp);
    }
  }, [paramsInit]);

  useEffect(() => {
    if (!isEmpty(hotelDetail)) {
      getHotelReview();
      getTripAdvisorReview();
      fetInterestingPlaces();
      handleSaveHotelDetailToStorage(hotelDetail);
      if (checkIsEmptyParamsQuery() && !isFirst) {
        // change hotel id with not params
        getRoomHotel(paramsSearch);
      }
      setIsFirst(false);
    }
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", () => handleScroll);
      breakPolling = false;
    };
  }, [hotelDetail.id]);

  const genParamsUrl = () => {
    let paramsUrl = {
      checkIn: paramsSearch.checkIn,
      checkOut: paramsSearch.checkOut,
      adults: paramsSearch.adults,
      children: paramsSearch.children,
      rooms: paramsSearch.rooms,
    };
    if (paramsSearch.children > 0) {
      paramsUrl = {
        ...paramsUrl,
        childrenAges: paramsSearch.childrenAges.toString(),
      };
    }
    return paramsUrl;
  };

  const checkIsEmptyParamsQuery = () => {
    return !(router.query.checkIn || router.query.checkOut);
  };

  const handleLastSearchHistoryToStorage = (paramsSearch) => {
    let lastSearchHistory =
      JSON.parse(localStorage.getItem(LAST_SEARCH_HISTORY)) || {};
    lastSearchHistory = {
      ...lastSearchHistory,
      checkIn: paramsSearch.checkIn,
      checkOut: paramsSearch.checkOut,
      adults: paramsSearch.adults,
      rooms: paramsSearch.rooms,
      children: paramsSearch.children,
      childrenAges: paramsSearch.childrenAges,
    };
    localStorage.setItem(
      LAST_SEARCH_HISTORY,
      JSON.stringify(lastSearchHistory)
    );
  };

  const getRoomHotel = async (paramsDefault = {}) => {
    let polling = true;
    try {
      let dataDTO = {
        hotelId: hotelDetail.id,
        ...paramsDefault,
      };
      if (!isEmpty(router.query.priceKey)) {
        dataDTO = {
          ...dataDTO,
          priceKey: router.query.priceKey,
        };
      }
      setIsFetchingRoom(true);
      while (polling && breakPolling) {
        const { data } = await getHotelsRoom(dataDTO);
        if (data.code === 200) {
          setListRoom(data.data.items);
          if (data.data.completed) {
            setIsFetchingRoom(false);
            polling = false;
            break;
          } else {
            if (data.data.total !== 0) {
              setIsFetchingRoom(false);
            }
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
          setIsFetchingRoom(false);
        }
      }
    } catch (error) {
      polling = false;
      setIsFetchingRoom(false);
    }
  };
  const getHotelReview = async () => {
    try {
      const { data } = await getHotelReviews({
        page: 1,
        size: 10,
        hotelId: hotelDetail.id,
      });
      setReviews(data.data);
    } catch (error) {}
  };
  const getTripAdvisorReview = async () => {
    try {
      const { data } = await getTripAdvisorReviews({
        page: 1,
        size: 5,
        hotelId: hotelDetail.id,
      });
      setReviewsTripadvisor(data.data);
    } catch (error) {}
  };

  const fetInterestingPlaces = async () => {
    try {
      const { data } = await getInterestingPlaces({
        page: 1,
        size: 10,
        hotelId: hotelDetail.id,
        latitude: hotelDetail?.address?.coordinate.latitude,
        longitude: hotelDetail?.address?.coordinate.longitude,
      });
      if (data.code === 200) {
        setListInterestingPlaces(data.data);
      }
    } catch (error) {}
  };

  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };
  const handleScroll = () => {
    if (ref.current) {
      if (ref.current.getBoundingClientRect().top <= -600) {
        setSticky(listPosition.POSITION_DATE_ROOM); // over date room
      } else if (ref.current.getBoundingClientRect().top <= -235) {
        setSticky(listPosition.POSITION_IMAGE); // over image banner
      } else if (ref.current.getBoundingClientRect().top <= -10) {
        setSticky(listPosition.POSITION_MENU); // over menu bar banner
      } else {
        setSticky("");
      }
    }
    if (refStart.current && refEnd.current) {
      if (
        refStart.current.getBoundingClientRect().top + 48 >
          window.innerHeight ||
        refEnd.current.getBoundingClientRect().top <= 136
      ) {
        setIsShowSelectPrice(true);
      } else {
        setIsShowSelectPrice(false);
      }
    }
  };
  const handleScrollToRate = () => {
    setVisibleDrawerType("");
    setTimeout(() => {
      if (refStart.current) {
        refStart.current.scrollIntoView({
          behavior: "smooth",
          block: "center",
        });
      }
    }, 50);
  };
  const handleSelectParams = (values) => {
    let paramsSearchTemp = {
      ...paramsSearch,
      adults: values.adultSearch,
      children: values.childSearch,
      rooms: values.roomSearch,
      childrenAges: values.childrenAges,
    };
    if (!checkIsEmptyParamsQuery()) {
      if (!isEmpty(values.childrenAges)) {
        paramsSearchTemp = {
          ...paramsSearchTemp,
          childrenAges: values.childrenAges.toString(),
        };
      } else {
        delete paramsSearchTemp.childrenAges;
      }
      router.push({
        pathname: routeStatic.HOTEL_DETAIL.href,
        query: {
          alias: router.query.alias,
          ...paramsSearchTemp,
          priceKey: router.query.priceKey,
        },
      });
    } else {
      setParamsSearch(paramsSearchTemp);
      getRoomHotel(paramsSearchTemp);
      handleLastSearchHistoryToStorage(paramsSearchTemp);
    }
  };
  const handleBookRoomRate = (params = {}) => async () => {
    gtm.addEventGtm(listEventHotel.HotelAddToCart);
    router.push({
      pathname: routeStatic.BOOKING_HOTEL.href,
      query: {
        roomKey: params.roomKey,
        rateKey: params.rateKey,
        ...genParamsUrl(),
        hotelId: hotelDetail.id,
        promotionCode: params?.promotionCode || "",
      },
    });
  };

  const checkShowComparePrice = () => {
    let isShow = false;
    listRoom.forEach((el) => {
      el.rates.forEach((item) => {
        if (!isEmpty(item.specialOffer)) {
          isShow = true;
          return isShow;
        }
      });
      if (isShow) return isShow;
    });
    return isShow;
  };
  const setFullDate = (start, end) => {
    let paramsSearchTemp = {
      ...paramsSearch,
      checkIn: start.format(formatDate.firstDay),
      checkOut: end.format(formatDate.firstDay),
    };
    if (!checkIsEmptyParamsQuery()) {
      if (!isEmpty(paramsSearchTemp.childrenAges)) {
        paramsSearchTemp = {
          ...paramsSearchTemp,
          childrenAges: paramsSearch.childrenAges.toString(),
        };
      } else {
        delete paramsSearchTemp.childrenAges;
      }
      router.push({
        pathname: routeStatic.HOTEL_DETAIL.href,
        query: {
          alias: router.query.alias,
          ...paramsSearchTemp,
          priceKey: router.query.priceKey,
        },
      });
    } else {
      setParamsSearch(paramsSearchTemp);
      getRoomHotel(paramsSearchTemp);
      handleLastSearchHistoryToStorage(paramsSearchTemp);
    }
  };
  return (
    <Layout
      isHeader={false}
      title={`Giá phòng ${hotelDetail.name} từ ${paramsInit.checkIn} đến ${paramsInit.checkOut} | Đảm bảo giá tốt nhất`}
      description={`${hotelDetail.name} - Đặt phòng khách sạn ${hotelDetail.name} trực tuyến ở ${hotelDetail?.address?.provinceName} từ ${paramsInit.checkIn} đến ${paramsInit.checkOut}, cam kết giá rẻ nhất, nhanh nhất và dịch vụ khách hàng tốt nhất.`}
      keywords={`khách sạn, đặt phòng khách sạn, đặt phòng giá rẻ, đặt khách sạn hạng sang giá tốt, đặt khách sạn sát ngày giá tốt, đặt phòng khách sạn thuận tiện, hỗ trợ dịch vụ khách hàng 24/7 đặt phòng khách sạn, ${hotelDetail.name} giá rẻ`}
      iconOgImage={hotelDetail?.thumbnail?.image?.small}
    >
      <Box ref={ref}>
        <BannerHotel
          item={hotelDetail}
          sticky={sticky}
          listPosition={listPosition}
        />
        <Box className={classes.boxSearchContent}>
          <InfoHotelDetails
            item={hotelDetail}
            handleViewReview={() =>
              setVisibleDrawerType(listTypeModal.MODAL_REVIEW_HOTEL)
            }
            listInterestingPlaces={listInterestingPlaces}
            handleScrollToRate={handleScrollToRate}
            isShowComparePrice={checkShowComparePrice()}
          />
          <Box id="banner_details" className={classes.bannerImg} />
          <Box className={classes.divider} />
          <ConvenientHotel toggleDrawer={toggleDrawer} item={hotelDetail} />
          <Box className={classes.divider} />
          <Box p={2}>
            <Box className={classes.convenientHotel}>
              {listString.IDS_MT_SELECT_ROOM}
            </Box>
            <SelectParamsDetail
              checkIn={moment(paramsSearch.checkIn, formatDate.firstDay)}
              checkOut={moment(paramsSearch.checkOut, formatDate.firstDay)}
              // setCheckIn={setCheckIn}
              // setCheckOut={setCheckOut}
              setFullDate={setFullDate}
              paramsSearch={paramsSearch}
              sticky={sticky}
              listPosition={listPosition}
              handleSelectParams={handleSelectParams}
            />
          </Box>
          <ListRoomHotel
            listRoom={listRoom}
            hotelDetail={hotelDetail}
            refStart={refStart}
            refEnd={refEnd}
            handleBookRoomRate={handleBookRoomRate}
            isFetchingRoom={isFetchingRoom}
          />

          <TopAddress
            hotelDetail={hotelDetail}
            listInterestingPlaces={listInterestingPlaces}
            handleScrollToRate={handleScrollToRate}
          />
          <Box className={classes.divider} />
          <Box p={2}>
            <Box className={classes.convenientHotel}>
              {listString.IDS_MT_TEXT_EVALUATE}
            </Box>
            <EvaluateHotelDetails item={hotelDetail?.rating?.hotel || {}} />
            {!isEmpty(reviews) &&
              reviews.items.map((el, index) => {
                if (index >= 3) return null;
                return (
                  <ReviewCommentHotel
                    key={index.toString()}
                    item={el}
                    isShowLineClamp
                  />
                );
              })}
            {!isEmpty(reviews) && reviews.total > 3 && (
              <Box display="flex" justifyContent="center">
                <ButtonComponent
                  typeButton="outlined"
                  borderColor="#A0AEC0"
                  backgroundColor="initial"
                  width="fit-content"
                  borderRadius={8}
                  color="1A202C"
                  padding="8px 22px"
                  handleClick={() =>
                    setVisibleDrawerType(listTypeModal.MODAL_REVIEW_HOTEL)
                  }
                >
                  {listString.IDS_MT_TEXT_LOAD_MORE_REVIEW}
                </ButtonComponent>
              </Box>
            )}
          </Box>
          <Box className={classes.divider} />
          <Box p={2}>
            <Box className={classes.convenientHotel}>
              {listString.IDS_MT_TEXT_HOTEL_POLICY}
            </Box>
            <Box className={classes.wrapCheckIn}>
              <Box className={classes.textCheckIn}>
                {listString.IDS_MT_TEXT_CHECK_IN_ROOM}
                <Typography variant="subtitle2" className={classes.timeCheckIn}>
                  {`Từ ${hotelDetail?.checkInTime}`}
                </Typography>
              </Box>
              <Box className={classes.dividerTime} />
              <Box className={classes.textCheckIn}>
                {listString.IDS_MT_TEXT_CHECK_OUT_ROOM}
                <Typography variant="subtitle2" className={classes.timeCheckIn}>
                  {`Trước ${hotelDetail?.checkOutTime}`}
                </Typography>
              </Box>
            </Box>
            {hotelDetail?.policies?.map((el, index) => {
              if (index > 2) return null;
              return (
                <Box
                  display="flex"
                  flexDirection="column"
                  key={index.toString()}
                >
                  <Box component="span" fontSize={14} fontWeight={600} pb={1}>
                    {el.title}
                  </Box>
                  <Box
                    fontSize={14}
                    color="gray.grayDark1"
                    maxHeight={132}
                    overflow="hidden"
                    mb={12 / 8}
                    lineHeight="19px"
                    className={classes.itemPolices}
                    dangerouslySetInnerHTML={{
                      __html: sanitizeHtml(
                        unEscape(unescape(el.description || "")),
                        optionClean
                      ),
                    }}
                  />
                </Box>
              );
            })}

            <Box display="flex" justifyContent="center">
              <ButtonComponent
                typeButton="outlined"
                borderColor="#A0AEC0"
                backgroundColor="initial"
                width="fit-content"
                borderRadius={8}
                color="1A202C"
                padding="8px 22px"
                handleClick={() => {
                  setVisibleDrawerType(listTypeModal.MODAL_INFO_HOTEL);
                  setInitialTab(listTab.TAB_POLICY);
                }}
              >
                {listString.IDS_MT_TEXT_LOAD_MORE}
              </ButtonComponent>
            </Box>
          </Box>
          <Box className={classes.divider} />
          <Box p={2}>
            <Box className={classes.convenientHotel}>
              {listString.IDS_MT_TEXT_HOTEL_DES}
            </Box>
            <Box
              fontSize={14}
              color="gray.grayDark1"
              maxHeight={132}
              overflow="hidden"
              mb={12 / 8}
              lineHeight="22px"
              dangerouslySetInnerHTML={{
                __html: sanitizeHtml(
                  unEscape(unescape(hotelDetail?.descriptions || "")),
                  optionClean
                ),
              }}
            />
            <Box display="flex" justifyContent="center">
              <ButtonComponent
                typeButton="outlined"
                borderColor="#A0AEC0"
                backgroundColor="initial"
                width="fit-content"
                borderRadius={8}
                color="1A202C"
                padding="8px 22px"
                handleClick={() => {
                  setVisibleDrawerType(listTypeModal.MODAL_INFO_HOTEL);
                  setInitialTab(listTab.TAB_DESCRIPTION);
                }}
              >
                {listString.IDS_MT_TEXT_LOAD_MORE}
              </ButtonComponent>
            </Box>
          </Box>
          <Box className={classes.divider} />
          <ListHotelRecent
            hotelDetail={hotelDetail}
            paramsSearch={paramsSearch}
          />
          {!isEmpty(listRoom) && (
            <FooterSelectBestPrice
              isShowSelectPrice={isShowSelectPrice}
              listRoom={listRoom}
              handleScrollToRate={handleScrollToRate}
            />
          )}
          <ConvenientPopup
            open={visibleDrawerType === listTypeModal.MODAL_INFO_HOTEL}
            toggleDrawer={toggleDrawer}
            hotelDetail={hotelDetail}
            initialTab={
              initialTab === listTab.TAB_DESCRIPTION
                ? 2
                : initialTab === listTab.TAB_POLICY
                ? 1
                : 0
            }
            handleScrollToRate={handleScrollToRate}
          />
          <ReviewHotelPopup
            open={visibleDrawerType === listTypeModal.MODAL_REVIEW_HOTEL}
            toggleDrawer={toggleDrawer}
            reviewsInit={reviews || {}}
            reviewsInitTripadvisor={reviewsTripadvisor || {}}
            hotelRating={hotelDetail?.rating?.hotel}
            hotelRatingTripadvisor={hotelDetail?.rating?.tripadvisor}
            handleScrollToRate={handleScrollToRate}
            hotelId={hotelDetail.id}
          />
        </Box>
      </Box>
      <WriteReviewModal hotelDetail={hotelDetail} />
    </Layout>
  );
};

export default HotelDetailMobile;
