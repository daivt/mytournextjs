import clsx from "clsx";
import { useState } from "react";
import { useRouter } from "next/router";
import { Box, Grid, makeStyles, Typography } from "@material-ui/core";

import { isEmpty, getMemberDeal } from "@utils/helpers";
import { listString, routeStatic } from "@utils/constants";
import {
  IconCancelPolicy,
  IconBreakFast,
  IconInfo,
  IconShowPriceMobile,
  IconFlash,
  IconBoxGif,
  IconDot,
} from "@public/icons";

import ButtonComponent from "@src/button/Button";
import PromotionItem from "@components/mobile/hotels/details/roomDetails/PromotionItem";
import PolicyRoomlModal from "@components/mobile/hotels/details/PolicyRoomlModal";
import PoliciesModal from "@components/mobile/hotels/details/PoliciesModal";

const useStyles = makeStyles((theme) => ({
  rateContainer: {
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    padding: "10px 0",
  },
  textCancelPolicy: {
    fontSize: 14,
    lineHeight: "17px",
    fontWeight: "normal",
    color: theme.palette.gray.grayDark8,
    marginLeft: 6,
  },
  iconFlash: {
    stroke: theme.palette.green.greenLight7,
  },
  textFreeBreakFast: {
    color: theme.palette.green.greenLight7,
  },
  boxRight: { display: "flex", whiteSpace: "nowrap", alignItems: "flex-start" },
  iconBreakFast: {
    stroke: theme.palette.gray.grayDark8,
  },
  iconPolicies: {
    stroke: theme.palette.blue.blueLight8,
    marginTop: -2,
  },
  iconBuySignals: {
    stroke: theme.palette.green.greenLight7,
  },
  buyingSignals: {
    color: theme.palette.green.greenLight7,
  },
  iconBed: {
    stroke: theme.palette.blue.blueLight8,
    color: theme.palette.blue.blueLight8,
  },
  numberAllotment: {
    color: theme.palette.red.redLight5,
    marginLeft: 0,
  },
  wrapRoomAndPrice: {
    fontSize: 12,
    color: theme.palette.black.black3,
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    width: "100%",
    justifyContent: "center",
    marginTop: 12,
  },
  subPrice: {
    textDecorationLine: "line-through",
    color: theme.palette.gray.grayDark8,
    margin: "6px 0 2px 0",
    fontSize: 12,
    lineHeight: "14px",
  },
  mainPrice: {
    fontWeight: 600,
    display: "flex",
    alignItems: "center",
    fontSize: 16,
    lineHeight: "19px",
  },
  discountPercent: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 42,
    minHeight: 18,
    color: theme.palette.white.main,
    background: theme.palette.pink.main,
    borderRadius: "3px 3px 0px 3px",
    position: "relative",
  },
  discountText: {
    padding: "2px 4px",
    fontSize: 12,
    lineHeight: "14px",
    fontWeight: 600,
  },
  triangleBottomright: {
    width: 0,
    height: 0,
    borderStyle: "solid",
    borderColor: `transparent ${theme.palette.pink.main} transparent transparent`,
    borderStyle: "solid",
    borderWidth: "0px 5px 5px 0",
    position: "absolute",
    right: 0,
    bottom: -4,
  },
  warpIconShowPriceMobile: {
    display: "flex",
    background: theme.palette.blue.blueLight9,
    width: 107,
    height: 32,
    borderRadius: 4,
    color: theme.palette.primary.main,
    alignItems: "center",
    marginTop: 6,
  },
  wrapShowPriceMobile: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  freeCancelPolicy: {
    stroke: theme.palette.green.greenLight7,
  },
  contentRate: {
    paddingBottom: 12,
  },
  info: {
    marginLeft: 4,
    stroke: theme.palette.gray.grayDark8,
  },
  infoActive: {
    marginLeft: 4,
    stroke: theme.palette.green.greenLight7,
  },
  dateFreeCancel: {
    marginTop: 4,
    fontSize: 12,
    lineHeight: "14px",
    fontWeight: 400,
  },
  boxLeft: {
    display: "flex",
    marginBottom: 12,
  },
  boxLeftGift: {
    marginBottom: 4,
  },
  wrapGiftBox: {
    marginTop: 4,
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    background: theme.palette.green.greenLight8,
    borderRadius: 8,
    marginLeft: 20,
    width: "calc(100% - 36px)",
    padding: "4px 0",
  },
  itemGift: {
    padding: 4,
    display: "flex",
    alignItems: "center",
  },
  iconDot: {
    fill: theme.palette.black.black3,
    margin: "0 6px 0 8px",
    borderRadius: "50%",
  },
  textItemPolicy: {
    display: "-webkit-box",
    WebkitLineClamp: 3,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
    color: theme.palette.blue.blueLight8,
    fontSize: 14,
    lineHeight: "17px",
  },
}));

const listTypeModal = {
  MODAL_CANCEL_POLICY: "MODAL_CANCEL_POLICY",
  MODAL_POLICIES: "MODAL_POLICIES",
};

const ItemRatesRoom = ({
  item = {},
  index = 0,
  isShowOption = false,
  handleViewRoomRate = () => {},
  handleBookRoomRate = () => {},
  isShowPrice = true,
  customerStyleRateRoom = "",
  isShowCountDown = false,
  isShowBtnBookingNow = true,
  isShowCode = false,
  isShowDetailsGift = false,
  isColumn = true,
}) => {
  const classes = useStyles();
  const router = useRouter();
  const handleBooking = () => {
    handleBookRoomRate();
  };
  const [visibleDrawerType, setVisibleDrawerType] = useState("");
  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };
  const handleLogin = (event) => {
    event.preventDefault();
    router.push({
      pathname: routeStatic.LOGIN.href,
    });
  };
  const arrSignalPromo = (signalPromo = []) => {
    let resultPromo = [];
    return (resultPromo = signalPromo.filter(
      (item) => item.groupCode === "PROMOTIONS"
    ));
  };
  const getNamePolices = (policies = []) => {
    let textPolicies = "";
    policies.forEach((el, index) => {
      textPolicies += `${
        index !== 0 ? `${index === policies.length - 1 ? " & " : ", "}` : ""
      }${el.title}`;
    });
    return textPolicies;
  };
  return (
    <Box className={clsx(classes.rateContainer, customerStyleRateRoom)}>
      {isShowOption && (
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          pb={10 / 8}
          fontSize={14}
          lineHeight="17px"
        >
          <Box
            component="span"
            color="black.black3"
            fontWeight={600}
            lineHeight="17px"
            fontSize={14}
          >{`Lựa chọn ${index + 1} `}</Box>
          <ButtonComponent
            typeButton="text"
            width="fit-content"
            color="#00B6F3"
            padding="0 8px"
            height={20}
            handleClick={() => handleViewRoomRate(item)}
          >
            {listString.IDS_MT_TEXT_VIEW_DETAIL}
          </ButtonComponent>
        </Box>
      )}
      <Grid container className={classes.contentRate}>
        <Grid item xs={isColumn ? 8 : 12}>
          {item.freeCancellation ? (
            <>
              <Box
                className={classes.boxLeft}
                onClick={toggleDrawer(listTypeModal.MODAL_CANCEL_POLICY)}
              >
                <IconCancelPolicy
                  className={`svgFillAll ${classes.freeCancelPolicy}`}
                />
                <Typography
                  className={classes.textCancelPolicy}
                  style={{ color: "#48BB78" }}
                >
                  {listString.IDS_MT_TEXT_FREE_CANCEL}
                </Typography>
                <IconInfo className={`svgFillAll ${classes.infoActive}`} />
              </Box>
              <Typography
                variant="body2"
                style={{ marginLeft: "22px", marginTop: -6, marginBottom: 3 }}
              >
                {!isEmpty(item.hotelCancellationPolicies) &&
                  item.hotelCancellationPolicies.map((el, i) => (
                    <Box
                      component="span"
                      className={classes.dateFreeCancel}
                      mt={-6 / 8}
                      key={i}
                    >
                      {el.price === el.refundAmount &&
                        `Trước ${el.dateTo.split(" ")[0]}`}
                    </Box>
                  ))}
              </Typography>
            </>
          ) : (
            <Box
              className={classes.boxLeft}
              onClick={() => {
                if (item.refundable) {
                  setVisibleDrawerType(listTypeModal.MODAL_CANCEL_POLICY);
                }
              }}
            >
              <IconCancelPolicy />
              <Typography className={classes.textCancelPolicy}>
                {item.shortCancelPolicy}
              </Typography>
              {item.refundable && (
                <IconInfo className={`svgFillAll ${classes.info}`} />
              )}
            </Box>
          )}
          <PolicyRoomlModal
            open={visibleDrawerType === listTypeModal.MODAL_CANCEL_POLICY}
            toggleDrawer={toggleDrawer}
            item={item}
          />
          {item?.freeBreakfast ? (
            <Box className={classes.boxLeft}>
              <IconBreakFast />
              <Typography
                className={clsx(
                  classes.textCancelPolicy,
                  classes.textFreeBreakFast
                )}
              >
                {listString.IDS_MT_TEXT_ROOM_FREE_BREAK_FAST}
              </Typography>
            </Box>
          ) : (
            <Box className={classes.boxLeft}>
              <IconBreakFast
                className={`svgFillAll ${classes.iconBreakFast}`}
              />
              <Typography className={classes.textCancelPolicy}>
                {listString.IDS_MT_TEXT_ROOM_NO_FREE_BREAK_FAST}
              </Typography>
            </Box>
          )}
          {!isEmpty(item?.policies) && (
            <Box
              className={classes.boxLeft}
              onClick={toggleDrawer(listTypeModal.MODAL_POLICIES)}
            >
              <Box style={{ width: 16, height: 16 }}>
                <IconInfo className={`svgFillAll ${classes.iconPolicies}`} />
              </Box>
              <Box
                display="flex"
                flexDirection="column"
                pl={6 / 8}
                className={classes.textItemPolicy}
              >
                {getNamePolices(item?.policies)}
              </Box>
            </Box>
          )}
          {item?.requestPrice ? (
            <Box className={classes.boxLeft}>
              <Box style={{ width: "16px", height: "16px  " }}>
                {" "}
                <IconFlash />
              </Box>
              <Typography
                className={classes.textCancelPolicy}
                style={{ color: "#ED8936" }}
              >
                {listString.IDS_MT_TEXT_CONFIRM_AROUNR_30_MINUTES}
              </Typography>
            </Box>
          ) : (
            <Box className={classes.boxLeft}>
              <Box style={{ width: "16px", height: "16px " }}>
                {" "}
                <IconFlash className={`svgFillAll ${classes.iconFlash}`} />
              </Box>
              <Typography
                className={classes.textCancelPolicy}
                style={{ color: "#48BB78" }}
              >
                {listString.IDS_MT_TEXT_CONFIRM_NOW}
              </Typography>
            </Box>
          )}
          {!isEmpty(item.buyingSignals) && (
            <Box className={clsx(classes.boxLeft, classes.boxLeftGift)}>
              <Box style={{ width: 16, height: 16 }}>
                <IconBoxGif
                  className={`svgFillAll ${classes.iconBuySignals}`}
                />
              </Box>
              <Typography
                className={clsx(
                  classes.textCancelPolicy,
                  classes.buyingSignals
                )}
              >
                Quà tặng kèm
              </Typography>
            </Box>
          )}

          {isShowDetailsGift && !isEmpty(item.buyingSignals) && (
            <Box className={classes.wrapGiftBox}>
              {arrSignalPromo(item.buyingSignals).map((el, index) => (
                <Box className={classes.itemGift} key={index}>
                  {item.buyingSignals.length > 1 && (
                    <IconDot className={`svgFillAll ${classes.iconDot}`} />
                  )}
                  {el.name}
                </Box>
              ))}
            </Box>
          )}
        </Grid>
        {!item.hiddenPrice && isShowPrice && (
          <Grid item xs={isColumn ? 4 : 12} className={classes.boxRight}>
            <Box className={classes.wrapRoomAndPrice}>
              {!isEmpty(item.basePromotionInfo) && (
                <>
                  <Box className={classes.discountPercent}>
                    <Box className={classes.discountText}>
                      -{item?.basePromotionInfo?.discountPercentage}
                      {""}%
                    </Box>
                    <Box className={classes.triangleBottomright}></Box>
                  </Box>
                  <Box component="span" className={classes.subPrice}>
                    <Typography variant="body2">
                      {item?.basePromotionInfo?.priceBeforePromotion.formatMoney()}
                      đ /đêm
                    </Typography>
                  </Box>
                </>
              )}

              <Box component="span" className={classes.mainPrice}>
                <Typography variant="subtitle1" component="span">
                  {`${item?.basePrice?.formatMoney()}đ`}
                </Typography>
                <Box component="span" color="black.black4" fontWeight="normal">
                  /đêm
                </Box>
              </Box>

              {item?.priceOnlyMobile && (
                <Box className={classes.warpIconShowPriceMobile}>
                  <IconShowPriceMobile
                    style={{ width: "13px", margin: "11px 7px" }}
                  />

                  <Typography
                    variant="body2"
                    className={classes.wrapShowPriceMobile}
                  >
                    {listString.IDS_MT_TEXT_ONLY_SHOW_PRICE_MOBILE}
                  </Typography>
                </Box>
              )}
            </Box>
          </Grid>
        )}
      </Grid>
      {!isEmpty(item.promotions) && (
        <PromotionItem
          isShowCountDown={isShowCountDown}
          promotion={item.promotions[0]}
        />
      )}

      {isShowBtnBookingNow && isShowPrice && (
        <>
          {!item.hiddenPrice && (
            <>
              <Box display="flex" alignItems="center" justifyContent="flex-end">
                {item?.availableAllotment > 0 && item?.availableAllotment <= 5 && (
                  <Box className={classes.boxLeft} pr={12 / 8}>
                    <Typography
                      className={clsx(
                        classes.textCancelPolicy,
                        classes.numberAllotment
                      )}
                    >
                      {item?.availableAllotment === 1
                        ? "Phòng cuối cùng của chúng tôi!"
                        : `${listString.IDS_MT_TEXT_ONLY} ${item?.availableAllotment} phòng trống!`}
                    </Typography>
                  </Box>
                )}
                <ButtonComponent
                  backgroundColor="#FF1284"
                  borderRadius={8}
                  width="fit-content"
                  height={36}
                  padding="8px 16px"
                  handleClick={handleBooking}
                  fontSize={16}
                  fontWeight={600}
                >
                  {listString.IDS_MT_TEXT_SELECT}
                </ButtonComponent>
              </Box>
            </>
          )}
          {item.hiddenPrice && (
            <>
              <Box display="flex" flexDirection="column" alignItems="flex-end">
                <Box
                  component="span"
                  fontSize={14}
                  lineHeight="17px"
                  pb={6 / 8}
                >
                  {listString.IDS_MT_TEXT_LOGIN_USE_PROMO}
                  <Box
                    component="span"
                    pl={2 / 8}
                    fontWeight={600}
                    color="secondary.main"
                  >{`Giảm ${
                    !isEmpty(item.basePromotionInfo)
                      ? getMemberDeal(item.basePromotionInfo, item.basePrice)
                      : "10%"
                  }`}</Box>
                </Box>
                <Box
                  component="span"
                  pb={6 / 8}
                  color="gray.grayDark8"
                  fontStyle="italic"
                >
                  {listString.IDS_MT_TEXT_DISCOUNT_ON_MEMBER}
                </Box>
              </Box>
              <Box display="flex" alignItems="center" justifyContent="flex-end">
                {item?.availableAllotment > 0 && item?.availableAllotment <= 5 && (
                  <Box className={classes.boxLeft} pr={12 / 8}>
                    <Typography
                      className={clsx(
                        classes.textCancelPolicy,
                        classes.numberAllotment
                      )}
                    >
                      {listString.IDS_MT_TEXT_ONLY} {item?.availableAllotment}{" "}
                      phòng trống!
                    </Typography>
                  </Box>
                )}
                <ButtonComponent
                  backgroundColor="#FF1284"
                  width="fit-content"
                  borderRadius={8}
                  handleClick={handleLogin}
                  fontSize={16}
                  fontWeight={600}
                >
                  {listString.IDS_MT_TEXT_LOGIN}
                </ButtonComponent>
              </Box>
            </>
          )}
        </>
      )}
      {!isEmpty(item?.policies) && (
        <PoliciesModal
          open={visibleDrawerType === listTypeModal.MODAL_POLICIES}
          toggleDrawer={toggleDrawer}
          item={item.policies}
        />
      )}
    </Box>
  );
};

export default ItemRatesRoom;
