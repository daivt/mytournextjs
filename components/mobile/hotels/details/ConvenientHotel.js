import { memo } from "react";
import { Box, makeStyles, Typography } from "@material-ui/core";

import { listString } from "@utils/constants";
import { adapterAmenitiesGroup, areEqualItem } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  convenientHotel: {
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
    color: theme.palette.black.black3,
    margin: "16px 0",
  },
  wrapContainer: {
    margin: 16,
    fontSize: 11,
    lineHeight: "13px",
    fontWeight: "normal",
  },
  classIcon: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
  },
  loadMoreConvenient: {
    height: 40,
    width: 40,
    borderRadius: "50%",
    background: theme.palette.blue.blueLight10,
    color: theme.palette.primary.main,
    display: "flex",
    alignItems: "center",
    justifyContent: "center ",
    fontWeight: 600,
    fontSize: 14,
  },
  iconConvient: {
    width: 24,
    height: 24,
  },
  nameAmenities: {
    fontSize: 11,
    lineHeight: "13px",
    textAlign: "center",
    display: "-webkit-box",
    WebkitLineClamp: 2,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
  },
}));

const ConvenientHotel = ({ item, toggleDrawer }) => {
  const classes = useStyles();
  return (
    <Box className={classes.wrapContainer}>
      <Typography className={classes.convenientHotel}>
        {listString.IDS_MT_TEXT_CONVENIENT_HOTEL}
      </Typography>
      <Box display="flex" onClick={toggleDrawer("MODAL_INFO_HOTEL")}>
        <Box display="flex" width="calc(100% - 40px)">
          {adapterAmenitiesGroup(item.groupAmenities).map((el, index) => {
            if (index >= 5) return null;
            return (
              <Box key={index.toString()} width="25%">
                <Box
                  display="flex"
                  color="black.black3"
                  letterSpacing={-0.23}
                  lineHeight={1.5}
                  className={classes.classIcon}
                >
                  <img src={el.icon} className={classes.iconConvient} />
                  <Box component="span" className={classes.nameAmenities}>
                    {el?.name}
                  </Box>
                </Box>
              </Box>
            );
          })}
        </Box>

        {adapterAmenitiesGroup(item.groupAmenities).length > 5 && (
          <Box className={classes.loadMoreConvenient}>
            <Box component="span">{`+${adapterAmenitiesGroup(
              item.groupAmenities
            ).length - 5}`}</Box>
          </Box>
        )}
      </Box>
    </Box>
  );
};
export default memo(ConvenientHotel, areEqualItem);
