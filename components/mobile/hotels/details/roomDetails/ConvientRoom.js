import { Box, makeStyles, Typography } from "@material-ui/core";

import { listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
const useStyles = makeStyles((theme) => ({
  wrapContainer: {
    margin: 16,
    color: theme.palette.black.black3,
  },
  wrapConvient: {
    display: "flex",
  },
  childListConvient: {
    fontSize: 14,
    lineHeight: "28px",
    display: "flex",
    alignItems: "center",
    padding: "3px 0",
  },
  iconConven: {
    width: 24,
    height: 24,
    marginRight: 6,
  },
}));

const ConvientRoom = ({ item = {} }) => {
  const classes = useStyles();
  const arrayGroupAmenities = Object.values(item.groupAmenities);
  return (
    <>
      <Box className={classes.wrapContainer}>
        <Typography variant="subtitle1" style={{ marginBottom: 12 }}>
          {listString.IDS_MT_TEXT_ROOM_CONVIENT}
        </Typography>
        {!isEmpty(arrayGroupAmenities) &&
          arrayGroupAmenities.map((el, index) => (
            <Box key={index.toString()} className={classes.wrapConvient}>
              <Box marginLeft={12 / 8}>
                <Typography variant="subtitle2" style={{ lineHeight: "18px" }}>
                  {el?.name || "Tiện nghi khác"}
                </Typography>
                {!isEmpty(el.amenities) &&
                  el.amenities.map((val, idx) => (
                    <Box key={idx.toString()}>
                      <Typography className={classes.childListConvient}>
                        {!isEmpty(val.icon) && (
                          <img src={val.icon} className={classes.iconConven} />
                        )}
                        {val.name}
                      </Typography>
                    </Box>
                  ))}
              </Box>
            </Box>
          ))}
      </Box>
    </>
  );
};
export default ConvientRoom;
