import clsx from "clsx";
import sanitizeHtml from "sanitize-html";
import { Box, makeStyles, Typography } from "@material-ui/core";

import { unEscape, isEmpty } from "@utils/helpers";
import { listString, optionClean } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  wrapPolicy: {
    margin: 16,
    color: theme.palette.black.black3,
  },
  roomPolicy: {
    fontSize: 18,
    fontWeight: 600,
    lineHeight: "24px",
  },
  cancelPolicy: {
    margin: "8px 0",
    fontSize: 16,
  },
}));

const PolicyRoom = ({ rateSelected = {} }) => {
  const classes = useStyles();
  return (
    <>
      {!isEmpty(rateSelected.policies) && (
        <Box className={classes.wrapPolicy}>
          <Typography className={classes.roomPolicy}>
            {listString.IDS_MT_TEXT_ROOM_POLICY}
          </Typography>
          {!isEmpty(rateSelected.policies) &&
            rateSelected.policies.map((el) => (
              <Box>
                <Typography
                  variant="subtitle1"
                  className={clsx(classes.roomPolicy, classes.cancelPolicy)}
                >
                  {el.title}
                </Typography>
                <Box
                  fontSize={14}
                  lineHeight="22px"
                  color="gray.grayDark1"
                  dangerouslySetInnerHTML={{
                    __html: sanitizeHtml(
                      unEscape(unescape(el.description || "")),
                      optionClean
                    ),
                  }}
                />
              </Box>
            ))}
        </Box>
      )}
    </>
  );
};
PolicyRoom.propTypes = {};
export default PolicyRoom;
