import { Box, makeStyles, Typography } from "@material-ui/core";

import { IconUser2, IconAcreage, IconView, IconBed } from "@public/icons";
import { isEmpty } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  wrapContainer: {
    margin: "16px 16px 12px",
  },
  rating: { marginTop: 6 },
  point: {
    lineHeight: "17px",
    color: theme.palette.white.main,
    background: theme.palette.blue.blueLight8,
    height: 18,
    width: 30,
    padding: "2px 4px",
    borderRadius: 4,
  },
  countEvaluate: {
    display: "flex",
    alignItems: "center",
    marginTop: 14,
    justifyContent: "space-between",
    marginBottom: 9,
  },
  messageReview: {
    fontSize: 14,
    lineHeight: "17px",
    fontWeight: "normal",
    color: theme.palette.black.black3,
    marginLeft: 4,
    whiteSpace: "nowrap",
  },
  countReview: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark7,
    marginLeft: 2,
  },
  viewMessageReview: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.blue.blueLight8,
    display: "flex",
    alignItems: "center",
  },
  iconArrowDownToggle: {
    stroke: theme.palette.blue.blueLight8,
    transform: "rotate(270deg)",
    marginLeft: 2,
  },
  wrapLocation: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    position: "relative",
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  address: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.black.black3,
    fontWeight: "normal",
    width: 252,
  },
  wrapAddress: {
    display: "flex",
    alignItems: "center",
    marginTop: 10,
  },
  wrapMap: {
    display: "flex",
    flexDirection: "column",
    position: "absolute",
    right: 0,
    top: 9,
    alignItems: "center",
    color: theme.palette.blue.blueLight8,
  },
  infoRoom: {
    display: "flex",
    alignItems: "center",
  },
  IconUser2: {
    width: 12,
    height: 12,
  },
  wrapBed: {
    marginTop: 8,
    display: "flex",
  },
  textBed: {
    marginLeft: 6,
    fontWeight: "normal",
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark8,
  },
  viewRoom: {
    display: "inline-block",
    width: 130,
    whiteSpace: "nowrap",
    overflow: "hidden !important",
    textOverflow: "ellipsis",
  },
}));

const InfoRoom = ({ item = initItem }) => {
  const classes = useStyles();

  const getView = (view = []) => {
    let viewString = "";
    view.forEach((el, idx) => {
      viewString = `${viewString}${idx !== 0 ? ", " : ""}${el}`;
    });
    return viewString;
  };

  const getListBed = (bedGroups = []) => {
    let listBed = "";
    bedGroups.forEach((el, index) => {
      listBed = `${listBed}${index !== 0 ? " hoặc " : ""}${el.bedInfo}`;
    });
    return listBed;
  };

  return (
    <>
      <Box className={classes.wrapContainer}>
        <Typography variant="subtitle1" style={{ lineHeight: "24px" }}>
          {item?.name}
        </Typography>
        <Box className={classes.infoRoom}>
          {!isEmpty(item.standardNumberOfGuests) && (
            <>
              <IconUser2 className={classes.IconUser2} />
              <Box marginLeft={1}>{item?.standardNumberOfGuests} người</Box>
            </>
          )}

          {!isEmpty(item.roomArea) && (
            <>
              <IconAcreage style={{ marginLeft: "20px" }} />
              <Box marginLeft={1}>{item?.roomArea}m2 </Box>
            </>
          )}

          {!isEmpty(item.views) && (
            <>
              <IconView style={{ marginLeft: "20px" }} />
              <Box marginLeft={1} className={classes.viewRoom}>
                {getView(item?.views)}{" "}
              </Box>
            </>
          )}
        </Box>

        {!isEmpty(item.rates[0].bedGroups[0].bedInfo) && (
          <>
            <Box className={classes.wrapBed}>
              <IconBed />
              <Typography className={classes.textBed}>
                {getListBed(item?.bedGroups)}
              </Typography>
            </Box>
          </>
        )}
      </Box>
    </>
  );
};
export default InfoRoom;
