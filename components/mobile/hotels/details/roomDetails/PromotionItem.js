import moment from "moment";
import { useState, useEffect } from "react";
import { Box, makeStyles, Typography } from "@material-ui/core";

import { isEmpty } from "@utils/helpers";
import { IconClock } from "@public/icons";
import { listString } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  container: {
    fontSize: 14,
    color: theme.palette.black.black3,
    margin: "0 -16px",
    paddingBottom: 12,
  },
  wrapPromo: {
    background: theme.palette.gray.grayLight26,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "8px 16px",
  },
  wrapPercent: {
    background: theme.palette.primary.main,
    height: 14,
    borderRadius: 2,
    marginLeft: 6,
    minWidth: 34,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  percent: {
    fontSize: 10,
    lineHeight: "12px",
    fontWeight: 600,
    color: theme.palette.white.main,
    padding: "1px 3px",
  },
  iconClockTime: {
    stroke: theme.palette.black.black3,
    marginLeft: -4,
  },
  iconClockCountDown: {
    stroke: theme.palette.orange.main,
    marginLeft: -4,
  },
  itemCountDown: {
    fontSize: 14,
    fontWeight: 600,
    lineHeight: "17px",
    backgroundColor: theme.palette.orange.main,
    padding: "5px 6px",
    borderRadius: 4,
    color: theme.palette.white.main,
  },
  itemBetween: {
    display: "flex",
    justifyContent: "center",
    width: 5,
    color: theme.palette.orange.main,
    fontWeight: 600,
    lineHeight: "17px",
    margin: "0 4px",
  },
}));

let interval = null;

const PromotionItem = ({ promotion = {}, isShowCountDown = false }) => {
  const classes = useStyles();
  const [isOnceDay, setIsOnceDay] = useState(false);
  const [timer, setTimer] = useState({
    hours: "00",
    minutes: "00",
    seconds: "00",
  });
  const startDate = moment();
  const endDate = moment(promotion.validTo);
  const diffTime = endDate.valueOf() - moment().valueOf();
  let duration = moment.duration(diffTime, "milliseconds");
  useEffect(() => {
    if (isShowCountDown && diffTime > 0) {
      const isOnceDayTemp = endDate.diff(startDate) < 86400000;
      setIsOnceDay(isOnceDayTemp);
      if (isOnceDayTemp) {
        interval = setInterval(() => {
          duration = moment.duration(duration - 1000, "milliseconds");
          setTimer({
            hours:
              duration.hours() < 10 ? `0${duration.hours()}` : duration.hours(),
            minutes:
              duration.minutes() < 10
                ? `0${duration.minutes()}`
                : duration.minutes(),
            seconds:
              duration.seconds() < 10
                ? `0${duration.seconds()}`
                : duration.seconds(),
          });
          if (
            duration.hours() === 0 &&
            duration.minutes() === 0 &&
            duration.seconds() === 0
          ) {
            clearInterval(interval);
          }
        }, 1000);
      }
    }
    return () => {
      clearInterval(interval);
    };
  }, []);

  const basePromotion = !isEmpty(promotion.basePromotion)
    ? promotion.basePromotion
    : {};
  return (
    <Box className={classes.container}>
      <Box className={classes.wrapPromo}>
        <Box display="flex">
          {listString.IDS_MT_TEXT_TYPE_PROMO_CODE}:
          <Typography
            component="span"
            style={{
              fontSize: "12px",
              fontWeight: "600",
              lineHeight: "14px",
              marginLeft: "2px",
            }}
            color="primary"
          >
            {promotion.code}
          </Typography>
          {!isEmpty(basePromotion) && (
            <Box className={classes.wrapPercent}>
              <Box component="span" className={classes.percent}>
                {`-${
                  basePromotion.valueType === "percent"
                    ? `${basePromotion.percent}%`
                    : `${basePromotion.value.formatMoney()}đ`
                }`}
              </Box>
            </Box>
          )}
        </Box>
        <Box>
          <Typography component="span" variant="subtitle1" color="secondary">
            {`${basePromotion.price.formatMoney()}đ`}
          </Typography>
          <Typography
            component="span"
            color="secondary"
            style={{ fontSize: "12px", fontWeight: "normal", paddingLeft: 2 }}
          >
            /đêm
          </Typography>
        </Box>
      </Box>
      {isShowCountDown && diffTime > 0 ? (
        isOnceDay ? (
          <Box
            display="flex"
            alignItems="center"
            fontSize={12}
            lineHeight="14px"
            bgcolor="rgba(237,137,54,0.1)"
            p="5px 16px"
          >
            <IconClock className={`svgFillAll ${classes.iconClockCountDown}`} />
            <Box
              component="span"
              color="orange.main"
              pr={1}
            >{`Thời gian còn lại`}</Box>
            <Box className={classes.itemCountDown}>{timer.hours}</Box>
            <Box className={classes.itemBetween}>{`:`}</Box>
            <Box className={classes.itemCountDown}>{timer.minutes}</Box>
            <Box className={classes.itemBetween}>{`:`}</Box>
            <Box className={classes.itemCountDown}>{timer.seconds}</Box>
          </Box>
        ) : (
          <Box
            display="flex"
            alignItems="center"
            fontSize={12}
            lineHeight="14px"
            bgcolor="gray.grayLight22"
            p="5px 16px"
          >
            <IconClock className={`svgFillAll ${classes.iconClockTime}`} />
            <Box component="span" pl={6 / 8}>{`Thời gian: ${moment(
              promotion.validFrom
            ).format("DD/MM/YYYY")}-${endDate.format("DD/MM/YYYY")}.`}</Box>
            {endDate.diff(startDate, "days") < 6 ? (
              <Box
                pl={2 / 8}
                component="span"
                color="red.redLight5"
              >{`Chỉ còn ${endDate.diff(startDate, "days")} ngày nữa!`}</Box>
            ) : null}
          </Box>
        )
      ) : null}
    </Box>
  );
};

export default PromotionItem;
