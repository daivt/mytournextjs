import { makeStyles } from "@material-ui/styles";
import { Box, IconButton } from "@material-ui/core";

import { isEmpty } from "@utils/helpers";
import { IconClose, IconImageHotel } from "@public/icons";

import Image from "@src/image/Image";
import SlideShow from "@components/common/slideShow/SlideShow";
import ViewAlbumImageHotelDetail from "@components/mobile/hotels/details/ViewAlbumImageHotelDetail";

const useStyles = makeStyles((theme) => ({
  container: {
    position: "relative",
  },
  bannerSearchBox: {
    position: "relative",
    height: 252,
  },
  imageHotel: {
    width: "100%",
    height: "100%",
  },
  headerSearchBox: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    padding: 16,
    display: "flex",
    justifyContent: "space-between",
  },
  iconBack: {
    stroke: theme.palette.black.main,
  },
  btnIcon: {
    backgroundColor: `${theme.palette.white.main} !important`,
    width: 36,
    height: 36,
    padding: 0,
  },
  btnIconFavorite: {
    marginRight: 16,
  },
  totalImageHotel: {
    position: "absolute",
    display: "flex",
    alignItems: "center",
    bottom: 28,
    right: 16,
    height: 30,
    borderRadius: 100,
    padding: 6,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    color: theme.palette.white.main,
    fontSize: 14,
    lineHeight: 17,
  },
  btnIconImageHotel: {},
  favorite: {
    fill: theme.palette.pink.main,
  },
  imageSlick: {
    width: "100%",
    height: 252,
  },
  slickDot: {
    bottom: "30px !important",
    "& li": {
      height: 2,
      width: 16,
      margin: "0 2px",
      "& div": {
        height: 2,
        width: 16,
        borderRadius: 100,
        backgroundColor: theme.palette.gray.grayLight22,
        border: "none",
      },
    },
    "&.slick-active": {
      "& div": {
        backgroundColor: theme.palette.white.main,
      },
    },
  },
  imageEmpty: {
    width: 343,
    height: 128,
  },
}));
const settings = {
  dots: true,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  arrows: false,
  lazyLoad: true,
};
const BannerRoom = ({ item = {}, toggleDrawerRoom = () => () => {} }) => {
  const classes = useStyles();
  const [visibleDrawerImage, setVisibleDrawerImage] = React.useState("");
  const toggleDrawerImage = (open = "") => () => {
    setVisibleDrawerImage(open);
  };
  return (
    <Box className={classes.container}>
      <Box
        className={classes.bannerSearchBox}
        onClick={() => {
          if (item?.images?.length > 0) {
            setVisibleDrawerImage("DRAWER_IMAGE");
          }
        }}
      >
        {!isEmpty(item.images) ? (
          <SlideShow settingProps={settings} slickDotStyle={classes.slickDot}>
            {item.images.map((el, index) => {
              if (index > 8) return null;
              return (
                <Box key={index.toString()}>
                  <Image
                    srcImage={el}
                    className={classes.imageSlick}
                    alt={item.name}
                    title={item.name}
                  />
                </Box>
              );
            })}
          </SlideShow>
        ) : (
          <Box
            width="100%"
            height="100%"
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <Image
              srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_room_default.svg"
              className={classes.imageEmpty}
            />
          </Box>
        )}

        <Box className={classes.headerSearchBox}>
          <IconButton
            onClick={toggleDrawerRoom("")}
            className={classes.btnIcon}
          >
            <IconClose className={`svgFillAll ${classes.iconBack}`} />
          </IconButton>
        </Box>
        {item?.images?.length > 1 && (
          <Box className={classes.totalImageHotel}>
            <Box pr={3 / 8} component="span">
              {`+${item?.images.length - 1}`}
            </Box>
            <IconImageHotel />
          </Box>
        )}
      </Box>
      <ViewAlbumImageHotelDetail
        open={visibleDrawerImage === "DRAWER_IMAGE"}
        toggleDrawer={toggleDrawerImage}
        listImage={item.images}
      />
    </Box>
  );
};

export default BannerRoom;
