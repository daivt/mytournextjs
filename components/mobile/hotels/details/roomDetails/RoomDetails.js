import { useRouter } from "next/router";
import { Box, Drawer, makeStyles, Divider } from "@material-ui/core";

import { listString, routeStatic } from "@utils/constants";

import ButtonComponent from "@src/button/Button";
import BannerRoom from "@components/mobile/hotels/details/roomDetails/BannerRoom";
import InfoRoom from "@components/mobile/hotels/details/roomDetails/InfoRoom";
import PolicyRoom from "@components/mobile/hotels/details/roomDetails/PolicyRoom";
import ConvientRoom from "@components/mobile/hotels/details/roomDetails/ConvientRoom";
import ItemRatesRoom from "@components/mobile/hotels/details/ItemRatesRoom";

const useStyles = makeStyles((theme) => ({
  paperAnchorBottom: {
    borderRadius: 0,
    width: "100vw",
    height: "100vh",
  },
  infoRoomDetail: {
    position: "absolute",
    width: "100%",
    marginTop: -20,
    backgroundColor: theme.palette.white.main,
    borderRadius: "16px 16px 0px 0px",
    paddingBottom: 90,
  },
  wrapRatesRoom: {
    margin: "10px 16px",
  },
  divider: {
    height: 6,
    width: "100%",
    background: theme.palette.gray.grayLight22,
  },
  boxFotter: {
    zIndex: 1000,
    bottom: 0,
    left: 0,
    position: "fixed",
    color: "#000",
    background: "#fff",
    width: "100%",
    padding: "10px 16px",
    borderTop: "2px solid #EDF2F7",
  },
}));
const listTypeModal = {
  MODAL_ROOM_BANNER: "MODAL_ROOM_BANNER",
};
const RoomDetails = ({
  item = {},
  rateSelected = {},
  open = false,
  toggleDrawer = () => {},
  handleBookRoomRate = () => {},
}) => {
  const router = useRouter();
  const classes = useStyles();
  const handleBooking = () => {
    handleBookRoomRate();
  };
  const handleLogin = (event) => {
    event.preventDefault();
    router.push({
      pathname: routeStatic.LOGIN.href,
    });
  };
  return (
    <>
      <Drawer
        anchor="bottom"
        open={open}
        onClose={toggleDrawer("")}
        classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
      >
        <Box className={classes.wrapDrawer}>
          <BannerRoom
            item={item}
            open={listTypeModal.MODAL_ROOM_BANNER}
            toggleDrawerRoom={toggleDrawer}
          />
          <Box className={classes.infoRoomDetail}>
            <InfoRoom item={item} />
            <Box className={classes.wrapRatesRoom}>
              <ItemRatesRoom
                item={rateSelected}
                isShowCountDown
                isShowBtnBookingNow={false}
                isShowCode
                isShowDetailsGift
                isColumn={false}
              />
            </Box>
            <Divider className={classes.divider} />
            <Box className={classes.wrapPolicy}>
              <PolicyRoom rateSelected={rateSelected} />
            </Box>
            <ConvientRoom item={item} />
          </Box>
          <Box className={classes.boxFotter}>
            {rateSelected?.availableAllotment > 0 &&
              rateSelected?.availableAllotment <= 5 && (
                <Box
                  color="red.redLight5"
                  fontSize={14}
                  pb={10 / 8}
                  lineHeight="17px"
                >
                  {rateSelected?.availableAllotment === 1
                    ? "Phòng cuối cùng của chúng tôi!"
                    : `${listString.IDS_MT_TEXT_ONLY} ${rateSelected?.availableAllotment} phòng trống!`}
                </Box>
              )}
            {rateSelected.hiddenPrice ? (
              <ButtonComponent
                type="submit"
                backgroundColor="#FF1284"
                borderRadius={8}
                height={48}
                padding="16px 55px"
                fontSize={16}
                fontWeight={600}
                handleClick={handleLogin}
              >
                {listString.IDS_MT_TEXT_LOGIN}
              </ButtonComponent>
            ) : (
              <Box>
                <ButtonComponent
                  type="submit"
                  backgroundColor="#FF1284"
                  borderRadius={8}
                  height={48}
                  padding="16px 55px"
                  fontSize={16}
                  fontWeight={600}
                  handleClick={handleBooking}
                >
                  {listString.IDS_MT_TEXT_SELECT}
                </ButtonComponent>
              </Box>
            )}
          </Box>
        </Box>
      </Drawer>
    </>
  );
};
export default RoomDetails;
