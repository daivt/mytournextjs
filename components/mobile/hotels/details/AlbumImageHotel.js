import clsx from "clsx";
import { Box } from "@material-ui/core";
import { useRef, useState } from "react";
import { makeStyles } from "@material-ui/styles";

import utilStyles from "@styles/utilStyles";

import Image from "@src/image/Image";
import SlideShow from "@components/common/slideShow/SlideShow";

const useStyles = makeStyles((theme) => ({
  imageContent: {},
  imageSlick: {
    width: "100%",
    height: 220,
  },
  btnArrowStyle: {
    backgroundColor: "initial !important",
  },
  imageItem: {
    height: 80,
    width: 110,
  },
  listViewImage: {
    transition: "all 0.4s",
  },
}));

const settings = {
  dots: false,
  infinite: false,
  slidesToShow: 1,
  slidesToScroll: 1,
  // lazyLoad: true,
};

const AlbumImageHotel = ({ listImage = [] }) => {
  const slideEl = useRef(null);
  const slideEl1 = useRef(null);
  const classes = useStyles();
  const classesUtils = utilStyles();
  const [indexActive, setIndexActive] = useState(0);

  const handleActiveImage = (index) => {
    setIndexActive(index);
    if (slideEl.current) {
      slideEl.current.slickGoTo(index);
    }
  };
  return (
    <Box
      height="100%"
      display="flex"
      flexDirection="column"
      justifyContent="center"
      position="relative"
    >
      {listImage && listImage.length > 0 && (
        <SlideShow
          settingProps={{
            ...settings,
            beforeChange: (current, next) => {
              slideEl1.current.scrollLeft = (next - 1) * 110;
              setIndexActive(next);
            },
            ref: slideEl,
          }}
          className={classes.imageContent}
          btnArrowStyle={classes.btnArrowStyle}
        >
          {listImage.map((el, index) => (
            <Box key={index.toString()}>
              <Image
                srcImage={el}
                className={classes.imageSlick}
                alt={el.desc}
                title={el.desc}
              />
            </Box>
          ))}
        </SlideShow>
      )}

      <Box position="absolute" bottom={40} width="100%">
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          color="white.main"
          pb={2}
        >
          <Box
            width={90}
            borderBottom="2px solid #fff"
            textAlign="center"
            pb={5 / 8}
          >{`Tất cả (${listImage.length})`}</Box>
          <Box pr={1}>{`${indexActive + 1}/${listImage.length}`}</Box>
        </Box>
        <Box
          className={clsx(
            classesUtils.scrollViewHorizontal,
            classes.listViewImage
          )}
          ref={slideEl1}
        >
          {listImage.map((el, index) => (
            <Box
              key={index.toString()}
              mr={index === listImage.length - 1 ? 0 : "4px"}
              ml={index === 0 ? 0 : "4px"}
              border={indexActive === index ? "1px solid #fff" : ""}
              onClick={() => handleActiveImage(index)}
            >
              <Image
                srcImage={el}
                className={classes.imageItem}
                alt={el.desc}
                title={el.desc}
              />
            </Box>
          ))}
        </Box>
      </Box>
    </Box>
  );
};

export default AlbumImageHotel;
