import clsx from "clsx";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import { isEmpty } from "@utils/helpers";
import { listString } from "@utils/constants";

import ButtonComponent from "@src/button/Button";

const useStyles = makeStyles((theme) => ({
  boxFotter: {
    opacity: 0,
    transition: "all 0.4s ease-in-out",
  },
  sticky: {
    opacity: 1,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    zIndex: 1000,
    bottom: 0,
    left: 0,
    position: "fixed",
    transition: "all 0.4s ease-in-out",
    color: "#000",
    background: "#fff",
    width: "100%",
    padding: "8px 16px",
    borderTop: "2px solid #EDF2F7",
  },
  priceBefore: {
    textDecoration: "line-through",
  },
}));

const FooterSelectBestPrice = ({
  isShowSelectPrice = false,
  listRoom = [],
  handleScrollToRate = () => {},
}) => {
  const classes = useStyles();
  const rates = !isEmpty(listRoom) ? listRoom[0].rates : [];
  const rate = !isEmpty(rates) ? rates[0] : {};
  return (
    <Box
      className={clsx(classes.boxFotter, isShowSelectPrice && classes.sticky)}
    >
      <Box display="flex" flexDirection="column">
        {!isEmpty(listRoom) &&
          !isEmpty(listRoom[0].rates) &&
          !rate.hiddenPrice && (
            <>
              {!isEmpty(rate.basePromotionInfo) && (
                <Box
                  display="flex"
                  alignItems="center"
                  fontSize={12}
                  lineHeight="14px"
                  pb={2 / 8}
                >
                  <Box
                    color="gray.grayDark8"
                    pr={6 / 8}
                    className={classes.priceBefore}
                    component="span"
                  >{`${rate.basePromotionInfo.priceBeforePromotion.formatMoney()}đ /đêm`}</Box>
                  <Box
                    bgcolor="pink.main"
                    fontWeight={600}
                    p={2 / 8}
                    borderRadius={3}
                    color="white.main"
                    component="span"
                  >{`-${rate.basePromotionInfo.discountPercentage}%`}</Box>
                </Box>
              )}
              <Box
                fontSize={16}
                color="black.black3"
                fontWeight={600}
                lineHeight="19px"
                component="span"
              >
                {`${rate.basePrice.formatMoney()}đ`}
                <Box
                  color="gray.grayDark8"
                  component="span"
                  fontWeight="normal"
                  fontSize={14}
                >{` /đêm`}</Box>
              </Box>
            </>
          )}
      </Box>
      <ButtonComponent
        backgroundColor="#FF1284"
        borderRadius={8}
        width="fit-content"
        height={48}
        padding="16px 30px"
        fontSize={16}
        fontWeight={600}
        handleClick={handleScrollToRate}
      >
        {listString.IDS_MT_SELECT_ROOM}
      </ButtonComponent>
    </Box>
  );
};

export default FooterSelectBestPrice;
