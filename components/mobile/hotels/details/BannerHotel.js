import clsx from "clsx";
import cookie from "js-cookie";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import { Box, IconButton } from "@material-ui/core";

import { isEmpty } from "@utils/helpers";
import { addFavorite, removeFavorite } from "@api/user";
import { TOKEN, LAST_HOTEL_FAVORITES } from "utils/constants";

import Image from "@src/image/Image";
import snackbarSetting from "@src/alert/Alert";
import ViewAlbumImageHotelDetail from "@components/mobile/hotels/details/ViewAlbumImageHotelDetail";

import { IconBack, IconFavorite, IconImageHotel } from "@public/icons";

const useStyles = makeStyles((theme) => ({
  container: {
    position: "relative",
  },
  bannerSearchBox: {
    position: "relative",
    height: 252,
  },
  imageHotel: {
    width: "100%",
    height: "100%",
  },
  headerSearchBox: {
    position: "absolute",
    zIndex: 3,
    top: 0,
    left: 0,
    width: "100%",
    padding: 16,
    display: "flex",
    justifyContent: "space-between",
  },
  iconBack: {
    stroke: theme.palette.black.main,
  },
  btnIcon: {
    backgroundColor: `${theme.palette.white.main} !important`,
    width: 36,
    height: 36,
    padding: 0,
  },
  totalImageHotel: {
    position: "absolute",
    display: "flex",
    alignItems: "center",
    bottom: 28,
    right: 16,
    height: 30,
    borderRadius: 100,
    padding: 6,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    color: theme.palette.white.main,
    fontSize: 14,
    lineHeight: 17,
  },
  btnIconImageHotel: {},
  favorite: {
    fill: theme.palette.pink.main,
  },
  sticky: {
    zIndex: 1000,
    top: 0,
    left: 0,
    position: "fixed",
    transition: "all 0.1s ease-in-out",
    color: "#000",
  },
  stickyMenu: {
    background: "rgba(255, 255, 255, 0.4)",
  },
  stickyImage: {
    background: "#fff",
    boxShadow: "0 0 10px rgba(0,0,0,0.5)",
  },
  stickyDate: {
    background: "#fff",
  },
  nameHotel: {
    color: theme.palette.black.black3,
    fontWeight: 600,
    fontSize: 16,
    display: "inline-block",
    width: 190,
    whiteSpace: "nowrap",
    overflow: "hidden !important",
    textOverflow: "ellipsis",
    paddingLeft: 10,
    lineHeight: "22px",
  },
}));
const listTypeModal = {
  MODAL_ALBUM_IMAGE: "MODAL_ALBUM_IMAGE",
};
const BannerHotel = ({ item = {}, sticky = "", listPosition = {} }) => {
  const router = useRouter();
  const classes = useStyles();
  const [visibleDrawerType, setVisibleDrawerType] = useState("");
  const [isFavorite, setIsFavorite] = useState(item.isFavoriteHotel);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  useEffect(() => {
    checkFavoriteHotel();
  }, []);

  const checkFavoriteHotel = () => {
    const listHotelFavorites =
      JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
    if (isEmpty(cookie.get(TOKEN)) && !isEmpty(listHotelFavorites)) {
      setIsFavorite(listHotelFavorites.includes(item.id));
    }
  };
  const handleBackRoute = (event) => {
    event.preventDefault();
    router.back();
  };
  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };
  const handleFavorite = (event) => {
    event.preventDefault();
    if (!isEmpty(cookie.get(TOKEN))) {
      changeFavorite();
    } else {
      let listHotelFavoritesTemp =
        JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
      if (isFavorite) {
        listHotelFavoritesTemp = listHotelFavoritesTemp.filter(
          (el) => el !== item.id
        );
      } else {
        listHotelFavoritesTemp.unshift(item.id);
      }
      localStorage.setItem(
        LAST_HOTEL_FAVORITES,
        JSON.stringify(listHotelFavoritesTemp)
      );
      setIsFavorite(!isFavorite);
    }
  };
  const changeFavorite = async () => {
    try {
      if (isFavorite) {
        const { data } = await removeFavorite({ hotelId: item.id });
        if (data.code === 200) {
          setIsFavorite(!isFavorite);
        } else {
          data?.message &&
            enqueueSnackbar(
              data?.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
        }
      } else {
        const { data } = await addFavorite({ hotelId: item.id });
        if (data.code === 200) {
          setIsFavorite(!isFavorite);
        } else {
          data?.message &&
            enqueueSnackbar(
              data?.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
        }
      }
    } catch (error) {}
  };
  const adapterImage = (listImage = []) => {
    let result = [];
    listImage.forEach((el) => {
      result.push(el?.image?.small || el.src);
    });
    return result;
  };
  return (
    <Box className={classes.container}>
      <Box className={classes.bannerSearchBox}>
        <Box
          width="100%"
          height="100%"
          onClick={toggleDrawer(listTypeModal.MODAL_ALBUM_IMAGE)}
        >
          <Image
            srcImage={item?.thumbnail?.image?.small}
            className={classes.imageHotel}
            title={item?.name}
            alt={item?.name}
          />
        </Box>

        <Box
          className={clsx(
            classes.headerSearchBox,
            !isEmpty(sticky) && classes.sticky,
            sticky === listPosition.POSITION_MENU
              ? classes.stickyMenu
              : sticky === listPosition.POSITION_IMAGE
              ? classes.stickyImage
              : sticky === listPosition.POSITION_DATE_ROOM
              ? classes.stickyDate
              : ""
          )}
        >
          <Box display="flex" alignItems="center">
            <IconButton onClick={handleBackRoute} className={classes.btnIcon}>
              <IconBack className={`svgFillAll ${classes.iconBack}`} />
            </IconButton>
            {(sticky === listPosition.POSITION_IMAGE ||
              sticky === listPosition.POSITION_DATE_ROOM) && (
              <Box component="span" className={classes.nameHotel}>
                {item.name}
              </Box>
            )}
          </Box>
          <Box>
            <IconButton onClick={handleFavorite} className={classes.btnIcon}>
              <IconFavorite
                className={`svgFillAll ${
                  isFavorite ? classes.favorite : classes.iconBack
                }`}
              />
            </IconButton>
          </Box>
        </Box>
        {item?.images?.length > 1 && (
          <Box
            className={classes.totalImageHotel}
            onClick={() => toggleDrawer(listTypeModal.MODAL_ALBUM_IMAGE)}
          >
            <Box pr={3 / 8} component="span">
              {`+${item?.images?.length - 1}`}
            </Box>
            <IconImageHotel />
          </Box>
        )}
      </Box>
      <ViewAlbumImageHotelDetail
        open={visibleDrawerType === listTypeModal.MODAL_ALBUM_IMAGE}
        toggleDrawer={toggleDrawer}
        listImage={adapterImage(item.images)}
      />
    </Box>
  );
};

export default BannerHotel;
