import {
  Box,
  Tabs,
  IconButton,
  makeStyles,
  Drawer,
  Typography,
  Tab,
} from "@material-ui/core";
import { useState, useEffect } from "react";
import sanitizeHtml from "sanitize-html";
import { useTheme, withStyles } from "@material-ui/core/styles";

import { IconClose } from "@public/icons";
import { listString, optionClean } from "@utils/constants";
import { isEmpty, unEscape, adapterAmenitiesGroup } from "@utils/helpers";

import TabPanel from "@src/tabPanel/TabPanel";
import ButtonComponent from "@src/button/Button";

const useStyles = makeStyles((theme) => ({
  wrapButtonClose: {
    padding: 0,
  },
  wraperHeader: {
    padding: "12px 16px",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    height: 48,
    display: "flex",
    alignItems: "center",
  },
  wrapHeaderDraw: {
    display: "flex",
    alignItems: "center",
    height: 48,
    borderBottom: "1px solid #EDF2F7",
    width: "100%",
  },
  textFilter: { textAlign: "center", width: "100%" },
  paperAnchorBottom: {
    height: "95%",
    borderRadius: "16px 16px 0 0",
  },
  tabItem: {
    color: theme.palette.black.black3,
    fontSize: 16,
    textTransform: "none",
    minHeight: 40,
    paddingTop: 20,
    paddingLeft: 0,
    fontWeight: 600,
    "&:selected": {
      color: theme.palette.blue.blueLight8,
      fontSize: 16,
      lineHeight: "19px",
    },
    "&:active": {
      color: theme.palette.blue.blueLight8,
      fontSize: 16,
      lineHeight: "19px",
    },
    "&:focus": {
      color: theme.palette.blue.blueLight8,
      fontSize: 16,
      lineHeight: "19px",
    },
    "& span": {
      flexDirection: "row",
      "& svg": {
        marginRight: 6,
      },
    },
    "&.Mui-selected": {
      color: theme.palette.blue.blueLight8,
    },
  },
  wrapperTab: {
    position: "relative",
    padding: "24px 16px 13px",
    color: theme.palette.black.black3,
    fontSize: 14,
    overflowY: "auto",
    height: "calc(100% - 58px)",
  },
  iconConvient: {
    width: 24,
    height: 24,
  },
  boxSectionSearch: {
    height: "calc(100% - 106px)",
  },
  wrapCheckIn: {
    background: theme.palette.gray.grayLight22,
    color: theme.palette.black.black3,
    display: "flex",
    justifyContent: "space-around",
    borderRadius: 8,
    marginBottom: 12,
  },
  textCheckIn: {
    fontWeight: "normal",
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark8,
    marginTop: 8,
    textAlign: "center",
  },
  dividerTime: {
    width: 1,
    height: 28,
    background: theme.palette.gray.grayLight23,
    margin: "12px 0",
  },
  timeCheckIn: {
    marginBottom: 8,
    lineHeight: "17px",
  },
  itemPolices: {
    "& p": {
      margin: 0,
    },
  },
}));

export const TabsInient = withStyles((theme) => ({
  indicator: {
    background: theme.palette.blue.blueLight8,
  },
}))(Tabs);

const ConvenientPopup = ({
  open = false,
  toggleDrawer = () => {},
  handleScrollToRate = () => {},
  hotelDetail = {},
  initialTab = 0,
}) => {
  const theme = useTheme();
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState(initialTab);
  useEffect(() => {
    setCurrentTab(initialTab);
  }, [initialTab]);
  const handleChangeTab = (event, newValue) => {
    setCurrentTab(newValue);
  };
  return (
    <>
      <Drawer
        anchor="bottom"
        open={open}
        onClose={toggleDrawer("")}
        classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
      >
        <Box className={classes.wraperHeader}>
          <Box className={classes.wrapHeaderDraw}>
            <IconButton
              onClick={toggleDrawer("")}
              className={classes.wrapButtonClose}
            >
              <IconClose />
            </IconButton>
            <Typography variant="subtitle1" className={classes.textFilter}>
              {listString.IDS_MT_TEXT_INFO_HOTEL}
            </Typography>
          </Box>
        </Box>
        <div className={classes.boxSectionSearch}>
          <TabsInient
            value={currentTab}
            onChange={handleChangeTab}
            classes={{
              indicator: classes.indicator,
            }}
            aria-label="full width tabs example"
            variant="fullWidth"
          >
            <Tab
              label={listString.IDS_MT_TEXT_FILTER_CONVENIENT}
              className={classes.tabItem}
            />

            <Tab
              label={listString.IDS_MT_TEXT_POLICY}
              className={classes.tabItem}
            />
            <Tab
              label={listString.IDS_MT_TEXT_DESCRIPTION}
              className={classes.tabItem}
            />
          </TabsInient>
          <div className={classes.wrapperTab}>
            <TabPanel value={currentTab} index={0}>
              {adapterAmenitiesGroup(hotelDetail.groupAmenities).map(
                (el, index) => (
                  <Box
                    display="flex"
                    alignItems="center"
                    key={index.toString()}
                    pb={2}
                  >
                    <img src={el.icon} className={`${classes.iconConvient}`} />
                    <Box component="span" pl={14 / 8}>
                      {el?.name}
                    </Box>
                  </Box>
                )
              )}
            </TabPanel>
            <TabPanel value={currentTab} index={1}>
              <Box className={classes.wrapCheckIn}>
                <Box className={classes.textCheckIn}>
                  {listString.IDS_MT_TEXT_CHECK_IN_ROOM}
                  <Typography
                    variant="subtitle2"
                    className={classes.timeCheckIn}
                  >
                    {`Từ ${hotelDetail?.checkInTime}`}
                  </Typography>
                </Box>
                <Box className={classes.dividerTime} />
                <Box className={classes.textCheckIn}>
                  {listString.IDS_MT_TEXT_CHECK_OUT_ROOM}
                  <Typography
                    variant="subtitle2"
                    className={classes.timeCheckIn}
                  >
                    {`Trước ${hotelDetail?.checkOutTime}`}
                  </Typography>
                </Box>
              </Box>
              {!isEmpty(hotelDetail?.policies) &&
                hotelDetail.policies.map((el, index) => (
                  <Box key={index}>
                    <Box>
                      <Typography variant="subtitle2">{el.title}</Typography>
                      <Box
                        fontSize={14}
                        color="gray.grayDark1"
                        lineHeight="19px"
                        mt={1}
                        className={classes.itemPolices}
                        dangerouslySetInnerHTML={{
                          __html: sanitizeHtml(
                            unEscape(unescape(el.description || "")),
                            optionClean
                          ),
                        }}
                      />
                    </Box>
                  </Box>
                ))}
            </TabPanel>
            <TabPanel value={currentTab} index={2}>
              <Box
                fontSize={14}
                color="gray.grayDark1"
                overflow="hidden"
                lineHeight="22px"
                dangerouslySetInnerHTML={{
                  __html: sanitizeHtml(
                    unEscape(unescape(hotelDetail?.descriptions || "")),
                    optionClean
                  ),
                }}
              />
            </TabPanel>
          </div>
          <Box px={2} position="fixed" bottom={8} width="100%">
            <ButtonComponent
              backgroundColor={theme.palette.secondary.main}
              height={48}
              fontSize={16}
              fontWeight={600}
              borderRadius={8}
              handleClick={handleScrollToRate}
            >
              {listString.IDS_MT_SELECT_ROOM}
            </ButtonComponent>
          </Box>
        </div>
      </Drawer>
    </>
  );
};
ConvenientPopup.propTypes = {};
export default ConvenientPopup;
