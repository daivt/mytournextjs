import { Box, IconButton, makeStyles, Drawer } from "@material-ui/core";

import { IconClose } from "@public/icons";

import AlbumImageHotel from "@components/mobile/hotels/details/AlbumImageHotel";
import { isEmpty } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  paperAnchorBottom: {
    height: "100vh",
    width: "100vw",
    borderRadius: 0,
    backgroundColor: theme.palette.black.main,
  },
  iconClose: {
    stroke: theme.palette.white.main,
  },
  headerBox: {
    zIndex: 2,
  },
}));

const ViewAlbumImageHotelDetail = ({
  open = true,
  toggleDrawer = () => {},
  listImage = [],
}) => {
  const classes = useStyles();
  return (
    <>
      <Drawer
        anchor="bottom"
        open={open}
        onClose={toggleDrawer("")}
        classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
      >
        <Box
          display="flex"
          justifyContent="flex-end"
          position="absolute"
          width="100%"
          className={classes.headerBox}
        >
          <IconButton onClick={toggleDrawer("")}>
            <IconClose className={`svgFillAll ${classes.iconClose}`} />
          </IconButton>
        </Box>
        {!isEmpty(listImage) && <AlbumImageHotel listImage={listImage} />}
      </Drawer>
    </>
  );
};
export default ViewAlbumImageHotelDetail;
