import PropTypes from "prop-types";
import { Box, makeStyles, Typography } from "@material-ui/core";

import { listString } from "@utils/constants";
import Image from "@src/image/Image";

const initItem = {};

const useStyles = makeStyles((theme) => ({
  warpContainer: {
    color: theme.palette.black.black3,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  noReviewHotel: {
    width: 201,
    height: 124,
  },
}));

const NoReviewHotelDetails = ({}) => {
  const classes = useStyles();
  return (
    <>
      <Typography
        variant="subtitle1"
        style={{ fontSize: 18, margin: "16px 0px 12px 16px" }}
      >
        {listString.IDS_MT_TEXT_EVALUATE}
      </Typography>
      <Box className={classes.warpContainer}>
        <Image
          srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_no_review_hotel.svg"
          className={classes.noReviewHotel}
        />
        <Typography variant="body1" style={{ marginTop: "8px" }}>
          {listString.IDS_MT_TEXT_HOTEL_NO_REVIEW}
        </Typography>
      </Box>
    </>
  );
};
NoReviewHotelDetails.propTypes = {};
export default NoReviewHotelDetails;
