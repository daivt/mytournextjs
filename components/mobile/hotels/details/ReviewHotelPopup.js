import {
  Box,
  IconButton,
  makeStyles,
  Drawer,
  Typography,
  withStyles,
  Tab,
  Tabs,
  Link,
} from "@material-ui/core";
import { useState, useEffect } from "react";
import { useTheme } from "@material-ui/core/styles";

import { isEmpty } from "@utils/helpers";
import utilStyles from "@styles/utilStyles";
import { listString } from "@utils/constants";
import TabPanel from "@src/tabPanel/TabPanel";
import {
  IconClose,
  IconArrowDownToggle,
  IconExternalLink,
} from "@public/icons";

import { getHotelReviews } from "@api/hotels";
import ButtonComponent from "@src/button/Button";
import SortReviewHotel from "@components/mobile/hotels/details/SortReviewHotel";
import EvaluateHotelDetails from "@components/mobile/hotels/details/EvaluateHotelDetails";
import ReviewCommentHotel from "@components/mobile/hotels/details/ReviewCommentHotel";

import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  wrapButtonClose: {
    padding: 0,
  },
  wraperHeader: {
    padding: "12px 16px",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    height: 48,
    display: "flex",
    alignItems: "center",
  },
  wrapHeaderDraw: {
    display: "flex",
    alignItems: "center",
    height: 48,
    borderBottom: "1px solid #EDF2F7",
    width: "100%",
  },
  textFilter: { textAlign: "center", width: "100%" },
  paperAnchorBottom: {
    minHeight: "55vh",
    maxHeight: "95%",
    borderRadius: "16px 16px 0 0",
  },
  tabItem: {
    color: theme.palette.black.black3,
    fontSize: 16,
    textTransform: "none",
    minHeight: 40,
    paddingTop: 20,
    paddingLeft: 0,
    fontWeight: 600,
    "&:selected": {
      color: theme.palette.blue.blueLight8,
      fontSize: 16,
      lineHeight: "19px",
    },
    "&:active": {
      color: theme.palette.blue.blueLight8,
      fontSize: 16,
      lineHeight: "19px",
    },
    "&:focus": {
      color: theme.palette.blue.blueLight8,
      fontSize: 16,
      lineHeight: "19px",
    },
    "& span": {
      flexDirection: "row",
      "& svg": {
        marginRight: 6,
      },
    },
  },
  wrapperTab: {
    position: "relative",
  },
  styleWraper: {
    margin: "12px 16px",
  },
  wrapSort: {
    color: theme.palette.black.black3,
    background: theme.palette.gray.grayLight22,
    display: "flex",
    alignItems: "center",
    height: 40,
    borderRadius: 8,
    justifyContent: "space-between",
  },
  sortText: {
    fontSize: 14,
    lineHeight: "17px",
    marginLeft: 12,
  },
  sort: {
    marginRight: 12,
  },
  wrapRatingTravel: {
    display: "flex",
    marginTop: 10,
  },
  ratingTravel: {
    background: theme.palette.gray.grayLight22,
    color: theme.palette.black.black3,
    borderRadius: 100,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    fontSize: 14,
    height: 31,
    marginBottom: 24,
    marginRight: 8,
  },
  ratingTravelActive: {
    background: theme.palette.gray.grayDark7,
    color: theme.palette.white.main,
  },
  textRating: {
    padding: "7px 12px",
    whiteSpace: "nowrap",
  },
  ratingCount: {
    color: theme.palette.gray.grayDark7,
  },
  btnSelectRoom: {
    position: "fixed",
    backgroundColor: theme.palette.white.main,
    bottom: 0,
    width: "100%",
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  btnBooking: {
    margin: "8px 16px",
    width: "92%",
  },
  wrapSortContent: {
    height: "73vh",
    overflowY: "auto",
    paddingBottom: 48,
    "&::-webkit-scrollbar": {
      display: "none",
    },
  },
}));
export const TabsInient = withStyles((theme) => ({
  indicator: {
    background: theme.palette.blue.blueLight8,
  },
}))(Tabs);
const listTypeModal = {
  MODAL_SORT_REVIEW: "MODAL_SORT_REVIEW",
};
const pageSize = 50;
const ReviewHotelPopup = ({
  open = true,
  toggleDrawer = () => () => {},
  handleScrollToRate = () => {},
  styleWraper,
  reviewsInit = {},
  reviewsInitTripadvisor = {},
  hotelRating = {},
  hotelRatingTripadvisor = {},
  hotelId = null,
}) => {
  const theme = useTheme();
  const classesUtils = utilStyles();
  const propStyle = {
    styleWraper,
  };
  const classes = useStyles(propStyle);
  const [modalReview, setModalReview] = useState("");
  const [reviews, setReviews] = useState(reviewsInit.items || []);
  const [sortBy, setSortBy] = useState("");
  const [filterRating, setfilterRating] = useState("");
  const [currentTab, setCurrentTab] = useState(0);
  const handleChangeTab = (event, newValue) => {
    setCurrentTab(newValue);
  };

  const toggleDrawerReview = (openModal = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setModalReview(openModal);
  };

  const handleSelectedItemSort = (sort) => (e) => {
    setSortBy(sort);
    setModalReview("");
    e.stopPropagation();
  };

  useEffect(() => {
    setReviews(reviewsInit.items || []);
    if (!isEmpty(reviewsInit)) {
      setSortBy(reviewsInit.sortOptions[0].code);
    }
  }, [reviewsInit]);

  useEffect(() => {
    let dataDTO = {
      page: 1,
      size: pageSize,
      hotelId,
      sortBy,
    };
    if (!isEmpty(filterRating)) {
      dataDTO = {
        ...dataDTO,
        filters: {
          travelTypes: [filterRating],
        },
      };
    }
    fetHotelReview(dataDTO);
  }, [sortBy, filterRating]);

  const fetHotelReview = async (dataDTO) => {
    try {
      const { data } = await getHotelReviews(dataDTO);
      if (data.code === 200) {
        setReviews(data.data.items);
      }
    } catch (error) {}
  };

  const handelFilterRating = (value) => {
    setfilterRating(value);
  };
  const getTextDetailSort = () => {
    let textString = "";
    if (!isEmpty(reviewsInit)) {
      const sortSelect = reviewsInit.sortOptions.find(
        (el) => el.code === sortBy
      );
      if (!isEmpty(sortSelect)) {
        textString = sortSelect.textDetail;
      }
    }
    return textString;
  };

  return (
    <>
      <Drawer
        anchor="bottom"
        open={open}
        onClose={toggleDrawer("")}
        classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
      >
        <Box className={classes.wraperHeader}>
          <Box className={classes.wrapHeaderDraw}>
            <IconButton
              onClick={toggleDrawer("")}
              className={classes.wrapButtonClose}
            >
              <IconClose />
            </IconButton>
            <Typography variant="subtitle1" className={classes.textFilter}>
              {listString.IDS_MT_TEXT_LOAD_REVIEW_HOTEL}
            </Typography>
          </Box>
        </Box>
        <TabsInient
          value={currentTab}
          onChange={handleChangeTab}
          classes={{
            indicator: classes.indicator,
          }}
          aria-label="full width tabs example"
          variant="fullWidth"
        >
          <Tab label="Mytour" className={classes.tabItem} />
          <Tab label="Tripadvisor" className={classes.tabItem} />
        </TabsInient>
        <div className={classes.wrapperTab}>
          <TabPanel value={currentTab} index={0}>
            <Box px={2}>
              <Box className={classes.wrapSortContent}>
                <Box className={classes.wrapperTab}>
                  <Box
                    className={clsx(
                      classes.wrapRatingTravel,
                      classesUtils.scrollViewHorizontal
                    )}
                  >
                    {!isEmpty(reviewsInit) &&
                      reviewsInit?.countByTravelType.map((el, index) => (
                        <Box
                          className={
                            filterRating === el.value
                              ? clsx(
                                  classes.ratingTravel,
                                  classes.ratingTravelActive
                                )
                              : classes.ratingTravel
                          }
                          key={index.toString()}
                          onClick={() => handelFilterRating(el.value)}
                        >
                          <Typography className={classes.textRating}>
                            {el?.name}{" "}
                            <Typography
                              component="span"
                              className={
                                filterRating === el.value
                                  ? classes.ratingTravelActive
                                  : classes.ratingCount
                              }
                            >
                              ({el.count})
                            </Typography>
                          </Typography>
                        </Box>
                      ))}
                  </Box>

                  <EvaluateHotelDetails item={hotelRating} />
                  <Box
                    className={classes.wrapSort}
                    onClick={toggleDrawerReview(
                      listTypeModal.MODAL_SORT_REVIEW
                    )}
                  >
                    <Typography className={classes.sortText}>
                      {listString.IDS_MT_TEXT_SORT}:{" "}
                      <Typography
                        component="span"
                        style={{ fontSize: "14px", fontWeight: "600" }}
                      >
                        {" "}
                        {getTextDetailSort()}
                      </Typography>
                    </Typography>
                    <IconArrowDownToggle className={classes.sort} />
                  </Box>
                </Box>
                <SortReviewHotel
                  openModal={modalReview === listTypeModal.MODAL_SORT_REVIEW}
                  toggleDrawerReview={toggleDrawerReview}
                  handleSelectedItem={handleSelectedItemSort}
                  sortBy={sortBy}
                  sortOptions={reviewsInit.sortOptions || []}
                />

                {reviews.map((el, index) => {
                  return (
                    <ReviewCommentHotel key={index.toString()} item={el} />
                  );
                })}
              </Box>
            </Box>
          </TabPanel>
          <TabPanel value={currentTab} index={1}>
            <Box px={2}>
              <Box className={classes.wrapSortContent}>
                <EvaluateHotelDetails item={hotelRatingTripadvisor} />
                {reviewsInitTripadvisor?.items?.map((el, index) => {
                  return (
                    <ReviewCommentHotel key={index.toString()} item={el} />
                  );
                })}
              </Box>
            </Box>
            <Link
              href={reviewsInitTripadvisor.reviewUrl}
              target="_blank"
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                width: "100%",
                fontSize: 14,
                lineHeight: "17px",
                color: "#00B6F3",
                marginBottom: 5,
              }}
            >
              {listString.IDS_MT_VIEW_MORE_REVIEW_TRIPADVISOR}&nbsp;
              <IconExternalLink />
            </Link>
          </TabPanel>
          <Box className={classes.btnSelectRoom}>
            <ButtonComponent
              backgroundColor={theme.palette.secondary.main}
              height={48}
              fontSize={16}
              fontWeight={600}
              borderRadius={8}
              className={classes.btnBooking}
              handleClick={handleScrollToRate}
            >
              {listString.IDS_MT_SELECT_ROOM}
            </ButtonComponent>
          </Box>
        </div>
      </Drawer>
    </>
  );
};
export default ReviewHotelPopup;
