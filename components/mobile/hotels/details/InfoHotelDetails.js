import MapDetailDialog from "@components/common/map/MapDetailDialog";
import { Box, Typography } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import {
  IconArrowDownToggle,
  IconLocationBorder,
  IconMap,
  IconStar,
} from "@public/icons";
import ButtonComponent from "@src/button/Button";
import Image from "@src/image/Image";
import { listIcons, listString, prefixUrlIcon } from "@utils/constants";
import { getMessageHotelReviews, isEmpty } from "@utils/helpers";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  wrapContainer: {
    margin: "20px 16px",
    color: theme.palette.black.black3,
    fontSize: 14,
  },
  rating: { marginTop: 6, color: theme.palette.yellow.yellowLight3 },
  point: {
    lineHeight: "17px",
    color: theme.palette.white.main,
    background: theme.palette.blue.blueLight8,
    width: 30,
    padding: "2px 4px",
    borderRadius: 4,
    textAlign: "center",
  },
  countEvaluate: {
    display: "flex",
    alignItems: "center",
    marginTop: 6,
    justifyContent: "space-between",
    marginBottom: 9,
  },
  messageReview: {
    lineHeight: "17px",
    fontWeight: "normal",
    marginLeft: 4,
  },
  countReview: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark7,
    marginLeft: 2,
  },
  viewMessageReview: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.blue.blueLight8,
    display: "flex",
    alignItems: "center",
    whiteSpace: "nowrap",
  },
  iconArrowDownToggle: {
    stroke: theme.palette.blue.blueLight8,
    transform: "rotate(270deg)",
    marginLeft: 2,
  },
  wrapLocation: {
    minHeight: 46,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    position: "relative",
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    padding: "10px 0 0 0",
  },
  address: {
    lineHeight: "17px",
    fontWeight: "normal",
    width: "100%",
    display: "-webkit-box",
    WebkitLineClamp: 2,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
  },
  wrapAddress: {
    display: "flex",
    alignItems: "center",
    width: "80%",
  },
  wrapMap: {
    display: "flex",
    flexDirection: "column",
    position: "absolute",
    right: 0,
    top: 9,
    alignItems: "center",
    color: theme.palette.blue.blueLight8,
  },
  priceTag: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.green.greenLight7,
    marginLeft: 12,
    padding: "9px 0",
  },
  warPriceTag: {
    marginTop: 10,
    display: "flex",
    alignItems: "center",
    background: theme.palette.green.greenLight8,
    borderRadius: 8,
    color: theme.palette.green.greenLight7,
  },
  wrapRatingStar: {
    display: "flex",
    marginTop: 6,
  },
  rankingHotel: {
    stroke: theme.palette.yellow.yellowLight3,
    fill: theme.palette.yellow.yellowLight3,
    width: 16,
    height: 16,
    marginRight: 3,
  },
  nameHotel: {
    display: "-webkit-box",
    WebkitLineClamp: 3,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
  },
  tagItem: {
    borderRadius: 4,
    padding: "2px 4px",
    color: theme.palette.white.main,
    fontSize: 11,
    fontWeight: 600,
    lineHeight: "13px",
    marginRight: 6,
  },
  wrapTypeHotel: {
    background: "#FF1284",
    color: theme.palette.white.main,
    display: "flex",
    alignItems: "center",
    fontSize: 11,
    fontWeight: 400,
    lineHeight: "13px",
    borderRadius: 4,
    width: "fit-content",
    borderBottomRightRadius: 0,
  },
  clipPath: {
    width: 15,
    height: 20,
    clipPath: "polygon(105% 0, 48% 0, 100% 100%, 102% 100%)",
    background: "#ffdced",
  },
  villa: {
    height: 20,
    fontSize: 12,
    lineHeight: "14px",
    fontWeight: 600,
    color: "#FF1284",
    padding: "3px 6px",
    marginLeft: 0,
    background: "rgba(255, 18, 132, 0.15)",
    display: "flex",
    alignItems: "center",
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    whiteSpace: "nowrap",
  },
  iconMPlus: {
    width: 63,
    height: 20,
  },
  iconTripadvisor: {
    width: 18,
    height: 18,
  },
}));
const arrStar = [1, 2, 3, 4, 5];

const InfoHotelDetails = ({
  item = {},
  handleViewReview = () => {},
  handleScrollToRate = () => {},
  listInterestingPlaces = [],
  isShowComparePrice = false,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const coordinate = !isEmpty(item.address) ? item.address.coordinate : {};
  const tagItem = !isEmpty(item.tags) ? item.tags[0] : "";

  return (
    <Box className={classes.wrapContainer}>
      <Box pb={6 / 8} display="flex" alignItems="center">
        {!isEmpty(tagItem) && (
          <Box
            className={classes.tagItem}
            bgcolor="yellow.yellowLight3"
            component="span"
          >
            {tagItem.name}
          </Box>
        )}
        {!isEmpty(item.lastBookedTime) &&
          moment().diff(moment(item.lastBookedTime)) <= 7200000 && (
            <Box
              className={classes.tagItem}
              bgcolor="red.redLight5"
              component="span"
            >
              {listString.IDS_MT_TEXT_SALING_TOP}
            </Box>
          )}
      </Box>
      <Typography
        variant="subtitle1"
        className={classes.nameHotel}
        style={{ lineHeight: "24px", fontSize: "18px", fontWeight: "600" }}
      >
        {item?.name}
      </Typography>

      {!isEmpty(item.category) && (
        <Box display="flex" alignItems="center">
          <Box className={classes.wrapRatingStar}>
            {arrStar.map((el, index) => {
              if (el > item.starNumber) return null;
              return (
                <IconStar
                  key={index}
                  className={`svgFillAll ${classes.rankingHotel}`}
                />
              );
            })}
          </Box>
          <Box
            display="flex"
            alignItems="center"
            style={{ marginLeft: 12, marginTop: 6 }}
          >
            {item.category.code === "villa" && (
              <>
                <Box className={classes.wrapTypeHotel}>
                  <Box
                    display="flex"
                    alignItems="flex-end"
                    style={{ marginLeft: 6 }}
                  >
                    <Image
                      srcImage={`${prefixUrlIcon}${listIcons.IconMPlus}`}
                      className={classes.iconMPlus}
                    />
                    <Box className={classes.clipPath}></Box>
                  </Box>
                </Box>
                <Box className={classes.villa}> Villa, Căn hộ</Box>
              </>
            )}
          </Box>
        </Box>
      )}

      <Box className={classes.countEvaluate}>
        <Box style={{ display: "flex", alignItems: "center" }}>
          <Typography variant="subtitle2" className={classes.point}>
            {2 * item?.rating?.hotel?.rating === 10
              ? 10
              : (2 * item?.rating?.hotel?.rating).toFixed(1)}
          </Typography>
          <Typography component="span" className={classes.messageReview}>
            {getMessageHotelReviews(item?.rating?.hotel.rating)}
          </Typography>
          {item?.rating?.hotel.count > 0 && (
            <Typography component="span" className={classes.countReview}>
              ({item?.rating?.hotel.count} {""}đánh giá)
            </Typography>
          )}
          {!isEmpty(item?.rating?.tripadvisor?.rating) && (
            <Box component="span" display="flex" alignItems="center" pl={4 / 8}>
              <Image
                srcImage={`${prefixUrlIcon}${listIcons.IconTripadvisor}`}
                className={classes.iconTripadvisor}
              />
              <Box component="span" pl={4 / 8} fontSize={14}>
                {2 * item?.rating?.tripadvisor?.rating === 10
                  ? 10
                  : (2 * item?.rating?.tripadvisor?.rating).toFixed(1)}
              </Box>
            </Box>
          )}
        </Box>
        <ButtonComponent
          typeButton="text"
          color={theme.palette.blue.blueLight8}
          width="fit-content"
          padding="6px 0"
          handleClick={handleViewReview}
        >
          <Typography className={classes.viewMessageReview}>
            {listString.IDS_MT_TEXT_SEE_REVIEW}
            <IconArrowDownToggle
              className={`svgFillAll ${classes.iconArrowDownToggle}`}
            />
          </Typography>
        </ButtonComponent>
      </Box>

      <Box className={classes.wrapLocation}>
        <Box className={classes.wrapAddress}>
          <IconLocationBorder />
          <Box component="span" pl={1} className={classes.address}>
            {item?.address?.address}
          </Box>
        </Box>
        <MapDetailDialog
          child={
            <Box className={classes.wrapMap}>
              <IconMap />
              <Typography variant="body2">
                {listString.IDS_MT_TEXT_MAP}
              </Typography>
            </Box>
          }
          listInterestingPlaces={listInterestingPlaces}
          handleScrollToRate={handleScrollToRate}
          coordinate={coordinate}
        />
      </Box>
      {isShowComparePrice && (
        <Box className={classes.warPriceTag}>
          <Box style={{ width: "36px", height: "36px", marginLeft: "12px" }}>
            <img
              src={`${prefixUrlIcon}${listIcons.IconPriceTag2}`}
              alt="icon_price_tag"
              style={{ width: 36, height: 36 }}
            />
          </Box>
          <Typography className={classes.priceTag}>
            {listString.IDS_MT_TEXT_COMPARE_PRICE_THREE_SOURCE}
          </Typography>
        </Box>
      )}
    </Box>
  );
};
export default InfoHotelDetails;
