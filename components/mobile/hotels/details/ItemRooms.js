import { memo } from "react";
import { Box, makeStyles, Typography } from "@material-ui/core";

import {
  IconUser2,
  IconAcreage,
  IconView,
  IconArrowDownToggle,
  IconBed,
} from "@public/icons";
import { prefixUrlIcon, listIcons } from "@utils/constants";
import { isEmpty, areEqualItemRoom } from "@utils/helpers";

import Image from "@src/image/Image";
import SlideShow from "@components/common/slideShow/SlideShow";
import ItemRatesRoom from "@components/mobile/hotels/details/ItemRatesRoom";
import RoomDetails from "@components/mobile/hotels/details/roomDetails/RoomDetails";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      background: theme.palette.white.main,
      width: "50%",
      borderColor: theme.palette.gray.grayLight25,
      borderRadius: 8,
    },
    "& .MuiOutlinedInput-root": {
      height: 48,
      borderRadius: 8,
    },
    "& .MuiSelect-select:focus": {
      background: "none",
      outline: "none",
    },
    "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
      borderColor: theme.palette.gray.grayLight25,
    },
  },
  container: {
    margin: "16px 16px 0 16px",
  },
  imageSlick: {
    width: "100%",
    height: 154,
    borderRadius: 8,
  },
  slickDot: {
    bottom: "10px !important",
    "& li": {
      height: 2,
      width: 16,
      margin: "0 2px",
      "& div": {
        height: 2,
        width: 16,
        borderRadius: 100,
        backgroundColor: theme.palette.gray.grayLight22,
        border: "none",
      },
    },
    "&.slick-active": {
      "& div": {
        backgroundColor: theme.palette.white.main,
      },
    },
  },
  iconRecommendRoom: { position: "absolute", top: 12, left: -6, zIndex: 2 },
  wrapContent: {
    margin: "10px 0",
  },
  wrapRoomName: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    color: theme.palette.black.black3,
    paddingRight: 10,
  },
  infoRoom: {
    display: "flex",
    alignItems: "center",
    marginTop: 8,
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark8,
  },
  IconUser2: {
    width: 12,
    height: 12,
  },
  wrapBed: {
    marginTop: 8,
    display: "flex",
    alignItems: "center",
  },
  textBed: {
    marginLeft: 6,
    fontWeight: "normal",
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark8,
  },
  viewRoom: {
    display: "inline-block",
    width: 130,
    whiteSpace: "nowrap",
    overflow: "hidden !important",
    textOverflow: "ellipsis",
  },
  imageEmpty: {
    width: 343,
    height: 128,
  },
  iconRecommend: {
    width: 111,
    height: 24,
  },
}));

const settings = {
  dots: true,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  lazyLoad: true,
};
const listTypeModal = {
  MODAL_ROOM_DETAILS: "MODAL_ROOM_DETAILS",
};

const ItemRooms = ({
  item = {},
  roomRecommend = false,
  hotelDetail = {},
  handleBookRoomRate = () => {},
}) => {
  const classes = useStyles();
  const [visibleDrawerType, setVisibleDrawerType] = React.useState("");
  const [rateSelected, setRateSelected] = React.useState({});
  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
    event.stopPropagation();
  };

  const getListBed = (bedGroups = []) => {
    let listBed = "";
    bedGroups.forEach((el, index) => {
      listBed = `${listBed}${index !== 0 ? " hoặc " : ""}${el.bedInfo}`;
    });
    return listBed;
  };
  const handleViewRoomRate = (itemRate) => {
    setRateSelected(itemRate);
    setVisibleDrawerType(listTypeModal.MODAL_ROOM_DETAILS);
  };
  const handleViewRoom = () => {
    setRateSelected(item?.rates[0]);
    setVisibleDrawerType(listTypeModal.MODAL_ROOM_DETAILS);
  };
  const getView = (view = []) => {
    let viewString = "";
    view.forEach((el, idx) => {
      viewString = `${viewString}${idx !== 0 ? ", " : ""}${el}`;
    });
    return viewString;
  };

  return (
    <Box className={classes.container}>
      <Box onClick={handleViewRoom}>
        <Box position="relative" width="100%">
          {roomRecommend && (
            <Box className={classes.iconRecommendRoom}>
              <Image
                srcImage={`${prefixUrlIcon}${listIcons.IconRecommendRoom}`}
                className={classes.iconRecommend}
              />
            </Box>
          )}
          {!isEmpty(item.images) ? (
            <SlideShow settingProps={settings} slickDotStyle={classes.slickDot}>
              {item.images.map((el, index) => {
                if (index > 8) return null;
                return (
                  <Box key={index.toString()}>
                    <Image
                      srcImage={el}
                      className={classes.imageSlick}
                      alt={item.name}
                      title={item.name}
                      borderRadiusProp="8px"
                    />
                  </Box>
                );
              })}
            </SlideShow>
          ) : (
            <Image
              srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_room_default.svg"
              className={classes.imageEmpty}
            />
          )}
        </Box>

        <Box className={classes.wrapContent}>
          <Box className={classes.wrapRoomName}>
            <Typography variant="subtitle1">{item?.name}</Typography>
            {item?.rates.length < 2 && (
              <IconArrowDownToggle
                style={{
                  transform: "rotate(271deg)",
                }}
              />
            )}
          </Box>

          <Box className={classes.infoRoom}>
            {!isEmpty(item.standardNumberOfGuests) && (
              <>
                <IconUser2 className={classes.IconUser2} />
                <Box marginLeft={1}>{item?.standardNumberOfGuests} người</Box>
              </>
            )}
            {!isEmpty(item.roomArea) && (
              <>
                <IconAcreage style={{ marginLeft: "20px" }} />
                <Box marginLeft={1}>{item?.roomArea}m2 </Box>
              </>
            )}

            {!isEmpty(item.views) && (
              <>
                <IconView style={{ marginLeft: "20px" }} />
                <Box marginLeft={1} className={classes.viewRoom}>
                  {getView(item?.views)}{" "}
                </Box>
              </>
            )}
          </Box>
          {!isEmpty(item.bedGroups) && (
            <>
              <Box className={classes.wrapBed}>
                <Box style={{ width: 16, height: 16 }}>
                  <IconBed />
                </Box>
                <Typography className={classes.textBed}>
                  {getListBed(item.bedGroups)}
                </Typography>
              </Box>
            </>
          )}
        </Box>
      </Box>

      <Box>
        {item?.rates.map((el, index) => (
          <ItemRatesRoom
            item={el}
            key={index.toString()}
            key={index.toString()}
            index={index}
            isShowOption={item.rates.length > 1}
            handleViewRoomRate={handleViewRoomRate}
            handleBookRoomRate={handleBookRoomRate({
              roomKey: item.roomKey,
              rateKey: el.rateKey,
              rate: el,
              room: item,
              promotionCode: !isEmpty(el?.promotions)
                ? el?.promotions[0]?.code
                : "",
            })}
            isShowCode
          />
        ))}
      </Box>

      <RoomDetails
        open={visibleDrawerType === listTypeModal.MODAL_ROOM_DETAILS}
        toggleDrawer={toggleDrawer}
        item={item}
        rateSelected={rateSelected}
        handleBookRoomRate={handleBookRoomRate({
          roomKey: item.roomKey,
          rateKey: rateSelected.rateKey,
          rate: rateSelected,
          room: item,
          promotionCode: !isEmpty(rateSelected?.promotions)
            ? rateSelected?.promotions[0]?.code
            : "",
        })}
      />
    </Box>
  );
};
export default memo(ItemRooms, areEqualItemRoom);
