import { Box } from "@material-ui/core";
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import InfiniteScroll from "react-infinite-scroller";

import { getHotelsAvailability } from "@api/hotels";
import {
  listString,
  formatDate,
  DELAY_TIMEOUT_POLLING,
  RADIUS_GET_HOTEL_LAT_LONG,
} from "@utils/constants";
import { isEmpty, adapterHotelAvailability } from "@utils/helpers";
import HotelItemListing from "@components/mobile/hotels/HotelItemListing";

import LoadingMore from "@components/common/loading/LoadingMore";

const useStyles = makeStyles((theme) => ({
  container: {
    padding: "16px 12px 60px",
    position: "relative",
  },
  styleHotelItem: {
    padding: "12px 0",
  },
  divider: {
    height: 6,
    width: "calc(100% + 24px)",
    backgroundColor: theme.palette.gray.grayLight22,
    margin: "0 -12px",
  },
}));
const pageSize = 10;

const ListHotelRecent = ({ hotelDetail = {}, paramsSearch = {} }) => {
  const classes = useStyles();
  const [listHotel, setListHotel] = useState([]);
  const [totalHotel, setTotalHotel] = useState(0);
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(0);
  let breakPolling = true;
  useEffect(() => {
    setPage(0);
  }, [hotelDetail]);

  useEffect(() => {
    return () => {
      breakPolling = false;
    };
  }, []);

  const loadFunc = async () => {
    let polling = true;
    const pageTemp = page + 1;
    try {
      const params = {
        ...paramsSearch,
        latitude: hotelDetail?.address?.coordinate.latitude,
        longitude: hotelDetail?.address?.coordinate.longitude,
        radius: RADIUS_GET_HOTEL_LAT_LONG,
        searchType: "latlon",
        page: pageTemp,
        size: pageSize,
      };
      setLoading(false);
      while (polling && breakPolling) {
        const { data } = await getHotelsAvailability(params);
        if (data.code === 200) {
          if (data.data.completed) {
            setListHotel([...listHotel, ...data?.data?.items]);
            setTotalHotel(data?.data.total);
            setPage(pageTemp);
            if (pageTemp * pageSize < data?.data.total) {
              setLoading(true);
            }
            polling = false;
            break;
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      polling = false;
    }
  };

  return (
    <Box className={classes.container}>
      <Box component="span" fontSize={18} fontWeight={600} lineHeight="21px">
        {listString.IDS_MT_TEXT_HOTEL_RECENT}
      </Box>
      <InfiniteScroll pageStart={page} loadMore={loadFunc} hasMore={loading}>
        {adapterHotelAvailability(listHotel).map((el, index) => {
          if (el.id === hotelDetail.id) return null;
          return (
            <Box key={el.id}>
              <HotelItemListing
                styleHotelItem={classes.styleHotelItem}
                item={el}
                paramsFilterInit={paramsSearch}
              />
              {index !== listHotel.length - 1 && (
                <Box className={classes.divider} />
              )}
            </Box>
          );
        })}
        {!loading && page * pageSize < totalHotel && <LoadingMore />}
      </InfiniteScroll>
    </Box>
  );
};

export default ListHotelRecent;
