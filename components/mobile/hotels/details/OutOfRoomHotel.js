import PropTypes from "prop-types";
import { Box, makeStyles, Typography } from "@material-ui/core";

import { IconSad } from "@public/icons";
import Image from "@src/image/Image";
import { listString } from "@utils/constants";

const initItem = {};

const useStyles = makeStyles((theme) => ({
  wraperContainer: {
    color: theme.palette.black.black3,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    margin: "24px 16px 0 16px",
  },
  iconSad: {
    display: "flex",
    alignItems: "center",
    marginTop: 14,
  },
  soldOut: {
    fontSize: 14,
    lineHeight: "22px",
  },
  imageEmpty: {
    width: 193,
    height: 170,
  },
}));

const OutOfRoomHotel = () => {
  const classes = useStyles();
  return (
    <>
      <Box className={classes.wraperContainer}>
        <Image
          srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_no_result_search_listing.svg"
          className={classes.imageEmpty}
        />
        <Box className={classes.iconSad}>
          <IconSad />{" "}
          <Typography
            variant="subtitle1"
            component="span"
            style={{ marginLeft: "8px" }}
          >
            {listString.IDS_MT_TEXT_HOTEL_MISS_ROOM}
          </Typography>
        </Box>
        <Box marginTop={1} textAlign="center" fontSize={14}>
          <Typography className={classes.soldOut} component="span">
            {listString.IDS_MT_TEXT_HOTEL_ROOM_SOLD_OUT}{" "}
          </Typography>
          <Typography style={{ fontWeight: "600" }} component="span">
            {listString.IDS_MT_TEXT_HOTEL_HOTLINE_CONTACT}{" "}
          </Typography>
          {listString.IDS_MT_TEXT_HOTEL_SUPPORT_CONTACT}
        </Box>
      </Box>
    </>
  );
};
OutOfRoomHotel.propTypes = {};
export default OutOfRoomHotel;
