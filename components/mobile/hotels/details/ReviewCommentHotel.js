import clsx from "clsx";
import moment from "moment";
import { memo } from "react";
import sanitizeHtml from "sanitize-html";
import { makeStyles } from "@material-ui/core/styles";
import { Box, Typography, Avatar } from "@material-ui/core";

import { IconDot } from "@public/icons";
import {
  isEmpty,
  unEscape,
  getMessageHotelReviews,
  areEqualItem,
  getFirstName,
} from "@utils/helpers";
import { optionClean } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  wrapContainer: {
    margin: "12px 0",
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  wrapHeader: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  point: {
    lineHeight: "17px",
    color: theme.palette.white.main,
    background: theme.palette.blue.blueLight8,
    height: 18,
    width: 30,
    padding: "2px 4px",
    borderRadius: 4,
    marginLeft: 4,
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
  },
  wrapInfoUser: {
    display: "flex",
    marginLeft: 8,
  },
  dateReview: {
    color: theme.palette.gray.grayDark7,
    display: "flex",
    alignItems: "center",
  },
  blue: {
    background: theme.palette.blue.blueLight9,
    color: theme.palette.blue.blueLight8,
    fontSize: 14,
  },
  iconDot: {
    margin: "0 5px",
    fill: theme.palette.gray.grayLight25,
  },
  messageReview: {
    fontSize: 14,
    lineHeight: "17px",
    fontWeight: "normal",
    color: theme.palette.black.black3,
  },
  wrapPoint: {
    display: "flex",
  },
  wrapDateReview: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    marginLeft: 8,
  },
  wrapContentReview: {
    marginTop: 8,
    color: theme.palette.black.black3,
  },
  descriptionReview: {
    fontSize: 14,
    lineHeight: "22px",
    fontWeight: "normal",
    margin: "4px 0 12px 0",
  },
  lineClamp: {
    display: "-webkit-box",
    WebkitLineClamp: 3,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
  },
}));

const ReviewCommentHotel = ({
  item = {},
  styleWraper = "",
  isShowLineClamp = false,
}) => {
  const classes = useStyles();
  return (
    <>
      <Box className={clsx(classes.wrapContainer, styleWraper)}>
        <Box className={classes.wrapHeader}>
          <Box className={classes.wrapInfoUser}>
            <Avatar className={classes.blue}>
              {getFirstName(item.username)}
            </Avatar>
            <Box className={classes.wrapDateReview}>
              <Typography variant="subtitle2" style={{ marginBottom: "3px" }}>
                {item?.username || "Khách Lưu Trú"}
              </Typography>
              <Typography
                component="span"
                variant="body2"
                className={classes.dateReview}
              >
                {moment(item?.publishedDate).format("DD/MM/YYYY")}{" "}
                <IconDot className={`svgFillAll ${classes.iconDot}`} />
                <Typography variant="body2" className={classes.dateReview}>
                  {item?.travelType}
                </Typography>
              </Typography>
            </Box>
          </Box>
          <Box className={classes.wrapPoint}>
            <Typography className={classes.messageReview}>
              {getMessageHotelReviews(item?.rating)}
            </Typography>
            <Typography variant="subtitle2" className={classes.point}>
              {!isEmpty(item.rating) && 2 * item.rating === 10
                ? 10
                : (2 * item.rating).toFixed(1)}
            </Typography>
          </Box>
        </Box>
        <Box className={classes.wrapContentReview}>
          <Typography variant="subtitle2">{item?.title}</Typography>
          {/* <Typography className={classes.descriptionReview}>
            {item?.content}
          </Typography> */}
          <Box
            className={clsx(
              classes.descriptionReview,
              isShowLineClamp && classes.lineClamp
            )}
            dangerouslySetInnerHTML={{
              __html: sanitizeHtml(
                unEscape(unescape(item?.content || "")),
                optionClean
              ),
            }}
          />
        </Box>
      </Box>
    </>
  );
};

export default memo(ReviewCommentHotel, areEqualItem);
