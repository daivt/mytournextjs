import { Box, Grid, Typography } from "@material-ui/core";
import LinearProgress from "@material-ui/core/LinearProgress";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { listString } from "@utils/constants";
import { getMessageHotelReviews } from "@utils/helpers";
import clsx from "clsx";
import PropTypes from "prop-types";

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 4,
    borderRadius: 100,
    width: 128,
  },
  colorPrimary: {
    backgroundColor: theme.palette.gray.grayLight23,
  },
  bar: {
    borderRadius: 100,
    height: 4,
    backgroundColor: theme.palette.blue.blueLight8,
  },
}))(LinearProgress);

const useStyles = makeStyles((theme) => ({
  wrapContainer: {
    margin: "12px 0",
  },
  wrapEvaluate: {
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  wrapBoxLeft: {
    height: 111,
    border: `1px solid ${theme.palette.blue.blueLight8}`,
    color: theme.palette.blue.blueLight8,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    borderRadius: 8,
    marginBottom: 16,
  },
  rating: {
    fontWeight: 600,
    fontSize: 28,
    lineHeight: "33px",
  },
  messageReview: {
    marginTop: 4,
    lineHeight: "17px",
  },
  wrapBoxRight: {
    display: "flex",
    flexDirection: "column",
    marginLeft: 16,
  },
  itemLevel: {
    margin: "4px 0",
    fontSize: 14,
    color: theme.palette.black.black3,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  nonMargin: {
    margin: 0,
  },

  wrapProgressBar: {
    width: 128,
  },
  countEvaluate: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.black.black3,
    width: 21,
    textAlign: "right",
  },
  levelMessage: { fontSize: 14, width: 61, marginRight: 12 },
}));

const EvaluateHotelDetails = ({ item = {}, styleWraper = "" }) => {
  const propStyle = {
    styleWraper,
  };
  const classes = useStyles(propStyle);

  const getListRating = [
    {
      id: 0,
      name: "Vị trí",
      point: item.locationRating,
    },
    {
      id: 1,
      name: "Giá cả",
      point: item.priceRating,
    },
    {
      id: 2,
      name: "Phục vụ",
      point: item.serviceRating,
    },
    {
      id: 3,
      name: "Vệ sinh",
      point: item.cleanlinessRating,
    },
    {
      id: 4,
      name: "Tiện nghi",
      point: item.convenientRating,
    },
  ];
  return (
    <>
      <Box className={clsx(classes.wrapContainer, styleWraper)}>
        <Grid container className={classes.wrapEvaluate}>
          <Grid item xs={3}>
            <Box className={classes.wrapBoxLeft}>
              <Box className={classes.rating}>
                {2 * item?.rating === 10 ? 10 : (2 * item?.rating).toFixed(1)}
              </Box>
              <Typography variant="subtitle2" className={classes.messageReview}>
                {getMessageHotelReviews(item?.rating)}
              </Typography>
              <Box>
                {item?.count} {listString.IDS_MT_TEXT_EVALUATE}
              </Box>
            </Box>
          </Grid>
          <Grid item xs={9}>
            <Box className={classes.wrapBoxRight}>
              {getListRating.map((el, index) => (
                <Box
                  className={clsx(
                    classes.itemLevel,
                    index === 0 && classes.nonMargin,
                    index === getListRating.length - 1 && classes.nonMargin
                  )}
                  key={index.toString()}
                >
                  <Box className={classes.levelMessage}>{el.name}</Box>

                  <Box className={classes.wrapProgressBar}>
                    <BorderLinearProgress
                      variant="determinate"
                      value={(el.point * 100) / 5}
                    />
                  </Box>
                  <Typography className={classes.countEvaluate}>
                    {(2 * el.point).toFixed(1)}
                  </Typography>
                </Box>
              ))}
            </Box>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};
EvaluateHotelDetails.propTypes = { styleWraper: PropTypes.string };
export default EvaluateHotelDetails;
