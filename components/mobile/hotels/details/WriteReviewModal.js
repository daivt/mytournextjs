import React from "react";
import { addReviewRating, uploadImage } from "@api/hotels";
import CloseIcon from "@material-ui/icons/Close";
import { useSnackbar } from "notistack";
import {
  Box,
  Drawer,
  Slide,
  AppBar,
  TextField,
  TextareaAutosize,
  Select,
  MenuItem,
  FormControl,
} from "@material-ui/core";
import { IconArrowDownToggle, IconUpload } from "@public/icons";
import { makeStyles } from "@material-ui/styles";
import { useRouter } from "next/router";
import { isEmpty } from "@utils/helpers";
import ButtonComponent from "@src/button/Button";
import { listString, routeStatic, TRAVEL_TYPE } from "@utils/constants";
import { useField, useFormikContext, Formik, Form } from "formik";
import snackbarSetting from "@src/alert/Alert";

const useStyles = makeStyles((theme) => ({
  paperAnchorBottom: {
    height: "100vh",
    width: "100vw",
  },
  appBar: {
    position: "relative",
    borderRadius: 0,
    background: "white",
    color: "#1A202C",
    boxShadow: "none",
    display: "flex",
    padding: "10px 24px 12px 24px",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  headerText: {
    fontWeight: 600,
    fontSize: 20,
    lineHeight: "24px",
    marginTop: 12,
  },
  dialogContent: { padding: "0 24px" },
  rowGroupItem: { display: "flex", flexDirection: "column" },
  inputItem: {
    margin: 0,
    marginBottom: 20,
    width: "100%",
    "& > .MuiInput-root": { marginTop: 0 },
  },
  itemGroup: {
    margin: 0,
    marginBottom: 20,
    width: "100%",
    display: "flex",
    flexDirection: "column",
  },
  textItem: {
    fontSize: 14,
    lineHeight: "17px",
    color: "#1A202C",
    marginBottom: 8,
  },
  rateItem: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  normalText: { fontSize: 14, lineHeight: "17px", color: "#1A202C" },
  textAreaItem: {
    resize: "none",
    maxHeight: 164,
    minHeight: 164,
    border: "1px solid #CBD5E0",
    borderRadius: 8,
    fontFamily: "SF Pro Text",
    padding: 16,
    fontSize: 14,
    lineHeight: "17px",
    color: "#1A202C",
    overflowY: "auto",
  },
  uploadImageBox: {
    background: "#F7FAFC",
    border: "1px dashed #CBD5E0",
    borderRadius: 8,
    width: 80,
    height: 80,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 12,
    lineHeight: "14px",
    color: "#718096",
    cursor: "pointer",
  },
  imageGroup: { display: "flex" },
  imageItem: {
    width: 80,
    height: 80,
    background: "#F7FAFC",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 12,
    borderRadius: 8,
  },
}));
const sliderRange = { min: 0, max: 10 };

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
const MyTextField = ({ ...props }) => {
  const [field, meta] = useField(props);
  const { submitCount } = useFormikContext();
  return (
    <TextField
      fullWidth
      {...field}
      {...props}
      error={submitCount > 0 && meta.error ? meta.error : ""}
    />
  );
};
const MyTextAreaField = ({ ...props }) => {
  const [field, meta] = useField(props);
  const { submitCount } = useFormikContext();
  return (
    <TextareaAutosize
      fullWidth
      {...field}
      {...props}
      error={submitCount > 0 && meta.error ? meta.error : ""}
      maxRows={10}
      style={{ height: 164, overflow: "auto" }}
    />
  );
};

const ratingDetailData = [
  {
    name: listString.IDS_MT_TEXT_EVALUATE_LEVEL_LOCATION,
    value: "locationRating",
  },
  {
    name: listString.IDS_MT_TEXT_EVALUATE_LEVEL_PRICE,
    value: "priceRating",
  },
  {
    name: listString.IDS_MT_TEXT_EVALUATE_LEVEL_SERVE,
    value: "serviceRating",
  },
  {
    name: listString.IDS_MT_TEXT_EVALUATE_LEVEL_CLEAN,
    value: "cleanlinessRating",
  },
  {
    name: listString.IDS_MT_TEXT_FILTER_CONVENIENT,
    value: "convenientRating",
  },
];
const WriteReviewModal = ({ hotelDetail = {} }) => {
  const classes = useStyles();
  const router = useRouter();
  const [fileUploads, setFileUploads] = React.useState([]);
  const [open, setOpen] = React.useState(false);
  const [uploadError, setError] = React.useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  React.useEffect(() => {
    if (!isEmpty(router.query) && router.query.review === "true") {
      setOpen(true);
    }
  }, [router.query]);
  const handleClose = () => {
    setOpen(false);
    router.push({
      pathname: routeStatic.HOTEL_DETAIL.href,
      query: {
        alias: `${hotelDetail.id}-${
          !isEmpty(hotelDetail.name) ? hotelDetail.name.stringSlug() : ""
        }.html`,
      },
    });
  };
  const handleUploadFile = async (e) => {
    const { files } = e.target;
    if (files && files.length > 0) {
      const temp = [];
      const fileList = Object.values(e.target.files);
      const fileUp = fileList.filter((v) => v.size <= 25 * 1024 * 1024);
      if (fileList.length > fileUp.length) setError(true);
      else setError(false);
      if (!isEmpty(fileUp)) {
        fileUp.forEach((file, idx) => {
          if (idx < 5 - fileUploads.length) {
            const formData = new FormData();
            formData.append("file", file);
            temp.push(uploadImage(formData));
          } else {
            setError(true);
          }
        });
        const res = await Promise.all(temp);
        const fileUploaded = [];
        if (!isEmpty(res)) {
          res.forEach((v) => {
            if (v.data.code === 200 && !isEmpty(v.data.photo)) {
              fileUploaded.push(v.data.photo.thumbLink);
            }
          });
        }
        setFileUploads([...fileUploads, ...fileUploaded]);
      }
    }
  };
  const handleReview = async (values) => {
    let temp = {
      hotelId: hotelDetail.id,
      title: values.title,
      content: values.content,
      travelType: values.travelType,
      images: fileUploads,
      ratingDetail: {
        locationRating: values.locationRating / 2,
        priceRating: values.priceRating / 2,
        serviceRating: values.serviceRating / 2,
        cleanlinessRating: values.cleanlinessRating / 2,
        convenientRating: values.convenientRating / 2,
      },
    };
    if (router.query.hashKey) {
      temp = { ...temp, hashKey: router.query.hashKey };
    }
    try {
      const { data } = await addReviewRating(temp);
      if (data.code === 200) {
        enqueueSnackbar(
          "Cám ơn bạn đã đánh giá!",
          snackbarSetting((key) => closeSnackbar(key), { color: "success" })
        );
        handleClose();
      } else {
        enqueueSnackbar(
          data.message,
          snackbarSetting((key) => closeSnackbar(key), { color: "error" })
        );
      }
    } catch (error) {
      console.log("error", error);
    }
  };
  const getSliderBG = (value) => {
    const { min, max } = sliderRange;
    return `linear-gradient(to right, #00B6F3 0%, #00B6F3 ${((value - min) /
      (max - min)) *
      100}%, #DEE2E6 ${((value - min) / (max - min)) * 100}%, #DEE2E6 100%)`;
  };
  return (
    <Drawer
      anchor="bottom"
      open={open}
      onClose={handleClose}
      classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
    >
      <AppBar className={classes.appBar}>
        <span className={classes.headerText}>
          {listString.IDS_MT_TEXT_WRITE_REVIEW_HOTEL}
        </span>
        <CloseIcon
          style={{ fontSize: 24, cursor: "pointer" }}
          onClick={handleClose}
        />
      </AppBar>
      <Formik
        initialValues={{
          locationRating: 9,
          priceRating: 9,
          serviceRating: 9,
          cleanlinessRating: 9,
          convenientRating: 9,
          travelType: "couple",
        }}
        onSubmit={handleReview}
      >
        {({ values, setValues, errors, validateForm, setFieldValue }) => {
          return (
            <Form>
              <Box className={classes.dialogContent}>
                <Box className={classes.rowGroupItem}>
                  <FormControl className={classes.itemGroup}>
                    <span
                      className={classes.textItem}
                      style={{ marginBottom: 0 }}
                    >
                      {listString.IDS_MT_TEXT_TITLE}
                    </span>
                    <MyTextField label="" name="title" />
                  </FormControl>
                  <FormControl className={classes.itemGroup}>
                    <span
                      className={classes.textItem}
                      style={{ marginBottom: 0 }}
                    >
                      {listString.IDS_MT_TEXT_TYPE_TRIP}
                    </span>
                    <Select
                      label={listString.IDS_MT_TEXT_TYPE_TRIP}
                      name="travelType"
                      value={values.travelType || ""}
                      onChange={(e) => {
                        setFieldValue("travelType", e.target.value);
                      }}
                      IconComponent={() => <IconArrowDownToggle />}
                    >
                      {TRAVEL_TYPE.map((el, i) => (
                        <MenuItem value={el.type}>{el.name}</MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Box>
                <Box className={classes.rowGroupItem}>
                  <Box className={classes.itemGroup}>
                    <span className={classes.textItem}>
                      {listString.IDS_MT_CONTENT_REVIEW}
                    </span>
                    <MyTextAreaField
                      label={listString.IDS_MT_CONTENT_REVIEW}
                      name="content"
                      className={classes.textAreaItem}
                    />
                  </Box>
                  <Box className={classes.itemGroup}>
                    <span className={classes.textItem}>
                      {listString.IDS_MT_DETAIL_REVIEW}
                    </span>
                    {ratingDetailData.map((el, i) => (
                      <Box
                        key={i}
                        className={classes.rateItem}
                        style={{ marginTop: i === 0 ? 8 : 16 }}
                      >
                        <Box
                          className={classes.normalText}
                          style={{ width: 70 }}
                        >
                          {el.name}
                        </Box>
                        <input
                          type="range"
                          min={sliderRange.min}
                          max={sliderRange.max}
                          step={0.1}
                          name={el.value}
                          id={el.value}
                          value={values[el.value] || 0.0}
                          style={{
                            width: "calc(100% - 100px)",
                            background: getSliderBG(values[el.value] || 0.0),
                          }}
                          className="input-slider"
                          onChange={(e) => {
                            setFieldValue(`${el.value}`, e.target.value);
                          }}
                        />

                        <Box
                          className={classes.normalText}
                          style={{ width: 30, textAlign: "right" }}
                        >
                          {values[el.value] || 0.0}
                        </Box>
                      </Box>
                    ))}
                  </Box>
                </Box>
                <p className={classes.textItem} style={{ marginTop: 0 }}>
                  <span className={classes.textItem}>
                    {listString.IDS_MT_UPLOAD_IMAGE}
                  </span>
                  &nbsp;
                  <span
                    className={classes.textItem}
                    style={{ color: "#718096" }}
                  >
                    {listString.IDS_MT_LIMIT_UPLOAD}
                  </span>
                </p>
                <Box className={classes.imageGroup}>
                  {!isEmpty(fileUploads) &&
                    fileUploads.map((elUrl, i) => (
                      <Box className={classes.imageItem} key={i}>
                        <img
                          key={i}
                          src={elUrl}
                          alt=""
                          style={{ maxWidth: 80, maxHeight: 80 }}
                        />
                      </Box>
                    ))}
                  {fileUploads.length < 5 && (
                    <Box
                      className={classes.uploadImageBox}
                      onClick={() => {
                        document.getElementById("upload_file").click();
                      }}
                    >
                      <input
                        type="file"
                        style={{ display: "none" }}
                        id="upload_file"
                        multiple
                        onChange={handleUploadFile}
                        onClick={(e) => {
                          e.target.value = null;
                        }}
                      />
                      <IconUpload
                        className="svgFillAll"
                        style={{ stroke: "#718096", marginBottom: 6 }}
                      />
                      <span>{listString.IDS_MT_UPLOAD}</span>
                    </Box>
                  )}
                </Box>
              </Box>
              <Box style={{ display: "flex", justifyContent: "flex-end" }}>
                <ButtonComponent
                  type="submit"
                  backgroundColor="#FF1284"
                  height={44}
                  fontSize={16}
                  fontWeight={600}
                  borderRadius={8}
                  style={{ padding: "12px 30px", margin: 24, width: "auto" }}
                >
                  {listString.IDS_MT_SEND_REVIEW}
                </ButtonComponent>
              </Box>
            </Form>
          );
        }}
      </Formik>
    </Drawer>
  );
};

export default WriteReviewModal;
