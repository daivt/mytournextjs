import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import { isEmpty } from "@utils/helpers";
import ItemRooms from "@components/mobile/hotels/details/ItemRooms";
import OutOfRoomHotel from "@components/mobile/hotels/details/OutOfRoomHotel";

const useStyles = makeStyles((theme) => ({
  divider: {
    height: 8,
    backgroundColor: theme.palette.gray.grayLight22,
  },
  loadingBoxRoom: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: 250,
    width: "100%",
  },
}));

const ListRoomHotel = ({
  listRoom = [],
  hotelDetail = {},
  refStart = null,
  refEnd = null,
  isFetchingRoom = false,
  handleBookRoomRate = () => {},
}) => {
  const classes = useStyles();

  if (isFetchingRoom) {
    return (
      <Box className={classes.loadingBoxRoom}>
        <img
          src="https://staticproxy.mytourcdn.com/0x0/themes/images/mloading.gif"
          className={classes.imgLoading}
        />
      </Box>
    );
  }
  if (isEmpty(listRoom)) {
    return <OutOfRoomHotel />;
  }

  return (
    <>
      {listRoom.map((el, index) => (
        <Box
          key={index.toString()}
          ref={
            index === 0
              ? refStart
              : index === listRoom.length - 1
              ? refEnd
              : null
          }
        >
          <ItemRooms
            item={el}
            roomRecommend={index === 0}
            hotelDetail={hotelDetail}
            handleBookRoomRate={handleBookRoomRate}
          />
          <Box className={classes.divider} />
        </Box>
      ))}
    </>
  );
};

export default ListRoomHotel;
