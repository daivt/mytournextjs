import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import InfiniteScroll from "react-infinite-scroller";
import { Box, Grid, Typography } from "@material-ui/core";

import {
  isEmpty,
  adapterHotelHotDeal,
  paramsDefaultHotel,
} from "@utils/helpers";
import { getTopHotels } from "@api/homes";
import utilStyles from "@styles/utilStyles";
import { listString, DELAY_TIMEOUT_POLLING } from "@utils/constants";
import { getListPromotionCode, getHotelsAvailability } from "@api/hotels";

import * as actionTypes from "@contextProvider/system/ActionTypes";
import { useDispatchSystem } from "@contextProvider/ContextProvider";

import Image from "@src/image/Image";
import Layout from "@components/layout/mobile/Layout";
import HotelCard from "@components/common/card/hotel/HotelCard";
import LoadingMore from "@components/common/loading/LoadingMore";
import DiscountCard from "@components/common/card/hotel/DiscountCard";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    color: theme.palette.black.black3,
    fontSize: 14,
    padding: "0 16px",
  },
  topBanner: {
    height: 183,
    width: "100%",
  },
  wrapperItem: {
    paddingTop: 16,
  },
  itemHotelCard: {
    paddingBottom: 16,
  },
  classNameCard: {
    minWidth: "80%",
  },
}));

const pageSize = 20;

const MobileContent = () => {
  const classes = useStyles();
  const classesUtils = utilStyles();
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(true);
  const [dataHotel, setDataHotel] = useState([]);
  const [totalHotel, setTotalHotel] = useState(0);
  const [listPromotion, setListPromotion] = useState([]);
  let breakPolling = true;
  const dispatch = useDispatchSystem();

  useEffect(() => {
    fetPromotionCode();
    return () => {
      breakPolling = false;
    };
  }, []);

  const fetPromotionCode = async () => {
    try {
      const { data } = await getListPromotionCode({
        page: 1,
        size: 10,
        module: "hotel",
      });
      setListPromotion(data?.data.items);
    } catch (error) {}
  };

  const loadFunc = async () => {
    const pageTemp = page;
    try {
      setLoading(false);
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: true,
      });
      const { data } = await getTopHotels({
        page: pageTemp,
        size: pageSize,
        ...paramsDefaultHotel(),
      });
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
      if (data.code === 200) {
        const dataConcat = [...dataHotel, ...data?.data?.items];
        fetPriceHotelHotDeal(data?.data?.items, dataConcat);
        setPage(pageTemp + 1);
        setDataHotel(dataConcat);
        setTotalHotel(data?.data.total);
        if (pageTemp * pageSize < data?.data.total) {
          setLoading(true);
        }
      }
    } catch (error) {
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
    }
  };

  const fetPriceHotelHotDeal = (dataFetPrice = [], dataConcat = []) => {
    if (!isEmpty(dataFetPrice)) {
      const hotelHotDealIds = dataFetPrice.map((el) => el.id);
      fetPriceHotel(dataConcat, hotelHotDealIds);
    }
  };

  const fetPriceHotel = async (dataConcat = [], ids = []) => {
    let polling = true;
    try {
      const dataDto = {
        ...paramsDefaultHotel(),
        hotelIds: ids,
        searchType: "hotel",
      };
      while (polling && breakPolling) {
        const { data } = await getHotelsAvailability(dataDto);
        if (data.code === 200) {
          mergerPriceToData(dataConcat, data?.data.items);
          if (data.data.completed) {
            polling = false;
            break;
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      polling = false;
    }
  };

  const mergerPriceToData = (dataConcat = [], dataPoling = []) => {
    let dataResult = [...dataConcat];
    dataPoling.forEach((el) => {
      dataResult.forEach((item, index) => {
        if (item.id === el.id) {
          dataResult[index] = {
            ...item,
            promotionInfo: el.basePromotionInfo,
            price: el.basePrice,
            hiddenPrice: el.hiddenPrice,
          };
        }
      });
    });
    setDataHotel(dataResult);
  };

  return (
    <Layout isHeaderBack titleHeaderBack={listString.IDS_TEXT_CAN_BELIEVE}>
      <Image
        srcImage="https://storage.googleapis.com/tripi-assets/mytour/banner/img_banner_best_price.png"
        className={classes.topBanner}
      />
      <Box className={classes.container}>
        {!isEmpty(listPromotion) && (
          <>
            <Box component="span" py={2}>
              <Typography variant="h5">
                {listString.IDS_TEXT_CODE_DEALS_HOT}
              </Typography>
            </Box>
            <Box ml={-1} className={classesUtils.scrollViewHorizontal}>
              {listPromotion.map((el) => (
                <DiscountCard
                  cardInfo={el}
                  key={el.id}
                  classNameCard={classes.classNameCard}
                />
              ))}
            </Box>
          </>
        )}
        <Box component="span" pt={2}>
          <Typography variant="h5">
            {listString.IDS_TEXT_HOTEL_BIG_SALE}
          </Typography>
        </Box>
        <InfiniteScroll
          initialLoad
          pageStart={page}
          loadMore={loadFunc}
          hasMore={loading}
        >
          <Grid container spacing={2} className={classes.wrapperItem}>
            {adapterHotelHotDeal(dataHotel).map((el) => (
              <Grid
                item
                lg={6}
                md={6}
                sm={6}
                xs={6}
                key={el.id}
                className={classes.itemHotelCard}
              >
                <HotelCard item={el} />
              </Grid>
            ))}
          </Grid>
          {!loading && page * pageSize < totalHotel && <LoadingMore />}
        </InfiniteScroll>
      </Box>
    </Layout>
  );
};

export default MobileContent;
