import moment from "moment";
import { makeStyles } from "@material-ui/styles";
import { Box, Typography } from "@material-ui/core";

import { listString } from "@utils/constants";
import { IconMoonlight } from "@public/icons";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    alignItems: "center",
    color: theme.palette.black.black3,
  },
  divider: {
    width: 1,
    height: 48,
    backgroundColor: theme.palette.gray.grayLight22,
  },
  nightDay: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    width: 28,
    height: 28,
    borderRadius: "50%",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    backgroundColor: theme.palette.white.main,
  },
}));
const ShowDateView = ({ isShowTitle = false, startDate, endDate }) => {
  const classes = useStyles();
  return (
    <Box className={classes.container}>
      <Box>
        {isShowTitle && (
          <Box pt={10 / 8}>
            <Typography variant="body2">
              {listString.IDS_MT_TEXT_CHECK_IN_ROOM}
            </Typography>
          </Box>
        )}
        <Box display="flex" flexDirection="row" alignItems="center">
          <Box fontSize={36} lineHeight="43px" pr={1}>
            {startDate ? startDate.format("DD") : "-"}
          </Box>
          <Box display="flex" flexDirection="column">
            {startDate && (
              <Box fontSize={12} fontWeight={600} lineHeight="14px" pb={2 / 8}>
                {startDate.format("YYYY") !== moment().format("YYYY") ? (
                  <Typography variant="body2" style={{ fontWeight: 600 }}>
                    Tháng {startDate.format("MM")}, {startDate.format("YYYY")}
                  </Typography>
                ) : (
                  <Typography variant="body2" style={{ fontWeight: 600 }}>
                    Tháng {startDate.format("MM")}
                  </Typography>
                )}
              </Box>
            )}
            <Box color="gray.grayDark8">
              <Typography variant="body2">
                {startDate
                  ? startDate
                      .locale("vi_VN")
                      .format("ddd")
                      .replace("T", "Thứ ")
                      .replace("CN", "Chủ Nhật")
                  : "-"}
              </Typography>
            </Box>
          </Box>
        </Box>
      </Box>
      <Box
        mx={4}
        position="relative"
        display="flex"
        justifyContent="center"
        alignItems="center"
      >
        <Box className={classes.divider} />
        <Box className={classes.nightDay}>
          <Box component="span" fontSize={11} lineHeight="13px" pr={2 / 8}>
            {startDate && endDate
              ? Math.abs(
                  moment(startDate)
                    .startOf("day")
                    .diff(endDate.startOf("day"), "days")
                )
              : "0"}
          </Box>
          <IconMoonlight />
        </Box>
      </Box>
      <Box>
        {isShowTitle && (
          <Box pt={10 / 8}>
            <Typography variant="body2">
              {listString.IDS_MT_TEXT_CHECK_OUT_ROOM}
            </Typography>
          </Box>
        )}
        <Box display="flex" flexDirection="row" alignItems="center">
          <Box fontSize={36} lineHeight="43px" pr={1}>
            {endDate ? endDate.format("DD") : "-"}
          </Box>
          {endDate && (
            <Box display="flex" flexDirection="column">
              {endDate.format("YYYY") !== moment().format("YYYY") ? (
                <Typography variant="body2" style={{ fontWeight: 600 }}>
                  Tháng {endDate.format("MM")}, {endDate.format("YYYY")}
                </Typography>
              ) : (
                <Typography variant="body2" style={{ fontWeight: 600 }}>
                  Tháng {endDate.format("MM")}
                </Typography>
              )}
              <Box color="gray.grayDark8">
                <Typography variant="body2">
                  {endDate
                    ? endDate
                        .locale("vi_VN")
                        .format("ddd")
                        .replace("T", "Thứ ")
                        .replace("CN", "Chủ Nhật")
                    : "-"}
                </Typography>
              </Box>
            </Box>
          )}
        </Box>
      </Box>
    </Box>
  );
};

export default ShowDateView;
