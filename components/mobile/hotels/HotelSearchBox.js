import clsx from "clsx";
import { useState } from "react";
import { useRouter } from "next/router";
import { makeStyles } from "@material-ui/styles";
import { Box, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";

import {
  IconCalenderSearchHotel,
  IconDirection,
  IconDot,
  IconSearch,
  IconUser2,
} from "@public/icons";
import { isEmpty } from "@utils/helpers";
import {
  formatDate,
  listString,
  routeStatic,
  listEventHotel,
  LAST_SEARCH_HISTORY,
} from "@utils/constants";
import * as gtm from "@utils/gtm";

import ButtonComponent from "@src/button/Button";
import ShowDateView from "@components/mobile/hotels/ShowDateView";
import CalendarDialog from "@components/common/calendar/CalendarDialog";
import GuestInfoBoxModal from "@components/common/modal/hotel/GuestInfoBoxModal";
import DestinationSearchModal from "@components/common/modal/hotel/DestinationSearchModal";

const useStyles = makeStyles((theme) => ({
  container: {
    position: "relative",
    padding: 16,
  },
  boxContentItem: {
    padding: "12px 0",
    borderBottom: "1px solid #EDF2F7",
  },
  searchBoxContent: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  guestBoxContent: {
    display: "flex",
    alignItems: "center",
    padding: "19px 0 18px",
  },
  calendarBoxContent: {
    display: "flex",
    alignItems: "center",
  },
  iconDot: {
    margin: "0 8px",
  },
  infoSearch: {
    fontSize: 16,
    fontWeight: 400,
    color: theme.palette.black.black3,
  },
  IconUser2: {
    width: 24,
    height: 24,
  },
}));

const listTypeModal = {
  MODAL_DESTINATION_SEARCH: "MODAL_DESTINATION_SEARCH",
  MODAL_GUEST_INFO: "MODAL_GUEST_INFO",
};
const HotelSearchBox = ({
  paramsSearch = {},
  checkIn = null,
  checkOut = null,
  itemLocationSelect = {},
  handleOkSelectGuest = () => {},
  setItemLocationSelected = () => {},
  setCheckIn = () => {},
  setCheckOut = () => {},
}) => {
  const theme = useTheme();
  const router = useRouter();
  const classes = useStyles();
  const [visibleDrawerType, setVisibleDrawerType] = useState("");

  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };
  const handleSelectGuest = (values) => {
    handleOkSelectGuest(values);
    setVisibleDrawerType("");
  };

  const handleSelectedItem = (item) => {
    setItemLocationSelected(item);
    setVisibleDrawerType("");
  };

  const handleAllowGps = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          actionSearch(
            {
              slug: [`ksgb`, `khach-san-gan-ban.html`],
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
            },
            "near-hotel"
          );
        },
        () => {}
      );
    }
  };

  const handleBtnSearch = () => {
    if (isEmpty(itemLocationSelect)) {
      setVisibleDrawerType(listTypeModal.MODAL_DESTINATION_SEARCH);
      return;
    } else if (itemLocationSelect.type === "near-hotel") {
      handleAllowGps();
    } else {
      if (itemLocationSelect.type === "HOTEL") {
        actionSearch(
          {
            alias: `${
              itemLocationSelect.hotelId
            }-${itemLocationSelect?.name.stringSlug()}.html`,
          },
          "hotel"
        );
      } else {
        actionSearch(
          {
            slug: [
              `${itemLocationSelect.aliasCode}`,
              `khach-san-tai-${itemLocationSelect?.name.stringSlug()}.html`,
            ],
          },
          "location"
        );
      }
    }
  };

  const actionSearch = (queryInit = {}, type = "") => {
    let paramsSelect = {
      checkIn: checkIn.format(formatDate.firstDay),
      checkOut: checkOut.format(formatDate.firstDay),
      adults: paramsSearch.adults,
      rooms: paramsSearch.rooms,
      children: paramsSearch.children,
    };
    if (!isEmpty(paramsSearch.childrenAges)) {
      paramsSelect = {
        ...paramsSelect,
        childrenAges: paramsSearch.childrenAges.toString(),
      };
    }
    // save last-search-history for storage
    saveLastSearchHistory(paramsSelect, type);
    ///// redirect route
    if (type === "hotel") {
      gtm.addEventGtm(listEventHotel.HotelSearchByName, {
        hotelId: itemLocationSelect?.hotelId || "",
      });
      router.push({
        pathname: routeStatic.HOTEL_DETAIL.href,
        query: {
          ...queryInit,
          ...paramsSelect,
        },
      });
    } else if (type === "location") {
      if (!isEmpty(itemLocationSelect.address)) {
        pushGtm(itemLocationSelect.address);
      } else {
        pushGtm(itemLocationSelect);
      }
      router.push({
        pathname: routeStatic.LISTING_HOTEL.href,
        query: {
          ...queryInit,
          ...paramsSelect,
        },
      });
    } else if (type === "near-hotel") {
      router.push({
        pathname: routeStatic.LISTING_HOTEL.href,
        query: {
          ...queryInit,
          ...paramsSelect,
        },
      });
    }
  };

  const pushGtm = (location = {}) => {
    if (!isEmpty(location)) {
      let addressGtm = {};
      if (!isEmpty(location.districtId)) {
        addressGtm = {
          ...addressGtm,
          districtId: location?.districtId || "",
          districtName: location?.districtName || "",
        };
      }
      if (!isEmpty(location.provinceId)) {
        addressGtm = {
          ...addressGtm,
          provinceId: location?.provinceId || "",
          provinceName: location?.provinceName || location?.name || "",
        };
      }
      gtm.addEventGtm(listEventHotel.HotelSearchBySlug, {
        address: addressGtm,
      });
    }
  };

  const saveLastSearchHistory = (paramsSelect = {}, type = "hotel") => {
    let itemLocationSelectTemp = {};
    if (type === "hotel") {
      itemLocationSelectTemp = {
        name: itemLocationSelect.name,
        type: "HOTEL",
        hotelId: itemLocationSelect.hotelId,
      };
    } else if (type === "location") {
      itemLocationSelectTemp = {
        name: itemLocationSelect.name,
        type: "LOCATION",
        aliasCode: itemLocationSelect.aliasCode,
      };
    } else if (type === "near-hotel") {
      itemLocationSelectTemp = {
        name: "Khách sạn gần bạn",
        type: "near-hotel",
      };
    }
    const lastSearchHistory = {
      ...paramsSelect,
      childrenAges: paramsSearch.childrenAges,
      itemLocationSelect: itemLocationSelectTemp,
    };
    localStorage.setItem(
      LAST_SEARCH_HISTORY,
      JSON.stringify(lastSearchHistory)
    );
  };
  return (
    <Box className={classes.container}>
      <Box
        className={clsx(classes.boxContentItem, classes.searchBoxContent)}
        onClick={toggleDrawer(listTypeModal.MODAL_DESTINATION_SEARCH)}
      >
        <Box display="flex" alignItems="center" width="100%">
          <IconSearch />
          <Box
            display="flex"
            component="span"
            width="100%"
            pl={12 / 8}
            color={
              isEmpty(itemLocationSelect) ? "gray.grayDark7" : "black.black3"
            }
          >
            <Typography
              variant="body1"
              style={{
                fontWeight: !isEmpty(itemLocationSelect.name) ? 600 : "normal",
              }}
            >
              {isEmpty(itemLocationSelect)
                ? listString.IDS_MT_TEXT_PLAHOLDER_SEARCH_HOTEL_MOBILE
                : itemLocationSelect.name}
            </Typography>
          </Box>
        </Box>
        <IconDirection />
      </Box>
      <Box className={clsx(classes.boxContentItem, classes.calendarBoxContent)}>
        <CalendarDialog
          child={
            <Box display="flex" alignItems="center">
              <Box marginLeft={-3 / 8}>
                <IconCalenderSearchHotel />
              </Box>
              <Box pl={1}>
                <ShowDateView startDate={checkIn} endDate={checkOut} />
              </Box>
            </Box>
          }
          type="hotel"
          initStartDate={checkIn}
          initEndDate={checkOut}
          updateStartDate={setCheckIn}
          updateEndDate={setCheckOut}
        />
      </Box>
      <Box
        className={clsx(classes.boxContentItem, classes.guestBoxContent)}
        onClick={toggleDrawer(listTypeModal.MODAL_GUEST_INFO)}
      >
        <IconUser2 className={classes.IconUser2} />
        <Box display="flex" alignItems="center" pl={1}>
          <Typography variant="subtitle1">
            {paramsSearch.rooms}{" "}
            <Box className={classes.infoSearch} component="span">
              {" "}
              phòng
            </Box>
          </Typography>
          <IconDot className={classes.iconDot} />
          <Typography variant="subtitle1">
            {paramsSearch.adults}{" "}
            <Box className={classes.infoSearch} component="span">
              {" "}
              người lớn
            </Box>
          </Typography>
          <IconDot className={classes.iconDot} />
          <Typography variant="subtitle1">
            {paramsSearch.children}{" "}
            <Box className={classes.infoSearch} component="span">
              trẻ em
            </Box>
          </Typography>
        </Box>
      </Box>
      <Box pt={2}>
        <ButtonComponent
          backgroundColor={theme.palette.secondary.main}
          height={48}
          fontSize={16}
          fontWeight={600}
          borderRadius={8}
          handleClick={handleBtnSearch}
        >
          {listString.IDS_MT_TEXT_SEARCH}
        </ButtonComponent>
      </Box>
      <GuestInfoBoxModal
        paramsSearch={paramsSearch}
        open={visibleDrawerType === listTypeModal.MODAL_GUEST_INFO}
        toggleDrawer={toggleDrawer}
        handleOk={handleSelectGuest}
      />
      <DestinationSearchModal
        open={visibleDrawerType === listTypeModal.MODAL_DESTINATION_SEARCH}
        toggleDrawer={toggleDrawer}
        handleSelectedItem={handleSelectedItem}
      />
    </Box>
  );
};

export default HotelSearchBox;
