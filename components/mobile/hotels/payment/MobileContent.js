import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import { Box } from "@material-ui/core";

import {
  listString,
  routeStatic,
  listEventHotel,
  flightPaymentMethodCode,
  LAST_BOOKING_HOTEL,
} from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import {
  getPaymentMethods,
  createPaymentForBooking,
  getBookingInfo,
} from "@api/hotels";
import * as gtm from "@utils/gtm";

import snackbarSetting from "@src/alert/Alert";
import Layout from "@components/layout/mobile/Layout";
import AtmModal from "@components/mobile/hotels/payment/AtmModal";
import InfoPayment from "@components/mobile/hotels/payment/InfoPayment";
import TimeBooking from "@components/mobile/hotels/payment/TimeBooking";
import PaymentMethod from "@components/mobile/hotels/payment/PaymentMethod";

const listTypeModal = {
  MODAL_ATM: "MODAL_ATM",
  MODAL_BT: "MODAL_BT",
};

let lastBookingHotel = {};

const useStyles = makeStyles((theme) => ({
  loadMoreBox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    fontSize: 14,
    minHeight: "calc(100vh - 48px)",
  },
}));

const Payment = ({ hotelDetail = {} }) => {
  const router = useRouter();
  const classes = useStyles();
  const [listPaymentMethod, setListPaymentMethod] = useState([]);
  const [paymentSelected, setPaymentSelect] = useState({});
  const [bankSelected, setBankSelect] = useState({});
  const [bookingDetail, setBookingDetail] = useState({});
  const [bankList, setBankList] = useState([]);
  const [codeMethod, setCodeMethod] = useState("");
  const [visibleDrawerType, setVisibleDrawerType] = useState("");
  const [loadingSubmit, setLoadingSubmit] = useState(false);

  const [fetching, setFetching] = useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const query = router.query || {};

  useEffect(() => {
    lastBookingHotel =
      JSON.parse(localStorage.getItem(LAST_BOOKING_HOTEL)) || {};
    if (!isEmpty(lastBookingHotel)) {
      fetchPaymentMethod();
      fetBookingInfo();
    } else {
      router.push({
        pathname: routeStatic.HOME.href,
      });
    }
  }, []);
  const fetBookingInfo = async () => {
    const dataDTO = {
      id: lastBookingHotel.bookingId,
      orderToken: lastBookingHotel.orderToken,
    };
    try {
      setFetching(true);
      const { data } = await getBookingInfo(dataDTO);
      setBookingDetail(data.data);
      setFetching(false);
    } catch (err) {
      setFetching(false);
    }
  };

  const fetchPaymentMethod = async () => {
    const dataDTO = {
      bookingCode: lastBookingHotel.orderCode,
    };
    try {
      const { data } = await getPaymentMethods(dataDTO);
      setListPaymentMethod(data?.data.items || []);
    } catch (err) {}
  };

  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };
  const handleSelectPayment = (methodItem = {}) => {
    setCodeMethod(methodItem.code);

    gtm.addEventGtm(listEventHotel.HotelAddPaymentInfo);
    if (methodItem.code === flightPaymentMethodCode.ATM) {
      // the ATM/ Tai khoan ngan hang
      setBankList(methodItem.bankList);
      setVisibleDrawerType(listTypeModal.MODAL_ATM);
    } else if (methodItem.code === flightPaymentMethodCode.BANK_TRANSFER_2) {
      // Chuyển khoản ngân hàng
      if (methodItem.bankList.length === 1) {
        setPaymentSelect(methodItem);
        setBankSelect(methodItem.bankList[0]);
      } else {
        setBankList(methodItem.bankList);
        setVisibleDrawerType(listTypeModal.MODAL_BT);
      }
    } else if (
      methodItem.code === flightPaymentMethodCode.VISA_MASTER ||
      methodItem.code === flightPaymentMethodCode.QR_CODE
    ) {
      // Thẻ Visa, Master Card
      setBankSelect({});
      setPaymentSelect(methodItem);
    }
  };

  const handleSelectBank = (bank = {}) => {
    setBankSelect(bank);
    setVisibleDrawerType("");
    const methodResult = listPaymentMethod.find((el) => el.code === codeMethod);
    setPaymentSelect(methodResult);
  };

  const handlePaymentBooking = async () => {
    if (isEmpty(paymentSelected)) {
      enqueueSnackbar(
        "Vui lòng chọn phương thức thanh toán!",
        snackbarSetting((key) => closeSnackbar(key), { color: "error" })
      );
      return null;
    }
    let dataDTO = {
      bookingCode: lastBookingHotel.orderCode,
      paymentMethodId: paymentSelected.id,
    };
    if (!isEmpty(query.promotionCode)) {
      dataDTO = {
        ...dataDTO,
        promotionCode: query.promotionCode,
      };
    }
    if (
      paymentSelected.code === flightPaymentMethodCode.ATM ||
      paymentSelected.code === flightPaymentMethodCode.BANK_TRANSFER_2
    ) {
      dataDTO = {
        ...dataDTO,
        bankId: bankSelected.id,
      };
    }
    try {
      setLoadingSubmit(true);
      const { data } = await createPaymentForBooking(dataDTO);
      if (data.code === 200) {
        if (data.data.isRedirect) {
          window.location.href = data.data.paymentLink;
        }
        // else {
        //   router.push({
        //     pathname: routeStatic.RESULT_PAYMENT_BOOKING.href,
        //     query: {
        //       transactionId: data.data.transactionId,
        //     },
        //   });
        // }
      } else {
        setLoadingSubmit(false);
        data?.message &&
          enqueueSnackbar(
            data?.message,
            snackbarSetting((key) => closeSnackbar(key), { color: "error" })
          );
        // code = 400 het han thanh toan don hang
        if (data.code === 400) {
          router.back();
        }
      }
    } catch (error) {
      setLoadingSubmit(false);
    }
  };
  return (
    <Layout
      isHeaderBack
      isShowTitleInit
      titleHeaderBack={listString.IDS_MT_TEXT_PAYMENT_MENTHODS}
      title={`Thanh toán đơn đặt phòng khách sạn ${hotelDetail?.name} | Mytour.vn đảm bảo giá tốt nhất!`}
      iconOgImage={hotelDetail?.thumbnail?.src}
    >
      {fetching ? (
        <Box className={classes.loadMoreBox}>
          <img
            src="https://staticproxy.mytourcdn.com/0x0/themes/images/mloading.gif"
            className={classes.imgLoading}
          />
        </Box>
      ) : (
        <>
          <InfoPayment item={bookingDetail} />
          <PaymentMethod
            listPaymentMethod={listPaymentMethod}
            bookingDetail={bookingDetail}
            paymentSelected={paymentSelected}
            bankSelected={bankSelected}
            handleSelectPayment={handleSelectPayment}
            handlePaymentBooking={handlePaymentBooking}
            lastBookingHotel={lastBookingHotel}
            loadingSubmit={loadingSubmit}
          />
        </>
      )}
      <AtmModal
        open={
          visibleDrawerType === listTypeModal.MODAL_ATM ||
          visibleDrawerType === listTypeModal.MODAL_BT
        }
        toggleDrawer={toggleDrawer}
        bankList={bankList}
        handleSelectBank={handleSelectBank}
        bankSelected={bankSelected}
        listTypeModal={listTypeModal}
        visibleDrawerType={visibleDrawerType}
        emailCustomer={bookingDetail?.customer?.email}
      />
      <TimeBooking hotelDetail={hotelDetail} />
    </Layout>
  );
};

export default Payment;
