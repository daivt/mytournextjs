import clsx from "clsx";
import { makeStyles } from "@material-ui/styles";
import { Box, Drawer, Grid, IconButton } from "@material-ui/core";

import { listString } from "@utils/constants";
import { IconShieldDone, IconClose } from "@public/icons";

import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  paperAnchorBottom: {
    minHeight: "55vh",
    maxHeight: "80vh",
    borderRadius: "16px 16px 0 0",
  },
  contentModal: {
    padding: 16,
    fontSize: 14,
    color: theme.palette.black.black3,
  },
  divider: {
    height: 1,
    backgroundColor: theme.palette.gray.grayLight22,
    margin: "12px -16px",
  },
  bankItem: {
    borderRadius: 8,
    border: `1px solid ${theme.palette.gray.grayLight23}`,
    height: 52,
  },
  bankItemSelect: {
    border: `2px solid ${theme.palette.blue.blueLight8}`,
  },
  iconBank: {
    width: "100%",
    height: "100%",
    objectFit: "contain",
  },
  desBankTransfer: {
    borderRadius: 8,
    backgroundColor: theme.palette.blue.blueLight11,
    padding: "7px 12px",
    lineHeight: "17px",
  },
}));

const AtmModal = ({
  open = true,
  toggleDrawer = () => () => {},
  handleSelectBank = () => () => {},
  bankList = [],
  listTypeModal = [],
  bankSelected = {},
  visibleDrawerType = "",
  emailCustomer = "",
}) => {
  const classes = useStyles();
  return (
    <>
      <Drawer
        anchor="bottom"
        open={open}
        onClose={toggleDrawer("")}
        classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
      >
        <Box className={classes.contentModal}>
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
          >
            <Box
              component="span"
              fontSize={16}
              fontWeight={600}
              lineHeight="19px"
            >
              {visibleDrawerType === listTypeModal.MODAL_ATM
                ? listString.IDS_MT_TEXT_PAYMENT_METHOD_ATM
                : listString.IDS_MT_TEXT_PAYMENT_METHOD_1}
            </Box>
            <IconButton
              onClick={toggleDrawer("")}
              className={classes.wrapButtonClose}
            >
              <IconClose />
            </IconButton>
          </Box>

          <Box className={classes.divider} />
          {visibleDrawerType === listTypeModal.MODAL_ATM ? (
            <Box display="flex" alignItems="center">
              <IconShieldDone />
              <Box
                component="span"
                color="green.greenLight7"
                lineHeight="17px"
                pl={10 / 8}
              >
                {listString.IDS_MT_TEXT_PAYMENT_METHOD_ATM_DES}
              </Box>
            </Box>
          ) : (
            <Box className={classes.desBankTransfer}>
              {`${listString.IDS_MT_TEXT_BANK_TRANSFER_DES}`}
              <Box pl={2 / 8} component="span" fontWeight={600}>
                {emailCustomer}
              </Box>
            </Box>
          )}

          <Box fontSize={14} fontWeight={600} lineHeight="17px" py={12 / 8}>
            {listString.IDS_MT_TEXT_SELECT_BANK}
          </Box>
          <Grid container spacing={2}>
            {bankList.map((el) => (
              <Grid item xs={4} key={el.id}>
                <Box
                  className={clsx(
                    classes.bankItem,
                    bankSelected.id === el.id ? classes.bankItemSelect : ""
                  )}
                  onClick={() => handleSelectBank(el)}
                >
                  <img src={el.logoUrl} className={classes.iconBank} />
                </Box>
              </Grid>
            ))}
          </Grid>
        </Box>
      </Drawer>
    </>
  );
};
export default AtmModal;
