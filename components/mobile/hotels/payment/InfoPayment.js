import { Box, Grid, makeStyles, Typography } from "@material-ui/core";
import Image from "@src/image/Image";
import { listString } from "@utils/constants";
import moment from "moment";
import clsx from "clsx";
import { isEmpty } from "@utils/helpers";
const useStyles = makeStyles((theme) => ({
  wraper: {
    background: theme.palette.gray.grayLight23,
    padding: 16,
  },
  container: {
    borderRadius: 8,
    background: theme.palette.white.main,
    boxShadow: "0px 8px 8px rgba(0, 0, 0, 0.08)",
  },
  wrapContent: {
    padding: 16,
  },
  wrapBox: {
    display: "flex",
    justifyContent: "space-between",
  },
  imageHotel: {
    width: 88,
    height: 88,
    borderRadius: 8,
  },
  timeCheckIn: {
    fontSize: 12,
    lineHeight: "17px",
    color: theme.palette.black.black3,
    fontWeight: 400,
  },
  wrapTimeCheckIn: {
    display: "flex",
    alignItems: "center",
    color: theme.palette.gray.grayDark8,
    marginTop: 8,
  },
  wrapTimeCheckOut: { marginTop: 4 },
  wrapInfoRoom: {
    color: theme.palette.gray.grayDark8,
    display: "flex",
    flexDirection: "column",
    marginTop: 12,
    position: "relative",
    borderLeft: `1px solid ${theme.palette.gray.grayDark7}`,
  },
  wrapInfoCustomer: {
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    marginTop: 15,
  },
}));

const InfoPayment = ({ item = {} }) => {
  const classes = useStyles();
  let checkIn = moment(item.checkIn, "DD-MM-YYYY");
  let checkOut = moment(item.checkOut, "DD-MM-YYYY");
  const getListBed = (bedTypes = []) => {
    let listBed = "";
    bedTypes.forEach((el, index) => {
      listBed = `${listBed}${index !== 0 ? " hoặc " : ""}${el}`;
    });
    return listBed;
  };
  const roomBookings = !isEmpty(item.roomBookings) ? item.roomBookings[0] : {};
  return (
    <Box className={classes.wraper}>
      <Box className={classes.container}>
        <Box className={classes.wrapContent}>
          <Box className={classes.wrapBox}>
            <Box pr={1}>
              <Typography variant="subtitle1">{item?.hotel?.name}</Typography>
              <Typography variant="body2" className={classes.wrapTimeCheckIn}>
                <Box component="span" pr={2 / 8}>
                  {`${listString.IDS_MT_TEXT_CHECK_IN_ROOM}:`}
                </Box>
                <Typography className={classes.timeCheckIn} component="span">
                  {checkIn
                    ? checkIn
                        .locale("vi_VN")
                        .format("ddd")
                        .replace("T", "Thứ ")
                        .replace("CN", "Chủ Nhật")
                    : "-"}
                  <Box component="span">
                    {", "}
                    {checkIn.format("DD")} Tháng {checkIn.format("MM")}
                  </Box>
                </Typography>
              </Typography>
              <Typography
                variant="body2"
                className={clsx(
                  classes.wrapTimeCheckIn,
                  classes.wrapTimeCheckOut
                )}
              >
                <Box component="span" pr={2 / 8}>
                  {`${listString.IDS_MT_TEXT_CHECK_OUT_ROOM}:`}
                </Box>
                <Typography className={classes.timeCheckIn} component="span">
                  {checkOut
                    ? checkOut
                        .locale("vi_VN")
                        .format("ddd")
                        .replace("T", "Thứ ")
                        .replace("CN", "Chủ Nhật")
                    : "-"}
                  <Box component="span">
                    {", "}
                    {checkOut.format("DD")} Tháng {checkOut.format("MM")}
                  </Box>
                </Typography>
              </Typography>
            </Box>
            <Box>
              <Image
                srcImage={item?.hotel?.thumbnail?.src}
                className={classes.imageHotel}
                borderRadiusProp="8px"
              />
            </Box>
          </Box>

          <Box className={classes.wrapInfoRoom}>
            <Typography
              variant="subtitle2"
              style={{
                marginLeft: "12px",
                color: "#1a202c",
                paddingBottom: "4px",
              }}
            >
              {item?.numRooms}x {roomBookings.name}
            </Typography>
            <Typography
              component="p"
              variant="body2"
              style={{ marginLeft: "12px" }}
            >
              {`${parseInt(item.numAdult)} người`},{" "}
              {getListBed(roomBookings.bedTypes)},{" "}
              {roomBookings.freeBreakfast === true
                ? "bữa ăn sáng miễn phí"
                : "không bao gồm bữa ăn sáng"}
              ,{" "}
              {roomBookings.freeCancellation === true
                ? "miễn phí hoàn hủy"
                : "không hoàn huỷ"}
            </Typography>
          </Box>
          <Box className={classes.wrapInfoCustomer}>
            <Typography
              variant="subtitle2"
              style={{ marginTop: "16px", paddingBottom: "4px" }}
            >
              {item?.customer?.name}
            </Typography>
            <Box color="gray.grayDark8">
              <Typography
                variant="body2"
                component="span"
                style={{ marginTop: "4px" }}
              >
                {item?.customer?.phone}
                {",  "}
              </Typography>
              <Typography variant="body2" component="span">
                {item?.customer?.email}
              </Typography>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
InfoPayment.propTypes = {};
export default InfoPayment;
