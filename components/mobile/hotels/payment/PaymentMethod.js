import { useState, useEffect } from "react";
import moment from "moment";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import * as gtm from "@utils/gtm";
import { getDiscountPromotionCode } from "@api/hotels";
import { isEmpty } from "@utils/helpers";
import {
  listString,
  listEventHotel,
  flightPaymentMethodCode,
  BASE_URL_CONDITION_MYTOUR,
} from "@utils/constants";

import Link from "@src/link/Link";
import ButtonComponent from "@src/button/Button";
import PaymentMethodItem from "@components/mobile/hotels/payment/PaymentMethodItem";

const useStyles = makeStyles((theme) => ({
  container: {
    fontSize: 14,
    color: theme.palette.black.black3,
    padding: 16,
    display: "flex",
    flexDirection: "column",
  },
  boxFotter: {
    opacity: 1,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    zIndex: 1000,
    bottom: 0,
    left: 0,
    position: "fixed",
    color: "#000",
    background: "#fff",
    width: "100%",
    padding: "8px 16px",
    borderTop: "2px solid #EDF2F7",
  },
  linkPolicy: {
    color: theme.palette.blue.blueLight8,
    padding: "0 2px",
    "&:hover": {
      textDecoration: "none",
    },
  },
}));
const PaymentMethod = ({
  listPaymentMethod = [],
  bookingDetail = {},
  paymentSelected = {},
  bankSelected = {},
  handleSelectPayment = () => {},
  handlePaymentBooking = () => {},
  lastBookingHotel = {},
  loadingSubmit = false,
}) => {
  const classes = useStyles();
  const [discount, setDiscount] = useState(0);

  useEffect(() => {
    if (!isEmpty(lastBookingHotel.promotionCode)) {
      fetCheckPromotion(
        lastBookingHotel.promotionCode,
        lastBookingHotel.orderCode
      );
    }
  }, []);

  const fetCheckPromotion = async (promotionCode = "", orderCode = "") => {
    try {
      const { data } = await getDiscountPromotionCode({
        code: promotionCode,
        bookingCode: orderCode,
      });
      if (data.code === 200) {
        setDiscount(data.data.discount);
      }
    } catch (error) {}
  };
  const getNight = () => {
    let checkIn = moment(bookingDetail.checkIn, "DD-MM-YYYY");
    let checkOut = moment(bookingDetail.checkOut, "DD-MM-YYYY");
    if (checkIn && checkOut) {
      return Math.abs(
        moment(checkIn)
          .startOf("day")
          .diff(checkOut.startOf("day"), "days")
      );
    }
    return 1;
  };

  const getFinalPrice = () => {
    let finalPrice = !isEmpty(bookingDetail)
      ? bookingDetail.price * bookingDetail.numRooms * getNight()
      : 0;
    finalPrice = finalPrice - discount;
    if (!isEmpty(paymentSelected)) {
      finalPrice = finalPrice + getPaymentFee();
      if (finalPrice < paymentSelected.minAmount) {
        finalPrice = paymentSelected.minAmount;
      }
    }
    return finalPrice;
  };

  const getPaymentFee = () => {
    return (
      paymentSelected.fixedFee +
      (paymentSelected.percentFee * (paymentSelected.amount - discount)) / 100
    );
  };

  const handlePayment = () => {
    handlePaymentBooking();
    gtm.addEventGtm(listEventHotel.HotelPurchase, {
      amount: getFinalPrice(),
      bookingId: bookingDetail.id,
    });
  };

  const finalPrice = !isEmpty(bookingDetail)
    ? bookingDetail.price * bookingDetail.numRooms * getNight()
    : 0;
  return (
    <Box className={classes.container}>
      <Box component="span" fontSize={16} fontWeight={600} lineHeight="19px">
        {listString.IDS_MT_TEXT_PAYMENT_MENTHODS}
      </Box>
      <Box
        component="span"
        fontSize={12}
        lineHeight="14px"
        color="green.greenLight7"
        pt={4 / 8}
      >
        {listString.IDS_MT_TEXT_DES_PAYMENT_METHOD}
      </Box>
      <Box pt={12 / 8}>
        {listPaymentMethod.map((el) => (
          <PaymentMethodItem
            methodItem={el}
            key={el.id}
            paymentSelected={paymentSelected}
            bankSelected={bankSelected}
            handleSelectPayment={handleSelectPayment}
            finalPrice={finalPrice}
            discountPrice={discount}
          />
        ))}
      </Box>
      <Box
        p="12px 29px 74px"
        textAlign="center"
        fontSize={12}
        lineHeight="14px"
      >
        <Box component="span">
          {listString.IDS_MT_TEXT_PAYMENT_METHOD_3_DES_5}
        </Box>
        <Link
          href={BASE_URL_CONDITION_MYTOUR}
          target="_blank"
          className={classes.linkPolicy}
        >
          {listString.IDS_MT_TEXT_PAYMENT_METHOD_3_DES_6}
        </Link>
        <Box component="span">
          {listString.IDS_MT_TEXT_PAYMENT_METHOD_3_DES_7}
        </Box>
      </Box>
      <Box className={classes.boxFotter}>
        <Box display="flex" flexDirection="column">
          <Box
            component="span"
            fontSize={12}
            lineHeight="14px"
            color="gray.grayDark8"
            pb={2 / 8}
          >
            {listString.IDS_MT_TEXT_TOTAL_PRICE}
          </Box>
          <Box
            component="span"
            fontSize={20}
            lineHeight="24px"
            fontWeight={600}
          >
            {`${
              !isEmpty(bookingDetail.finalPrice)
                ? Math.max(getFinalPrice(), 0).formatMoney()
                : 0
            }đ`}
          </Box>
        </Box>
        <ButtonComponent
          backgroundColor="#FF1284"
          borderRadius={8}
          width="fit-content"
          height={48}
          padding="16px 40px"
          fontSize={16}
          fontWeight={600}
          handleClick={handlePayment}
          // disabled={isEmpty(paymentSelected)}
          loading={loadingSubmit}
        >
          {paymentSelected.code === flightPaymentMethodCode.BANK_TRANSFER
            ? listString.IDS_MT_TEXT_SEND_REQUEST
            : listString.IDS_MT_TEXT_PAYMENT}
        </ButtonComponent>
      </Box>
    </Box>
  );
};

export default PaymentMethod;
