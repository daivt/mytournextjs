import clsx from "clsx";
import { makeStyles } from "@material-ui/styles";
import { Box, Checkbox } from "@material-ui/core";

import {
  flightPaymentMethodCode,
  listIcons,
  listString,
  prefixUrlIcon,
} from "@utils/constants";
import {
  IconMethodAtmCard,
  IconMethodBankTransfer,
  IconQRPay,
} from "@public/icons";
import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  container: {
    padding: "14px 16px",
    borderRadius: 8,
    border: `1px solid ${theme.palette.gray.grayLight23}`,
    marginBottom: 12,
  },
  containerSelected: {
    border: `1px solid ${theme.palette.blue.blueLight8}`,
    backgroundColor: theme.palette.blue.blueLight11,
  },
  icon: {
    borderRadius: 100,
    width: 22,
    height: 22,
    border: "1px solid #CBD5E0",
    backgroundColor: "#fff",
  },
  checkedIconRadio: {
    backgroundColor: "#00B6F3",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: 20,
      height: 20,
      backgroundImage: "url(/icons/icon_checked.svg)",
      backgroundSize: "cover",
      content: '""',
    },
  },
  bankItem: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.gray.grayLight23}`,
    height: 24,
    width: 48,
    backgroundColor: theme.palette.white.main,
  },
  iconBank: {
    width: "100%",
    height: "100%",
    objectFit: "contain",
  },
}));

const getIconMethod = (methodCode = "") => {
  switch (methodCode) {
    case flightPaymentMethodCode.BANK_TRANSFER_2:
      return <IconMethodBankTransfer />;
    case flightPaymentMethodCode.VISA_MASTER:
      return (
        <img
          src={`${prefixUrlIcon}${listIcons.IconMethodVisa}`}
          alt="logo_payment_visa"
          style={{ width: 32, height: 32 }}
        />
      );
    case flightPaymentMethodCode.ATM:
      return <IconMethodAtmCard />;
    case flightPaymentMethodCode.QR_CODE:
      return <IconQRPay />;
    default:
      return <IconMethodBankTransfer />;
  }
};

const PaymentMethodItem = ({
  methodItem = {},
  paymentSelected = {},
  bankSelected = {},
  handleSelectPayment = () => {},
  finalPrice = 0,
  discountPrice = 0,
}) => {
  const classes = useStyles();

  const genContentMethod = (methodCode = "") => {
    switch (methodCode) {
      case flightPaymentMethodCode.VISA_MASTER:
        return (
          <Box display="flex" alignItems="center" pl={44 / 8}>
            <img src="/images/image_visa_master_card.png" />
          </Box>
        );
      case flightPaymentMethodCode.ATM:
      case flightPaymentMethodCode.BANK_TRANSFER_2:
        return (
          <Box display="flex" alignItems="center" pl={44 / 8}>
            <Box className={classes.bankItem}>
              <Image
                srcImage={bankSelected.logoUrl}
                className={classes.iconBank}
              />
            </Box>
            <Box fontWeight={600} lineHeight="17px" pl={1}>
              {bankSelected.name}
            </Box>
          </Box>
        );
      default:
        return null;
    }
  };

  const getPaymentFee = () => {
    return (
      methodItem.fixedFee +
      (methodItem.percentFee * (methodItem.amount - discountPrice)) / 100
    );
  };

  return (
    <Box
      className={clsx(
        classes.container,
        paymentSelected.id === methodItem.id ? classes.containerSelected : ""
      )}
      onClick={() => handleSelectPayment(methodItem)}
    >
      <Box display="flex" alignItems="center" justifyContent="space-between">
        <Box display="flex" alignItems="center">
          {getIconMethod(methodItem.code)}
          <Box component="span" fontSize={16} lineHeight="19px" pl={12 / 8}>
            {methodItem.name}
          </Box>
        </Box>
        <Box>
          <Checkbox
            checked={paymentSelected.id === methodItem.id}
            color="default"
            checkedIcon={
              <span className={clsx(classes.icon, classes.checkedIconRadio)} />
            }
            icon={<span className={classes.icon} />}
          />
        </Box>
      </Box>
      {getPaymentFee() > 0 && (
        <Box
          pl={44 / 8}
          fontSize={12}
          lineHeight="14px"
          color="gray.grayDark7"
          pb={1}
        >{`${
          listString.IDS_MT_TEXT_SUB_FEE
        } ${getPaymentFee().formatMoney()}đ`}</Box>
      )}

      {paymentSelected.id === methodItem.id &&
        genContentMethod(methodItem.code)}
    </Box>
  );
};

export default PaymentMethodItem;
