import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import { Box, Dialog, Typography } from "@material-ui/core";

import { listString, routeStatic } from "@utils/constants";

import ButtonComponent from "@src/button/Button";
import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  timeBlock: {
    width: "100%",
    background: "#FFBC39",
    borderRadius: 8,
    padding: 8,
    textAlign: "center",
    display: "flex",
    justifyContent: "center",
  },
  textItem: {
    display: "flex",
    alignItems: "center",
    fontSize: 14,
    lineHeight: "17px",
  },
  dialogContent: {
    padding: "24px 12px 12px 12px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  buttonGroup: {
    display: "flex",
    width: "100%",
    flexDirection: "column",
    alignItems: "center",
  },
  textTitleDialog: {
    padding: "0 12px",
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
    margin: "24px 0 8px 0",
    textAlign: "center",
  },
  textDesDialog: {
    padding: "0 12px",
    fontSize: 14,
    lineHeight: "22px",
    margin: "0 0 12px 0",
    textAlign: "center",
  },
  linkDetail: {
    textDecoration: "none",
    width: "100%",
    margin: "0 12px 12px",
    "&:hover": { textDecoration: "none" },
  },
  paper: {
    margin: 16,
  },
  iconTimeOut: {
    width: 241,
    height: 210,
  },
}));

const TimeBooking = ({ hotelDetail = {} }) => {
  const classes = useStyles();
  const router = useRouter();
  const [time, setTime] = useState(15 * 60);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (time > 0) setTimeout(() => setTime(time - 1), 1000);
    else setOpen(true); // eslint-disable-next-line
  }, [time]);

  const backToHotelDetail = () => {
    router.push({
      pathname: routeStatic.HOTEL_DETAIL.href,
      query: {
        alias: `${hotelDetail.id}-${hotelDetail?.name?.stringSlug()}.html`,
      },
    });
  };
  const min = (time - (time % 60)) / 60;
  const sec = Number(time - min * 60);
  return (
    <Dialog
      onClose={() => setOpen(false)}
      aria-labelledby="simple-dialog-title"
      open={open}
      maxWidth="lg"
      disableBackdropClick
      classes={{ paper: classes.paper }}
    >
      <Box className={classes.dialogContent}>
        <Image
          srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_time_out.svg"
          className={classes.iconTimeOut}
        />
        <Typography variant="body2" className={classes.textTitleDialog}>
          {listString.IDS_MT_TIME_BOOKING_OUT_TEXT}
        </Typography>
        <Typography variant="body2" className={classes.textDesDialog}>
          {listString.IDS_MT_TIME_BOOKING_OUT_DESCRIPTION}
        </Typography>
        <Box className={classes.buttonGroup}>
          <ButtonComponent
            backgroundColor="#E2E8F0"
            color="#1A202C"
            height={48}
            fontSize={16}
            fontWeight={600}
            borderRadius={8}
            handleClick={backToHotelDetail}
          >
            {listString.IDS_MT_SELECT_OTHER_ROOM}
          </ButtonComponent>
          <ButtonComponent
            backgroundColor="#FF1284"
            color="#FFFFFF"
            height={48}
            fontSize={16}
            fontWeight={600}
            borderRadius={8}
            style={{ margin: "12px" }}
            handleClick={() => {
              router.reload();
            }}
          >
            {listString.IDS_MT_CONTINUE_BOOKING}
          </ButtonComponent>
        </Box>
      </Box>
    </Dialog>
  );
};

export default TimeBooking;
