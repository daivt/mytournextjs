import moment from "moment";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import InfiniteScroll from "react-infinite-scroller";
import { Box, Grid, Avatar, Typography } from "@material-ui/core";

import { useDispatchSystem } from "@contextProvider/ContextProvider";
import { SET_TOP_LOCATION } from "@contextProvider/hotel/ActionTypes";
import { cityList } from "@utils/dataFake";
import utilStyles from "@styles/utilStyles";
import {
  formatDate,
  HOTEL_SERVICE,
  LAT_LONG_USER,
  routeStatic,
  LAST_SEARCH_HISTORY,
  DELAY_TIMEOUT_POLLING,
} from "@utils/constants";
import { getTopHotels } from "@api/homes";
import { getHotelsAvailability } from "@api/hotels";
import { isEmpty, adapterHotelHotDeal } from "@utils/helpers";
import Image from "@src/image/Image";

import Link from "@src/link/Link";
import Layout from "@components/layout/mobile/Layout";
import HotlineBox from "@components/common/card/HotlineBox";
import BannerHotel from "@components/mobile/hotels/BannerHotel";
import HotelCard from "@components/common/card/hotel/HotelCard";
import LoadingMore from "@components/common/loading/LoadingMore";
import WhyBookTicket from "@components/common/card/WhyBookTicket";
import HotelSearchBox from "@components/mobile/hotels/HotelSearchBox";
import AllowGPSModal from "@components/common/modal/hotel/AllowGPSModal";
import ListHotelLastView from "@components/mobile/homes/ListHotelLastView";

const useStyles = makeStyles((theme) => ({
  divider: {
    height: 8,
    width: "100%",
    backgroundColor: theme.palette.gray.grayLight22,
  },
  boxSearchContent: {
    position: "absolute",
    width: "100%",
    marginTop: -20,
    backgroundColor: theme.palette.white.main,
    borderRadius: "16px 16px 0px 0px",
    paddingBottom: 34,
  },
  wrapperLocation: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  iconAvatar: {
    width: 64,
    height: 64,
  },
  nameLocation: {
    textAlign: "center",
    width: "100%",
    display: "-webkit-box",
    WebkitLineClamp: 2,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
    fontSize: 14,
    lineHeight: "17px",
  },
  iconOpenGps: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: theme.palette.gray.grayLight26,
    width: 64,
    height: 64,
    borderRadius: "50%",
  },
  wrapperItem: {
    paddingTop: 16,
  },
  itemHotelCard: {
    paddingBottom: 16,
  },
  linkAvatarLocation: {
    color: theme.palette.black.black3,
    textDecoration: "none !important",
  },
  gps: {
    width: 56,
    height: 56,
  },
  itemHotelCard: {
    paddingBottom: 16,
  },
}));

const pageSize = 20;

const Hotels = ({ topLocation = [] }) => {
  const router = useRouter();
  const classes = useStyles();
  const classesUtils = utilStyles();
  const [visibleDrawerFilter, setVisibleDrawerFilter] = useState(false);
  const dispatch = useDispatchSystem();
  const [paramsSearch, setParamsSearch] = useState({
    rooms: 1,
    adults: 2,
    children: 0,
    childrenAges: [],
  });
  const [checkIn, setCheckIn] = useState(moment());
  const [checkOut, setCheckOut] = useState(moment().add(1, "days"));
  const [itemLocationSelect, setItemLocationSelect] = useState({});

  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(true);
  const [dataHotel, setDataHotel] = useState([]);
  const [totalHotel, setTotalHotel] = useState(0);
  let breakPolling = true;
  const citySelect = cityList.find(
    (el) => el.aliasCode === router.query.slug[0]
  );

  useEffect(() => {
    dispatch({
      type: SET_TOP_LOCATION,
      payload: topLocation,
    });
    updateRecentSearchFromStorage();
    setItemLocationSelect({
      ...citySelect,
      type: "location",
      provinceId: citySelect.id,
    });
    return () => {
      breakPolling = false;
    };
  }, []);

  const updateRecentSearchFromStorage = () => {
    const lastSearchHistory =
      JSON.parse(localStorage.getItem(LAST_SEARCH_HISTORY)) || {};
    setItemLocationSelect(lastSearchHistory?.itemLocationSelect || {});
    setParamsSearch({
      rooms: lastSearchHistory?.rooms || 1,
      adults: lastSearchHistory?.adults || 2,
      children: lastSearchHistory?.children || 0,
      childrenAges: lastSearchHistory?.childrenAges || [],
    });
    if (!isEmpty(lastSearchHistory)) {
      const todayText = moment().format(formatDate.firstYear);
      const checkIn = moment(
        lastSearchHistory.checkIn,
        formatDate.firstDay
      ).format(formatDate.firstYear);
      const checkOut = moment(
        lastSearchHistory.checkOut,
        formatDate.firstDay
      ).format(formatDate.firstYear);
      if (moment(checkIn).isAfter(todayText)) {
        setCheckIn(moment(lastSearchHistory.checkIn, formatDate.firstDay));
      }
      if (moment(checkOut).isAfter(todayText)) {
        setCheckOut(moment(lastSearchHistory.checkOut, formatDate.firstDay));
      }
    }
  };

  const handleOkSelectGuest = (values) => {
    setParamsSearch({
      ...paramsSearch,
      rooms: values.roomSearch,
      adults: values.adultSearch,
      children: values.childSearch,
      childrenAges: values.childrenAges,
    });
  };
  const setItemLocationSelected = (item) => {
    setItemLocationSelect(item);
  };

  const toggleDrawer = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerFilter(open);
  };

  const handleAllowGps = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const positionTemp = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          };
          // save storage
          localStorage.setItem(LAT_LONG_USER, JSON.stringify(positionTemp));
          const queryInit = {
            slug: [`ksgb`, `khach-san-gan-ban.html`],
            latitude: positionTemp.latitude,
            longitude: positionTemp.longitude,
          };
          router.push({
            pathname: routeStatic.LISTING_HOTEL.href,
            query: {
              ...queryInit,
            },
          });
        },
        () => {}
      );
    }
    setVisibleDrawerFilter(false);
  };

  let paramsSelect = {
    checkIn: checkIn.format(formatDate.firstDay),
    checkOut: checkOut.format(formatDate.firstDay),
    adults: paramsSearch.adults,
    rooms: paramsSearch.rooms,
    children: paramsSearch.children,
    childrenAges: paramsSearch.childrenAges,
  };

  const loadFunc = async () => {
    const pageTemp = page;
    try {
      setLoading(false);
      const { data } = await getTopHotels({
        ...paramsSelect,
        province: citySelect.id,
        page: pageTemp,
        size: pageSize,
      });
      if (data.code === 200) {
        const dataConcat = [...dataHotel, ...data?.data?.items];
        fetPriceHotelHotDeal(data?.data?.items, dataConcat);
        setPage(pageTemp + 1);
        setDataHotel(dataConcat);
        setTotalHotel(data?.data.total);
        if (pageTemp * pageSize < data?.data.total) {
          setLoading(true);
        }
      }
    } catch (error) {}
  };

  const fetPriceHotelHotDeal = (dataFetPrice = [], dataConcat = []) => {
    if (!isEmpty(dataFetPrice)) {
      const hotelHotDealIds = dataFetPrice.map((el) => el.id);
      fetPriceHotel(dataConcat, hotelHotDealIds);
    }
  };

  const fetPriceHotel = async (dataConcat = [], ids = []) => {
    let polling = true;
    try {
      const dataDto = {
        ...paramsSelect,
        hotelIds: ids,
        searchType: "hotel",
      };
      while (polling && breakPolling) {
        const { data } = await getHotelsAvailability(dataDto);
        if (data.code === 200) {
          mergerPriceToData(dataConcat, data?.data.items);
          if (data.data.completed) {
            polling = false;
            break;
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      polling = false;
    }
  };

  const mergerPriceToData = (dataConcat = [], dataPoling = []) => {
    let dataResult = [...dataConcat];
    dataPoling.forEach((el) => {
      dataResult.forEach((item, index) => {
        if (item.id === el.id) {
          dataResult[index] = {
            ...item,
            promotionInfo: el.basePromotionInfo,
            price: el.basePrice,
            hiddenPrice: el.hiddenPrice,
          };
        }
      });
    });
    setDataHotel(dataResult);
  };

  return (
    <Layout
      isHeader={false}
      title="Đặt phòng khách sạn hạng sang giá rẻ - giá tốt nhất trên Mytour.vn | Đặt sát ngày giá càng tốt."
    >
      <Box>
        <BannerHotel />
        <Box className={classes.boxSearchContent}>
          <HotelSearchBox
            paramsSearch={paramsSearch}
            checkIn={checkIn}
            checkOut={checkOut}
            itemLocationSelect={itemLocationSelect}
            handleOkSelectGuest={handleOkSelectGuest}
            setItemLocationSelected={setItemLocationSelected}
            setCheckIn={setCheckIn}
            setCheckOut={setCheckOut}
          />
          <Box className={classes.divider}></Box>
          <Box pl={1} className={classesUtils.scrollViewHorizontal} pt={3}>
            <Box px={1} className={classes.wrapperLocation}>
              <Box
                className={classes.iconOpenGps}
                mb={1}
                onClick={toggleDrawer(true)}
              >
                <Image
                  srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_open_gps.svg"
                  className={classes.gps}
                />
              </Box>
              <Typography variant="caption" component="span">
                Gần tôi
              </Typography>
            </Box>
            {topLocation.map((el, index) => (
              <Link
                href={{
                  pathname: routeStatic.LISTING_HOTEL.href,
                  query: {
                    slug: [
                      `${el.aliasCode}`,
                      `khach-san-tai-${el?.name.stringSlug()}.html`,
                    ],
                  },
                }}
                className={classes.linkAvatarLocation}
                key={el.provinceId}
              >
                <Box px={1} className={classes.wrapperLocation}>
                  <Avatar
                    title={`Khạch sạn tại ${el.name}`}
                    alt={`Khạch sạn tại ${el.name}`}
                    src={el.thumb}
                    className={classes.iconAvatar}
                  />
                  <Box mt={1} className={classes.nameLocation}>
                    {el?.name}
                  </Box>
                </Box>
              </Link>
            ))}
          </Box>
          <ListHotelLastView />
          <Box px={2}>
            <WhyBookTicket source={HOTEL_SERVICE} />
            <HotlineBox />
            {totalHotel !== 0 && (
              <>
                <Typography variant="h5">
                  {`Khách sạn tại ${citySelect.name}`}
                </Typography>
              </>
            )}
            <InfiniteScroll
              initialLoad
              pageStart={page}
              loadMore={loadFunc}
              hasMore={loading}
            >
              <Grid container spacing={2} className={classes.wrapperItem}>
                {adapterHotelHotDeal(dataHotel).map((el) => (
                  <Grid
                    item
                    lg={6}
                    md={6}
                    sm={6}
                    xs={6}
                    key={el.id}
                    className={classes.itemHotelCard}
                  >
                    <HotelCard
                      item={el}
                      isCountBooking={false}
                      isShowPriceInImage
                    />
                  </Grid>
                ))}
              </Grid>
              {!loading && page * pageSize < totalHotel && <LoadingMore />}
            </InfiniteScroll>
          </Box>
        </Box>
      </Box>
      <AllowGPSModal
        open={visibleDrawerFilter}
        toggleDrawer={toggleDrawer}
        handleAllowGps={handleAllowGps}
      />
    </Layout>
  );
};

export default Hotels;
