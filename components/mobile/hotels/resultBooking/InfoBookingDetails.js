import clsx from "clsx";
import { Box, Grid, makeStyles, Typography } from "@material-ui/core";
import {
  IconUser2,
  IconMoonlight,
  IconBed,
  IconInfo,
  IconCancelPolicy,
  IconBreakFast,
  IconStar,
} from "@public/icons";
import Image from "@src/image/Image";
import { listString } from "@utils/constants";
import { getMessageHotelReviews, isEmpty } from "@utils/helpers";
import moment from "moment";
import { useState } from "react";

import PolicyRoomlModal from "@components/mobile/hotels/details/PolicyRoomlModal";

const useStyles = makeStyles((theme) => ({
  boerHeaer: {
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  wrapContainer: {
    margin: 16,
  },
  textBooking: {
    width: "calc(100% - 48px)",
    textAlign: "center",
  },
  hotelName: {
    display: "flex",
    flexDirection: "column",
  },
  rating: { margin: "4px 0 8px 0", color: theme.palette.yellow.yellowLight3 },
  wrapInfoHotel: {
    color: theme.palette.black.black3,
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
    marginTop: 16,
    paddingBottom: 16,
  },
  freeCancelPolicy: {
    stroke: theme.palette.green.greenLight7,
  },
  iconBreakFast: {
    stroke: theme.palette.gray.grayDark8,
  },
  textCancelPolicy: {
    fontSize: 14,
    lineHeight: "17px",
    fontWeight: "normal",
    color: theme.palette.gray.grayDark8,
    marginLeft: 6,
  },
  textFreeBreakFast: {
    color: theme.palette.green.greenLight7,
  },
  imageHotel: {
    width: 88,
    height: 88,
    borderRadius: 8,
  },
  wrapBoxRight: {
    display: "flex",
    justifyContent: "flex-end",
    margin: "0 0 16px 0",
  },
  wrapReviewPoint: {
    fontSize: 14,
    lineHeight: "17px",
    display: "flex",
    alignContent: "center",
    whiteSpace: "nowrap",
  },
  point: {
    color: theme.palette.white.main,
    padding: "1px 4px 1px 4px",
    background: theme.palette.blue.blueLight8,
    width: 30,
    borderRadius: 4,
    marginRight: 4,
    display: "flex",
    alignItems: "center",
  },
  messageReview: {
    color: theme.palette.black.black3,
    marginRight: 4,
  },
  countReview: {
    color: theme.palette.gray.grayDark7,
  },
  wrapTimeBook: {
    display: "flex",
    alignItems: "center",
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  timeBooking: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark8,
    marginTop: 16,
  },
  nightDay: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 34,
    height: 28,
    borderRadius: "100px",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    backgroundColor: theme.palette.white.main,
  },
  rightBookingTime: {
    textAlign: "right",
  },
  timeCheckIn: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.black.black3,
    fontWeight: 600,
    marginTop: 2,
  },
  iconUser2: {
    width: 16,
    height: 16,
  },
  numberOfGuest: { display: "flex", alignItems: "center", margin: "8px 0" },
  textInfoRoom: {
    color: theme.palette.black.black3,
    marginLeft: 8,
  },
  colorFreeBreak: {
    color: theme.palette.green.greenLight7,
  },
  colorIconInfo: {
    stroke: theme.palette.green.greenLight7,
  },
  wrapInfoRoom: {
    marginTop: 16,
  },
  wrapUser: {
    display: "flex",
    alignItems: "center",
    marginTop: 9,
  },
  iconUser2: {
    width: 16,
    height: 16,
    marginRight: 6,
  },
  wrapRatingStar: {
    marginTop: 5,
    marginBottom: 7,
    display: "flex",
  },
  rankingHotel: {
    stroke: theme.palette.yellow.yellowLight3,
    fill: theme.palette.yellow.yellowLight3,
    width: 12,
    height: 12,
    marginRight: 3,
  },
}));
const listTypeModal = {
  MODAL_CANCEL_POLICY: "MODAL_CANCEL_POLICY",
};
const arrStar = [1, 2, 3, 4, 5];

const InfoBookingDetails = ({ hotelDetail = {}, bookingDetails = {} }) => {
  const classes = useStyles();
  let checkIn = moment(bookingDetails.checkIn, "DD-MM-YYYY");
  let checkOut = moment(bookingDetails.checkOut, "DD-MM-YYYY");
  let hasBookingDetails = !isEmpty(bookingDetails.roomBookings)
    ? bookingDetails.roomBookings[0]
    : {};
  const getListBed = (bedTypes = []) => {
    let listBed = "";
    bedTypes.forEach((el, index) => {
      listBed = `${listBed}${index !== 0 ? " hoặc " : ""}${el}`;
    });
    return listBed;
  };
  const [visibleDrawerType, setVisibleDrawerType] = useState("");
  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };

  const getTextShortCancelPolicies = () => {
    if (hasBookingDetails.freeCancellation) {
      return "Miễn phí hoàn hủy";
    } else if (hasBookingDetails.refundable) {
      return "Có thể hoàn hủy";
    } else {
      return "Không hoàn hủy";
    }
  };

  return (
    <Box className={classes.boerHeaer}>
      <Box className={classes.wrapContainer}>
        <Grid container className={classes.wrapInfoHotel}>
          <Grid item xs={7} className={classes.wrapBoxLeft}>
            <Box className={classes.wrapReview}>
              <Typography variant="subtitle1" className={classes.hotelName}>
                {hotelDetail?.name}
              </Typography>
              <Box className={classes.wrapRatingStar}>
                {arrStar.map((el, index) => {
                  if (el > hotelDetail?.starNumber) return null;
                  return (
                    <IconStar
                      key={index}
                      className={`svgFillAll ${classes.rankingHotel}`}
                    />
                  );
                })}
              </Box>
              <Box className={classes.wrapReviewPoint}>
                <Typography variant="subtitle2" className={classes.point}>
                  {2 * hotelDetail?.rating?.hotel?.rating === 10
                    ? 10
                    : (2 * hotelDetail?.rating?.hotel?.rating).toFixed(1)}
                </Typography>
                <Typography className={classes.messageReview}>
                  {getMessageHotelReviews(hotelDetail?.rating?.hotel?.rating)}
                </Typography>
                {hotelDetail?.rating?.hotel?.count > 0 && (
                  <Typography className={classes.countReview}>
                    {`(${hotelDetail?.rating?.hotel?.count} đánh giá)`}
                  </Typography>
                )}
              </Box>
            </Box>
          </Grid>
          <Grid item xs={5} className={classes.wrapBoxRight}>
            <Image
              srcImage={hotelDetail?.thumbnail?.src}
              className={classes.imageHotel}
              borderRadiusProp="8px"
            />
          </Grid>
        </Grid>
        <Grid container className={classes.wrapTimeBook}>
          <Grid item xs={5} className={classes.leftBookingTime}>
            <Typography className={classes.timeBooking}>
              {listString.IDS_MT_TEXT_CHECK_IN_ROOM}
            </Typography>
            <Typography className={classes.timeCheckIn}>
              {checkIn
                ? checkIn
                    .locale("vi_VN")
                    .format("ddd")
                    .replace("T", "Thứ ")
                    .replace("CN", "Chủ Nhật")
                : "-"}
              <Box component="span">
                {", "}
                {checkIn.format("DD")} Tháng {checkIn.format("MM")}
              </Box>
            </Typography>
            <Typography
              className={classes.timeCheckIn}
              style={{
                color: "#4A5568",
                margin: "2px 0 16px 0",
                fontWeight: "400",
              }}
            >
              {hotelDetail.checkInTime}
            </Typography>
          </Grid>
          <Grid item xs={2} className={classes.centerBookingTime}>
            <Box className={classes.nightDay}>
              <Box component="span" fontSize={11} lineHeight="13px" pr={2 / 8}>
                {checkIn && checkOut
                  ? Math.abs(
                      moment(checkIn)
                        .startOf("day")
                        .diff(checkOut.startOf("day"), "days")
                    )
                  : "-"}
              </Box>
              <IconMoonlight />
            </Box>
          </Grid>
          <Grid item xs={5} className={classes.rightBookingTime}>
            <Typography className={classes.timeBooking}>
              {listString.IDS_MT_TEXT_CHECK_OUT_ROOM}
            </Typography>

            <Typography className={classes.timeCheckIn}>
              {checkOut
                ? checkOut
                    .locale("vi_VN")
                    .format("ddd")
                    .replace("T", "Thứ ")
                    .replace("CN", "Chủ Nhật")
                : "-"}
              <Box component="span">
                {", "}
                {checkOut.format("DD")} Tháng {checkOut.format("MM")}
              </Box>
            </Typography>
            <Typography
              className={classes.timeCheckIn}
              style={{
                color: "#4A5568",
                margin: "2px 0 16px 0",
                fontWeight: "400",
              }}
            >
              {hotelDetail.checkOutTime}
            </Typography>
          </Grid>
        </Grid>

        <Box className={classes.wrapInfoRoom}>
          <Typography variant="subtitle1" style={{ lineHeight: "17px" }}>
            {`${bookingDetails?.numRooms}x`} {hasBookingDetails?.name}
          </Typography>
          <Box className={classes.wrapUser}>
            <Box>
              {" "}
              <IconUser2 className={classes.iconUser2} />{" "}
            </Box>
            <Typography component="span" variant="body2">
              {" "}
              {`${parseInt(bookingDetails?.numAdult) +
                parseInt(bookingDetails?.numChildren)} người`}
            </Typography>
          </Box>
          <Box className={classes.wrapUser}>
            <Box style={{ width: 16, height: 16, marginRight: 6 }}>
              <IconBed className={classes.iconUser2} />
            </Box>
            <Typography component="span" variant="body2">
              {" "}
              {getListBed(hasBookingDetails?.bedTypes)}
            </Typography>
          </Box>
        </Box>
        <Box className={classes.contentRate}>
          {hasBookingDetails?.freeBreakfast ? (
            <Box mt={1} style={{ display: "flex", alignItems: "center" }}>
              <IconBreakFast />
              <Typography
                className={clsx(
                  classes.textCancelPolicy,
                  classes.textFreeBreakFast
                )}
              >
                {listString.IDS_MT_TEXT_ROOM_FREE_BREAK_FAST}
              </Typography>
            </Box>
          ) : (
            <Box mt={1} style={{ display: "flex", alignItems: "center" }}>
              <IconBreakFast
                className={`svgFillAll ${classes.iconBreakFast}`}
              />
              <Typography className={classes.textCancelPolicy}>
                {listString.IDS_MT_TEXT_ROOM_NO_FREE_BREAK_FAST}
              </Typography>
            </Box>
          )}
          {hasBookingDetails?.freeCancellation ? (
            <>
              <Box
                mt={1}
                style={{ display: "flex", alignItems: "center" }}
                onClick={toggleDrawer(listTypeModal.MODAL_CANCEL_POLICY)}
              >
                <IconCancelPolicy
                  className={`svgFillAll ${classes.freeCancelPolicy}`}
                />
                <Typography
                  className={classes.textCancelPolicy}
                  style={{ color: "#48BB78" }}
                >
                  {listString.IDS_MT_TEXT_FREE_CANCEL}
                </Typography>
                <IconInfo className={`svgFillAll ${classes.infoActive}`} />
              </Box>
              <Typography variant="body2" style={{ marginLeft: "22px" }}>
                {!isEmpty(hasBookingDetails.hotelCancellationPolicies) &&
                  hasBookingDetails.hotelCancellationPolicies.map((el, i) => (
                    <Box component="span" color="gray.grayDark8" key={i}>
                      {el.price === el.refundAmount &&
                        `Trước ${el.dateTo.split(" ")[0]}`}
                    </Box>
                  ))}
              </Typography>
            </>
          ) : (
            <Box
              mt={1}
              style={{ display: "flex", alignItems: "center" }}
              onClick={() => {
                if (hasBookingDetails.refundable) {
                  setVisibleDrawerType(listTypeModal.MODAL_CANCEL_POLICY);
                }
              }}
            >
              <IconCancelPolicy />
              <Typography className={classes.textCancelPolicy}>
                {getTextShortCancelPolicies()}
              </Typography>
              {hasBookingDetails.refundable && (
                <IconInfo className={`svgFillAll ${classes.info}`} />
              )}
            </Box>
          )}
        </Box>
      </Box>
      <PolicyRoomlModal
        open={visibleDrawerType === listTypeModal.MODAL_CANCEL_POLICY}
        toggleDrawer={toggleDrawer}
        item={hasBookingDetails}
      />
    </Box>
  );
};
InfoBookingDetails.propTypes = {};
export default InfoBookingDetails;
