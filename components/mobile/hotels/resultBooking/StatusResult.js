import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import { isEmpty } from "@utils/helpers";
import { listString, flightPaymentMethodCode } from "@utils/constants";

import {
  IconPaymentFailedHotel,
  IconPaymentPendingHotel,
  IconPaymentSettledHotel,
} from "@public/icons";

import InfoPaymentTransferBooking from "@components/mobile/hotels/resultBooking/InfoPaymentTransferBooking";

const useStyles = makeStyles((theme) => ({}));

const StatusResult = ({
  status = "",
  LIST_STATUS = {},
  bookingDetail = {},
  transactionInfo = {},
  transferInfo = {},
  transferOptions = [],
}) => {
  const classes = useStyles();
  const contentSettled = (
    <Box>
      <Box display="flex" flexDirection="column" alignItems="center">
        <IconPaymentSettledHotel />
        <Box
          component="span"
          fontWeight={600}
          fontSize={16}
          lineHeight="19px"
          p="12px 0 8px"
        >
          {transactionInfo?.paymentMethodInfo?.code === "PR"
            ? "Gửi yêu cầu đặt phòng thành công"
            : listString.IDS_MT_TEXT_BOOKING_SUCCESS}
        </Box>
        <Box
          component="span"
          lineHeight="17px"
          p="2px 4px"
          color="green.greenLight7"
          borderRadius={4}
          border="1px solid #48BB78"
        >
          {bookingDetail.partnerBookingCode
            ? `Mã đặt phòng: ${bookingDetail.partnerBookingCode}`
            : `Mã đơn hàng: ${bookingDetail.orderCode}`}
        </Box>
      </Box>
      {transactionInfo?.paymentMethodInfo?.code === "PR" ? (
        <Box
          pt={12 / 8}
          lineHeight="22px"
        >{`Mytour sẽ chủ động liên hệ với quý khách trong vòng 30 phút để xác nhận phòng trống và hướng dẫn thanh toán.`}</Box>
      ) : (
        <Box display="flex" flexDirection="column" pt={12 / 8}>
          <Box
            component="span"
            lineHeight="22px"
          >{`Xác nhận đặt phòng đã được gửi tới email`}</Box>
          <Box component="span" lineHeight="22px" fontWeight={600}>
            {bookingDetail?.customer?.email}
          </Box>
          <Box
            component="span"
            lineHeight="22px"
          >{`Chúc quý khách có 1 chuyến đi vui vẻ.`}</Box>
        </Box>
      )}
    </Box>
  );
  const contentPending = (
    <Box>
      <Box display="flex" flexDirection="column" alignItems="center">
        {status === LIST_STATUS.PENDING ? (
          <IconPaymentPendingHotel />
        ) : (
          <IconPaymentFailedHotel />
        )}
        <Box
          component="span"
          fontWeight={600}
          fontSize={16}
          lineHeight="19px"
          p="12px 0 8px"
        >
          {status === LIST_STATUS.PENDING
            ? listString.IDS_MT_TEXT_BOOKING_PENDING
            : listString.IDS_MT_TEXT_BOOKING_FAILED}
        </Box>
        <Box
          component="span"
          lineHeight="17px"
          p="2px 4px"
          color="green.greenLight7"
          borderRadius={4}
          border="1px solid #48BB78"
        >
          {`Mã đặt phòng: ${transactionInfo.bookingCode}`}
        </Box>
      </Box>

      <Box bgcolor="rgb(237, 137, 54, 0.1)" m="12px -16px">
        <Box display="flex" flexDirection="column" p="12px 16px">
          <Box
            component="span"
            lineHeight="22px"
            color="orange.main"
            pb={4 / 8}
          >
            {listString.IDS_TEXT_FLIGHT_BOOKING_RESULT_DESC_FAIL}
          </Box>
          {status === LIST_STATUS.PENDING && (
            <Box component="span" lineHeight="22px">
              {listString.IDS_MT_TEXT_PAYMENT_TA_DES_1}
            </Box>
          )}
        </Box>
      </Box>
      {status === LIST_STATUS.PENDING && (
        <Box component="span">
          <Box
            component="span"
            lineHeight="22px"
          >{`Hướng dẫn thanh toán sẽ được gửi tới email `}</Box>
          <Box component="span" lineHeight="22px" fontWeight={600}>
            {`${bookingDetail?.customer?.email}. `}
          </Box>
        </Box>
      )}
    </Box>
  );
  const getStatusContent = () => {
    if (
      !isEmpty(transactionInfo.paymentMethodInfo) &&
      transactionInfo?.paymentMethodInfo.code ===
        flightPaymentMethodCode.BANK_TRANSFER_2
    ) {
      return (
        <InfoPaymentTransferBooking
          transactionInfo={transactionInfo}
          transferInfo={transferInfo}
          transferOptions={transferOptions}
          bookingDetail={bookingDetail}
        />
      );
    }
    switch (status) {
      case LIST_STATUS.PENDING:
      case LIST_STATUS.FAILED:
        return contentPending;
      case LIST_STATUS.SETTLED:
        return contentSettled;
      default:
        return contentSettled;
    }
  };
  return <Box pb={12 / 8}>{getStatusContent()}</Box>;
};

export default StatusResult;
