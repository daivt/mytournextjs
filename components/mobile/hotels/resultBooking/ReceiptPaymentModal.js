import clsx from "clsx";
import Typography from "@material-ui/core/Typography";
import { Box, Drawer, makeStyles } from "@material-ui/core";

import ButtonComponent from "@src/button/Button";
import { IconRemovePhoto, IconUpload } from "@public/icons";
import { listString } from "@utils/constants";

import { isEmpty } from "@utils/helpers";

import React, { useEffect, useState } from "react";
import Dropzone, { useDropzone } from "react-dropzone";

const useStyles = makeStyles((theme) => ({
  paperAnchorBottom: {
    borderRadius: "16px 16px 0 0",
  },
  mainDrawerFilter: {
    width: "100vw",
    padding: "0 16px",
  },
  warpHeader: {
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  swipeDown: {
    height: 5,
    width: 42,
    display: "flex",
    justifyContent: "center",
    background: theme.palette.gray.grayLight23,
    margin: "8px auto 14px auto",
    borderRadius: 100,
  },
  wrapContent: { marginTop: 12 },
  wrapUpload: {
    height: 52,
    border: `1px dashed ${theme.palette.gray.grayLight25}`,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    margin: "12px 0 64px 0",
  },
  nonWrapUpload: {
    display: "none",
  },
  wrapBtn: {
    margin: "8px 16px",
  },
  boxFotter: {
    opacity: 1,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    zIndex: 1000,
    bottom: 0,
    left: 0,
    position: "relative",
    color: "#000",
    background: "#fff",
    width: "100%",
    borderTop: "2px solid #EDF2F7",
  },
}));
const thumbsContainer = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  margin: "16px 0 22px 0",
};

const thumbInner = {
  display: "flex",
  minWidth: 0,
  overflow: "hidden",
  position: "relative",
  marginRight: 10,
};

const img = {
  display: "block",
  width: "94px",
  height: "94px",
  borderRadius: "8px",
};
const wrapIconRemove = {
  background: "#000000",
  opacity: 0.7,
  width: 24,
  height: 24,
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  borderRadius: 100,
  position: "absolute",
  top: 6,
  right: 6,
};
const removePhoto = {
  stroke: "white",
};
const ReceiptPaymentModal = ({ open = true, toggleDrawer = () => {} }) => {
  const classes = useStyles();
  const [files, setFiles] = useState([]);
  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/*",
    onDrop: (acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );
    },
  });
  const removeFile = (file) => () => {
    const newFiles = [...files];
    newFiles.splice(newFiles.indexOf(file), 1);
    setFiles(newFiles);
  };
  const thumbs = files.map((file) => (
    <div style={thumbInner}>
      <div style={wrapIconRemove}>
        <IconRemovePhoto style={removePhoto} onClick={removeFile(file)} />
      </div>
      <img src={file.preview} style={img} />
    </div>
  ));

  useEffect(
    () => () => {
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [files]
  );
  return (
    <Drawer
      anchor="bottom"
      open={open}
      onClose={toggleDrawer("")}
      classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
    >
      <Box className={classes.swipeDown} />
      <Box className={classes.warpHeader}>
        <Typography
          variant="subtitle1"
          style={{ margin: "16px 0px 11px 16px" }}
        >
          {listString.IDS_MT_RECEIPT_PAYMENT}
        </Typography>
      </Box>
      <Box className={classes.mainDrawerFilter}>
        <Box className={classes.wrapContent}>
          <Typography variant="subtitle2" style={{ fontWeight: "400" }}>
            {listString.IDS_MT_RECEIPT_PAYMENT_DESC}
          </Typography>
          {isEmpty(files) && (
            <Box {...getRootProps({ className: "dropzone" })}>
              <Box className={classes.wrapUpload}>
                <input {...getInputProps()} />
                <Box>
                  <IconUpload />{" "}
                </Box>
                <Typography
                  variant="subtitle2"
                  component="span"
                  color="primary"
                >
                  {listString.IDS_MT_CHOOSE_PHOTO}
                </Typography>
              </Box>
            </Box>
          )}
          <aside style={thumbsContainer}>{thumbs} </aside>
        </Box>
      </Box>
      <Box className={classes.boxFotter}>
        <ButtonComponent
          className={classes.wrapBtn}
          color="#fff"
          backgroundColor="#FF1284"
          borderRadius={8}
          height={48}
          padding="16px 40px"
          fontSize={16}
          fontWeight={600}
        >
          {listString.IDS_MT_SEND_PHOTO}
        </ButtonComponent>
      </Box>
    </Drawer>
  );
};

export default ReceiptPaymentModal;
