import clsx from "clsx";
import moment from "moment";
import {
  Box,
  Divider,
  Drawer,
  Grid,
  IconButton,
  makeStyles,
  Typography,
} from "@material-ui/core";

import { isEmpty } from "@utils/helpers";
import { listString } from "@utils/constants";
import { IconClose, IconDot } from "@public/icons";

import InfoBookingDetails from "@components/mobile/hotels/resultBooking/InfoBookingDetails";
const useStyles = makeStyles((theme) => ({
  wrapButtonClose: { padding: 0 },
  wraperHeader: {
    padding: "12px 16px",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    minHeight: 48,
    display: "flex",
    alignItems: "center",
  },
  wrapHeaderDraw: {
    display: "flex",
    alignItems: "center",
    minHeight: 55,
    width: "100%",
  },
  textFilter: {
    textAlign: "center",
    width: "calc(100% - 40px)",
  },
  paperAnchorBottom: {
    borderRadius: "16px 16px 0 0",
    maxHeight: "98%",
  },
  wrapUse: {
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    position: "fixed",
    bottom: 0,
  },
  wrapBtn: {
    margin: "7px 16px",
  },
  wrapCustomer: {
    margin: 16,
    fontWeight: 600,
    fontSize: 16,
    color: theme.palette.black.black3,
  },
  infoContact: {
    lineHeight: "19px",
  },
  userName: {
    marginTop: 8,
    lineHeight: "17px",
  },
  usePhone: {
    marginTop: 4,
    fontWweight: 400,
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark8,
    display: "flex",
    alignItems: "center",
  },
  divider: {
    height: 6,
    color: theme.palette.gray.grayLight22,
  },
  wrapInvoice: {
    margin: 16,
    fontSize: 14,
    color: theme.palette.black.black3,
  },
  exportInvoice: {
    marginTop: 8,
    lineHeight: "17px",
    fontWeight: 600,
    fontSize: 16,
  },
  wrapBoxLeft: {
    fontWeight: "normal",
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark8,
  },

  wrapBoxRight: {
    fontWeight: 400,
    fontSize: 14,
    lineHeight: "17px",
    textAlign: "right",
    color: theme.palette.black.black3,
  },
  invoice: {
    marginTop: 12,
    justifyContent: "space-between",
    display: "flex",
  },
  wrapContainerTop: {
    marginTop: 12,
  },

  // ========= Start style of info details price ========= //
  itemPrice: {
    display: "flex",
    justifyContent: "space-between",
    lineHeight: "17px",
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
    padding: "14px 0 12px",
    position: "relative",
    fontSize: 14,
  },
  iconArrowRight: {
    transform: "rotate(270deg)",
    stroke: theme.palette.blue.blueLight8,
  },
  textUppercase: {
    textTransform: "uppercase",
  },
  totalPrice: {
    borderBottom: "none",
  },
  boxFotter: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    zIndex: 1000,
    bottom: 0,
    left: 0,
    position: "fixed",
    color: "#000",
    background: "#fff",
    width: "100%",
    padding: "8px 16px",
    borderTop: "2px solid #EDF2F7",
  },
  wrapPromo: {
    height: 24,
    whiteSpace: "nowrap",
    display: "flex",
    alignItems: "center",
  },
  promo: {
    border: `1px solid ${theme.palette.gray.grayLight24}`,
    padding: "4px 6px",
    marginLeft: 8,
    borderRadius: 4,
  },
  textPromo: {
    fontSize: 14,
    lineHeight: "17px",
    display: "flex",
  },
  wrapDetailsPrice: {
    margin: 16,
  },
  requestSpecial: {
    display: "flex",
    flexDirection: "column",
  },
  includedVat: {
    fontSize: 12,
    lineHeight: "14px",
    color: theme.palette.gray.grayDark7,
    fontWeight: 400,
    marginTop: 2,
    paddingBottom: 14,
  },
  // ========= End style of info details price ========= //
}));

const DetaisResultBookingModal = ({
  open = true,
  hotelDetail = {},
  bookingDetails = {},
  toggleDrawer = () => {},
}) => {
  const classes = useStyles();

  const roomBookings = !isEmpty(bookingDetails.roomBookings)
    ? bookingDetails.roomBookings
    : [];
  const roomBooking = !isEmpty(roomBookings) ? roomBookings[0] : {};

  const getNight = () => {
    let checkIn = moment(bookingDetails.checkIn, "DD-MM-YYYY");
    let checkOut = moment(bookingDetails.checkOut, "DD-MM-YYYY");
    if (checkIn && checkOut) {
      return Math.abs(
        moment(checkIn)
          .startOf("day")
          .diff(checkOut.startOf("day"), "days")
      );
    }
    return 1;
  };
  // tong gia chua app ma khuyen mai
  const totalPrice = () => {
    if (bookingDetails.numRooms && bookingDetails.price && getNight()) {
      return bookingDetails.numRooms * getNight() * bookingDetails.price;
    } else {
      return 0;
    }
  };

  // tong gia cac dem
  const nightPrice = () => {
    if (bookingDetails.numRooms && bookingDetails.basePrice && getNight()) {
      return bookingDetails.numRooms * getNight() * bookingDetails.basePrice;
    } else {
      return 0;
    }
  };

  // giam gia makup
  const getDiscount = () => {
    return roomBookings
      .map((el) => Math.abs(el.totalDiscount))
      .reduce((a, b) => a + b, 0);
  };

  // thue va phi dich vu khach san

  const vatPrice = () => {
    return totalPrice() - nightPrice() + getDiscount() - totalSurcharge();
  };

  // phu phi
  const totalSurcharge = () => {
    return roomBookings
      .map((el) => el.totalSurcharge)
      .reduce((a, b) => a + b, 0);
  };

  return (
    <>
      <Drawer
        anchor="bottom"
        open={open}
        onClose={toggleDrawer("")}
        classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
      >
        <Box className={classes.wraperHeader}>
          <Box className={classes.wrapHeaderDraw}>
            <IconButton
              onClick={toggleDrawer("")}
              className={classes.wrapButtonClose}
            >
              <IconClose />
            </IconButton>
            <Typography variant="subtitle1" className={classes.textFilter}>
              {listString.IDS_MT_TEXT_BOOKING_DETAILS}
            </Typography>
          </Box>
        </Box>
        <InfoBookingDetails
          hotelDetail={hotelDetail}
          bookingDetails={bookingDetails}
        />
        <Divider className={classes.divider} />
        <Box className={classes.wrapCustomer}>
          <Box className={classes.infoContact}>
            {listString.IDS_MT_TEXT_INFO_CONTACT}
          </Box>
          <Box className={classes.userName}>
            {bookingDetails?.customer?.name}
          </Box>
          <Box className={classes.usePhone}>
            {bookingDetails?.customer?.phone}{" "}
            <Box mx={1}>
              {" "}
              <IconDot />
            </Box>{" "}
            {bookingDetails?.customer?.email}
          </Box>
        </Box>
        <Divider className={classes.divider} />
        <Box className={classes.wrapInvoice}>
          {(roomBooking.isHighFloor || roomBooking.noSmoking) && (
            <Box className={classes.requestSpecial}>
              <Box fontWeight={600} fontSize={16} lineHeight="19px" pb={1}>
                {listString.IDS_MT_TEXT_REQUEST_SPECIAL}
              </Box>
              {roomBooking.isHighFloor && (
                <Box display="flex" alignItems="center">
                  <IconDot />
                  <Box component="span" lineHeight="24px" pl={1}>
                    {listString.IDS_MT_ROOM_HIGH_LEVEL}
                  </Box>
                </Box>
              )}
              {roomBooking.noSmoking && (
                <Box display="flex" alignItems="center">
                  <IconDot />
                  <Box component="span" lineHeight="24px" pl={1}>
                    {listString.IDS_MT_NO_SMOKE_ROOM}
                  </Box>
                </Box>
              )}
            </Box>
          )}

          {!isEmpty(roomBooking.specialRequest) && (
            <Box className={classes.userName}>
              <Box
                fontWeight={600}
                fontSize={16}
                lineHeight="19px"
                component="span"
              >
                {`${listString.IDS_MT_TEXT_BOOKING_REQUEST_PRIVATE}:`}
              </Box>
              <Box component="span" pl={2 / 8}>
                {roomBooking.specialRequest}
              </Box>
            </Box>
          )}

          {!isEmpty(bookingDetails?.invoiceInfo?.taxNumber) && (
            <Box className={classes.exportInvoice}>
              {listString.IDS_MT_TEXT_INVOICE}:
              <Box>
                <Grid container className={classes.wrapContainerTop}>
                  <Grid item xs={4}>
                    <Box className={classes.wrapBoxLeft}>
                      {listString.IDS_MT_TEXT_COMPANY_NAME}{" "}
                    </Box>
                  </Grid>

                  <Grid item xs={8}>
                    <Box className={classes.wrapBoxRight}>
                      {bookingDetails?.invoiceInfo?.companyName}
                    </Box>
                  </Grid>
                </Grid>

                <Grid container className={classes.wrapContainerTop}>
                  <Grid item xs={4}>
                    <Box className={classes.wrapBoxLeft}>
                      {listString.IDS_MT_TEXT_CODE_INVOICE}{" "}
                    </Box>
                  </Grid>

                  <Grid item xs={8}>
                    <Box className={classes.wrapBoxRight}>
                      {bookingDetails?.invoiceInfo?.taxNumber}
                    </Box>
                  </Grid>
                </Grid>

                <Grid container className={classes.wrapContainerTop}>
                  <Grid item xs={4}>
                    <Box className={classes.wrapBoxLeft}>
                      {listString.IDS_MT_TEXT_ADDRESS_COMPANY}{" "}
                    </Box>
                  </Grid>

                  <Grid item xs={8}>
                    <Box className={classes.wrapBoxRight}>
                      {bookingDetails?.invoiceInfo?.companyAddress}
                    </Box>
                  </Grid>
                </Grid>

                <Grid container className={classes.wrapContainerTop}>
                  <Grid item xs={5}>
                    <Box className={classes.wrapBoxLeft}>
                      {listString.IDS_MT_TEXT_ADDRESS_INVOICE}
                    </Box>
                  </Grid>

                  <Grid item xs={7}>
                    <Box className={classes.wrapBoxRight}>
                      {bookingDetails?.invoiceInfo?.recipientAddress}
                    </Box>
                  </Grid>
                </Grid>

                <Grid container className={classes.wrapContainerTop}>
                  <Grid item xs={5}>
                    <Box className={classes.wrapBoxLeft}>
                      {listString.IDS_TEXT_PLACEHOLDER_INVOICE_CUSTOMER_NAME}
                    </Box>
                  </Grid>

                  <Grid item xs={7}>
                    <Box className={classes.wrapBoxRight}>
                      {bookingDetails?.invoiceInfo?.recipientName}
                    </Box>
                  </Grid>
                </Grid>

                <Grid container className={classes.wrapContainerTop}>
                  <Grid item xs={5}>
                    <Box className={classes.wrapBoxLeft}>
                      {listString.IDS_MT_TEXT_PHONE_NUMBER}
                    </Box>
                  </Grid>

                  <Grid item xs={7}>
                    <Box className={classes.wrapBoxRight}>
                      {bookingDetails?.invoiceInfo?.recipientPhone}
                    </Box>
                  </Grid>
                </Grid>

                <Grid container className={classes.wrapContainerTop}>
                  <Grid item xs={5}>
                    <Box className={classes.wrapBoxLeft}>
                      {listString.IDS_MT_TEXT_EMAIL}
                    </Box>
                  </Grid>

                  <Grid item xs={7}>
                    <Box className={classes.wrapBoxRight}>
                      {bookingDetails?.invoiceInfo?.recipientEmail}
                    </Box>
                  </Grid>
                </Grid>
              </Box>
            </Box>
          )}
        </Box>
        <Divider className={classes.divider} />

        <Box className={classes.wrapDetailsPrice}>
          <Box
            component="span"
            color="black.black3"
            lineHeight="19px"
            fontWeight={600}
            fontSize={16}
            pb={12 / 8}
          >
            {listString.IDS_MT_TEXT_PRICE_DETAIL}
          </Box>

          <Box className={classes.itemPrice}>
            <Box component="span">{`${
              bookingDetails?.numRooms
            } phòng x ${getNight()} đêm`}</Box>
            <Box component="span">{`${nightPrice().formatMoney()}đ`}</Box>
          </Box>

          {totalSurcharge() > 0 && (
            <Box className={classes.itemPrice}>
              <Box component="span">{`Phụ phí`}</Box>
              {`${totalSurcharge().formatMoney()}đ`}
            </Box>
          )}

          {getDiscount() > 0 && (
            <Box className={classes.itemPrice} color="green.greenLight7">
              <Box component="span">{`Chúng tôi khớp giá, giảm thêm`}</Box>
              {`-${getDiscount().formatMoney()}đ`}
            </Box>
          )}

          {vatPrice() > 2 && (
            <Box className={classes.itemPrice}>
              <Box component="span">{`${
                bookingDetails.includedVAT ? "Thuế và phí " : "Phí "
              }dịch vụ khách sạn`}</Box>
              {`${vatPrice().formatMoney()}đ`}
            </Box>
          )}

          {!isEmpty(bookingDetails.paymentMethodFee) &&
            bookingDetails.paymentMethodFee > 0 && (
              <Box className={classes.itemPrice}>
                <Box component="span">{`Phụ phí thanh toán`}</Box>
                <Box component="span">{`${bookingDetails?.paymentMethodFee?.formatMoney()}đ`}</Box>
              </Box>
            )}

          <Box className={classes.itemPrice}>
            <Box component="span" className={classes.wrapPromo}>
              {listString.IDS_TEXT_VOUCHER}{" "}
              {!isEmpty(bookingDetails.promotionCode) && (
                <Box component="span" className={classes.promo}>
                  <Typography
                    component="span"
                    className={classes.textPromo}
                    style={{
                      color: "#1A202C",
                    }}
                  >
                    {bookingDetails?.promotionCode?.toUpperCase()}
                  </Typography>
                </Box>
              )}
            </Box>
            <Box display="flex" alignItems="center" color="blue.blueLight8">
              <Typography
                component="span"
                className={classes.textPromo}
                style={{
                  color: "#48BB78",
                }}
              >
                {`${
                  bookingDetails?.discount > 0 ? "-" : ""
                }${bookingDetails?.discount?.formatMoney()}đ`}
              </Typography>
            </Box>
          </Box>

          <Box
            className={clsx(classes.itemPrice, classes.totalPrice)}
            fontWeight={600}
          >
            <Box component="span">{listString.IDS_MT_TEXT_TOTAL_PRICE}</Box>
            <Box component="span" fontSize={18}>
              {`${bookingDetails?.finalPrice?.formatMoney()}đ`}
            </Box>
          </Box>
          <Box className={classes.includedVat}>
            {bookingDetails.includedVAT
              ? "Đã bao gồm thuế, phí, VAT"
              : "Giá chưa bao gồm VAT"}
          </Box>
        </Box>
      </Drawer>
    </>
  );
};

export default DetaisResultBookingModal;
