import ItemRatesRoom from "@components/mobile/hotels/details/ItemRatesRoom";
import { Box, Grid, makeStyles, Typography } from "@material-ui/core";
import Rating from "@material-ui/lab/Rating";
import { IconMoonlight, IconArrowDownToggle, IconStar } from "@public/icons";
import Image from "@src/image/Image";
import { listString } from "@utils/constants";
import { getMessageHotelReviews, isEmpty } from "@utils/helpers";
import moment from "moment";
import { useRouter } from "next/router";
import { useState } from "react";
import DetaisResultBookingModal from "@components/mobile/hotels/resultBooking/DetaisResultBookingModal";

const useStyles = makeStyles((theme) => ({
  wrapContainer: {
    margin: "16px 0",
  },
  textBooking: {
    width: "calc(100% - 48px)",
    textAlign: "center",
  },
  hotelName: {
    display: "flex",
    flexDirection: "column",
  },
  rating: { margin: "4px 0 8px 0", color: theme.palette.yellow.yellowLight3 },
  wrapInfoHotel: {
    color: theme.palette.black.black3,
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
    marginTop: 16,
  },
  imageHotel: {
    width: 88,
    height: 88,
    borderRadius: 8,
  },
  wrapBoxRight: {
    display: "flex",
    justifyContent: "flex-end",
    margin: "0 0 16px 0",
  },
  wrapReviewPoint: {
    fontSize: 14,
    lineHeight: "17px",
    display: "flex",
    alignContent: "center",
    whiteSpace: "nowrap",
  },
  point: {
    color: theme.palette.white.main,
    padding: "1px 4px 1px 4px",
    background: theme.palette.blue.blueLight8,
    width: 30,
    borderRadius: 4,
    marginRight: 4,
    display: "flex",
    alignItems: "center",
  },
  messageReview: {
    color: theme.palette.black.black3,
    marginRight: 4,
  },
  countReview: {
    color: theme.palette.gray.grayDark7,
  },
  wrapTimeBook: {
    display: "flex",
    alignItems: "center",
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  timeBooking: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark8,
    marginTop: 16,
  },
  nightDay: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 34,
    height: 28,
    borderRadius: "100px",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    backgroundColor: theme.palette.white.main,
  },
  rightBookingTime: {
    textAlign: "right",
  },
  timeCheckIn: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.black.black3,
    fontWeight: 600,
  },
  iconUser2: {
    width: 16,
    height: 16,
  },
  numberOfGuest: { display: "flex", alignItems: "center", margin: "8px 0" },
  textInfoRoom: {
    color: theme.palette.black.black3,
    marginLeft: 8,
  },
  colorFreeBreak: {
    color: theme.palette.green.greenLight7,
  },
  colorIconInfo: {
    stroke: theme.palette.green.greenLight7,
  },
  wrapInfoRoom: {
    marginTop: 16,
  },
  wrapUser: {
    display: "flex",
    alignItems: "center",
    marginTop: 9,
  },
  iconUser2: {
    width: 16,
    height: 16,
    marginRight: 6,
  },
  wrapHeader: {
    marginTop: 30,
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
  },
  wrapDetails: {
    display: "flex",
    justifyContent: "center",
    fontSize: 14,
    lineHeight: "17px",
    fontWeight: "normal",
    color: theme.palette.blue.blueLight8,
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    alignItems: "center",
  },
  iconArrow: {
    transform: "rotate(270deg)",
    stroke: theme.palette.blue.blueLight8,
    marginTop: 12,
    marginLeft: 9,
  },
  wrapRatingStar: {
    display: "flex",
    margin: "6px 0",
  },
  rankingHotel: {
    stroke: theme.palette.yellow.yellowLight3,
    fill: theme.palette.yellow.yellowLight3,
    width: 16,
    height: 16,
    marginRight: 3,
  },
}));
const arrStar = [1, 2, 3, 4, 5];
const listTypeModal = {
  MODAL_DETAILS_RESULT_BOOKING: "MODAL_DETAILS_RESULT_BOOKING",
};
const InfoHotel = ({ bookingDetail = {}, roomRate = {} }) => {
  const router = useRouter();
  const classes = useStyles();
  const [visibleDrawerType, setVisibleDrawerType] = useState("");

  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };
  const checkIn = moment(bookingDetail.checkIn, "DD-MM-YYYY");
  const checkOut = moment(bookingDetail.checkOut, "DD-MM-YYYY");

  return (
    <Box className={classes.wrapContainer}>
      <Grid container className={classes.wrapInfoHotel}>
        <Grid item xs={7} className={classes.wrapBoxLeft}>
          <Box className={classes.wrapReview}>
            <Typography variant="subtitle1" className={classes.hotelName}>
              {bookingDetail?.hotel?.name}
            </Typography>
            <Box className={classes.wrapRatingStar}>
              {arrStar.map((el, index) => {
                if (el > bookingDetail?.hotel?.starNumber) return null;
                return (
                  <IconStar
                    key={index}
                    className={`svgFillAll ${classes.rankingHotel}`}
                  />
                );
              })}
            </Box>
            <Box className={classes.wrapReviewPoint}>
              <Typography variant="subtitle2" className={classes.point}>
                {2 * bookingDetail?.hotel?.rating?.hotel?.rating === 10
                  ? 10
                  : (2 * bookingDetail?.hotel?.rating?.hotel?.rating).toFixed(
                      1
                    )}
              </Typography>
              <Typography className={classes.messageReview}>
                {getMessageHotelReviews(
                  bookingDetail?.hotel?.rating?.hotel?.rating
                )}
              </Typography>
              {bookingDetail?.hotel?.rating?.hotel?.count > 0 && (
                <Typography className={classes.countReview}>
                  {`(${bookingDetail?.hotel?.rating?.hotel?.count} đánh giá)`}
                </Typography>
              )}
            </Box>
          </Box>
        </Grid>
        <Grid item xs={5} className={classes.wrapBoxRight}>
          <Image
            srcImage={bookingDetail?.hotel?.thumbnail?.src}
            className={classes.imageHotel}
            borderRadiusProp="8px"
          />
        </Grid>
      </Grid>
      <Grid container className={classes.wrapTimeBook}>
        <Grid item xs={5} className={classes.leftBookingTime}>
          <Typography className={classes.timeBooking}>
            {listString.IDS_MT_TEXT_CHECK_IN_ROOM}
          </Typography>
          <Typography className={classes.timeCheckIn}>
            {checkIn
              ? checkIn
                  .locale("vi_VN")
                  .format("ddd")
                  .replace("T", "Thứ ")
                  .replace("CN", "Chủ Nhật")
              : "-"}
            <Box component="span">
              {", "}
              {checkIn.format("DD")} Tháng {checkIn.format("MM")}
            </Box>
          </Typography>
          <Typography
            className={classes.timeCheckIn}
            style={{
              color: "#4A5568",
              margin: "2px 0 16px 0",
              fontWeight: "400",
            }}
          >
            {bookingDetail?.hotel?.checkInTime}
          </Typography>
        </Grid>
        <Grid item xs={2} className={classes.centerBookingTime}>
          <Box className={classes.nightDay}>
            <Box component="span" fontSize={11} lineHeight="13px" pr={2 / 8}>
              {checkIn && checkOut
                ? Math.abs(
                    moment(checkIn)
                      .startOf("day")
                      .diff(checkOut.startOf("day"), "days")
                  )
                : "-"}
            </Box>

            <IconMoonlight />
          </Box>
        </Grid>
        <Grid item xs={5} className={classes.rightBookingTime}>
          <Typography className={classes.timeBooking}>
            {listString.IDS_MT_TEXT_CHECK_OUT_ROOM}
          </Typography>
          <Typography className={classes.timeCheckIn}>
            {checkOut
              ? checkOut
                  .locale("vi_VN")
                  .format("ddd")
                  .replace("T", "Thứ ")
                  .replace("CN", "Chủ Nhật")
              : "-"}
            <Box component="span">
              {", "}
              {checkOut.format("DD")} Tháng {checkOut.format("MM")}
            </Box>
          </Typography>
          <Typography
            className={classes.timeCheckIn}
            style={{
              color: "#4A5568",
              margin: "2px 0 16px 0",
              fontWeight: "400",
            }}
          >
            {bookingDetail?.hotel?.checkOutTime}
          </Typography>
        </Grid>
      </Grid>
      <Box
        className={classes.wrapDetails}
        onClick={toggleDrawer(listTypeModal.MODAL_DETAILS_RESULT_BOOKING)}
      >
        <Box marginTop={12 / 8}>{listString.IDS_MT_TEXT_VIEW_DETAIL} </Box>
        <Box>
          <IconArrowDownToggle className={`svgFillAll ${classes.iconArrow}`} />
        </Box>
      </Box>
      <DetaisResultBookingModal
        open={visibleDrawerType === listTypeModal.MODAL_DETAILS_RESULT_BOOKING}
        toggleDrawer={toggleDrawer}
        hotelDetail={bookingDetail.hotel}
        bookingDetails={bookingDetail}
        isDrawer
      />
    </Box>
  );
};
InfoHotel.propTypes = {};
export default InfoHotel;
