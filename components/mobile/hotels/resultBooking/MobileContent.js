import { Box } from "@material-ui/core";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";

import {
  listString,
  routeStatic,
  listEventHotel,
  LAST_BOOKING_HOTEL,
  LAST_BOOKING_HOTEL_INFO,
  flightPaymentMethodCode,
} from "@utils/constants";
import * as gtm from "@utils/gtm";
import { isEmpty } from "@utils/helpers";
import { useDispatchSystem } from "@contextProvider/ContextProvider";
import { checkingPaymentStatus, getBookingInfo } from "@api/hotels";
import { SET_VISIBLE_FETCHING } from "@contextProvider/system/ActionTypes";

import ButtonComponent from "@src/button/Button";
import Layout from "@components/layout/mobile/Layout";
import HotlineBox from "@components/common/card/HotlineBox";
import InfoHotel from "@components/mobile/hotels/resultBooking/InfoHotel";
import StatusResult from "@components/mobile/hotels/resultBooking/StatusResult";
import ReceiptPaymentModal from "@components/mobile/hotels/resultBooking/ReceiptPaymentModal";

let timeIntervalPaymentStatus = null;
const TIME_CALL_API_STATUS = 1000;
const listTypeModal = {
  MODAL_RECEIPT_PAYMENT: "MODAL_RECEIPT_PAYMENT",
};
const useStyles = makeStyles((theme) => ({
  container: {
    fontSize: 14,
    color: theme.palette.black.black3,
    padding: "42px 16px 38px",
  },
  divider: {
    height: 6,
    backgroundColor: theme.palette.gray.grayLight22,
    margin: "0 -16px",
  },
  boxHotline: {
    margin: "16px 0 40px 0",
  },
  boxFotter: {
    position: "fixed",
    opacity: 1,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    zIndex: 1000,
    bottom: 0,
    left: 0,
    color: "#000",
    background: "#fff",
    width: "100%",
    borderTop: "2px solid #EDF2F7",
  },
  containerReceipt: {
    margin: "32px 0px 16px",
    borderRadius: 8,
  },
  iconLeft: {
    marginRight: 12,
    display: "flex",
    alignItems: "center",
  },
  title: {
    paddingBottom: 6,
  },
  desc: {
    fontWeight: 400,
  },
  wrapBtn: {
    margin: "8px 16px",
  },
}));

const LIST_STATUS = {
  PENDING: "PENDING",
  SETTLED: "SETTLED",
  FAILED: "FAILED",
};
const MobileContent = ({}) => {
  const classes = useStyles();
  const router = useRouter();
  const dispatch = useDispatchSystem();
  const [status, setStatus] = useState(LIST_STATUS.FAILED);
  const [bookingDetail, setBookingDetail] = useState({});
  const [transactionInfo, setTransactionInfo] = useState({});
  const [transferInfo, setTransferInfo] = useState({});
  const [transferOptions, setTransferOptions] = useState([]);
  const [count, setCount] = useState(0);
  const [visibleDrawer, setVisibleDrawer] = useState("");

  const toggleDrawer = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawer(open);
  };
  useEffect(() => {
    fetPaymentStatus();
    timeIntervalPaymentStatus = setInterval(() => {
      fetPaymentStatus();
    }, TIME_CALL_API_STATUS);
    return () => {
      clearInterval(timeIntervalPaymentStatus);
    };
  }, []);

  const fetPaymentStatus = async () => {
    try {
      if (count === 0) {
        dispatch({
          type: SET_VISIBLE_FETCHING,
          payload: true,
        });
      }
      const { data } = await checkingPaymentStatus({
        transactionId: router.query.transactionId,
        gatewayParams: router.asPath.split("?")[1],
      });
      setCount(1);
      const transactionInfo = data?.data?.transactionInfo || {};
      setTransactionInfo(transactionInfo);
      if (
        transactionInfo?.paymentMethodInfo.code ===
        flightPaymentMethodCode.BANK_TRANSFER_2
      ) {
        setTransferInfo(data?.data?.transferInfo || {});
        setTransferOptions(data?.data?.transferOptions || {});
        localStorage.removeItem(LAST_BOOKING_HOTEL);
        localStorage.removeItem(LAST_BOOKING_HOTEL_INFO);
      }
      if (
        transactionInfo.status.toUpperCase() === LIST_STATUS.SETTLED ||
        transactionInfo.status.toUpperCase() === LIST_STATUS.FAILED ||
        transactionInfo?.paymentMethodInfo.code ===
          flightPaymentMethodCode.BANK_TRANSFER_2
      ) {
        setStatus(transactionInfo.status.toUpperCase());
        clearInterval(timeIntervalPaymentStatus);
        const paramsInit = {
          hashBookingId: transactionInfo.hashBookingId,
        };
        fetchBookingDetail(paramsInit);
        localStorage.removeItem(LAST_BOOKING_HOTEL);
        localStorage.removeItem(LAST_BOOKING_HOTEL_INFO);
        if (transactionInfo.status.toUpperCase() === LIST_STATUS.FAILED) {
          gtm.addEventGtm(listEventHotel.HotelPurchaseFailed);
        } else {
          gtm.addEventGtm(listEventHotel.HotelPurchaseCompleted, {
            amount: transactionInfo?.amount,
            bookingId: transactionInfo?.bookingCode?.substring(1),
          });
        }
      }
      dispatch({
        type: SET_VISIBLE_FETCHING,
        payload: false,
      });
    } catch (error) {
      dispatch({
        type: SET_VISIBLE_FETCHING,
        payload: false,
      });
    }
  };

  const fetchBookingDetail = async (paramsInit = {}) => {
    try {
      const { data } = await getBookingInfo(paramsInit);
      if (data.code === 200) {
        setBookingDetail(data.data || {});
      }
    } catch (err) {}
  };

  return (
    <Layout
      title={`Đặt phòng khách sạn ${
        !isEmpty(bookingDetail?.hotel) ? bookingDetail?.hotel?.name : ""
      } thành công | Mytour.vn đảm bảo giá tốt nhất!`}
    >
      {count === 0 ? (
        ""
      ) : (
        <Box position="relative">
          <Box className={classes.container}>
            <StatusResult
              status={status}
              LIST_STATUS={LIST_STATUS}
              transactionInfo={transactionInfo}
              transferInfo={transferInfo}
              transferOptions={transferOptions}
              bookingDetail={bookingDetail}
            />
            <Box className={classes.divider} />
            <InfoHotel bookingDetail={bookingDetail} />
            <ReceiptPaymentModal
              open={visibleDrawer === listTypeModal.MODAL_RECEIPT_PAYMENT}
              toggleDrawer={toggleDrawer}
            />
            <Box className={classes.divider} />
            <HotlineBox className={classes.boxHotline} />
          </Box>
          <Box className={classes.boxFotter}>
            <ButtonComponent
              className={classes.wrapBtn}
              color="#fff"
              backgroundColor="#FF1284"
              borderRadius={8}
              height={48}
              padding="16px 40px"
              fontSize={16}
              fontWeight={600}
              handleClick={() => {
                router.push({
                  pathname: routeStatic.HOME.href,
                });
              }}
            >
              {listString.IDS_MT_TEXT_NOT_FOUND_403_DES_3}
            </ButtonComponent>
          </Box>
        </Box>
      )}
    </Layout>
  );
};

export default MobileContent;
