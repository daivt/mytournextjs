import clsx from "clsx";
import { useRouter } from "next/router";
import { makeStyles } from "@material-ui/styles";
import { useRef, useState, useEffect } from "react";
import { Box, Typography, IconButton } from "@material-ui/core";

import { cityList } from "@utils/dataFake";
import { listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";

import Image from "@src/image/Image";
import { IconBack } from "@public/icons";

const useStyles = makeStyles((theme) => ({
  container: {
    position: "relative",
  },
  bannerSearchBox: {
    position: "relative",
    height: 142,
  },
  overlayBanner: {
    position: "absolute",
    top: 0,
    width: "100%",
    height: "100%",
    background: "linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, #000000 100%)",
    opacity: 0.6,
  },
  headerSearchBox: {
    position: "absolute",
    zIndex: 3,
    top: 0,
    padding: "0 4px",
    transition: "all 0.4s ease-in-out",
  },
  iconBack: {
    stroke: theme.palette.white.main,
  },
  iconBackSticky: {
    stroke: theme.palette.black.main,
  },
  titleHeaderSearchBox: {
    display: "flex",
    flexDirection: "column",
    position: "absolute",
    bottom: 36,
    left: 16,
    color: theme.palette.white.main,
  },
  desTitleBanner: {
    opacity: 0.8,
    marginTop: 4,
    lineHeight: "16px",
  },
  topBanner: {
    height: 142,
    width: "100%",
  },
  sticky: {
    height: 48,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    zIndex: 1000,
    left: 0,
    position: "fixed",
    transition: "all 0.4s ease-in-out",
    background: "#fff",
    width: "100%",
    padding: "8px 16px",
    borderBottom: "2px solid #EDF2F7",
  },
  btnBackSticky: {
    position: "absolute",
    left: 0,
  },
}));
const BannerHotel = () => {
  const ref = useRef(null);
  const [sticky, setSticky] = useState(false);
  const router = useRouter();
  const classes = useStyles();

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", () => handleScroll);
    };
  }, []);

  const handleScroll = () => {
    if (ref.current) {
      if (ref.current.getBoundingClientRect().top <= -142) {
        setSticky(true);
      } else {
        setSticky(false);
      }
    }
  };

  const handleBackRoute = () => {
    router.back();
  };

  let citySelect = {};
  if (!isEmpty(router?.query?.slug)) {
    citySelect = cityList.find((el) => el.aliasCode === router?.query?.slug[0]);
  }

  return (
    <Box className={classes.container} ref={ref}>
      <Box className={classes.bannerSearchBox}>
        <Image
          srcImage="https://storage.googleapis.com/tripi-assets/mytour/banner/background_banner_hotel.jpg"
          className={classes.topBanner}
        />
        <Box className={classes.overlayBanner}></Box>
        <Box
          className={clsx(
            classes.headerSearchBox,
            sticky ? classes.sticky : ""
          )}
        >
          <IconButton
            onClick={handleBackRoute}
            className={sticky ? classes.btnBackSticky : ""}
          >
            <IconBack
              className={`svgFillAll ${
                sticky ? classes.iconBackSticky : classes.iconBack
              }`}
            />
          </IconButton>
          {sticky && (
            <Box
              component="span"
              fontSize={16}
              fontWeight={600}
              lineHeight="19px"
            >
              {listString.IDS_MT_TEXT_HOTEL}
            </Box>
          )}
        </Box>
        <Box className={classes.titleHeaderSearchBox}>
          <Typography variant="h4">
            {isEmpty(citySelect)
              ? listString.IDS_MT_TEXT_HOTEL
              : `Khách sạn tại ${citySelect.name}`}
          </Typography>
          <Typography variant="caption" className={classes.desTitleBanner}>
            {listString.IDS_TEXT_DES_HOTEL_BANNER_MOBILE}
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default BannerHotel;
