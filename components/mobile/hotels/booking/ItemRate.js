import clsx from "clsx";
import { useState } from "react";
import { Box, makeStyles, Typography } from "@material-ui/core";

import { listString } from "@utils/constants";
import {
  IconInfo,
  IconUser2,
  IconCheck,
  IconBed,
  IconBreakFast,
  IconCancelPolicy,
  IconDot,
  IconBoxGif,
} from "@public/icons";
import { isEmpty } from "@utils/helpers";

import PolicyRoomlModal from "@components/mobile/hotels/details/PolicyRoomlModal";

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
    display: "flex",
    flexDirection: "column",
  },
  wrapUser: {
    display: "flex",
    alignItems: "center",
    marginTop: 8,
  },
  iconUser2: {
    width: 16,
    height: 16,
    marginRight: 6,
  },
  iconBreakFast: {
    stroke: theme.palette.gray.grayDark8,
  },
  iconFreeBreakFast: {
    stroke: theme.palette.green.greenLight7,
  },
  freeCancelPolicy: {
    stroke: theme.palette.green.greenLight7,
  },
  info: {
    marginLeft: 4,
    stroke: theme.palette.gray.grayDark8,
  },
  textCancelPolicy: {
    fontSize: 12,
    lineHeight: "14px",
    fontWeight: "normal",
    color: theme.palette.black.black3,
    marginLeft: 6,
  },
  textFreeBreakFast: {
    color: theme.palette.green.greenLight7,
  },
  signal: {
    display: "flex",
    alignItems: "center",
    backgroundColor: "rgba(255,18,132,0.1)",
    borderRadius: 8,
    padding: "10px 0 10px 12px",
    marginTop: 6,
  },
  iconCheck: {
    stroke: theme.palette.pink.main,
  },
  wrapGiftBox: {
    marginTop: 4,
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    background: theme.palette.green.greenLight8,
    borderRadius: 8,
    marginLeft: 20,
    width: "calc(100% - 36px)",
    padding: "4px 0",
  },
  itemGift: {
    padding: 4,
    display: "flex",
    alignItems: "center",
    fontSize: 12,
    lineHeight: "14px",
  },
  iconDot: {
    fill: theme.palette.black.black3,
    margin: "0 6px 0 8px",
    borderRadius: "50%",
  },
  boxLeft: { display: "flex", marginTop: 8, alignItems: "center" },
  buyingSignals: {
    color: theme.palette.green.greenLight7,
  },
  iconBuySignals: {
    stroke: theme.palette.green.greenLight7,
  },
}));

const listTypeModal = {
  MODAL_CANCEL_POLICY: "MODAL_CANCEL_POLICY",
};

const ItemRate = ({ nameRoom = "", item = {}, paramsInit = {} }) => {
  const classes = useStyles();
  const [visibleDrawerType, setVisibleDrawerType] = useState("");

  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };

  const getListBed = () => {
    let listBed = "";
    item.bedGroups.forEach((el, index) => {
      listBed = `${listBed}${index !== 0 ? " hoặc " : ""}${el.bedInfo}`;
    });
    return listBed;
  };

  const arrSignalPromo = (signalPromo = []) => {
    let resultPromo = [];
    return (resultPromo = signalPromo.filter(
      (item) => item.groupCode === "PROMOTIONS"
    ));
  };
  return (
    <Box className={classes.container}>
      <Box className={classes.wrapInfoRoom}>
        <Typography
          variant="subtitle1"
          style={{ lineHeight: "17px", fontSize: 14 }}
        >
          {`${paramsInit.rooms}x`} {nameRoom}
        </Typography>
        <Box className={classes.wrapUser}>
          <IconUser2 className={classes.iconUser2} />{" "}
          <Typography component="span" variant="body2">
            {`${parseInt(paramsInit.adults) +
              parseInt(paramsInit.children)} người`}
          </Typography>
        </Box>
        <Box className={classes.wrapUser}>
          <Box style={{ width: 16, height: 16, marginRight: 6 }}>
            <IconBed className={classes.iconUser2} />{" "}
          </Box>
          <Typography component="span" variant="body2">
            {" "}
            {getListBed()}
          </Typography>
        </Box>
      </Box>
      <Box className={classes.wrapUser}>
        <IconBreakFast
          className={`svgFillAll ${
            item?.freeBreakfast
              ? classes.iconFreeBreakFast
              : classes.iconBreakFast
          }`}
        />
        <Typography
          className={clsx(
            classes.textCancelPolicy,
            item?.freeBreakfast ? classes.textFreeBreakFast : ""
          )}
        >
          {item?.freeBreakfast
            ? listString.IDS_MT_TEXT_ROOM_FREE_BREAK_FAST
            : listString.IDS_MT_TEXT_ROOM_NO_FREE_BREAK_FAST}
        </Typography>
      </Box>
      {item.freeCancellation === true ? (
        <>
          <Box
            className={classes.wrapUser}
            onClick={toggleDrawer(listTypeModal.MODAL_CANCEL_POLICY)}
          >
            <IconCancelPolicy
              className={`svgFillAll ${classes.freeCancelPolicy}`}
            />
            <Typography
              className={classes.textCancelPolicy}
              style={{ color: "#48BB78" }}
            >
              {listString.IDS_MT_TEXT_FREE_CANCEL}
            </Typography>
            <IconInfo className={`svgFillAll ${classes.iconFreeBreakFast}`} />
          </Box>
          <Typography variant="body2" style={{ marginLeft: "22px" }}>
            {!isEmpty(item.hotelCancellationPolicies) &&
              item.hotelCancellationPolicies.map((el, i) => (
                <Box component="span" color="gray.grayDark8" key={i}>
                  {el.price === el.refundAmount &&
                    `Trước ${el.dateTo.split(" ")[0]}`}
                </Box>
              ))}
          </Typography>
        </>
      ) : (
        <Box
          className={classes.wrapUser}
          onClick={() => {
            if (item.refundable) {
              setVisibleDrawerType(listTypeModal.MODAL_CANCEL_POLICY);
            }
          }}
        >
          <IconCancelPolicy />
          <Typography className={classes.textCancelPolicy}>
            {item.shortCancelPolicy}
          </Typography>
          {item.refundable && (
            <IconInfo className={`svgFillAll ${classes.info}`} />
          )}
        </Box>
      )}
      {!isEmpty(item.buyingSignals) && (
        <Box className={classes.boxLeft}>
          <Box style={{ width: 16, height: 16 }}>
            <IconBoxGif className={`svgFillAll ${classes.iconBuySignals}`} />
          </Box>
          <Typography
            className={clsx(classes.textCancelPolicy, classes.buyingSignals)}
          >
            Quà tặng kèm
          </Typography>
        </Box>
      )}
      {!isEmpty(item.buyingSignals) && (
        <Box className={classes.wrapGiftBox}>
          {arrSignalPromo(item.buyingSignals).map((el, index) => (
            <Box className={classes.itemGift} key={index}>
              {item.buyingSignals.length > 1 && (
                <IconDot className={`svgFillAll ${classes.iconDot}`} />
              )}
              {el.name}
            </Box>
          ))}
        </Box>
      )}
      {item.requestPrice ? (
        <Box className={classes.signal}>
          <IconCheck className={`svgFillAll ${classes.iconCheck}`} />
          <Box pl={12 / 8} component="span" lineHeight="17px" color="pink.main">
            {listString.IDS_MT_TEXT_AGREE_ROOM_EARLY}
          </Box>
        </Box>
      ) : (
        <>
          {item?.availableAllotment > 0 && item?.availableAllotment <= 5 && (
            <Box className={classes.signal}>
              <Box component="span" lineHeight="17px" color="pink.main">
                {`Đừng bỏ lỡ! ${
                  item.availableAllotment === 1
                    ? "Phòng cuối cùng của chúng tôi."
                    : `Chúng tôi chỉ còn ${item.availableAllotment} phòng có
                giá này.`
                } Hãy đặt ngay!`}
              </Box>
            </Box>
          )}
        </>
      )}
      <PolicyRoomlModal
        open={visibleDrawerType === listTypeModal.MODAL_CANCEL_POLICY}
        toggleDrawer={toggleDrawer}
        item={item}
      />
    </Box>
  );
};

export default ItemRate;
