import { useRouter } from "next/router";
import { makeStyles } from "@material-ui/styles";
import { Box, Dialog, Typography } from "@material-ui/core";

import { listString } from "@utils/constants";

import ButtonComponent from "@src/button/Button";

const useStyles = makeStyles((theme) => ({
  dialogContent: {
    padding: "24px 12px 12px 12px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  buttonGroup: { display: "flex", width: "100%" },

  textTitleDialog: {
    padding: "0 12px",
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
    margin: "12px 0 8px 0",
    textAlign: "center",
  },
  textDesDialog: {
    padding: "0 12px",
    fontSize: 14,
    lineHeight: "22px",
    margin: "12px 0",
    textAlign: "center",
  },
}));

const AlmBookingModal = ({
  open = false,
  toggleDrawer = () => {},
  messageError = listString.IDS_MT_EMPTY_ROOM_BOOKING,
}) => {
  const classes = useStyles();
  const router = useRouter();
  return (
    <Dialog
      onClose={toggleDrawer("")}
      aria-labelledby="simple-dialog-title"
      open={open}
      disableBackdropClick
    >
      <Box className={classes.dialogContent}>
        <Typography variant="body2" className={classes.textTitleDialog}>
          Mytour.vn
        </Typography>

        <Typography variant="body2" className={classes.textDesDialog}>
          {messageError}
        </Typography>
        <Box className={classes.buttonGroup}>
          <ButtonComponent
            backgroundColor="#FF1284"
            color="#FFFFFF"
            padding="6px 20px"
            height={48}
            fontSize={16}
            fontWeight={600}
            borderRadius={8}
            style={{ margin: "0 12px 12px" }}
            handleClick={() => {
              router.back();
            }}
          >
            {listString.IDS_MT_SELECT_OTHER_ROOM}
          </ButtonComponent>
        </Box>
      </Box>
    </Dialog>
  );
};

export default AlmBookingModal;
