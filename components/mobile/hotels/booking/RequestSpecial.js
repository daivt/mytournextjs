import * as yup from "yup";
import { Formik, Form } from "formik";
import { makeStyles } from "@material-ui/styles";
import { Drawer } from "@material-ui/core";

import { isEmpty } from "@utils/helpers";
import { listString } from "@utils/constants";

import RequestSpecialInfo from "@components/mobile/hotels/booking/RequestSpecialInfo";

const useStyles = makeStyles((theme) => ({
  paperAnchorBottom: {
    width: "100vw",
    height: "100vh",
    borderRadius: 0,
    fontSize: 14,
  },
}));

const RequestSpecial = ({
  open = false,
  toggleDrawer = () => {},
  handleUseRequestOther = () => {},
  requestOther = {},
  roomRate = {},
}) => {
  const classes = useStyles();
  const storeSchema = yup.object().shape({
    companyName: yup.string().when("isRequestSpecial", {
      is: true,
      then: yup
        .string()
        .trim()
        .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    }),
    taxNumber: yup.string().when("isRequestSpecial", {
      is: true,
      then: yup
        .string()
        .trim()
        .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    }),
    companyAddress: yup.string().when("isRequestSpecial", {
      is: true,
      then: yup
        .string()
        .trim()
        .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    }),
    recipientAddress: yup.string().when("isRequestSpecial", {
      is: true,
      then: yup
        .string()
        .trim()
        .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    }),
    recipientName: yup.string().when("isRequestSpecial", {
      is: true,
      then: yup
        .string()
        .trim()
        .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    }),
    recipientEmail: yup.string().when("isRequestSpecial", {
      is: true,
      then: yup
        .string()
        .trim()
        .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    }),
  });
  const rate = !isEmpty(roomRate.rates) ? roomRate.rates[0] : {};

  return (
    <>
      <Drawer
        anchor="bottom"
        open={open}
        onClose={toggleDrawer("")}
        classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
      >
        <Formik
          initialValues={{
            textOther: requestOther.textOther || "",
            isRequestSpecial: false,
            companyName: requestOther?.invoiceInfo?.companyName || "",
            taxNumber: requestOther?.invoiceInfo?.taxNumber || "",
            companyAddress: requestOther?.invoiceInfo?.companyAddress || "",
            recipientAddress: requestOther?.invoiceInfo?.recipientAddress || "",
            recipientName: requestOther?.invoiceInfo?.recipientName || "",
            recipientEmail: requestOther?.invoiceInfo?.recipientEmail || "",
            note: requestOther?.invoiceInfo?.note || "",
            noSmoking: false,
            isHighFloor: false,
          }}
          onSubmit={async (values) => {
            try {
              handleUseRequestOther(values);
            } catch (error) {}
          }}
          validationSchema={storeSchema}
        >
          {() => {
            return (
              <Form>
                <RequestSpecialInfo
                  roomRate={roomRate}
                  toggleDrawer={toggleDrawer}
                  requestOther={requestOther}
                />
              </Form>
            );
          }}
        </Formik>
      </Drawer>
    </>
  );
};
export default RequestSpecial;
