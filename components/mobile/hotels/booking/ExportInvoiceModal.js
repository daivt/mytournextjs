import {
  Box,
  Drawer,
  IconButton,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { IconClose } from "@public/icons";
import { listString } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  paperAnchorBottom: {
    borderRadius: "16px 16px 0 0",
    maxHeight: "48%",
    paddingBottom: 20,
  },
  wrapButtonClose: { padding: 0 },
  wraperHeader: {
    padding: "12px 16px",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    height: 48,
    display: "flex",
    alignItems: "center",
  },
  wrapHeaderDraw: {
    display: "flex",
    alignItems: "center",
    height: 48,
    borderBottom: "1px solid #EDF2F7",
    width: "100%",
  },
  textFilter: {
    textAlign: "center",
    width: "calc(100% - 40px)",
  },
  tooltipText: {
    fontSize: 14,
    lineHeight: "22px",
    "& li": { marginBottom: 12 },
  },
  wrapContent: {
    padding: 15,
  },
}));

const ExportInvoiceModal = ({
  open = true,
  toggleDrawerExportInvoice = () => {},
}) => {
  const classes = useStyles();
  return (
    <>
      <Drawer
        anchor="bottom"
        open={open}
        onClose={toggleDrawerExportInvoice("")}
        classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
      >
        <Box className={classes.wraperHeader}>
          <Box className={classes.wrapHeaderDraw}>
            <IconButton
              onClick={toggleDrawerExportInvoice("")}
              className={classes.wrapButtonClose}
            >
              <IconClose />
            </IconButton>
            <Typography variant="subtitle1" className={classes.textFilter}>
              {listString.IDS_MT_INVOICE_REQUIRE}
            </Typography>
          </Box>
        </Box>
        <Box className={classes.wrapContent}>
          <Box>
            <Typography
              className={classes.tooltipText}
              style={{ fontWeight: 600 }}
            >
              {listString.IDS_TEXT_CONDITION_INVOICE_TITLE}
            </Typography>
            <ul className={classes.tooltipText} style={{ paddingLeft: 16 }}>
              <li>{listString.IDS_MT_REQUIRE_INVOICE_DES_1}</li>
              <li>{listString.IDS_MT_REQUIRE_INVOICE_DES_2}</li>
              <li>{listString.IDS_MT_REQUIRE_INVOICE_DES_3}</li>
              <li>{listString.IDS_MT_REQUIRE_INVOICE_DES_4}</li>
            </ul>
          </Box>
        </Box>
      </Drawer>
    </>
  );
};
ExportInvoiceModal.propTypes = {};
export default ExportInvoiceModal;
