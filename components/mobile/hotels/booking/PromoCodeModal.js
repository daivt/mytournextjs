import clsx from "clsx";
import * as yup from "yup";
import moment from "moment";
import { useState } from "react";
import { Formik, Form } from "formik";
import { DATE_FORMAT } from "@utils/moment";
import {
  Box,
  makeStyles,
  Drawer,
  Grid,
  Typography,
  useTheme,
} from "@material-ui/core";

import { isEmpty } from "@utils/helpers";
import { listString } from "@utils/constants";

import CodeInfoModal from "./CodeInfoModal";
import ButtonComponent from "@src/button/Button";
import FieldTextBaseContent from "@src/form/FieldTextBaseContent";

const listTypeModal = {
  MODAL_USE_PROMO: "MODAL_USE_PROMO",
};

const useStyles = makeStyles((theme) => ({
  paperAnchorBottom: {
    borderRadius: "8px 8px 0 0 ",
    maxHeight: "85%",
  },
  wrapCard: {
    display: "flex",
    flexDirection: "column",
    padding: 12,
    borderLeft: `solid 5px ${theme.palette.blue.blueLight8}`,
    borderRadius: 8,
    background: theme.palette.white.main,
  },
  wrapDiscountPercent: {
    borderLeft: `1px dashed ${theme.palette.gray.grayLight25}`,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    color: theme.palette.blue.blueLight8,
    height: "100%",
    borderRadius: 8,
    background: theme.palette.white.main,
  },

  circle: (props) => ({
    height: 21,
    width: 21,
    backgroundColor: props.bgCircle,
    borderRadius: "50%",
    position: "absolute",
    left: -10,
  }),
  containerDiscount: {
    position: "relative",
  },
  wraper: {
    borderRadius: 8,
    display: "flex",
    flexDirection: "row",
    background: theme.palette.gray.grayLight22,
    margin: "16px 0",
  },
  brDefault: {
    border: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  activeVoucher: {
    backgroundColor: theme.palette.blue.blueLight11,
    "& .circle": {
      border: `solid 1px ${theme.palette.blue.blueLight8}`,
    },
    "& .voucherUsed": {
      borderTop: `solid 1px ${theme.palette.blue.blueLight8}`,
      borderBottom: `solid 1px ${theme.palette.blue.blueLight8}`,
      borderRight: `solid 1px ${theme.palette.blue.blueLight8}`,
    },
    "& .voucherSelected": {
      borderTop: `solid 1px ${theme.palette.blue.blueLight8}`,
      borderBottom: `solid 1px ${theme.palette.blue.blueLight8}`,
    },
  },
  expiredDate: {
    fontSize: 12,
    fontWeight: 400,
    lineHeight: "14px",
    color: theme.palette.gray.grayDark7,
    paddingTop: 8,
  },
  voucherCode: {
    color: theme.palette.gray.grayDark7,
    textTransform: "uppercase",
    paddingBottom: 6,
  },
  containerV: {
    overflow: "hidden",
    borderRadius: 8,
    margin: "0 16px",
  },
  swipeDown: {
    height: 5,
    width: 42,
    display: "flex",
    justifyContent: "center",
    background: theme.palette.gray.grayLight23,
    margin: "8px auto 14px auto",
    borderRadius: 100,
  },
  wrapInput: {
    marginLeft: 16,
    display: "flex",
    alignItems: "center",
    position: "relative",
  },
  wrapBtn: {
    whiteSpace: "nowrap",
    position: "absolute",
    right: 16,
    top: 4,
  },
  usePromo: {
    margin: "16px 16px",
    fontSize: 14,
    lineHeight: "17px",
    fontWeight: 600,
  },
  wrapUserPromo: {
    background: theme.palette.gray.grayLight22,
  },
  wrapContent: {
    background: theme.palette.gray.grayLight22,
    height: "100%",
  },
}));

const PromoCodeModal = ({
  open = false,
  bgCircle = "#EDF2F7",
  toggleDrawer = () => {},
  listPromotion = [],
  promotionActive = {},
  hasBorder = false,
  handleSetVoucher = () => {},
  handleUseEnterCode = () => {},
}) => {
  const propsStyle = {
    bgCircle,
  };
  const classes = useStyles(propsStyle);
  const theme = useTheme();
  const [visibleDrawerPromo, setVisibleDrawerPromo] = useState("");
  const [promoCodeSelected, setPromoCodeSelected] = useState({});
  const toggleDrawerPromo = (openPromo = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerPromo(openPromo);
  };
  const storeSchema = yup.object().shape({
    code: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_PROMOTION_CODE),
  });

  return (
    <>
      <Drawer
        anchor="bottom"
        open={open}
        onClose={toggleDrawer("")}
        classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
      >
        <Box>
          {" "}
          <Box className={classes.swipeDown} />
        </Box>
        <Typography variant="subtitle1" style={{ margin: "16px 0 14px 16px" }}>
          {listString.IDS_MT_TEXT_USE_PROMO_CODE}
        </Typography>
        <Formik
          initialValues={{
            code: "",
          }}
          onSubmit={async (values) => {
            handleUseEnterCode(values.code);
            try {
            } catch (error) {}
          }}
          validationSchema={storeSchema}
        >
          {({ values, setFieldValue }) => {
            return (
              <Form>
                <Box className={classes.wrapInput}>
                  <FieldTextBaseContent
                    id="field-code"
                    name="code"
                    inputProps={{
                      autoComplete: "off",
                    }}
                    label={
                      listString.IDS_MT_TEXT_PLACEHOLDER_ENTER_CODE_SALE_OFF
                    }
                  />
                  <ButtonComponent
                    backgroundColor={theme.palette.secondary.main}
                    height={48}
                    fontSize={16}
                    fontWeight={600}
                    borderRadius={8}
                    width={98}
                    className={classes.wrapBtn}
                    type="submit"
                  >
                    {listString.IDS_TEXT_USE}
                  </ButtonComponent>
                </Box>
              </Form>
            );
          }}
        </Formik>

        <Box className={classes.wrapContent}>
          {!isEmpty(listPromotion) && (
            <Box className={classes.wrapUserPromo}>
              <Typography className={classes.usePromo}>
                {listString.IDS_MT_TEXT_CHOOSE_TWO_PROMO_CODE}
                <Typography
                  component="span"
                  style={{ fontSize: "14px", paddingLeft: 2 }}
                >
                  {`(${listPromotion.length} mã)`}
                </Typography>
              </Typography>
            </Box>
          )}

          {!isEmpty(listPromotion) &&
            listPromotion.map((item, index) => (
              <Box className={classes.wraper} key={index.toString()}>
                <Grid
                  container
                  className={clsx(
                    classes.containerV,
                    promotionActive?.id === item?.id && classes.activeVoucher,
                    hasBorder && classes.brDefault
                  )}
                >
                  <Grid
                    item
                    lg={9}
                    md={9}
                    sm={9}
                    xs={9}
                    onClick={() => {
                      setPromoCodeSelected(item);
                      setVisibleDrawerPromo(listTypeModal.MODAL_USE_PROMO);
                    }}
                  >
                    <Box className={`voucherSelected ${classes.wrapCard}`}>
                      <Typography
                        variant="subtitle2"
                        className={classes.voucherCode}
                      >
                        {item?.code}
                      </Typography>
                      <Typography
                        variant="subtitle1"
                        className={classes.voucherDesc}
                      >
                        {item?.title}
                      </Typography>
                      <Typography
                        variant="body2"
                        className={classes.expiredDate}
                      >
                        {listString.IDS_TEXT_DISCOUNT_CARD_EXPIRED}:{" "}
                        {moment(item?.toDate).format(DATE_FORMAT)}
                      </Typography>
                    </Box>
                  </Grid>
                  <Grid
                    item
                    lg={3}
                    md={3}
                    sm={3}
                    xs={3}
                    className={classes.containerDiscount}
                    onClick={() => {
                      handleSetVoucher(item);
                    }}
                  >
                    <Box className={`circle ${classes.circle}`} top={-11} />
                    <Box
                      className={`voucherUsed ${classes.wrapDiscountPercent}`}
                    >
                      {promotionActive?.id === item?.id ? (
                        <Typography variant="subtitle2">
                          {listString.IDS_MT_TEXT_CANCEL_VN}
                        </Typography>
                      ) : (
                        <Typography variant="subtitle2">
                          {listString.IDS_TEXT_USE}
                        </Typography>
                      )}
                    </Box>
                    <Box className={`circle ${classes.circle}`} top="90%" />
                  </Grid>
                </Grid>
              </Box>
            ))}
          <CodeInfoModal
            openPromo={visibleDrawerPromo === listTypeModal.MODAL_USE_PROMO}
            toggleDrawerPromo={toggleDrawerPromo}
            promoCodeSelected={promoCodeSelected}
            handleUsePromotionCode={() => {
              setVisibleDrawerPromo("");
              handleSetVoucher(promoCodeSelected);
            }}
          />
        </Box>
      </Drawer>
    </>
  );
};
PromoCodeModal.propTypes = {};
export default PromoCodeModal;
