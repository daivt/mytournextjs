import clsx from "clsx";
import { useEffect, useState } from "react";
import { useFormikContext } from "formik";
import MuiAccordion from "@material-ui/core/Accordion";
import sanitizeHtml from "sanitize-html";
import { withStyles, makeStyles } from "@material-ui/styles";
import { Box, Checkbox, IconButton } from "@material-ui/core";
import MuiAccordionSummary from "@material-ui/core/AccordionSummary";
import MuiAccordionDetails from "@material-ui/core/AccordionDetails";

import { isEmpty, unEscape } from "@utils/helpers";
import { listString, optionClean } from "@utils/constants";
import { IconClose, IconInfo, IconDot } from "@public/icons";

import { CustomSwitch } from "@src/element";
import ButtonComponent from "@src/button/Button";
import FieldTextContent from "@src/form/FieldTextContent";
import FieldTextBaseContent from "@src/form/FieldTextBaseContent";
import ExportInvoiceModal from "@components/mobile/hotels/booking/ExportInvoiceModal";

const useStyles = makeStyles((theme) => ({
  iconClose: {
    position: "absolute",
    top: 0,
    left: 0,
  },
  serviceAttach: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
    color: theme.palette.black.black3,
    height: 47,
  },
  icon: {
    borderRadius: 6,
    width: 22,
    height: 22,
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
    "$root.Mui-focusVisible &": {
      outline: "2px auto #00B6F3",
      outlineOffset: 2,
    },
  },
  checkedIconRadio: {
    backgroundColor: "#00B6F3",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: 22,
      height: 22,
      backgroundImage: "url(/icons/icon_checked.svg)",
      backgroundSize: "cover",
      content: '""',
    },
  },
  divider: {
    height: 8,
    backgroundColor: theme.palette.gray.grayLight22,
  },
  borderNone: {
    border: "none",
  },
  iconInfo: {
    stroke: "#00B6F3",
  },
  header: {
    zIndex: 1000,
    top: 0,
    left: 0,
    position: "fixed",
    color: "#000",
    background: "#fff",
    width: "100%",
  },
  boxFotter: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    zIndex: 1000,
    bottom: 0,
    left: 0,
    position: "fixed",
    color: "#000",
    background: "#fff",
    width: "100%",
    padding: "12px 16px",
    borderTop: "2px solid #EDF2F7",
  },
  invoiceDes: {
    "& > p": {
      lineHeight: "14px !important",
    },
    paddingTop: 5,
  },
}));

const listPopularRequest = [
  {
    id: "0",
    name: "Phòng không hút thuốc",
    key: "noSmoking",
  },
  {
    id: "1",
    name: "Phòng ở tầng cao",
    key: "isHighFloor",
  },
];
const Accordion = withStyles({
  root: {
    boxShadow: "none",
    backgroundColor: "transparent",
    "&:before": {
      display: "none",
    },
  },
  expanded: {
    borderBottom: "none",
  },
})(MuiAccordion);
const AccordionSummary = withStyles({
  root: {
    minHeight: "initial",
    padding: 0,
    "&$expanded": {
      minHeight: "initial",
    },
  },
  content: {
    margin: 0,
    "&$expanded": {
      margin: 0,
    },
  },
  expanded: {},
})(MuiAccordionSummary);
const AccordionDetails = withStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    padding: 0,
  },
}))(MuiAccordionDetails);
const listTypeModal = {
  MODAL_EXPORT_INVOICE: "MODAL_EXPORT_INVOICE",
};
const RequestSpecialInfo = ({
  toggleDrawer = () => {},
  roomRate = {},
  requestOther = {},
}) => {
  const classes = useStyles();
  const { setFieldValue, values } = useFormikContext();
  const rate = !isEmpty(roomRate.rates) ? roomRate.rates[0] : {};
  const [visibleDrawerInvoice, setVisibleDrawerInvoice] = useState("");
  const toggleDrawerExportInvoice = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerInvoice(open);
  };
  useEffect(() => {
    if (!isEmpty(requestOther)) {
      setFieldValue("noSmoking", requestOther?.noSmoking || false);
      setFieldValue("isHighFloor", requestOther?.isHighFloor || false);
      setFieldValue("textOther", requestOther?.specialRequest || "");
      if (!isEmpty(requestOther.invoiceInfo)) {
        setFieldValue("isRequestSpecial", true);
        setFieldValue(
          "companyName",
          requestOther?.invoiceInfo?.companyName || ""
        );
        setFieldValue("taxNumber", requestOther?.invoiceInfo?.taxNumber || "");
        setFieldValue(
          "companyAddress",
          requestOther?.invoiceInfo?.companyAddress || ""
        );
        setFieldValue(
          "recipientAddress",
          requestOther?.invoiceInfo?.recipientAddress || ""
        );
        setFieldValue(
          "recipientName",
          requestOther?.invoiceInfo?.recipientName || ""
        );
        setFieldValue(
          "recipientEmail",
          requestOther?.invoiceInfo?.recipientEmail || ""
        );
        setFieldValue("note", requestOther?.invoiceInfo?.note || "");
      }
    }
  }, [requestOther]);

  return (
    <>
      <Box>
        <Box
          display="flex"
          alignItems="center"
          justifyContent="center"
          position="relative"
          height={45}
          borderBottom="1px solid #EDF2F7"
          className={classes.header}
        >
          <Box
            component="span"
            fontSize={16}
            fontWeight={600}
            lineHeight="19px"
          >
            {listString.IDS_MT_TEXT_REQUEST_SPECIAL}
          </Box>
          <Box className={classes.iconClose}>
            <IconButton onClick={toggleDrawer("")}>
              <IconClose />
            </IconButton>
          </Box>
        </Box>
        <Box pb={15} pt={45 / 8}>
          <Box p="16px 7px 16px 16px">
            <Box component="span" lineHeight="17px" fontWeight={600}>
              {listString.IDS_MT_TEXT_REQUEST_POPULAR}
            </Box>
            <Box>
              {listPopularRequest.map((el, index) => (
                <Box
                  className={clsx(
                    classes.serviceAttach,
                    index === listPopularRequest.length - 1 &&
                      classes.borderNone
                  )}
                  key={el.id}
                  onClick={() => {
                    if (el.key === "noSmoking") {
                      setFieldValue("noSmoking", !values.noSmoking);
                    } else {
                      setFieldValue("isHighFloor", !values.isHighFloor);
                    }
                  }}
                >
                  {el?.name}
                  <Checkbox
                    checked={values[el.key]}
                    color="default"
                    checkedIcon={
                      <span
                        className={clsx(classes.icon, classes.checkedIconRadio)}
                      />
                    }
                    icon={<span className={classes.icon} />}
                  />
                </Box>
              ))}
            </Box>
          </Box>
          <Box className={classes.divider} />
          <Box p={2}>
            <Box lineHeight="17px" fontWeight={600} mb={12 / 8}>
              {listString.IDS_MT_TEXT_REQUEST_PRIVATE}
            </Box>
            <FieldTextContent
              id="field-textOther"
              name="textOther"
              placeholder={
                listString.IDS_MT_TEXT_PLACEHOLDER_ENTER_REQUEST_OTHER
              }
              inputProps={{
                autoComplete: "off",
              }}
              rows={2}
              multiline
              hideHelperText
            />
          </Box>
          <Box className={classes.divider} />
          {rate?.invoiceInfo?.invoiceType === "not_marketplace" ? (
            <Box px={2}>
              <Accordion expanded={values.isRequestSpecial}>
                <AccordionSummary id="family-field">
                  <Box
                    width="100%"
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                    py={10 / 8}
                  >
                    <Box
                      display="flex"
                      alignItems="center"
                      lineHeight="17px"
                      fontWeight={600}
                      onClick={toggleDrawerExportInvoice(
                        listTypeModal.MODAL_EXPORT_INVOICE
                      )}
                    >
                      {listString.IDS_TEXT_EXPORT_INVOICE}
                      <IconInfo
                        className={`svgFillAll ${classes.iconInfo}`}
                        style={{ marginLeft: "4px" }}
                      />
                    </Box>
                    <CustomSwitch
                      checked={values.isRequestSpecial}
                      onChange={(e) => {
                        setFieldValue(
                          `isRequestSpecial`,
                          !values.isRequestSpecial
                        );
                      }}
                    />
                  </Box>
                </AccordionSummary>
                <AccordionDetails>
                  <Box display="flex" flexDirection="column">
                    <FieldTextBaseContent
                      id="field-taxNumber"
                      name="taxNumber"
                      inputProps={{
                        autoComplete: "off",
                      }}
                      label={listString.IDS_MT_TEXT_CODE_INVOICE}
                    />
                    <FieldTextBaseContent
                      id="field-companyName"
                      name="companyName"
                      inputProps={{
                        autoComplete: "off",
                      }}
                      label={listString.IDS_TEXT_PLACEHOLDER_INVOICE_NAME}
                    />
                    <FieldTextBaseContent
                      id="field-companyAddress"
                      name="companyAddress"
                      inputProps={{
                        autoComplete: "off",
                      }}
                      label={listString.IDS_TEXT_PLACEHOLDER_INVOICE_ADDRESS}
                    />
                    <FieldTextBaseContent
                      id="field-recipientAddress"
                      name="recipientAddress"
                      inputProps={{
                        autoComplete: "off",
                      }}
                      label={listString.IDS_TEXT_PLACEHOLDER_INVOICE_RECEIVER}
                    />
                    <FieldTextBaseContent
                      id="field-recipientName"
                      name="recipientName"
                      inputProps={{
                        autoComplete: "off",
                      }}
                      label={
                        listString.IDS_TEXT_PLACEHOLDER_INVOICE_CUSTOMER_NAME
                      }
                    />
                    <FieldTextBaseContent
                      id="field-recipientEmail"
                      name="recipientEmail"
                      inputProps={{
                        autoComplete: "off",
                      }}
                      label={listString.IDS_MT_TEXT_EMAIL}
                    />
                    <FieldTextBaseContent
                      id="field-note"
                      name="note"
                      inputProps={{
                        autoComplete: "off",
                      }}
                      label={listString.IDS_TEXT_PLACEHOLDER_INVOICE_NOTE}
                    />
                  </Box>
                </AccordionDetails>
              </Accordion>
            </Box>
          ) : (
            <Box display="flex" flexDirection="column" p={2}>
              <Box component="span" lineHeight="17px" fontWeight={600}>
                {listString.IDS_TEXT_EXPORT_INVOICE}
              </Box>
              <Box
                fontSize={14}
                component="span"
                lineHeight="22px"
                color="gray.grayDark1"
                className={classes.invoiceDes}
                dangerouslySetInnerHTML={{
                  __html: sanitizeHtml(
                    unEscape(
                      unescape(rate?.invoiceInfo?.invoiceDescription || "")
                    ),
                    optionClean
                  ),
                }}
              />
            </Box>
          )}
        </Box>

        <Box className={classes.boxFotter}>
          <ButtonComponent
            backgroundColor="#FF1284"
            borderRadius={8}
            height={48}
            fontSize={16}
            fontWeight={600}
            type="submit"
          >
            {listString.IDS_MT_TEXT_APPLY}
          </ButtonComponent>
          <Box
            component="span"
            fontSize={12}
            lineHeight="14px"
            color="gray.grayDark8"
            textAlign="center"
            pt={1}
          >
            {listString.IDS_MT_TEXT_POLICY_REQUEST_PRIVATE}
          </Box>
        </Box>
      </Box>
      <ExportInvoiceModal
        open={visibleDrawerInvoice === listTypeModal.MODAL_EXPORT_INVOICE}
        toggleDrawerExportInvoice={toggleDrawerExportInvoice}
      />
    </>
  );
};
export default RequestSpecialInfo;
