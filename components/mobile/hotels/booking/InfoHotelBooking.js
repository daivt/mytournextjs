import moment from "moment";
import { Box, Grid, makeStyles, Typography } from "@material-ui/core";
import { IconMoonlight, IconStar } from "@public/icons";

import Image from "@src/image/Image";
import { listString } from "@utils/constants";
import { getMessageHotelReviews } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  boerHeaer: {
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  wrapContainer: {
    margin: "0 16px",
  },
  textBooking: {
    width: "calc(100% - 48px)",
    textAlign: "center",
  },
  hotelName: {
    display: "flex",
    flexDirection: "column",
  },
  rating: { margin: "4px 0 8px 0", color: theme.palette.yellow.yellowLight3 },
  wrapInfoHotel: {
    color: theme.palette.black.black3,
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
    marginTop: 16,
    paddingBottom: 16,
  },
  imageHotel: {
    width: 88,
    height: 88,
    borderRadius: 8,
  },
  boxLeft: { display: "flex", marginTop: 6 },
  wrapBoxRight: {
    display: "flex",
    justifyContent: "flex-end",
    margin: "0 0 16px 0",
  },
  wrapReviewPoint: {
    fontSize: 14,
    lineHeight: "17px",
    display: "flex",
    alignContent: "center",
    whiteSpace: "nowrap",
    marginTop: 11,
  },
  point: {
    color: theme.palette.white.main,
    padding: "1px 4px 1px 4px",
    background: theme.palette.blue.blueLight8,
    width: 30,
    borderRadius: 4,
    marginRight: 4,
    display: "flex",
    alignItems: "center",
  },
  messageReview: {
    color: theme.palette.black.black3,
    marginRight: 4,
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    display: "flex",
    alignItems: "center",
  },
  countReview: {
    fontSize: 14,
    color: theme.palette.gray.grayDark7,
    lineHeight: "16px",
    fontWeight: 400,
    display: "flex",
    alignItems: "center",
  },
  wrapTimeBook: {
    display: "flex",
    alignItems: "center",
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  timeBooking: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark8,
    marginTop: 16,
  },
  nightDay: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 34,
    height: 28,
    borderRadius: "100px",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    backgroundColor: theme.palette.white.main,
    margin: "0 auto",
  },
  rightBookingTime: {
    textAlign: "right",
  },
  timeCheckIn: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.black.black3,
    fontWeight: 600,
    marginTop: 2,
  },
  wrapRatingStar: {
    display: "flex",
    marginTop: 6,
  },
  rankingHotel: {
    stroke: theme.palette.yellow.yellowLight3,
    fill: theme.palette.yellow.yellowLight3,
    width: 16,
    height: 16,
    marginRight: 3,
  },
}));
const arrStar = [1, 2, 3, 4, 5];

const InfoHotelBooking = ({
  hotelDetail = {},
  roomRate = {},
  paramsInit = {},
}) => {
  const classes = useStyles();
  let checkIn = moment(paramsInit.checkIn, "DD-MM-YYYY");
  let checkOut = moment(paramsInit.checkOut, "DD-MM-YYYY");

  return (
    <Box className={classes.boerHeaer}>
      <Box className={classes.wrapContainer}>
        <Grid container className={classes.wrapInfoHotel}>
          <Grid item xs={7} className={classes.wrapBoxLeft}>
            <Box className={classes.wrapReview}>
              <Typography variant="subtitle1" className={classes.hotelName}>
                {hotelDetail.name}
              </Typography>
              <Box className={classes.wrapRatingStar}>
                {arrStar.map((el, index) => {
                  if (el > hotelDetail.starNumber) return null;
                  return (
                    <IconStar
                      key={index}
                      className={`svgFillAll ${classes.rankingHotel}`}
                    />
                  );
                })}
              </Box>
              <Box className={classes.wrapReviewPoint}>
                <Typography variant="subtitle2" className={classes.point}>
                  {2 * hotelDetail?.rating?.hotel?.rating === 10
                    ? 10
                    : (2 * hotelDetail?.rating?.hotel?.rating).toFixed(1)}
                </Typography>
                <Box className={classes.messageReview}>
                  {getMessageHotelReviews(hotelDetail?.rating?.hotel?.rating)}
                </Box>
                {hotelDetail?.rating?.hotel?.count > 0 && (
                  <Typography className={classes.countReview}>
                    {`(${hotelDetail?.rating?.hotel?.count} đánh giá)`}
                  </Typography>
                )}
              </Box>
            </Box>
          </Grid>
          <Grid item xs={5} className={classes.wrapBoxRight}>
            <Image
              srcImage={roomRate?.thumbnail}
              className={classes.imageHotel}
              borderRadiusProp="8px"
            />
          </Grid>
        </Grid>
        <Grid container className={classes.wrapTimeBook}>
          <Grid item xs={5} className={classes.leftBookingTime}>
            <Typography className={classes.timeBooking}>
              {listString.IDS_MT_TEXT_CHECK_IN_ROOM}
            </Typography>
            <Typography className={classes.timeCheckIn}>
              {checkIn
                ? checkIn
                    .locale("vi_VN")
                    .format("ddd")
                    .replace("T", "Thứ ")
                    .replace("CN", "Chủ Nhật")
                : "-"}
              <Box component="span">
                {", "}
                {checkIn.format("DD")} Tháng {checkIn.format("MM")}
              </Box>
            </Typography>
            <Typography
              className={classes.timeCheckIn}
              style={{
                color: "#4A5568",
                margin: "2px 0 16px 0",
                fontWeight: "400",
              }}
            >
              {hotelDetail.checkInTime}
            </Typography>
          </Grid>
          <Grid item xs={2} className={classes.centerBookingTime}>
            <Box className={classes.nightDay}>
              <Box component="span" fontSize={11} lineHeight="13px" pr={2 / 8}>
                {checkIn && checkOut
                  ? Math.abs(
                      moment(checkIn)
                        .startOf("day")
                        .diff(checkOut.startOf("day"), "days")
                    )
                  : "-"}
              </Box>

              <IconMoonlight />
            </Box>
          </Grid>
          <Grid item xs={5} className={classes.rightBookingTime}>
            <Typography className={classes.timeBooking}>
              {listString.IDS_MT_TEXT_CHECK_OUT_ROOM}
            </Typography>

            <Typography className={classes.timeCheckIn}>
              {checkOut
                ? checkOut
                    .locale("vi_VN")
                    .format("ddd")
                    .replace("T", "Thứ ")
                    .replace("CN", "Chủ Nhật")
                : "-"}
              <Box component="span">
                {", "}
                {checkOut.format("DD")} Tháng {checkOut.format("MM")}
              </Box>
            </Typography>
            <Typography
              className={classes.timeCheckIn}
              style={{
                color: "#4A5568",
                margin: "2px 0 16px 0",
                fontWeight: "400",
              }}
            >
              {hotelDetail.checkOutTime}
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};
export default InfoHotelBooking;
