import clsx from "clsx";
import * as yup from "yup";
import sortBy from "lodash/sortBy";
import { Formik, Form } from "formik";
import { useSnackbar } from "notistack";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Box, Radio } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import {
  getRoomsRates,
  getListPromotionCode,
  createBookingsBook,
  updateBookingsBook,
  getDiscountPromotionCode,
  getHotelsRoom,
  createPaymentForBooking,
} from "@api/hotels";
import { isEmpty } from "@utils/helpers";
import {
  IconArrowDownToggle,
  IconBedDouble,
  IconBedSingle,
} from "@public/icons";
import {
  listIcons,
  listString,
  routeStatic,
  prefixUrlIcon,
  LAST_BOOKING_HOTEL,
  DELAY_TIMEOUT_POLLING,
  LAST_BOOKING_HOTEL_INFO,
} from "@utils/constants";

import Image from "@src/image/Image";
import snackbarSetting from "@src/alert/Alert";
import Layout from "@components/layout/mobile/Layout";
import ItemRate from "@components/mobile/hotels/booking/ItemRate";
import InfoPrice from "@components/mobile/hotels/booking/InfoPrice";
import RequestSpecial from "@components/mobile/hotels/booking/RequestSpecial";
import AlmBookingModal from "@components/mobile/hotels/booking/AlmBookingModal";
import InfoHotelBooking from "@components/mobile/hotels/booking/InfoHotelBooking";
import InfoContactCustomer from "@components/mobile/hotels/booking/InfoContactCustomer";

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
  },
  divider: {
    height: 8,
    backgroundColor: theme.palette.gray.grayLight22,
  },
  iconArrowRight: {
    transform: "rotate(270deg)",
    stroke: theme.palette.blue.blueLight8,
  },

  wrapContainer: {
    margin: 16,
  },
  itemBed: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottom: "1px solid #EDF2F7",
    padding: "12px 0",
  },
  iconRadio: {
    borderRadius: "50%",
    width: 20,
    height: 20,
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
  },
  checkedIconRadio: {
    backgroundColor: "#00B6F3",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: 20,
      height: 20,
      backgroundImage: "url(/icons/icon_checked.svg)",
      backgroundSize: "cover",
      content: '""',
    },
  },
  iconBedDouble: {
    fill: theme.palette.gray.grayDark7,
  },
  iconDoubleSingleBed: {
    width: 42,
    height: 24,
  },
}));

const listTypeModal = {
  MODAL_REQUEST_SPECIAL: "MODAL_REQUEST_SPECIAL",
  MODAL_ALM_BOOKING: "MODAL_ALM_BOOKING",
};
const statusTooltipAddPromotion = {
  NONE: "NONE",
  SET: "SET",
  DONE: "DONE",
};
const listStatus = {
  INIT_STATE: "INIT_STATE",
  FIRST_REQUEST: "FIRST_REQUEST",
};

const Booking = ({ hotelDetail = {}, paramsInit = {} }) => {
  const router = useRouter();
  const classes = useStyles();
  const [roomRate, setRoomRate] = useState({});
  const [listPromotion, setListPromotion] = useState([]);
  const [visibleDrawerType, setVisibleDrawerType] = useState("");
  const [shrui, setShrui] = useState("");
  const [promotionActive, setPromotionActive] = useState({});
  const [promotionDiscount, setPromotionDiscount] = useState({});
  const [requestOther, setRequestOther] = useState({});
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [messageError, setMessageError] = useState("");
  const [showTooTipPromotion, setShowTooTipPromotion] = useState(
    statusTooltipAddPromotion.NONE
  );
  const [status, setStatus] = useState(listStatus.INIT_STATE);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  let breakPolling = true;
  useEffect(() => {
    fetRoomRate();
    updateRequestOther();
    return () => {
      breakPolling = false;
    };
  }, []);

  useEffect(() => {
    if (!isEmpty(promotionActive)) {
      fetchDiscountPromotionCode(promotionActive.code);
    }
  }, [promotionActive]);

  const updateRequestOther = () => {
    let lastBookingHotelInfo =
      JSON.parse(localStorage.getItem(LAST_BOOKING_HOTEL_INFO)) || {};
    if (!isEmpty(lastBookingHotelInfo)) {
      let otherTemp = {
        specialRequest: lastBookingHotelInfo?.textOther || "",
        isHighFloor: lastBookingHotelInfo?.isHighFloor || false,
        noSmoking: lastBookingHotelInfo?.noSmoking || false,
        invoiceInfo: {},
      };
      if (!isEmpty(lastBookingHotelInfo.invoiceInfo)) {
        otherTemp = {
          ...otherTemp,
          invoiceInfo: {
            companyAddress:
              lastBookingHotelInfo?.invoiceInfo?.companyAddress || "",
            companyName: lastBookingHotelInfo?.invoiceInfo?.companyName || "",
            taxNumber: lastBookingHotelInfo?.invoiceInfo?.taxNumber || "",
            recipientAddress:
              lastBookingHotelInfo?.invoiceInfo?.recipientAddress || "",
            recipientName:
              lastBookingHotelInfo?.invoiceInfo?.recipientName || "",
            recipientEmail:
              lastBookingHotelInfo?.invoiceInfo?.recipientEmail || "",
            note: lastBookingHotelInfo?.invoiceInfo?.note || "",
          },
        };
      }
      setRequestOther(otherTemp);
    }
  };

  const fetRoomRate = async () => {
    try {
      const { data } = await getRoomsRates(paramsInit);
      if (data.code === 200) {
        setRoomRate(data.data || {});
        if (!data.data.outOfRoom) {
          setInitShrui(data.data);
          checkForBooking(data.data);
        } else {
          getRoomHotel();
        }
      }
    } catch (error) {}
  };

  const handleErrorBooking = (data = {}) => {
    if (
      data.code === 402 ||
      data.code === 5101 ||
      data.code === 5102 ||
      data.code === 5103 ||
      data.code === 5104 ||
      data.code === 5108
    ) {
      localStorage.removeItem(LAST_BOOKING_HOTEL);
      router.reload();
    } else if (data.code === 5201 || data.code === 5202 || data.code === 5203) {
      setMessageError(data?.message || "");
      setVisibleDrawerType(listTypeModal.MODAL_ALM_BOOKING);
    } else {
      setLoadingSubmit(false);
      data?.message &&
        enqueueSnackbar(
          data?.message,
          snackbarSetting((key) => closeSnackbar(key), { color: "error" })
        );
    }
  };

  const checkForBooking = async (rate = {}) => {
    try {
      const lastBookingHotel =
        JSON.parse(localStorage.getItem(LAST_BOOKING_HOTEL)) || {};
      const rateItem = rate.rates[0] || {};
      let dataDTO = getDataForBooking(
        !isEmpty(rateItem.bedGroups) ? rateItem.bedGroups[0].shrui : ""
      );
      let dataBooking = {};
      if (!isEmpty(lastBookingHotel)) {
        dataDTO = {
          ...dataDTO,
          orderToken: lastBookingHotel.orderToken,
          bookingId: lastBookingHotel.bookingId,
        };
        const { data } = await updateBookingsBook(dataDTO);
        if (data.code === 200) {
          dataBooking = data?.data || {};
        } else {
          handleErrorBooking(data);
        }
      } else {
        const { data } = await createBookingsBook(dataDTO);
        if (data.code === 200) {
          dataBooking = data?.data || {};
        } else {
          handleErrorBooking(data);
        }
      }
      if (!isEmpty(dataBooking)) {
        const lastBookingHotelSave = {
          bookingId: dataBooking.id,
          orderToken: dataBooking.orderToken,
          orderCode: dataBooking.orderCode,
        };
        localStorage.setItem(
          LAST_BOOKING_HOTEL,
          JSON.stringify(lastBookingHotelSave)
        );
        fetPromotionCode(dataBooking.orderCode);
      }
    } catch (error) {}
  };

  const getRoomHotel = async () => {
    let polling = true;
    try {
      while (polling && breakPolling) {
        const { data } = await getHotelsRoom(paramsInit);
        if (data.code === 200) {
          if (data.data.completed) {
            if (isEmpty(data.data.items)) {
              //// show modal
              setMessageError(listString.IDS_MT_EMPTY_ROOM_BOOKING);
              setVisibleDrawerType(listTypeModal.MODAL_ALM_BOOKING);
            } else {
              const roomRate = !isEmpty(data.data.items)
                ? data.data.items[0]
                : {};
              setRoomRate(roomRate);
              setInitShrui(roomRate);
              if (!roomRate.outOfRoom) {
                checkForBooking(roomRate);
              }
            }
            polling = false;
            break;
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      polling = false;
    }
  };

  const fetPromotionCode = async (bookingCode = "") => {
    try {
      const { data } = await getListPromotionCode({
        page: 1,
        size: 10,
        bookingCode: bookingCode,
      });
      if (data.code === 200) {
        setListPromotion(data?.data?.items);
        if (!isEmpty(data?.data?.items)) {
          if (!isEmpty(router.query.promotionCode)) {
            const promotionSelect = data?.data?.items.find(
              (el) =>
                el.code.toUpperCase() ===
                router.query.promotionCode.toUpperCase()
            );
            if (!isEmpty(promotionSelect)) {
              setPromotionActive(promotionSelect);
            } else {
              setPromotionActive(
                !isEmpty(data?.data?.items) ? data?.data?.items[0] : {}
              );
            }
          } else {
            setPromotionActive(
              !isEmpty(data?.data?.items) ? data?.data?.items[0] : {}
            );
          }
        }
      }
    } catch (error) {}
  };

  const fetchDiscountPromotionCode = async (code = "", isEnterCode = false) => {
    try {
      const lastBookingHotel =
        JSON.parse(localStorage.getItem(LAST_BOOKING_HOTEL)) || {};
      if (!isEmpty(lastBookingHotel)) {
        const { data } = await getDiscountPromotionCode({
          code: code,
          bookingCode: lastBookingHotel.orderCode,
        });
        if (status === listStatus.INIT_STATE) {
          setStatus(listStatus.FIRST_REQUEST);
        }
        if (data.code === 200) {
          if (showTooTipPromotion === statusTooltipAddPromotion.NONE) {
            setShowTooTipPromotion(statusTooltipAddPromotion.SET);
            setTimeout(() => {
              setShowTooTipPromotion(statusTooltipAddPromotion.DONE);
            }, 1500);
          }
          setPromotionDiscount(data?.data || {});
          if (isEnterCode) {
            enqueueSnackbar(
              "Thêm mã giảm giá thành công",
              snackbarSetting((key) => closeSnackbar(key), { color: "success" })
            );
            setPromotionActive({
              id: data?.data?.id,
              code: code,
              type: "enter",
            });
          }
        } else {
          setShowTooTipPromotion(statusTooltipAddPromotion.DONE);
          if (isEnterCode || status === listStatus.FIRST_REQUEST) {
            data?.message &&
              enqueueSnackbar(
                data?.message,
                snackbarSetting((key) => closeSnackbar(key), { color: "error" })
              );
          }
          setPromotionActive({});
          setPromotionDiscount({});
        }
      }
    } catch (error) {
      if (status === listStatus.INIT_STATE) {
        setStatus(listStatus.FIRST_REQUEST);
      }
    }
  };

  const storeSchema = yup.object().shape({
    name: yup
      .string()
      .trim()
      .required("Bạn chưa nhập đầy đủ họ và tên"),
    email: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_EMAIL)
      .email(listString.IDS_MT_TEXT_EMAIL_INVALIDATE),
    phoneNumber: yup
      .string()
      .trim()
      .min(9, listString.IDS_MT_TEXT_INVALID_PHONE_NUMBER)
      .max(12, listString.IDS_MT_TEXT_INVALID_PHONE_NUMBER)
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_PHONE_NUMBER),
    contactName: yup.string().when("bookingUserOther", {
      is: true,
      then: yup
        .string()
        .trim()
        .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    }),
  });

  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };
  const handleSetVoucher = (item = {}) => {
    if (item?.id === promotionActive?.id) {
      setPromotionActive({});
      setPromotionDiscount({});
    } else {
      setPromotionActive(item);
    }
  };

  const getDataForBooking = (shrui = "") => {
    const roomRatesTemp = [];
    for (let index = 0; index < paramsInit.rooms; index++) {
      roomRatesTemp.push({
        shrui: shrui || "",
      });
    }
    let dataDTO = {
      checkIn: paramsInit.checkIn,
      checkOut: paramsInit.checkOut,
      adults: paramsInit.adults,
      children: paramsInit.children,
      rooms: paramsInit.rooms,
      roomRates: roomRatesTemp,
    };
    if (!isEmpty(paramsInit.childrenAges)) {
      dataDTO = {
        ...dataDTO,
        childrenAges: paramsInit.childrenAges,
      };
    }
    return dataDTO;
  };

  const getDataForBookingFinal = (values = {}) => {
    const roomRatesTemp = [];
    for (let index = 0; index < paramsInit.rooms; index++) {
      roomRatesTemp.push({
        shrui: shrui || "",
        contactName: values.bookingUserOther ? values.contactName : values.name,
        specialRequest: requestOther.specialRequest || "",
        phoneNumber: values.phoneNumber || "",
        isHighFloor: requestOther.isHighFloor,
        noSmoking: requestOther.noSmoking,
      });
    }
    let dataDTO = {
      checkIn: paramsInit.checkIn,
      checkOut: paramsInit.checkOut,
      adults: paramsInit.adults,
      children: paramsInit.children,
      rooms: paramsInit.rooms,
      roomRates: roomRatesTemp,
    };
    if (!isEmpty(paramsInit.childrenAges)) {
      dataDTO = {
        ...dataDTO,
        childrenAges: paramsInit.childrenAges,
      };
    }
    return dataDTO;
  };

  const handleUseEnterCode = (code) => {
    fetchDiscountPromotionCode(code, true);
  };

  const checkIcon = (className) => (
    <span className={clsx(className, classes.checkedIconRadio)} />
  );

  const getListBed = (rate = {}) => {
    let listBed = [];
    rate.bedGroups.forEach((el) => {
      let bedTemp = {
        shrui: el.shrui,
        bedInfo: el.bedInfo,
      };
      let bedTypesTemp = [];
      el.bedTypes.forEach((item) => {
        for (let index = 0; index < item.numberOfBed; index++) {
          bedTypesTemp.push(item.size);
        }
      });
      bedTemp = {
        ...bedTemp,
        bedTypes: bedTypesTemp,
        numberBed: bedTypesTemp.length,
      };
      listBed.push(bedTemp);
    });
    listBed = sortBy(listBed, ["numberBed"]);
    return listBed;
  };
  const handleUseRequestOther = (values) => {
    let otherTemp = {
      specialRequest: values.textOther,
      isHighFloor: values.isHighFloor,
      noSmoking: values.noSmoking,
    };
    if (values.isRequestSpecial) {
      otherTemp = {
        ...otherTemp,
        invoiceInfo: {
          companyAddress: values.companyAddress,
          companyName: values.companyName,
          taxNumber: values.taxNumber,
          recipientAddress: values.recipientAddress,
          recipientName: values.recipientName,
          recipientEmail: values.recipientEmail,
          note: values.note,
        },
      };
    }
    setRequestOther(otherTemp);
    setVisibleDrawerType("");
  };
  const createPayment = async (orderCode = "") => {
    let dataDTO = {
      bookingCode: orderCode,
      paymentMethodCode: "PR",
    };
    if (!isEmpty(promotionActive?.code)) {
      dataDTO = {
        ...dataDTO,
        promotionCode: promotionActive?.code,
      };
    }
    try {
      const { data } = await createPaymentForBooking(dataDTO);
      if (data.code === 200) {
        if (data.data.isRedirect) {
          window.location.href = data.data.paymentLink;
        } else {
          router.push({
            pathname: routeStatic.RESULT_PAYMENT_BOOKING.href,
            query: {
              transactionId: data.data.transactionId,
            },
          });
        }
      } else {
        setLoadingSubmit(false);
        data?.message &&
          enqueueSnackbar(
            data?.message,
            snackbarSetting((key) => closeSnackbar(key), { color: "error" })
          );
      }
    } catch (error) {
      setLoadingSubmit(false);
    }
  };
  const setInitShrui = (rate = {}) => {
    let listBed = getListBed(rate?.rates[0]);
    setShrui(listBed[0].shrui);
  };

  const saveInfoBookingToStorage = (dataDTO) => {
    let lastBookingHotelInfo =
      JSON.parse(localStorage.getItem(LAST_BOOKING_HOTEL_INFO)) || {};
    lastBookingHotelInfo = {
      ...lastBookingHotelInfo,
      customer: dataDTO.customer,
      contactName: dataDTO.roomRates[0].contactName,
      bookingUserOther:
        dataDTO.customer.name !== dataDTO.roomRates[0].contactName,
      noSmoking: dataDTO.roomRates[0].noSmoking || false,
      isHighFloor: dataDTO.roomRates[0].isHighFloor || false,
      textOther: dataDTO.roomRates[0].specialRequest || "",
      invoiceInfo: dataDTO.invoiceInfo || {},
    };
    localStorage.setItem(
      LAST_BOOKING_HOTEL_INFO,
      JSON.stringify(lastBookingHotelInfo)
    );
  };

  const checkShowInvoice = () => {
    if (!isEmpty(roomRate.rates)) {
      const rate = roomRate.rates[0];
      if (rate?.invoiceInfo?.invoiceType !== "not_marketplace") {
        return true;
      }
    }
    return false;
  };
  return (
    <Layout
      isHeaderBack
      isShowTitleInit
      titleHeaderBack={listString.IDS_MT_TEXT_BOOKING}
      title={`Đơn đặt phòng khách sạn ${hotelDetail?.name} | Mytour.vn đảm bảo giá tốt nhất!`}
      iconOgImage={hotelDetail?.thumbnail?.src}
    >
      <Box className={classes.container}>
        <InfoHotelBooking
          hotelDetail={hotelDetail}
          paramsInit={paramsInit}
          roomRate={roomRate}
        />
        <Box className={classes.wrapContainer}>
          {!isEmpty(roomRate.rates) &&
            roomRate.rates.map((el) => (
              <ItemRate
                item={el}
                paramsInit={paramsInit}
                key={el.rateKey}
                nameRoom={roomRate.name}
              />
            ))}
        </Box>
        <Box className={classes.divider} />
        <Formik
          initialValues={{
            name: "",
            email: "",
            phoneNumber: "",
            bookingUserOther: false,
            contactName: "",
          }}
          onSubmit={async (values) => {
            try {
              let lastBookingHotel =
                JSON.parse(localStorage.getItem(LAST_BOOKING_HOTEL)) || {};
              let dataDTO = {
                ...getDataForBookingFinal(values),
                customer: {
                  email: values.email,
                  name: values.name,
                  phoneNumber: values.phoneNumber,
                },
                invoiceInfo: !isEmpty(requestOther.invoiceInfo)
                  ? {
                      ...requestOther.invoiceInfo,
                      customerName: requestOther.invoiceInfo.recipientName,
                      recipientPhone: values.phoneNumber,
                    }
                  : {},
                invoiceRequest: !isEmpty(requestOther.invoiceInfo),
              };

              setLoadingSubmit(true);
              let data = {};
              if (!isEmpty(lastBookingHotel)) {
                const dataRes = await updateBookingsBook({
                  ...dataDTO,
                  orderToken: lastBookingHotel.orderToken,
                  bookingId: lastBookingHotel.bookingId,
                  isFinal: true,
                });
                data = dataRes.data || {};
              } else {
                const dataRes = await createBookingsBook({
                  ...dataDTO,
                  isFinal: true,
                });
                data = dataRes.data || {};
              }
              if (data.code === 200) {
                if (roomRate.rates[0].requestPrice) {
                  // case request price
                  createPayment(lastBookingHotel.orderCode);
                } else {
                  // save info booking to storage
                  saveInfoBookingToStorage(dataDTO);
                  let queryUrl = {
                    hotelId: paramsInit.hotelId,
                  };
                  if (isEmpty(lastBookingHotel)) {
                    const dataBooking = data.data || {};
                    lastBookingHotel = {
                      ...lastBookingHotel,
                      bookingId: dataBooking.id,
                      orderToken: dataBooking.orderToken,
                      orderCode: dataBooking.orderCode,
                    };
                  }
                  if (!isEmpty(promotionActive.code)) {
                    queryUrl = {
                      ...queryUrl,
                      promotionCode: promotionActive?.code || "",
                    };
                    lastBookingHotel = {
                      ...lastBookingHotel,
                      promotionCode: promotionActive?.code,
                      discountPrice: promotionDiscount?.discount,
                    };
                  }
                  localStorage.setItem(
                    LAST_BOOKING_HOTEL,
                    JSON.stringify(lastBookingHotel)
                  );
                  router.push({
                    pathname: routeStatic.PAYMENT_HOTEL.href,
                    query: queryUrl,
                  });
                }
              } else {
                handleErrorBooking(data);
              }
            } catch (error) {
              console.log("error", error);
              setLoadingSubmit(false);
            }
          }}
          validationSchema={storeSchema}
        >
          {() => {
            return (
              <Form>
                <InfoContactCustomer />
                <Box className={classes.divider} />
                <Box
                  display="flex"
                  flexDirection="column"
                  p="16px 25px 16px 16px"
                >
                  <Box
                    component="span"
                    lineHeight="19px"
                    fontWeight={600}
                    fontSize={16}
                    pb={6 / 8}
                  >
                    {listString.IDS_MT_TEXT_REQUEST_SPECIAL}
                  </Box>
                  <Box component="span" lineHeight="17px" pb={6 / 8}>
                    {listString.IDS_MT_SELECT_BED_TYPE}
                  </Box>
                  {!isEmpty(roomRate?.rates) &&
                    getListBed(roomRate?.rates[0]).map((el) => (
                      <Box
                        className={classes.itemBed}
                        key={el.shrui}
                        onClick={() => {
                          setShrui(el.shrui);
                        }}
                      >
                        <Box component="span">{el.bedInfo}</Box>
                        <Box display="flex" alignItems="center">
                          {el.numberBed > 2 ? (
                            <Image
                              srcImage={`${prefixUrlIcon}${
                                el.bedTypes.includes("twin")
                                  ? listIcons.Icon3SingleBed
                                  : listIcons.IconDoubleSingleBed
                              }`}
                              className={classes.iconDoubleSingleBed}
                            />
                          ) : (
                            el.bedTypes.map((item, index) => (
                              <Box
                                key={index.toString()}
                                display="flex"
                                pr={2 / 8}
                              >
                                {item.toLowerCase().includes("twin") ? (
                                  <IconBedSingle
                                    className={`svgFillAll ${classes.iconBedDouble}`}
                                  />
                                ) : (
                                  <IconBedDouble
                                    className={`svgFillAll ${classes.iconBedDouble}`}
                                  />
                                )}
                              </Box>
                            ))
                          )}
                          <Radio
                            checkedIcon={checkIcon(classes.iconRadio)}
                            icon={<span className={classes.iconRadio} />}
                            checked={shrui === el.shrui}
                          />
                        </Box>
                      </Box>
                    ))}
                  <Box
                    display="flex"
                    pt={12 / 8}
                    alignItems="center"
                    onClick={toggleDrawer(listTypeModal.MODAL_REQUEST_SPECIAL)}
                  >
                    <Box
                      component="span"
                      color="blue.blueLight8"
                      lineHeight="17px"
                    >
                      {listString.IDS_MT_TEXT_INVOICING_AND_OTHER_REQUEST}
                    </Box>
                    <IconArrowDownToggle
                      className={`svgFillAll ${classes.iconArrowRight}`}
                    />
                  </Box>
                  {checkShowInvoice() && (
                    <Box color="gray.grayDark8" lineHeight="17px" pt={6 / 8}>
                      Để xuất hoá đơn, quý khách vui lòng liên hệ trực tiếp với
                      khách sạn
                    </Box>
                  )}
                </Box>
                <Box className={classes.divider} />
                {!isEmpty(roomRate?.rates) &&
                  roomRate.rates.map((el, index) => (
                    <InfoPrice
                      key={index.toString()}
                      listPromotion={listPromotion}
                      promotionActive={promotionActive}
                      handleSetVoucher={handleSetVoucher}
                      hasBorder={true}
                      promotionDiscount={promotionDiscount || {}}
                      paramsInit={paramsInit}
                      rate={el}
                      handleUseEnterCode={handleUseEnterCode}
                      showTooTipPromotion={showTooTipPromotion}
                      statusTooltipAddPromotion={statusTooltipAddPromotion}
                      loadingSubmit={loadingSubmit}
                    />
                  ))}
              </Form>
            );
          }}
        </Formik>
        <RequestSpecial
          open={visibleDrawerType === listTypeModal.MODAL_REQUEST_SPECIAL}
          toggleDrawer={toggleDrawer}
          handleUseRequestOther={handleUseRequestOther}
          requestOther={requestOther}
          roomRate={roomRate}
        />
        <AlmBookingModal
          open={visibleDrawerType === listTypeModal.MODAL_ALM_BOOKING}
          toggleDrawer={toggleDrawer}
          messageError={messageError}
        />
      </Box>
    </Layout>
  );
};

export default Booking;
