import clsx from "clsx";
import moment from "moment";
import { useState, useEffect } from "react";
import { Box, makeStyles, Typography, useTheme } from "@material-ui/core";

import * as gtm from "@utils/gtm";
import { isEmpty } from "@utils/helpers";
import { listString, listEventHotel } from "@utils/constants";
import { IconArrowDownToggle, IconCloseInBackGround } from "@public/icons";

import ButtonComponent from "@src/button/Button";
import PromoCodeModal from "@components/mobile/hotels/booking/PromoCodeModal";
import BootstrapTooltip from "@components/common/bootstrapTooltip/BootstrapTooltip";

const useStyles = makeStyles((theme) => ({
  wrapContainer: {
    display: "flex",
    flexDirection: "column",
    fontSize: 14,
    color: theme.palette.black.black3,
    padding: 16,
  },
  itemPrice: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    lineHeight: "17px",
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
    padding: "14px 0 12px",
    position: "relative",
  },
  iconArrowRight: {
    transform: "rotate(270deg)",
    stroke: theme.palette.blue.blueLight8,
  },
  textUppercase: {
    textTransform: "uppercase",
  },
  totalPrice: {
    borderBottom: "none",
    marginBottom: 75,
  },
  boxFotter: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    zIndex: 1000,
    bottom: 0,
    left: 0,
    position: "fixed",
    color: "#000",
    background: "#fff",
    width: "100%",
    padding: "8px 16px",
    borderTop: "2px solid #EDF2F7",
  },
  wrapPromo: {
    height: 24,
    whiteSpace: "nowrap",
    display: "flex",
    alignItems: "center",
  },
  promo: {
    border: `1px solid ${theme.palette.gray.grayLight24}`,
    padding: "4px 6px",
    marginLeft: 8,
    borderRadius: 8,
  },
  textPromo: {
    fontSize: 14,
    lineHeight: "17px",
    display: "flex",
  },
  customTooltip: {
    background: theme.palette.green.greenLight7,
    padding: 8,
    borderRadius: 8,
    fontSize: 14,
    lineHeight: "17px",
  },
  arrowTooltip: {
    color: theme.palette.green.greenLight7,
  },
}));
const listTypeModal = {
  MODAL_PROMO_CODE: "MODAL_PROMO_CODE",
};
const InfoPrice = ({
  listPromotion = [],
  promotionActive = {},
  handleSetVoucher = () => {},
  handleUseEnterCode = () => {},
  promotionDiscount = {},
  paramsInit = {},
  rate = {},
  showTooTipPromotion = false,
  statusTooltipAddPromotion = {},
  loadingSubmit = false,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const [visibleDrawerType, setVisibleDrawerType] = useState("");

  const priceDetail = !isEmpty(rate) ? rate.priceDetail : {};
  const occupancies = !isEmpty(priceDetail) ? priceDetail.occupancies : [];

  useEffect(() => {
    if (promotionActive.type === "enter") {
      setVisibleDrawerType("");
    }
  }, [promotionActive]);

  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };

  const handleSelectPromo = (item) => {
    gtm.addEventGtm(listEventHotel.HotelAddPromo);
    setVisibleDrawerType("");
    handleSetVoucher(item);
  };

  const getNight = () => {
    let checkIn = moment(paramsInit.checkIn, "DD-MM-YYYY");
    let checkOut = moment(paramsInit.checkOut, "DD-MM-YYYY");
    if (checkIn && checkOut) {
      return Math.abs(
        moment(checkIn)
          .startOf("day")
          .diff(checkOut.startOf("day"), "days")
      );
    }
    return 1;
  };

  // tong gia chua app ma khuyen mai
  const totalPrice = () => {
    if (paramsInit.rooms && rate?.price && getNight()) {
      return paramsInit.rooms * getNight() * rate.price;
    } else {
      return 0;
    }
  };

  // tong gia cac dem
  const nightPrice = () => {
    if (paramsInit.rooms && rate.basePrice && getNight()) {
      return paramsInit.rooms * getNight() * rate.basePrice;
    } else {
      return 0;
    }
  };

  // giam gia makup
  const getDiscount = () => {
    return Math.abs(priceDetail?.discount) || 0;
  };

  // thue va phi dich vu khach san
  const vatPrice = () => {
    return totalPrice() - nightPrice() + getDiscount() - totalSurcharge();
  };

  // phu phi
  const totalSurcharge = () => {
    return occupancies
      .map((el) => el.totalSurcharge)
      .reduce((a, b) => a + b, 0);
  };

  // tong gia sau khi ap ma khuyen mai
  const totalPriceAfterDiscount = () => {
    if (!isEmpty(promotionDiscount)) {
      return totalPrice() - promotionDiscount?.discount;
    } else {
      return totalPrice();
    }
  };

  return (
    <Box className={classes.wrapContainer}>
      <Box
        component="span"
        color="black.black3"
        lineHeight="19px"
        fontWeight={600}
        fontSize={16}
        pb={12 / 8}
      >
        {listString.IDS_MT_TEXT_PRICE_DETAIL}
      </Box>

      <Box className={classes.itemPrice}>
        <Box component="span">{`${
          paramsInit.rooms
        } phòng x ${getNight()} đêm`}</Box>
        <Box component="span">{`${nightPrice().formatMoney()}đ`}</Box>
      </Box>

      {totalSurcharge() > 0 && (
        <Box className={classes.itemPrice}>
          <Box component="span">{`Phụ phí`}</Box>
          {`${totalSurcharge().formatMoney()}đ`}
        </Box>
      )}

      {vatPrice() > 2 && (
        <Box className={classes.itemPrice}>
          <Box component="span">{`${
            rate.includedVAT ? "Thuế và phí " : "Phí "
          }dịch vụ khách sạn`}</Box>
          {`${vatPrice().formatMoney()}đ`}
        </Box>
      )}

      {getDiscount() > 0 && (
        <Box className={classes.itemPrice} color="green.greenLight7">
          <Box component="span">{`Chúng tôi khớp giá, giảm thêm`}</Box>
          {`-${getDiscount().formatMoney()}đ`}
        </Box>
      )}

      {!isEmpty(promotionActive?.code) && (
        <Box
          className={classes.itemPrice}
          fontWeight={600}
          style={{ borderBottom: "4px solid #EDF2F7" }}
        >
          <Box component="span">{`Tổng giá phòng`}</Box>
          <Box component="span" fontSize={18} style={{ textAlign: "right" }}>
            {`${totalPrice().formatMoney()}đ`}
          </Box>
        </Box>
      )}

      <Box
        className={classes.itemPrice}
        style={{
          marginBottom: !isEmpty(promotionActive?.code) ? 85 : 0,
          borderBottom: !isEmpty(promotionActive?.code)
            ? "none"
            : "1px solid #EDF2F7",
        }}
      >
        <Box component="span" className={classes.wrapPromo}>
          {listString.IDS_TEXT_VOUCHER}{" "}
          {promotionActive?.code && (
            <Box component="span" className={classes.promo}>
              <Typography
                component="span"
                className={classes.textPromo}
                style={{
                  color: "#1A202C",
                }}
              >
                {promotionActive?.code?.toUpperCase()}
                <Box marginLeft={6 / 8}>
                  <IconCloseInBackGround
                    onClick={() => handleSetVoucher(promotionActive)}
                  />
                </Box>
              </Typography>
            </Box>
          )}
        </Box>
        <Box display="flex" alignItems="center" color="blue.blueLight8">
          {promotionActive?.code ? (
            <Typography
              component="span"
              className={classes.textPromo}
              style={{
                color: "#48BB78",
              }}
            >
              {!isEmpty(promotionDiscount) &&
                `-${promotionDiscount?.discount.formatMoney()}đ`}
            </Typography>
          ) : (
            <Box
              display="flex"
              alignItems="center"
              onClick={() => {
                gtm.addEventGtm(listEventHotel.HotelViewListPromo);
                setVisibleDrawerType(listTypeModal.MODAL_PROMO_CODE);
              }}
            >
              <Box component="span">
                {listString.IDS_MT_TEXT_TYPE_PROMO_CODE}
              </Box>
              <IconArrowDownToggle
                className={`svgFillAll ${classes.iconArrowRight}`}
              />
            </Box>
          )}
        </Box>

        <PromoCodeModal
          open={visibleDrawerType === listTypeModal.MODAL_PROMO_CODE}
          toggleDrawer={toggleDrawer}
          bgCircle={theme.palette.gray.grayLight22}
          hasBorder={false}
          listPromotion={listPromotion}
          promotionActive={promotionActive}
          handleSetVoucher={handleSelectPromo}
          handleUseEnterCode={handleUseEnterCode}
        />
      </Box>

      {isEmpty(promotionActive?.code) && (
        <Box
          className={clsx(classes.itemPrice, classes.totalPrice)}
          fontWeight={600}
        >
          <Box component="span">
            {listString.IDS_TEXT_ORDER_PAYMENT_TOTAL_PRICE}
          </Box>
          <Box display="flex" flexDirection="column" alignItems="flex-end">
            <Box component="span" fontSize={18}>{`${Math.max(
              totalPriceAfterDiscount(),
              0
            ).formatMoney()}đ`}</Box>
            <Box
              component="span"
              pt={4 / 8}
              fontSize={12}
              lineHeight="14px"
              fontWeight="normal"
              color="gray.grayDark8"
            >
              {rate.includedVAT
                ? "Đã bao gồm thuế, phí, VAT"
                : "Giá chưa bao gồm VAT"}
            </Box>
          </Box>
        </Box>
      )}

      <Box className={classes.boxFotter}>
        <BootstrapTooltip
          title={
            <Box component="span">
              Đã áp dụng mã
              <Box
                component="span"
                fontWeight={600}
              >{`  ${promotionActive?.code?.toUpperCase()} - giảm ${promotionDiscount?.discount?.formatMoney()} đ`}</Box>
            </Box>
          }
          placement="top"
          classes={{
            tooltip: classes.customTooltip,
            arrow: classes.arrowTooltip,
          }}
          open={showTooTipPromotion === statusTooltipAddPromotion.SET}
        >
          <Box display="flex" flexDirection="column">
            <Box
              component="span"
              fontSize={12}
              lineHeight="14px"
              color="gray.grayDark8"
            >
              {listString.IDS_TEXT_ORDER_PAYMENT_TOTAL_PRICE}
            </Box>
            <Box
              component="span"
              fontSize={20}
              fontWeight={600}
              lineHeight="24px"
            >
              {`${Math.max(totalPriceAfterDiscount(), 0).formatMoney()}đ`}
            </Box>
            {!isEmpty(promotionDiscount) && (
              <Box
                component="span"
                lineHeight="17px"
                style={{ textDecoration: "line-through" }}
              >
                {`${totalPrice().formatMoney()}đ`}
              </Box>
            )}
            <Box
              component="span"
              fontSize={12}
              lineHeight="14px"
              color="gray.grayDark8"
            >
              {rate.includedVAT
                ? "Đã bao gồm thuế, phí, VAT"
                : "Giá chưa bao gồm VAT"}
            </Box>
          </Box>
        </BootstrapTooltip>
        <ButtonComponent
          type="submit"
          backgroundColor="#FF1284"
          borderRadius={8}
          width="fit-content"
          height={48}
          padding="16px 30px"
          fontSize={16}
          fontWeight={600}
          loading={loadingSubmit}
        >
          {rate.requestPrice
            ? listString.IDS_MT_SEND_ORDER_ROOM
            : listString.IDS_MT_TEXT_CONTINUE}
        </ButtonComponent>
      </Box>
    </Box>
  );
};
export default InfoPrice;
