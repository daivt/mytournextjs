import { useEffect, useState } from "react";
import {
  Box,
  makeStyles,
  Drawer,
  Typography,
  IconButton,
  useTheme,
} from "@material-ui/core";
import moment from "moment";
import Image from "@src/image/Image";
import { listString, optionClean } from "@utils/constants";
import { IconClose } from "@public/icons";
import ButtonComponent from "@src/button/Button";
import { getDetailsPromotionCode } from "@api/hotels";
import { DATE_FORMAT } from "@utils/moment";
import sanitizeHtml from "sanitize-html";
import { unEscape } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  paperAnchorBottom: {
    borderRadius: 8,
    height: "100%",
  },
  iconClose: {
    position: "absolute",
    zIndex: 2,
    top: 19,
    left: 7,
    stroke: theme.palette.white.main,
  },
  wrapHeader: {
    position: "relative",
    width: "100%",
  },
  image: {
    height: 210,
    width: "100%",
  },
  wrapContent: {
    margin: "16px 16px",
    overflowY: "auto",
    height: "calc(100% - 58px)",
  },
  code: {
    background: theme.palette.blue.blueLight8,
    color: theme.palette.white.main,
    display: "inline-block",
    height: 24,
    borderRadius: 4,
    padding: "4px 8px 3px 8px",
  },
  description: {
    fontSize: 22,
    lineHeight: "26px",
    fontWeight: 600,
    color: theme.palette.black.black3,
    marginTop: 8,
  },
  exprired: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark7,
    marginTop: 12,
  },
  policy: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.black.black3,
    marginTop: 24,
  },
  wrapUse: {
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    position: "fixed",
    bottom: 0,
  },
  wrapBtn: {
    margin: "7px 16px",
  },
}));

const CodeInfoModal = ({
  openPromo = true,
  toggleDrawerPromo = () => {},
  handleUsePromotionCode = () => {},
  promoCodeSelected = {},
}) => {
  const [promoCodeDetail, setPromoCodeDetail] = useState({});
  useEffect(() => {
    fetchDetailsPromotionCode();
  }, [promoCodeSelected]);

  const fetchDetailsPromotionCode = async () => {
    try {
      if (promoCodeSelected?.id) {
        const { data } = await getDetailsPromotionCode({
          id: promoCodeSelected.id,
          objectType: promoCodeSelected.objectType,
        });
        setPromoCodeDetail(data?.data);
      }
    } catch (error) {}
  };
  const classes = useStyles();
  const theme = useTheme();
  return (
    <>
      <Drawer
        anchor="bottom"
        open={openPromo}
        onClose={toggleDrawerPromo("")}
        classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
      >
        <Box className={classes.wrapHeader}>
          <Image srcImage={promoCodeDetail?.banner} className={classes.image} />
          <Box className={`svgFillAll ${classes.iconClose}`}>
            <IconButton onClick={toggleDrawerPromo("")}>
              <IconClose />
            </IconButton>
          </Box>
        </Box>
        <Box className={classes.wrapContent}>
          <Typography variant="subtitle2" className={classes.code}>
            {promoCodeDetail?.code}
          </Typography>
          <Typography className={classes.description}>
            {promoCodeDetail?.title}
          </Typography>
          <Typography className={classes.exprired}>
            {listString.IDS_TEXT_DISCOUNT_CARD_EXPIRED}:
            {moment(promoCodeDetail?.toDate).format(DATE_FORMAT)}
          </Typography>
          <Box
            fontSize={14}
            color="gray.grayDark1"
            dangerouslySetInnerHTML={{
              __html: sanitizeHtml(
                unEscape(unescape(promoCodeDetail?.policy || "")),
                optionClean
              ),
            }}
          />
        </Box>
        <Box className={classes.wrapUse}>
          <ButtonComponent
            backgroundColor={theme.palette.secondary.main}
            height={48}
            width={343}
            fontSize={16}
            fontWeight={600}
            borderRadius={8}
            className={classes.wrapBtn}
            handleClick={handleUsePromotionCode}
          >
            {listString.IDS_TEXT_USE}
          </ButtonComponent>
        </Box>
      </Drawer>
    </>
  );
};
CodeInfoModal.propTypes = {};
export default CodeInfoModal;
