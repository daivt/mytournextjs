import { useEffect } from "react";
import { useRouter } from "next/router";
import { useFormikContext } from "formik";
import { Box, makeStyles } from "@material-ui/core";

import * as gtm from "@utils/gtm";
import { isEmpty } from "@utils/helpers";
import {
  listString,
  routeStatic,
  listEventHotel,
  LAST_BOOKING_HOTEL_INFO,
} from "@utils/constants";
import { validPhoneNumberRegex } from "@utils/regex";
import { useSystem } from "@contextProvider/ContextProvider";
import { IconAccountLogin, IconArrowDownToggle } from "@public/icons";

import FieldCheckboxContent from "@src/form/FieldCheckboxContent";
import FieldTextBaseContent from "@src/form/FieldTextBaseContent";

const useStyles = makeStyles((theme) => ({
  wrapContainer: {
    fontSize: 14,
    color: theme.palette.black.black3,
    padding: 16,
  },
  labelCheckBox: {
    fontSize: 14,
    fontWeight: "normal",
    color: theme.palette.gray.grayDark1,
  },
  formRoot: {
    marginLeft: -13,
  },
  boxLogin: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "rgba(0,182,243,0.1)",
    padding: "10px 12px",
    borderRadius: 8,
    margin: "12px 0 8px",
  },
  iconDown: {
    stroke: theme.palette.primary.main,
    transform: "rotate(270deg)",
  },
}));
const InfoContactCustomer = ({}) => {
  const classes = useStyles();
  const router = useRouter();
  const { systemReducer } = useSystem();
  const { setFieldValue, values } = useFormikContext();

  const informationUser = systemReducer.informationUser || {};
  useEffect(() => {
    const informationUser = systemReducer.informationUser || {};
    let lastBookingHotelInfo =
      JSON.parse(localStorage.getItem(LAST_BOOKING_HOTEL_INFO)) || {};
    if (!isEmpty(informationUser)) {
      setFieldValue("name", informationUser?.name || "");
      setFieldValue("phoneNumber", informationUser?.phoneInfo || "");
      setFieldValue("email", informationUser?.emailInfo || "");
      if (!isEmpty(lastBookingHotelInfo)) {
        setFieldValue(
          "bookingUserOther",
          lastBookingHotelInfo.bookingUserOther || false
        );
        setFieldValue(
          "contactName",
          lastBookingHotelInfo.bookingUserOther
            ? lastBookingHotelInfo.contactName
            : ""
        );
      }
    } else {
      if (!isEmpty(lastBookingHotelInfo)) {
        setFieldValue("name", lastBookingHotelInfo?.customer?.name || "");
        setFieldValue(
          "phoneNumber",
          lastBookingHotelInfo?.customer?.phoneNumber || ""
        );
        setFieldValue("email", lastBookingHotelInfo?.customer?.email || "");
        setFieldValue(
          "bookingUserOther",
          lastBookingHotelInfo.bookingUserOther || false
        );
        setFieldValue(
          "contactName",
          lastBookingHotelInfo.bookingUserOther
            ? lastBookingHotelInfo.contactName
            : ""
        );
      }
    }
  }, [systemReducer]);

  const handleRouteLogin = () => {
    router.push({
      pathname: routeStatic.LOGIN.href,
    });
  };

  const handleBlur = (type = "") => {
    if (type === "phoneNumber") {
      gtm.addEventGtm(listEventHotel.HotelInitCheckout);
    } else {
      gtm.addEventGtm(listEventHotel.HotelInitCheckout);
    }
  };

  return (
    <Box className={classes.wrapContainer}>
      <Box
        component="span"
        fontWeight={600}
        lineHeight="19px"
        fontSize={16}
        pb={1}
      >
        {listString.IDS_MT_TEXT_INFO_CONTACT}
      </Box>
      {isEmpty(informationUser) && (
        <Box className={classes.boxLogin} onClick={handleRouteLogin}>
          <Box display="flex" alignItems="center">
            <IconAccountLogin />
            <Box
              component="span"
              color="primary.main"
              fontWeight={600}
              pl={10 / 12}
            >
              {listString.IDS_MT_TEXT_LOGIN}
            </Box>
            <Box component="span" pl={4 / 8}>
              để đặt phòng nhanh hơn
            </Box>
          </Box>
          <Box>
            <IconArrowDownToggle className={`svgFillAll ${classes.iconDown}`} />
          </Box>
        </Box>
      )}

      <FieldTextBaseContent
        id="field-name"
        name="name"
        inputProps={{
          autoComplete: "off",
        }}
        label="Họ và tên"
      />
      <FieldTextBaseContent
        id="field-phoneNumber"
        name="phoneNumber"
        inputProps={{
          autoComplete: "off",
        }}
        label={listString.IDS_MT_TEXT_PHONE_NUMBER}
        onBlur={() => handleBlur("phoneNumber")}
        onChange={(e) => {
          validPhoneNumberRegex.test(e.target.value) &&
            setFieldValue("phoneNumber", e.target.value);
        }}
      />
      <FieldTextBaseContent
        id="field-email"
        name="email"
        inputProps={{
          autoComplete: "off",
        }}
        label={listString.IDS_MT_TEXT_EMAIL}
        onBlur={() => handleBlur("email")}
      />
      <FieldCheckboxContent
        id="field-bookingUserOther"
        name="bookingUserOther"
        label={listString.IDS_MT_ORDER_ROOM_FOR_OTHER_CUSTOMER}
        classes={{ label: classes.labelCheckBox, root: classes.formRoot }}
      />

      {values.bookingUserOther && (
        <>
          <Box pt={12 / 8} fontWeight={600} lineHeight="17px">
            {listString.IDS_MT_OTHER_CUSTOMER_INFO}
          </Box>
          <FieldTextBaseContent
            id="field-contactName"
            name="contactName"
            inputProps={{
              autoComplete: "off",
            }}
            label={listString.IDS_MT_TEXT_CONTACT_NAME_SIMPLE}
          />
        </>
      )}
    </Box>
  );
};
export default InfoContactCustomer;
