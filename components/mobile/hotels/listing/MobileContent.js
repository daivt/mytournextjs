import { useRouter } from "next/router";
import { Box, LinearProgress } from "@material-ui/core";
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import InfiniteScroll from "react-infinite-scroller";

import { getHotelsAvailability } from "@api/hotels";
import {
  adapterHotelAvailability,
  isEmpty,
  checkAndChangeCheckIO,
} from "@utils/helpers";

import {
  routeStatic,
  DELAY_TIMEOUT_POLLING,
  RADIUS_GET_HOTEL_LAT_LONG,
  LAST_SEARCH_HISTORY,
} from "@utils/constants";
import Layout from "@components/layout/mobile/Layout";
import LoadingMore from "@components/common/loading/LoadingMore";
import HotelItemListing from "@components/mobile/hotels/HotelItemListing";
import HotelHeaderListing from "@components/mobile/hotels/listing/HotelHeaderListing";
import SkeletonItemListingMobile from "@components/common/desktop/skeleton/SkeletonItemListingMobile";
import NoResultSearchListing from "@components/mobile/hotels/listing/NoResultSearchListing";

const useStyles = makeStyles((theme) => ({
  headerListing: {
    position: "fixed",
    backgroundColor: theme.palette.white.main,
    zIndex: 20,
    top: 0,
    width: "100%",
  },
  styleHotelItem: {
    padding: "12px 0",
  },
  deveide: {
    height: 6,
    width: "calc(100% + 24px)",
    backgroundColor: theme.palette.gray.grayLight22,
    margin: "0 -12px",
  },
  loadingContent: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "calc(100vh - 94px)",
  },
  rootLinearProgress: {
    height: 2,
    // borderRadius: 5,
  },
  bannerImg: {
    width: "calc(100% - 24px)",
    maxHeight: 84,
    borderRadius: 8,
    margin: "0 12px 0",
  },
}));
const pageSize = 20;
let timer = null;
const listStatus = {
  INIT_STATE: "INIT_STATE",
  FIRST_REQUEST: "FIRST_REQUEST",
  LAST_REQUEST: "LAST_REQUEST",
};

const getParamInit = (paramsQuery = {}) => {
  let paramsUrlTemp = {
    checkIn: checkAndChangeCheckIO(paramsQuery.checkIn, "checkIn"),
    checkOut: checkAndChangeCheckIO(paramsQuery.checkOut, "checkOut"),
    adults: parseInt(paramsQuery.adults),
    rooms: parseInt(paramsQuery.rooms),
    children: parseInt(paramsQuery.children),
  };
  if (!isEmpty(paramsQuery.childrenAges)) {
    paramsUrlTemp = {
      ...paramsUrlTemp,
      childrenAges: paramsQuery.childrenAges,
    };
  }
  if (paramsQuery.aliasCode === "ksgb") {
    paramsUrlTemp = {
      ...paramsUrlTemp,
      latitude: paramsQuery.latitude,
      longitude: paramsQuery.longitude,
      radius: RADIUS_GET_HOTEL_LAT_LONG,
    };
  }
  return paramsUrlTemp;
};

const ListingMobile = ({
  paramsQuery = {},
  filtersQuery = {},
  sortOptions = [],
  location = {},
  filters = {},
  items = [],
  totalHotels = 0,
  timeServer = "",
}) => {
  const router = useRouter();
  const classes = useStyles();
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(true);
  const [dataHotel, setDataHotel] = useState(items);
  const [totalHotel, setTotalHotel] = useState(totalHotels);
  const [progress, setProgress] = useState(0);
  const [paramsUrl, setParamsUrl] = useState(getParamInit(paramsQuery));
  const [sortAndFilters, setSortAndFilters] = useState({
    sortOptions: sortOptions,
    filters: filters,
  });
  const [status, setStatus] = useState(listStatus.INIT_STATE);
  let breakPolling = true;

  let sortQueryUrl = {};
  if (!isEmpty(router.query.sortBy)) {
    sortQueryUrl = {
      ...sortQueryUrl,
      sortBy: router.query.sortBy || "",
    };
  }

  useEffect(() => {
    console.log("timeServer", timeServer);
    setStatus(listStatus.INIT_STATE);
    setPage(1);
    setLoading(true);
    setDataHotel(items);
    setTotalHotel(totalHotels);
    setProgress(0);
    let paramsUrlTemp = { ...paramsUrl };
    const lastSearchHistory =
      JSON.parse(localStorage.getItem(LAST_SEARCH_HISTORY)) || {};
    if (checkIsEmptyParamsQuery()) {
      if (!isEmpty(lastSearchHistory)) {
        paramsUrlTemp = {
          ...paramsUrlTemp,
          checkIn: checkAndChangeCheckIO(lastSearchHistory.checkIn, "checkIn"),
          checkOut: checkAndChangeCheckIO(
            lastSearchHistory.checkOut,
            "checkOut"
          ),
          adults: parseInt(lastSearchHistory.adults) || 0,
          rooms: parseInt(lastSearchHistory.rooms) || 0,
          children: parseInt(lastSearchHistory.children) || 0,
          childrenAges: lastSearchHistory.childrenAges || [],
        };
        setParamsUrl(paramsUrlTemp);
      }
    }
    fetData(paramsUrlTemp);
  }, [items]);

  useEffect(() => {
    return () => {
      breakPolling = false;
      clearInterval(timer);
    };
  }, []);

  const fetData = async (paramsUrl) => {
    let polling = true;
    try {
      setTimer();
      while (polling && breakPolling) {
        const { data } = await getHotelsAvailability({
          page: 1,
          size: pageSize,
          filters: filtersQuery,
          ...paramsUrl,
          ...sortQueryUrl,
          searchType: paramsQuery.aliasCode === "ksgb" ? "latlon" : "location",
          aliasCode: paramsQuery.aliasCode,
        });

        if (data.code === 200) {
          if (data?.data?.total !== 0) {
            setDataHotel(data?.data?.items || []);
            setTotalHotel(data?.data?.total);
            if (status === listStatus.INIT_STATE) {
              setStatus(listStatus.FIRST_REQUEST);
            } else if (status === listStatus.FIRST_REQUEST) {
              setStatus(listStatus.LAST_REQUEST);
            }
          }
          if (data.data.completed) {
            if (status === listStatus.INIT_STATE) {
              setStatus(listStatus.FIRST_REQUEST);
            } else if (status === listStatus.FIRST_REQUEST) {
              setStatus(listStatus.LAST_REQUEST);
            }
            setSortAndFilters({
              sortOptions: data?.data?.sortOptions || [],
              filters: data?.data?.filters || {},
            });
            clearInterval(timer);
            setProgress(100);
            polling = false;
            break;
          } else {
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          if (status === listStatus.INIT_STATE) {
            setStatus(listStatus.FIRST_REQUEST);
          } else if (status === listStatus.FIRST_REQUEST) {
            setStatus(listStatus.LAST_REQUEST);
          }
          clearInterval(timer);
          setProgress(100);
          polling = false;
        }
      }
    } catch (error) {
      if (status === listStatus.INIT_STATE) {
        setStatus(listStatus.FIRST_REQUEST);
      } else if (status === listStatus.FIRST_REQUEST) {
        setStatus(listStatus.LAST_REQUEST);
      }
      clearInterval(timer);
      setProgress(100);
      polling = false;
    }
  };

  const setTimer = () => {
    timer = setInterval(() => {
      setProgress((oldProgress) => {
        if (oldProgress === 90) {
          clearInterval(timer);
          return oldProgress;
        }
        const diff = Math.random() * 10;
        return Math.min(oldProgress + diff, 90);
      });
    }, 700);
  };
  const loadFunc = async () => {
    let polling = true;
    if (dataHotel.length !== 0) {
      const pageTemp = page + 1;
      try {
        setLoading(false);
        while (polling && breakPolling) {
          const { data } = await getHotelsAvailability({
            page: pageTemp,
            size: pageSize,
            filters: filtersQuery,
            ...paramsUrl,
            ...sortQueryUrl,
            searchType:
              paramsQuery.aliasCode === "ksgb" ? "latlon" : "location",
            aliasCode: paramsQuery.aliasCode,
          });
          if (data.code === 200) {
            if (data.data.completed) {
              setPage(pageTemp);
              setDataHotel([...dataHotel, ...data?.data?.items]);
              setTotalHotel(data?.data.total);
              if (pageTemp * pageSize < data?.data.total) {
                setLoading(true);
              }
              polling = false;
              break;
            }
            await new Promise((resolve) =>
              setTimeout(resolve, DELAY_TIMEOUT_POLLING)
            );
          } else {
            polling = false;
          }
        }
      } catch (error) {
        polling = false;
      }
    }
  };

  const handleFilterSearch = (filters) => {
    let filterQueryUrl = {};
    const keys = Object.keys(filters);
    keys.forEach((el) => {
      if (!isEmpty(filters[el])) {
        if (el === "priceRanges") {
          if (
            filters.priceRanges[0].minPrice !== 0 ||
            filters.priceRanges[0].maxPrice !== 5000000
          ) {
            filterQueryUrl = {
              ...filterQueryUrl,
              minPrice: filters.priceRanges[0].minPrice,
              maxPrice: filters.priceRanges[0].maxPrice,
            };
          }
        } else {
          filterQueryUrl = {
            ...filterQueryUrl,
            [el]: filters[el].toString(),
          };
        }
      }
    });
    let paramsSearch = {};
    if (!checkIsEmptyParamsQuery()) {
      paramsSearch = {
        checkIn: paramsUrl.checkIn,
        checkOut: paramsUrl.checkOut,
        adults: paramsUrl.adults,
        rooms: paramsUrl.rooms,
        children: paramsUrl.children,
      };
      if (!isEmpty(paramsUrl.childrenAges)) {
        paramsSearch = {
          ...paramsSearch,
          childrenAges: paramsUrl.childrenAges.toString(),
        };
      }
    }

    router
      .push({
        pathname: routeStatic.LISTING_HOTEL.href,
        query: {
          slug: [`${paramsQuery.aliasCode}`, `${paramsQuery.listing}`],
          ...paramsSearch,
          ...filterQueryUrl,
          ...sortQueryUrl,
        },
      })
      .then(() => window.scrollTo(0, 0));
  };

  const checkIsEmptyParamsQuery = () => {
    return !(
      router.query.checkIn ||
      router.query.checkOut ||
      router.query.adults ||
      router.query.rooms ||
      router.query.children
    );
  };

  return (
    <Layout
      title={`Khách sạn giá tốt ở ${
        paramsQuery.aliasCode === "ksgb" ? "gần bạn" : location.name
      } từ ${paramsUrl.checkIn} đến ${
        paramsUrl.checkOut
      } | Đảm bảo giá tốt nhất`}
    >
      <Box position="relative" height={94}>
        <Box className={classes.headerListing}>
          <HotelHeaderListing
            paramsUrl={paramsUrl}
            paramsQuery={paramsQuery}
            filtersQuery={filtersQuery}
            sortOptions={sortAndFilters.sortOptions}
            location={location}
            filters={sortAndFilters.filters}
            items={dataHotel}
            handleFilterSearch={handleFilterSearch}
          />
          {progress !== 100 && (
            <LinearProgress
              classes={{ root: classes.rootLinearProgress }}
              color="secondary"
              value={progress}
              variant="determinate"
            />
          )}
        </Box>
      </Box>
      <Box id="banner_listing" className={classes.bannerImg} />

      {totalHotel === 0 ? (
        status === listStatus.INIT_STATE ? (
          [1, 2, 3, 4, 5].map((el) => <SkeletonItemListingMobile key={el} />)
        ) : (
          <NoResultSearchListing />
        )
      ) : (
        <Box px={12 / 8} position="relative">
          <InfiniteScroll
            pageStart={page}
            loadMore={loadFunc}
            hasMore={loading}
          >
            {adapterHotelAvailability(dataHotel).map((el, index) => (
              <Box key={el.id}>
                <HotelItemListing
                  styleHotelItem={classes.styleHotelItem}
                  item={el}
                  paramsFilterInit={paramsQuery}
                />
                {index !== dataHotel.length - 1 && (
                  <Box className={classes.deveide} />
                )}
              </Box>
            ))}
            {!loading && page * pageSize < totalHotel && <LoadingMore />}
          </InfiniteScroll>
        </Box>
      )}
    </Layout>
  );
};

export default ListingMobile;
