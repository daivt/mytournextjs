import { Box, Typography } from "@material-ui/core";
import makeStyles from "@material-ui/styles/makeStyles";
import { listString } from "@utils/constants";
import Image from "@src/image/Image";
const useStyles = makeStyles((theme) => ({
  wrapResultSearch: {
    marginTop: 155,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
  result: {
    marginTop: 16,
  },
  textSearchAgain: {
    marginTop: 8,
    color: theme.palette.black.black3,
    fontWeight: "normal",
  },
  imageEmpty: {
    width: 193,
    height: 170,
  },
}));

const NoResultSearchListing = () => {
  const classes = useStyles();
  return (
    <>
      <Box className={classes.wrapResultSearch}>
        <Box className={classes.wrapIconResult}>
          <Image
            srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_no_result_search_listing.svg"
            className={classes.imageEmpty}
          />
        </Box>
        <Typography variant="subtitle1" className={classes.result}>
          {listString.IDS_MT_TEXT_NO_SEARCH_HOTEL}
        </Typography>
        <Typography variant="subtitle2" className={classes.textSearchAgain}>
          {listString.IDS_MT_TEXT_PLEASE_CHANGE_INFO_SEARCH}
        </Typography>
      </Box>
    </>
  );
};
NoResultSearchListing.propTypes = {};
export default NoResultSearchListing;
