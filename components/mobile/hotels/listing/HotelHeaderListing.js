import moment from "moment";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Box, IconButton } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/styles";

import {
  IconArrowLeft,
  IconDot,
  IconSort,
  IconMap,
  IconFilter,
} from "@public/icons";
import { isEmpty } from "@utils/helpers";
import { dataUserVote } from "@utils/dataFake";
import { listString, formatDate, routeStatic } from "@utils/constants";

import ButtonComponent from "@src/button/Button";
import MapDialog from "@components/common/map/MapDialog";
import SortHotelListing from "@components/common/modal/hotel/SortHotelListing";
import FilterHotelListing from "@components/mobile/hotels/listing/FilterHotelListing";

const useStyles = makeStyles((theme) => ({
  wrapHeaderListing: {
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  containerInfoSearch: {
    height: 48,
    width: 375,
    background: theme.palette.white.main,
    display: "flex",
    alignItems: "center",
  },
  containerInfoFilter: {
    height: 44,
    background: theme.palette.white.main,
    color: theme.palette.black.black3,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    position: "relative",
  },
  btnBack: {
    position: "absolute",
    left: 0,
  },
  iconArrowLeft: {
    display: "flex ",
    alignItems: "center",
  },
  infoSearch: {
    display: "flex",
    color: theme.palette.black.black3,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: "0 auto",
  },
  location: {
    fontSize: 16,
    fontWeight: 600,
  },
  checkIn: {
    color: theme.palette.black.black4,
    fontSize: 12,
  },
  wrapInfoCheckIn: {
    display: "flex",
    alignItems: "center",
    marginTop: 2,
  },
  wrapFilter: {
    display: "flex",
  },
  filter: {
    display: "flex",
    alignItems: "center",
  },
  filterText: {
    fontSize: 14,
    position: "relative",
    margin: "0 8px 0 4px",
  },
  dotFilter: {
    width: 7,
    height: 7,
    background: theme.palette.red.redLight5,
    position: "absolute",
    top: 0,
    borderRadius: "50%",
  },
  sort: {
    display: "flex",
    alignItems: "center",
  },
  wrapMap: {
    display: "flex",
    alignItems: "center",
  },
  map: {
    display: "flex",
    alignItems: "center",
    position: "absolute",
    right: 16,
  },
  mapText: {
    color: theme.palette.blue.blueLight8,
  },
}));

const listTypeDrawer = {
  SORT_DRAWER: "SORT_DRAWER",
  FILTER_DRAWER: "FILTER_DRAWER",
};
const HotelHeaderListing = ({
  paramsUrl = {},
  paramsQuery = {},
  filtersQuery = {},
  sortOptions = [],
  location = {},
  filters = {},
  items = [],
  handleFilterSearch = () => {},
}) => {
  const router = useRouter();
  const classes = useStyles();
  const theme = useTheme();
  const [visibleDrawerType, setVisibleDrawerType] = useState("");
  const [sortBy, setSortBy] = useState("");

  useEffect(() => {
    setSortBy(
      !isEmpty(router.query.sortBy)
        ? router.query.sortBy
        : !isEmpty(sortOptions)
        ? sortOptions[0].code
        : ""
    );
  }, [router.query]);

  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };

  const handleBackRoute = () => {
    router.back();
  };

  const handleSelectedItemSort = (sort) => () => {
    setSortBy(sort);
    setVisibleDrawerType("");
    const keys = Object.keys(filtersQuery);
    let filterQueryUrl = {};
    keys.forEach((el) => {
      if (!isEmpty(filtersQuery[el])) {
        if (el === "priceRanges") {
          if (
            filtersQuery.priceRanges[0].minPrice !== 0 ||
            filtersQuery.priceRanges[0].maxPrice !== 5000000
          ) {
            filterQueryUrl = {
              ...filterQueryUrl,
              minPrice: filtersQuery.priceRanges[0].minPrice,
              maxPrice: filtersQuery.priceRanges[0].maxPrice,
            };
          }
        } else if (el === "ratingRanges") {
          let ratingRangesTemp = [];
          filtersQuery.ratingRanges.forEach((el) => {
            dataUserVote.forEach((item) => {
              if (
                item.value.minRating === el.minRating &&
                item.value.maxRating === el.maxRating
              ) {
                ratingRangesTemp.push(item.id);
              }
            });
          });
          filterQueryUrl = {
            ...filterQueryUrl,
            ratingRanges: ratingRangesTemp.toString(),
          };
        } else {
          filterQueryUrl = {
            ...filterQueryUrl,
            [el]: filtersQuery[el].toString(),
          };
        }
      }
    });
    let paramsSearch = {};
    if (!checkIsEmptyParamsQuery()) {
      paramsSearch = {
        checkIn: paramsUrl.checkIn,
        checkOut: paramsUrl.checkOut,
        adults: paramsUrl.adults,
        rooms: paramsUrl.rooms,
        children: paramsUrl.children,
      };
      if (!isEmpty(paramsUrl.childrenAges)) {
        paramsSearch = {
          ...paramsSearch,
          childrenAges: paramsUrl.childrenAges.toString(),
        };
      }
    }
    router
      .push({
        pathname: routeStatic.LISTING_HOTEL.href,
        query: {
          slug: [`${paramsQuery.aliasCode}`, `${paramsQuery.listing}`],
          ...paramsSearch,
          ...filterQueryUrl,
          sortBy: sort,
        },
      })
      .then(() => window.scrollTo(0, 0));
  };

  const checkIsEmptyParamsQuery = () => {
    return !(
      router.query.checkIn ||
      router.query.checkOut ||
      router.query.adults ||
      router.query.rooms ||
      router.query.children
    );
  };

  const checkHasFilter = () => {
    if (!isEmpty(filtersQuery)) {
      return (
        !isEmpty(filtersQuery.amenities) ||
        !isEmpty(filtersQuery.priceRanges) ||
        !isEmpty(filtersQuery.ratingRanges) ||
        !isEmpty(filtersQuery.stars) ||
        !isEmpty(filtersQuery.services) ||
        !isEmpty(filtersQuery.types) ||
        !isEmpty(filtersQuery.subLocations)
      );
    }
    return false;
  };

  return (
    <>
      <Box className={classes.wrapHeaderListing}>
        <Box className={classes.containerInfoSearch}>
          <Box className={classes.iconArrowLeft}>
            <IconButton className={classes.btnBack} onClick={handleBackRoute}>
              <IconArrowLeft />
            </IconButton>
          </Box>
          <Box className={classes.infoSearch}>
            <Box component="span" className={classes.location}>
              {paramsQuery.aliasCode === "ksgb"
                ? "Khách sạn gần bạn"
                : location.name}
            </Box>
            <Box className={classes.wrapInfoCheckIn}>
              <Box component="span" className={classes.checkIn}>
                {moment(paramsUrl.checkIn, formatDate.firstDay).format(
                  formatDate.dayAndMonth
                )}{" "}
                -{" "}
                {moment(paramsUrl.checkOut, formatDate.firstDay).format(
                  formatDate.dayAndMonth
                )}
              </Box>
              <Box component="span" margin={4 / 8} display="flex">
                <IconDot />
              </Box>
              <Box component="span" className={classes.checkIn}>
                {paramsUrl.rooms || 0} phòng
              </Box>
              <Box component="span" margin={4 / 8} display="flex">
                <IconDot />
              </Box>
              <Box component="span" className={classes.checkIn}>
                {Number(paramsUrl.adults || 0) +
                  Number(paramsUrl.children || 0)}{" "}
                khách
              </Box>
            </Box>
          </Box>
        </Box>
        <Box className={classes.containerInfoFilter}>
          <Box className={classes.wrapFilter}>
            <ButtonComponent
              handleClick={toggleDrawer(listTypeDrawer.FILTER_DRAWER)}
              backgroundColor="inherit"
              width="fit-content"
              color={theme.palette.black.black3}
              padding="6px 0 6px 8px"
            >
              <IconFilter />
              <Box component="span" className={classes.filterText}>
                {listString.IDS_MT_TEXT_FILTER_SINGLE}{" "}
                {checkHasFilter() && (
                  <Box component="span" className={classes.dotFilter} />
                )}
              </Box>
            </ButtonComponent>
            <ButtonComponent
              handleClick={toggleDrawer(listTypeDrawer.SORT_DRAWER)}
              backgroundColor="inherit"
              width="fit-content"
              color={theme.palette.black.black3}
            >
              <IconSort />
              <Box component="span" fontSize={14} pl={2 / 8}>
                {listString.IDS_MT_TEXT_SORT}
              </Box>
            </ButtonComponent>
          </Box>
          <Box className={classes.wrapMap}>
            <Box className={classes.map}>
              <MapDialog
                child={
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <IconMap />
                    <Box component="span" className={classes.mapText}>
                      {listString.IDS_MT_TEXT_MAP}
                    </Box>
                  </div>
                }
                items={items}
                location={location}
                paramsFilterInit={paramsQuery}
                paramsUrl={paramsUrl}
                filters={filters}
              />
            </Box>
          </Box>
        </Box>
      </Box>
      <FilterHotelListing
        open={visibleDrawerType === listTypeDrawer.FILTER_DRAWER}
        toggleDrawer={toggleDrawer}
        filtersData={filters}
        handleFilterSearch={(filters) => {
          setVisibleDrawerType("");
          handleFilterSearch(filters);
        }}
      />
      <SortHotelListing
        open={visibleDrawerType === listTypeDrawer.SORT_DRAWER}
        toggleDrawer={toggleDrawer}
        sortOptions={sortOptions}
        sortBy={sortBy}
        handleSelectedItem={handleSelectedItemSort}
      />
    </>
  );
};
HotelHeaderListing.propTypes = {};

export default HotelHeaderListing;
