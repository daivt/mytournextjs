import clsx from "clsx";
import {
  Box,
  Drawer,
  IconButton,
  makeStyles,
  Typography,
  Radio,
  Checkbox,
} from "@material-ui/core";
import PropTypes from "prop-types";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { useTheme } from "@material-ui/core/styles";

import { isEmpty } from "@utils/helpers";
import { listString } from "@utils/constants";
import { dataItemStar, dataUserVote } from "@utils/dataFake";
import { IconClose, IconStar, IconArrowDownToggle } from "@public/icons";

import ButtonComponent from "@src/button/Button";
import RangerSliderPrice from "@src/slider/RangerSliderPrice";

const useStyles = makeStyles((theme) => ({
  paperAnchorLeft: {
    borderRadius: 0,
  },
  wrapContainer: {
    margin: "0px 16px",
  },
  mainDrawerFilter: {
    width: "100vw",
    color: theme.palette.black.black3,
    fontSize: 16,
    height: "100vh",
    overflow: "auto",
    "&::-webkit-scrollbar": {
      display: "none",
    },
  },
  wrapHeaderDraw: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    height: 48,
    borderBottom: "1px solid #EDF2F7",
    width: "100%",
  },
  textCancelFilter: {
    fontWeight: 400,
    color: theme.palette.blue.blueLight8,
  },
  wrapButtonClose: {
    padding: 0,
  },
  wraperHeader: {
    padding: "12px 16px",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    height: 48,
    display: "flex",
    alignItems: "center",
  },
  wrapStar: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 12,
  },
  star: {
    width: 77,
    height: 40,
    borderRadius: 8,
    border: `1px solid ${theme.palette.gray.grayLight25}`,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  activeStar: {
    width: 77,
    height: 40,
    borderRadius: 8,
    border: `1px solid ${theme.palette.blue.blueLight8}`,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: theme.palette.blue.blueLight9,
  },
  starSvg: {
    fill: theme.palette.gray.grayDark8,
  },
  activeStarSvg: {
    fill: theme.palette.blue.blueLight8,
  },
  wrapServiceAttach: {
    marginTop: 26,
  },
  serviceAttach: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
    color: theme.palette.black.black3,
    height: 47,
  },
  icon: {
    borderRadius: 6,
    width: 22,
    height: 22,
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
    "$root.Mui-focusVisible &": {
      outline: "2px auto #00B6F3",
      outlineOffset: 2,
    },
  },
  iconRadio: {
    borderRadius: "50%",
    width: 22,
    height: 22,
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
  },
  checkedIconRadio: {
    backgroundColor: "#00B6F3",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: 22,
      height: 22,
      backgroundImage: "url(/icons/icon_checked.svg)",
      backgroundSize: "cover",
      content: '""',
    },
  },
  IconArrowDownToggle: {
    stroke: theme.palette.blue.blueLight8,
    marginLeft: 8,
  },
  loadMore: {
    display: "flex",
    color: theme.palette.blue.blueLight8,
    alignItems: "center",
    marginTop: 14,
  },
  IconArrowUpToggle: {
    stroke: theme.palette.blue.blueLight8,
    marginLeft: 8,
    transform: "rotate(180deg)",
  },
  wrapBtnFilter: {
    height: 64,
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    display: "flex",
    alignItems: "center",
    marginTop: 73,
    padding: "0 16px",
  },
}));

const FilterHotelListing = ({
  open = true,
  toggleDrawer = () => {},
  filtersData = {},
  handleFilterSearch = () => {},
  isResetState = true,
  isFilterMap = false,
  props,
}) => {
  const router = useRouter();
  const theme = useTheme();
  const classes = useStyles();
  const [isLoadMoreLocation, setIsLoadMoreLocation] = useState(false);
  const [isLoadMoreAmenities, setIsLoadMoreAmenities] = useState(false);
  const query = router.query || {};
  const [filters, setFilters] = useState({
    priceRanges: [
      {
        minPrice: 0,
        maxPrice: 5000000,
      },
    ],
    stars: [],
    services: [],
    ratingRanges: [],
    types: [],
    subLocations: [],
    amenities: [],
  });
  useEffect(() => {
    setFiltersQuery();
  }, [query]);

  useEffect(() => {
    if (isResetState) {
      setFiltersQuery();
    }
  }, [open]);

  const setFiltersQuery = () => {
    setFilters({
      priceRanges: [
        {
          minPrice: !isEmpty(query.minPrice) ? parseInt(query.minPrice) : 0,
          maxPrice: !isEmpty(query.maxPrice)
            ? parseInt(query.maxPrice)
            : 5000000,
        },
      ],
      stars: !isEmpty(query.stars) ? query.stars.split(",").map(Number) : [],
      services: !isEmpty(query.services)
        ? query.services.split(",").map(Number)
        : [],
      ratingRanges: !isEmpty(query.ratingRanges)
        ? query.ratingRanges.split(",").map(Number)
        : [],
      types: !isEmpty(query.types) ? query.types.split(",").map(Number) : [],
      subLocations: !isEmpty(query.subLocations)
        ? query.subLocations.split(",").map(Number)
        : [],
      amenities: !isEmpty(query.amenities)
        ? query.amenities.split(",").map(Number)
        : [],
    });
  };
  const handleChangePrice = (event, newValue) => {
    setFilters({
      ...filters,
      priceRanges: [
        {
          minPrice: newValue[0],
          maxPrice: newValue[1],
        },
      ],
    });
  };
  const handleRatingStar = (id) => {
    let starsResult = [];
    if (filters.stars.includes(id)) {
      starsResult = filters.stars.filter((el) => el !== id);
      if (id === 2) {
        starsResult = starsResult.filter((el) => el !== 1);
      }
    } else {
      starsResult = [...filters.stars, id];
      if (id === 2) {
        starsResult = [...starsResult, 1];
      }
    }
    setFilters({
      ...filters,
      stars: starsResult,
    });
  };
  const handleChangeService = (id, type = "") => {
    let result = [];
    if (filters[type].includes(id)) {
      result = filters[type].filter((el) => el !== id);
    } else {
      result = [...filters[type], id];
    }
    setFilters({
      ...filters,
      [type]: result,
    });
  };
  const handleSelectSubLocation = (item) => {
    setFilters({
      ...filters,
      subLocations: [item.id],
    });
  };
  const handleFilterOk = () => {
    handleFilterSearch(filters);
  };
  return (
    <>
      <Drawer
        anchor="left"
        open={open}
        onClose={toggleDrawer(false)}
        classes={{ paperAnchorLeft: classes.paperAnchorLeft }}
      >
        <Box className={classes.mainDrawerFilter}>
          <Box className={classes.wraperHeader}>
            <Box className={classes.wrapHeaderDraw}>
              <IconButton
                onClick={toggleDrawer(false)}
                className={classes.wrapButtonClose}
              >
                <IconClose />
              </IconButton>
              <Typography variant="subtitle1" className={classes.textFilter}>
                {listString.IDS_MT_TEXT_FILTER}
              </Typography>
              <Typography
                variant="subtitle1"
                className={classes.textCancelFilter}
                onClick={() => handleFilterSearch({})}
              >
                {listString.IDS_MT_TEXT_CANCEL_FILTER}
              </Typography>
            </Box>
          </Box>
          <Box className={classes.wrapContainer}>
            <Box className={classes.wrapRangePrice}>
              <Typography variant="subtitle2" style={{ marginTop: "24px" }}>
                {listString.IDS_MT_TEXT_RANGE_PRICE_ONE_NIGHT}
              </Typography>
              <Typography
                variant="subtitle2"
                style={{ fontWeight: "normal", marginTop: "8px" }}
              >
                {`${filters.priceRanges[0].minPrice.formatMoney()}đ - `}
                {`${filters.priceRanges[0].maxPrice.formatMoney()}${
                  filters.priceRanges[0].maxPrice >= 5000000 ? "+ " : ""
                }đ`}
              </Typography>
              <Box pr={2}>
                <RangerSliderPrice
                  valueRange={[
                    filters.priceRanges[0].minPrice,
                    filters.priceRanges[0].maxPrice,
                  ]}
                  handleChangeRange={handleChangePrice}
                />
              </Box>
            </Box>
            <Typography variant="subtitle1" style={{ marginTop: 40 }}>
              {listString.IDS_MT_TEXT_STAR}
            </Typography>
            <Box className={classes.wrapStar}>
              {dataItemStar.map((el, key) => (
                <Box
                  className={
                    filters.stars.includes(el.id)
                      ? classes.activeStar
                      : classes.star
                  }
                  key={el?.id}
                  onClick={() => handleRatingStar(el.id)}
                >
                  {el?.star}
                  <Box marginLeft={4 / 8} width={15} height={15}>
                    <IconStar
                      className={
                        filters.stars.includes(el.id)
                          ? `svgFillAll ${classes.activeStarSvg}`
                          : `svgFillAll ${classes.starSvg}`
                      }
                    />
                  </Box>
                </Box>
              ))}
            </Box>
            {!isFilterMap && (
              <>
                <Typography variant="subtitle1" style={{ marginTop: 40 }}>
                  {listString.IDS_MT_TEXT_SERVICE_ATTACH}
                </Typography>
                <Box className={classes.wrapServiceAttach}>
                  {!isEmpty(filtersData) &&
                    filtersData?.services.map((el, key) => (
                      <Box
                        className={classes.serviceAttach}
                        key={el.id}
                        onClick={() => handleChangeService(el.id, "services")}
                      >
                        {el?.name}
                        <Checkbox
                          checked={filters.services.includes(el.id)}
                          className={classes.root}
                          color="default"
                          checkedIcon={
                            <span
                              className={clsx(
                                classes.icon,
                                classes.checkedIconRadio
                              )}
                            />
                          }
                          icon={<span className={classes.icon} />}
                        />
                      </Box>
                    ))}
                </Box>
              </>
            )}

            <Typography variant="subtitle1" style={{ marginTop: 40 }}>
              {listString.IDS_MT_TEXT_USER_VOTE}
            </Typography>
            <Box className={classes.wrapServiceAttach}>
              {dataUserVote.map((el, key) => (
                <Box
                  className={classes.serviceAttach}
                  key={el?.id}
                  onClick={() => handleChangeService(el.id, "ratingRanges")}
                >
                  {el?.name}
                  <Checkbox
                    checked={filters.ratingRanges.includes(el.id)}
                    className={classes.root}
                    color="default"
                    checkedIcon={
                      <span
                        className={clsx(classes.icon, classes.checkedIconRadio)}
                      />
                    }
                    icon={<span className={classes.icon} />}
                  />
                </Box>
              ))}
            </Box>

            <Typography variant="subtitle1" style={{ marginTop: 40 }}>
              {listString.IDS_MT_TEXT_HOTEL_TYPE}
            </Typography>
            <Box className={classes.wrapServiceAttach}>
              {!isEmpty(filtersData) &&
                filtersData?.types.map((el, key) => (
                  <Box
                    className={classes.serviceAttach}
                    key={el?.id}
                    onClick={() => handleChangeService(el.id, "types")}
                  >
                    {el?.name}
                    <Checkbox
                      checked={filters.types.includes(el.id)}
                      className={classes.root}
                      color="default"
                      checkedIcon={
                        <span
                          className={clsx(
                            classes.icon,
                            classes.checkedIconRadio
                          )}
                        />
                      }
                      icon={<span className={classes.icon} />}
                    />
                  </Box>
                ))}
            </Box>
            {!isFilterMap && (
              <>
                <Typography variant="subtitle1" style={{ marginTop: 40 }}>
                  {listString.IDS_MT_TEXT_AREA}
                </Typography>

                {!isEmpty(filtersData) &&
                  filtersData?.locations.map((el, index) => {
                    if (!isLoadMoreLocation && index >= 5) return null;
                    return (
                      <Box
                        className={classes.serviceAttach}
                        key={el.id}
                        onClick={() => handleSelectSubLocation(el)}
                      >
                        {el?.name} ({el?.count})
                        <Radio
                          checkedIcon={
                            <span
                              className={clsx(
                                classes.iconRadio,
                                classes.checkedIconRadio
                              )}
                            />
                          }
                          icon={<span className={classes.iconRadio} />}
                          checked={filters.subLocations.includes(el.id)}
                          {...props}
                        />
                      </Box>
                    );
                  })}
                {!isEmpty(filtersData) && filtersData?.locations.length > 5 && (
                  <ButtonComponent
                    typeButton="text"
                    color="#00B6F3"
                    width="fit-content"
                    padding="6px 8px 6px 0"
                    handleClick={() =>
                      setIsLoadMoreLocation(!isLoadMoreLocation)
                    }
                  >
                    <Typography
                      variant="body1"
                      component="span"
                      style={{
                        alignItems: "center",
                        display: "flex",
                      }}
                    >
                      {listString.IDS_MT_TEXT_LOAD_MORE}
                      <IconArrowDownToggle
                        className={`svgFillAll ${
                          isLoadMoreLocation
                            ? classes.IconArrowUpToggle
                            : classes.IconArrowDownToggle
                        }`}
                      />
                    </Typography>
                  </ButtonComponent>
                )}
              </>
            )}
            {!isFilterMap && (
              <>
                <Typography variant="subtitle1" style={{ marginTop: 40 }}>
                  {listString.IDS_MT_TEXT_FILTER_CONVENIENT}
                </Typography>

                <Box className={classes.wrapServiceAttach}>
                  {!isEmpty(filtersData) &&
                    filtersData?.amenities.map((el, index) => {
                      if (!isLoadMoreAmenities && index >= 5) return null;
                      return (
                        <Box
                          className={classes.serviceAttach}
                          key={el?.id}
                          onClick={() =>
                            handleChangeService(el.id, "amenities")
                          }
                        >
                          {el?.name}
                          <Checkbox
                            checked={filters.amenities.includes(el.id)}
                            className={classes.root}
                            color="default"
                            checkedIcon={
                              <span
                                className={clsx(
                                  classes.icon,
                                  classes.checkedIconRadio
                                )}
                              />
                            }
                            icon={<span className={classes.icon} />}
                          />
                        </Box>
                      );
                    })}
                </Box>
                {!isEmpty(filtersData) && filtersData?.amenities.length > 5 && (
                  <ButtonComponent
                    typeButton="text"
                    color="#00B6F3"
                    width="fit-content"
                    padding="6px 8px 6px 0"
                    handleClick={() =>
                      setIsLoadMoreAmenities(!isLoadMoreAmenities)
                    }
                  >
                    <Typography
                      variant="body1"
                      component="span"
                      style={{
                        alignItems: "center",
                        display: "flex",
                      }}
                    >
                      {listString.IDS_MT_TEXT_LOAD_COMPACT}
                      <IconArrowDownToggle
                        className={`svgFillAll ${
                          isLoadMoreAmenities
                            ? classes.IconArrowUpToggle
                            : classes.IconArrowDownToggle
                        }`}
                      />
                    </Typography>
                  </ButtonComponent>
                )}
              </>
            )}
          </Box>
          <Box className={classes.wrapBtnFilter}>
            <ButtonComponent
              backgroundColor={theme.palette.secondary.main}
              height={48}
              fontSize={16}
              fontWeight={600}
              borderRadius={8}
              handleClick={handleFilterOk}
            >
              {listString.IDS_MT_TEXT_FILTER}
            </ButtonComponent>
          </Box>
        </Box>
      </Drawer>
    </>
  );
};
FilterHotelListing.propTypes = {
  open: PropTypes.bool.isRequired,
  toggleDrawer: PropTypes.func,
};
export default FilterHotelListing;
