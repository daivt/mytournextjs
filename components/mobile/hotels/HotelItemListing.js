import { memo } from "react";
import moment from "moment";
import cookie from "js-cookie";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import { Box, Grid, Typography, IconButton } from "@material-ui/core";

import {
  IconHeart,
  IconSightSeeing,
  IconBreakFast,
  IconStar,
  IconMytourType,
  IconDot,
} from "@public/icons";
import {
  TOKEN,
  LAST_HOTEL_FAVORITES,
  routeStatic,
  listString,
  prefixUrlIcon,
  listIcons,
} from "utils/constants";
import { addFavorite, removeFavorite } from "@api/user";
import { isEmpty, areEqualHotelItemListing } from "@utils/helpers";

import Link from "@src/link/Link";
import Image from "@src/image/Image";
import snackbarSetting from "@src/alert/Alert";
import ButtonComponent from "@src/button/Button";

const useStyles = makeStyles((theme) => ({
  linkHotelDetail: {
    textDecoration: "none !important",
  },
  wrapImgHotel: {
    position: "relative",
    height: "100%",
  },
  imgHotel: {
    height: "100%",
    width: "100%",
    borderRadius: 8,
  },
  wrapHotelLoved: {
    position: "absolute",
    zIndex: 2,
    top: 0,
    right: 0,
  },
  wrapInfoHotel: {
    marginLeft: 8,
    color: theme.palette.black.black3,
  },
  wrapMissAllotment: {
    fontSize: 14,
    lineHeight: "16px",
    color: theme.palette.red.redLight5,
    background: theme.palette.red.redLight6,
    borderRadius: 8,
    padding: 10,
    marginTop: 10,
  },
  point: {
    borderRadius: 4,
    fontSize: 12,
    fontWeight: 600,
    color: theme.palette.white.main,
    background: theme.palette.blue.blueLight8,
    padding: "1px 4px;",
  },
  ratingText: {
    margin: "0 4px",
    fontSize: 12,
    lineHeight: "14px",
    fontWeight: 400,
  },
  countComment: {
    color: theme.palette.gray.grayDark7,
    fontSize: 12,
  },
  wrapLocation: {
    fontSize: 12,
    display: "flex",
    alignItems: "center",
    marginTop: 4,
  },
  wrapBreakFast: {
    display: "flex",
    fontSize: 12,
    lineHeight: "14px",
    marginTop: 6,
    alignItems: "center",
  },
  wrapRoomAndPrice: {
    fontSize: 12,
    color: theme.palette.black.black3,
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    marginTop: 16,
  },
  subPrice: {
    textDecorationLine: "line-through",
    color: theme.palette.black.black4,
    margin: "2px 0",
    fontSize: 12,
    lineHeight: "14px",
  },
  mainPrice: {
    fontWeight: 600,
    fontSize: 16,
    lineHeight: "19px",
  },
  discountPercent: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 42,
    minHeight: 18,
    color: theme.palette.white.main,
    background: theme.palette.pink.main,
    borderRadius: "3px 3px 0px 3px",
    position: "relative",
    fontSize: 12,
    fontWeight: 600,
    lineHeight: "14px",
  },
  discountText: {
    padding: "2px 4px",
  },
  triangleBottomright: {
    width: 0,
    height: 0,
    borderStyle: "solid",
    borderColor: `transparent ${theme.palette.pink.main} transparent transparent`,
    borderWidth: "0px 5px 5px 0",
    position: "absolute",
    right: 0,
    bottom: -4,
  },
  favorite: {
    fill: "#FF1284",
    stroke: theme.palette.white.main,
  },
  notFavorite: {
    fill: "rgba(0, 0, 0, 0.6)",
    stroke: theme.palette.white.main,
  },
  hotelName: {
    fontWeight: 600,
    color: theme.palette.black.black3,
    display: "-webkit-box",
    WebkitLineClamp: 3,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
    fontSize: 16,
    lineHeight: "19px",
  },
  wrapTypeHotel: {
    background: "#FF1284",
    color: theme.palette.white.main,
    display: "flex",
    alignItems: "center",
    fontSize: 11,
    fontWeight: 400,
    lineHeight: "13px",
    borderRadius: 4,
    width: "fit-content",
    borderBottomRightRadius: 0,
  },
  clipPath: {
    width: 15,
    height: 20,
    clipPath: "polygon(105% 0, 48% 0, 100% 100%, 102% 100%)",
    background: "#ffdced",
  },
  villa: {
    height: 20,
    fontSize: 12,
    lineHeight: "14px",
    fontWeight: 600,
    color: "#FF1284",
    padding: "3px 6px",
    marginLeft: 0,
    background: "rgba(255, 18, 132, 0.15)",
    display: "flex",
    alignItems: "center",
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    whiteSpace: "nowrap",
  },
  iconNoBreakFast: {
    stroke: theme.palette.gray.grayDark8,
    marginRight: 6,
  },
  rankingHotel: {
    stroke: theme.palette.yellow.yellowLight3,
    fill: theme.palette.yellow.yellowLight3,
    width: 12,
    height: 12,
    marginRight: 3,
  },
  hotelLocation: {
    fontSize: 12,
    lineHeight: "14px",
    display: "-webkit-box",
    WebkitLineClamp: 2,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
  },
  wrapPromo: {
    border: `1px dashed ${theme.palette.gray.grayLight23} `,
    display: "flex",
    justifyContent: "space-between",
    borderRadius: 4,
    marginTop: 14,
  },
  discountPromo: {
    fontSize: 12,
    lineHeight: "14px",
    fontWeight: 600,
    borderRadius: "0px 100px 100px 0px",
    background: theme.palette.pink.main,
    color: theme.palette.white.main,
    margin: "11px 0 11px 0",
    display: "flex",
    alignItems: "center",
    padding: "5px 4px",
  },
  wrapCode: {
    fontSize: 12,
    fontWeight: 600,
    lineHeight: "14px",
    color: theme.palette.white.main,
    background: theme.palette.blue.blueLight8,
    padding: "1px 3px",
    marginLeft: 4,
    borderRadius: 2,
  },
  wrapPriceCode: { margin: "3px 0 4px 0 ", width: "100%", paddingRight: 8 },
  iconButtonFavorite: {
    padding: 8,
  },
  tagItem: {
    borderRadius: 4,
    padding: "2px 4px",
    color: theme.palette.white.main,
    fontSize: 11,
    fontWeight: 600,
    lineHeight: "13px",
    marginRight: 6,
  },
  iconMPlus: {
    width: 63,
    height: 20,
  },
  iconTripadvisor: {
    width: 18,
    height: 18,
  },
}));

const HotelItemListing = ({
  item = {},
  styleHotelItem = "",
  paramsFilterInit = {},
}) => {
  const router = useRouter();
  const classes = useStyles();
  const [isFavorite, setIsFavorite] = useState(item.isFavoriteHotel);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  let paramsUrl = {
    alias: `${item.id}-${item?.hotelName.stringSlug()}.html`,
  };

  const checkIsEmptyParamsQuery = () => {
    return !(
      router.query.checkIn ||
      router.query.checkOut ||
      router.query.adults ||
      router.query.rooms ||
      router.query.children
    );
  };

  if (!checkIsEmptyParamsQuery()) {
    paramsUrl = {
      ...paramsUrl,
      checkIn: paramsFilterInit.checkIn,
      checkOut: paramsFilterInit.checkOut,
      adults: parseInt(paramsFilterInit.adults),
      rooms: parseInt(paramsFilterInit.rooms),
      children: parseInt(paramsFilterInit.children),
    };
    if (!isEmpty(paramsFilterInit.childrenAges)) {
      paramsUrl = {
        ...paramsUrl,
        childrenAges: paramsFilterInit.childrenAges.toString(),
      };
    }
  }
  paramsUrl = {
    ...paramsUrl,
    priceKey: item.priceKey || "",
  };

  useEffect(() => {
    checkFavoriteHotel();
  }, []);

  useEffect(() => {
    setIsFavorite(item.isFavoriteHotel);
  }, [item]);

  const checkFavoriteHotel = () => {
    const listHotelFavorites =
      JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
    if (isEmpty(cookie.get(TOKEN)) && !isEmpty(listHotelFavorites)) {
      setIsFavorite(listHotelFavorites.includes(item.id));
    }
  };
  const handleFavorite = (event) => {
    event.preventDefault();
    if (!isEmpty(cookie.get(TOKEN))) {
      changeFavorite();
    } else {
      let listHotelFavoritesTemp =
        JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
      if (isFavorite) {
        listHotelFavoritesTemp = listHotelFavoritesTemp.filter(
          (el) => el !== item.id
        );
      } else {
        listHotelFavoritesTemp.unshift(item.id);
      }
      localStorage.setItem(
        LAST_HOTEL_FAVORITES,
        JSON.stringify(listHotelFavoritesTemp)
      );
      setIsFavorite(!isFavorite);
    }
  };
  const changeFavorite = async () => {
    try {
      if (isFavorite) {
        const { data } = await removeFavorite({ hotelId: item.id });
        if (data.code === 200) {
          setIsFavorite(!isFavorite);
        } else {
          data?.message &&
            enqueueSnackbar(
              data?.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
        }
      } else {
        const { data } = await addFavorite({ hotelId: item.id });
        if (data.code === 200) {
          setIsFavorite(!isFavorite);
        } else {
          data?.message &&
            enqueueSnackbar(
              data?.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
        }
      }
    } catch (error) {}
  };
  const arrStar = [1, 2, 3, 4, 5];
  const handleLogin = (event) => {
    event.preventDefault();
    router.push({
      pathname: routeStatic.LOGIN.href,
    });
  };

  const promotion = !isEmpty(item.promotions) ? item.promotions[0] : {};
  const basePromotion = !isEmpty(promotion.basePromotion)
    ? promotion.basePromotion
    : {};
  const tagItem = !isEmpty(item.tags) ? item.tags[0] : "";

  return (
    <Link
      href={{
        pathname: routeStatic.HOTEL_DETAIL.href,
        query: paramsUrl,
      }}
      className={classes.linkHotelDetail}
    >
      <Grid container className={styleHotelItem}>
        <Grid item xl={4} lg={4} sm={4} md={4} xs={4}>
          <Box className={classes.wrapImgHotel}>
            <Image
              srcImage={item.srcImage}
              className={classes.imgHotel}
              borderRadiusProp="8px"
            />
            <Box className={classes.wrapHotelLoved}>
              <IconButton
                onClick={handleFavorite}
                className={classes.iconButtonFavorite}
              >
                <IconHeart
                  className={`svgFillAll ${
                    isFavorite ? classes.favorite : classes.notFavorite
                  }`}
                />
              </IconButton>
            </Box>
          </Box>
        </Grid>
        <Grid item xl={8} lg={8} sm={8} md={8} xs={8}>
          <Box className={classes.wrapInfoHotel}>
            <Box pb={6 / 8} display="flex" alignItems="center">
              {!isEmpty(tagItem) && (
                <Box
                  className={classes.tagItem}
                  bgcolor="yellow.yellowLight3"
                  component="span"
                >
                  {tagItem.name}
                </Box>
              )}
              {!isEmpty(item?.lastBookedTime) &&
                moment().diff(moment(item?.lastBookedTime)) <= 7200000 && (
                  <Box
                    className={classes.tagItem}
                    bgcolor="red.redLight5"
                    component="span"
                  >{`Đang bán chạy`}</Box>
                )}
            </Box>

            <Box className={classes.hotelName}>{item.hotelName}</Box>
            {!isEmpty(item.category) && (
              <Box display="flex" alignItems="center" mt={1}>
                <Box display="flex">
                  {arrStar.map((el, index) => {
                    if (el > item.ratingStar) return null;
                    return (
                      <IconStar
                        key={index}
                        className={`svgFillAll ${classes.rankingHotel}`}
                      />
                    );
                  })}
                </Box>
                <Box
                  display="flex"
                  alignItems="center"
                  style={{ marginLeft: 12 }}
                >
                  {item.category.code === "villa" && (
                    <>
                      <Box className={classes.wrapTypeHotel}>
                        <Box
                          display="flex"
                          alignItems="flex-end"
                          style={{ marginLeft: 6 }}
                        >
                          <Image
                            srcImage={`${prefixUrlIcon}${listIcons.IconMPlus}`}
                            className={classes.iconMPlus}
                          />
                          <Box className={classes.clipPath}></Box>
                        </Box>
                      </Box>
                      <Box className={classes.villa}> Villa, Căn hộ</Box>
                    </>
                  )}
                </Box>
              </Box>
            )}
            <Box
              display="flex"
              alignItems="center"
              style={{ marginTop: "8px" }}
            >
              <Box component="span" className={classes.point}>
                {item.ratingPoint}
              </Box>
              <Box component="span" className={classes.ratingText}>
                {item.ratingText}
              </Box>
              {item.countComment > 0 && (
                <Box component="span" pr={1} className={classes.countComment}>
                  ({item.countComment})
                </Box>
              )}

              <Image
                srcImage={`${prefixUrlIcon}${listIcons.IconTripadvisor}`}
                className={classes.iconTripadvisor}
              />
              <Box component="span" pl={4 / 8} fontSize={12}>
                {2 * item.taRating === 10 ? 10 : (2 * item.taRating).toFixed(1)}
              </Box>
            </Box>
            <Box className={classes.wrapLocation}>
              <Box component="span" width={16}>
                <IconSightSeeing />
              </Box>
              <Box
                component="span"
                marginLeft={6 / 8}
                className={classes.hotelLocation}
              >
                {item.location}
              </Box>
            </Box>
            {item.freeBreakfast ? (
              <Box
                color="green.greenLight7"
                display="flex"
                alignItems="center"
                mt={6 / 8}
              >
                <IconBreakFast />
                <Box component="span" pl={6 / 8}>
                  {listString.IDS_MT_TEXT_ROOM_FREE_BREAK_FAST}
                </Box>
              </Box>
            ) : (
              <Box
                color="black.black3"
                display="flex"
                alignItems="center"
                mt={6 / 8}
              >
                <IconBreakFast
                  className={`svgFillAll ${classes.iconNoBreakFast}`}
                />
                {listString.IDS_MT_TEXT_ROOM_NO_FREE_BREAK_FAST}
              </Box>
            )}
            {!isEmpty(item?.lastBookedTime) &&
              moment().diff(moment(item?.lastBookedTime)) <= 86400000 && (
                <Box pt={5 / 8}>{`Vừa được đặt ${moment(
                  item?.lastBookedTime
                ).fromNow()}`}</Box>
              )}
            {!item.outOfRoom && item?.allotment > 0 && item?.allotment <= 5 && (
              <Box className={classes.wrapBreakFast}>
                <Box className={classes.breakFast}>
                  <Typography
                    variant="body2"
                    component="span"
                    style={{ color: "#E53E3E" }}
                  >
                    Chỉ còn {item?.allotment} phòng trống
                  </Typography>
                </Box>
              </Box>
            )}

            {item.outOfRoom ? (
              <Box className={classes.wrapMissAllotment}>
                {listString.IDS_MT_TEXT_YOU_MISS_ALLOTMENT}
              </Box>
            ) : item.hiddenPrice ? (
              <Box
                display="flex"
                pt={2}
                flexDirection="column"
                alignItems="flex-end"
              >
                {!isEmpty(item?.roomName) && (
                  <Box pb={6 / 8} textAlign="right">
                    {item?.roomName}
                  </Box>
                )}
                <Box
                  component="span"
                  fontSize={14}
                  lineHeight="17px"
                  pb={6 / 8}
                >
                  {listString.IDS_MT_TEXT_LOGIN_USE_PROMO}
                  <Box
                    component="span"
                    pl={2 / 8}
                    fontWeight={600}
                    color="secondary.main"
                  >{`Giảm ${item.memberDeal}`}</Box>
                </Box>
                <Box
                  component="span"
                  pb={6 / 8}
                  color="gray.grayDark8"
                  fontStyle="italic"
                >
                  {listString.IDS_MT_TEXT_DISCOUNT_ON_MEMBER}
                </Box>
                <ButtonComponent
                  backgroundColor="#FF1284"
                  borderRadius={8}
                  width="fit-content"
                  height={36}
                  padding="8px 16px"
                  fontSize={16}
                  fontWeight={600}
                  handleClick={handleLogin}
                >
                  {listString.IDS_MT_TEXT_LOGIN}
                </ButtonComponent>
              </Box>
            ) : (
              <>
                <Box className={classes.wrapRoomAndPrice}>
                  {!isEmpty(item?.roomName) && (
                    <Box pb={6 / 8} textAlign="right">
                      {item?.roomName}
                    </Box>
                  )}
                  {!isEmpty(item.discountPercent) && (
                    <>
                      <Box className={classes.discountPercent}>
                        <Box className={classes.discountText}>
                          {item?.discountPercent}%
                        </Box>
                        <Box className={classes.triangleBottomright}></Box>
                      </Box>
                      <Box component="span" className={classes.subPrice}>
                        {item.subPrice.formatMoney()} /đêm
                      </Box>
                    </>
                  )}
                  {!isEmpty(item.mainPrice) && item.mainPrice >= 0 && (
                    <Box component="span" className={classes.mainPrice}>
                      {`${item.mainPrice.formatMoney()}đ`}
                      <Box
                        component="span"
                        color="black.black4"
                        fontWeight="normal"
                      >
                        {" "}
                        /đêm
                      </Box>
                    </Box>
                  )}
                </Box>
                {item.freeCancellation && (
                  <Box
                    fontSize={12}
                    lineHeight="14px"
                    color="green.greenLight7"
                    textAlign="right"
                    pt={4 / 8}
                  >
                    {listString.IDS_MT_TEXT_PRICE_OF_FREE_CANCEL}
                  </Box>
                )}
                {!isEmpty(basePromotion) && (
                  <>
                    <Box className={classes.wrapPromo}>
                      <Box className={classes.discountPromo}>
                        {`-${
                          basePromotion.valueType === "percent"
                            ? `${basePromotion.percent}%`
                            : `${basePromotion.value.formatMoney()}đ`
                        }`}
                      </Box>
                      <Box
                        display="flex"
                        alignItems="flex-end"
                        flexDirection="column"
                        justifyContent="center"
                        pr={1}
                      >
                        <Box display="flex" alignItems="center" pb={2 / 8}>
                          {listString.IDS_MT_TEXT_TYPE_PROMO_CODE}{" "}
                          <Box className={classes.wrapCode}>
                            {promotion.code}
                          </Box>
                        </Box>
                        <Box color="secondary.main" lineHeight="19px">
                          <Box
                            component="span"
                            fontSize={16}
                            fontWeight={600}
                          >{`${basePromotion.price.formatMoney()}đ`}</Box>
                          <Box component="span" pl={2 / 8}>{`/đêm`}</Box>
                        </Box>
                      </Box>
                    </Box>
                  </>
                )}
              </>
            )}
          </Box>
        </Grid>
      </Grid>
    </Link>
  );
};
export default memo(HotelItemListing, areEqualHotelItemListing);
