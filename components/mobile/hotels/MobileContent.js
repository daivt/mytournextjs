import moment from "moment";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import InfiniteScroll from "react-infinite-scroller";
import { Box, Grid, Avatar, Typography } from "@material-ui/core";

import { useDispatchSystem } from "@contextProvider/ContextProvider";
import { SET_TOP_LOCATION } from "@contextProvider/hotel/ActionTypes";

import utilStyles from "@styles/utilStyles";
import {
  formatDate,
  HOTEL_SERVICE,
  LAT_LONG_USER,
  listString,
  routeStatic,
  LAST_SEARCH_HISTORY,
  DELAY_TIMEOUT_POLLING,
  RADIUS_GET_HOTEL_LAT_LONG,
} from "@utils/constants";
import { getHotelsAvailability } from "@api/hotels";
import { isEmpty, adapterHotelHotNear } from "@utils/helpers";
import Image from "@src/image/Image";

import Link from "@src/link/Link";
import Layout from "@components/layout/mobile/Layout";
import HotlineBox from "@components/common/card/HotlineBox";
import BannerHotel from "@components/mobile/hotels/BannerHotel";
import HotelCard from "@components/common/card/hotel/HotelCard";
import LoadingMore from "@components/common/loading/LoadingMore";
import WhyBookTicket from "@components/common/card/WhyBookTicket";
import HotelSearchBox from "@components/mobile/hotels/HotelSearchBox";
import AllowGPSModal from "@components/common/modal/hotel/AllowGPSModal";
import ListHotelLastView from "@components/mobile/homes/ListHotelLastView";

const useStyles = makeStyles((theme) => ({
  divider: {
    height: 8,
    width: "100%",
    backgroundColor: theme.palette.gray.grayLight22,
  },
  boxSearchContent: {
    position: "absolute",
    width: "100%",
    marginTop: -20,
    backgroundColor: theme.palette.white.main,
    borderRadius: "16px 16px 0px 0px",
    paddingBottom: 34,
  },
  wrapperLocation: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  iconAvatar: {
    width: 64,
    height: 64,
  },
  nameLocation: {
    textAlign: "center",
    width: "100%",
    display: "-webkit-box",
    WebkitLineClamp: 2,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
    fontSize: 14,
    lineHeight: "17px",
  },
  iconOpenGps: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: theme.palette.gray.grayLight26,
    width: 64,
    height: 64,
    borderRadius: "50%",
  },
  wrapperItem: {
    paddingTop: 16,
  },
  itemHotelCard: {
    paddingBottom: 16,
  },
  linkAvatarLocation: {
    color: theme.palette.black.black3,
    textDecoration: "none !important",
  },
  gps: {
    width: 56,
    height: 56,
  },
}));

const pageSize = 20;
const Hotels = ({ topLocation = [] }) => {
  const router = useRouter();
  const classes = useStyles();
  const classesUtils = utilStyles();
  const [page, setPage] = useState(0);
  const [visibleDrawerFilter, setVisibleDrawerFilter] = useState(false);
  const dispatch = useDispatchSystem();
  const [paramsSearch, setParamsSearch] = useState({
    rooms: 1,
    adults: 2,
    children: 0,
    childrenAges: [],
  });
  const [checkIn, setCheckIn] = useState(moment());
  const [checkOut, setCheckOut] = useState(moment().add(1, "days"));
  const [itemLocationSelect, setItemLocationSelect] = useState({});
  const [latLong, setLatLong] = useState({});
  const [listHotelNear, setListHotelNear] = useState([]);
  const [totalHotel, setTotalHotel] = useState(0);
  const [loading, setLoading] = useState(true);
  let breakPolling = true;
  useEffect(() => {
    dispatch({
      type: SET_TOP_LOCATION,
      payload: topLocation,
    });

    updateRecentSearchFromStorage();
    const positionTemp = JSON.parse(localStorage.getItem(LAT_LONG_USER)) || {};
    if (!isEmpty(positionTemp)) {
      handleAllowGps(false);
    }
    return () => {
      breakPolling = false;
    };
  }, []);

  const updateRecentSearchFromStorage = () => {
    const lastSearchHistory =
      JSON.parse(localStorage.getItem(LAST_SEARCH_HISTORY)) || {};
    setItemLocationSelect(lastSearchHistory?.itemLocationSelect || {});
    setParamsSearch({
      rooms: lastSearchHistory?.rooms || 1,
      adults: lastSearchHistory?.adults || 2,
      children: lastSearchHistory?.children || 0,
      childrenAges: lastSearchHistory?.childrenAges || [],
    });
    if (!isEmpty(lastSearchHistory)) {
      const todayText = moment().format(formatDate.firstYear);
      const checkIn = moment(
        lastSearchHistory.checkIn,
        formatDate.firstDay
      ).format(formatDate.firstYear);
      const checkOut = moment(
        lastSearchHistory.checkOut,
        formatDate.firstDay
      ).format(formatDate.firstYear);
      if (moment(checkIn).isAfter(todayText)) {
        setCheckIn(moment(lastSearchHistory.checkIn, formatDate.firstDay));
      }
      if (moment(checkOut).isAfter(todayText)) {
        setCheckOut(moment(lastSearchHistory.checkOut, formatDate.firstDay));
      }
    }
  };

  const handleOkSelectGuest = (values) => {
    setParamsSearch({
      ...paramsSearch,
      rooms: values.roomSearch,
      adults: values.adultSearch,
      children: values.childSearch,
      childrenAges: values.childrenAges,
    });
  };
  const setItemLocationSelected = (item) => {
    setItemLocationSelect(item);
  };

  const loadFunc = async (position = latLong) => {
    let polling = true;
    const pageTemp = page + 1;
    try {
      const params = {
        ...paramsSelect,
        latitude: position.latitude,
        longitude: position.longitude,
        radius: RADIUS_GET_HOTEL_LAT_LONG,
        searchType: "latlon",
        page: pageTemp,
        size: pageSize,
      };
      setLoading(false);
      while (polling && breakPolling) {
        const { data } = await getHotelsAvailability(params);
        if (data.code === 200) {
          if (data.data.completed) {
            setTotalHotel(data.data.total);
            setListHotelNear([...listHotelNear, ...data?.data?.items]);
            setPage(pageTemp);
            if ((pageTemp + 1) * pageSize < data?.data.total) {
              setLoading(true);
            }
            polling = false;
            break;
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      polling = false;
    }
  };
  const toggleDrawer = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerFilter(open);
  };

  const handleAllowGps = (isRoute = true) => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const positionTemp = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          };
          // save storage
          localStorage.setItem(LAT_LONG_USER, JSON.stringify(positionTemp));
          if (isRoute) {
            const queryInit = {
              slug: [`ksgb`, `khach-san-gan-ban.html`],
              latitude: positionTemp.latitude,
              longitude: positionTemp.longitude,
            };
            router.push({
              pathname: routeStatic.LISTING_HOTEL.href,
              query: {
                ...queryInit,
              },
            });
          } else {
            setLatLong(positionTemp);
            loadFunc(positionTemp);
          }
        },
        () => {}
      );
    }
    setVisibleDrawerFilter(false);
  };

  let paramsSelect = {
    checkIn: checkIn.format(formatDate.firstDay),
    checkOut: checkOut.format(formatDate.firstDay),
    adults: paramsSearch.adults,
    rooms: paramsSearch.rooms,
    children: paramsSearch.children,
    childrenAges: paramsSearch.childrenAges,
  };

  return (
    <Layout
      isHeader={false}
      title="Đặt phòng khách sạn hạng sang giá rẻ - giá tốt nhất trên Mytour.vn | Đặt sát ngày giá càng tốt."
    >
      <Box>
        <BannerHotel />
        <Box className={classes.boxSearchContent}>
          <HotelSearchBox
            paramsSearch={paramsSearch}
            checkIn={checkIn}
            checkOut={checkOut}
            itemLocationSelect={itemLocationSelect}
            handleOkSelectGuest={handleOkSelectGuest}
            setItemLocationSelected={setItemLocationSelected}
            setCheckIn={setCheckIn}
            setCheckOut={setCheckOut}
          />
          <Box className={classes.divider}></Box>
          <Box pl={1} className={classesUtils.scrollViewHorizontal} pt={3}>
            <Box px={1} className={classes.wrapperLocation}>
              <Box
                className={classes.iconOpenGps}
                mb={1}
                onClick={toggleDrawer(true)}
              >
                <Image
                  srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_open_gps.svg"
                  className={classes.gps}
                />
              </Box>
              <Typography variant="caption" component="span">
                Gần tôi
              </Typography>
            </Box>
            {topLocation.map((el, index) => (
              <Link
                href={{
                  pathname: routeStatic.LISTING_HOTEL.href,
                  query: {
                    slug: [
                      `${el.aliasCode}`,
                      `khach-san-tai-${el?.name.stringSlug()}.html`,
                    ],
                  },
                }}
                className={classes.linkAvatarLocation}
                key={el.provinceId}
              >
                <Box px={1} className={classes.wrapperLocation}>
                  <Avatar
                    title={`Khạch sạn tại ${el.name}`}
                    alt={`Khạch sạn tại ${el.name}`}
                    src={el.thumb}
                    className={classes.iconAvatar}
                  />
                  <Box mt={1} className={classes.nameLocation}>
                    {el?.name}
                  </Box>
                </Box>
              </Link>
            ))}
          </Box>
          <ListHotelLastView />
          <Box px={2}>
            <WhyBookTicket source={HOTEL_SERVICE} />
            <HotlineBox />
            {!isEmpty(listHotelNear) && (
              <>
                <Typography variant="h5">
                  {listString.IDS_MT_TEXT_HOTEL_NEAR_YOUR}
                </Typography>
                <Box
                  component="span"
                  color="black.black4"
                  lineHeight="17px"
                  pt={4 / 8}
                >
                  {listString.IDS_TEXT_PEOPLE_CARE_THE_MOST}
                </Box>
                <InfiniteScroll
                  pageStart={page}
                  loadMore={() => loadFunc()}
                  hasMore={loading}
                >
                  <Grid container spacing={2} className={classes.wrapperItem}>
                    {adapterHotelHotNear(listHotelNear).map((el, index) => (
                      <Grid
                        item
                        lg={6}
                        md={6}
                        sm={6}
                        xs={6}
                        key={index}
                        className={classes.itemHotelCard}
                      >
                        <HotelCard
                          isShowDistance
                          isCountBooking={false}
                          isShowPriceInImage={false}
                          item={el}
                        />
                      </Grid>
                    ))}
                  </Grid>
                  {!loading && page * pageSize < totalHotel && <LoadingMore />}
                </InfiniteScroll>
              </>
            )}
          </Box>
        </Box>
      </Box>
      <AllowGPSModal
        open={visibleDrawerFilter}
        toggleDrawer={toggleDrawer}
        handleAllowGps={handleAllowGps}
      />
    </Layout>
  );
};

export default Hotels;
