import { getGeneralInformation } from "@api/flight";
import CalendarDialog from "@components/common/calendar/CalendarDialog";
import PersonSelectionModal from "@components/common/modal/flight/PersonSelectionModal";
import SeatClassModal from "@components/common/modal/flight/SeatClassModal";
import { Box, ButtonBase, Divider, Grid, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import { makeStyles } from "@material-ui/styles";
import {
  IconCalenderDeparture,
  IconCalenderReturn,
  IconSeat2,
  IconUser2,
} from "@public/icons";
import snackbarSetting from "@src/alert/Alert";
import ButtonComponent from "@src/button/Button";
import { CustomSwitch } from "@src/element";
import {
  listEventFlight,
  listString,
  SEARCH_PREV,
  SEAT_TYPE_LIST,
} from "@utils/constants";
import { DATE_FORMAT, DATE_FORMAT_BACK_END, DATE_MONTH } from "@utils/moment";
import moment from "moment";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import React, { useEffect, useState } from "react";
import { isEmpty } from "utils/helpers";
import DestinationsBox from "./DestinationsBox";
import * as gtm from "@utils/gtm";
const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: theme.palette.white.main,
    display: "flex",
    justifyContent: "space-between",
    margin: "8px 0px 0px 0px",
  },
  showDot: {
    whiteSpace: "nowrap",
    maxWidth: "132px",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  buttonSearch: {
    height: 48,
    margin: "8px 0px 8px 0px",
    color: "#fff",
    fontWeight: "bold",
    backgroundColor: theme.palette.secondary.main,
    "& i": {
      paddingRight: 5,
    },
    "&:hover": {
      backgroundColor: theme.palette.secondary.main,
    },
  },
  grayText: {
    color: theme.palette.gray.grayDark8,
  },
  IconUser2: {
    width: 24,
    height: 24,
  },
  IconSeat2: {
    width: 24,
    height: 24,
  },
}));

const listTypeModal = {
  MODAL_PERSON: "MODAL_PERSON",
  MODAL_SEAT: "MODAL_SEAT",
};

const SearchBox = ({ dataQuery, codeFlight, setDestinationDataHotTicket }) => {
  const classes = useStyles();
  const theme = useTheme();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const [paramsSearch, setParamsSearch] = useState({
    adultCount: parseInt(dataQuery.adultCount) || 1,
    childCount: parseInt(dataQuery.childCount) || 0,
    infantCount: parseInt(dataQuery.infantCount) || 0,
    seatSearch: parseInt(dataQuery.seatSearch) || 1,
  });
  const [visibleDrawer, setVisibleDrawer] = useState("");
  const [destinationData, setDestinationData] = useState({
    origin: {
      code: codeFlight?.code?.split("-")[0] || "SGN",
      location: codeFlight?.depart || "Hồ Chí Minh",
    },
    destination: {
      code:
        !isEmpty(dataQuery) && codeFlight ? codeFlight.code?.split("-")[1] : "",
      location: !isEmpty(dataQuery) && codeFlight ? codeFlight.arrival : "",
    },
  });
  const [isRoundTrip, setIsRoundTrip] = useState(
    (dataQuery && dataQuery?.slug && dataQuery?.slug[0] === "chang-bay") ||
      false
  );
  const [startDate, setStartDate] = useState(
    dataQuery.departureDate
      ? moment(dataQuery.departureDate, DATE_FORMAT_BACK_END, true)
      : moment()
  );
  const [endDate, setEndDate] = useState(
    dataQuery.returnDate && isRoundTrip
      ? moment(dataQuery.returnDate, DATE_FORMAT_BACK_END, true)
      : undefined
  );
  const [seatTypeList, setSeatTypeList] = useState(SEAT_TYPE_LIST);
  const [searchHistory, setSearchHistory] = React.useState([]);
  const [isSearchParamsValid, setIsSearchParamsValid] = useState(false);
  const router = useRouter();

  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawer(open);
  };

  const handleOkSelectGuest = (values) => () => {
    setParamsSearch({
      ...paramsSearch,
      adultCount: values.adultCount,
      childCount: values.childCount,
      infantCount: values.infantCount,
    });
    setVisibleDrawer("");
  };

  const handleOkSelectSeat = (values) => () => {
    setParamsSearch({
      ...paramsSearch,
      seatSearch: values,
    });
    setVisibleDrawer("");
  };

  const switchFunction = () => {
    [destinationData.origin, destinationData.destination] = [
      destinationData.destination,
      destinationData.origin,
    ];
    setDestinationData({ ...destinationData });
  };

  const totalGuest =
    paramsSearch.adultCount +
    paramsSearch.childCount +
    paramsSearch.infantCount;

  const actionsGeneralInformation = async () => {
    const { data } = await getGeneralInformation();
    const arr3 = seatTypeList.map((item, i) =>
      Object.assign({}, item, data?.data?.ticketclass[i])
    );
    setSeatTypeList([...arr3]);
  };

  const actionSearch = () => {
    if (!isSearchParamsValid) {
      let message = "Vui lòng nhập cả điểm đi điểm đến để tìm kiếm vé";
      if (
        destinationData?.origin?.code === destinationData?.destination?.code
      ) {
        message = "Vui lòng chọn điểm đi và điểm đến khác nhau";
      }
      enqueueSnackbar(
        message,
        snackbarSetting((key) => closeSnackbar(key), {
          color: "error",
        })
      );
      return;
    }

    //Save history
    let tempSearchHistory = [];
    if (JSON.parse(localStorage.getItem(`SEARCH_RECENT`))) {
      tempSearchHistory = tempSearchHistory.concat(
        JSON.parse(localStorage.getItem(`SEARCH_RECENT`))
      );
    }

    let formatDateFromHistory = startDate
      ? moment(startDate, DATE_FORMAT_BACK_END).format(DATE_MONTH)
      : moment().format(DATE_MONTH);
    let formatDateFromString = startDate
      .locale("vi_VN")
      .format("ddd")
      .replace("T", "Thứ ")
      .replace("CN", "Chủ Nhật");
    let strReturnDate = "";
    let formatDateToHistory = "";
    if (endDate && isRoundTrip) {
      formatDateToHistory = endDate
        ? moment(endDate, DATE_FORMAT_BACK_END).format(DATE_MONTH)
        : moment().format(DATE_MONTH);
      let formatDateToString = endDate
        .locale("vi_VN")
        .format("ddd")
        .replace("T", "Thứ ")
        .replace("CN", "Chủ Nhật");
      strReturnDate = ` - ${formatDateToString}, ${formatDateToHistory}`;
    }

    const strFrom = `${destinationData.origin?.location} (${destinationData.origin?.code})`;
    const strTo = `${destinationData.destination?.location} (${destinationData.destination?.code})`;
    let tempArr = [];
    if (!isEmpty(tempSearchHistory)) {
      tempSearchHistory.map((el, key) => {
        if (el.from === strFrom && el.to === strTo && el.guest === totalGuest) {
        } else {
          tempArr.push(el);
        }
      });
    }

    tempArr.unshift({
      adult: paramsSearch.adultCount,
      child: paramsSearch.childCount,
      baby: paramsSearch.infantCount,
      origin: destinationData.origin?.code,
      destination: destinationData.destination?.code,
      originName: destinationData.origin?.name,
      destinationName: destinationData.destination?.name,
      flightFrom: destinationData.origin?.location,
      flightTo: destinationData.destination?.location,
      from: strFrom,
      to: strTo,
      date: `${formatDateFromString}, ${formatDateFromHistory} ${strReturnDate}`,
      dateDep: startDate
        ? moment(startDate, DATE_FORMAT_BACK_END).format(DATE_FORMAT)
        : moment().format(DATE_FORMAT),
      dateRet: isRoundTrip
        ? endDate
          ? moment(endDate, DATE_FORMAT_BACK_END).format(DATE_FORMAT)
          : moment().format(DATE_FORMAT)
        : "",
      guest: totalGuest,
      seatCode: seatTypeList
        .map((el) => (el?.cid === paramsSearch.seatSearch ? el?.code : ""))
        .filter((element) => element),
      seat: seatTypeList
        .map((el) => (el?.cid === paramsSearch.seatSearch ? el?.i_name : ""))
        .filter((element) => element),
      type: isRoundTrip,
    });

    localStorage.setItem(SEARCH_PREV, JSON.stringify(tempArr.slice(0, 1)));
    tempArr = tempArr.slice(0, 9);
    localStorage.setItem(`SEARCH_RECENT`, JSON.stringify(tempArr));

    if (isRoundTrip) {
      gtm.addEventGtm(listEventFlight.FlightSearchRoundTrip, {
        cityName: destinationData?.destination?.code || "",
      });
    } else {
      gtm.addEventGtm(listEventFlight.FlightSearchOneDirection, {
        cityName: destinationData?.destination?.code || "",
      });
    }
    router.push({
      pathname: "/ve-may-bay/result/outbound",
      query: {
        ...paramsSearch,
        ...{
          seatSearch: seatTypeList
            .map((el) => (el?.cid === paramsSearch.seatSearch ? el?.code : ""))
            .filter((element) => element),
          departureDate: startDate
            ? moment(startDate).format(DATE_FORMAT_BACK_END)
            : moment().format(DATE_FORMAT_BACK_END),
          returnDate:
            endDate && isRoundTrip
              ? moment(endDate).format(DATE_FORMAT_BACK_END)
              : undefined,
          origin_code: destinationData.origin?.code,
          origin_location: destinationData.origin?.location,
          destination_code: destinationData.destination?.code,
          destination_location: destinationData.destination?.location,
          originName: destinationData.origin?.name,
          destinationName: destinationData.destination?.name,
        },
      },
    });
  };

  useEffect(() => {
    actionsGeneralInformation();
  }, []);

  useEffect(() => {
    let tempDestinationData = {};
    if (isEmpty(dataQuery)) {
      if (!isEmpty(searchHistory)) {
        tempDestinationData = {
          origin: {
            code: searchHistory[0]?.origin,
            location: searchHistory[0]?.flightFrom,
          },
          destination: {
            code: searchHistory[0]?.destination,
            location: searchHistory[0]?.flightTo,
          },
        };
      } else {
        tempDestinationData = {
          origin: {
            code: "SGN",
            location: "Hồ Chí Minh",
          },
          destination: {
            code: "",
            location: "",
          },
        };
      }
    } else {
      if (dataQuery?.slug[0] === "den") {
        tempDestinationData = {
          origin: {
            code: "",
            location: "",
          },
          destination: {
            code: codeFlight.code?.split("-")[1] || "",
            location: codeFlight.arrival || "",
          },
        };
      } else {
        tempDestinationData = {
          origin: {
            code: codeFlight?.code?.split("-")[0] || "",
            location: codeFlight?.depart || "",
          },
          destination: {
            code: "",
            location: "",
          },
        };
      }
    }
    setDestinationData(tempDestinationData);
  }, [codeFlight, searchHistory]);

  useEffect(() => {
    const ISSERVER = typeof window === "undefined";

    if (!ISSERVER) {
      const temp = JSON.parse(localStorage.getItem(SEARCH_PREV));
      if (temp) {
        setIsRoundTrip(
          (dataQuery &&
            dataQuery?.slug &&
            dataQuery?.slug[0] === "chang-bay") ||
            (temp.length && temp[0]?.type)
        );
        setStartDate(
          dataQuery.departureDate
            ? moment(dataQuery.departureDate, DATE_FORMAT_BACK_END, true)
            : temp[0].dateDep &&
              moment(temp[0].dateDep, DATE_FORMAT).isSameOrAfter(
                moment(),
                "days"
              )
            ? moment(temp[0].dateDep, DATE_FORMAT, true)
            : moment()
        );
        setEndDate(
          dataQuery.returnDate
            ? moment(dataQuery.returnDate, DATE_FORMAT_BACK_END, true)
            : temp[0].dateRet &&
              moment(temp[0].dateRet, DATE_FORMAT).isSameOrAfter(
                moment(),
                "days"
              )
            ? moment(temp[0].dateRet, DATE_FORMAT, true)
            : temp[0].dateRet
            ? moment()
            : undefined
        );
        setParamsSearch({
          adultCount:
            parseInt(dataQuery.adultCount) ||
            (temp.length && temp[0]?.adult) ||
            1,
          childCount:
            parseInt(dataQuery.childCount) ||
            (temp.length && temp[0]?.child) ||
            0,
          infantCount:
            parseInt(dataQuery.infantCount) ||
            (temp.length && temp[0]?.baby) ||
            0,
          seatSearch:
            parseInt(dataQuery.seatSearch) ||
            (temp.length &&
              seatTypeList.find((seat) => seat?.code === temp[0]?.seatCode[0])
                ?.cid) ||
            1,
        });
      }
      setSearchHistory(temp || []);
    }
  }, [seatTypeList]);

  useEffect(() => {
    const tempSttActionSearch =
      !isEmpty(destinationData?.origin?.code) &&
      !isEmpty(destinationData?.destination?.code) &&
      destinationData?.origin?.code !== destinationData?.destination?.code;
    setIsSearchParamsValid(tempSttActionSearch);
    setDestinationDataHotTicket(destinationData);
  }, [destinationData]);
  if (!paramsSearch) {
    return <LoadingIcon />;
  }
  return (
    <Box display="flex" flexDirection="column">
      <DestinationsBox
        switchFunction={switchFunction}
        destinationData={destinationData}
        setDestinationData={setDestinationData}
      />
      <Box className={classes.container}>
        <Box display="flex" flexDirection="column" width="100%">
          <Typography
            variant="body2"
            style={{
              display: "flex",
              justifyContent: "flex-start",
              width: "100%",
            }}
            className={classes.grayText}
          >
            {listString.IDS_TEXT_DAY_OUT}
          </Typography>
          <Box display="flex" marginTop="8px" alignItems="center">
            <CalendarDialog
              child={
                <Box
                  display="flex"
                  alignItems="center"
                  className={`triggerClick`}
                  mb={1}
                >
                  <IconCalenderDeparture className={classes.IconUser2} />
                  <Typography
                    style={{ marginLeft: 12, display: "flex" }}
                    variant="body1"
                  >
                    {startDate
                      ? startDate
                          .locale("vi_VN")
                          .format("ddd")
                          .replace("T", "Thứ ")
                          .replace("CN", "Chủ Nhật")
                      : "-"}
                    ,&nbsp;
                    {startDate ? startDate.format("DD") : ","}&nbsp;
                    {startDate.format("YYYY") !== moment().format("YYYY") ? (
                      <Typography variant="body1">
                        Tháng {startDate.format("MM")},{" "}
                        {startDate.format("YYYY")}
                      </Typography>
                    ) : (
                      <Typography variant="body1">
                        Tháng {startDate.format("MM")}
                      </Typography>
                    )}
                  </Typography>
                </Box>
              }
              type="flight"
              isSingle={!isRoundTrip}
              initStartDate={startDate}
              initEndDate={endDate}
              updateStartDate={setStartDate}
              updateEndDate={setEndDate}
            />
          </Box>
        </Box>
        <Box display="flex" flexDirection="column" textAlign="right">
          <Typography variant="body2" className={classes.grayText}>
            {listString.IDS_MT_TEXT_ROUND_TRIP}
          </Typography>
          <CustomSwitch
            checked={isRoundTrip}
            onChange={(e) => {
              // setStartDate(moment());
              setEndDate(
                e.target.checked
                  ? moment(startDate, DATE_FORMAT_BACK_END).add(1, "days")
                  : null
              );
              setIsRoundTrip(e.target.checked);
            }}
          />
        </Box>
      </Box>
      {!!isRoundTrip && (
        <Box className={classes.container}>
          <Box display="flex" flexDirection="column">
            <Typography variant="body2" className={classes.grayText}>
              {listString.IDS_TEXT_DAY_IN}
            </Typography>
            <Box
              display="flex"
              alignItems="center"
              triggerClick="selectDate"
              my={1}
            >
              <CalendarDialog
                child={
                  <Box display="flex" alignItems="center">
                    <IconCalenderReturn className={classes.IconUser2} />
                    <Typography
                      style={{ marginLeft: 12, display: "flex" }}
                      variant="body1"
                    >
                      {endDate
                        ? endDate
                            .locale("vi_VN")
                            .format("ddd")
                            .replace("T", "Thứ ")
                            .replace("CN", "Chủ Nhật")
                        : "-"}
                      ,&nbsp;
                      {endDate ? endDate.format("DD") : ","}&nbsp;
                      {endDate.format("YYYY") !== moment().format("YYYY") ? (
                        <Typography variant="body1">
                          Tháng {endDate.format("MM")}, {endDate.format("YYYY")}
                        </Typography>
                      ) : (
                        <Typography variant="body1">
                          Tháng {endDate.format("MM")}
                        </Typography>
                      )}
                    </Typography>
                  </Box>
                }
                type="flight"
                isSingle={!isRoundTrip}
                initStartDate={startDate}
                initEndDate={endDate}
                updateStartDate={setStartDate}
                updateEndDate={setEndDate}
              />
            </Box>
          </Box>
        </Box>
      )}
      <Divider
        style={{
          margin: "0px 0px 8px 0px",
          backgroundColor: theme.palette.gray.grayLight22,
        }}
      />
      <Box className={classes.container}>
        <Grid container>
          <Grid item lg={6} md={6} sm={6} xs={6}>
            <Box display="flex" flexDirection="column">
              <Typography variant="body2" className={classes.grayText}>
                {listString.IDS_TEXT_COUNT_CUSTOMER}
              </Typography>
              <Box
                display="flex"
                marginTop="8px"
                alignItems="center"
                onClick={toggleDrawer(listTypeModal.MODAL_PERSON)}
              >
                <ButtonBase>
                  <IconUser2 className={classes.IconUser2} />
                  <Typography style={{ marginLeft: 12 }} variant="body1">
                    {totalGuest}
                  </Typography>
                  &nbsp;
                  <Typography variant="body1">
                    {listString.IDS_MT_TEXT_SINGLE_CUSTOMER}
                  </Typography>
                </ButtonBase>
              </Box>
            </Box>
          </Grid>
          <Grid item lg={6} md={6} sm={6} xs={6}>
            <Box display="flex" flexDirection="column" textAlign="left">
              <Typography variant="body2" className={classes.grayText}>
                {listString.IDS_MT_TEXT_RATING_SEAT}
              </Typography>
              <Box
                display="flex"
                marginTop="8px"
                alignItems="center"
                onClick={toggleDrawer(listTypeModal.MODAL_SEAT)}
              >
                <ButtonBase>
                  <IconSeat2 className={classes.IconSeat2} />
                  <Typography
                    style={{ marginLeft: 12, whiteSpace: "nowrap" }}
                    variant="body1"
                  >
                    {seatTypeList.map((el) =>
                      el?.cid === paramsSearch.seatSearch ? el?.name : ""
                    )}
                  </Typography>
                </ButtonBase>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Box>
      <Divider
        style={{
          margin: "8px 0px",
          backgroundColor: theme.palette.gray.grayLight22,
        }}
      />
      <ButtonComponent
        type="submit"
        className={classes.buttonSearch}
        height={48}
        fontSize={16}
        fontWeight={600}
        borderRadius={8}
        handleClick={() => actionSearch()}
      >
        <SearchIcon />
        {listString.IDS_MT_TEXT_SEARCH}
      </ButtonComponent>
      <PersonSelectionModal
        paramsSearch={paramsSearch}
        open={visibleDrawer === listTypeModal.MODAL_PERSON}
        toggleDrawer={toggleDrawer}
        handleOk={handleOkSelectGuest}
      />
      <SeatClassModal
        paramsSearch={paramsSearch}
        open={visibleDrawer === listTypeModal.MODAL_SEAT}
        toggleDrawer={toggleDrawer}
        handleOk={handleOkSelectSeat}
      />
    </Box>
  );
};

export default SearchBox;
