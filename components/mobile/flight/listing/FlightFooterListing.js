import { getGeneralInformation } from "@api/flight";
import FlightFilterModal from "@components/common/modal/flight/FlightFilterModal";
import FlightSortModal from "@components/common/modal/flight/FlightSortModal";
import { Box, ButtonBase, IconButton } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconFilter, IconSort } from "@public/icons";
import { listString } from "@utils/constants";
import clsx from "clsx";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  wrapHeaderListing: {
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  containerInfoFilter: {
    width: "100vw",
    height: 44,
    background: theme.palette.white.main,
    color: theme.palette.black.black3,
    display: "flex",
    alignItems: "center",
    position: "relative",
    justifyContent: "center",
  },
  localtion: {
    fontSize: 16,
    fontWeight: 600,
  },
  checkIn: {
    color: theme.palette.black.black4,
    fontSize: 12,
  },
  wrapFilter: {
    display: "flex",
  },
  filter: {
    display: "flex",
    alignItems: "center",
  },
  filterText: {
    fontSize: 14,
    position: "relative",
    margin: "0 20px 0 4px",
  },
  sort: {
    display: "flex",
    alignItems: "center",
    borderLeft: `solid 1px ${theme.palette.gray.grayLight22}`,
    paddingLeft: 20,
  },
  hasSort: {
    color: theme.palette.blue.blueLight8,
  },
  wrapIconFilter: {
    paddingRight: 0,
  },
}));

const listTypeDrawer = {
  SORT_DRAWER: "SORT_DRAWER",
  FILTER_DRAWER: "FILTER_DRAWER",
};
const FlightFooterListing = ({
  data,
  setParamFilter = () => {},
  paramFilter,
  hasListTicket = true,
}) => {
  const router = useRouter();
  const classes = useStyles();
  const dataBox = data;

  const filterPriceLimit = {
    maxPrice: dataBox?.filters?.maxPrice,
    maxFlightTime: dataBox?.filters?.maxDuration
      ? parseInt(dataBox?.filters.maxDuration / 1000 / 60)
      : 120,
  };

  const [visibleDrawerType, setVisibleDrawerType] = useState("");
  const [paramsFilterAndSort, setParamsFilterAndSort] = useState({
    priceFilter: 0,
    airlines: [],
    flyTimeFilter: 0,
    liftTimeFilter: [],
    seatFilter: [],
    sort: 0,
    numStops: [],
  });

  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };

  React.useEffect(() => {
    if (!hasListTicket) {
      setParamsFilterAndSort(paramFilter);
    }
  }, []);

  const handleSetSingleParam = (index, value) => {
    setParamsFilterAndSort({
      ...paramsFilterAndSort,
      [index]: value,
    });
  };

  const handleSetArrayParam = (field, id) => {
    const fieldCheck = paramsFilterAndSort[field];
    if (fieldCheck.includes(id)) {
      setParamsFilterAndSort({
        ...paramsFilterAndSort,
        [field]: fieldCheck.filter((el) => el !== id),
      });
    } else {
      setParamsFilterAndSort({
        ...paramsFilterAndSort,
        [field]: [...fieldCheck, id],
      });
    }
  };

  const handleOkFilter = (values) => () => {
    setParamFilter(paramsFilterAndSort);
    setVisibleDrawerType("");
  };

  const handleOkSorter = (values) => () => {
    setParamsFilterAndSort({
      ...paramsFilterAndSort,
      sort: values,
    });
    setParamFilter({
      ...paramFilter,
      sortBy: values,
    });
    setVisibleDrawerType("");
  };

  const handleResetFilter = (value) => () => {
    setParamsFilterAndSort({
      ...paramsFilterAndSort,
      priceFilter: 0,
      airlines: [],
      flyTimeFilter: 0,
      liftTimeFilter: [],
      seatFilter: [],
      sort: 0,
      numStops: [],
    });
  };

  const dataFilter = {
    branchPartner: dataBox?.airlines,
    ticketClass: dataBox?.ticketClasses,
    filterPriceLimit: filterPriceLimit,
    numStops: dataBox?.filters?.numStops,
  };

  return (
    <>
      <Box className={classes.wrapHeaderListing}>
        <Box className={classes.containerInfoFilter}>
          <Box className={classes.wrapFilter}>
            <ButtonBase
              className={classes.filter}
              onClick={toggleDrawer(listTypeDrawer.FILTER_DRAWER)}
            >
              <IconFilter className={classes.wrapIconFilter} />
              <Typography
                component="span"
                variant="caption"
                className={classes.filterText}
              >
                {listString.IDS_MT_TEXT_FILTER_SINGLE}
              </Typography>
            </ButtonBase>
            <ButtonBase
              className={clsx(
                classes.sort,
                paramsFilterAndSort?.sort > 1 && classes.hasSort
              )}
              onClick={toggleDrawer(listTypeDrawer.SORT_DRAWER)}
            >
              <IconSort className={classes.wrapIconFilter} />
              <Typography component="span" variant="caption" ml={4 / 8}>
                {listString.IDS_MT_TEXT_SORT}
              </Typography>
            </ButtonBase>
          </Box>
        </Box>
      </Box>
      <FlightFilterModal
        dataFilter={dataFilter}
        paramFilter={paramsFilterAndSort}
        open={visibleDrawerType === listTypeDrawer.FILTER_DRAWER}
        toggleDrawer={toggleDrawer}
        handleFilter={handleOkFilter}
        handleSetSingleParam={handleSetSingleParam}
        handleSetArrayParam={handleSetArrayParam}
        handleReset={handleResetFilter}
      />
      <FlightSortModal
        open={visibleDrawerType === listTypeDrawer.SORT_DRAWER}
        toggleDrawer={toggleDrawer}
        handleSorter={handleOkSorter}
      />
    </>
  );
};
FlightFooterListing.propTypes = {};

export default FlightFooterListing;
