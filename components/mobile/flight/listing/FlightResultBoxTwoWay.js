import TicketFlightCard from "@components/common/card/flight/TicketFlightCard";
import FlightInfoBoxModal from "@components/common/modal/flight/FlightInfoBoxModal";
import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { useSnackbar } from "notistack";
import React, { useState } from "react";
import FlightFooterListing from "./FlightFooterListing";
import { listEventFlight } from "@utils/constants";

import * as gtm from "@utils/gtm";

const useStyles = makeStyles((theme) => ({
  listFlightCard: {
    backgroundColor: theme.palette.gray.grayLight23,
    padding: "8px 8px 48px 8px",
    height: "100%",
    minHeight: "100vh",
  },
  divider: {
    height: 8,
    backgroundColor: theme.palette.gray.grayLight23,
  },
  footerListing: {
    position: "fixed",
    backgroundColor: theme.palette.white.main,
    zIndex: 20,
    bottom: 0,
  },
}));

const FlightResultBoxTwoWay = ({
  dataTickets,
  dataFilter,
  data,
  setParamFilter = () => {},
}) => {
  const classes = useStyles();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [visibleDrawerType, setVisibleDrawerType] = useState(false);
  const [ticket, setTicket] = useState();
  const [ticketId, setTicketId] = useState();

  const toggleDrawer = (open = false, index = 0) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
    if (open) {
      const ticket = dataTickets[index] || {};
      gtm.addEventGtm(listEventFlight.FlightViewDetail);
      setTicketId(dataTickets[index].tid);
      setTicket(dataTickets[index]);
    } else {
      setTicket();
    }
  };

  const getTicketAirline = (id) => {
    let airline = {};
    if (dataFilter?.airlines) {
      airline = dataFilter.airlines.find((al) => al.id === id);
    }
    return airline ? airline : {};
  };

  const getAirlineInfo = () => {
    if (data && ticket) {
      return data?.airlines.find((element) => {
        return element.id == ticket?.outbound?.aid;
      });
    } else {
      return {};
    }
  };

  if (dataTickets && dataTickets.length) {
    return (
      <>
        <Box className={classes.listFlightCard}>
          <Typography gutterBottom variant="subtitle1">
            Chọn vé chiều đi
          </Typography>
          {dataTickets.map((el, index) => (
            <>
              <Box key={index} onClick={toggleDrawer(true, index)}>
                <TicketFlightCard
                  item={el.outbound}
                  airline={getTicketAirline(el.outbound.aid)}
                />
              </Box>
              {index < dataTickets.length - 1 ? (
                <Box className={classes.divider} />
              ) : null}
            </>
          ))}
        </Box>
        <Box position="relative">
          <Box className={classes.footerListing}>
            <FlightFooterListing
              data={dataFilter}
              setParamFilter={setParamFilter}
            />
          </Box>
        </Box>
        <FlightInfoBoxModal
          isTwoWay
          open={visibleDrawerType}
          setOpen={setVisibleDrawerType}
          ticket={ticket?.outbound}
          data={data}
          isTransit={!!ticket?.outbound?.transitTickets}
          airlineInfo={getAirlineInfo()}
          ticketId={ticketId}
        />
      </>
    );
  }
};

export default FlightResultBoxTwoWay;
