import { searchHotTickets } from "@api/flight";
import CalendarDialog from "@components/common/calendar/CalendarDialog";
import { Box, IconButton } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconArrowLeft, IconCalenderSearchHotel, IconDot } from "@public/icons";
import utilStyles from "@styles/utilStyles";
import { listString } from "@utils/constants";
import { C_DATE_FORMAT, DATE_FORMAT_BACK_END } from "@utils/moment";
import { uniqBy } from "lodash";
import moment from "moment";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import { useEffect, useRef, useState } from "react";
import { isEmpty } from "utils/helpers";
import FlightDatePrice from "./FlightDatePrice";

const useStyles = makeStyles((theme) => ({
  wrapHeaderListing: {
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  containerInfoSearch: {
    height: 48,
    width: "100vw",
    background: theme.palette.white.main,
    display: "flex",
    alignItems: "center",
  },
  btnBack: {
    position: "absolute",
    left: 0,
  },
  btnCalendar: {
    top: 49,
    position: "absolute",
    right: 0,
    backgroundColor: "#ffffff",
    borderRadius: 0,
    "&:hover": {
      backgroundColor: "#ffffff",
    },
  },
  iconArrowLeft: {
    display: "flex ",
    alignItems: "center",
  },
  iconArrowRight: {
    display: "flex ",
    alignItems: "center",
  },
  infoSearch: {
    display: "flex",
    color: theme.palette.black.black3,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: "0 auto",
  },
  localtion: {
    fontSize: 16,
    fontWeight: 600,
  },
  checkIn: {
    color: theme.palette.black.black4,
    fontSize: 12,
  },
  wrapInfoCheckIn: {
    display: "flex",
    alignItems: "center",
    marginTop: 2,
  },
}));

const FlightHeaderListing = ({ isHideScroll, dataQuery, isInBound }) => {
  const router = useRouter();
  const classes = useStyles();
  const classesUtils = utilStyles();
  const [isSelect, setIsSelect] = useState(0);
  const [hotTickets, setHotTickets] = useState([]);
  const [startDate, setStartDate] = useState(
    dataQuery && dataQuery.departureDate
      ? moment(dataQuery.departureDate, DATE_FORMAT_BACK_END, true)
      : moment()
  );
  const [endDate, setEndDate] = useState(
    dataQuery && !isEmpty(dataQuery.returnDate)
      ? moment(dataQuery.returnDate, DATE_FORMAT_BACK_END, true)
      : undefined
  );
  let tempStartDate = null;

  const slideEl = useRef(null);
  // const theme = useTheme();

  const getHotFly = async () => {
    try {
      let tempHotTickets = [];
      const start = moment(dataQuery.departureDate, DATE_FORMAT_BACK_END, true);
      const end = moment(
        dataQuery.departureDate,
        DATE_FORMAT_BACK_END,
        true
      ).add(4, "days");

      let loop = start;
      while (end.diff(loop)) {
        const hotTicket = {
          airlineId: null,
          departureDate: moment(loop, DATE_FORMAT_BACK_END).format(
            DATE_FORMAT_BACK_END
          ),
          discountAdult: 0,
          farePrice: null,
          totalPrice: null,
        };
        tempHotTickets.push(hotTicket);
        const newDate = loop.add(1, "days");
        loop = newDate;
      }
      const dataDTO = {
        adults:
          dataQuery && dataQuery.adultCount
            ? parseInt(dataQuery.adultCount)
            : 1,
        children:
          dataQuery && dataQuery.childCount
            ? parseInt(dataQuery.childCount)
            : 0,
        fromDate:
          dataQuery && dataQuery.departureDate
            ? moment(dataQuery.departureDate, DATE_FORMAT_BACK_END, true)
                .subtract(3, "days")
                .format(C_DATE_FORMAT)
            : moment().format(C_DATE_FORMAT),
        toDate:
          dataQuery && dataQuery.returnDate
            ? moment(dataQuery.returnDate, DATE_FORMAT_BACK_END, true).format(
                C_DATE_FORMAT
              )
            : dataQuery && dataQuery.departureDate
            ? moment(dataQuery.departureDate, DATE_FORMAT_BACK_END, true)
                .add(3, "days")
                .format(C_DATE_FORMAT)
            : moment().format(C_DATE_FORMAT),
        groupByDate: true,
        infants:
          dataQuery && dataQuery.infantCount
            ? parseInt(dataQuery.infantCount)
            : 0,
        itineraries: [
          {
            arrivalCity:
              dataQuery && dataQuery.destination_location
                ? dataQuery.destination_location
                : "Hà Nội",
            departCity:
              dataQuery && dataQuery.origin_location
                ? dataQuery.origin_location
                : "Hồ Chí Minh",
            fromAirport:
              dataQuery && dataQuery.origin_code
                ? dataQuery.origin_code
                : "SGN",
            toAirport:
              dataQuery && dataQuery.destination_code
                ? dataQuery.destination_code
                : "HAN",
          },
        ],
      };
      const { data } = await searchHotTickets(dataDTO);
      data.data?.hotTickets[0].priceOptions?.find((element, index) => {
        if (element?.departureDate === dataQuery?.departureDate)
          setIsSelect(index);
      });
      let tempListHotTicket = uniqBy(
        [...data.data.hotTickets[0]?.priceOptions, ...tempHotTickets],
        "departureDate"
      ).sort(
        (a, b) =>
          moment(a.departureDate).format("YYYYMMDD") -
          moment(b.departureDate).format("YYYYMMDD")
      );
      setHotTickets(tempListHotTicket);
    } catch (error) {}
  };

  const _handleBackRoute = () => {
    router.back();
  };

  useEffect(() => {
    getHotFly();
  }, [dataQuery]);

  useEffect(() => {
    if (slideEl && slideEl.current) {
      slideEl.current.scrollLeft = 104 * (isSelect - 1) + 8;
    }
  }, [isHideScroll, hotTickets, isSelect]);
  if (!dataQuery) {
    return <div />;
  }
  return (
    <>
      <Box className={classes.wrapHeaderListing}>
        <Box
          className={classes.containerInfoSearch}
          borderBottom="1px solid #EDF2F7"
        >
          <Box className={classes.iconArrowLeft}>
            <IconButton className={classes.btnBack} onClick={_handleBackRoute}>
              <IconArrowLeft />
            </IconButton>
          </Box>
          <Box className={classes.infoSearch}>
            <Box component="span" className={classes.localtion}>
              {dataQuery?.origin_location} - {dataQuery?.destination_location}
            </Box>
            <Box className={classes.wrapInfoCheckIn}>
              {isHideScroll ? (
                <Box component="span" className={classes.checkIn}>
                  {dataQuery?.returnDate
                    ? moment(dataQuery.returnDate, DATE_FORMAT_BACK_END, true)
                        .locale("vi_VN")
                        .format("ddd")
                        .replace("T", "Thứ ")
                        .replace("CN", "Chủ Nhật")
                    : "-"}
                  ,{" "}
                  {dataQuery?.returnDate
                    ? moment(
                        dataQuery.returnDate,
                        DATE_FORMAT_BACK_END,
                        true
                      ).format("DD/MM")
                    : ","}
                  &nbsp;
                </Box>
              ) : (
                <Box component="span" className={classes.checkIn}>
                  {dataQuery?.departureDate
                    ? moment(
                        dataQuery.departureDate,
                        DATE_FORMAT_BACK_END,
                        true
                      )
                        .locale("vi_VN")
                        .format("ddd")
                        .replace("T", "Thứ ")
                        .replace("CN", "Chủ Nhật")
                    : "-"}
                  ,{" "}
                  {dataQuery?.departureDate
                    ? moment(
                        dataQuery.departureDate,
                        DATE_FORMAT_BACK_END,
                        true
                      ).format("DD/MM")
                    : ","}
                  &nbsp;
                </Box>
              )}
              <Box component="span" margin={4 / 8} display="flex">
                <IconDot />
              </Box>
              <Box component="span" className={classes.checkIn}>
                {parseInt(dataQuery.adultCount) +
                  parseInt(dataQuery.childCount) +
                  parseInt(dataQuery.infantCount)}
                &nbsp;
                {listString.IDS_MT_TEXT_SINGLE_CUSTOMER}
              </Box>
            </Box>
          </Box>
        </Box>
        {!isHideScroll && !isEmpty(hotTickets) ? (
          <Box className={classes.containerInfoSearch}>
            <Box
              pl={1}
              ref={slideEl}
              className={classesUtils.scrollViewHorizontal}
            >
              {!isEmpty(hotTickets) &&
                hotTickets.length >= 2 &&
                hotTickets.map((el, index) => (
                  <Box key={index} px={1}>
                    <FlightDatePrice
                      isSelect={isSelect === index}
                      setIsSelect={setIsSelect}
                      index={index}
                      ticket={el}
                      dataQuery={dataQuery}
                      isHideScroll={isHideScroll}
                    />
                  </Box>
                ))}
            </Box>
            <Box width="36px"></Box>
            <Box className={classes.iconArrowRight}>
              <CalendarDialog
                child={
                  <IconButton className={classes.btnCalendar}>
                    <IconCalenderSearchHotel />
                  </IconButton>
                }
                type="flight"
                initStartDate={startDate}
                initEndDate={endDate}
                updateStartDate={(day) => {
                  setStartDate(day);
                  tempStartDate = moment(day).format(DATE_FORMAT_BACK_END);
                  if (isEmpty(dataQuery.returnDate)) {
                    router.push({
                      pathname: router.pathname,
                      query: {
                        ...router.query,
                        departureDate: tempStartDate,
                      },
                    });
                  }
                }}
                updateEndDate={(day) => {
                  setEndDate(day);
                  const tempReturnDate = moment(day).format(
                    DATE_FORMAT_BACK_END
                  );
                  if (!isEmpty(dataQuery.returnDate)) {
                    router.push({
                      pathname: router.pathname,
                      query: {
                        ...router.query,
                        returnDate: tempReturnDate,
                        departureDate: dataQuery.departureDate,
                      },
                    });
                  }
                }}
                isSingle={isEmpty(dataQuery.returnDate)}
              />
            </Box>
          </Box>
        ) : null}
      </Box>
    </>
  );
};
FlightHeaderListing.propTypes = {
  isHideScroll: PropTypes.bool,
};
FlightHeaderListing.defaultProps = {
  isHideScroll: false,
};
export default FlightHeaderListing;
