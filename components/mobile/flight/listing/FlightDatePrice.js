import { Box, ButtonBase, Typography } from "@material-ui/core";
import { DATE_FORMAT_BACK_END } from "@utils/moment";
import moment from "moment";
import { useRouter } from "next/router";
import React from "react";

const FlightDatePrice = ({
  isSelect,
  setIsSelect,
  index,
  ticket,
  dataQuery,
}) => {
  const router = useRouter();
  //   const classes = useStyles();
  // const classesUtils = utilStyles();
  const DAYS_BETWEEN = moment(
    dataQuery?.returnDate,
    DATE_FORMAT_BACK_END,
    true
  ).diff(moment(dataQuery?.departureDate, DATE_FORMAT_BACK_END, true), "days");
  return (
    <ButtonBase
      style={{
        backgroundColor: isSelect ? "#00B6F3" : "#ffffff",
        borderRadius: "8px",
        height: 36,
        width: 80,
        marginRight: 8,
      }}
      onClick={() => {
        router.push({
          pathname: "/ve-may-bay/result/outbound",
          query: {
            ...router?.query,
            departureDate: ticket?.departureDate,
            returnDate: dataQuery?.returnDate
              ? moment(ticket?.departureDate, DATE_FORMAT_BACK_END, true)
                  .add(DAYS_BETWEEN, "days")
                  .format(DATE_FORMAT_BACK_END)
              : null,
          },
        });
        setIsSelect(index);
      }}
    >
      <Box display="flex" flexDirection="column">
        <Typography
          variant="body2"
          style={{
            whiteSpace: "nowrap",
            display: "flex",
            justifyContent: "center",
            color: isSelect ? "#ffffff" : "#4A5568",
          }}
        >
          {ticket?.departureDate && !dataQuery?.returnDate
            ? moment(ticket?.departureDate, DATE_FORMAT_BACK_END, true)
                .locale("vi_VN")
                .format("ddd")
                .replace("T", "Thứ ")
                .replace("CN", "Chủ Nhật")
            : ""}
          {ticket?.departureDate && !dataQuery?.returnDate ? ", " : ""}
          {moment(ticket?.departureDate, DATE_FORMAT_BACK_END, true).format(
            "DD/MM"
          )}{" "}
          {dataQuery?.returnDate ? "- " : ""}
          {dataQuery?.returnDate
            ? moment(ticket?.departureDate, DATE_FORMAT_BACK_END, true)
                .add(DAYS_BETWEEN, "days")
                .format("DD/MM")
            : ""}
        </Typography>
        <Typography
          variant="body2"
          style={{
            whiteSpace: "nowrap",
            display: "flex",
            justifyContent: "center",
            color: isSelect ? "#ffffff" : "#000000",
            fontWeight: 600,
            marginTop: 4,
          }}
        >
          {ticket?.totalPrice
            ? Math.floor(ticket?.farePrice / 1000) + "k"
            : "-"}
        </Typography>
      </Box>
    </ButtonBase>
  );
};

export default FlightDatePrice;
