import TicketFlightCard from "@components/common/card/flight/TicketFlightCard";
import DialogCheckTime2Ways from "@components/common/modal/flight/DialogCheckTime2Ways";
import FlightInfoBoxModal from "@components/common/modal/flight/FlightInfoBoxModal";
import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { listEventFlight } from "@utils/constants";
import * as gtm from "@utils/gtm";
import { checkFlightTime2Ways } from "@utils/helpers";
import React, { useState } from "react";
import FlightFooterListing from "./FlightFooterListing";

const useStyles = makeStyles((theme) => ({
  listFlightCard: {
    backgroundColor: theme.palette.gray.grayLight23,
    padding: "8px 8px 48px 8px",
    height: "100%",
    minHeight: "100vh",
  },
  divider: {
    height: 8,
    backgroundColor: theme.palette.gray.grayLight23,
  },
  footerListing: {
    position: "fixed",
    backgroundColor: theme.palette.white.main,
    zIndex: 20,
    bottom: 0,
  },
}));

const FlightResultBoxOneWay = ({
  dataTickets,
  dataFilter,
  data,
  setParamFilter = () => {},
  paramFilter,
  dataQuery,
}) => {
  const classes = useStyles();
  const [visibleDrawerType, setVisibleDrawerType] = useState(false);
  const [ticket, setTicket] = useState();
  const [ticketId, setTicketId] = useState();
  const [isDialogCheckTime2Ways, setDialogCheckTime2Ways] = useState(false);

  const tempTicketOutBound = data?.outbound?.tickets?.find(
    (ele) => ele?.tid == dataQuery?.ticketOutBoundId
  );

  const toggleDrawer = (open = false, index = 0, el) => (event) => {
    if (!checkFlightTime2Ways(tempTicketOutBound, el.outbound)) {
      setDialogCheckTime2Ways(true);
      return;
    }
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
    if (open) {
      const ticket = dataTickets[index] || {};
      gtm.addEventGtm(listEventFlight.FlightViewDetail);
      setTicketId(dataTickets[index].tid);
      setTicket(dataTickets[index]);
    } else {
      setTicket();
    }
  };

  const getTicketAirline = (id) => {
    let airline = {};
    if (dataFilter?.airlines) {
      airline = dataFilter.airlines.find((al) => al.id === id);
    }
    return airline ? airline : {};
  };

  const getAirlineInfo = () => {
    if (data && ticket) {
      return data?.airlines.find((element) => {
        return element.id == ticket?.outbound?.aid;
      });
    } else {
      return {};
    }
  };

  if (dataTickets && dataTickets.length) {
    return (
      <>
        <Box className={classes.listFlightCard}>
          {!!data?.outbound && (
            <Typography gutterBottom variant="subtitle1">
              Chọn vé chiều về
            </Typography>
          )}
          {dataTickets.map((el, index) => (
            <Box key={index}>
              <Box onClick={toggleDrawer(true, index, el)}>
                <TicketFlightCard
                  item={el.outbound}
                  airline={getTicketAirline(el.outbound.aid)}
                />
              </Box>
              {index < dataTickets.length - 1 ? (
                <Box className={classes.divider} />
              ) : null}
            </Box>
          ))}
        </Box>
        <Box position="relative">
          <Box className={classes.footerListing}>
            <FlightFooterListing
              data={dataFilter}
              setParamFilter={setParamFilter}
              paramFilter={paramFilter}
            />
          </Box>
        </Box>
        <FlightInfoBoxModal
          isTwoWay={false}
          open={visibleDrawerType}
          setOpen={setVisibleDrawerType}
          ticket={ticket?.outbound}
          data={data}
          isTransit={!!ticket?.outbound?.transitTickets}
          airlineInfo={getAirlineInfo()}
          ticketId={ticketId}
        />
        <DialogCheckTime2Ways
          open={isDialogCheckTime2Ways}
          toggleDrawer={() => setDialogCheckTime2Ways(false)}
        />
      </>
    );
  }
};

export default FlightResultBoxOneWay;
