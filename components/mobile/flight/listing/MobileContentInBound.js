import { searchRoundTripTickets } from "@api/flight";
import { Box, LinearProgress, Typography } from "@material-ui/core";
import { createStyles, makeStyles, withStyles } from "@material-ui/styles";
import snackbarSetting from "@src/alert/Alert";
import {
  listString,
  SORTER_FLIGHT_LIST,
  LINK_FLIGHT_BOADING_PASS,
} from "@utils/constants";
import {
  filterAndSort,
  flightCheapestCmp,
  flightFastestCmp,
  flightTimeTakeOffCmp,
  isEmpty,
} from "@utils/helpers";
import { debounce, uniqBy } from "lodash";
import moment from "moment";
import { useSnackbar } from "notistack";
import React, { useState, useEffect } from "react";
import FlightFooterListing from "./FlightFooterListing";
import FlightResultBoxOneWay from "./FlightResultBoxOneWay";

const BorderLinearProgress = withStyles((theme) =>
  createStyles({
    root: {
      height: 2,
      borderRadius: 5,
    },
  })
)(LinearProgress);

const useStyles = makeStyles((theme) => ({
  headerListing: {
    position: "sticky",
    backgroundColor: theme.palette.white.main,
    zIndex: 20,
    top: 0,
  },
  styleHotelItem: {
    padding: "12px 0",
  },
  deveide: {
    height: 6,
    width: "calc(100% + 24px)",
    backgroundColor: theme.palette.gray.grayLight22,
    margin: "0 -12px",
  },
  footerListing: {
    position: "fixed",
    backgroundColor: theme.palette.white.main,
    zIndex: 20,
    bottom: 0,
  },
  listFlightCard: {
    backgroundColor: theme.palette.gray.grayLight23,
    padding: 8,
  },
  divider: {
    height: 8,
    backgroundColor: theme.palette.gray.grayLight23,
  },
  notFoundContainer: {
    marginTop: 160,
    backgroundColor: theme.palette.white.main,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    alignItems: "center",
  },
  wrapNotFound: {
    marginTop: 24,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  imgLoading: {
    zIndex: 4,
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
}));
const MobileContentInBound = ({ dataQuery }) => {
  const classes = useStyles();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const [searchCompleted, setSearchCompleted] = useState(0);
  const [time, setTime] = useState(moment());
  const [dataTickets, setDataTickets] = useState(undefined);
  const [dataFilter, setDataFilter] = useState(undefined);
  const [data, setData] = useState();
  const [paramFilter, setParamFilter] = useState({ sortBy: 0 });
  let breakPolling = true;

  const searchData = React.useCallback(
    debounce(
      async () => {
        if (!dataQuery) {
          return;
        }
        let waitFor = null;
        let polling = true;
        setDataTickets([]);
        setData();
        setSearchCompleted(0);
        let i = 1;
        let n = 1;
        while (polling && breakPolling) {
          const dataPost = {
            waitFor,
            from_airport: dataQuery && dataQuery.origin_code,
            to_airport: dataQuery && dataQuery.destination_code,
            depart: `${dataQuery.departureDate}`,
            return: dataQuery.returnDate
              ? `${dataQuery.returnDate}`
              : undefined,
            num_adults: dataQuery.adultCount,
            num_childs: dataQuery.childCount,
            num_infants: dataQuery.infantCount,
            one_way: !dataQuery.returnDate ? "1" : "0",
            currency: "VND",
            lang: "vi",
            filters: {
              airlines: [],
              paging: { itemsPerPage: 200, page: 1 },
              sort: { priceUp: true },
              ticketClassCodes: dataQuery.seatSearch
                ? [dataQuery.seatSearch]
                : [],
            },
            sort: "tripi_recommended",
          };
          const { data } = await searchRoundTripTickets(dataPost);
          if (data?.code !== 200) {
            data?.message &&
              enqueueSnackbar(
                data?.message,
                snackbarSetting((key) => closeSnackbar(key), { color: "error" })
              );
            break;
          }
          setDataTickets(data?.data?.inbound?.tickets);
          setData(data?.data);
          const fieldFilterList = {
            airlines: data?.data?.airlines,
            airports: data?.data?.airports,
            filters: data?.data?.filters,
            ticketClasses: data?.data?.ticketClasses,
          };
          setDataFilter(fieldFilterList);
          waitFor = data?.data?.waitFor;
          polling = data?.data?.polling;
          if (i === 1) {
            n = 1 + waitFor.split(",").length;
          }
          const searchPercent = (100 * i) / n;

          setSearchCompleted(searchPercent < 100 ? searchPercent : 99);
          i += 1;
        }
        setTime(moment());
        setSearchCompleted(100);
      },
      500,
      {
        trailing: true,
        leading: false,
      }
    ),
    [dataQuery]
  );

  const filterTickets = (dataTickets) => {
    let tempTickets = uniqBy(dataTickets, function(e) {
      return e?.outbound?.flightNumber && e?.outbound?.departureTimeStr;
    });
    dataFilter?.airlines?.map((ele) => {
      tempTickets.find((element, index) => {
        if (element?.outbound?.aid === ele?.id) {
          tempTickets[index].outbound.ticketdetail.isBestPrice = 1;
          return true;
        }
        return false;
      });
    });
    return tempTickets.slice(0, 100);
  };

  const filteredTickets = React.useMemo(() => {
    const { sortBy } = paramFilter;

    return filterAndSort(
      filterTickets(dataTickets),
      paramFilter,
      sortBy === SORTER_FLIGHT_LIST[1].id
        ? flightCheapestCmp
        : sortBy === SORTER_FLIGHT_LIST[3].id
        ? flightFastestCmp
        : sortBy === SORTER_FLIGHT_LIST[2].id
        ? flightTimeTakeOffCmp
        : undefined
    );
  }, [data, paramFilter, dataFilter]);

  useEffect(() => {
    return () => {
      breakPolling = false;
    };
  }, []);

  useEffect(() => {
    searchData();
  }, []);

  if (isEmpty(filteredTickets) && searchCompleted !== 100) {
    return (
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        height="74vh"
      >
        <img
          src={LINK_FLIGHT_BOADING_PASS}
          className={classes.imgLoading}
          alt=""
        />
      </Box>
    );
  }
  if (!isEmpty(filteredTickets)) {
    return (
      <>
        <BorderLinearProgress
          color="secondary"
          variant="determinate"
          value={searchCompleted}
        />
        <FlightResultBoxOneWay
          dataTickets={filteredTickets}
          dataFilter={dataFilter}
          data={data}
          setParamFilter={setParamFilter}
          dataQuery={dataQuery}
        />
      </>
    );
  }
  /**
   * Flight ticket not found
   */
  if (searchCompleted === 100 && isEmpty(filteredTickets)) {
    return (
      <>
        <Box className={classes.notFoundContainer}>
          <img
            src="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_flight_not_found.svg"
            alt=""
          />
          <Box className={classes.wrapNotFound}>
            <Typography variant="subtitle1" component="span">
              {listString.IDS_TEXT_FLIGHT_NOT_FOUND}
            </Typography>
            <Typography variant="caption" component="span">
              {listString.IDS_TEXT_FLIGHT_CHANGE_SEARCH_INFO}
            </Typography>
          </Box>
        </Box>
        <Box position="relative">
          <Box className={classes.footerListing}>
            <FlightFooterListing
              hasListTicket={false}
              dataFilter={dataFilter}
              data={dataFilter}
              setParamFilter={setParamFilter}
              paramFilter={paramFilter}
            />
          </Box>
        </Box>
      </>
    );
  }
  return null;
};

export default MobileContentInBound;
