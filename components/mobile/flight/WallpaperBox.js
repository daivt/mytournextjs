import { Box, IconButton, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconBack } from "@public/icons";
import Image from "@src/image/Image";
import { listString } from "@utils/constants";
import { handleTitleFlightSearchForm } from "@utils/helpers";
import clsx from "clsx";
import { useRouter } from "next/router";
import React from "react";

const useStyles = makeStyles((theme) => ({
  imageHotel: {
    width: "100%",
    height: 152,
  },
  iconBack: {
    stroke: theme.palette.white.main,
  },
  content: {
    borderRadius: 4,
    boxShadow: "0 1px 2px 0 rgba(0, 0, 0, 0.1)",
    backgroundColor: "#fdfdfd",
    position: "relative",
    display: "flex",
    cursor: "pointer",
    color: "#fdfdfd",
  },
  contentHotel: {
    position: "absolute",
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  headerSearchBox: {
    padding: "0 4px",
    position: "absolute",
    zIndex: 3,
    top: 0,
  },
}));

const WallpaperBox = ({ codeFlight, query }) => {
  const router = useRouter();
  const classes = useStyles();

  const handleBackRoute = () => {
    router.back();
  };

  return (
    <Box
      display="flex"
      flexDirection="column"
      color="gray.grayDark1"
      color={14}
    >
      <div className={classes.content}>
        <Image
          srcImage={codeFlight?.thumb}
          className={clsx(classes.imageHotel)}
        />
        <div className={classes.contentHotel}>
          <Box className={classes.headerSearchBox}>
            <IconButton onClick={handleBackRoute}>
              <IconBack className={`svgFillAll ${classes.iconBack}`} />
            </IconButton>
          </Box>
          <Typography
            className={classes.titleHotel}
            variant="h4"
            style={{ marginLeft: 16, paddingTop: 48 }}
          >
            {listString.IDS_MT_TEXT_FLY}{" "}
            {handleTitleFlightSearchForm(query, codeFlight)}
          </Typography>
          <Typography
            variant="caption"
            style={{ marginLeft: 16, marginBottom: 16, opacity: 0.8 }}
          >
            {listString.IDS_TEXT_FIND_AND_BOOKING}
          </Typography>
        </div>
      </div>
    </Box>
  );
};

export default WallpaperBox;
