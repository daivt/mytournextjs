import { getVoucherListPayment } from "@api/flight";
import DiscountCard from "@components/common/card/flight/DiscountCard";
import HotlineBox from "@components/common/card/HotlineBox";
import WhyBookTicket from "@components/common/card/WhyBookTicket";
import VoucherDetailModal from "@components/common/modal/flight/VoucherDetailModal";
import Layout from "@components/layout/mobile/Layout";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import snackbarSetting from "@src/alert/Alert";
import utilStyles from "@styles/utilStyles";
import { FLIGHT_SERVICE } from "@utils/constants";
import { handleTitleFlight, isEmpty } from "@utils/helpers";
import { useSnackbar } from "notistack";
import { default as React, useState } from "react";
import DiscoveryTicket from "./DiscoveryTicket";
import RecentSearch from "./RecentSearch";
import SearchBox from "./SearchBox";
import WallpaperBox from "./WallpaperBox";

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: theme.palette.white.main,
    borderRadius: "16px 16px 0px 0px",
    position: "absolute",
    top: "140px",
    width: "100%",
    padding: "0px 16px",
  },
  historySearchBox: {
    display: "flex",
    flexDirection: "row",
  },
  divider: {
    height: 8,
    margin: "0px -16px 16px",
    backgroundColor: theme.palette.gray.grayLight22,
  },
  discountItem: {
    minWidth: 300,
  },
}));

const listTypeModal = {
  MODAL_USE_PROMO: "MODAL_USE_PROMO",
};

const MobileContent = ({ queryCodeFlight, query }) => {
  const classes = useStyles();
  const classesUtils = utilStyles();
  const [codeFlight, setCodeFlight] = useState(queryCodeFlight);
  const [destinationDataHotTicket, setDestinationDataHotTicket] = useState({});

  const [listVouchers, setListVouchers] = useState([]);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [visibleDrawerPromo, setVisibleDrawerPromo] = useState("");
  const [promoCodeSelected, setPromoCodeSelected] = useState({});
  const toggleDrawerPromo = (open = "") => (event) => {
    setVisibleDrawerPromo(open);
  };

  const handleViewDetailVoucher = (item) => () => {
    setPromoCodeSelected(item);
    setVisibleDrawerPromo(listTypeModal.MODAL_USE_PROMO);
  };

  const handleFetchData = async (keyword = "") => {
    const dataPost = {
      info: { productType: "flight" },
      isUsed: false,
      page: 1,
      module: "flight",
      size: 20,
      term: keyword,
    };
    const { data } = await getVoucherListPayment(dataPost);
    if (data?.code == 200) {
      let dataTemp = [];
      const arrayVoucher = data?.data?.list;
      arrayVoucher.forEach((value, key) => {
        dataTemp.push({
          value,
        });
      });
      setListVouchers(arrayVoucher);
    }
  };

  React.useEffect(() => {
    handleFetchData();
  }, []);

  return (
    <Layout isHeader={false} {...handleTitleFlight(query, queryCodeFlight)}>
      <WallpaperBox codeFlight={codeFlight} query={query} />
      <Box className={classes.container}>
        <SearchBox
          codeFlight={codeFlight}
          dataQuery={query}
          setCodeFlight={setCodeFlight}
          setDestinationDataHotTicket={setDestinationDataHotTicket}
        />

        {!isEmpty(listVouchers) && (
          <>
            <Box className={classes.divider} />
            <Box ml={-1} mb={4}>
              <Box className={classesUtils.scrollViewHorizontal}>
                {listVouchers.map((el) => (
                  <Box
                    key={el.id}
                    className={classes.discountItem}
                    onClick={handleViewDetailVoucher(el)}
                  >
                    <DiscountCard cardInfo={el} />
                  </Box>
                ))}
              </Box>
            </Box>
          </>
        )}
        <RecentSearch />
        <DiscoveryTicket
          dataQuery={query}
          codeFlight={codeFlight}
          destinationDataHotTicket={destinationDataHotTicket}
        />
        <WhyBookTicket source={FLIGHT_SERVICE} />
        <HotlineBox />
        <VoucherDetailModal
          openPromo={visibleDrawerPromo === listTypeModal.MODAL_USE_PROMO}
          toggleDrawerPromo={(openVal) => setVisibleDrawerPromo(openVal)}
          promoCodeSelected={promoCodeSelected}
        />
      </Box>
    </Layout>
  );
};

export default MobileContent;
