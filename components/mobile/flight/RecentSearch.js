import FlightBookingInfoCard from "@components/common/card/FlightBookingInfoCard";
import { Box, makeStyles, Typography } from "@material-ui/core";
import utilStyles from "@styles/utilStyles";
import { listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  historySearchItem: {
    padding: "8px",
    "&:first-child": {
      padding: "8px 8px 8px 2px",
    },
  },
  divider: {
    height: 8,
    margin: "0px -16px 16px",
    backgroundColor: theme.palette.gray.grayLight22,
  },
}));

const RecentSearch = () => {
  const classesUtils = utilStyles();
  const classes = useStyles();
  const [searchHistory, setSearchHistory] = useState({});
  useEffect(() => {
    const ISSERVER = typeof window === "undefined";
    if (!ISSERVER) {
      setSearchHistory(JSON.parse(localStorage.getItem(`SEARCH_RECENT`)));
    }
  }, []);

  if (isEmpty(searchHistory)) return <></>;

  return (
    <>
      <Box y={2}>
        <Typography variant="h5">
          {listString.IDS_TEXT_RECENT_SEARCH}
        </Typography>
        <Typography variant="caption">
          {listString.IDS_TEXT_RECENT_SEARCH_FLIGTH}
        </Typography>
        <Box className={classesUtils.scrollViewHorizontal}>
          {searchHistory.map((el, index) => (
            <Box key={index} className={classes.historySearchItem}>
              <FlightBookingInfoCard item={el} />
            </Box>
          ))}
        </Box>
      </Box>
    </>
  );
};

export default RecentSearch;
