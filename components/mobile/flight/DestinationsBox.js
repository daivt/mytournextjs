import SearchDestinationsBox from "@components/common/mobile/SearchDestinationsBox";
import { Box, ButtonBase, Divider, Grid, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import makeStyles from "@material-ui/styles/makeStyles";
import { IconSwitch } from "@public/icons";
import { listString } from "@utils/constants";
import React from "react";

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: theme.palette.white.main,
    display: "flex",
    justifyContent: "space-between",
    margin: "16px 0px 8px 0px",
  },
  showDot: {
    whiteSpace: "nowrap",
    maxWidth: "132px",
    overflow: "hidden",
    textOverflow: "ellipsis",
    color: theme.palette.gray.grayDark8,
  },
  grayText: {
    color: theme.palette.gray.grayDark8,
  },
}));
const DestinationsBox = ({
  destinationData,
  switchFunction = () => {},
  setDestinationData = () => {},
}) => {
  const [open, setOpen] = React.useState(false);
  const [isFrom, setIsFrom] = React.useState(true);
  const classes = useStyles();
  const theme = useTheme();

  return (
    <>
      <Box className={classes.container}>
        <Grid container>
          <Grid item lg={5} md={5} sm={5} xs={5}>
            <ButtonBase
              style={{
                width: "100%",
                textAlign: "right",
                display: "flex",
                justifyContent: "flex-start",
                minHeight: 75,
                alignItems: "flex-start",
              }}
              onClick={() => {
                setIsFrom(true);
                setOpen(!open);
              }}
            >
              <Box
                display="flex"
                flexDirection="column"
                maxWidth="132px"
                textAlign="left"
              >
                <Typography
                  variant="body2"
                  style={{ marginBottom: 12 }}
                  className={classes.grayText}
                >
                  {listString.IDS_MT_TEXT_FROM}
                </Typography>
                <Typography variant="h4">
                  {destinationData?.origin?.code}
                </Typography>
                <Typography className={classes.showDot} variant="caption">
                  {destinationData?.origin?.location}
                </Typography>
              </Box>
            </ButtonBase>
            <Divider
              style={{
                marginTop: 10,
                backgroundColor: theme.palette.gray.grayLight22,
              }}
            />
          </Grid>
          <Grid item lg={2} md={2} sm={2} xs={2}>
            <Box mt={3} display="flex" justifyContent="center">
              <ButtonBase>
                <IconSwitch onClick={() => switchFunction()} />
              </ButtonBase>
            </Box>
          </Grid>
          <Grid item lg={5} md={5} sm={5} xs={5}>
            <ButtonBase
              style={{
                width: "100%",
                textAlign: "right",
                display: "flex",
                justifyContent: "flex-end",
                minHeight: 75,
                alignItems: "flex-start",
              }}
              onClick={() => {
                setIsFrom(false);
                setOpen(!open);
              }}
            >
              <Box display="flex" flexDirection="column">
                <Typography
                  variant="body2"
                  style={{ marginBottom: 12 }}
                  className={classes.grayText}
                >
                  {listString.IDS_MT_TEXT_TO}
                </Typography>
                <Typography variant="h4">
                  {destinationData?.destination?.code}
                </Typography>
                <Typography className={classes.showDot} variant="caption">
                  {destinationData?.destination?.location}
                </Typography>
              </Box>
            </ButtonBase>
            <Divider
              style={{
                marginTop: 10,
                backgroundColor: theme.palette.gray.grayLight22,
              }}
            />
          </Grid>
        </Grid>
      </Box>
      <SearchDestinationsBox
        open={open}
        handleClose={setOpen}
        isFrom={isFrom}
        setDestinationData={(data) => {
          if (isFrom) {
            setDestinationData({ ...destinationData, origin: data });
          } else {
            setDestinationData({ ...destinationData, destination: data });
          }
        }}
      />
    </>
  );
};

export default DestinationsBox;
