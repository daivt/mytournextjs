import { searchHotTickets } from "@api/flight";
import FlightBookingReviewCard from "@components/common/card/FlightBookingReviewCard";
import { Box, makeStyles } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { formatDate, listString } from "@utils/constants";
import { domesticInBound } from "@utils/domestic";
import { isEmpty } from "@utils/helpers";
import { DATE_FORMAT, DATE_FORMAT_BACK_END } from "@utils/moment";
import find from "lodash/find";
import minBy from "lodash/minBy";
import take from "lodash/take";
import moment from "moment";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  wrapDiscovery: {
    backgroundColor: theme.palette.gray.grayLight23,
    padding: "32px 16px 8px 16px",
    margin: "32px -16px 0 -16px",
  },
  textIntro: {
    color: theme.palette.gray.grayDark8,
  },
  header: {
    marginBottom: 12,
    display: "flex",
    flexDirection: "column",
  },
  ticketItem: {
    marginBottom: 16,
  },
}));

const DiscoveryTicket = ({
  dataQuery,
  codeFlight,
  destinationDataHotTicket,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const [listData, setListData] = useState([]);
  const [departName, setDepartName] = useState("");

  useEffect(() => {
    getBestFly();
  }, [destinationDataHotTicket]);

  const getBestFly = async () => {
    try {
      let departParam = "SGN";
      let departCitySearch = "Hồ Chí Minh";
      if (destinationDataHotTicket?.origin?.code) {
        departParam = destinationDataHotTicket?.origin?.code;
        departCitySearch = destinationDataHotTicket?.origin?.location;
      }
      setDepartName(departCitySearch);

      let listInBound = [];
      domesticInBound.forEach((data, index) => {
        const airportCode = data.code.split("-");
        if (departParam === airportCode[0]) {
          listInBound.push({
            arrivalCity: data?.arrival,
            departCity: departCitySearch,
            fromAirport: departParam,
            toAirport: airportCode[1],
          });
        }
      });

      const itinerariesParam = take(listInBound, 5);
      const dataDTO = {
        adults: 1,
        children: 0,
        fromDate: moment().format(formatDate.firstYear),
        groupByDate: true,
        infants: 0,
        itineraries: itinerariesParam,
        toDate: moment()
          .add(15, "days")
          .format(formatDate.firstYear),
      };
      let listData = [];

      const { data } = await searchHotTickets(dataDTO);
      data?.data?.hotTickets.forEach((el) => {
        if (!isEmpty(el.priceOptions)) {
          const tickets = minBy(el.priceOptions, "totalPrice");
          const itinerary = {
            fromAirport: find(
              data?.data?.airports,
              (o) => o.code === el.itinerary.fromAirport
            ),
            toAirport: find(
              data?.data?.airports,
              (o) => o.code === el.itinerary.toAirport
            ),
          };
          const airlines = find(
            data?.data?.airlines,
            (o) => o.id === tickets.airlineId
          );
          listData.push({
            origin: el.itinerary.fromAirport,
            destination: el.itinerary.toAirport,
            flightFrom: itinerary.fromAirport.name,
            flightTo: itinerary.toAirport.name,
            date: tickets.departureDate.replaceAll("-", "/"),
            airlineLogo: airlines.logo,
            airlineName: airlines.name,
            discountPercent: tickets.discountAdult,
            subPrice:
              (tickets.totalPrice * (100 + tickets.discountAdult)) / 100,
            mainPrice: tickets.totalPrice,
            farePrice: tickets?.farePrice || 0,
          });
        }
      });
      setListData(listData);
    } catch (error) {
      console.log("error", error);
    }
  };

  const handleClickTicket = (item) => () => {
    router.push({
      pathname: "/ve-may-bay/result/outbound",
      query: {
        adultCount: parseInt(dataQuery.adultCount) || 1,
        childCount: parseInt(dataQuery.childCount) || 0,
        infantCount: parseInt(dataQuery.infantCount) || 0,
        seatSearch: "economy",
        departureDate: item?.date
          ? moment(item.date, DATE_FORMAT).format(DATE_FORMAT_BACK_END)
          : moment().format(DATE_FORMAT_BACK_END),
        origin_code: item?.origin,
        origin_location: item?.flightFrom,
        destination_code: item?.destination,
        destination_location: item?.flightTo,
      },
    });
  };

  return (
    <>
      {!isEmpty(listData) && (
        <Box className={classes.wrapDiscovery}>
          <Box className={classes.header}>
            <Typography variant="h5">
              {listString.IDS_MT_TEXT_DISCOVER}
            </Typography>
            <Typography className={classes.textIntro} variant="caption">
              {listString.IDS_MT_TEXT_TOP_FLIGHT_DISCOVERY}&nbsp;
              {departName || ""}
            </Typography>
          </Box>
          <Box className={classes.listItem}>
            {listData.map((el, index) => (
              <Box className={classes.ticketItem} key={index}>
                <FlightBookingReviewCard
                  item={el}
                  bgCircle={theme.palette.gray.grayLight23}
                  handleClickTicket={handleClickTicket}
                />
              </Box>
            ))}
          </Box>
        </Box>
      )}
    </>
  );
};

export default DiscoveryTicket;
