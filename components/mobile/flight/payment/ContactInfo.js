import { Box, Typography } from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import MuiFormControlLabel from "@material-ui/core/FormControlLabel";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles, withStyles } from "@material-ui/styles";
import {
  IconArrowDownToggle,
  IconCheckBox,
  IconCheckBoxActive,
} from "@public/icons";
import AccordionCustom from "@src/accordion/AccordionCustom";
import AccordionDetailsCustom from "@src/accordion/AccordionDetailsCustom";
import AccordionSummaryCustom from "@src/accordion/AccordionSummaryCustom";
import FieldSelectContent from "@src/form/FieldSingleSelectContent";
import FieldTextContent from "@src/form/FieldTextContent";
import { GENDER_LIST, listString, listEventFlight } from "@utils/constants";
import { validPhoneNumberRegex, validNameRegex } from "@utils/regex";
import { useFormikContext } from "formik";
import { useEffect, useState } from "react";
import voca from "voca";
import * as gtm from "@utils/gtm";
import { useSystem } from "@contextProvider/ContextProvider";
import { isEmpty, removeAccent } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  boxTitle: {
    color: theme.palette.black.black3,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  boxDesc: {
    color: theme.palette.gray.grayDark8,
    lineHeight: "17px",
    marginTop: -8,
  },
  contactForm: {},
  inputStyle: {
    "& > input": {
      padding: 0,
    },
  },
  inputSelectStyle: {
    "& > input": {
      padding: 0,
      width: 48,
      height: "auto",
    },
  },
}));

const FormControlLabel = withStyles((theme) => ({
  root: {
    margin: "0 0 0 -12px",
  },
  label: {
    fontSize: 14,
    fontWeight: "normal",
    color: theme.palette.gray.grayDark1,
  },
}))(MuiFormControlLabel);
const MarkSameInfoCheckbox = withStyles((theme) => ({
  root: {
    color: theme.palette.gray.grayLight25,
    "&$checked": {
      color: theme.palette.blue.blueLight8,
    },
  },
  checked: {},
}))((props) => (
  <Checkbox
    color="default"
    {...props}
    icon={<IconCheckBox />}
    checkedIcon={<IconCheckBoxActive />}
  />
));

const ContactInfo = () => {
  const classes = useStyles();
  const theme = useTheme();
  const { setFieldValue, values, errors } = useFormikContext();
  const { systemReducer } = useSystem();
  const informationUser = systemReducer.informationUser || {};
  const [stateCheck, setStateCheck] = useState({
    checkedSameInfo: false,
  });
  const handleCheckbox = (event) => {
    setStateCheck({ ...stateCheck, [event.target.name]: event.target.checked });
    setFieldValue("checkedSame", event.target.checked);
    if (event.target.checked) {
      setFieldValue("gender_0", values.genderContact);
      setFieldValue("name_0", voca.latinise(values.nameContact));
    }
  };
  const getStyleInput = {
    minHeight: 40,
    width: "100%",
    borderTop: "none",
    borderRight: "none",
    borderLeft: "none",
    borderRadius: 0,
  };
  const [expanded, setExpanded] = useState("contactInfo");
  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  useEffect(() => {
    setStateCheck({
      ...stateCheck,
      checkedSameInfo: values?.checkedSame,
    });
  }, []);

  useEffect(() => {
    const ISSERVER = typeof window === "undefined";
    let lastBookData = {};
    if (!ISSERVER) {
      lastBookData = JSON.parse(localStorage.getItem(`LAST_REQUEST`));
    }
    if (!isEmpty(lastBookData)) {
      setFieldValue("genderContact", lastBookData.gender || "");
      setFieldValue("nameContact", lastBookData.name || "");
      setFieldValue("phoneNumberContact", lastBookData.phone || "");
      setFieldValue("emailContact", lastBookData.email || "");
    } else {
      if (!isEmpty(informationUser)) {
        if (isEmpty(values.nameContact)) {
          setFieldValue("nameContact", informationUser.name || "");
        }
        if (isEmpty(values.phoneNumberContact)) {
          setFieldValue("phoneNumberContact", informationUser.phoneInfo || "");
        }
        if (isEmpty(values.emailContact)) {
          setFieldValue("emailContact", informationUser.emailInfo || "");
        }
      }
    }
  }, [informationUser]);

  const handleBlur = (type = "") => {
    if (type === "phoneNumber") {
      gtm.addEventGtm(listEventFlight.FlightInitCheckout);
    } else {
      gtm.addEventGtm(listEventFlight.FlightInitCheckout);
    }
  };

  return (
    <AccordionCustom
      expanded={expanded === "contactInfo"}
      onChange={handleChange("contactInfo")}
    >
      <AccordionSummaryCustom expandIcon={<IconArrowDownToggle />}>
        <Typography className={classes.leftTitle} variant="subtitle1">
          {listString.IDS_MT_TEXT_INFO_CONTACT}
        </Typography>
      </AccordionSummaryCustom>
      <AccordionDetailsCustom>
        <Typography className={classes.boxDesc} variant="caption">
          {listString.IDS_MT_TEXT_INFO_FLIGHT_CONTACT_DESC}
        </Typography>
        <Box display="flex" mt={20 / 8}>
          <FieldSelectContent
            name="genderContact"
            options={GENDER_LIST || []}
            getOptionLabel={(v) => v.label}
            onSelectOption={(value) => {
              setFieldValue("genderContact", value);
              if (stateCheck.checkedSameInfo) {
                setFieldValue("gender_0", value);
              }
            }}
            formControlStyle={{ width: "fit-content" }}
            inputStyle={{
              minHeight: 40,
              border: "none",
              borderBottom: `solid 1px ${theme.palette.gray.grayLight25}`,
              borderRadius: 0,
            }}
            formControlStyle={{
              minWidth: 74,
              width: 74,
              marginRight: 16,
            }}
            className={classes.inputSelectStyle}
            iconRight={<IconArrowDownToggle />}
          />
          <FieldTextContent
            formControlStyle={{ marginRight: 0 }}
            name="nameContact"
            placeholder={listString.IDS_MT_TEXT_CONTACT_NAME}
            inputStyle={getStyleInput}
            className={classes.inputStyle}
            inputProps={{
              autoComplete: "off",
            }}
            onChange={(event) => {
              validNameRegex.test(removeAccent(event.target.value)) &&
                setFieldValue("nameContact", event.target.value.toUpperCase());
              if (stateCheck.checkedSameInfo) {
                setFieldValue(
                  "name_0",
                  removeAccent(event.target.value).toUpperCase()
                );
              }
            }}
            onBlur={(event) => {
              setFieldValue(
                `nameContact`,
                removeAccent(event.target.value).trim()
              );
            }}
            helperTextStyle={{ paddingTop: 8 }}
          />
        </Box>
        <FieldTextContent
          name="phoneNumberContact"
          placeholder={listString.IDS_MT_TEXT_PLACEHOLDER_PHONE_NUMBER}
          inputStyle={getStyleInput}
          className={classes.inputStyle}
          inputProps={{
            autoComplete: "off",
          }}
          onBlur={() => handleBlur("phoneNumber")}
          styleLabel={classes.styleLabel}
          helperTextStyle={{ paddingTop: 8 }}
          onChange={(e) => {
            validPhoneNumberRegex.test(e.target.value) &&
              setFieldValue("phoneNumberContact", e.target.value);
          }}
        />
        <FieldTextContent
          name="emailContact"
          placeholder={listString.IDS_MT_TEXT_EMAIL}
          inputStyle={getStyleInput}
          className={classes.inputStyle}
          inputProps={{
            autoComplete: "off",
          }}
          onBlur={() => handleBlur("email")}
          styleLabel={classes.styleLabel}
          helperTextStyle={{ paddingTop: 8 }}
        />
        <Box display="flex" flexDirection="column">
          <FormControlLabel
            control={
              <MarkSameInfoCheckbox
                checked={stateCheck.checkedSameInfo}
                onChange={handleCheckbox}
                name="checkedSameInfo"
              />
            }
            label={listString.IDS_MT_TEXT_CONTACT_SAME_INFO_CUSTOMER}
          />
        </Box>
      </AccordionDetailsCustom>
    </AccordionCustom>
  );
};

export default ContactInfo;
