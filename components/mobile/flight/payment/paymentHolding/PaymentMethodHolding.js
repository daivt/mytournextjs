import { getPaymentMethodOfHoldingBooking } from "@api/flight";
import AtmModal from "@components/mobile/hotels/payment/AtmModal";
import { Box, Divider, Typography, withStyles } from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import {
  IconMethodAtmCard,
  IconMethodBankTransfer,
  IconQRPay,
  IconRadio,
  IconRadioActive,
} from "@public/icons";
import snackbarSetting from "@src/alert/Alert";
import Image from "@src/image/Image";
import {
  flightPaymentMethodCode,
  listIcons,
  listString,
  prefixUrlIcon,
} from "@utils/constants";
import { getSurchargeMoney, isEmpty } from "@utils/helpers";
import clsx from "clsx";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "column",
  },
  paymentItem: {
    padding: 16,
    borderRadius: 8,
    border: `solid 1px ${theme.palette.gray.grayLight23}`,
    display: "flex",
    flexDirection: "row",
  },
  paymentIcon: {
    width: "14%",
  },
  paymentDesc: {
    width: "76%",
  },
  paymentRadioCheck: {
    width: "10%",
  },
  paymentHoldTime: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: theme.palette.orange.orangeLight1,
    color: theme.palette.orange.main,
    padding: "4px 8px",
    borderRadius: 4,
    width: "70%",
  },
  activeMethod: {
    border: `solid 1px ${theme.palette.blue.blueLight8}`,
    backgroundColor: theme.palette.blue.blueLight11,
  },
  partnerLogo: {
    width: 48,
    height: 24,
    objectFit: "scale-down",
    borderRadius: 4,
    backgroundColor: theme.palette.white.main,
  },
  bankItem: {
    display: "flex",
    alignItems: "center",
  },
  hideBank: {
    display: "none",
  },
  bankList: {
    height: 400,
    overflow: "auto",
  },
  divider: {
    height: 6,
    color: theme.palette.gray.grayLight22,
  },
}));

const PaymentRadio = withStyles((theme) => ({
  root: {
    padding: 0,
  },
  checked: {},
}))((props) => (
  <Checkbox
    color="default"
    {...props}
    icon={<IconRadio />}
    checkedIcon={props.checked ? <IconRadioActive /> : <div></div>}
  />
));

const listTypeModal = {
  MODAL_ATM: "MODAL_ATM",
  MODAL_BT: "MODAL_BT",
};

const PaymentMethodHolding = ({
  paymentMethodChecked,
  setPaymentMethodChecked,
  paymentMethodSelected,
  setBankSelect,
  bankSelected,
  grandTotal,
  setSurchargeMoney,
  bookingId,
  discountMoneyVoucher,
}) => {
  const theme = useTheme();
  const classes = useStyles();
  const router = useRouter();
  const [dataMethod, setDataMethod] = useState([]);
  const [bankList, setBankList] = useState([]);
  const [bankListTransfer, setBankListTransfer] = useState([]);
  const [visibleDrawerType, setVisibleDrawerType] = useState("");
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  useEffect(() => {
    getPaymentMethod();
  }, []);
  const totalMoney = grandTotal - discountMoneyVoucher;
  useEffect(() => {
    if (dataMethod.length) {
      recheckSurchargeMoney();
    }
  }, [discountMoneyVoucher, dataMethod]);

  const toggleDrawer = (open = "") => (event) => {
    setVisibleDrawerType(open);
  };
  const handleSelectBank = (bank = {}) => {
    setBankSelect(bank);
    setVisibleDrawerType("");
  };

  const getPaymentMethod = async () => {
    const dataPost = {
      module: "flight",
      bookingId: bookingId,
    };

    const { data } = await getPaymentMethodOfHoldingBooking(dataPost);
    if (data?.code === "200" || data?.code === 200) {
      let dataTemps = [];
      const arrayMethod = data?.data;
      arrayMethod.forEach((value, key) => {
        switch (value?.code) {
          case flightPaymentMethodCode.ATM:
            setBankList(value?.bankList);
            return dataTemps.push(value);
          case flightPaymentMethodCode.VISA_MASTER:
            return dataTemps.push(value);
          case flightPaymentMethodCode.QR_CODE:
            return dataTemps.push(value);
          case flightPaymentMethodCode.BANK_TRANSFER:
          case flightPaymentMethodCode.BANK_TRANSFER_2:
            setBankListTransfer(value?.bankList);
            return dataTemps.push(value);
          default:
            return {};
        }
      });
      if (paymentMethodSelected === 121 || paymentMethodSelected === 119) {
        if (paymentMethodSelected === 119) {
          dataTemps = dataTemps.filter((item) => item.id !== 119);
        }
        setPaymentMethodChecked(dataTemps[0]?.id);
      } else {
        setPaymentMethodChecked(paymentMethodSelected);
      }
      setDataMethod(dataTemps);
    } else {
      data?.message &&
        enqueueSnackbar(
          data?.message,
          snackbarSetting((key) => closeSnackbar(key), { color: "error" })
        );
      router.back();
    }
  };

  const getIconMethod = (methodCode) => {
    switch (methodCode) {
      case flightPaymentMethodCode.ATM:
        return <IconMethodAtmCard />;
      case flightPaymentMethodCode.QR_CODE:
        return <IconQRPay />;
      case flightPaymentMethodCode.VISA_MASTER:
        return (
          <img
            src={`${prefixUrlIcon}${listIcons.IconMethodVisa}`}
            alt="logo_payment_visa"
            style={{ width: 32, height: 32 }}
          />
        );
      case flightPaymentMethodCode.BANK_TRANSFER:
      case flightPaymentMethodCode.BANK_TRANSFER_2:
        return <IconMethodBankTransfer />;
      default:
        break;
    }
  };

  const handleChangeMethod = (method) => {
    if (method?.code === flightPaymentMethodCode.ATM) {
      setVisibleDrawerType(listTypeModal.MODAL_ATM);
    } else if (
      method?.code === flightPaymentMethodCode.BANK_TRANSFER ||
      method?.code === flightPaymentMethodCode.BANK_TRANSFER_2
    ) {
      setVisibleDrawerType(listTypeModal.MODAL_BT);
    } else {
      setBankSelect({});
    }
    dataMethod.map((item, index) => {
      if (item?.id === method?.id) {
        const moneySur = getSurchargeMoney(
          totalMoney,
          item?.confirmFee,
          item?.percentFee,
          item?.fixedFee
        );
        setSurchargeMoney(moneySur);
      }
    });
    setPaymentMethodChecked(method?.id);
  };

  const recheckSurchargeMoney = () => {
    if (dataMethod.length) {
      const moneySurDefault = getSurchargeMoney(
        totalMoney,
        dataMethod[0]?.confirmFee,
        dataMethod[0]?.percentFee,
        dataMethod[0]?.fixedFee
      );
      setSurchargeMoney(moneySurDefault);
    }
  };

  return (
    <>
      <Box className={classes.wrapper} p={2}>
        <Box>
          <Typography variant="subtitle1">
            {listString.IDS_MT_TEXT_PAYMENT_MENTHODS}
          </Typography>
        </Box>
        <Box className={classes.paymentList}>
          {!isEmpty(dataMethod) &&
            dataMethod.map((item, index) => (
              <Box mt={12 / 8} key={index}>
                <Box
                  className={clsx(
                    classes.paymentItem,
                    item?.id === paymentMethodChecked && classes.activeMethod
                  )}
                  onClick={() => handleChangeMethod(item)}
                >
                  <Box className={classes.paymentIcon}>
                    {getIconMethod(item?.code)}
                  </Box>
                  <Box className={classes.paymentDesc}>
                    <Typography variant="body1">{item?.name}</Typography>
                    {totalMoney &&
                    getSurchargeMoney(
                      totalMoney,
                      item?.confirmFee,
                      item?.percentFee,
                      item?.fixedFee
                    ) > 0 ? (
                      <Typography
                        variant="body2"
                        style={{
                          color: theme.palette.gray.grayDark7,
                          paddingTop: 6,
                        }}
                      >
                        {`${listString.IDS_MT_TEXT_SUB_FEE} ${getSurchargeMoney(
                          totalMoney,
                          item?.confirmFee,
                          item?.percentFee,
                          item?.fixedFee
                        ).formatMoney()}đ`}
                      </Typography>
                    ) : (
                      <Typography
                        variant="body2"
                        style={{
                          color: theme.palette.green.greenLight7,
                          paddingTop: 6,
                        }}
                      >
                        {listString.IDS_MT_TEXT_FREE_SURCHANGE}
                      </Typography>
                    )}
                    {(item?.code === flightPaymentMethodCode.ATM ||
                      item?.code === flightPaymentMethodCode.BANK_TRANSFER ||
                      item?.code === flightPaymentMethodCode.BANK_TRANSFER_2) &&
                      item?.id === paymentMethodChecked &&
                      bankSelected?.logoUrl && (
                        <Box className={classes.bankItem} pt={1}>
                          <Box
                            border={`solid 1px ${theme.palette.gray.grayLight23}`}
                          >
                            <Image
                              srcImage={bankSelected.logoUrl}
                              className={classes.partnerLogo}
                              borderRadiusProp="4px"
                            />
                          </Box>
                          <Box pl={1}>
                            <Typography variant="subtitle2">
                              {bankSelected.name}
                            </Typography>
                          </Box>
                        </Box>
                      )}
                  </Box>
                  <Box className={classes.paymentRadioCheck}>
                    <PaymentRadio
                      checked={item?.id === paymentMethodChecked}
                      name="paymentCheck"
                    />
                  </Box>
                </Box>
              </Box>
            ))}
        </Box>
      </Box>
      <Divider className={classes.divider} />
      <AtmModal
        open={
          visibleDrawerType === listTypeModal.MODAL_ATM ||
          visibleDrawerType === listTypeModal.MODAL_BT
        }
        toggleDrawer={toggleDrawer}
        bankList={
          visibleDrawerType === listTypeModal.MODAL_ATM
            ? bankList
            : bankListTransfer
        }
        handleSelectBank={handleSelectBank}
        bankSelected={bankSelected}
        listTypeModal={listTypeModal}
        visibleDrawerType={visibleDrawerType}
        emailCustomer={""}
      />
    </>
  );
};

export default PaymentMethodHolding;
