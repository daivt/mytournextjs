import {
  getFlightBookingDetail,
  getGeneralInformation,
  getVoucherListPayment,
  repayForHoldingBooking,
  validateVoucherCode,
} from "@api/flight";
import FlightTicketDetailPayment from "@components/common/card/flight/FlightTicketDetailPayment";
import Layout from "@components/layout/mobile/Layout";
import Coupon from "@components/mobile/flight/payment/Coupon";
import PaymentMethodHolding from "@components/mobile/flight/payment/paymentHolding/PaymentMethodHolding";
import TermAndConditionPayment from "@components/mobile/flight/payment/TermAndConditionPayment";
import { Box, Grid, IconButton, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import { IconArrowLeft } from "@public/icons";
import snackbarSetting from "@src/alert/Alert";
import ButtonComponent from "@src/button/Button";
import utilStyles from "@styles/utilStyles";
import { listString, routeStatic } from "@utils/constants";
import clsx from "clsx";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { useEffect, useState } from "react";
import { isEmpty } from "@utils/helpers";

const settings = {
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  speed: 500,
  arrows: false,
  autoplay: true,
};

const useStyles = makeStyles((theme) => ({
  footer: {
    position: "fixed",
    backgroundColor: theme.palette.white.main,
    bottom: 0,
    width: "100%",
  },
  body: {
    backgroundColor: theme.palette.white.main,
    display: "flex",
    flexDirection: "column",
    padding: "12px 0px 0px 12px",
  },
  divider2: {
    backgroundColor: theme.palette.gray.grayLight22,
  },
  button: {
    width: "100%",
    height: 48,
    color: theme.palette.primary.main,
    backgroundColor: theme.palette.white.main,
    borderRadius: "0px 0px 8px 8px",
    "& i": {
      paddingRight: 5,
    },
    "&:hover": {
      backgroundColor: theme.palette.white.main,
    },
  },
  headerBox: {
    borderTop: `solid 1px ${theme.palette.gray.grayLight22}`,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  container: {
    padding: "8px 16px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  leftBox: {
    display: "flex",
    flexDirection: "column",
    width: "50%",
  },
  numGuests: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    color: theme.palette.gray.grayDark8,
  },
  mainPrice: {
    textDecorationLine: "line-through",
    color: theme.palette.gray.grayDark8,
  },
  rightBox: {
    width: "50%",
  },
  boxTicket: {
    padding: 16,
    background: theme.palette.gray.grayLight22,
  },
  ticketDetail: {
    boxShadow: "0px 8px 8px rgba(0, 0, 0, 0.08)",
    background: theme.palette.white.main,
    borderRadius: 8,
    padding: 16,
  },
  slideItem: {
    minWidth: "90%",
    marginRight: 16,
  },
  titleTicket: {
    height: "18px",
    width: "fit-content",
    overflow: "hidden",
    alignItems: "center",
    borderRadius: "4px",
    display: "flex",
    justifyContent: "center",
    color: "white",
    backgroundColor: theme.palette.primary.main,
    fontWeight: 600,
    fontSize: "12px",
    padding: "2px 4px",
    marginBottom: 10,
  },
}));

const MobileContent = ({ title = "", description = "", keywords = "" }) => {
  const classes = useStyles();
  const theme = useTheme();
  const classesUtils = utilStyles();
  const router = useRouter();
  const queryData = router.query;

  const [totalMoney, setTotalMoney] = useState(0);
  const [grandTotal, setGrandTotal] = useState(0);
  const [surchargeMoney, setSurchargeMoney] = useState(0);
  const [discountMoneyVoucher, setDiscountMoneyVoucher] = useState(0);
  const [voucherUsing, setVoucherUsing] = useState({});
  const [listVouchers, setListVouchers] = useState([]);
  const [totalVoucher, setTotalVoucher] = useState(0);
  const [inputSearchVoucher, setInputSearchVoucher] = useState("");
  const [paymentMethodChecked, setPaymentMethodChecked] = useState(0);
  const [bankSelected, setBankSelect] = useState({});
  const [bookingInfo, setBookingInfo] = useState({});
  const [airlines, setAirlines] = useState();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const handleCancelUseVoucher = (voucherUsing) => {
    if (voucherUsing?.code) {
      setVoucherUsing({});
      setDiscountMoneyVoucher(0);
    }
  };

  const actionsGeneralInformation = async () => {
    const { data } = await getGeneralInformation();
    setAirlines(data?.data?.airlines);
  };

  const actionsTicketDetail = async () => {
    try {
      let granMoneyTicket = 0;
      const { data } = await getFlightBookingDetail({
        id: queryData?.bookingId,
      });
      if (data.code === 200) {
        setBookingInfo(data?.data || {});
        granMoneyTicket += parseInt(data?.data?.grandTotal);
        if (granMoneyTicket) setGrandTotal(granMoneyTicket);
        actionsGeneralInformation();
        actionsVoucher(data?.data?.id, data?.data?.promotionCode || "");
      } else {
        router.push({
          pathname: routeStatic.FLIGHT.href,
        });
      }
    } catch (error) {
      router.push({
        pathname: routeStatic.FLIGHT.href,
      });
    }
  };

  const actionsVoucher = async (bookId, code = "") => {
    const dataPost = {
      info: {
        bookingId: bookId,
        data: {
          isExtraServiceHandle: false,
          module: "flight",
          originPoint: 0,
        },
      },
      isUsed: false,
      page: 1,
      module: "flight",
      size: 20,
      term: inputSearchVoucher,
    };
    const { data } = await getVoucherListPayment(dataPost);
    let firstVoucher = {};
    if (code) {
      firstVoucher = data?.data?.list.find((item) => item?.codeDetail == code);
    } else {
      firstVoucher = data?.data?.list[0];
    }

    if (firstVoucher) {
      setVoucherUsing({
        id: firstVoucher?.id,
        code: firstVoucher?.codeDetail,
        description: firstVoucher?.rewardProgram?.title,
        expired: firstVoucher?.responseData?.validTo,
      });
    }

    let dataTemp = [];
    const arrayVoucher = data?.data?.list || [];
    arrayVoucher?.forEach((value, key) => {
      dataTemp.push({
        id: value?.id,
        code: value?.codeDetail,
        description: value?.rewardProgram?.title,
        expired: value?.responseData?.validTo,
      });
    });
    setTotalVoucher(data?.data?.total);
    if (isEmpty(listVouchers)) {
      setListVouchers(dataTemp);
    }
  };

  const validVoucher = async (code = "") => {
    try {
      const paramCheck = {
        bookingId: queryData?.bookingId || 0,
        code: code ? code : voucherUsing?.code,
        data: {
          isExtraServiceHandle: false,
        },
        module: "flight",
        originPoint: 0,
      };
      const { data } = await validateVoucherCode(paramCheck);
      const dataValidVoucher = data;
      if (dataValidVoucher.code != 200) {
        setVoucherUsing({});
        dataValidVoucher?.data?.promotionDescription &&
          enqueueSnackbar(
            dataValidVoucher?.data?.promotionDescription,
            snackbarSetting((key) => closeSnackbar(key), { color: "error" })
          );
      } else {
        setDiscountMoneyVoucher(dataValidVoucher?.data?.discount);
        data?.message &&
          enqueueSnackbar(
            data?.message,
            snackbarSetting((key) => closeSnackbar(key), { color: "success" })
          );
      }
    } catch (error) {}
  };

  const handleRepay = async () => {
    try {
      const dataPost = {
        module: "flight",
        bookingId: queryData?.bookingId,
        paymentMethodBankId: bankSelected?.id,
        paymentMethodId: paymentMethodChecked,
        promotionCode: voucherUsing?.code,
      };
      const { data } = await repayForHoldingBooking(dataPost);

      if (data.code === 200 || data.code === "200") {
        const { paymentLink } = data.data;
        window.location.assign(paymentLink);
      } else {
        data?.message &&
          enqueueSnackbar(
            data?.message,
            snackbarSetting((key) => closeSnackbar(key), { color: "error" })
          );
      }
    } catch (error) {}
  };

  useEffect(() => {
    actionsTicketDetail();
  }, []);

  useEffect(() => {
    if (voucherUsing?.code) {
      validVoucher();
    }
  }, [voucherUsing]);

  useEffect(() => {
    setTotalMoney(grandTotal - discountMoneyVoucher + surchargeMoney);
  }, [grandTotal, discountMoneyVoucher, surchargeMoney]);

  useEffect(() => {
    if (bookingInfo?.id) {
      actionsVoucher(bookingInfo?.id);
    }
  }, [inputSearchVoucher]);

  if (!queryData?.bookingId || !bookingInfo?.outbound) return <div />;
  return (
    <Layout title={title} description={description} keywords={keywords}>
      <Grid container className={classes.headerBox}>
        <Grid item lg={2} md={2} sm={2} xs={2}>
          <IconButton
            onClick={() => {
              router.back();
            }}
            aria-label="close"
          >
            <IconArrowLeft />
          </IconButton>
        </Grid>
        <Grid item lg={8} md={8} sm={8} xs={8}>
          <Box display="flex" justifyContent="center">
            <Typography variant="subtitle1">
              {listString.IDS_TEXT_ORDER_PAYMENT_METHOD}
            </Typography>
          </Box>
        </Grid>
      </Grid>
      <Box className={classes.boxTicket}>
        {bookingInfo?.isTwoWay ? (
          <Box className={classesUtils.scrollViewHorizontal}>
            <Box className={clsx(classes.ticketDetail, classes.slideItem)}>
              <Box className={classes.titleTicket}>
                {listString.IDS_MT_TEXT_DEPARTURE.toUpperCase()}
              </Box>
              <FlightTicketDetailPayment
                ticket={bookingInfo?.outbound}
                airlines={airlines}
              />
            </Box>
            <Box className={clsx(classes.ticketDetail, classes.slideItem)}>
              <Box className={classes.titleTicket}>
                {listString.IDS_MT_TEXT_RETURN.toUpperCase()}
              </Box>
              <FlightTicketDetailPayment
                ticket={bookingInfo?.inbound}
                airlines={airlines}
              />
            </Box>
          </Box>
        ) : (
          <Box className={classes.ticketDetail}>
            <FlightTicketDetailPayment
              ticket={bookingInfo?.outbound}
              airlines={airlines}
            />
          </Box>
        )}
      </Box>
      <Coupon
        listVouchers={listVouchers}
        totalVoucher={totalVoucher}
        voucherUsing={voucherUsing}
        setVoucherUsing={setVoucherUsing}
        handleCancelUseVoucher={handleCancelUseVoucher}
        setInputSearchVoucher={setInputSearchVoucher}
      />
      <PaymentMethodHolding
        discountMoneyVoucher={discountMoneyVoucher}
        paymentMethodChecked={paymentMethodChecked}
        setPaymentMethodChecked={setPaymentMethodChecked}
        paymentMethodSelected={bookingInfo?.paymentMethodId}
        bankSelected={bankSelected}
        setBankSelect={setBankSelect}
        grandTotal={grandTotal}
        setSurchargeMoney={setSurchargeMoney}
        bookingId={queryData?.bookingId}
      />
      <TermAndConditionPayment />
      <Box className={classes.footer}>
        <Box className={classes.container}>
          <Box className={classes.leftBox}>
            <Box className={classes.numGuests}>
              <Typography variant="body2">
                Tổng ({bookingInfo?.numGuests} khách)
              </Typography>
            </Box>
            <Typography variant="h6">
              {totalMoney.formatMoney()}
              &nbsp;đ
            </Typography>
            {grandTotal > totalMoney && (
              <Typography variant="caption" className={classes.mainPrice}>
                {grandTotal.formatMoney()}&nbsp;đ
              </Typography>
            )}
          </Box>
          <ButtonComponent
            type="submit"
            backgroundColor={theme.palette.secondary.main}
            height={48}
            borderRadius={8}
            handleClick={handleRepay}
            className={classes.rightBox}
          >
            <Typography variant="subtitle1">
              {listString.IDS_MT_TEXT_PAYMENT}
            </Typography>
          </ButtonComponent>
        </Box>
      </Box>
    </Layout>
  );
};

export default MobileContent;
