import InvoiceModal from "@components/common/modal/flight/InvoiceModal";
import { Box, Grid, IconButton, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconPencil } from "@public/icons";
import { CustomSwitch } from "@src/element";
import { listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  container: {
    alignItems: "center",
  },
  boxTitle: {
    color: theme.palette.black.black3,
  },
  boxLeft: {
    display: "flex",
    flexDirection: "column",
    padding: 16,
  },
  boxLeftUp: {
    display: "flex",
    alignItems: "center",
  },
  boxLeftDown: {
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    maxWidth: "250px",
  },
  pencil: {
    stroke: theme.palette.blue.blueLight8,
  },
  boxRight: {
    textAlign: "right",
  },
  iconButton: {
    padding: "0 10px",
  },
  swBtn: {
    padding: 0,
  },
}));

const Invoice = ({
  invoiceInfo = {},
  handleInvoiceInfo = () => {},
  recipientEmail,
  recipientName,
}) => {
  const classes = useStyles();
  const [isCheckC, setIsCheckC] = useState(false);
  const [visibleInvoice, setVisibleInvoice] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const toggleDrawer = ({ open, fromEdit = false, isExport = false }) => {
    setVisibleInvoice(open);
    setIsEdit(fromEdit);
    if (!open && !fromEdit && !isExport) {
      handleInvoiceInfo({});
      setIsCheckC(false);
    }
  };

  const handleChangeExport = () => {
    setIsCheckC(!isCheckC);
    if (isCheckC) {
      setIsEdit(false);
      handleInvoiceInfo({});
    } else {
      setVisibleInvoice(true);
    }
  };

  useEffect(() => {
    setIsCheckC(!isEmpty(invoiceInfo));
  }, [invoiceInfo]);

  return (
    <>
      <Grid container className={classes.container}>
        <Grid item xs={10}>
          <Box className={classes.boxLeft}>
            <Box className={classes.boxLeftUp}>
              <Typography variant="subtitle1" className={classes.boxTitle}>
                {listString.IDS_TEXT_EXPORT_INVOICE}
              </Typography>
              {invoiceInfo?.taxIdNumber && (
                <IconButton
                  onClick={() => toggleDrawer({ open: true, fromEdit: true })}
                  classes={{ root: classes.iconButton }}
                >
                  <IconPencil className={`svgFillAll ${classes.pencil}`} />
                </IconButton>
              )}
            </Box>
            <Box className={classes.boxLeftDown}>
              {invoiceInfo?.taxIdNumber && (
                <Typography variant="caption">
                  {`MST: ${invoiceInfo?.taxIdNumber ||
                    ""} - ${invoiceInfo?.companyName || ""}`}
                </Typography>
              )}
            </Box>
          </Box>
        </Grid>
        <Grid item xs={2} className={classes.boxRight}>
          <IconButton onClick={handleChangeExport} className={classes.swBtn}>
            <CustomSwitch checked={isCheckC} />
          </IconButton>
        </Grid>
      </Grid>
      <InvoiceModal
        open={visibleInvoice}
        toggleDrawer={toggleDrawer}
        invoiceInfo={invoiceInfo}
        handleInvoiceInfo={handleInvoiceInfo}
        recipientEmail={recipientEmail}
        recipientName={recipientName}
        isEdit={isEdit}
      />
    </>
  );
};

export default Invoice;
