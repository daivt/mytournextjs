import PriceDetailsTwoWayModal from "@components/common/modal/flight/PriceDetailsTwoWayModal";
import { Box, IconButton, Typography, useTheme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconArrowDownToggle, IconCloseInBackGround } from "@public/icons";
import { listString } from "@utils/constants";
import { useState } from "react";
import { useRouter } from "next/router";
import { isEmpty } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  container: {
    alignItems: "center",
    padding: 16,
  },
  boxTitle: {
    display: "flex",
    alignItems: "center",
  },
  boxTotalPrice: {
    marginTop: 12,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  totalPriceTxt: {
    fontSize: 18,
    lineHeight: "22px",
    fontWeight: 600,
  },
  boxDiscount: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
    padding: "10px 0",
  },
  boxDiscountLeft: {
    display: "flex",
    alignItems: "center",
  },
  discountTxt: {
    marginRight: 8,
  },
  discountCode: {
    height: 24,
    borderRadius: 4,
    border: `1px solid ${theme.palette.gray.grayLight24}`,
    alignItems: "center",
    display: "flex",
    padding: 4,
  },
  discountPrice: {
    color: theme.palette.green.greenLight7,
  },
  boxSubFee: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
    padding: "10px 0",
  },
}));

const PriceDetails = (props) => {
  const theme = useTheme();
  const classes = useStyles();
  const router = useRouter();
  const [visibleInvoice, setVisibleInvoice] = useState(false);

  const toggleDrawer = (open) => () => {
    setVisibleInvoice(open);
  };
  const {
    ticketOutBound,
    ticketInBound,
    voucherUsing,
    surchargeMoney,
    discountMoneyVoucher,
    totalGuest,
  } = props;
  const isDiscountCode = voucherUsing?.code ? true : false;
  const discountCode = voucherUsing?.code;
  const discountMoney = discountMoneyVoucher || 0;
  const ISSERVER = typeof window === "undefined";
  let PRICE_INFO = null;
  let CUSTOMER_INFO = null;
  if (!ISSERVER) {
    PRICE_INFO = JSON.parse(
      localStorage.getItem(`PRICE_INFO_${router.query.requestId}`)
    );
    CUSTOMER_INFO = JSON.parse(
      localStorage.getItem(`CUSTOMER_INFO_${router.query.requestId}`)
    );
  }
  let totalMoneyBoxPrice = 0;
  let totalMoneyInsurance = 0;
  if (PRICE_INFO) {
    totalMoneyInsurance = PRICE_INFO.totalPriceInsurance;
    totalMoneyBoxPrice =
      props.totalMoney +
      PRICE_INFO.totalPriceInsurance +
      PRICE_INFO.bagPrice +
      surchargeMoney -
      discountMoney;
  }

  let detailPriceTicket = {};
  if (!isEmpty(ticketOutBound) && !isEmpty(CUSTOMER_INFO)) {
    const dataBagIn = ticketInBound?.ticket?.outbound?.baggages;
    const dataBagOut = ticketOutBound?.ticket?.outbound?.baggages;
    let totalInboundBagMoney = 0;
    let totalOutboundBagMoney = 0;
    for (let index = 0; index < totalGuest; index++) {
      let bagInfoById = dataBagOut.filter(
        (el) => el.id === CUSTOMER_INFO[`outbound_baggage_${index}`]
      );

      if (!isEmpty(bagInfoById)) {
        totalOutboundBagMoney += bagInfoById[0]?.price;
      }
      if (ticketInBound) {
        let bagInfoByIdInbound = dataBagIn.filter(
          (el) => el.id === CUSTOMER_INFO[`inbound_baggage_${index}`]
        );
        if (!isEmpty(bagInfoByIdInbound)) {
          totalInboundBagMoney += bagInfoByIdInbound[0]?.price;
        }
      }
    }

    detailPriceTicket = {
      outbound: {
        numAdult: ticketOutBound?.searchRequest?.numAdults,
        adultMoney:
          parseInt(ticketOutBound?.ticket?.outbound?.ticketdetail?.farePrice) *
          parseInt(ticketOutBound?.searchRequest?.numAdults),
        numChildren: ticketOutBound?.searchRequest?.numChildren,
        childMoney:
          parseInt(ticketOutBound?.ticket?.outbound?.ticketdetail?.farePrice) *
          parseInt(ticketOutBound?.searchRequest?.numChildren),
        numInfants: ticketOutBound?.searchRequest?.numInfants,
        babyMoney: ticketOutBound?.ticket?.outbound?.ticketdetail?.priceInfants,
        taxFee:
          parseInt(ticketOutBound?.ticket?.totalPrice) -
          parseInt(
            ticketOutBound?.ticket?.outbound?.ticketdetail?.farePrice *
              (ticketOutBound?.searchRequest?.numAdults +
                ticketOutBound?.searchRequest?.numChildren)
          ),
        baggageMoney: totalOutboundBagMoney,
        total: ticketOutBound?.ticket?.totalPrice + totalOutboundBagMoney,
      },
      inbound: ticketInBound
        ? {
            numAdult: ticketInBound?.searchRequest?.numAdults,
            adultMoney:
              parseInt(
                ticketInBound?.ticket?.outbound?.ticketdetail?.farePrice
              ) * parseInt(ticketInBound?.searchRequest?.numAdults),
            numChildren: ticketInBound?.searchRequest?.numChildren,
            childMoney:
              parseInt(
                ticketInBound?.ticket?.outbound?.ticketdetail?.farePrice
              ) * parseInt(ticketInBound?.searchRequest?.numChildren),
            numInfants: ticketInBound?.searchRequest?.numInfants,
            babyMoney:
              ticketInBound?.ticket?.outbound?.ticketdetail?.priceInfants,
            taxFee:
              parseInt(ticketInBound?.ticket?.totalPrice) -
              parseInt(
                ticketInBound?.ticket?.outbound?.ticketdetail?.farePrice *
                  (ticketInBound?.searchRequest?.numAdults +
                    ticketInBound?.searchRequest?.numChildren)
              ),
            baggageMoney: totalInboundBagMoney,
            total: ticketInBound?.ticket?.totalPrice + totalInboundBagMoney,
          }
        : null,
      insurance: totalMoneyInsurance,
      voucherCode: discountCode,
      voucherMoney: discountMoney,
      surchargeMoney: surchargeMoney,
      totalPayment: totalMoneyBoxPrice,
    };
  }

  return (
    <Box className={classes.container}>
      <Box className={classes.boxTitle}>
        <Typography variant="subtitle1">
          {listString.IDS_MT_TEXT_PRICE_DETAIL}
        </Typography>
        <IconButton
          onClick={props.toggleDrawer(true)}
          style={{
            padding: 8,
          }}
        >
          <IconArrowDownToggle
            className="svgFillAll"
            style={{
              stroke: theme.palette.primary.main,
            }}
          />
        </IconButton>
      </Box>
      {discountMoney ? (
        <Box className={classes.boxDiscount}>
          <Box className={classes.boxDiscountLeft}>
            <Typography variant="caption" className={classes.discountTxt}>
              {listString.IDS_MT_TEXT_DISCOUNT_CODE}
            </Typography>
            <Box
              className={classes.discountCode}
              onClick={() => {
                props.handleCancelUseVoucher(voucherUsing);
              }}
            >
              <Typography
                variant="caption"
                style={{
                  marginRight: 6,
                }}
              >
                {discountCode}
              </Typography>
              <IconCloseInBackGround />
            </Box>
          </Box>
          <Typography variant="caption" className={classes.discountPrice}>
            -{discountMoney.formatMoney()}đ
          </Typography>
        </Box>
      ) : null}
      {surchargeMoney > 0 ? (
        <Box className={classes.boxSubFee}>
          <Typography variant="caption">
            {listString.IDS_MT_TEXT_SUB_FEE}
          </Typography>
          <Typography variant="caption">
            {surchargeMoney.formatMoney()}&nbsp;đ
          </Typography>
        </Box>
      ) : null}
      <Box className={classes.boxTotalPrice}>
        <Typography variant="subtitle2">
          {listString.IDS_TEXT_ORDER_PAYMENT_TOTAL_PRICE}
        </Typography>
        <Typography className={classes.totalPriceTxt}>
          {totalMoneyBoxPrice.formatMoney()}&nbsp;đ
        </Typography>
      </Box>
      <Typography variant="caption">
        {listString.IDS_MT_TEXT_INCLUDE_TAX_FEE_VAT}
      </Typography>
      <PriceDetailsTwoWayModal
        open={props.detailPriceModal}
        toggleDrawer={props.toggleDrawer}
        item={detailPriceTicket}
      />
    </Box>
  );
};

export default PriceDetails;
