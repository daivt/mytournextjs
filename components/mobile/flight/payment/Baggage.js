import AddBaggageModal from "@components/common/modal/flight/AddBaggageModal";
import { Box, Grid, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { makeStyles } from "@material-ui/styles";
import { IconBaggage } from "@public/icons";
import { listString } from "@utils/constants";
import { useFormikContext } from "formik";
import { useEffect, useState } from "react";
import { isEmpty, formatTotalMoneyBaggage } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "column",
  },
  viewDetail: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    color: theme.palette.gray.grayDark8,
  },
  iconLuggage: {
    width: 32,
  },
  baggageNote: {
    color: theme.palette.gray.grayDark8,
  },
}));

const Baggage = ({
  outboundBaggageInfo = {},
  inboundBaggageInfo = {},
  totalGuest = 0,
  customerInfo,
  setCustomerInfo = () => {},
  bagInfo = {},
  setBagInfo = () => {},
  setBaggageCustomer = () => {},
  detailTicketStatus,
  setMessageAlert = () => {},
}) => {
  const { values, setFieldValue } = useFormikContext();
  const theme = useTheme();
  const classes = useStyles();

  const [visibleDrawerType, setVisibleDrawerType] = useState(false);

  let customerInfoTemp = [];

  const toggleDrawer = (open) => (event) => {
    let checkNameCustomer = false;
    for (let index = 0; index < totalGuest; index++) {
      if (values[`name_${index}`]) {
        checkNameCustomer = true;
      }
    }

    if (checkNameCustomer) {
      setVisibleDrawerType(open);
      setMessageAlert("");
    } else {
      setMessageAlert(
        "Vui lòng nhập thông tin hành khách trước khi chọn mua thêm hành lý. Xin cảm ơn!"
      );
    }
  };
  const handleSubmit = (data) => () => {
    let dataTempBaggage = {};
    if (data) {
      let numGuest = 0;
      let totalMoney = 0;
      let totalWeight = 0;
      data.forEach((element, index) => {
        let arrayTemp = [...customerInfo];
        if (element.departureWeight) {
          numGuest++;
          totalMoney += element.departureMoney;
          totalWeight += element.departureWeight;
          arrayTemp[index].outboundBaggageId = element.departureBaggage;
        } else {
          element.departureBaggage = undefined;
        }
        setFieldValue(`outbound_baggage_${index}`, element.departureBaggage);
        dataTempBaggage[`outbound_baggage_${index}`] = element.departureBaggage;

        if (element.returnWeight) {
          numGuest++;
          totalMoney += element.returnMoney;
          totalWeight += element.returnWeight;
          arrayTemp[index].inboundBaggageId = element.returnBaggage;
        } else {
          element.returnBaggage = undefined;
        }
        setFieldValue(`inbound_baggage_${index}`, element.returnBaggage);
        dataTempBaggage[`inbound_baggage_${index}`] = element.returnBaggage;
        setCustomerInfo(arrayTemp);
      });
      setBagInfo({
        numGuest: numGuest,
        weight: totalWeight,
        money: totalMoney,
      });
    }
    setVisibleDrawerType(false);
    setBaggageCustomer(dataTempBaggage);
  };

  for (let index = 0; index < totalGuest; index++) {
    customerInfoTemp = [
      ...customerInfoTemp,
      {
        name: values[`name_${index}`],
        keyPosition: index,
        departureBaggage: values[`outbound_baggage_${index}`],
        returnBaggage: values[`inbound_baggage_${index}`],
        returnMoney:
          inboundBaggageInfo?.baggages && values[`inbound_baggage_${index}`]
            ? inboundBaggageInfo?.baggages.find(
                (element) => element?.id === values[`inbound_baggage_${index}`]
              )?.price
            : 0,
        returnWeight:
          inboundBaggageInfo?.baggages && values[`inbound_baggage_${index}`]
            ? inboundBaggageInfo?.baggages.find(
                (element) => element?.id === values[`inbound_baggage_${index}`]
              )?.weight
            : 0,
        departureMoney:
          outboundBaggageInfo?.baggages && values[`outbound_baggage_${index}`]
            ? outboundBaggageInfo?.baggages.find(
                (element) => element?.id === values[`outbound_baggage_${index}`]
              )?.price
            : 0,
        departureWeight:
          outboundBaggageInfo?.baggages && values[`outbound_baggage_${index}`]
            ? outboundBaggageInfo?.baggages.find(
                (element) => element?.id === values[`outbound_baggage_${index}`]
              )?.weight
            : 0,
      },
    ];
  }

  const dataBaggage = {
    arrayCustomer: customerInfoTemp,
    outboundBaggageInfo,
    inboundBaggageInfo,
    return: !!inboundBaggageInfo,
  };

  useEffect(() => {
    if (detailTicketStatus) {
      let numGuest = 0;
      let totalMoney = 0;
      let totalWeight = 0;
      for (let index = 0; index < totalGuest; index++) {
        if (values[`name_${index}`]) {
          let obBagId = null;
          let ibBagId = null;
          if (
            inboundBaggageInfo?.baggages &&
            values[`inbound_baggage_${index}`]
          ) {
            ibBagId = inboundBaggageInfo?.baggages.find(
              (element) => element?.id === values[`inbound_baggage_${index}`]
            );
          }

          if (
            outboundBaggageInfo?.baggages &&
            values[`outbound_baggage_${index}`]
          ) {
            obBagId = outboundBaggageInfo?.baggages.find(
              (element) => element?.id === values[`outbound_baggage_${index}`]
            );
          }

          totalMoney += (obBagId?.price || 0) + (ibBagId?.price || 0);
          let weightTemp = 0;
          weightTemp = (obBagId?.weight || 0) + (ibBagId?.weight || 0);
          totalWeight += weightTemp;
          if (weightTemp) {
            numGuest++;
          }
        }
      }
      setBagInfo({
        numGuest: numGuest,
        weight: totalWeight,
        money: totalMoney,
      });
    }
  }, [detailTicketStatus]);
  return (
    <>
      <Box className={classes.wrapper}>
        <Grid container>
          <Grid
            item
            lg={11}
            md={11}
            sm={11}
            xs={11}
            onClick={toggleDrawer(true)}
          >
            <Box className={classes.containerInfoFilter}>
              <Typography variant="subtitle1">
                {listString.IDS_MT_TEXT_ADD_BAGGAGE}
              </Typography>
            </Box>
            <Box display="flex" flexDirection="row" mt={1}>
              <Box>
                <IconBaggage className={classes.iconLuggage} />
              </Box>
              <Box pl={1} pr={1}>
                {bagInfo?.weight ? (
                  <Box pl={1}>
                    <Typography
                      variant="caption"
                      className={classes.baggageNote}
                    >
                      {bagInfo?.numGuest}&nbsp;
                      {listString.IDS_MT_TEXT_SINGLE_CUSTOMER}&nbsp;-&nbsp;
                      {bagInfo?.weight}&nbsp;kg
                    </Typography>
                    <Typography variant="subtitle2">
                      {bagInfo?.money.formatMoney()} đ
                    </Typography>
                  </Box>
                ) : (
                  <Typography variant="caption" className={classes.baggageNote}>
                    {listString.IDS_MT_TEXT_BAGGAGE_NOTE}
                  </Typography>
                )}
              </Box>
            </Box>
          </Grid>
          <Grid item lg={1} md={1} sm={1} xs={1} className={classes.viewDetail}>
            <ChevronRightIcon />
          </Grid>
        </Grid>
      </Box>
      <AddBaggageModal
        data={dataBaggage}
        open={visibleDrawerType}
        toggleDrawer={toggleDrawer}
        handleSubmit={handleSubmit}
      />
    </>
  );
};
Baggage.propTypes = {};

export default Baggage;
