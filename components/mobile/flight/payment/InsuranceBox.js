import { getInsurancePackage } from "@api/flight";
import { Box, Divider, Typography } from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import MuiFormControlLabel from "@material-ui/core/FormControlLabel";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles, withStyles } from "@material-ui/styles";
import {
  IconArrowDownToggle,
  IconCheckBox,
  IconCheckBoxActive,
} from "@public/icons";
import AccordionCustom from "@src/accordion/AccordionCustom";
import AccordionDetailsCustom from "@src/accordion/AccordionDetailsCustom";
import AccordionSummaryCustom from "@src/accordion/AccordionSummaryCustom";
import Link from "@src/link/Link";
import { listEventFlight, listString } from "@utils/constants";
import * as gtm from "@utils/gtm";
import { useFormikContext } from "formik";
import React from "react";
import { isEmpty } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  topInfo: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    paddingBottom: 20,
  },
  insPrice: {
    color: theme.palette.gray.grayDark7,
  },
  numCustomer: {
    color: theme.palette.black.black3,
  },
  insTotalMoney: {
    color: theme.palette.black.black3,
  },
  leftTitle: {
    color: theme.palette.gray.grayDark7,
    marginLeft: 12,
  },
  chubbContent: {
    fontSize: 14,
    color: theme.palette.gray.grayDark8,
  },
  chubbNote: {
    color: theme.palette.red.redLight5,
  },
  divider: {
    height: 6,
    backgroundColor: theme.palette.gray.grayLight22,
  },
}));

const FormControlLabel = withStyles((theme) => ({
  root: {
    margin: "0px",
  },
}))(MuiFormControlLabel);
const InsuranceCheckbox = withStyles((theme) => ({
  root: {
    margin: "-20px 0px 0px -12px",
    color: theme.palette.gray.grayLight25,
    "&$checked": {
      color: theme.palette.blue.blueLight8,
    },
  },
  checked: {},
}))((props) => (
  <Checkbox
    color="default"
    {...props}
    icon={<IconCheckBox />}
    checkedIcon={<IconCheckBoxActive />}
  />
));

const InsuranceBox = ({
  paramsInsurance = {},
  setTotalPrice = () => {},
  totalPrice,
  setBuyInsurance = () => {},
}) => {
  const { setFieldValue, values, errors } = useFormikContext();

  const [state, setState] = React.useState({
    checkedInsurance: false,
  });
  const [retailPrice, setRetailPrice] = React.useState(0);
  const [insuranceMoney, setInsuranceMoney] = React.useState(0);

  const classes = useStyles();
  const theme = useTheme();

  const handleCheckbox = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
    if (!event.target.checked) {
      setTotalPrice(0);
    } else {
      gtm.addEventGtm(listEventFlight.FlightAddInsurance);
      setTotalPrice(insuranceMoney);
    }
    setFieldValue("checkValidContactMail", event.target.checked);
    setBuyInsurance(event.target.checked);
  };

  const actionGetInsuranceInfo = async () => {
    const args = paramsInsurance;
    args.isMultipleInsurance = true;
    const { data } = await getInsurancePackage(args);
    if (data.code == 200) {
      const result = data?.data[0];
      setFieldValue("insurancePackageCode", result?.code);
      setRetailPrice(result?.retailPrice);
      setInsuranceMoney(paramsInsurance?.numGuest * result?.retailPrice);
      if (values?.checkValidContactMail) {
        setTotalPrice(paramsInsurance?.numGuest * result?.retailPrice);
      }
    }
  };

  React.useEffect(() => {
    setState({ ...state, checkedInsurance: values?.checkValidContactMail });
  }, [values?.checkValidContactMail]);

  React.useEffect(() => {
    actionGetInsuranceInfo();
  }, []);

  if (isEmpty(retailPrice)) return <></>;

  return (
    <>
      <Divider className={classes.divider} />
      <Box pt={2} pl={2} pr={2}>
        <Typography variant="subtitle1">
          {listString.IDS_TEXT_INSURANCE_TITLE}
        </Typography>
        <Box className={classes.topInfo}>
          <Box
            display="flex"
            justifyContent="space-between"
            width="100%"
            alignItems="center"
            mt={4 / 8}
          >
            <FormControlLabel
              control={
                <InsuranceCheckbox
                  checked={state.checkedInsurance}
                  onChange={handleCheckbox}
                  name="checkedInsurance"
                />
              }
              label={
                <Box display="flex" flexDirection="column">
                  <Typography variant="subtitle2">
                    {listString.IDS_TEXT_BUY_INSURANCE}
                  </Typography>
                  <Typography variant="caption" className={classes.insPrice}>
                    {`${retailPrice.formatMoney()}đ/người`}
                  </Typography>
                </Box>
              }
            />
            <Box display="flex" flexDirection="column" textAlign="end">
              <Typography variant="caption" className={classes.numCustomer}>
                {`${paramsInsurance?.numGuest} ${listString.IDS_MT_TEXT_SINGLE_CUSTOMER}`}
              </Typography>
              <Typography variant="caption" className={classes.insTotalMoney}>
                {`${insuranceMoney.formatMoney()}đ`}
              </Typography>
            </Box>
          </Box>
        </Box>
        <AccordionCustom>
          <AccordionSummaryCustom expandIcon={<IconArrowDownToggle />}>
            <Box display="flex" alignItems="center">
              <img
                src="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_logo_chubb.svg"
                alt=""
              />
              <Typography className={classes.leftTitle} variant="body2">
                {listString.IDS_TEXT_INSURANCE_CHUBB_TITLE}
              </Typography>
            </Box>
          </AccordionSummaryCustom>
          <AccordionDetailsCustom>
            <Box className={classes.chubbContent}>
              <Typography variant="caption">
                {listString.IDS_TEXT_CHUBB_CONTENT_HEAD}
              </Typography>
              <ul>
                <li>
                  <Typography variant="caption">
                    {listString.IDS_TEXT_CHUBB_CONTENT_LI_01}
                  </Typography>
                </li>
                <li>
                  <Typography variant="caption">
                    {listString.IDS_TEXT_CHUBB_CONTENT_LI_02}
                  </Typography>
                </li>
                <li>
                  <Typography variant="caption">
                    {listString.IDS_TEXT_CHUBB_CONTENT_LI_03}
                  </Typography>
                </li>
              </ul>
              <p>
                {`${listString.IDS_TEXT_CHUBB_CONTENT_FOOTER_01} `}
                <Link
                  href={listString.IDS_TEXT_CHUBB_TERM_HREF}
                  target="_blank"
                >
                  {listString.IDS_TEXT_CHUBB_CONTENT_LINK}
                </Link>
                {` ${listString.IDS_TEXT_CHUBB_CONTENT_FOOTER_02}`}
              </p>
              <Typography variant="caption" className={classes.chubbNote}>
                {listString.IDS_TEXT_CHUBB_CONTENT_NOTE}
              </Typography>
            </Box>
          </AccordionDetailsCustom>
        </AccordionCustom>
      </Box>
    </>
  );
};

export default InsuranceBox;
