import { getVoucherListPayment } from "@api/flight";
import VoucherCard from "@components/common/card/flight/VoucherCard";
import VoucherListModal from "@components/common/modal/flight/VoucherListModal";
import { Box, Typography, Divider } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { makeStyles } from "@material-ui/styles";
import { IconCoupon } from "@public/icons";
import snackbarSetting from "@src/alert/Alert";
import utilStyles from "@styles/utilStyles";
import { listString, listEventFlight } from "@utils/constants";
import { debounce } from "lodash";
import { useSnackbar } from "notistack";
import React, { useState } from "react";
import * as gtm from "@utils/gtm";
const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "column",
  },
  itemVoucher: {
    boxShadow:
      "0px 0px 6px rgba(0, 0, 0, 0.06), 0px 6px 6px rgba(0, 0, 0, 0.06)",
    overflow: "hidden",
    borderRadius: 8,
    minWidth: 300,
  },
  divider: {
    height: 6,
    color: theme.palette.gray.grayLight22,
  },
}));

const Coupon = ({
  listVouchers,
  totalVoucher,
  voucherUsing,
  setVoucherUsing,
  handleCancelUseVoucher = () => {},
  setInputSearchVoucher,
  visibleDrawerType,
  setVisibleDrawerType,
}) => {
  const theme = useTheme();
  const classes = useStyles();
  const classesUtils = utilStyles();

  const toggleDrawer = (open) => (event) => {
    if (open) {
      gtm.addEventGtm(listEventFlight.FlightViewListPromo);
    }
    setVisibleDrawerType(open);
  };

  const handleSetVoucher = (info) => {
    if (info?.id === voucherUsing?.id) {
      info = {};
      handleCancelUseVoucher(voucherUsing);
    }
    setVoucherUsing(info);
    setVisibleDrawerType(false);
    gtm.addEventGtm(listEventFlight.FlightAddPromo);
  };

  const initItem = listVouchers;

  return (
    <Box className={classes.wrapper}>
      <Box display="flex" onClick={toggleDrawer(true)} px={2} pt={2}>
        <Box display="flex" alignItems="center" width={"50%"}>
          <IconCoupon />
          <Box pl={10 / 8}>
            <Typography variant="subtitle1">
              {listString.IDS_TEXT_VOUCHER}
            </Typography>
          </Box>
        </Box>
        <Box
          display="flex"
          alignItems="center"
          justifyContent="flex-end"
          color={theme.palette.blue.blueLight8}
          width={"50%"}
        >
          <Typography variant="caption">
            {listString.IDS_MT_TEXT_TYPE_PROMO_CODE}
          </Typography>
          <ChevronRightIcon fontSize={"small"} />
        </Box>
      </Box>
      <Box p={2}>
        <Box className={classesUtils.scrollViewHorizontal}>
          {initItem.map((item, index) => (
            <Box key={index} mr={2} className={classes.itemVoucher}>
              <VoucherCard
                item={item}
                bgCircle={theme.palette.gray.grayLight22}
                handleSetVoucher={handleSetVoucher}
                voucherUsing={voucherUsing}
                hasBorder={true}
              />
            </Box>
          ))}
        </Box>
      </Box>
      <Divider className={classes.divider} />
      <VoucherListModal
        data={initItem}
        open={visibleDrawerType}
        toggleDrawer={toggleDrawer}
        handleSetVoucher={handleSetVoucher}
        voucherUsing={voucherUsing}
        totalVoucher={totalVoucher}
        setInputSearchVoucher={setInputSearchVoucher}
      />
    </Box>
  );
};

export default Coupon;
