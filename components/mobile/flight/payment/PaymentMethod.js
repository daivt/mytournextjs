import { getPaymentMethodData } from "@api/flight";
import AtmModal from "@components/mobile/hotels/payment/AtmModal";
import { Box, Divider, Typography, withStyles } from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import {
  IconHoldTicket,
  IconMethodAtmCard,
  IconMethodBankTransfer,
  IconQRPay,
  IconRadio,
  IconRadioActive,
} from "@public/icons";
import snackbarSetting from "@src/alert/Alert";
import Image from "@src/image/Image";
import {
  listString,
  PAYMENT_ATM_CODE,
  PAYMENT_HOLDING_CODE,
  PAYMENT_TRANSFER_CODE,
  PAYMENT_VISA_CODE,
  PAYMENT_VNPAY_CODE,
  listEventFlight,
  prefixUrlIcon,
  listIcons,
} from "@utils/constants";
import { getSurchargeMoney, isEmpty, secondToString } from "@utils/helpers";
import clsx from "clsx";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { useEffect, useState } from "react";

import * as gtm from "@utils/gtm";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "column",
  },
  paymentItem: {
    padding: 16,
    borderRadius: 8,
    border: `solid 1px ${theme.palette.gray.grayLight23}`,
    display: "flex",
    flexDirection: "row",
  },
  paymentIcon: {
    width: "14%",
  },
  paymentDesc: {
    width: "76%",
  },
  paymentRadioCheck: {
    width: "10%",
  },
  paymentHoldTime: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: theme.palette.orange.orangeLight1,
    color: theme.palette.orange.main,
    padding: "4px 8px",
    borderRadius: 4,
    width: "70%",
  },
  activeMethod: {
    border: `solid 1px ${theme.palette.blue.blueLight8}`,
    backgroundColor: theme.palette.blue.blueLight11,
  },
  noteFee: {
    color: theme.palette.gray.grayDark7,
    paddingTop: 6,
  },
  partnerLogo: {
    width: 48,
    height: 24,
    objectFit: "scale-down",
    borderRadius: 4,
    backgroundColor: theme.palette.white.main,
  },
  bankItem: {
    display: "flex",
    alignItems: "center",
  },
  hideBank: {
    display: "none",
  },
  bankList: {
    height: 400,
    overflow: "auto",
  },
  divider: {
    height: 6,
    color: theme.palette.gray.grayLight22,
  },
}));

const PaymentRadio = withStyles((theme) => ({
  root: {
    padding: 0,
  },
  checked: {},
}))((props) => (
  <Checkbox
    color="default"
    {...props}
    icon={<IconRadio />}
    checkedIcon={props.checked ? <IconRadioActive /> : <div></div>}
  />
));

const RenderListBank = (props) => {
  const theme = useTheme();
  const classes = useStyles();
  const arrayBank = props?.data || [];

  return (
    <Box
      className={clsx(
        classes.bankList,
        props.show === false && classes.hideBank
      )}
    >
      {arrayBank.map((item, index) => (
        <Box className={classes.bankItem} key={index} pt={1}>
          <Box>
            <Image
              srcImage={item.logoUrl}
              className={classes.partnerLogo}
              borderRadiusProp="4px"
            />
          </Box>
          <Box pl={1}>
            <Typography variant="subtitle2"> {item.name}</Typography>
          </Box>
        </Box>
      ))}
    </Box>
  );
};

const listTypeModal = {
  MODAL_ATM: "MODAL_ATM",
  MODAL_BT: "MODAL_BT",
};

const PaymentMethod = ({
  ticketOutBound,
  discountMoneyVoucher,
  paymentMethodChecked,
  setPaymentMethodChecked,
  setPaymentMethodCheckedText,
  setBankSelect,
  bankSelected,
  insuranceMoney,
  bagMoney,
  totalMoney,
  setSurchargeMoney,
  ticketStatus,
}) => {
  const theme = useTheme();
  const classes = useStyles();
  const router = useRouter();
  let [dataMethod, setDataMethod] = useState([]);
  const [bankList, setBankList] = useState([]);
  const [bankListTransfer, setBankListTransfer] = useState([]);
  const [visibleDrawerType, setVisibleDrawerType] = useState("");
  const [loadingDone, setLoadingDone] = useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const toggleDrawer = (open = "") => (event) => {
    setVisibleDrawerType(open);
  };
  const handleSelectBank = (bank = {}) => {
    setBankSelect(bank);
    setVisibleDrawerType("");
  };
  // Loại bỏ tiền giảm trừ voucher khi lấy phụ thu
  totalMoney -= discountMoneyVoucher;
  totalMoney += insuranceMoney + bagMoney;

  const getPaymentMethod = async () => {
    let dataPost = {
      supportCOD: true,
      tickets: {
        outbound: {
          agencyId: router?.query?.agencyOutBoundId,
          requestId: router?.query?.requestId,
          ticketId: router?.query?.ticketOutBoundId,
        },
      },
    };
    if (router?.query?.ticketInBoundId) {
      dataPost.tickets = {
        ...dataPost.tickets,
        inbound: {
          agencyId: router?.query?.agencyInBoundId,
          requestId: router?.query?.requestId,
          ticketId: router?.query?.ticketInBoundId,
        },
      };
    }

    const { data } = await getPaymentMethodData(dataPost);
    if (data?.code !== 200) {
      data?.message &&
        enqueueSnackbar(
          data?.message,
          snackbarSetting((key) => closeSnackbar(key), { color: "error" })
        );
    } else {
      let dataTemps = [];
      const arrayMethod = data?.data;
      let dataHoldingMethod = {};
      arrayMethod.forEach((value, key) => {
        if (value?.code === PAYMENT_HOLDING_CODE) {
          dataHoldingMethod = value;
        }
        switch (value?.code) {
          case PAYMENT_ATM_CODE:
            setBankList(value?.bankList);
            return dataTemps.push(value);
          case PAYMENT_VISA_CODE:
            return dataTemps.push(value);
          case PAYMENT_VNPAY_CODE:
            return dataTemps.push(value);
          case PAYMENT_TRANSFER_CODE:
            setBankListTransfer(value?.bankList.slice(0, 1));
            return dataTemps.push(value);
          default:
            return {};
        }
      });
      if (!isEmpty(dataHoldingMethod)) {
        dataTemps.push(dataHoldingMethod);
      }
      setPaymentMethodChecked(dataTemps[0]?.id);
      setPaymentMethodCheckedText(dataTemps[0]?.name);
      setDataMethod(dataTemps);
      setLoadingDone(true);
    }
  };

  const getIconMethod = (methodCode) => {
    switch (methodCode) {
      case PAYMENT_HOLDING_CODE:
        return <IconHoldTicket />;
        break;
      case PAYMENT_ATM_CODE:
        return <IconMethodAtmCard />;
        break;
      case PAYMENT_VNPAY_CODE:
        return <IconQRPay />;
        break;
      case PAYMENT_VISA_CODE:
        return (
          <img
            src={`${prefixUrlIcon}${listIcons.IconMethodVisa}`}
            alt="logo_payment_visa"
            style={{ width: 32, height: 32 }}
          />
        );
        break;
      case PAYMENT_TRANSFER_CODE:
        return <IconMethodBankTransfer />;
        break;

      default:
        break;
    }
  };

  useEffect(() => {
    getPaymentMethod();
  }, []);

  const handleChangeMethod = (method) => {
    gtm.addEventGtm(listEventFlight.FlightAddPaymentInfo);
    if (
      method?.code === PAYMENT_ATM_CODE ||
      method?.code === PAYMENT_TRANSFER_CODE
    ) {
      if (method?.code === PAYMENT_ATM_CODE) {
        setVisibleDrawerType(listTypeModal.MODAL_ATM);
      }
      if (method?.code === PAYMENT_TRANSFER_CODE) {
        if (bankListTransfer.length === 1) {
          setBankSelect(bankListTransfer[0]);
        } else {
          setVisibleDrawerType(listTypeModal.MODAL_BT);
        }
      }
    } else {
      setBankSelect({});
    }
    dataMethod.map((item, index) => {
      if (item?.id === method?.id) {
        const moneySur = getSurchargeMoney(
          totalMoney,
          item?.confirmFee,
          item?.percentFee,
          item?.fixedFee
        );
        setSurchargeMoney(moneySur);
      }
    });
    setPaymentMethodChecked(method?.id);
    setPaymentMethodCheckedText(method?.name);
  };

  const recheckSurchargeMoney = () => {
    if (dataMethod.length) {
      let moneySurDefault = getSurchargeMoney(
        totalMoney,
        dataMethod[0]?.confirmFee,
        dataMethod[0]?.percentFee,
        dataMethod[0]?.fixedFee
      );
      setSurchargeMoney(moneySurDefault);
    }
  };

  useEffect(() => {
    if (loadingDone) {
      recheckSurchargeMoney();
    }
  }, [ticketStatus, loadingDone, discountMoneyVoucher]);

  return (
    <>
      <Box className={classes.wrapper} p={2}>
        <Box>
          <Typography variant="subtitle1">
            {listString.IDS_MT_TEXT_PAYMENT_MENTHODS}
          </Typography>
        </Box>
        <Box className={classes.paymentList}>
          {dataMethod.map((item, index) => (
            <Box mt={12 / 8} key={index}>
              <Box
                className={clsx(
                  classes.paymentItem,
                  item?.id === paymentMethodChecked && classes.activeMethod
                )}
                onClick={() => handleChangeMethod(item)}
              >
                <Box className={classes.paymentIcon}>
                  {getIconMethod(item?.code)}
                </Box>
                <Box className={classes.paymentDesc}>
                  <Typography variant="body1">{item?.name}</Typography>

                  {item?.code === PAYMENT_HOLDING_CODE ? (
                    <Box>
                      <Box color={theme.palette.green.greenLight7}>
                        <Typography variant="caption">
                          {listString.IDS_MT_TEXT_HOLDING_TICKET_PAY_LATER}
                        </Typography>
                      </Box>
                      <Box className={classes.paymentHoldTime} mt={1}>
                        <Typography variant="caption">
                          Giữ chỗ trong{" "}
                          {secondToString(
                            ticketOutBound?.ticket?.outbound?.ticketdetail
                              ?.holdingTime / 1000
                          )}
                        </Typography>
                      </Box>
                    </Box>
                  ) : (
                    <>
                      <Box className={classes.noteFee}>
                        <Typography variant="body2">
                          {getSurchargeMoney(
                            totalMoney,
                            item?.confirmFee,
                            item?.percentFee,
                            item?.fixedFee
                          ) > 0 ? (
                            listString.IDS_MT_TEXT_SUB_FEE +
                            " " +
                            getSurchargeMoney(
                              totalMoney,
                              item?.confirmFee,
                              item?.percentFee,
                              item?.fixedFee
                            ).formatMoney() +
                            "đ"
                          ) : (
                            <Box color={theme.palette.green.greenLight7}>
                              {listString.IDS_MT_TEXT_FREE_SURCHANGE}
                            </Box>
                          )}
                        </Typography>
                      </Box>
                      {(item?.code === PAYMENT_ATM_CODE ||
                        item?.code === PAYMENT_TRANSFER_CODE) &&
                        item?.id === paymentMethodChecked &&
                        bankSelected?.logoUrl && (
                          <Box className={classes.bankItem} pt={1}>
                            <Box
                              border={`solid 1px ${theme.palette.gray.grayLight23}`}
                            >
                              <Image
                                srcImage={bankSelected.logoUrl}
                                className={classes.partnerLogo}
                                borderRadiusProp="4px"
                              />
                            </Box>
                            <Box pl={1}>
                              <Typography variant="subtitle2">
                                {bankSelected.name}
                              </Typography>
                            </Box>
                          </Box>
                        )}
                      {item?.code === PAYMENT_VISA_CODE && (
                        <Box pt={1}>
                          <img src="/images/image_visa_master_card.png" />
                        </Box>
                      )}
                    </>
                  )}
                </Box>
                <Box className={classes.paymentRadioCheck}>
                  <PaymentRadio
                    checked={item?.id === paymentMethodChecked}
                    name="paymentCheck"
                  />
                </Box>
              </Box>
            </Box>
          ))}
        </Box>
      </Box>
      <Divider className={classes.divider} />
      <AtmModal
        open={
          visibleDrawerType === listTypeModal.MODAL_ATM ||
          visibleDrawerType === listTypeModal.MODAL_BT
        }
        toggleDrawer={toggleDrawer}
        bankList={
          visibleDrawerType === listTypeModal.MODAL_BT
            ? bankListTransfer
            : bankList
        }
        handleSelectBank={handleSelectBank}
        bankSelected={bankSelected}
        listTypeModal={listTypeModal}
        visibleDrawerType={visibleDrawerType}
        emailCustomer={""}
      />
    </>
  );
};

export default PaymentMethod;
