import FlightTicketDetailPayment from "@components/common/card/flight/FlightTicketDetailPayment";
import OrderContact from "@components/mobile/account/order/flight/orderDetail/OrderContact";
import OrderCustomerInfo from "@components/mobile/account/order/flight/orderDetail/OrderCustomerInfo";
import OrderInfo from "@components/mobile/account/order/flight/orderDetail/OrderInfo";
import OrderInsurance from "@components/mobile/account/order/flight/orderDetail/OrderInsurance";
import OrderExportInvoice from "@components/mobile/flight/payment/orderDetail/OrderExportInvoice";
import OrderPaymentInfo from "@components/mobile/flight/payment/orderDetail/OrderPaymentInfo";
import {
  Box,
  Dialog,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  IconButton,
  makeStyles,
  Slide,
  Typography,
} from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { IconClock, IconClose } from "@public/icons";
import ButtonComponent from "@src/button/Button";
import utilStyles from "@styles/utilStyles";
import { listString, routeStatic } from "@utils/constants";
import {
  getExpiredTimeMillisecond,
  getPaymentStatusBookingFlight,
  isEmpty,
} from "@utils/helpers";
import clsx from "clsx";
import moment from "moment";
import { useRouter } from "next/router";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  divider: {
    height: 6,
    color: theme.palette.gray.grayLight22,
  },
  dialogTitle: {
    maxHeight: 48,
    padding: 0,
    color: theme.palette.black.black3,
    border: `SOLID 1px ${theme.palette.gray.grayLight22}`,
  },
  dialogContent: {
    padding: "0",
  },
  boxHolder: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: theme.palette.orange.orangeLight1,
    color: theme.palette.orange.main,
    padding: "8px 0px",
  },
  boxFlightTicket: {
    padding: 16,
    background: theme.palette.gray.grayLight22,
  },
  ticketDetail: {
    background: theme.palette.white.main,
    boxShadow: "0px 8px 8px rgba(0, 0, 0, 0.08)",
    borderRadius: 8,
    padding: 16,
    display: "flex",
    flexDirection: "column",
    minWidth: "100%",
  },
  slideItem: {
    minWidth: "86%",
    marginRight: 16,
  },
  titleTicket: {
    height: "18px",
    width: "fit-content",
    overflow: "hidden",
    alignItems: "center",
    borderRadius: "4px",
    display: "flex",
    justifyContent: "center",
    color: "white",
    backgroundColor: theme.palette.primary.main,
    fontWeight: 600,
    fontSize: "12px",
    padding: "2px 4px",
    marginBottom: 10,
  },
  buttonBackHome: {
    padding: "8px 16px",
    borderTop: `SOLID 1px ${theme.palette.gray.grayLight22}`,
  },
}));
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
const OrderDetail = ({
  open = false,
  handleClose = () => {},
  orderDetail = {},
  paymentType,
  airlines = {},
}) => {
  const classes = useStyles();
  const classesUtils = utilStyles();
  const theme = useTheme();
  const paymentStatus = getPaymentStatusBookingFlight(
    orderDetail?.paymentStatus,
    orderDetail?.paymentMethodCode,
    orderDetail?.expiredTime || 0
  );
  const router = useRouter();
  return (
    <Dialog
      onClose={handleClose(false)}
      open={open}
      TransitionComponent={Transition}
      fullScreen
    >
      <DialogTitle disableTypography classes={{ root: classes.dialogTitle }}>
        <Grid container alignItems="center">
          <Grid item xs={4}>
            <IconButton onClick={handleClose(false)}>
              <IconClose />
            </IconButton>
          </Grid>
          <Grid item xs={8}>
            <Typography component="span" variant="subtitle1">
              {listString.IDS_TEXT_ORDER_DETAIL}
            </Typography>
          </Grid>
        </Grid>
      </DialogTitle>
      <DialogContent classes={{ root: classes.dialogContent }}>
        {orderDetail?.expiredTime &&
          orderDetail?.expiredTime / 1000 > moment().unix() && (
            <Box className={classes.boxHolder}>
              <IconClock />
              <Typography variant="caption">{`${
                listString.IDS_TEXT_FLIGHT_BOOKING_HOLD_TIME
              } ${getExpiredTimeMillisecond(
                orderDetail?.expiredTime
              )}`}</Typography>
            </Box>
          )}
        <Box className={classes.boxFlightTicket}>
          {orderDetail.isTwoWay ? (
            <Box className={classesUtils.scrollViewHorizontal}>
              <Box className={clsx(classes.ticketDetail, classes.slideItem)}>
                <Box className={classes.titleTicket}>
                  {listString.IDS_MT_TEXT_DEPARTURE.toUpperCase()}
                </Box>
                <FlightTicketDetailPayment
                  ticket={orderDetail?.outbound}
                  airlines={airlines}
                />
              </Box>
              <Box className={clsx(classes.ticketDetail, classes.slideItem)}>
                <Box className={classes.titleTicket}>
                  {listString.IDS_MT_TEXT_RETURN.toUpperCase()}
                </Box>
                <FlightTicketDetailPayment
                  ticket={orderDetail?.inbound}
                  airlines={airlines}
                />
              </Box>
            </Box>
          ) : (
            <Box className={classes.ticketDetail}>
              <FlightTicketDetailPayment
                ticket={orderDetail?.outbound}
                airlines={airlines}
              />
            </Box>
          )}
        </Box>
        <OrderInfo item={orderDetail} />
        <Divider className={classes.divider} />
        <OrderContact item={orderDetail.mainContact} />
        <Divider className={classes.divider} />
        {!isEmpty(orderDetail?.vatInvoiceInfo) && (
          <>
            <OrderExportInvoice
              invoice={orderDetail.vatInvoiceInfo}
              paymentStatus={paymentStatus}
            />
            <Divider className={classes.divider} />
          </>
        )}
        <OrderCustomerInfo guests={orderDetail.guests} />
        <Divider className={classes.divider} />
        <OrderInsurance
          insuranceContact={orderDetail.insuranceContact}
          insuranceInfo={orderDetail.guests[0].insuranceInfo}
          numberInsurance={orderDetail.numGuests}
        />
        <Divider className={classes.divider} />
        <OrderPaymentInfo
          paymentInfo={orderDetail}
          paymentStatus={paymentStatus}
        />
        <Box className={classes.buttonBackHome}>
          <ButtonComponent
            backgroundColor={theme.palette.secondary.main}
            height={48}
            borderRadius={8}
            handleClick={() => {
              router.push({
                pathname: routeStatic.HOME.href,
              });
            }}
          >
            <Typography variant="subtitle1">
              {listString.IDS_MT_TEXT_NOT_FOUND_403_DES_3}
            </Typography>
          </ButtonComponent>
        </Box>
      </DialogContent>
    </Dialog>
  );
};

OrderDetail.propTypes = {
  orderDetail: PropTypes.object,
};

export default OrderDetail;
