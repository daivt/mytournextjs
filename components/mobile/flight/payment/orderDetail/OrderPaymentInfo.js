import { Box, makeStyles, Typography } from "@material-ui/core";
import { listString } from "@utils/constants";
import React from "react";
const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    justifyContent: "space-between",
    textAlign: "center",
  },
}));

const OrderPaymentInfo = ({ paymentInfo = {}, paymentStatus = {} }) => {
  const classes = useStyles();

  return (
    <Box display="flex" flexDirection="column" pt={2} pr={2} pb={6} pl={2}>
      <Typography variant="subtitle1">
        {listString.IDS_MT_TEXT_PAYMENT_ACTION}
      </Typography>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_TEXT_ORDER_PAYMENT_TOTAL_PRICE}
        </Typography>
        <Typography variant="caption">
          {`${paymentInfo.finalPrice.formatMoney()}đ`}
        </Typography>
      </Box>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_TEXT_ORDER_PAYMENT_METHOD}
        </Typography>
        <Typography
          variant="caption"
          style={{
            width: "50%",
            textAlign: "end",
          }}
        >
          {paymentInfo.paymentMethod}
        </Typography>
      </Box>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_TEXT_ORDER_PAYMENT_STATUS}
        </Typography>
        <Box
          pt={4 / 8}
          pb={4 / 8}
          pr={1}
          pl={1}
          borderRadius={4}
          color={paymentStatus.color}
          bgcolor={paymentStatus.bgColor}
        >
          <Typography variant="caption">{paymentStatus.title}</Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default OrderPaymentInfo;
