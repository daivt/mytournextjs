import { Box, makeStyles, Typography } from "@material-ui/core";
import { IconDownloadInvoice } from "@public/icons";
import Link from "@src/link/Link";
import { listString } from "@utils/constants";
import { getInvoiceStatus, isEmpty } from "@utils/helpers";
const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    justifyContent: "space-between",
    textAlign: "center",
  },
  breakWord: {
    wordWrap: "break-word",
    maxWidth: "60%",
    textAlign: "end",
  },
  attachInvoice: {
    color: theme.palette.blue.blueLight8,
    display: "flex",
  },
}));

const OrderExportInvoice = ({ invoice = {}, paymentStatus = {} }) => {
  const classes = useStyles();
  const status = getInvoiceStatus(paymentStatus?.type, invoice?.status || "");

  return (
    <Box display="flex" flexDirection="column" pt={2} pr={2} pb={12 / 8} pl={2}>
      <Typography variant="subtitle1">
        {listString.IDS_MT_TEXT_INFO_INVOICE}
      </Typography>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_TEXT_PLACEHOLDER_SEARCH_TAX_CODE}
        </Typography>
        <Typography variant="caption" className={classes.breakWord}>
          {invoice?.taxIdNumber}
        </Typography>
      </Box>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_TEXT_PLACEHOLDER_INVOICE_NAME}
        </Typography>
        <Typography variant="caption" className={classes.breakWord}>
          {invoice?.companyName}
        </Typography>
      </Box>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_TEXT_PLACEHOLDER_INVOICE_ADDRESS}
        </Typography>
        <Typography variant="caption" className={classes.breakWord}>
          {invoice?.companyAddress}
        </Typography>
      </Box>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_MT_TEXT_CUSTOMER_NAME}
        </Typography>
        <Typography variant="caption" className={classes.breakWord}>
          {invoice?.recipientName}
        </Typography>
      </Box>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_TEXT_PLACEHOLDER_INVOICE_RECEIVER}
        </Typography>
        <Typography variant="caption" className={classes.breakWord}>
          {invoice?.recipientAddress}
        </Typography>
      </Box>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_MT_TEXT_EMAIL}
        </Typography>
        <Typography variant="caption" className={classes.breakWord}>
          {invoice?.recipientEmail}
        </Typography>
      </Box>
      <Box className={classes.content}>
        <Typography variant="caption">
          {listString.IDS_MT_TEXT_STATUS}
        </Typography>
        <Typography
          variant="caption"
          className={classes.breakWord}
          style={{ color: status?.textColor }}
        >
          {status?.text}
        </Typography>
      </Box>
      {!isEmpty(invoice?.outboundAttachLinks) ? (
        <Box className={classes.content}>
          <Typography variant="caption">
            {listString.IDS_MT_TEXT_INVOICE_OUTBOUND}
          </Typography>
          <Link
            href={invoice?.outboundAttachLinks[0]}
            className={classes.attachInvoice}
          >
            <IconDownloadInvoice />
            <Typography variant="caption">
              {listString.IDS_MT_TEXT_INVOICE_ATTACH_OUTBOUND_NAME}
            </Typography>
          </Link>
        </Box>
      ) : null}
      {!isEmpty(invoice?.inboundAttachLinks) ? (
        <Box className={classes.content}>
          <Typography variant="caption">
            {listString.IDS_MT_TEXT_INVOICE_INBOUND}
          </Typography>
          <Link
            href={invoice?.inboundAttachLinks[0]}
            className={classes.attachInvoice}
          >
            <Typography variant="caption">
              <IconDownloadInvoice />
              {listString.IDS_MT_TEXT_INVOICE_ATTACH_INBOUND_NAME}
            </Typography>
          </Link>
        </Box>
      ) : null}
    </Box>
  );
};

export default OrderExportInvoice;
