import AddPassportModal from "@components/common/modal/flight/AddPassportModal";
import BirthDayField from "@components/common/BirthDayField/BirthDayField";
import DirectoryModal from "@components/common/modal/flight/DirectoryModal";
import { Box, Grid, Typography } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import { useTheme } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/Add";
import { makeStyles } from "@material-ui/styles";
import { getAllCountries } from "@api/flight";
import {
  IconAdultFlight,
  IconChildrenFlight,
  IconBabyFlight,
  IconContact,
  IconEdit,
} from "@public/icons";
import FieldSelectContent from "@src/form/FieldSingleSelectContent";
import FieldTextBaseContent from "@src/form/FieldTextBaseContent";
import { CUSTOMER_TYPE, listString } from "@utils/constants";
import { isEmpty, formatDateOfBirth, removeAccent } from "@utils/helpers";
import clsx from "clsx";
import { useFormikContext } from "formik";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { validNameRegex } from "@utils/regex";
import voca from "voca";
import moment from "moment";
import { DATE_FORMAT_BACK_END } from "@utils/moment";

const useStyles = makeStyles((theme) => ({
  boxTitle: {
    color: theme.palette.black.black3,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  boxDesc: {
    color: theme.palette.blue.blueLight8,
  },
  typeCustomer: {
    backgroundColor: theme.palette.gray.grayLight22,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    width: "80%",
    padding: 6,
  },
  boxInfo: {
    marginBottom: 24,
  },
  cusText: {
    fontSize: 11,
    fontWeight: 400,
    lineHeight: "13px",
    color: theme.palette.gray.grayDark7,
    textAlign: "center",
  },
  inputField: {
    width: "100%",
  },
  hideTextFill: {
    "& .MuiFormHelperText-filled": {
      display: "none",
    },
    "& .MuiInputBase-input": {
      padding: "0 0 8px!important",
    },
  },
  slGender: {
    "& button": {
      display: "none",
    },
    "& .MuiInputBase-input": {
      borderBottom: `solid 1px ${theme.palette.gray.grayLight25}`,
      borderRadius: 0,
    },
  },
  addPassport: {
    color: theme.palette.blue.blueLight8,
    padding: 0,
  },
  blockPassport: {
    marginTop: 6,
    display: "flex",
    alignItems: "center",
    position: "relative",
    width: "100%",
  },
  editPassport: {
    right: 0,
    position: "absolute",
  },
  inputBox: {
    position: "relative",
  },
  iconButton: {
    position: "absolute",
    padding: 0,
    right: 0,
    top: 12,
  },
  brBottom: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight25}`,
  },
  textPassport: {
    color: theme.palette.black.black3,
  },
  inputName: {
    position: "relative",
  },
  inputTitle: {
    position: "absolute",
  },
}));

const CustomerInput = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const { listInputTemp, listGender, customError } = props;
  const gender = listGender;
  const router = useRouter();
  const queryData = router.query;
  const [visibleDrawerAddPassport, setVisibleDrawerAddPassport] = useState(
    false
  );
  const { values, setFieldValue, errors, touched } = useFormikContext();
  const [activePassportModal, setActivePassportModal] = useState(0);
  const toggleDrawerPassPort = (open, index) => () => {
    setActivePassportModal(index);
    setVisibleDrawerAddPassport(open);
  };
  const [visibleDrawer, setVisibleDrawer] = useState("");
  const [directory, setDirectory] = useState([]);
  const [numberPickContact, setNumberPickContact] = useState(0);
  const [typePickContact, setTypePickContact] = useState(0);
  const [countries, setCountries] = useState({});

  const toggleDrawer = (open = false, index = 0, typePick = 0) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setNumberPickContact(index);
    setTypePickContact(typePick);
    setVisibleDrawer(open);
  };

  function getIconCustomer(type) {
    switch (type) {
      case CUSTOMER_TYPE.BABY:
        return <IconBabyFlight />;
        break;
      case CUSTOMER_TYPE.CHILDREN:
        return <IconChildrenFlight />;
        break;
      default:
        return <IconAdultFlight />;
        break;
    }
  }

  function getTextCustomer(type) {
    switch (type) {
      case CUSTOMER_TYPE.BABY:
        return listString.IDS_MT_TEXT_BABY;
        break;
      case CUSTOMER_TYPE.CHILDREN:
        return listString.IDS_MT_TEXT_CHILDREN;
        break;
      default:
        return listString.IDS_MT_TEXT_ADULT;
        break;
    }
  }

  const handleSubmitPassport = (value, index) => () => {
    setVisibleDrawerAddPassport(false);
  };

  const handleBlurDOB = (event, field) => {
    let strDOB = event.target.value;
    strDOB = formatDateOfBirth(strDOB);
    // setFieldValue(field, strDOB);
  };
  const pickContact = (contact) => {
    setFieldValue(`name_${numberPickContact}`, contact?.name);
    setFieldValue(`gender_${numberPickContact}`, contact?.gender);
    if (typePickContact === CUSTOMER_TYPE.ADULT) {
      if (queryData?.needPassport) {
        setFieldValue(`passport_${numberPickContact}`, contact?.passport);
      }
    } else {
      setFieldValue(`dob_${numberPickContact}`, contact?.dob);
    }
    setVisibleDrawer(false);
  };

  const getCountries = async () => {
    try {
      const { data } = await getAllCountries();
      if (data?.code == 200) {
        setCountries(data?.data?.countries);
      }
    } catch (error) {}
  };

  useEffect(() => {
    const ISSERVER = typeof window === "undefined";

    if (!ISSERVER) {
      const tempDirectory = JSON.parse(localStorage.getItem(`DIRECTORY`)) || [];
      setDirectory(tempDirectory.reverse().slice(0, 10));
    }
    if (queryData?.needPassport) {
      getCountries();
    }
  }, []);

  return (
    <Box className={classes.partnerFilter}>
      {!isEmpty(listInputTemp) && (
        <>
          {listInputTemp.map((dataInput, index) => (
            <Grid container key={index} className={classes.boxInfo}>
              <Grid item lg={2} md={2} sm={2} xs={2}>
                <Box className={classes.typeCustomer}>
                  {getIconCustomer(dataInput?.type)}
                  <Box className={classes.cusText} mt={1}>
                    <span>{getTextCustomer(dataInput?.type)}</span>
                  </Box>
                </Box>
              </Grid>
              <Grid item lg={10} md={10} sm={10} xs={10}>
                <Box
                  display="flex"
                  flexDirection="column"
                  className={classes.inputBox}
                  data1={values[`name_${index}`]}
                >
                  {values[`name_${index}`] && (
                    <Typography
                      variant={"body2"}
                      className={classes.inputTitle}
                    >
                      {listString.IDS_MT_TEXT_CONTACT_NAME_SIMPLE}
                    </Typography>
                  )}
                  <FieldTextBaseContent
                    className={classes.inputName}
                    name={`name_${index}`}
                    inputProps={{
                      autoComplete: "off",
                      padding: "0 8px 8px 8px",
                    }}
                    inputStyle={
                      values[`name_${index}`] ? { marginTop: 12 } : {}
                    }
                    label={
                      values[`name_${index}`]
                        ? ""
                        : listString.IDS_MT_TEXT_CONTACT_NAME_SIMPLE
                    }
                    placeholder={listString.IDS_MT_TEXT_CONTACT_NAME}
                    value={values[`name_${index}`]}
                    onChange={(event) => {
                      if (
                        validNameRegex.test(removeAccent(event.target.value))
                      ) {
                        setFieldValue(
                          `name_${index}`,
                          event.target.value.toUpperCase()
                        );
                      }
                    }}
                    onBlur={(event) => {
                      setFieldValue(
                        `name_${index}`,
                        removeAccent(event.target.value).trim()
                      );
                    }}
                  />
                  {!isEmpty(directory) && (
                    <IconButton
                      className={classes.iconButton}
                      aria-label="search"
                      onClick={toggleDrawer(true, index, dataInput?.type)}
                    >
                      <IconContact />
                    </IconButton>
                  )}
                  <Box
                    component="span"
                    fontSize={12}
                    fontWeight={400}
                    pt={12 / 8}
                  >
                    {listString.IDS_MT_TEXT_GENDER}
                  </Box>
                  <Box className={clsx(classes.hideTextFill)} display="flex">
                    <FieldSelectContent
                      name={`gender_${index}`}
                      className={classes.slGender}
                      value={values[`gender_${index}`]}
                      options={gender || []}
                      getOptionLabel={(v) => v.label_1}
                      onSelectOption={(value) => {
                        setFieldValue(`gender_${index}`, value);
                      }}
                      inputStyle={{
                        minHeight: 40,
                        border: "none",
                      }}
                      formControlStyle={{
                        border: "none",
                        marginRight: 0,
                      }}
                      // helperTextStyle={{ display: "none" }}
                    />
                    {dataInput?.type !== CUSTOMER_TYPE.ADULT && (
                      <BirthDayField
                        disableFuture
                        name={`dob_${index}`}
                        date={
                          values[`dob_${index}`] &&
                          moment(values[`dob_${index}`], DATE_FORMAT_BACK_END)
                        }
                        update={(value) => {
                          setFieldValue(
                            `dob_${index}`,
                            moment(value).format(DATE_FORMAT_BACK_END)
                          );
                        }}
                        errorMessage={
                          !isEmpty(errors[`dob_${index}`]) &&
                          touched[`dob_${index}`]
                            ? listString.IDS_MT_TEXT_PLEASE_ENTER_DOB
                            : customError[`dob_${index}`]
                            ? customError[`dob_${index}`]
                            : ""
                        }
                        inputProps={{
                          autoComplete: "off",
                          padding: "0 8px 8px 8px",
                        }}
                        inputStyle={{
                          border: "none",
                          borderRadius: 0,
                          margin: "2px 0 0 10px",
                          padding: 0,
                        }}
                      />
                    )}
                  </Box>
                  {queryData.needPassport === "true" && (
                    <Box
                      className={classes.blockPassport}
                      onClick={toggleDrawerPassPort(true, index)}
                    >
                      <IconButton className={classes.addPassport}>
                        {values[`passport_${index}`] !== "" ? (
                          <Typography
                            variant="body1"
                            className={classes.textPassport}
                          >
                            {listString.IDS_MT_TEXT_PASSPORT}
                            {": "}
                            {values[`passport_${index}`]}
                          </Typography>
                        ) : (
                          <>
                            <AddIcon />
                            <Typography variant="body1">
                              {listString.IDS_MT_TEXT_ADD_PASSPORT}
                            </Typography>
                          </>
                        )}
                      </IconButton>
                      {values[`passport_${index}`] !== "" && (
                        <IconEdit className={classes.editPassport} />
                      )}
                    </Box>
                  )}
                </Box>
              </Grid>
            </Grid>
          ))}
          {queryData.needPassport === "true" && (
            <AddPassportModal
              open={visibleDrawerAddPassport}
              toggleDrawer={toggleDrawerPassPort}
              index={activePassportModal}
              handleSubmitPassport={handleSubmitPassport}
              countries={countries}
            />
          )}
          <DirectoryModal
            open={visibleDrawer}
            toggleDrawer={toggleDrawer}
            directory={directory}
            pickContact={pickContact}
          />
        </>
      )}
    </Box>
  );
};
export default CustomerInput;
