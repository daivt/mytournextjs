import { bookTicket } from "@api/flight";
import BootstrapTooltip from "@components/common/bootstrapTooltip/BootstrapTooltip";
import DialogBookTicketProcessing from "@components/common/modal/flight/DialogBookTicketProcessing";
import { Box, makeStyles } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { IconInfo } from "@public/icons";
import snackbarSetting from "@src/alert/Alert";
import ButtonComponent from "@src/button/Button";
import { listEventFlight, listString, STEP_NUMBER } from "@utils/constants";
import * as gtm from "@utils/gtm";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import React, { useState } from "react";

const useStyles = makeStyles((theme) => ({
  container: {
    padding: "8px 16px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  leftBox: {
    display: "flex",
    flexDirection: "column",
    width: "50%",
  },
  numGuests: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    color: theme.palette.gray.grayDark8,
  },
  mainPrice: {
    textDecorationLine: "line-through",
    color: theme.palette.gray.grayDark8,
  },
  rightBox: {
    width: "50%",
  },
  customTooltip: {
    maxWidth: 290,
    maxHeight: 100,
    background: theme.palette.green.greenLight7,
    padding: "8px 14px 8px 8px",
    boxSizing: "border-box",
    borderRadius: 6,
    fontSize: 14,
    lineHeight: "16px",
    margin: "24px 0",
  },
  arrowTooltip: {
    color: theme.palette.green.greenLight7,
    left: "12px!important",
  },
}));

const TotalPriceStepTwoBox = ({
  isDesktop,
  totalGuest,
  totalMoney,
  totalSubMoney,
  voucherUsing,
  paymentMethodChecked,
  paymentMethodCheckedText,
  notModal,
  bankSelected,
  insuranceMoney,
  bagMoney,
  bagInfo,
  surchargeMoney,
  discountMoneyVoucher,
  toggleDrawer = () => {},
  inboundBaggageInfo,
  outboundBaggageInfo,
  step = STEP_NUMBER.PAYMENT_STEP_3,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [clickPayment, setClickPayment] = useState(false);
  const [offTooltip, setOffTooltip] = useState(true);
  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  setTimeout(() => {
    setOffTooltip(false);
  }, 10000);

  let subPrice = 0;
  if (notModal) {
    bagMoney = bagInfo?.money;
  }

  if (totalSubMoney) {
    subPrice = totalSubMoney + bagMoney + insuranceMoney + surchargeMoney;
  } else {
    subPrice = totalMoney + bagMoney + insuranceMoney + surchargeMoney;
  }

  let voucherDesc = "";
  if (voucherUsing?.code) {
    voucherDesc = (
      <Box>
        Đã áp dụng mã&nbsp;
        <b>
          {voucherUsing?.code}&nbsp;-&nbsp;
          {voucherUsing?.description}
        </b>
      </Box>
    );
  }

  totalMoney =
    totalMoney +
    bagMoney +
    insuranceMoney +
    surchargeMoney -
    discountMoneyVoucher;
  const actionsBookTicket = async () => {
    setClickPayment(true);
    if (clickPayment) {
      enqueueSnackbar(
        "Hệ thống đang xử lý! Vui lòng đợi trong giây lát.",
        snackbarSetting((key) => closeSnackbar(key), { color: "error" })
      );
      return;
    }
    const customerInfo =
      JSON.parse(
        localStorage.getItem(`CUSTOMER_INFO_${router.query.requestId}`)
      ) || [];
    const totalGuest =
      parseInt(router?.query?.adultCount) +
      parseInt(router?.query?.childCount) +
      parseInt(router?.query?.infantCount);
    const tempGuests = [];
    if (customerInfo?.nameContact) {
      for (var i = 0; i < totalGuest; i++) {
        let obBagId = null;
        let ibBagId = null;
        if (
          inboundBaggageInfo?.baggages &&
          customerInfo[`inbound_baggage_${i}`]
        ) {
          ibBagId = inboundBaggageInfo?.baggages.find(
            (element) => element?.id === customerInfo[`inbound_baggage_${i}`]
          )?.id;
        }

        if (
          outboundBaggageInfo?.baggages &&
          customerInfo[`outbound_baggage_${i}`]
        ) {
          obBagId = outboundBaggageInfo?.baggages.find(
            (element) => element?.id === customerInfo[`outbound_baggage_${i}`]
          )?.id;
        }

        let cusName = customerInfo[`name_${i}`].trim();
        tempGuests[i] = {
          lastName: cusName.substr(0, cusName.indexOf(" ")).trim(),
          firstName: cusName
            .substr(cusName.indexOf(" "), cusName.length)
            .trim(),
          gender: customerInfo[`gender_${i}`],
          outboundBaggageId: obBagId || undefined,
          inboundBaggageId: ibBagId || undefined,
          insuranceInfo: customerInfo.checkValidContactMail
            ? {
                insurancePackageCode: customerInfo?.insurancePackageCode,
                fromDate: `${router?.query?.departureDate}` + " 00:00:00",
                toDate: router?.query?.returnDate
                  ? `${router?.query?.returnDate}` + " 00:00:00"
                  : `${router?.query?.departureDate}` + " 03:00:00",
              }
            : null,
          passport: customerInfo[`passport_${i}`],
          passportExpiredDate: customerInfo[`passport_expired_${i}`],
          passportCountryId: customerInfo[`passport_country_${i}`],
          nationalityCountryId: customerInfo[`passport_country_${i}`],
          dob: customerInfo[`dob_${i}`],
        };
      }

      let contactName = customerInfo?.nameContact.trim();
      let params = {
        contact: {
          addr1: "",
          email: customerInfo?.emailContact,
          lastName: contactName.substr(0, contactName.indexOf(" ")).trim(),
          firstName: contactName
            .substr(contactName.indexOf(" "), contactName.length)
            .trim(),
          phone1: customerInfo?.phoneNumberContact,
          gender: customerInfo?.genderContact === "M" ? "M" : "F",
          title: customerInfo?.genderContact === "M" ? "Mr" : "Mrs",
        },
        guests: tempGuests,
        insuranceContact: {
          addr1: "",
          email: customerInfo?.emailContact,
          firstName: customerInfo?.nameContact.substr(
            customerInfo?.nameContact.indexOf(" "),
            customerInfo?.nameContact.length
          ),
          lastName: customerInfo?.nameContact.substr(
            0,
            customerInfo?.nameContact.indexOf(" ")
          ),
          phone1: customerInfo?.phoneNumberContact,
          gender: customerInfo?.genderContact === "M" ? "M" : "F",
          title: customerInfo?.genderContact === "M" ? "Mr" : "Mrs",
        },
        paymentMethodId: paymentMethodChecked,
        point: 0,
        promoCode: voucherUsing?.code,
        tickets: {
          inbound: parseInt(router?.query?.ticketInBoundId)
            ? {
                agencyId: router?.query?.agencyInBoundId,
                requestId: router?.query?.ticketInBoundId
                  ? parseInt(router?.query?.requestId)
                  : undefined,
                ticketId: parseInt(router?.query?.ticketInBoundId),
              }
            : null,
          outbound: {
            agencyId: parseInt(router?.query?.agencyOutBoundId),
            requestId: parseInt(router?.query?.requestId),
            ticketId: parseInt(router?.query?.ticketOutBoundId),
          },
        },
        callbackDomain: `${window.location.origin}`,
        isMultipleInsurance: customerInfo?.isMultipleInsurance,
        paymentMethodBankId: bankSelected?.id,
      };

      if (customerInfo?.invoiceInfo) {
        let tempInvoice = customerInfo?.invoiceInfo;
        tempInvoice.recipientPhone = customerInfo?.phoneNumberContact;
        params = {
          ...params,
          invoiceInfo: tempInvoice,
        };
      }

      if (customerInfo?.checkValidInvoice && isDesktop) {
        params = {
          ...params,
          invoiceInfo: {
            companyAddress: customerInfo?.companyAddress,
            companyName: customerInfo?.companyName,
            customerName: customerInfo?.recipientName,
            note: customerInfo?.note || "",
            recipientAddress: customerInfo?.recipientAddress,
            recipientEmail: customerInfo?.recipientEmail,
            recipientName: customerInfo?.recipientName,
            taxIdNumber: customerInfo?.taxIdNumber,
            recipientPhone: customerInfo?.phoneNumberContact,
          },
        };
      }
      gtm.addEventGtm(listEventFlight.FlightPurchase, {
        amount: totalMoney,
        ticketId: router?.query?.ticketOutBoundId,
      });
      const { data } = await bookTicket(params);

      if (data.code === 200 || data.code === "200") {
        const { paymentLink } = data.data;
        window.location.assign(paymentLink);
      } else {
        setClickPayment(false);
        data?.message &&
          enqueueSnackbar(
            data?.message,
            snackbarSetting((key) => closeSnackbar(key), { color: "error" })
          );
      }
    } else {
      setClickPayment(false);
      enqueueSnackbar(
        listString.IDS_TEXT_NOT_FOUND_INFO_CUSTOMER,
        snackbarSetting((key) => closeSnackbar(key), { color: "error" })
      );
    }
  };
  return (
    <>
      {isDesktop ? (
        <ButtonComponent
          type="submit"
          backgroundColor={theme.palette.secondary.main}
          height={48}
          width={170}
          borderRadius={8}
          handleClick={() => {
            actionsBookTicket();
          }}
        >
          <Typography variant="subtitle1">
            {paymentMethodChecked && paymentMethodChecked === 121
              ? listString.IDS_MT_TEXT_BUTTON_HOLD_BOOKING
              : listString.IDS_TEXT_PAYMENT_STEP_2}
          </Typography>
        </ButtonComponent>
      ) : (
        <Box className={classes.container}>
          <Box className={classes.leftBox} onClick={toggleDrawer(true)}>
            <BootstrapTooltip
              open={offTooltip ? true : open}
              onClose={handleClose}
              onOpen={handleOpen}
              title={voucherDesc || ""}
              placement="top"
              classes={{
                tooltip: classes.customTooltip,
                arrow: classes.arrowTooltip,
              }}
            >
              <Box className={classes.numGuests}>
                <Typography variant="body2">
                  Tổng ({totalGuest} khách)
                </Typography>
                <IconInfo
                  className="svgFillAll"
                  style={{
                    stroke: theme.palette.blue.blueLight8,
                    marginLeft: 4,
                  }}
                />
              </Box>
            </BootstrapTooltip>
            <Typography variant="h6">
              {totalMoney.formatMoney()}
              &nbsp;đ
            </Typography>
            {subPrice > totalMoney && (
              <Typography variant="caption" className={classes.mainPrice}>
                {subPrice.formatMoney()}&nbsp;đ
              </Typography>
            )}
          </Box>
          <ButtonComponent
            type="submit"
            backgroundColor={theme.palette.secondary.main}
            height={48}
            borderRadius={8}
            handleClick={() => {
              if (notModal) {
                // router.push({
                //   pathname: "/ve-may-bay/payment/paymentMethodList",
                //   query: {
                //     ...router?.query,
                //   },
                // });
              } else {
                actionsBookTicket();
              }
            }}
            className={classes.rightBox}
          >
            <Typography variant="subtitle1">
              {step === STEP_NUMBER.PAYMENT_STEP_2
                ? listString.IDS_MT_TEXT_CONTINUE
                : paymentMethodChecked && paymentMethodChecked === 121
                ? listString.IDS_MT_TEXT_BUTTON_HOLD_BOOKING
                : listString.IDS_MT_TEXT_PAYMENT}
            </Typography>
          </ButtonComponent>
        </Box>
      )}
      <DialogBookTicketProcessing open={clickPayment} />
    </>
  );
};

export default TotalPriceStepTwoBox;
