import {
  getGeneralInformation,
  getTicketDetail,
  getVoucherListPayment,
  validateVoucherCode,
} from "@api/flight";
import FlightTicketDetail from "@components/common/card/flight/FlightTicketDetail";
import FlightInfoDetailBoxModal from "@components/common/modal/flight/FlightInfoDetailBoxModal";
import Layout from "@components/layout/mobile/Layout";
import TotalPriceStepTwoBox from "@components/mobile/flight/payment/TotalPriceStepTwoBox";
import {
  Box,
  Button,
  Divider,
  Grid,
  IconButton,
  Typography,
} from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import { IconArrowLeft, IconArrowRight, IconChevronRight } from "@public/icons";
import { listString } from "@utils/constants";
import { getContactInfoTemp, getInfoGuestTemp, isEmpty } from "@utils/helpers";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import Coupon from "../payment/Coupon";
import PaymentMethod from "../payment/PaymentMethod";
import PriceDetails from "../payment/PriceDetails";
import TermAndConditionPayment from "../payment/TermAndConditionPayment";
import { useSnackbar } from "notistack";
import snackbarSetting from "@src/alert/Alert";

const settings = {
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  speed: 500,
  arrows: false,
  autoplay: true,
};

const useStyles = makeStyles((theme) => ({
  footer: {
    position: "fixed",
    backgroundColor: theme.palette.white.main,
    zIndex: 20,
    bottom: 0,
    width: "100%",
  },
  bodyBox: {
    padding: 4,
    display: "flex",
    flexDirection: "column",
    background: theme.palette.gray.grayLight22,
  },
  body: {
    backgroundColor: theme.palette.white.main,
    display: "flex",
    flexDirection: "column",
  },
  divider2: {
    height: 1,
    backgroundColor: theme.palette.gray.grayLight22,
    margin: "0px 12px",
  },
  button: {
    width: "100%",
    height: 48,
    color: theme.palette.primary.main,
    backgroundColor: theme.palette.white.main,
    borderRadius: "0px 0px 8px 8px",
    "& i": {
      paddingRight: 5,
    },
    "&:hover": {
      backgroundColor: theme.palette.white.main,
    },
  },
  headerBox: {
    borderTop: `solid 1px ${theme.palette.gray.grayLight22}`,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
}));
const MobileContent = ({ title = "", description = "", keywords = "" }) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const queryData = router.query;
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [totalMoney, setTotalMoney] = useState(0);
  const [totalSubMoney, setTotalSubMoney] = useState(0);
  const [surchargeMoney, setSurchargeMoney] = useState(0);
  const [insuranceMoney, setInsuranceMoney] = useState(0);
  const [bagMoney, setBagMoney] = useState(0);
  const [discountMoneyVoucher, setDiscountMoneyVoucher] = useState(0);
  const [voucherUsing, setVoucherUsing] = useState({});
  const [listVouchers, setListVouchers] = useState([]);
  const [totalVoucher, setTotalVoucher] = useState(0);
  const [inputSearchVoucher, setInputSearchVoucher] = useState("");
  const [paymentMethodChecked, setPaymentMethodChecked] = useState(0);
  const [paymentMethodCheckedText, setPaymentMethodCheckedText] = useState("");
  const [bankSelected, setBankSelect] = useState({});
  const [detailTicketOutBound, setDetailTicketOutBound] = useState([]);
  const [detailTicketInBound, setDetailTicketInBound] = useState(null);
  const [generalInfomation, setGeneralInformation] = useState();
  const [visibleDrawerType, setVisibleDrawerType] = useState(false);
  const [visibleDrawerTypeVoucher, setVisibleDrawerTypeVoucher] = useState(
    false
  );
  const [detailPriceModal, setDetailPriceModal] = useState(false);
  const [detailTicketStatus, setDetailTicketStatus] = useState(false);
  const [isFirstTimeError, setFirstTimeError] = useState(false);

  const toggleDrawerPrice = (open) => () => {
    setDetailPriceModal(open);
  };

  const handleCancelUseVoucher = (voucherUsing) => {
    if (voucherUsing?.code) {
      setVoucherUsing({});
      setDiscountMoneyVoucher(0);
    }
  };

  const actionsTicketDetail = async () => {
    let moneyTicket = 0;
    let moneyTicketPromo = 0;
    if (queryData?.ticketOutBoundId) {
      const params = {
        agencyId: queryData.agencyOutBoundId,
        requestId: queryData.requestId,
        ticketId: queryData.ticketOutBoundId,
      };
      const { data } = await getTicketDetail(params);
      if (data?.code === 400) {
        data?.message &&
          enqueueSnackbar(
            data?.message,
            snackbarSetting((key) => closeSnackbar(key), { color: "error" })
          );
        router.push({
          pathname: "/ve-may-bay/",
          query: {},
        });
      }
      setDetailTicketOutBound(data?.data);
      moneyTicket += parseInt(data?.data?.ticket?.totalPrice);
      if (data?.data?.ticket?.outbound?.ticketdetail?.discount) {
        moneyTicketPromo +=
          moneyTicket +
          parseInt(data?.data?.ticket?.outbound?.ticketdetail?.discount);
      }
    }
    if (queryData?.ticketInBoundId) {
      const params = {
        agencyId: queryData.agencyInBoundId,
        requestId: queryData.requestId,
        ticketId: queryData.ticketInBoundId,
      };
      const { data } = await getTicketDetail(params);
      setDetailTicketInBound(data?.data);
      moneyTicket += parseInt(data?.data?.ticket?.totalPrice);
      if (data?.data?.ticket?.outbound?.ticketdetail?.discount) {
        moneyTicketPromo +=
          moneyTicket +
          parseInt(data?.data?.ticket?.outbound?.ticketdetail?.discount);
      }
    }
    if (moneyTicket) setTotalMoney(moneyTicket);
    if (moneyTicketPromo) setTotalSubMoney(moneyTicketPromo);
    setDetailTicketStatus(true);
  };

  const actionsGeneralInformation = async () => {
    const { data } = await getGeneralInformation();
    setGeneralInformation(data?.data);
  };

  const actionsVoucher = async () => {
    const dataPost = {
      info: { productType: "flight" },
      isUsed: false,
      page: 1,
      module: "flight",
      size: 20,
      term: inputSearchVoucher,
    };
    const { data } = await getVoucherListPayment(dataPost);
    const firstVoucher = data?.data?.list[0];
    if (firstVoucher) {
      setVoucherUsing({
        id: firstVoucher?.id,
        code: firstVoucher?.codeDetail,
        description: firstVoucher?.rewardProgram?.title,
        expiredTo: firstVoucher?.responseData?.validTo,
      });
      if (inputSearchVoucher) {
        setVisibleDrawerTypeVoucher(false);
      }
    }

    let dataTemp = [];
    const arrayVoucher = data?.data?.list;
    arrayVoucher?.forEach((value, key) => {
      dataTemp.push({
        id: value?.id,
        code: value?.codeDetail,
        description: value?.rewardProgram?.title,
        expiredTo: value?.responseData?.validTo,
      });
    });
    if (isEmpty(listVouchers)) {
      setListVouchers(dataTemp);
      setTotalVoucher(data?.data?.total);
    }
  };

  let customerInfo = {};
  let priceInfo = {};

  const ISSERVER = typeof window === "undefined";
  if (!ISSERVER) {
    customerInfo = JSON.parse(
      localStorage.getItem(`CUSTOMER_INFO_${queryData.requestId}`)
    );
    priceInfo = JSON.parse(
      localStorage.getItem(`PRICE_INFO_${router.query.requestId}`)
    );
  }
  useEffect(() => {
    actionsTicketDetail();
    actionsGeneralInformation();
    if (priceInfo) {
      setInsuranceMoney(priceInfo?.totalPriceInsurance);
      setBagMoney(priceInfo?.bagPrice);
    }
  }, []);

  useEffect(() => {
    actionsVoucher();
  }, [inputSearchVoucher]);

  let totalGuest = 0;
  if (queryData.adultCount) {
    let numAdults = queryData.adultCount;
    let numChildren = queryData.childCount;
    let numInfants = queryData.infantCount;
    totalGuest =
      parseInt(numAdults) + parseInt(numChildren) + parseInt(numInfants);
  }

  let tempGuests = {};
  let tempContact = {};
  if (customerInfo) {
    tempGuests = getInfoGuestTemp(customerInfo, router, totalGuest);
    tempContact = getContactInfoTemp(customerInfo);
  }

  const validVoucher = async () => {
    try {
      if (voucherUsing?.code) {
        const paramCheck = {
          code: voucherUsing?.code,
          data: {
            contact: tempContact,
            guests: tempGuests,
            insuranceContact: customerInfo?.checkValidContactMail
              ? tempContact
              : null,
            paymentMethodId: paymentMethodChecked,
            point: 0,
            promoCode: "",
            tickets: {
              inbound: queryData?.ticketInBoundId
                ? {
                    agencyId: queryData.agencyInBoundId,
                    requestId: queryData.requestId,
                    ticketId: queryData.ticketInBoundId,
                  }
                : null,
              outbound: {
                agencyId: queryData.agencyOutBoundId,
                requestId: queryData.requestId,
                ticketId: queryData.ticketOutBoundId,
              },
            },
          },
          module: "flight",
          originPoint: 0,
        };

        const { data } = await validateVoucherCode(paramCheck);
        const dataValidVoucher = data;

        if (dataValidVoucher.code === 200) {
          setDiscountMoneyVoucher(dataValidVoucher?.data?.discount);
        } else {
          isFirstTimeError &&
            dataValidVoucher?.message &&
            enqueueSnackbar(
              dataValidVoucher?.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
          setVoucherUsing({});
          setDiscountMoneyVoucher(0);
          setFirstTimeError(true);
        }
      }
    } catch (error) {}
  };

  useEffect(() => {
    validVoucher();
  }, [voucherUsing]);

  if (!queryData || !detailTicketOutBound) return <div />;
  return (
    <Layout title={title} description={description} keywords={keywords}>
      <Grid container className={classes.headerBox}>
        <Grid item lg={2} md={2} sm={2} xs={2}>
          <IconButton
            onClick={() => {
              router.back();
            }}
            aria-label="close"
          >
            <IconArrowLeft />
          </IconButton>
        </Grid>
        <Grid item lg={8} md={8} sm={8} xs={8}>
          <Box display="flex" justifyContent="center">
            <Typography variant="subtitle1">
              {listString.IDS_TEXT_ORDER_PAYMENT_METHOD}
            </Typography>
          </Box>
        </Grid>
      </Grid>
      <Box className={classes.bodyBox}>
        <div className={classes.bodyBox}>
          {!queryData?.ticketInBoundId ? (
            <>
              <FlightTicketDetail
                ticket={detailTicketOutBound?.ticket?.outbound}
                data={{
                  ...detailTicketOutBound,
                  ticketClasses: generalInfomation?.ticketclass,
                }}
                isTransit={
                  !!detailTicketOutBound?.ticket?.outbound?.transitTickets
                }
                airlineInfo={detailTicketOutBound?.agency}
                title="Chuyến bay"
                isSimple={true}
                isSeat={true}
                isSlider
              />
              <Divider className={classes.divider2} />
              <Button
                type="submit"
                className={classes.button}
                variant="contained"
                disableElevation
                onClick={() => {
                  setVisibleDrawerType(true);
                }}
              >
                Xem chi tiết chuyến bay
                <IconChevronRight
                  className="svgFillAll"
                  style={{
                    stroke: theme.palette.primary.main,
                    marginLeft: 8,
                  }}
                />
              </Button>
            </>
          ) : (
            <Box>
              <Box className={classes.body}>
                <Typography
                  variant="subtitle2"
                  style={{
                    backgroundColor: theme.palette.primary.main,
                    color: theme.palette.white.main,
                    width: 80,
                    textAlign: "center",
                    padding: "2px 4px",
                    borderRadius: 4,
                    marginLeft: 12,
                    marginTop: 12,
                  }}
                >
                  CHIỀU ĐI
                </Typography>
                <Box
                  style={{
                    margin: "8px 0 12px 12px",
                    color: theme.palette.gray.grayDark8,
                  }}
                  display="flex"
                  flexDirection="column"
                >
                  <Typography variant="subtitle1" style={{ display: "flex" }}>
                    {queryData?.origin_location} ({queryData?.origin_code})
                    &nbsp;
                    <IconArrowRight />
                    &nbsp;
                    {queryData?.destination_location} (
                    {queryData?.destination_code})
                  </Typography>
                  <Typography
                    variant="body2"
                    style={{ display: "flex", marginTop: 4 }}
                  >
                    {detailTicketOutBound?.ticket?.outbound?.departureTimeStr},{" "}
                    {detailTicketOutBound?.ticket?.outbound?.departureDayStr}{" "}
                    &bull;{" "}
                    {detailTicketOutBound?.ticket?.outbound?.arrivalTimeStr},{" "}
                    {detailTicketOutBound?.ticket?.outbound?.arrivalDayStr}
                  </Typography>
                </Box>
              </Box>
              {queryData?.returnDate && (
                <Box className={classes.body}>
                  <Divider className={classes.divider2} />
                  <Box mt={3 / 2}>
                    <Typography
                      variant="subtitle2"
                      style={{
                        backgroundColor: theme.palette.primary.main,
                        color: theme.palette.white.main,
                        width: 80,
                        textAlign: "center",
                        padding: "2px 4px",
                        borderRadius: 4,
                        marginLeft: 12,
                      }}
                    >
                      CHIỀU VỀ
                    </Typography>
                    <Box
                      style={{
                        margin: "8px 0 12px 12px",
                        color: theme.palette.gray.grayDark8,
                      }}
                      display="flex"
                      flexDirection="column"
                    >
                      <Typography
                        variant="subtitle1"
                        style={{ display: "flex" }}
                      >
                        {queryData?.destination_location} (
                        {queryData?.destination_code}) &nbsp;
                        <IconArrowRight />
                        &nbsp;
                        {queryData?.origin_location} ({queryData?.origin_code})
                      </Typography>
                      <Typography
                        variant="body2"
                        style={{ display: "flex", marginTop: 4 }}
                      >
                        {
                          detailTicketInBound?.ticket?.outbound
                            ?.departureTimeStr
                        }
                        ,{" "}
                        {detailTicketInBound?.ticket?.outbound?.departureDayStr}{" "}
                        &bull;{" "}
                        {detailTicketInBound?.ticket?.outbound?.arrivalTimeStr},{" "}
                        {detailTicketInBound?.ticket?.outbound?.arrivalDayStr}
                      </Typography>
                    </Box>
                  </Box>
                </Box>
              )}
              <Box className={classes.body}>
                <Divider className={classes.divider2} />
              </Box>
              <Button
                type="submit"
                className={classes.button}
                variant="contained"
                disableElevation
                onClick={() => {
                  setVisibleDrawerType(true);
                }}
              >
                Xem chi tiết chuyến bay
                <IconChevronRight
                  className="svgFillAll"
                  style={{
                    stroke: theme.palette.primary.main,
                    marginLeft: 8,
                  }}
                />
              </Button>
            </Box>
          )}
        </div>
        <FlightInfoDetailBoxModal
          open={visibleDrawerType}
          setOpen={setVisibleDrawerType}
          ticket={detailTicketOutBound?.ticket?.outbound}
          ticketInBound={detailTicketInBound?.ticket?.outbound}
          ticketPersion={detailTicketOutBound?.searchRequest}
          data={{
            ...detailTicketOutBound,
            ticketClasses: generalInfomation?.ticketclass,
          }}
          dataInBound={{
            ...detailTicketInBound,
            ticketClasses: generalInfomation?.ticketclass,
          }}
          isTransit={!!detailTicketOutBound?.ticket?.outbound?.transitTickets}
          airlineInfo={detailTicketOutBound?.agency}
        />
      </Box>
      <Coupon
        listVouchers={listVouchers}
        totalVoucher={totalVoucher}
        voucherUsing={voucherUsing}
        setVoucherUsing={setVoucherUsing}
        handleCancelUseVoucher={handleCancelUseVoucher}
        setInputSearchVoucher={setInputSearchVoucher}
        visibleDrawerType={visibleDrawerTypeVoucher}
        setVisibleDrawerType={setVisibleDrawerTypeVoucher}
      />
      <PaymentMethod
        ticketOutBound={detailTicketOutBound}
        discountMoneyVoucher={discountMoneyVoucher}
        paymentMethodChecked={paymentMethodChecked}
        setPaymentMethodChecked={setPaymentMethodChecked}
        setPaymentMethodCheckedText={setPaymentMethodCheckedText}
        bankSelected={bankSelected}
        setBankSelect={setBankSelect}
        insuranceMoney={insuranceMoney}
        bagMoney={bagMoney}
        totalMoney={totalMoney}
        setSurchargeMoney={setSurchargeMoney}
        ticketStatus={detailTicketStatus}
      />
      <PriceDetails
        surchargeMoney={surchargeMoney}
        voucherUsing={voucherUsing}
        discountMoneyVoucher={discountMoneyVoucher}
        totalMoney={totalMoney}
        totalSubMoney={totalSubMoney}
        ticketOutBound={detailTicketOutBound}
        ticketInBound={detailTicketInBound}
        totalGuest={
          parseInt(queryData.adultCount) + parseInt(queryData.childCount)
        }
        detailPriceModal={detailPriceModal}
        toggleDrawer={toggleDrawerPrice}
        handleCancelUseVoucher={(event) => {
          handleCancelUseVoucher(event);
        }}
      />
      <TermAndConditionPayment />
      <Box className={classes.footer}>
        <TotalPriceStepTwoBox
          isDesktop={false}
          totalGuest={totalGuest}
          totalMoney={totalMoney}
          insuranceMoney={insuranceMoney}
          bagMoney={bagMoney}
          totalSubMoney={totalSubMoney}
          voucherUsing={voucherUsing}
          paymentMethodChecked={paymentMethodChecked}
          paymentMethodCheckedText={paymentMethodCheckedText}
          bankSelected={bankSelected}
          surchargeMoney={surchargeMoney}
          discountMoneyVoucher={discountMoneyVoucher}
          detailPriceModal={detailPriceModal}
          toggleDrawer={toggleDrawerPrice}
          outboundBaggageInfo={detailTicketOutBound?.ticket?.outbound}
          inboundBaggageInfo={detailTicketInBound?.ticket?.outbound}
        />
      </Box>
    </Layout>
  );
};

export default MobileContent;
