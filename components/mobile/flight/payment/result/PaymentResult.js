/* eslint-disable no-nested-ternary */
import { getBankTransferInfo, getGeneralInformation } from "@api/flight";
import FlightTicketDetailPayment from "@components/common/card/flight/FlightTicketDetailPayment";
import Layout from "@components/layout/mobile/Layout";
import OrderDetail from "@components/mobile/flight/payment/orderDetail/OrderDetail";
import FlightBankTransfer from "@components/mobile/flight/payment/result/FlightBankTransfer";
import { Box, Divider, Grid, makeStyles, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import {
  IconArrowRight,
  IconBookingFlightFail,
  IconBookingFlightSuccess,
  IconChevronRight,
  IconDot,
} from "@public/icons";
import ButtonComponent from "@src/button/Button";
import {
  flightPaymentMethodCode,
  listString,
  paymentTypes,
  routeStatic,
} from "@utils/constants";
import {
  getExpiredTimeMillisecond,
  isEmpty,
  resetDataAfterBookTicket,
} from "@utils/helpers";
import {
  C_DATE_TIME_FORMAT,
  DATE_FORMAT,
  DATE_FORMAT_BACK_END,
  HOUR_MINUTE,
} from "@utils/moment";
import moment from "moment";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  wraper: {
    backgroundColor: theme.palette.blue.blueLight8,
    padding: "24px 16px",
    minHeight: "100vh",
    marginBottom: "64px",
  },
  buttonBackHome: {
    position: "fixed",
    zIndex: 20,
    bottom: 0,
    width: "100vw",
    padding: "8px 16px",
    borderTop: `SOLID 1px ${theme.palette.gray.grayLight22}`,
    background: theme.palette.white.main,
  },
  divider: {
    height: 1,
    color: theme.palette.gray.grayLight22,
  },
  boxInfo: {
    position: "relative",
    background: theme.palette.white.main,
    borderRadius: 20,
  },
  boxStatus: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    textAlign: "center",
  },
  boxTicket: {
    padding: "16px 16px 22px 16px",
    display: "flex",
    flexDirection: "column",
  },
  boxCode: {
    background: theme.palette.white.main,
    borderRadius: 20,
    display: "flex",
    justifyContent: "space-around",
    textAlign: "center",
    alignItems: "center",
    padding: "24px 0",
  },
  boxCodeInfo: {
    background: theme.palette.white.main,
    borderRadius: 20,
    textAlign: "center",
    alignItems: "center",
    padding: "24px 0",
  },
  codeLeft: {
    color: theme.palette.blue.blueLight8,
    fontSize: 18,
    lineHeight: "22px",
    fontWeight: 600,
  },
  codeRight: {
    fontSize: 18,
    lineHeight: "22px",
    fontWeight: 600,
    color: theme.palette.pink.main,
  },
  orderCode: {
    borderLeft: `SOLID 1px ${theme.palette.gray.grayLight22}`,
  },
  dotted: {
    position: "absolute",
    borderBottom: `dotted 4px ${theme.palette.blue.blueLight8}`,
    margin: "-2px 20px 0px 20px",
    width: "calc(100% - 40px)",
  },
  boxHolder: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: theme.palette.orange.orangeLight1,
    color: theme.palette.orange.main,
    padding: "4px 8px",
    borderRadius: 4,
    marginBottom: 8,
  },
  bodyBoxTicket: {
    display: "flex",
    flexDirection: "column",
  },
  bodyTicket: {
    backgroundColor: theme.palette.white.main,
    display: "flex",
    flexDirection: "column",
  },
  titleTicket: {
    height: "18px",
    width: "fit-content",
    overflow: "hidden",
    alignItems: "center",
    borderRadius: "4px",
    display: "flex",
    justifyContent: "center",
    color: "white",
    backgroundColor: theme.palette.primary.main,
    fontWeight: 600,
    fontSize: "12px",
    padding: "2px 4px",
  },
  iconDot: {
    margin: 4,
  },
}));

const PaymentResult = ({
  orderDetail = {},
  paymentType = paymentTypes.FAIL,
  fromBankTransfer = false,
  title = "",
  description = "",
  keywords = "",
}) => {
  const router = useRouter();
  const theme = useTheme();
  const classes = useStyles();
  const [viewOrderDetail, setViewOrderDetail] = useState(false);
  const [bankTransferInfo, setBankTransferInfo] = useState({});
  const [bankTransferOptions, setBankTransferOptions] = useState([]);
  const toggleDrawerOrderDetail = (open) => () => {
    setViewOrderDetail(open);
  };

  const [airlines, setAirlines] = useState();
  const actionsGeneralInformation = async () => {
    const { data } = await getGeneralInformation();
    setAirlines(data?.data?.airlines);
  };

  const actionGetBankTransfer = async () => {
    const { data } = await getBankTransferInfo(orderDetail?.bankTransferCode);
    if (data.code == 200) {
      setBankTransferInfo(data.data?.transferInfo);
      setBankTransferOptions(data.data?.transferOptions);
    }
  };

  useEffect(() => {
    actionsGeneralInformation();
    if (
      !isEmpty(orderDetail?.bankTransferCode) &&
      (orderDetail?.paymentMethodCode ===
        flightPaymentMethodCode.BANK_TRANSFER ||
        orderDetail?.paymentMethodCode ===
          flightPaymentMethodCode.BANK_TRANSFER_2)
    ) {
      actionGetBankTransfer();
    }

    //Clear book info
    const ISSERVER = typeof window === "undefined";
    let lastBookData = {};
    let customerData = {};
    if (!ISSERVER) {
      lastBookData = JSON.parse(localStorage.getItem(`LAST_REQUEST`));
    }

    if (lastBookData?.id) {
      customerData = JSON.parse(
        localStorage.getItem(`CUSTOMER_INFO_${lastBookData.id}`)
      );
      if (!isEmpty(customerData)) {
        resetDataAfterBookTicket(
          customerData,
          `CUSTOMER_INFO_${lastBookData.id}`,
          lastBookData.totalGuest
        );
      }
    }
  }, []);

  return (
    <Layout title={title} description={description} keywords={keywords}>
      <Box className={classes.wraper}>
        <Box className={classes.boxInfo}>
          <Box className={classes.boxStatus} pt={3} pr={12 / 8} pl={12 / 8}>
            {paymentType === paymentTypes.FAIL ? (
              <>
                <IconBookingFlightFail />
                <Typography
                  variant="subtitle1"
                  style={{ marginTop: 12, marginBottom: 8 }}
                >
                  {listString.IDS_TEXT_FLIGHT_BOOKING_FAIL}
                </Typography>
                <Typography variant="caption">
                  {listString.IDS_TEXT_FLIGHT_BOOKING_RESULT_DESC_FAIL}
                </Typography>
              </>
            ) : paymentType === paymentTypes.SUCCESS && !fromBankTransfer ? (
              <>
                <IconBookingFlightSuccess />
                <Typography
                  variant="subtitle1"
                  style={{ marginTop: 12, marginBottom: 8 }}
                >
                  {listString.IDS_TEXT_FLIGHT_BOOKING_SUCCESS}
                </Typography>
                <Typography variant="caption">
                  {listString.IDS_TEXT_FLIGHT_BOOKING_RESULT_DESC_SUCCESS}
                </Typography>
              </>
            ) : paymentType === paymentTypes.SUCCESS && fromBankTransfer ? (
              <>
                <IconBookingFlightSuccess />
                <Typography
                  variant="subtitle1"
                  style={{ marginTop: 12, marginBottom: 8 }}
                >
                  {listString.IDS_TEXT_FLIGHT_BOOKING_SUCCESS}
                </Typography>
                <Typography variant="caption">
                  {`${listString.IDS_TEXT_FLIGHT_BOOKING_BANK_TRANSFER_1} `}
                  <Typography variant="caption" style={{ fontWeight: 600 }}>
                    {orderDetail?.mainContact?.email}.
                    <Typography variant="caption">
                      {listString.IDS_TEXT_FLIGHT_BOOKING_BANK_TRANSFER_2}
                      <Typography variant="caption" style={{ fontWeight: 600 }}>
                        {!isEmpty(bankTransferInfo?.expiredTime)
                          ? ` ${moment(
                              bankTransferInfo?.expiredTime,
                              C_DATE_TIME_FORMAT
                            ).format(HOUR_MINUTE)}, `
                          : " "}
                        {!isEmpty(bankTransferInfo?.expiredTime)
                          ? moment(
                              bankTransferInfo.expiredTime,
                              C_DATE_TIME_FORMAT
                            )
                              .format("ddd")
                              .replace("T2", "Thứ hai")
                              .replace("T3", "Thứ ba")
                              .replace("T4", "Thứ tư")
                              .replace("T5", "Thứ năm")
                              .replace("T6", "Thứ sáu")
                              .replace("T7", "Thứ bảy")
                              .replace("CN", "Chủ Nhật")
                          : "-"}
                        {","}
                        {!isEmpty(bankTransferInfo?.expiredTime)
                          ? ` ngày ${moment(
                              bankTransferInfo?.expiredTime,
                              C_DATE_TIME_FORMAT
                            ).format(DATE_FORMAT)}`
                          : ""}
                      </Typography>
                    </Typography>
                  </Typography>
                </Typography>
              </>
            ) : (
              <>
                <IconBookingFlightSuccess />
                <Typography
                  variant="subtitle1"
                  style={{ marginTop: 12, marginBottom: 8 }}
                >
                  {listString.IDS_TEXT_FLIGHT_BOOKING_HOLD_SUCCESS}
                </Typography>
                <Box className={classes.boxHolder}>
                  <Typography variant="caption">{`${
                    listString.IDS_TEXT_FLIGHT_BOOKING_HOLD_TIME
                  } ${getExpiredTimeMillisecond(
                    orderDetail?.expiredTime || 0
                  )}`}</Typography>
                </Box>
                <Typography variant="caption">
                  {listString.IDS_TEXT_FLIGHT_BOOKING_RESULT_DESC_HOLD_SUCCESS}
                </Typography>
              </>
            )}
            <ButtonComponent
              fontWeight="normal"
              color={theme.palette.blue.blueLight8}
              typeButton="text"
              width="fit-content"
              handleClick={toggleDrawerOrderDetail(true)}
            >
              <Typography variant="caption" style={{ marginRight: 8 }}>
                {listString.IDS_TEXT_ORDER_DETAIL}
              </Typography>
              <IconChevronRight
                className="svgFillAll"
                stroke={theme.palette.blue.blueLight8}
              />
            </ButtonComponent>
            <OrderDetail
              open={viewOrderDetail}
              handleClose={toggleDrawerOrderDetail}
              orderDetail={orderDetail}
              paymentType={paymentType}
              airlines={airlines}
            />
          </Box>
          {!isEmpty(bankTransferInfo) ? (
            <Box padding="12px" bgcolor={theme.palette.gray.grayLight26}>
              <FlightBankTransfer
                bankTransferInfo={bankTransferInfo}
                bankTransferOptions={bankTransferOptions}
              />
            </Box>
          ) : (
            <Divider className={classes.divider} />
          )}

          <Box className={classes.boxTicket}>
            {orderDetail.isTwoWay ? (
              <Box className={classes.bodyBoxTicket}>
                <Box className={classes.bodyTicket}>
                  <Box className={classes.titleTicket}>
                    {listString.IDS_MT_TEXT_DEPARTURE.toUpperCase()}
                  </Box>
                  <Box display="flex" flexDirection="column" mt={1} mb={12 / 8}>
                    <Grid container>
                      <Grid item xl={5} lg={5} sm={5} md={5} xs={5}>
                        <Box
                          display="flex"
                          justifyContent="flex-start"
                          textAlign="start"
                        >
                          <Typography variant="subtitle1">{`${orderDetail?.outbound.departureCity} (${orderDetail?.outbound.fromAirport})`}</Typography>
                        </Box>
                      </Grid>
                      <Grid item xl={2} lg={2} sm={2} md={2} xs={2}>
                        <Box
                          display="flex"
                          alignItems="center"
                          justifyContent="center"
                        >
                          <IconArrowRight />
                        </Box>
                      </Grid>
                      <Grid item xl={5} lg={5} sm={5} md={5} xs={5}>
                        <Box
                          display="flex"
                          justifyContent="flex-end"
                          textAlign="end"
                        >
                          <Typography variant="subtitle1">{`${orderDetail?.outbound.arrivalCity} (${orderDetail?.outbound.toAirport})`}</Typography>
                        </Box>
                      </Grid>
                    </Grid>
                    <Box
                      display="flex"
                      color={theme.palette.gray.grayDark8}
                      alignItems="center"
                    >
                      <Typography variant="body2">
                        {`${orderDetail?.outbound.departureTime}, ${moment(
                          orderDetail?.outbound.departureDate,
                          DATE_FORMAT_BACK_END
                        ).format("DD/MM")}`}
                      </Typography>
                      <IconDot className={classes.iconDot} />
                      <Typography variant="body2">
                        {`${orderDetail?.outbound.arrivalTime}, ${moment(
                          orderDetail?.outbound.arrivalDate,
                          DATE_FORMAT_BACK_END
                        ).format("DD/MM")}`}
                      </Typography>
                    </Box>
                  </Box>
                </Box>
                <Box className={classes.bodyTicket}>
                  <Box className={classes.titleTicket}>
                    {listString.IDS_MT_TEXT_RETURN.toUpperCase()}
                  </Box>
                  <Box display="flex" flexDirection="column" mt={1}>
                    <Grid container>
                      <Grid item xl={5} lg={5} sm={5} md={5} xs={5}>
                        <Box
                          display="flex"
                          justifyContent="flex-start"
                          textAlign="start"
                        >
                          <Typography variant="subtitle1">{`${orderDetail?.inbound.departureCity} (${orderDetail?.inbound.fromAirport})`}</Typography>
                        </Box>
                      </Grid>
                      <Grid item xl={2} lg={2} sm={2} md={2} xs={2}>
                        <Box
                          display="flex"
                          alignItems="center"
                          justifyContent="center"
                        >
                          <IconArrowRight />
                        </Box>
                      </Grid>
                      <Grid item xl={5} lg={5} sm={5} md={5} xs={5}>
                        <Box
                          display="flex"
                          justifyContent="flex-end"
                          textAlign="end"
                        >
                          <Typography variant="subtitle1">{`${orderDetail?.inbound.arrivalCity} (${orderDetail?.inbound.toAirport})`}</Typography>
                        </Box>
                      </Grid>
                    </Grid>
                    <Box
                      display="flex"
                      color={theme.palette.gray.grayDark8}
                      alignItems="center"
                    >
                      <Typography variant="body2">
                        {`${orderDetail?.inbound.departureTime}, ${moment(
                          orderDetail?.inbound.departureDate,
                          DATE_FORMAT_BACK_END
                        ).format("DD/MM")}`}
                      </Typography>
                      <IconDot className={classes.iconDot} />
                      <Typography variant="body2">
                        {`${orderDetail?.inbound.arrivalTime}, ${moment(
                          orderDetail?.inbound.arrivalDate,
                          DATE_FORMAT_BACK_END
                        ).format("DD/MM")}`}
                      </Typography>
                    </Box>
                  </Box>
                </Box>
              </Box>
            ) : (
              <FlightTicketDetailPayment
                ticket={orderDetail?.outbound}
                airlines={airlines}
              />
            )}
          </Box>
          <Box className={classes.dotted}></Box>
        </Box>
        <Grid container className={classes.boxCodeInfo}>
          <Grid
            item
            xs={
              (!isEmpty(orderDetail.outboundPnrCode) ||
                !isEmpty(orderDetail.inboundPnrCode)) &&
              paymentType !== paymentTypes.FAIL
                ? 6
                : 12
            }
          >
            <Typography variant="caption">
              {listString.IDS_TEXT_FLIGHT_ORDER_CODE}
            </Typography>
            <Typography className={classes.codeLeft}>
              {orderDetail.orderCode}
            </Typography>
          </Grid>
          {paymentType !== paymentTypes.FAIL && (
            <>
              <Grid item xs={6}>
                {orderDetail.isTwoWay ? (
                  <Box className={classes.orderCode}>
                    {orderDetail.outboundPnrCode && (
                      <>
                        <Typography variant="caption">
                          {`${listString.IDS_TEXT_FLIGHT_BOOK_CODE_OUTBOUND} `}
                        </Typography>
                        <Typography
                          className={classes.codeRight}
                          style={{ marginBottom: 12 }}
                        >
                          {orderDetail.outboundPnrCode}
                        </Typography>
                      </>
                    )}
                    {orderDetail.inboundPnrCode && (
                      <>
                        <Typography variant="caption">
                          {`${listString.IDS_TEXT_FLIGHT_BOOK_CODE_INBOUND} `}
                        </Typography>
                        <Typography className={classes.codeRight}>
                          {orderDetail.inboundPnrCode}
                        </Typography>
                      </>
                    )}
                  </Box>
                ) : !orderDetail.isTwoWay && orderDetail.outboundPnrCode ? (
                  <Box className={classes.orderCode}>
                    <Typography variant="caption">
                      {`${listString.IDS_TEXT_FLIGHT_BOOK_CODE} `}
                    </Typography>
                    <Typography className={classes.codeRight}>
                      {orderDetail.outboundPnrCode}
                    </Typography>
                  </Box>
                ) : null}
              </Grid>
            </>
          )}
        </Grid>
      </Box>
      <Box className={classes.buttonBackHome}>
        <ButtonComponent
          backgroundColor={theme.palette.secondary.main}
          height={48}
          borderRadius={8}
          handleClick={() => {
            router.push({
              pathname: routeStatic.HOME.href,
            });
          }}
        >
          <Typography variant="subtitle1">
            {listString.IDS_MT_TEXT_NOT_FOUND_403_DES_3}
          </Typography>
        </ButtonComponent>
      </Box>
    </Layout>
  );
};

PaymentResult.propTypes = {
  orderDetail: PropTypes.object,
};
export default PaymentResult;
