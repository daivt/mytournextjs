import { Box, Divider, makeStyles, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { IconCopy } from "@public/icons";
import Image from "@src/image/Image";
import { listString } from "@utils/constants";
import React from "react";

const useStyles = makeStyles((theme) => ({
  divider: {
    height: 1,
    color: theme.palette.gray.grayLight22,
  },
  copy: {
    display: "flex",
    justifyContent: "flex-end",
    color: theme.palette.blue.blueLight8,
    alginItems: "center",
    fontSize: "14px",
    lineHeight: "17px",
  },
  iconBank: {
    width: "100%",
    height: "100%",
    objectFit: "contain",
  },
  bank: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
  },
  bankItem: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.gray.grayLight23}`,
    height: 24,
    width: 48,
    backgroundColor: theme.palette.white.main,
  },
  wrapBank: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    whiteSpace: "nowrap",
    fontSize: 14,
    lineHeight: "17px",
    marginTop: 12,
  },
  bankText: {
    fontWeight: 600,
    whiteSpace: "nowrap",
    overflow: "hidden !important",
    textOverflow: "ellipsis",
    width: 150,
  },
  infoBank: {
    borderBottom: `1px solid ${theme.palette.gray.grayLight23}`,
    marginBottom: 12,
  },
}));

const FlightBankTransfer = ({
  bankTransferInfo = {},
  bankTransferOptions = [],
}) => {
  const theme = useTheme();
  const classes = useStyles();

  return (
    <>
      <Typography variant="subtitle1">
        {listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER}
      </Typography>
      {bankTransferOptions &&
        bankTransferOptions.map((el, index) => (
          <Box key={index}>
            <Box className={classes.wrapBank}>
              <Typography variant="caption" className={classes.bank}>
                {listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_BANK}
              </Typography>
              <Box display="flex" alignItems="center">
                <Box component="span" className={classes.bankText}>
                  {el?.bankName}
                </Box>
                <Box className={classes.bankItem}>
                  <Image srcImage={el?.bankLogo} className={classes.iconBank} />
                </Box>
              </Box>
            </Box>
            <Box
              display="flex"
              justifyContent="space-between"
              mt={12 / 8}
              mb={12 / 8}
            >
              <Typography variant="caption" style={{ lineHeight: "16px" }}>
                {listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_NUMBER_ACCOUNT}
              </Typography>
              <Box display="flex" flexDirection="column">
                <Typography variant="subtitle2">{el?.accountNumber}</Typography>
                <Box
                  className={classes.copy}
                  onClick={() => {
                    navigator.clipboard.writeText(el?.accountNumber);
                  }}
                >
                  <IconCopy />
                  {listString.IDS_MT_TEXT_INFO_COPPY}
                </Box>
              </Box>
            </Box>
          </Box>
        ))}
      <Divider className={classes.divider} />
      <Box display="flex" justifyContent="space-between" mt={12 / 8}>
        <Typography variant="caption" style={{ lineHeight: "16px" }}>
          {listString.IDS_MT_TEXT_AMOUNT_MONEY}
        </Typography>
        <Box display="flex" flexDirection="column">
          <Typography variant="subtitle2">
            {`${bankTransferInfo?.totalAmount.formatMoney()}đ`}
          </Typography>
          <Box
            className={classes.copy}
            onClick={() => {
              navigator.clipboard.writeText(
                `${bankTransferInfo?.totalAmount.formatMoney()}đ`
              );
            }}
          >
            <IconCopy />
            {listString.IDS_MT_TEXT_INFO_COPPY}
          </Box>
        </Box>
      </Box>
      <Box display="flex" justifyContent="space-between" mt={12 / 8}>
        <Typography variant="caption" style={{ lineHeight: "16px" }}>
          {listString.IDS_MT_TEXT_CONTENT_TRANSFER}
        </Typography>
        <Box display="flex" flexDirection="column">
          <Typography
            variant="subtitle2"
            style={{ textAlign: "end", whiteSpace: "break-spaces" }}
          >
            {bankTransferInfo?.message}
          </Typography>
          <Box
            className={classes.copy}
            onClick={() => {
              navigator.clipboard.writeText(bankTransferInfo?.message);
            }}
          >
            <IconCopy />
            {listString.IDS_MT_TEXT_INFO_COPPY}
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default FlightBankTransfer;
