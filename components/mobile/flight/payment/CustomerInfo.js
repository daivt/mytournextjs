import CustomerInput from "@components/mobile/flight/payment/CustomerInput";
import { Box, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import { IconArrowDownToggle } from "@public/icons";
import AccordionCustom from "@src/accordion/AccordionCustom";
import AccordionDetailsCustom from "@src/accordion/AccordionDetailsCustom";
import AccordionSummaryCustom from "@src/accordion/AccordionSummaryCustom";
import { GENDER_LIST, listString } from "@utils/constants";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  boxTitle: {
    color: theme.palette.black.black3,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  boxDesc: {
    color: theme.palette.blue.blueLight8,
  },
  typeCustomer: {
    backgroundColor: theme.palette.gray.grayLight22,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    width: "80%",
  },
  boxInfo: {
    marginBottom: 24,
  },
}));

const CustomerInfo = (props) => {
  const classes = useStyles();
  const theme = useTheme();

  const [expanded, setExpanded] = useState("panel1");

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };
  const { listInputTemp, customError } = props;

  return (
    <>
      <AccordionCustom
        expanded={expanded === "panel1"}
        onChange={handleChange("panel1")}
      >
        <AccordionSummaryCustom expandIcon={<IconArrowDownToggle />}>
          <Box display="flex">
            <Typography className={classes.leftTitle} variant="subtitle1">
              {listString.IDS_MT_TEXT_INFO_CUSTOMER_FLIGHT}
            </Typography>
          </Box>
        </AccordionSummaryCustom>
        <AccordionDetailsCustom>
          <Box display="flex" flexDirection="column">
            <Box
              className={classes.note}
              borderRadius={8}
              bgcolor={theme.palette.blue.blueLight9}
              p={1}
            >
              <Typography className={classes.boxDesc} variant="caption">
                {listString.IDS_MT_TEXT_INFO_CUSTOMER_FLIGHT_NOTE_NO_ACCENT}
                &nbsp;{listString.IDS_MT_TEXT_INFO_CUSTOMER_FLIGHT_NOTE_NAME}
              </Typography>
            </Box>
            <Box mt={2}>
              <CustomerInput
                listInputTemp={listInputTemp}
                listGender={GENDER_LIST}
                customError={customError}
              />
            </Box>
          </Box>
        </AccordionDetailsCustom>
      </AccordionCustom>
    </>
  );
};

export default CustomerInfo;
