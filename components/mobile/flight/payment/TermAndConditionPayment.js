import { Box, makeStyles, Typography } from "@material-ui/core";
import Link from "@src/link/Link";
import { listString, BASE_URL_CONDITION_MYTOUR } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  wraper: {
    background: theme.palette.gray.grayLight22,
    textAlign: "center",
    padding: "16px 45px 90px 45px",
  },
  wrapText: {
    color: theme.palette.gray.grayDark8,
  },
  linkTerm: {
    color: theme.palette.blue.blueLight8,
    padding: "0 4px",
  },
}));
const TermAndConditionPayment = () => {
  const classes = useStyles();
  return (
    <Box className={classes.wraper}>
      <Typography component="span" variant="body2" className={classes.wrapText}>
        {listString.IDS_MT_TEXT_FLIGHT_TERM_PAYMENT}
      </Typography>
      <Link href={BASE_URL_CONDITION_MYTOUR} className={classes.linkTerm}>
        <Typography component="span" variant="body2">
          {listString.IDS_MT_TEXT_PAYMENT_METHOD_3_DES_6}
        </Typography>
      </Link>
      <Typography component="span" variant="body2" className={classes.wrapText}>
        {listString.IDS_MT_TEXT_PAYMENT_METHOD_3_DES_7}.
      </Typography>
    </Box>
  );
};

export default TermAndConditionPayment;
