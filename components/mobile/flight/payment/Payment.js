import {
  getGeneralInformation,
  getTicketDetail,
  getVoucherListPayment,
  validateVoucherCode,
} from "@api/flight";
import FlightTicketDetail from "@components/common/card/flight/FlightTicketDetail";
import DialogShowMessage from "@components/common/modal/flight/DialogShowMessage";
import FlightInfoDetailBoxModal from "@components/common/modal/flight/FlightInfoDetailBoxModal";
import PriceDetailsTwoWayModal from "@components/common/modal/flight/PriceDetailsTwoWayModal";
import Baggage from "@components/mobile/flight/payment/Baggage";
import ContactInfo from "@components/mobile/flight/payment/ContactInfo";
import CustomerInfo from "@components/mobile/flight/payment/CustomerInfo";
import InsuranceBox from "@components/mobile/flight/payment/InsuranceBox";
import Invoice from "@components/mobile/flight/payment/Invoice";
import TotalPriceStepTwoBox from "@components/mobile/flight/payment/TotalPriceStepTwoBox";
import {
  Box,
  Button,
  Divider,
  Grid,
  IconButton,
  Typography,
} from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import { IconArrowLeft, IconChevronRight } from "@public/icons";
import snackbarSetting from "@src/alert/Alert";
import utilStyles from "@styles/utilStyles";
import { CUSTOMER_TYPE, listString, STEP_NUMBER } from "@utils/constants";
import {
  formatDateOfBirth,
  getContactInfoTemp,
  getInfoGuestTemp,
  isEmpty,
} from "@utils/helpers";
import { DATE_FORMAT_BACK_END, DATE_TIME_FORMAT } from "@utils/moment";
import { Form, Formik } from "formik";
import moment from "moment";
import "moment/locale/vi";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import React, { useEffect, useState } from "react";
import * as yup from "yup";

const useStyle = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    width: "100vw",
  },
  divider: {
    height: 6,
    backgroundColor: theme.palette.gray.grayLight22,
  },
  divider2: {
    backgroundColor: theme.palette.gray.grayLight22,
    width: "84vw",
  },
  divider3: {
    backgroundColor: theme.palette.gray.grayLight22,
  },
  headerBox: {
    borderTop: `solid 1px ${theme.palette.gray.grayLight22}`,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  bodyBox: {
    padding: 16,
    display: "flex",
    flexDirection: "column",
    background: theme.palette.gray.grayLight23,
  },
  button: {
    width: "100%",
    height: 48,
    color: theme.palette.primary.main,
    backgroundColor: theme.palette.white.main,
    borderRadius: "0px 0px 8px 8px",
    "& i": {
      paddingRight: 5,
    },
    "&:hover": {
      backgroundColor: theme.palette.white.main,
    },
  },
  footer: {
    position: "fixed",
    backgroundColor: theme.palette.white.main,
    zIndex: 20,
    bottom: 0,
    width: "100vw",
  },
}));
const MobileContent = () => {
  const classes = useStyle();
  const theme = useTheme();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [visibleDrawerType, setVisibleDrawerType] = useState(false);
  const [detailTicketOutBound, setDetailTicketOutBound] = useState([]);
  const [totalMoney, setTotalMoney] = useState(0);
  const [totalSubMoney, setTotalSubMoney] = useState(0);
  const [voucherUsing, setVoucherUsing] = useState({});
  const [voucherSuggest, setVoucherSuggest] = useState({});
  const [discountMoneyVoucher, setDiscountMoneyVoucher] = useState(0);
  const [detailTicketInBound, setDetailTicketInBound] = useState(null);
  const [generalInfomation, setGeneralInformation] = useState();
  const [invoiceInfo, setInvoiceInfo] = useState({});
  const [bagInfo, setBagInfo] = useState({
    numGuest: 0,
    weight: 0,
    money: 0,
  });
  const [buyInsurance, setBuyInsurance] = React.useState("-1");
  const [baggageCustomer, setBaggageCustomer] = React.useState({});
  const [totalPrice, setTotalPrice] = React.useState(0);
  const [detailTicketStatus, setDetailTicketStatus] = React.useState(false);

  const [detailPriceModal, setDetailPriceModal] = useState(false);
  const [messageAlert, setMessageAlert] = useState("");
  const [showMessageAlert, setShowMessageAlert] = useState(false);
  const [customError, setCustomError] = useState({});

  const toggleDrawerAlert = (open) => () => {
    if (!open) {
      setMessageAlert("");
    }
    setShowMessageAlert(open);
  };
  const toggleDrawer = (open) => () => {
    setDetailPriceModal(open);
  };

  const classesUtils = utilStyles();
  const router = useRouter();
  const queryData = router.query;
  const fromDate = queryData.departureDate;
  const toDate = queryData?.returnDate || queryData.departureDate;
  const paramsInsurance = {
    fromAirportCode: queryData.origin_code,
    toAirportCode: queryData.destination_code,
    isOneWay: queryData.ticketInBoundId ? false : true,
    fromDate: moment(fromDate, DATE_FORMAT_BACK_END).format(DATE_TIME_FORMAT),
    toDate: moment(toDate, DATE_FORMAT_BACK_END).format(DATE_TIME_FORMAT),
    numGuest:
      parseInt(queryData.adultCount) +
      parseInt(queryData.childCount) +
      parseInt(queryData.infantCount),
  };
  const actionsTicketDetail = async () => {
    let moneyTicket = 0;
    let moneyTicketPromo = 0;
    if (queryData?.ticketOutBoundId) {
      const params = {
        agencyId: queryData.agencyOutBoundId,
        requestId: queryData.requestId,
        ticketId: queryData.ticketOutBoundId,
      };
      const { data } = await getTicketDetail(params);

      if (data?.code === 400) {
        data?.message &&
          enqueueSnackbar(
            data?.message,
            snackbarSetting((key) => closeSnackbar(key), { color: "error" })
          );
        router.push({
          pathname: "/ve-may-bay/",
          query: {},
        });
      }

      setDetailTicketOutBound(data?.data);
      moneyTicket += parseInt(data?.data?.ticket?.totalPrice);
      if (data?.data?.ticket?.outbound?.ticketdetail?.discount) {
        moneyTicketPromo +=
          moneyTicket +
          parseInt(data?.data?.ticket?.outbound?.ticketdetail?.discount);
      }
      if (!queryData?.ticketInBoundId) {
        setDetailTicketStatus(true);
      }
    }
    if (queryData?.ticketInBoundId) {
      const params = {
        agencyId: queryData.agencyInBoundId,
        requestId: queryData.requestId,
        ticketId: queryData.ticketInBoundId,
      };
      const { data } = await getTicketDetail(params);
      setDetailTicketInBound(data?.data);
      moneyTicket += parseInt(data?.data?.ticket?.totalPrice);
      if (data?.data?.ticket?.outbound?.ticketdetail?.discount) {
        moneyTicketPromo +=
          moneyTicket +
          parseInt(data?.data?.ticket?.outbound?.ticketdetail?.discount);
      }
      setDetailTicketStatus(true);
    }
    if (moneyTicket) setTotalMoney(moneyTicket);
    if (moneyTicketPromo) setTotalSubMoney(moneyTicketPromo);
  };

  const actionsGeneralInformation = async () => {
    const { data } = await getGeneralInformation();
    setGeneralInformation(data?.data);
  };

  const actionsVoucher = async (customerInfoValues) => {
    let firstVoucher = {};
    if (isEmpty(voucherSuggest)) {
      const dataPost = {
        info: { productType: "flight" },
        isUsed: false,
        page: 1,
        module: "flight",
        size: 2,
        term: "",
      };
      const { data } = await getVoucherListPayment(dataPost);
      firstVoucher = data?.data?.list[0];
      if (firstVoucher) {
        setVoucherUsing({
          id: firstVoucher?.id,
          code: firstVoucher?.codeDetail,
          description: firstVoucher?.rewardProgram?.title,
          expired: firstVoucher?.responseData?.validTo,
        });
        setVoucherSuggest(firstVoucher);
      }
    }
    if (firstVoucher?.codeDetail || !isEmpty(voucherSuggest)) {
      let tempGuests = [];
      let tempContact = {};
      if (customerInfoValues) {
        tempGuests = getInfoGuestTemp(customerInfoValues, router, totalGuest);
        tempContact = getContactInfoTemp(customerInfoValues);
      }

      const paramCheck = {
        code: firstVoucher?.codeDetail || voucherSuggest?.codeDetail,
        data: {
          contact: tempContact,
          guests: tempGuests,
          insuranceContact: customerInfoValues?.checkValidContactMail
            ? tempContact
            : null,
          point: 0,
          promoCode: "",
          tickets: {
            inbound: queryData?.ticketInBoundId
              ? {
                  agencyId: queryData.agencyInBoundId,
                  requestId: queryData.requestId,
                  ticketId: queryData.ticketInBoundId,
                }
              : null,
            outbound: {
              agencyId: queryData.agencyOutBoundId,
              requestId: queryData.requestId,
              ticketId: queryData.ticketOutBoundId,
            },
          },
        },
        module: "flight",
        originPoint: 0,
      };

      const { data } = await validateVoucherCode(paramCheck);
      const dataValidVoucher = data;

      if (dataValidVoucher.code === 200) {
        setDiscountMoneyVoucher(dataValidVoucher?.data?.discount);
      } else {
        setVoucherUsing({});
        setDiscountMoneyVoucher(0);
      }
    }
  };

  let listInputTemp = [];
  let totalGuest = 0;
  if (queryData.adultCount) {
    let numAdults = parseInt(queryData.adultCount);
    let numChildren = parseInt(queryData.childCount);
    let numInfants = parseInt(queryData.infantCount);
    totalGuest = numAdults + numChildren + numInfants;
    if (numAdults) {
      for (let index = 0; index < numAdults; index++) {
        listInputTemp = [
          ...listInputTemp,
          {
            gender: "",
            name: "",
            outboundBaggageId: undefined,
            inboundBaggageId: undefined,
            insuranceInfo: null,
            passport: "",
            passportExpiredDate: "",
            passportCountryId: null,
            nationalityCountryId: null,
            dob: "",
            type: CUSTOMER_TYPE.ADULT,
            isMultipleInsurance: true,
          },
        ];
      }
    }
    if (numChildren) {
      for (let index = 0; index < numChildren; index++) {
        listInputTemp = [
          ...listInputTemp,
          {
            gender: "",
            name: "",
            outboundBaggageId: undefined,
            inboundBaggageId: undefined,
            insuranceInfo: null,
            passport: "",
            passportExpiredDate: "",
            passportCountryId: null,
            nationalityCountryId: null,
            dob: "",
            type: CUSTOMER_TYPE.CHILDREN,
          },
        ];
      }
    }
    if (numInfants) {
      for (let index = 0; index < numInfants; index++) {
        listInputTemp = [
          ...listInputTemp,
          {
            gender: "",
            name: "",
            outboundBaggageId: undefined,
            inboundBaggageId: undefined,
            insuranceInfo: null,
            passport: "",
            passportExpiredDate: "",
            passportCountryId: null,
            nationalityCountryId: null,
            dob: "",
            type: CUSTOMER_TYPE.BABY,
          },
        ];
      }
    }
  }
  let customerInfo = null;

  const generateField = () => {
    const ISSERVER = typeof window === "undefined";

    if (!ISSERVER) {
      customerInfo = JSON.parse(
        localStorage.getItem(`CUSTOMER_INFO_${queryData.requestId}`)
      );
    }

    let result = {};
    listInputTemp.forEach((el, index) => {
      result = {
        ...result,
        genderContact: "M",
        nameContact: "",
        phoneNumberContact: "",
        emailContact: "",
        [`name_${index}`]: customerInfo ? customerInfo[`name_${index}`] : "",
        [`gender_${index}`]: customerInfo
          ? customerInfo[`gender_${index}`]
          : "M",
        [`outbound_baggage_${index}`]:
          customerInfo && customerInfo[`outbound_baggage_${index}`]
            ? customerInfo[`outbound_baggage_${index}`]
            : undefined,
        [`inbound_baggage_${index}`]:
          customerInfo && customerInfo[`inbound_baggage_${index}`]
            ? customerInfo[`inbound_baggage_${index}`]
            : undefined,
        [`passport_${index}`]: "",
        [`passport_expired_${index}`]: "",
        [`passport_residence_${index}`]: "",
        [`passport_country_${index}`]: "",
        [`insurance_${index}`]: "",
        [`dob_${index}`]: customerInfo ? customerInfo[`dob_${index}`] : "",
        checkValidContactMail: customerInfo
          ? customerInfo.checkValidContactMail
          : false,
        checkedSame: customerInfo ? customerInfo.checkedSame : false,
      };
    });

    return result;
  };
  let arrValidate = {};
  listInputTemp.forEach((el, index) => {
    arrValidate = {
      ...arrValidate,
      [`name_${index}`]: yup
        .string()
        .trim()
        .test({
          name: `name_${index}`,
          message: listString.IDS_MT_TEXT_VALIDATE_FULLNAME,
          test: (value) => {
            return value ? value.split(" ").length > 1 : true;
          },
        })
        .required(listString.IDS_MT_TEXT_PLEASE_ENTER_NAME)
        .max(50, listString.IDS_MT_TEXT_VALIDATE_NAME_MAX),
    };
    arrValidate = {
      ...arrValidate,
      [`gender_${index}`]: yup
        .string()
        .trim()
        .required(listString.IDS_MT_TEXT_PLEASE_ENTER_GENDER),
    };

    if (el.type !== CUSTOMER_TYPE.ADULT) {
      arrValidate = {
        ...arrValidate,
        [`dob_${index}`]: yup
          .string()
          .trim()
          .required(listString.IDS_MT_TEXT_PLEASE_ENTER_DOB),
      };
    }
  });
  arrValidate = {
    ...arrValidate,
    nameContact: yup
      .string()
      .trim()
      .test({
        name: "nameContact",
        message: listString.IDS_MT_TEXT_VALIDATE_FULLNAME,
        test: (value) => {
          return value ? value.split(" ").length > 1 : true;
        },
      })
      .max(50, listString.IDS_MT_TEXT_VALIDATE_NAME_MAX)
      .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    phoneNumberContact: yup
      .string()
      .trim()
      .min(9, listString.IDS_MT_TEXT_INVALID_PHONE_NUMBER)
      .max(12, listString.IDS_MT_TEXT_INVALID_PHONE_NUMBER)
      .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    emailContact: yup
      .string()
      .trim()
      .email(listString.IDS_MT_TEXT_INVALID_FORMAT_EMAIL)
      .max(50, listString.IDS_MT_TEXT_VALIDATE_EMAIL_MAX)
      .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
  };
  const storeSchema = yup.object().shape(arrValidate);

  useEffect(() => {
    actionsTicketDetail();
    actionsGeneralInformation();

    if (!isEmpty(customerInfo) && customerInfo?.invoiceInfo) {
      setInvoiceInfo(customerInfo.invoiceInfo);
    }
  }, []);

  useEffect(() => {
    if (!isEmpty(customerInfo)) {
      if (!isEmpty(baggageCustomer)) {
        Object.keys(baggageCustomer).forEach((element) => {
          customerInfo[element] = baggageCustomer[element];
        });
      }
      if (buyInsurance !== "-1") {
        customerInfo.checkValidContactMail = buyInsurance;
      }
      actionsVoucher(customerInfo);
    }
  }, [baggageCustomer, buyInsurance]);

  const handleInvoiceInfo = (values) => {
    setInvoiceInfo(
      isEmpty(values)
        ? {}
        : {
            companyAddress: values?.companyAddress,
            companyName: values?.companyName,
            customerName: values?.recipientName,
            note: values?.note || "",
            recipientAddress: values?.recipientAddress,
            recipientEmail: values?.recipientEmail,
            recipientName: values?.recipientName,
            taxIdNumber: values?.taxIdNumber,
          }
    );
  };
  return (
    <>
      <Box className={classes.container}>
        <Grid container className={classes.headerBox}>
          <Grid item lg={2} md={2} sm={2} xs={2}>
            <IconButton
              onClick={() => {
                router.back();
              }}
              aria-label="close"
            >
              <IconArrowLeft />
            </IconButton>
          </Grid>
          <Grid item lg={8} md={8} sm={8} xs={8}>
            <Box display="flex" justifyContent="center">
              <Typography variant="subtitle1">Thông tin hành khách</Typography>
            </Box>
          </Grid>
        </Grid>
        <div className={classes.bodyBox}>
          <Box className={classesUtils.scrollViewHorizontal}>
            <Box>
              <FlightTicketDetail
                ticket={detailTicketOutBound?.ticket?.outbound}
                data={detailTicketOutBound}
                isTransit={
                  !!detailTicketOutBound?.ticket?.outbound?.transitTickets
                }
                airlineInfo={detailTicketOutBound?.agency}
                title=""
                isSimple={true}
                isSeat={true}
                isSlider
                title2Ways={detailTicketInBound ? "CHIỀU ĐI" : null}
              />
              <Divider className={classes.divider2} />
              <Button
                type="submit"
                className={classes.button}
                variant="contained"
                disableElevation
                onClick={() => {
                  setVisibleDrawerType(true);
                }}
              >
                Xem chi tiết chuyến bay
                <IconChevronRight
                  className="svgFillAll"
                  style={{
                    stroke: theme.palette.primary.main,
                    marginLeft: 8,
                  }}
                />
              </Button>
            </Box>
            {detailTicketInBound && (
              <Box ml={12 / 8}>
                <FlightTicketDetail
                  ticket={detailTicketInBound?.ticket?.outbound}
                  data={detailTicketInBound}
                  isTransit={
                    !!detailTicketInBound?.ticket?.outbound?.transitTickets
                  }
                  airlineInfo={detailTicketInBound?.agency}
                  title=""
                  isSimple={true}
                  isSeat={true}
                  isSlider
                  title2Ways={detailTicketInBound ? "CHIỀU VỀ" : null}
                />
                <Divider className={classes.divider2} />
                <Button
                  type="submit"
                  className={classes.button}
                  variant="contained"
                  disableElevation
                  onClick={() => {
                    setVisibleDrawerType(true);
                  }}
                >
                  Xem chi tiết chuyến bay
                  <IconChevronRight
                    className="svgFillAll"
                    style={{
                      stroke: theme.palette.primary.main,
                      marginLeft: 8,
                    }}
                  />
                </Button>
              </Box>
            )}
          </Box>
        </div>
        {!isEmpty(listInputTemp) && (
          <Formik
            initialValues={generateField()}
            validationSchema={storeSchema}
            onSubmit={async (values, errors) => {
              let allowSubmit = true;
              let tempDirectory = [];
              if (JSON.parse(localStorage.getItem(`DIRECTORY`))) {
                tempDirectory = tempDirectory.concat(
                  JSON.parse(localStorage.getItem(`DIRECTORY`))
                );
              }
              let arrayCustomError = {};
              listInputTemp.forEach((element, index) => {
                if (values[`name_${index}`] && values[`gender_${index}`]) {
                  tempDirectory.push({
                    name: values[`name_${index}`],
                    gender: values[`gender_${index}`],
                  });
                }
                if (
                  element.type === CUSTOMER_TYPE.CHILDREN ||
                  element.type === CUSTOMER_TYPE.BABY
                ) {
                  let checkBOD = formatDateOfBirth(
                    values[`dob_${index}`],
                    element.type
                  );
                  if (checkBOD !== "") {
                    console.log(checkBOD);
                    arrayCustomError = {
                      ...arrayCustomError,
                      [`dob_${index}`]: checkBOD,
                    };
                    allowSubmit = false;
                  }
                }
              });
              setCustomError(arrayCustomError);
              const key = "name";
              tempDirectory = [
                ...new Map(
                  tempDirectory.map((item) => [item[key], item])
                ).values(),
              ];
              localStorage.setItem(`DIRECTORY`, JSON.stringify(tempDirectory));
              localStorage.setItem(
                `LAST_REQUEST`,
                JSON.stringify({
                  id: queryData.requestId,
                  name: values?.nameContact,
                  phone: values?.phoneNumberContact,
                  email: values?.emailContact,
                  gender: values?.genderContact,
                  totalGuest: totalGuest,
                })
              );
              let tempValues = {
                ...values,
              };
              if (!isEmpty(invoiceInfo)) {
                tempValues.invoiceInfo = invoiceInfo;
                tempValues.invoiceInfo.recipientPhone =
                  tempValues?.phoneNumberContact;
              }
              localStorage.setItem(
                `CUSTOMER_INFO_${queryData.requestId}`,
                JSON.stringify(tempValues)
              );
              localStorage.setItem(
                `PRICE_INFO_${queryData.requestId}`,
                JSON.stringify({
                  totalPrice: totalMoney,
                  totalGuest: totalGuest || 0,
                  totalPriceInsurance: totalPrice || 0,
                  bagPrice: bagInfo.money || 0,
                })
              );
              if (allowSubmit) {
                router.push({
                  pathname: "/ve-may-bay/payment/paymentMethodList",
                  query: {
                    ...router?.query,
                  },
                });
              }
            }}
          >
            {({ values, errors, setFieldError }) => {
              let totalMoneyBoxPrice = 0;
              let totalMoneyInsurance = totalPrice;
              totalMoneyBoxPrice =
                totalMoney + totalPrice + bagInfo.money - discountMoneyVoucher;

              let detailPriceTicket = {};
              if (!isEmpty(detailTicketOutBound)) {
                const dataBagIn =
                  detailTicketInBound?.ticket?.outbound?.baggages;
                const dataBagOut =
                  detailTicketOutBound?.ticket?.outbound?.baggages;
                let totalInboundBagMoney = 0;
                let totalOutboundBagMoney = 0;
                for (let index = 0; index < totalGuest; index++) {
                  let bagInfoById = dataBagOut.filter(
                    (el) => el.id === values[`outbound_baggage_${index}`]
                  );

                  if (!isEmpty(bagInfoById)) {
                    totalOutboundBagMoney += bagInfoById[0]?.price;
                  }
                  if (detailTicketInBound) {
                    let bagInfoByIdInbound = dataBagIn.filter(
                      (el) => el.id === values[`inbound_baggage_${index}`]
                    );
                    if (!isEmpty(bagInfoByIdInbound)) {
                      totalInboundBagMoney += bagInfoByIdInbound[0]?.price;
                    }
                  }
                }

                detailPriceTicket = {
                  outbound: {
                    numAdult: detailTicketOutBound?.searchRequest?.numAdults,
                    adultMoney:
                      parseInt(
                        detailTicketOutBound?.ticket?.outbound?.ticketdetail
                          ?.farePrice
                      ) *
                      parseInt(detailTicketOutBound?.searchRequest?.numAdults),
                    numChildren:
                      detailTicketOutBound?.searchRequest?.numChildren,
                    childMoney:
                      parseInt(
                        detailTicketOutBound?.ticket?.outbound?.ticketdetail
                          ?.farePrice
                      ) *
                      parseInt(
                        detailTicketOutBound?.searchRequest?.numChildren
                      ),
                    numInfants: detailTicketOutBound?.searchRequest?.numInfants,
                    babyMoney:
                      detailTicketOutBound?.ticket?.outbound?.ticketdetail
                        ?.priceInfants,
                    taxFee:
                      parseInt(detailTicketOutBound?.ticket?.totalPrice) -
                      parseInt(
                        detailTicketOutBound?.ticket?.outbound?.ticketdetail
                          ?.farePrice *
                          (detailTicketOutBound?.searchRequest?.numAdults +
                            detailTicketOutBound?.searchRequest?.numChildren)
                      ),
                    baggageMoney: totalOutboundBagMoney,
                    total:
                      detailTicketOutBound?.ticket?.totalPrice +
                      totalOutboundBagMoney,
                  },
                  inbound: detailTicketInBound
                    ? {
                        numAdult: detailTicketInBound?.searchRequest?.numAdults,
                        adultMoney:
                          parseInt(
                            detailTicketInBound?.ticket?.outbound?.ticketdetail
                              ?.farePrice
                          ) *
                          parseInt(
                            detailTicketInBound?.searchRequest?.numAdults
                          ),
                        numChildren:
                          detailTicketInBound?.searchRequest?.numChildren,
                        childMoney:
                          parseInt(
                            detailTicketInBound?.ticket?.outbound?.ticketdetail
                              ?.farePrice
                          ) *
                          parseInt(
                            detailTicketInBound?.searchRequest?.numChildren
                          ),
                        numInfants:
                          detailTicketInBound?.searchRequest?.numInfants,
                        babyMoney:
                          detailTicketInBound?.ticket?.outbound?.ticketdetail
                            ?.priceInfants,
                        taxFee:
                          parseInt(detailTicketInBound?.ticket?.totalPrice) -
                          parseInt(
                            detailTicketInBound?.ticket?.outbound?.ticketdetail
                              ?.farePrice *
                              (detailTicketInBound?.searchRequest?.numAdults +
                                detailTicketInBound?.searchRequest?.numChildren)
                          ),
                        baggageMoney: totalInboundBagMoney,
                        total:
                          detailTicketInBound?.ticket?.totalPrice +
                          totalInboundBagMoney,
                      }
                    : null,
                  insurance: totalMoneyInsurance,
                  voucherCode: voucherUsing?.code || "",
                  voucherMoney: discountMoneyVoucher,
                  surchargeMoney: 0,
                  totalPayment: totalMoneyBoxPrice,
                };
              }

              return (
                <Form autoComplete="off">
                  <Box padding={2}>
                    <ContactInfo />
                  </Box>
                  <Divider className={classes.divider} />
                  <Box padding={2}>
                    <CustomerInfo
                      listInputTemp={listInputTemp}
                      customError={customError}
                    />
                  </Box>
                  <Divider className={classes.divider} />
                  <Box padding={2}>
                    <Baggage
                      outboundBaggageInfo={
                        detailTicketOutBound?.ticket?.outbound
                      }
                      inboundBaggageInfo={detailTicketInBound?.ticket?.outbound}
                      totalGuest={
                        parseInt(queryData.adultCount) +
                        parseInt(queryData.childCount)
                      }
                      customerInfo={listInputTemp}
                      bagInfo={bagInfo}
                      setBagInfo={setBagInfo}
                      setBaggageCustomer={setBaggageCustomer}
                      detailTicketStatus={detailTicketStatus}
                      setMessageAlert={setMessageAlert}
                    />
                  </Box>
                  <InsuranceBox
                    setBuyInsurance={setBuyInsurance}
                    paramsInsurance={paramsInsurance}
                    totalPrice={totalPrice}
                    setTotalPrice={setTotalPrice}
                  />
                  <Divider className={classes.divider} />
                  <Box pr={1}>
                    <Invoice
                      invoiceInfo={invoiceInfo}
                      handleInvoiceInfo={handleInvoiceInfo}
                      recipientEmail={values?.emailContact}
                      recipientName={values?.nameContact}
                    />
                  </Box>
                  <Divider className={classes.divider} />
                  <Box className={classes.divider3} mb={8} height={70}></Box>
                  <Box className={classes.footer}>
                    <TotalPriceStepTwoBox
                      totalGuest={totalGuest}
                      totalMoney={totalMoney}
                      totalSubMoney={totalSubMoney}
                      surchargeMoney={0}
                      discountMoneyVoucher={discountMoneyVoucher}
                      notModal={true}
                      bagInfo={bagInfo}
                      insuranceMoney={totalPrice}
                      bagPrice={bagInfo.money}
                      toggleDrawer={toggleDrawer}
                      voucherUsing={voucherUsing}
                      step={STEP_NUMBER.PAYMENT_STEP_2}
                    />
                  </Box>
                  <PriceDetailsTwoWayModal
                    open={detailPriceModal}
                    toggleDrawer={toggleDrawer}
                    item={detailPriceTicket}
                  />
                </Form>
              );
            }}
          </Formik>
        )}
      </Box>
      <FlightInfoDetailBoxModal
        open={visibleDrawerType}
        setOpen={setVisibleDrawerType}
        ticket={detailTicketOutBound?.ticket?.outbound}
        ticketPersion={detailTicketOutBound?.searchRequest}
        ticketInBound={detailTicketInBound?.ticket?.outbound}
        data={{
          ...detailTicketOutBound,
          ticketClasses: generalInfomation?.ticketclass,
        }}
        dataInBound={{
          ...detailTicketInBound,
          ticketClasses: generalInfomation?.ticketclass,
        }}
        isTransit={!!detailTicketOutBound?.ticket?.outbound?.transitTickets}
        airlineInfo={detailTicketOutBound?.agency}
      />
      <DialogShowMessage
        message={messageAlert}
        toggleDrawer={toggleDrawerAlert}
      />
    </>
  );
};

export default MobileContent;
