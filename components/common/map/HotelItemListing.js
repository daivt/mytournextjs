import { memo } from "react";
import cookie from "js-cookie";
import { useSnackbar } from "notistack";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import { Box, Grid, IconButton } from "@material-ui/core";

import { addFavorite, removeFavorite } from "@api/user";
import { IconHeart, IconSightSeeing } from "@public/icons";
import { isEmpty, areEqualHotelItemListing } from "@utils/helpers";
import {
  TOKEN,
  LAST_HOTEL_FAVORITES,
  routeStatic,
  prefixUrlIcon,
  listIcons,
} from "utils/constants";

import Link from "@src/link/Link";
import Image from "@src/image/Image";
import snackbarSetting from "@src/alert/Alert";
import Vote from "@components/common/desktop/vote/Vote";

const useStyles = makeStyles((theme) => ({
  linkHotelDetail: {
    textDecoration: "none !important",
  },
  wrapImgHotel: {
    position: "relative",
    height: "100%",
  },
  imgHotel: {
    height: "100%",
    width: "100%",
    borderRadius: "8px 0 0 8px",
    objectFit: "cover",
  },
  wrapHotelLoved: {
    position: "absolute",
    top: -1,
    right: -2,
  },
  wrapInfoHotel: {
    margin: "12px 8px",
    height: "calc(100% - 24px)",
  },
  wrapHotelNames: {
    color: theme.palette.black.black3,
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  wrapRatingStar: {
    margin: "6px 0 8px",
    display: "flex",
  },
  ratingVote: {
    color: theme.palette.yellow.yellowLight3,
  },
  point: {
    borderRadius: 4,
    fontSize: 12,
    fontWeight: 600,
    color: theme.palette.white.main,
    background: theme.palette.blue.blueLight8,
    padding: "1px 4px;",
  },
  ratingText: {
    marginLeft: 4,
    fontSize: 12,
  },
  countComment: {
    color: theme.palette.gray.grayDark8,
    fontSize: 12,
    paddingLeft: 2,
    paddingRight: 2,
  },
  wrapLocation: {
    color: theme.palette.black.black3,
    fontSize: 12,
    display: "flex",
    alignItems: "center",
    paddingTop: 4,
  },
  wrapRoomAndPrice: {
    fontSize: 12,
    color: theme.palette.black.black3,
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    marginTop: 16,
  },
  subPrice: {
    textDecorationLine: "line-through",
    color: theme.palette.black.black4,
    margin: "2px 0",
  },
  mainPrice: {
    fontWeight: 600,
    fontSize: 16,
    lineHeight: "19px",
  },
  discountPercent: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 42,
    height: 18,
    color: theme.palette.white.main,
    background: theme.palette.pink.main,
    borderRadius: "3px 3px 0px 3px",
    position: "relative",
  },
  discountText: {
    padding: "2px 4px",
  },
  triangleBottomright: {
    width: 0,
    height: 0,
    borderStyle: "solid",
    borderColor: `transparent ${theme.palette.pink.main} transparent transparent`,
    borderWidth: "0px 5px 5px 0",
    position: "absolute",
    right: 0,
    bottom: -4,
  },
  favorite: {
    fill: "#FF1284",
    stroke: theme.palette.white.main,
  },
  notFavorite: {
    fill: "rgba(0, 0, 0, 0.6)",
    stroke: theme.palette.white.main,
  },
  textLocation: {
    whiteSpace: "nowrap",
    overflow: "hidden !important",
    textOverflow: "ellipsis",
    width: "100%",
  },
  hotelName: {
    fontWeight: 600,
    color: theme.palette.black.black3,
    display: "-webkit-box",
    WebkitLineClamp: 3,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
    fontSize: 16,
    lineHeight: "19px",
  },
  iconTripadvisor: {
    width: 18,
    height: 18,
  },
}));

const HotelItemListing = ({
  item = {},
  styleHotelItem = "",
  paramsFilterInit = {},
}) => {
  const router = useRouter();
  const classes = useStyles();
  const [isFavorite, setIsFavorite] = useState(item.isFavoriteHotel);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  let paramsUrl = {
    alias: `${item.id}-${item?.hotelName.stringSlug()}.html`,
  };

  const checkIsEmptyParamsQuery = () => {
    return !(
      router.query.checkIn ||
      router.query.checkOut ||
      router.query.adults ||
      router.query.rooms ||
      router.query.children
    );
  };

  if (!checkIsEmptyParamsQuery()) {
    paramsUrl = {
      ...paramsUrl,
      checkIn: paramsFilterInit.checkIn,
      checkOut: paramsFilterInit.checkOut,
      adults: parseInt(paramsFilterInit.adults),
      rooms: parseInt(paramsFilterInit.rooms),
      children: parseInt(paramsFilterInit.children),
    };
    if (!isEmpty(paramsFilterInit.childrenAges)) {
      paramsUrl = {
        ...paramsUrl,
        childrenAges: paramsFilterInit.childrenAges.toString(),
      };
    }
  }
  paramsUrl = {
    ...paramsUrl,
    priceKey: item.priceKey || "",
  };

  useEffect(() => {
    checkFavoriteHotel();
  }, []);

  useEffect(() => {
    setIsFavorite(item.isFavoriteHotel);
  }, [item]);

  const checkFavoriteHotel = () => {
    const listHotelFavorites =
      JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
    if (isEmpty(cookie.get(TOKEN)) && !isEmpty(listHotelFavorites)) {
      setIsFavorite(listHotelFavorites.includes(item.id));
    }
  };

  const handleFavorite = async (event) => {
    event.preventDefault();
    if (!isEmpty(cookie.get(TOKEN))) {
      changeFavorite();
    } else {
      let listHotelFavoritesTemp =
        JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
      if (isFavorite) {
        listHotelFavoritesTemp = listHotelFavoritesTemp.filter(
          (el) => el !== item.id
        );
      } else {
        listHotelFavoritesTemp.unshift(item.id);
      }
      localStorage.setItem(
        LAST_HOTEL_FAVORITES,
        JSON.stringify(listHotelFavoritesTemp)
      );
      setIsFavorite(!isFavorite);
    }
  };

  const changeFavorite = async () => {
    try {
      if (isFavorite) {
        const { data } = await removeFavorite({ hotelId: item.id });
        if (data.code === 200) {
          setIsFavorite(!isFavorite);
        } else {
          data?.message &&
            enqueueSnackbar(
              data?.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
        }
      } else {
        const { data } = await addFavorite({ hotelId: item.id });
        if (data.code === 200) {
          setIsFavorite(!isFavorite);
        } else {
          data?.message &&
            enqueueSnackbar(
              data?.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
        }
      }
    } catch (error) {}
  };

  return (
    <Link
      href={{
        pathname: routeStatic.HOTEL_DETAIL.href,
        query: paramsUrl,
      }}
      className={classes.linkHotelDetail}
    >
      <Grid container className={styleHotelItem}>
        <Grid item xl={4} lg={4} sm={4} md={4} xs={4}>
          <Box className={classes.wrapImgHotel}>
            <img src={item.srcImage} className={classes.imgHotel} />
            <Box className={classes.wrapHotelLoved}>
              <IconButton onClick={handleFavorite}>
                <IconHeart
                  className={`svgFillAll ${
                    isFavorite ? classes.favorite : classes.notFavorite
                  }`}
                />
              </IconButton>
            </Box>
          </Box>
        </Grid>
        <Grid item xl={8} lg={8} sm={8} md={8} xs={8}>
          <Box className={classes.wrapInfoHotel}>
            <Box className={classes.wrapHotelNames}>
              <>
                <Box className={classes.hotelName}>{item.hotelName}</Box>
                <Box className={classes.wrapRatingPoint}>
                  <Box className={classes.wrapRatingStar}>
                    <Vote maxValue={item.ratingStar} value={item.ratingStar} />
                  </Box>
                  <Box
                    display="flex"
                    alignItems="center"
                    style={{ marginTop: "8px" }}
                  >
                    <Box component="span" className={classes.point}>
                      {item.ratingPoint}
                    </Box>
                    <Box component="span" className={classes.ratingText}>
                      {item.ratingText}
                    </Box>
                    {item.countComment > 0 && (
                      <Box component="span" className={classes.countComment}>
                        ({item.countComment} đánh giá)
                      </Box>
                    )}
                    <Image
                      srcImage={`${prefixUrlIcon}${listIcons.IconTripadvisor}`}
                      className={classes.iconTripadvisor}
                    />
                    <Box component="span" pl={4 / 8} fontSize={12}>
                      {2 * item.taRating === 10
                        ? 10
                        : (2 * item.taRating).toFixed(1)}
                    </Box>
                  </Box>
                  <Box className={classes.wrapLocation}>
                    <Box width={16}>
                      <IconSightSeeing />
                    </Box>
                    <Box
                      component="span"
                      marginLeft={6 / 8}
                      marginTop={6 / 8}
                      className={classes.textLocation}
                    >
                      {item.location}
                    </Box>
                  </Box>
                </Box>
              </>
              <Box className={classes.wrapRoomAndPrice}>
                {!isEmpty(item.discountPercent) && (
                  <>
                    <Box className={classes.discountPercent}>
                      <Box className={classes.discountText}>
                        {item?.discountPercent}%
                      </Box>
                      <Box className={classes.triangleBottomright}></Box>
                    </Box>
                    <Box component="span" className={classes.subPrice}>
                      {item.subPrice.formatMoney()} /đêm
                    </Box>
                  </>
                )}
                <Box component="span" className={classes.mainPrice}>
                  {item.mainPrice.formatMoney()}
                  <Box
                    component="span"
                    color="black.black4"
                    fontWeight="normal"
                  >
                    {" "}
                    /đêm
                  </Box>
                </Box>
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Link>
  );
};
export default memo(HotelItemListing, areEqualHotelItemListing);
