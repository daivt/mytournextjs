import React, { Fragment } from "react";
import { Box, Dialog, Divider, Slide } from "@material-ui/core";
import { createStyles, makeStyles, useTheme } from "@material-ui/core/styles";

import { isEmpty } from "@utils/helpers";
import { listString, prefixUrlIcon, listIcons } from "@utils/constants";
import Image from "@src/image/Image";
import { IconBack2, IconBeach } from "@public/icons";

import ButtonComponent from "@src/button/Button";
import MapDetail from "@components/common/map/MapDetail";

const useStyles = makeStyles((theme) =>
  createStyles({
    dialogContent: {
      display: "flex",
      flexDirection: "column",
      overflowY: "hidden",
      fontSize: 14,
      color: theme.palette.black.black3,
    },
    mapContainer: {
      height: "100vh",
      position: "relative",
    },
    backBtn: {
      position: "absolute",
      display: "flex",
      top: 16,
      zIndex: 1,
    },
    addressContainer: {
      position: "sticky",
      bottom: 0,
      background: "white",
      borderRadius: "16px 16px 0px 0px",
      padding: 0,
      maxHeight: 353,
      overflowY: "auto",
      paddingBottom: 65,
    },
    addressTitle: {
      fontWeight: 600,
      fontSize: 14,
      lineHeight: "17px",
      color: "#1A202C",
      padding: "2px 16px",
    },
    addressContentContainer: {
      padding: "12px 16px",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
    },
    buttonContainer: {
      padding: "8px 16px",
      position: "fixed",
      bottom: 0,
      width: "100%",
      backgroundColor: theme.palette.white.main,
      borderTop: "1px solid #EDF2F7",
    },
  })
);
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const MapDetailDialog = (props) => {
  const theme = useTheme();
  const {
    child,
    listInterestingPlaces = {},
    handleScrollToRate = () => {},
    coordinate = {},
  } = props;
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <span onClick={handleOpen} aria-hidden="true">
        {child}
      </span>
      <Dialog
        fullScreen
        maxWidth="sm"
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <div className={classes.dialogContent}>
          <div className={classes.mapContainer}>
            <div
              className={classes.backBtn}
              onClick={handleClose}
              aria-hidden="true"
            >
              <IconBack2 />
            </div>
            <MapDetail
              position={coordinate}
              markerPosition={coordinate}
              places={listInterestingPlaces}
              zoomDefault={12}
            />
            <Box
              px={2}
              pb={1}
              borderTop="1px solid #EDF2F7"
              className={classes.addressContainer}
            >
              <p className={classes.addressTitle}>
                {listString.IDS_MT_HIGHT_LIGHT_ADDRESS}
              </p>
              <Divider />
              {!isEmpty(listInterestingPlaces) &&
                listInterestingPlaces.items.map((el, index) => {
                  const iconInfo = (listInterestingPlaces.types || []).find(
                    (v) => el.typeId === v.id
                  );
                  return (
                    <Fragment key={index.toString()}>
                      <Box
                        pt={12 / 8}
                        className={classes.addressContentContainer}
                      >
                        <Box display="flex" alignItems="center">
                          {!isEmpty(iconInfo.icon) ? (
                            <img
                              src={iconInfo.icon}
                              style={{ width: 24, height: 24 }}
                              alt=""
                            />
                          ) : (
                            <img
                              src={`${prefixUrlIcon}${listIcons.IconBeach}`}
                            />
                          )}

                          <Box display="flex" flexDirection="column" pl={1}>
                            <Box component="span" lineHeight="17px">
                              {el.name}
                            </Box>
                            <Box
                              component="span"
                              fontSize={12}
                              lineHeight="14px"
                              pt={4 / 8}
                              color="gray.grayDark8"
                            >
                              {iconInfo.name}
                            </Box>
                          </Box>
                        </Box>
                        <Box>{`${(el.distanceMeters / 1000).toFixed(
                          2
                        )}km`}</Box>
                      </Box>
                      <Divider />
                    </Fragment>
                  );
                })}

              <Box pt={12 / 8} className={classes.buttonContainer}>
                <ButtonComponent
                  backgroundColor={theme.palette.secondary.main}
                  height={48}
                  fontSize={16}
                  fontWeight={600}
                  borderRadius={8}
                  handleClick={() => {
                    setOpen(false);
                    handleScrollToRate();
                  }}
                >
                  {listString.IDS_MT_SELECT_ROOM}
                </ButtonComponent>
              </Box>
            </Box>
          </div>
        </div>
      </Dialog>
    </>
  );
};
export default MapDetailDialog;
