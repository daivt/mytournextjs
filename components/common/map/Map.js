import React, { useRef, useEffect } from "react";
import {
  GoogleMap,
  withGoogleMap,
  withScriptjs,
  InfoWindow,
} from "react-google-maps";
import { compose, withProps } from "recompose";
import { isEmpty } from "@utils/helpers";
// import { IconVietNamFlag } from "@public/icons";

// export const KEY_GOOGLE_MAP = "AIzaSyA9SPYP838bh5o7pBC8xA7632sDJ0jJwxM";
export const KEY_GOOGLE_MAP = "AIzaSyDeuZoF-imbzg9q9g3zKwRhE9mYfuobV2Q";
const BLUE = "#00B6F3";
const WHITE = "#FFFFFF";
const markerData = [
  {
    name: "Quần đảo Hoàng Sa",
    position: { lat: 16.84444444, lng: 112.347222224 },
  },
  {
    name: "Quần đảo Trường Sa",
    position: { lat: 8.64464, lng: 111.91969 },
  },
];

const Map = compose(
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${KEY_GOOGLE_MAP}&v=3.exp&libraries=drawing`,
    loadingElement: <div style={{ width: "100%", height: "100%" }} />,
    containerElement: <div style={{ width: "100%", height: "100%" }} />,
    mapElement: <div style={{ width: "100%", height: "100%" }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) => {
  let ref = useRef(null);
  const {
    items,
    itemActive,
    handleActive,
    handleCenterChanged,
    activeMove,
    centerAddress = { latitude: 21.00626, longitude: 105.85537 },
  } = props;
  // const [zoom] = React.useState(13);
  const [coordinateActive, setCoordinateActive] = React.useState(
    !isEmpty(itemActive)
      ? itemActive.address.coordinate
      : {
          latitude: centerAddress.latitude || 21.00626,
          longitude: centerAddress.longitude || 105.85537,
        }
  );

  const disableOption = {
    mapTypeControl: false,
    fullscreenControl: false,
    clickableIcons: true,
    streetViewControl: true,
    zoomControl: true,
    keyboardShortcuts: true,
    draggableCursor: "pointer",
    // gestureHandling: "none",
  };
  // const getPixelPositionOffset = (width, height) => ({
  //   x: -(width / 2),
  //   y: -(height / 2),
  // });
  // const getCenter = () => {
  //   if (!ref.current) return;
  //   const newPos = ref.current.getCenter().toJSON();
  // };
  const onDragEnd = () => {
    if (!ref.current) return;
    const newPos = ref.current.getCenter().toJSON();
    if (newPos) {
      const tempPosition = { latitude: newPos.lat, longitude: newPos.lng };
      setCoordinateActive(tempPosition);
      if (handleCenterChanged && activeMove) {
        handleCenterChanged({ ...tempPosition, radius: 20 });
      }
    }
  };
  const fitBounds = () => {
    const bounds = new window.google.maps.LatLngBounds();
    items.map((el) => {
      const { coordinate } = el.address;
      bounds.extend({ lat: coordinate.latitude, lng: coordinate.longitude });
      return el.id;
    });
    if (!ref.current) return;
    if (items.length > 1) ref.current.fitBounds(bounds);
  };
  useEffect(() => {
    if (!isEmpty(items)) fitBounds(); // eslint-disable-next-line
  }, [items]);
  // useEffect(() => {
  //   if (!isEmpty(itemActive))
  //     setCoordinateActive(itemActive.address.coordinate); // eslint-disable-next-line
  // }, [itemActive]);
  useEffect(() => {
    return () => {
      ref = null;
    }; // eslint-disable-next-line
  }, []);
  const getMapsProps = () => {
    let result = {};
    if (!isEmpty(items) && items.length === 1) {
      const { coordinate } = items[0].address;
      result = {
        ...result,
        center: { lat: coordinate.latitude, lng: coordinate.longitude },
      };
    }
    return result;
  };
  return (
    <GoogleMap
      {...getMapsProps()}
      defaultZoom={13}
      // center={{
      //   lat: coordinateActive.latitude,
      //   lng: coordinateActive.longitude,
      // }}
      key={KEY_GOOGLE_MAP}
      defaultOptions={disableOption}
      // onCenterChanged={getCenter}
      ref={ref}
      // ref={props.onMapMounted}
      onDragEnd={onDragEnd}
    >
      {!isEmpty(items) &&
        items.map((el) => {
          const isActive = !isEmpty(itemActive)
            ? el.id === itemActive.id
            : false;
          const { coordinate } = el.address;
          if (isEmpty(coordinate)) return null;
          return (
            <InfoWindow
              key={el.id}
              position={{
                lat: coordinate.latitude,
                lng: coordinate.longitude,
              }}
            >
              <div
                style={{
                  backgroundColor: isActive ? BLUE : WHITE,
                  fontSize: 14,
                  lineHeight: "17px",
                  padding: "3px 6px",
                  borderRadius: 8,
                  position: "relative",
                  color: isActive ? WHITE : BLUE,
                  fontWeight: 600,
                }}
                onClick={() => handleActive(el)}
                aria-hidden="true"
              >
                <div>
                  {!isEmpty(el.basePrice) &&
                  el.basePrice >= 0 &&
                  !el.hiddenPrice
                    ? `${new Intl.NumberFormat().format(
                        Number(el.basePrice)
                      )} đ`
                    : ""}
                </div>
              </div>
            </InfoWindow>
          );
        })}
      {/* {markerData.map((el, i) => {
        return (
          <InfoWindow position={el.position} key={i}>
            <div
              style={{
                backgroundColor: WHITE,
                fontSize: 14,
                lineHeight: "17px",
                padding: "3px 6px",
                borderRadius: 8,
                display: "flex",
                color: BLUE,
              }}
            >
              {el.name}
            </div>
          </InfoWindow>
        );
      })} */}
    </GoogleMap>
  );
});

export default Map;
