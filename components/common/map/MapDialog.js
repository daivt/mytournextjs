import clsx from "clsx";
import moment from "moment";
import {
  Dialog,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Slide,
  Box,
} from "@material-ui/core";
import { useRouter } from "next/router";
import { useState, useRef, forwardRef, useEffect } from "react";
import { createStyles, makeStyles } from "@material-ui/core/styles";

import {
  IconBack,
  IconFilter,
  IconMenu,
  IconDot,
  IconReload,
} from "@public/icons";
import { dataUserVote } from "@utils/dataFake";
import { getHotelsAvailability } from "@api/hotels";
import { isEmpty, adapterHotelAvailability } from "@utils/helpers";
import {
  formatDate,
  listString,
  DELAY_TIMEOUT_POLLING,
} from "@utils/constants";
import SlideShow from "@components/common/slideShow/SlideShow";

import Map from "./Map";
import HotelItemListing from "@components/common/map/HotelItemListing";
import FilterHotelListing from "@components/mobile/hotels/listing/FilterHotelListing";

const BLUE = "#00B6F3";
const useStyles = makeStyles((theme) =>
  createStyles({
    dialogContent: {
      display: "flex",
      flexDirection: "column",
      overflowY: "hidden",
    },
    appBar: {
      position: "sticky",
      top: 0,
      borderRadius: 0,
      background: "white",
      color: "#1A202C",
      boxShadow: "none",
    },
    title: {
      width: "calc(100% - 64px)",
      display: "flex",
      flexDirection: "column",
      textAlign: "center",
    },
    titleHeader: {
      fontSize: 16,
      lineHeight: "19px",
      fontWeight: 600,
    },
    subHeader: {
      display: "flex",
      justifyContent: "space-between",
      padding: "12px 16px",
    },
    subHeaderItem: {
      position: "relative",
      display: "flex",
      alignItems: "center",
    },
    listMenu: {
      justifyContent: "flex-end",
      color: BLUE,
    },
    menuActive: {
      stroke: BLUE,
    },
    mapContainer: {
      height: "calc(100vh - 104px)",
      position: "relative",
    },
    slideContainer: {
      position: "absolute",
      bottom: 34,
      width: "100%",
    },
    slideItem: {
      background: "white",
      width: 322,
      margin: "0 6px",
      borderRadius: 8,
    },
    checkIn: {
      color: theme.palette.black.black4,
      fontSize: 12,
    },
    wrapInfoCheckIn: {
      display: "flex",
      justifyContent: "center",
      marginTop: 2,
    },
    findBtn: {
      position: "absolute",
      display: "flex",
      top: 16,
      background: "white",
      zIndex: 1,
      padding: "4px 8px",
      borderRadius: 30,
      fontSize: 14,
      lineHeight: "17px",
      fontWeight: 600,
      color: "#1A202C",
      left: "50%",
      transform: "translateX(-50%)",
      whiteSpace: "nowrap",
    },
    dotFilter: {
      width: 7,
      height: 7,
      background: theme.palette.red.redLight5,
      position: "absolute",
      top: 0,
      right: -7,
      borderRadius: "50%",
    },
  })
);
const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const listTypeDrawer = {
  FILTER_DRAWER: "FILTER_DRAWER",
};
const pageSize = 20;
const MapDialog = (props) => {
  const {
    child,
    items,
    paramsFilterInit,
    location,
    filters,
    paramsUrl,
  } = props;
  const slideEl = useRef();
  const classes = useStyles();
  const router = useRouter();
  const [open, setOpen] = useState(false);
  const [itemActive, setItemActive] = useState({});
  const [dataHotel, setDataHotel] = useState(items);
  const [visibleDrawerType, setVisibleDrawerType] = useState("");
  let sortQueryUrl = {};
  let breakPolling = true;
  useEffect(() => {
    const itemTemp = [...items];
    // if (itemTemp.length > 20) {
    //   itemTemp.splice(20, itemTemp.length - 20);
    // }
    setDataHotel(itemTemp);
    return () => {
      breakPolling = false;
    };
  }, [items]);
  if (!isEmpty(router.query.sort)) {
    sortQueryUrl = {
      ...sortQueryUrl,
      sort: router.query.sort || "",
    };
  }
  const handleOpen = () => {
    setItemActive(dataHotel[0]);
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleActive = (it) => {
    setItemActive(it);
    let expectIdx = 0;
    (dataHotel || []).forEach((el, idx) => {
      if (el.id === it.id) {
        expectIdx = idx;
      }
    });
    if (slideEl.current) slideEl.current.slickGoTo(expectIdx);
  };
  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };
  const handleSearchData = async (filters) => {
    let polling = true;
    try {
      const keys = Object.keys(filters);
      let filtersQuery = {};
      keys.forEach((el) => {
        if (!isEmpty(filters[el])) {
          if (el === "ratingRanges") {
            let ratingRangesTemp = [];
            filters.ratingRanges.forEach((el) => {
              ratingRangesTemp.push(dataUserVote[el].value);
            });
            filtersQuery = {
              ...filtersQuery,
              ratingRanges: ratingRangesTemp,
            };
          } else {
            filtersQuery = {
              ...filtersQuery,
              [el]: filters[el],
            };
          }
        }
      });
      const dataDTO = {
        searchType: "hotel",
        aliasCode: paramsFilterInit.aliasCode,
        page: 1,
        size: pageSize,
        filters: filtersQuery,
        ...paramsUrl,
        ...sortQueryUrl,
      };
      while (polling && breakPolling) {
        const { data } = await getHotelsAvailability(dataDTO);
        if (data.code === 200) {
          if (data.data.completed) {
            setDataHotel(data?.data?.items);
            polling = false;
            break;
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      polling = false;
    }
  };
  return (
    <>
      <span onClick={handleOpen} aria-hidden="true">
        {child}
      </span>
      {open && (
        <Dialog
          fullScreen
          maxWidth="sm"
          open={open}
          onClose={handleClose}
          TransitionComponent={Transition}
        >
          <div className={classes.dialogContent}>
            <AppBar className={classes.appBar}>
              <Toolbar>
                <IconButton
                  edge="start"
                  color="inherit"
                  onClick={handleClose}
                  aria-label="close"
                >
                  <IconBack style={{ fontSize: 24 }} />
                </IconButton>
                <Typography variant="body2" className={classes.title}>
                  <span className={classes.titleHeader}>{location?.name}</span>
                  <Box className={classes.wrapInfoCheckIn}>
                    <Box component="span" className={classes.checkIn}>
                      {moment(
                        paramsFilterInit.checkIn,
                        formatDate.firstDay
                      ).format(formatDate.dayAndMonth)}
                      &nbsp; -&nbsp;
                      {moment(
                        paramsFilterInit.checkOut,
                        formatDate.firstDay
                      ).format(formatDate.dayAndMonth)}
                    </Box>
                    <Box component="span" margin={4 / 8} display="flex">
                      <IconDot />
                    </Box>
                    <Box component="span" className={classes.checkIn}>
                      {paramsFilterInit.rooms || 0} phòng
                    </Box>
                    <Box component="span" margin={4 / 8} display="flex">
                      <IconDot />
                    </Box>
                    <Box component="span" className={classes.checkIn}>
                      {Number(paramsFilterInit.adults || 0) +
                        Number(paramsFilterInit.children || 0)}
                      &nbsp;khách
                    </Box>
                  </Box>
                </Typography>
              </Toolbar>
              <div className={classes.subHeader}>
                <div
                  className={classes.subHeaderItem}
                  onClick={toggleDrawer(listTypeDrawer.FILTER_DRAWER)}
                >
                  <IconFilter />
                  &nbsp;Lọc
                  {!isEmpty(paramsFilterInit) && (
                    <Box component="span" className={classes.dotFilter} />
                  )}
                </div>
                <div
                  className={clsx(classes.subHeaderItem, classes.listMenu)}
                  onClick={handleClose}
                  aria-hidden="true"
                >
                  <IconMenu className={`svgFillAll ${classes.menuActive}`} />
                  &nbsp;Danh sách
                </div>
              </div>
            </AppBar>
            <div className={classes.mapContainer}>
              <div className={classes.findBtn}>
                <IconReload />
                &nbsp;{listString.IDS_MT_TEXT_FIND_NEAR_BY}
              </div>
              <Map
                items={dataHotel}
                itemActive={itemActive}
                handleActive={handleActive}
              />
              <div className={classes.slideContainer}>
                <SlideShow
                  settingProps={{
                    dots: false,
                    infinite: false,
                    arrows: false,
                    swipeToSlide: true,
                    slidesToShow: 1,
                    variableWidth: true,
                    lazyLoad: true,
                    // centerMode: true,
                    // adaptiveHeight: true,
                    beforeChange: (current, next) =>
                      setItemActive(dataHotel[next]),
                    ref: slideEl,
                  }}
                >
                  {adapterHotelAvailability(dataHotel).map((el) => (
                    <HotelItemListing
                      styleHotelItem={classes.slideItem}
                      item={el}
                      key={el.id}
                      paramsFilterInit={paramsFilterInit}
                    />
                  ))}
                </SlideShow>
              </div>
            </div>
          </div>
          <FilterHotelListing
            open={visibleDrawerType === listTypeDrawer.FILTER_DRAWER}
            toggleDrawer={toggleDrawer}
            filtersData={filters}
            handleFilterSearch={(filters) => {
              setVisibleDrawerType("");

              handleSearchData(filters);
            }}
            isResetState={false}
            isFilterMap
          />
        </Dialog>
      )}
    </>
  );
};
export default MapDialog;
