import { isEmpty } from "@utils/helpers";
import React from "react";
import {
  GoogleMap,
  withGoogleMap,
  withScriptjs,
  Marker,
  OverlayView,
} from "react-google-maps";
import { compose, withProps } from "recompose";
// import { IconLocationHistory } from "@public/icons";
import { KEY_GOOGLE_MAP } from "./Map";

const MapDetail = compose(
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${KEY_GOOGLE_MAP}&v=3.exp&libraries=drawing`,
    loadingElement: <div style={{ width: "100%", height: "100%" }} />,
    containerElement: <div style={{ width: "100%", height: "100%" }} />,
    mapElement: <div style={{ width: "100%", height: "100%" }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) => {
  const {
    position,
    places = {},
    markerPosition,
    type,
    zoomDefault = 16,
  } = props;
  const coordinateActive = markerPosition ||
    position || {
      latitude: 21.00626,
      longitude: 105.85537,
    };
  const [zoom] = React.useState(zoomDefault);

  const disableOption = {
    mapTypeControl: false,
    fullscreenControl: false,
    clickableIcons: true,
    streetViewControl: true,
    zoomControl: true,
    keyboardShortcuts: true,
    draggableCursor: "pointer",
    // gestureHandling: "none",
  };
  const getPixelPositionOffset = (width, height) => ({
    x: -(width / 2),
    y: -(height / 2),
  });
  let placeList = [];
  if (!isEmpty(places.items)) {
    placeList =
      type === "top_address"
        ? places.items.filter((v, idx) => idx < 5)
        : places.items;
  }
  return (
    <GoogleMap
      defaultZoom={zoom}
      center={{
        lat: coordinateActive.latitude,
        lng: coordinateActive.longitude,
      }}
      key={KEY_GOOGLE_MAP}
      defaultOptions={disableOption}
    >
      {!isEmpty(placeList) &&
        placeList.map((el, i) => {
          const iconInfo = (places.types || []).find((v) => el.typeId === v.id);
          return (
            <OverlayView
              key={i}
              position={{ lat: el.latitude, lng: el.longitude }}
              mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
              getPixelPositionOffset={getPixelPositionOffset}
            >
              <div
                style={{
                  backgroundColor: "fff",
                  fontSize: 14,
                  lineHeight: "17px",
                  padding: "3px 6px",
                  borderRadius: 8,
                  position: "relative",
                  color: "fff",
                }}
                aria-hidden="true"
                title={el.name}
              >
                {iconInfo && (
                  <img
                    alt=""
                    src={iconInfo.markerIcon}
                    style={{ width: 24, height: "auto" }}
                  />
                )}
              </div>
            </OverlayView>
          );
        })}
      {!isEmpty(markerPosition) && (
        <Marker
          position={{
            lat: markerPosition.latitude,
            lng: markerPosition.longitude,
          }}
        />
      )}
    </GoogleMap>
  );
});

export default MapDetail;
