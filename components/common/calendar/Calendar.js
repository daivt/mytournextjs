import { Typography } from "@material-ui/core";
import moment from "moment";
import styled from "styled-components";

const GREY_400 = "#BDBDBD";
const WHITE = "#ffffff";
const DATE_FORMAT_BACK_END = "DD-MM-YYYY";
const Row = styled.div`
  display: flex;
  align-items: center;
`;
const Cell = styled.td`
  height: 49px;
  width: calc(100% / 7);
  box-sizing: border-box;
  padding: 0px;
  user-select: none;
`;

const Calendar = (props) => {
  const {
    type,
    startDate,
    endDate,
    setStartDate,
    setEndDate,
    isSingle,
    handleSubmitFlight,
  } = props;
  const isOutsideRange = (date) => {
    return (
      date.isBefore(moment(), "days") ||
      date.isSameOrAfter(moment().add(1, "year"), "days")
    );
  };
  const handleSelectDate = (day) => {
    if (isSingle) {
      setStartDate(day);
      return;
    }
    if (startDate && endDate && !startDate.isSame(endDate, "days")) {
      setStartDate(day);
      setEndDate(null);
    } else if (startDate) {
      if (day.isBefore(startDate, "days")) {
        setEndDate(startDate);
        setStartDate(day);
        if (type === "flight") handleSubmitFlight(day, startDate);
      } else {
        setEndDate(day);
        if (type === "flight") handleSubmitFlight(startDate, day);
      }
    } else if (endDate) {
      if (day.isBefore(endDate, "days")) {
        setStartDate(day);
        if (type === "flight") handleSubmitFlight(day, endDate);
      } else {
        setStartDate(startDate);
        setEndDate(day);
        if (type === "flight") handleSubmitFlight(startDate, day);
      }
    } else {
      setStartDate(day);
      // setEndDate(day);
    }
  };
  const getColorDay = (day, isEdge) => {
    if (isOutsideRange(day)) return GREY_400;
    if (day.isSame(moment(), "days")) return isEdge ? "white" : "#00B6F3";
    return undefined;
  };

  const renderCalendar = (m) => {
    const totalCells = [];
    let week = [];
    const allWeeks = [];
    const firstDayOfMonth = m.startOf("month");
    for (let d = 1; d < firstDayOfMonth.day(); d += 1) {
      totalCells.push(
        <Cell isFocus={false} hover={false} key={`blank${d} `} />
      );
    }
    for (let d = 0; d < m.daysInMonth(); d += 1) {
      const day = m.clone().add(d, "days");
      const price = 1000000;
      const isSelected =
        startDate &&
        endDate &&
        day.isSameOrBefore(endDate, "days") &&
        day.isSameOrAfter(startDate, "days");

      const isEdge =
        (startDate && day.isSame(startDate, "days")) ||
        (endDate && day.isSame(endDate, "days"));
      startDate &&
        endDate &&
        day.isSameOrBefore(endDate, "days") &&
        day.isSameOrAfter(startDate, "days");
      let cellBG = WHITE;
      if (isSelected) cellBG = "rgba(0, 182, 243, 0.2)";
      if (isEdge) cellBG = "#00B6F3";
      totalCells.push(
        <Cell
          draggable={false}
          hover
          key={`day_${day.format(DATE_FORMAT_BACK_END)}`}
          style={{
            backgroundColor: cellBG,
            color: isEdge ? "white" : "#1A202C",
            borderTopLeftRadius:
              startDate && day.isSame(startDate, "days") ? 4 : 0,
            borderBottomLeftRadius:
              startDate && day.isSame(startDate, "days") ? 4 : 0,
            borderTopRightRadius:
              endDate && day.isSame(endDate, "days") ? 4 : 0,
            borderBottomRightRadius:
              endDate && day.isSame(endDate, "days") ? 4 : 0,
          }}
          onClick={() => {
            if (isOutsideRange(day)) return;
            handleSelectDate(day);
          }}
        >
          <Row
            style={{
              height: "100%",
              padding: 4,
              justifyContent: "center",
              flexDirection: "column",
            }}
          >
            <Typography
              style={{
                width: "100%",
                textAlign: "center",
                color: getColorDay(day, isEdge),
              }}
              variant="body2"
            >
              {d + 1}
            </Typography>
            {/* {price && type === 'flight' && (
              <Typography
                style={{
                  userSelect: 'none',
                  color: isEdge ? 'white' : 'rgb(189, 189, 189)',
                  fontSize: 10,
                  lineHeight: '12px',
                  marginTop: 2,
                }}
                variant="body2"
              >
                {new Intl.NumberFormat().format(price / 1000)}k
              </Typography>
            )} */}
          </Row>
        </Cell>
      );
    }
    for (
      let d = 1;
      d <= (7 - ((m.daysInMonth() + firstDayOfMonth.day()) % 7)) % 7;
      d += 1
    ) {
      totalCells.push(
        <Cell
          isFocus={false}
          hover={false}
          key={`blank${d + firstDayOfMonth.day()} `}
        />
      );
    }

    totalCells.forEach((day, index) => {
      if (index % 7 !== 0) {
        week.push(day);
      } else {
        allWeeks.push(week);
        week = [];
        week.push(day);
      }
      if (index === totalCells.length - 1) {
        allWeeks.push(week);
      }
    });
    return (
      <table
        style={{ borderCollapse: "collapse", width: "100%" }}
        draggable={false}
      >
        <thead>
          <tr>
            <td colSpan={7}>
              <div
                style={{
                  display: "flex",
                  padding: "6px 0",
                  alignItems: "center",
                }}
              >
                <Typography
                  variant="subtitle1"
                  style={{
                    userSelect: "none",
                    fontSize: 16,
                    lineHeight: "19px",
                    width: "100%",
                    textAlign: "center",
                  }}
                >
                  Tháng {m.format("MM")}, {m.format("YYYY")}
                </Typography>
              </div>
            </td>
          </tr>
        </thead>
        <tbody>
          {allWeeks.map((w, i) => {
            return <tr key={`week${i} `}>{w}</tr>;
          })}
        </tbody>
      </table>
    );
  };
  const totalMonth = moment().format("DD") === "01" ? 12 : 13;
  return (
    <>
      {Array(totalMonth)
        .fill(0)
        .map((v, index) => (
          <div key={index}>
            {renderCalendar(
              moment()
                .add(index, "months")
                .startOf("month")
            )}
          </div>
        ))}
    </>
  );
};

export default Calendar;
