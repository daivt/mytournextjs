import React from "react";
import styled from "styled-components";
import { createStyles, makeStyles, useTheme } from "@material-ui/core/styles";
import moment from "moment";
import {
  Box,
  Dialog,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Slide,
  FormControlLabel,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import ButtonComponent from "@src/button/Button";
import ShowDateView from "@components/mobile/hotels/ShowDateView";
import SwitchCustom from "@components/common/switchCustom/SwitchCustom";
import { listString } from "@utils/constants";
import Calendar from "./Calendar";

const useStyles = makeStyles((theme) =>
  createStyles({
    appBar: {
      position: "fixed",
      top: 0,
      borderRadius: 0,
      background: "white",
      color: "#1A202C",
      boxShadow: "none",
    },
    title: {
      width: "calc(100% - 52px)",
      textAlign: "center",
      fontSize: 16,
      lineHeight: "19px",
    },
    dayText: {
      color: "#718096",
      fontSize: 12,
      lineHeight: "14px",
      height: "100%",
      justifyContent: "center",
      display: "flex",
      alignItems: "center",
    },
    flightTitle: {
      fontSize: 12,
      lineHeight: "14px",
      fontWeight: 400,
      marginBottom: 6,
    },
    flightDayText: {
      fontSize: 18,
      lineHeight: "22px",
      fontWeight: 600,
    },
  })
);
const Head = styled.div`
  height: 30px;
  text-align: center;
  width: calc(100% / 7);
  background: #edf2f7;
`;
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const CalendarDialog = (props) => {
  const {
    child,
    type,
    updateStartDate,
    updateEndDate,
    updateFullDate,
    initStartDate,
    initEndDate,
    isSingle,
  } = props;
  const theme = useTheme();
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [isLowest, setLowest] = React.useState(true);
  const [startDate, setStartDate] = React.useState(initStartDate);
  const [endDate, setEndDate] = React.useState(initEndDate);

  const handleClickOpen = () => {
    setStartDate(initStartDate);
    setEndDate(initEndDate);
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleSubmit = (day) => {
    if (updateStartDate) updateStartDate(day);
    if (updateEndDate) updateEndDate(day);
    setOpen(false);
  };
  const handleSubmitFullDate = (start, end) => {
    if (updateStartDate) updateStartDate(start);
    if (updateEndDate) updateEndDate(end);
    if (updateFullDate) updateFullDate(start, end);
    setOpen(false);
  };
  const getNumberNight = () => {
    return Math.abs(
      moment(startDate)
        .startOf("day")
        .diff(endDate.startOf("day"), "days")
    );
  };
  return (
    <>
      <span onClick={handleClickOpen} aria-hidden="true">
        {child}
      </span>
      <Dialog
        fullScreen
        maxWidth="sm"
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <div
          style={{ display: "flex", flexDirection: "column", paddingTop: 86 }}
        >
          <AppBar className={classes.appBar}>
            <Toolbar>
              <IconButton
                edge="start"
                color="inherit"
                onClick={handleClose}
                aria-label="close"
              >
                <CloseIcon style={{ fontSize: 24 }} />
              </IconButton>
              <Typography variant="h6" className={classes.title}>
                Chọn ngày
              </Typography>
            </Toolbar>
            {type === "flight" && (
              <div style={{ display: "flex", padding: "12px 16px" }}>
                <div style={{ width: "50%" }}>
                  {startDate && (
                    <>
                      <Typography
                        variant="body2"
                        className={classes.flightTitle}
                      >
                        {listString.IDS_TEXT_DAY_OUT}
                      </Typography>
                      <Typography
                        variant="body2"
                        className={classes.flightDayText}
                      >
                        {startDate.locale("vi_VN").format("ddd")},&nbsp;
                        {startDate.format("DD")} Thg&nbsp;
                        {startDate.format("MM")}
                      </Typography>
                    </>
                  )}
                </div>
                <div style={{ width: "50%", textAlign: "right" }}>
                  {endDate && (
                    <>
                      <Typography
                        variant="body2"
                        className={classes.flightTitle}
                      >
                        {listString.IDS_TEXT_DAY_IN}
                      </Typography>
                      <Typography
                        variant="body2"
                        className={classes.flightDayText}
                      >
                        {endDate.locale("vi_VN").format("ddd")},&nbsp;
                        {endDate.format("DD")} Thg&nbsp;
                        {endDate.format("MM")}
                      </Typography>
                    </>
                  )}
                </div>
              </div>
            )}
            <div style={{ display: "flex", width: "100%" }}>
              {Array(7)
                .fill(0)
                .map((v, index) => (
                  <Head key={index}>
                    <Typography variant="body2" className={classes.dayText}>
                      {moment()
                        .day(index + 1)
                        .locale("vi_VN")
                        .format("ddd")
                        .replace("T", "Thứ ")
                        .replace("CN", "Chủ Nhật")}
                    </Typography>
                  </Head>
                ))}
            </div>
          </AppBar>
          <Box pb={135 / 8}>
            <Calendar
              isSingle={isSingle}
              type={type}
              startDate={startDate}
              endDate={endDate}
              setStartDate={(day) => {
                setStartDate(day);
                if (isSingle) handleSubmit(day);
              }}
              setEndDate={(day) => {
                setEndDate(day);
                if (isSingle) handleSubmit(day);
              }}
              handleSubmitFlight={handleSubmitFullDate}
            />
          </Box>

          <Box
            px={2}
            pb={1}
            borderTop="1px solid #EDF2F7"
            style={{
              position: "fixed",
              bottom: 0,
              background: "white",
              width: "100%",
            }}
          >
            {type === "flight" ? (
              <div style={{ display: "flex", alignItems: "center" }}>
                <Typography
                  variant="body2"
                  style={{ width: "100%", fontSize: 14, lineHeight: "17px" }}
                >
                  Xem giá ước tính thấp nhất
                </Typography>
                <FormControlLabel
                  control={
                    <SwitchCustom
                      checked={isLowest}
                      onChange={(e) => setLowest(e.target.checked)}
                      name="lowest"
                    />
                  }
                  label=""
                />
              </div>
            ) : (
              <>
                <Box display="flex" justifyContent="center">
                  <ShowDateView
                    isShowTitle
                    startDate={startDate}
                    endDate={endDate}
                  />
                </Box>
                <Box pt={12 / 8}>
                  <ButtonComponent
                    backgroundColor={theme.palette.secondary.main}
                    height={48}
                    fontSize={16}
                    fontWeight={600}
                    borderRadius={8}
                    disabled={!(startDate && endDate)}
                    handleClick={() => handleSubmitFullDate(startDate, endDate)}
                  >
                    {!(startDate && endDate)
                      ? "Chọn"
                      : `Chọn (${getNumberNight()} đêm)`}
                  </ButtonComponent>
                </Box>
              </>
            )}
          </Box>
        </div>
      </Dialog>
    </>
  );
};
export default CalendarDialog;
