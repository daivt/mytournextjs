import {
  Backdrop,
  Box,
  Fade,
  makeStyles,
  Modal,
  Typography,
} from "@material-ui/core";
import { IconChevronRight, IconMobile, IconMobileHotline } from "@public/icons";
import { listString } from "@utils/constants";
import clsx from "clsx";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  wrapHotline: {
    display: "flex",
    padding: "12px 16px",
    borderRadius: 8,
    background: theme.palette.gray.grayLight22,
  },
  center: {
    display: "flex",
    flexDirection: "column",
    margin: "0 18px",
  },
  title: {
    color: theme.palette.black.black3,
    marginBottom: 3,
  },
  desc: {
    color: theme.palette.gray.grayDark8,
  },
  iconRight: {
    display: "flex",
    alignItems: "center",
  },
  iconLeft: {
    display: "flex",
    alignItems: "center",
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    outline: "none",
    backgroundColor: theme.palette.white.main,
    borderRadius: 16,
    width: 343,
  },
  boxItem: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  hideBorder: {
    borderBottom: "none",
  },
  boxRight: {
    display: "flex",
    flexDirection: "column",
    margin: "18px 0",
  },
  boxLeft: {
    margin: "0 20px",
  },
  leftTitle: {
    color: theme.palette.black.black3,
  },
  leftDesc: {
    color: theme.palette.blue.blueLight8,
  },
  iconBtn: {
    padding: 0,
  },
}));
const listHotline = [
  {
    id: "0",
    icon: <IconMobileHotline />,
    title: listString.IDS_MT_HOTLINE_TEXT_HN,
    desc: listString.IDS_MT_TEXT_HOTLINE_HN,
  },
  {
    id: "1",
    icon: <IconMobileHotline />,
    title: listString.IDS_MT_HOTLINE_TEXT_HCM,
    desc: listString.IDS_MT_TEXT_HOTLINE_HCM,
  },
  {
    id: "2",
    icon: <IconMobileHotline />,
    title: listString.IDS_MT_TEXT_SUPPORT_ODER_FLIGHT,
    desc: listString.IDS_MT_TEXT_HOTLINE_SUPPORT_ODER_FLIGHT,
  },
];

const HotlineBox = ({ className = "" }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <Box
        onClick={handleOpen}
        className={clsx(classes.wrapHotline, className)}
        mb={4}
      >
        <Box className={classes.iconLeft}>
          <IconMobile />
        </Box>
        <Box className={classes.center}>
          <Typography className={classes.title} variant="subtitle1">
            {listString.IDS_TEXT_HOTLINE_BOX_TITLE}
          </Typography>
          <Typography className={classes.desc} variant="body2">
            {listString.IDS_TEXT_HOTLINE_BOX_DESC}
          </Typography>
        </Box>
        <Box className={classes.iconRight}>
          <IconChevronRight />
        </Box>
      </Box>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box className={classes.paper}>
            {listHotline.map((item, index) => (
              <Box
                className={clsx(
                  classes.boxItem,
                  index === listHotline.length - 1 && classes.hideBorder
                )}
                key={item.id}
              >
                <Box className={classes.boxLeft}>{item.icon}</Box>
                <Box className={classes.boxRight}>
                  <Typography className={classes.leftTitle} variant="body1">
                    {item.title}
                  </Typography>
                  <Typography className={classes.leftDesc} variant="body1">
                    {item.desc}
                  </Typography>
                </Box>
              </Box>
            ))}
          </Box>
        </Fade>
      </Modal>
    </>
  );
};

export default HotlineBox;
