import { makeStyles, Typography } from "@material-ui/core";

import { routeStatic } from "utils/constants";

import Link from "@src/link/Link";
import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  linkHotelDetail: {
    textDecoration: "none !important",
  },
  hotelImage: {
    height: 96,
    width: 96,
    borderRadius: 8,
  },
  hotelName: {
    margin: "8px 0 0 0",
    color: theme.palette.black.black3,
    width: 96,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    lineHeight: "17px",
  },
  localtion: {
    color: theme.palette.gray.grayDark7,
    marginTop: 6,
    fontWeight: 400,
  },
}));

const HotelLastViewCard = ({ item = {} }) => {
  const classes = useStyles();

  return (
    <Link
      href={{
        pathname: routeStatic.HOTEL_DETAIL.href,
        query: {
          alias: `${item.id}-${item?.hotelName.stringSlug()}.html`,
        },
      }}
      className={classes.linkHotelDetail}
    >
      <Image
        srcImage={item?.hotelImage}
        className={classes.hotelImage}
        borderRadiusProp="8px"
      />
      <Typography variant="subtitle2" className={classes.hotelName}>
        {item?.hotelName}
      </Typography>
      <Typography
        variant="body2"
        component="span"
        className={classes.localtion}
      >
        {item?.localtion}
      </Typography>
    </Link>
  );
};

export default HotelLastViewCard;
