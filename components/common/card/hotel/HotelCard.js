import { memo } from "react";
import cookie from "js-cookie";
import PropTypes from "prop-types";
import { useSnackbar } from "notistack";
import { useEffect, useState } from "react";
import { Box, makeStyles, Typography, IconButton } from "@material-ui/core";

import { addFavorite, removeFavorite } from "@api/user";
import { isEmpty, areEqualItem } from "@utils/helpers";
import { IconHeart, IconStar, IconGroupUser } from "@public/icons";
import {
  TOKEN,
  routeStatic,
  LAST_HOTEL_FAVORITES,
  listIcons,
  prefixUrlIcon,
} from "utils/constants";

import Link from "@src/link/Link";
import Image from "@src/image/Image";
import snackbarSetting from "@src/alert/Alert";

const useStyles = makeStyles((theme) => ({
  linkHotelDetail: {
    textDecoration: "none !important",
  },
  wrapCardHotel: {
    position: "relative",
    overflow: "hidden",
    width: "100%",
    borderRadius: 8,
  },
  imageHotel: {
    width: "100%",
    height: 158,
    borderRadius: 8,
  },
  discountPercent: {
    width: 80,
    height: 80,
    borderRadius: "50%",
    background: "rgba(255,18,132,0.9)",
    position: "absolute",
    top: -35,
    left: -23,
  },
  discountPercentNumber: {
    display: "flex",
    alignItems: "center",
    color: theme.palette.white.main,
    marginTop: 46.5,
    marginLeft: 28,
    position: "absolute",
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
  },
  discountPrice: {
    width: 85,
    background: "rgba(26, 32, 44, 0.7)",
    borderRadius: "0px 8px",
    position: "absolute",
    bottom: 0,
    margin: "6px 6px",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "center",
    height: 38,
  },
  subPrice: {
    textDecorationLine: "line-through",
    color: theme.palette.white.main,
    opacity: 0.8,
    marginLeft: 8,
    marginTop: 4,
    fontSize: 12,
  },
  mainPrice: {
    color: theme.palette.white.main,
    margin: "0 8px",
    fontSize: 14,
  },
  iconHeart: {
    position: "absolute",
    zIndex: 2,
    top: 8,
    right: 8,
  },
  wrapBtn: {
    padding: 0,
  },
  wrapHotelInfo: {},
  hotelName: {
    color: theme.palette.black.black3,
    width: 158,
    marginTop: 8,
    fontSize: 14,
    lineHeight: "17px",
    fontWeight: 600,
    display: "-webkit-box",
    WebkitLineClamp: 3,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
  },
  point: {
    height: 18,
    width: 27,
    fontWeight: 600,
    color: theme.palette.white.main,
    background: theme.palette.blue.blueLight8,
    padding: "2px 4px",
    borderRadius: 4,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    fontSize: 12,
    lineHeight: "14px",
  },
  rateLevel: {
    fontSize: 12,
    marginLeft: 4,
    lineHeight: "14px",
    fontWeight: 400,
    whiteSpace: "nowrap",
  },
  wrapPoint: {
    display: "flex",
    alignItems: "center",
  },
  location: {
    color: theme.palette.black.black3,
    margin: "4px 0",
  },
  wrapCountBooking: {
    width: 90,
    height: 18,
    border: `1px solid ${theme.palette.green.greenLight7}`,
    color: theme.palette.green.greenLight7,
    borderRadius: 4,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 12,
  },
  countNumberBooking: {
    marginLeft: 4.7,
    paddingTop: 2,
    paddingRight: 4,
  },
  discountPriceFavorite: {
    color: theme.palette.black.black3,
    display: "flex",
    fontSize: 14,
  },
  wrapDiscountPriceFavorite: {
    display: "flex",
    marginTop: 4,
    alignItems: "center",
  },
  discountSubPriceFavorite: {
    textDecorationLine: "line-through",
    marginLeft: 4,
    lineHeight: "13px",
  },
  wrapFlagTopHotel: {
    background: theme.palette.red.redLight5,
    fontSize: 14,
    height: 24,
    position: "absolute",
    bottom: 6,
    left: 6,
    color: "#FFF",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    lineHeight: "16px",
    fontWeight: 600,
    padding: "3.5px 4px 3.5px 4px",
  },
  favorite: {
    fill: "#FF1284",
    stroke: theme.palette.white.main,
  },
  notFavorite: {
    fill: "rgba(0, 0, 0, 0.6)",
    stroke: theme.palette.white.main,
  },
  wrapRatingStar: {
    marginTop: 5,
    marginBottom: 7,
    display: "flex",
  },
  rankingHotel: {
    stroke: theme.palette.yellow.yellowLight3,
    fill: theme.palette.yellow.yellowLight3,
    width: 12,
    height: 12,
    marginRight: 3,
  },
  iconTripadvisor: {
    width: 18,
    height: 18,
  },
}));
const arrStar = [1, 2, 3, 4, 5];
const HotelCard = ({
  item = {},
  isShowStatus = false,
  isHotelInterest = true,
  isCountBooking = true,
  isShowDiscountPercent = true,
  isShowPriceInImage = true,
  isShowDistance = false,
}) => {
  const classes = useStyles();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [isFavorite, setIsFavorite] = useState(item.isFavoriteHotel);

  useEffect(() => {
    checkFavoriteHotel();
  }, []);

  useEffect(() => {
    setIsFavorite(item.isFavoriteHotel);
  }, [item]);

  const checkFavoriteHotel = () => {
    const listHotelFavorites =
      JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
    if (isEmpty(cookie.get(TOKEN)) && !isEmpty(listHotelFavorites)) {
      setIsFavorite(listHotelFavorites.includes(item.id));
    }
  };

  const handleFavorite = (event) => {
    event.preventDefault();
    if (!isEmpty(cookie.get(TOKEN))) {
      changeFavorite();
    } else {
      let listHotelFavoritesTemp =
        JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
      if (isFavorite) {
        listHotelFavoritesTemp = listHotelFavoritesTemp.filter(
          (el) => el !== item.id
        );
      } else {
        listHotelFavoritesTemp.unshift(item.id);
      }
      localStorage.setItem(
        LAST_HOTEL_FAVORITES,
        JSON.stringify(listHotelFavoritesTemp)
      );
    }
    setIsFavorite(!isFavorite);
  };

  const changeFavorite = async () => {
    try {
      if (isFavorite) {
        const { data } = await removeFavorite({ hotelId: item.id });
        if (data.code === 200) {
          setIsFavorite(!isFavorite);
        } else {
          data?.message &&
            enqueueSnackbar(
              data?.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
        }
      } else {
        const { data } = await addFavorite({ hotelId: item.id });
        if (data.code === 200) {
          setIsFavorite(!isFavorite);
        } else {
          data?.message &&
            enqueueSnackbar(
              data?.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
        }
      }
    } catch (error) {}
  };

  return (
    <Link
      href={{
        pathname: routeStatic.HOTEL_DETAIL.href,
        query: {
          alias: `${item.id}-${item?.hotelName?.stringSlug()}.html`,
        },
      }}
      className={classes.linkHotelDetail}
    >
      <Box className={classes.wrapCardHotel}>
        <Image
          srcImage={item?.imageHotel}
          className={classes.imageHotel}
          borderRadiusProp="8px"
        />
        {isShowDiscountPercent &&
          !isEmpty(item.discountPercent) &&
          !item.hiddenPrice && (
            <Box className={classes.discountPercent}>
              <Box component="span" className={classes.discountPercentNumber}>
                {`-${item?.discountPercent}`}
                <Box component="span" fontSize={12}>
                  %
                </Box>
              </Box>
            </Box>
          )}

        {isShowPriceInImage ? (
          isShowStatus ? (
            <Box className={classes.wrapFlagTopHotel}>
              <Box component="span">{item?.flagTopHotel}</Box>
            </Box>
          ) : (
            <>
              {!item.hiddenPrice && !isEmpty(item?.mainPrice) && (
                <Box className={classes.discountPrice}>
                  {!isEmpty(item?.subPrice) && (
                    <Typography
                      variant="body2"
                      component="span"
                      className={classes.subPrice}
                    >
                      {item?.subPrice.formatMoney()}đ
                    </Typography>
                  )}
                  {!isEmpty(item?.mainPrice) && (
                    <Typography
                      variant="subtitle1"
                      component="span"
                      className={classes.mainPrice}
                    >
                      {item?.mainPrice.formatMoney()}đ
                    </Typography>
                  )}
                </Box>
              )}
            </>
          )
        ) : null}

        {isHotelInterest && (
          <Box className={classes.iconHeart}>
            <IconButton onClick={handleFavorite} className={classes.wrapBtn}>
              <IconHeart
                className={`svgFillAll ${
                  isFavorite ? classes.favorite : classes.notFavorite
                }`}
              />
            </IconButton>
          </Box>
        )}
      </Box>
      <Box className={classes.wrapHotelInfo}>
        <Typography variant="subtitle2" className={classes.hotelName}>
          {item?.hotelName}
        </Typography>
        <Box className={classes.wrapRatingStar}>
          {arrStar.map((el, index) => {
            if (el > item.rating) return null;
            return (
              <IconStar
                key={index}
                className={`svgFillAll ${classes.rankingHotel}`}
              />
            );
          })}
        </Box>
        <Box className={classes.wrapPoint}>
          {!isEmpty(item.point) && (
            <Typography className={classes.point}>
              {item?.point === 10 ? 10 : item?.point?.toFixed(1)}
            </Typography>
          )}

          <Box
            component="span"
            className={classes.rateLevel}
            color="black.black3"
          >
            {item?.rateLevel}
          </Box>
          {item?.countRating > 0 && (
            <Box
              component="span"
              className={classes.rateLevel}
              color="gray.grayDark8"
            >
              ({item?.countRating || ""})
            </Box>
          )}
          {!isEmpty(item?.taRating) && (
            <Box component="span" display="flex" alignItems="center" pl={4 / 8}>
              <Image
                srcImage={`${prefixUrlIcon}${listIcons.IconTripadvisor}`}
                className={classes.iconTripadvisor}
              />
              <Box component="span" pl={4 / 8} fontSize={12}>
                {2 * item?.taRating === 10
                  ? 10
                  : (2 * item?.taRating)?.toFixed(1)}
              </Box>
            </Box>
          )}
        </Box>
        {isShowDistance ? (
          <Box
            fontSize={12}
            lineHeight="14px"
            color="gray.grayDark7"
            my={4 / 8}
          >
            {!isEmpty(item.distanceToSearchLocation)
              ? `Cách bạn ${item?.distanceToSearchLocation?.toFixed(1)}km`
              : ""}
          </Box>
        ) : (
          <Typography variant="body2" className={classes.location}>
            {item?.location}
          </Typography>
        )}

        {/* {isCountBooking && (
          <Box className={classes.wrapCountBooking}>
            <IconGroupUser />
            <Typography
              variant="body2"
              component="span"
              className={classes.countNumberBooking}
            >
              {item?.countBooking} Đã đặt
            </Typography>
          </Box>
        )} */}
        {(!isShowPriceInImage || isShowStatus) && (
          <Box className={classes.wrapDiscountPriceFavorite}>
            {!isEmpty(item?.mainPrice) && (
              <Box
                color="black.black3"
                fontWeight={600}
                fontSize={16}
                className={classes.discountPriceFavorite}
              >
                {`${item?.mainPrice.formatMoney()}đ`}
              </Box>
            )}

            {!isEmpty(item.discountPercent) && (
              <Box
                color="#718096"
                fontSize={11}
                className={classes.discountSubPriceFavorite}
              >
                {`${item?.subPrice.formatMoney()}đ`}
              </Box>
            )}
          </Box>
        )}
      </Box>
    </Link>
  );
};

HotelCard.propTypes = {
  isTopSale: PropTypes.bool,
  isHotelInterest: PropTypes.bool,
  isCountBooking: PropTypes.bool,
  isShowStatus: PropTypes.bool,
};

export default memo(HotelCard, areEqualItem);
