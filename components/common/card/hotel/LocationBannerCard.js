import clsx from "clsx";
import { Box, makeStyles, Typography } from "@material-ui/core";

import { routeStatic } from "@utils/constants";

import Link from "@src/link/Link";
import Image from "@src/image/Image";

const initItem = {
  name: "Đà Nẵng",
  numHotels: 1045,
  thumb:
    "https://staticproxy.mytourcdn.com/0x0,q90/resources/pictures/cities/country1606212327.jpg",
  avgPrice: 570000,
};

const useStyles = makeStyles((theme) => ({
  linkBanner: {
    width: "100%",
    height: "100%",
  },
  wrap: {
    position: "absolute",
    left: 0,
    bottom: 0,
    width: "100%",
    background:
      "linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.7) 100%)",
  },
  wrapLocation: {
    position: "relative",
    width: "100%",
    height: "100%",
  },
  imageLocation: {
    width: "100%",
    height: "100%",
    position: "relative",
    borderRadius: 8,
  },
  wrapInfoLocation: {
    position: "relative",
    color: theme.palette.white.main,
    display: "flex",
    flexDirection: "column",
    padding: "0 0 12px 12px",
  },
  countHotel: {
    fontWeight: "normal",
    marginTop: 4,
  },
  wrapAvgPrice: {
    color: theme.palette.white.main,
    marginRight: 16,
    fontSize: 12,
  },
}));

const LocationBannerCard = ({
  item = initItem,
  styleImageBanner = "",
  isHome = true,
}) => {
  const classes = useStyles();
  return (
    <Link
      href={{
        pathname: routeStatic.LISTING_HOTEL.href,
        query: {
          slug: [
            `${item.aliasCode}`,
            `khach-san-tai-${item?.name?.stringSlug()}.html`,
          ],
        },
      }}
      className={classes.linkBanner}
    >
      <Box className={classes.wrapLocation}>
        <Image
          srcImage={item?.thumb}
          className={clsx(classes.imageLocation, styleImageBanner)}
          title={`Khạch sạn tại ${item?.name}`}
          alt={`Khạch sạn tại ${item?.name}`}
          borderRadiusProp="8px"
        />
        <Box
          className={classes.wrap}
          style={{ borderRadius: isHome ? "0 0 8px 8px" : "0px" }}
        >
          <Box className={classes.wrapInfoLocation}>
            <Typography variant="subtitle1" component="span">
              {item?.name}
            </Typography>
            <Typography
              variant="subtitle2"
              component="span"
              className={classes.countHotel}
            >
              {item?.numHotels} khách sạn
            </Typography>
          </Box>
        </Box>
      </Box>
    </Link>
  );
};
export default LocationBannerCard;
