import { Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { IconLocation, IconPlane, IconSeat16 } from "@public/icons";
import { durationMillisecondToHour } from "@utils/helpers";
import { DATE_FORMAT } from "@utils/moment";
import moment from "moment";
import styled from "styled-components";
import { getTicketClass, Policy, getStops } from "utils/helpers";
import { listString } from "@utils/constants";

export const BoxLogoAirline = styled.div`
  display: flex;
  flex-flow: column;
  text-align: center;
  align-items: center;
`;

export const BoxInfoFlight = styled.div`
  display: flex;
  align-items: start;
`;

const Row = styled.div`
  display: flex;
  align-items: center;
`;

const Col = styled.div`
  display: flex;
  flex-direction: column;
`;

export const GREY_500 = "#9e9e9e";

const FlightTicketDetail = (props) => {
  const {
    ticket,
    data,
    airlineInfo,
    title,
    isSimple,
    isSeat,
    isSlider,
    title2Ways,
  } = props;

  const theme = useTheme();

  const onTicket = true;
  const ticketClass = data?.ticketClasses;

  const flightTime = ticket?.duration;
  const aircraft = ticket?.ticketdetail?.facilities?.find(
    (f) => f.code === "AIRCRAFT_INFO"
  );

  if (!ticket) {
    return <div />;
  }
  return (
    <div
      style={{
        background: isSlider ? theme.palette.white.main : undefined,
        padding: isSlider ? 12 : 0,
        borderRadius: isSlider ? "8px 8px 0px 0px" : undefined,
      }}
    >
      <Typography variant="subtitle1" style={{ marginBottom: 12 }}>
        <span>{title}</span>
      </Typography>

      {title2Ways && (
        <Typography
          variant="subtitle2"
          style={{
            backgroundColor: theme.palette.primary.main,
            color: theme.palette.white.main,
            width: 80,
            textAlign: "center",
            marginBottom: 12,
            padding: 4,
            borderRadius: 4,
          }}
        >
          {title2Ways}
        </Typography>
      )}
      <div
        style={{
          display: "flex",
        }}
      >
        <BoxInfoFlight style={{ flex: 1.5 }}>
          <Col
            style={{
              alignItems: "center",
              height: "100%",
              justifyContent: "space-between",
            }}
          >
            <Col style={{ minWidth: 80, marginRight: 8, textAlign: "end" }}>
              <Typography variant="subtitle1">
                <span>{ticket.departureTimeStr}</span>
              </Typography>
              <Typography variant="caption" color="textSecondary">
                {moment(ticket.departureDayStr, DATE_FORMAT).format("L")}
              </Typography>
            </Col>
            <Row style={{ padding: "8px 0px" }}>
              <Typography variant="subtitle2" color="textSecondary">
                {durationMillisecondToHour(flightTime)}
              </Typography>
            </Row>
            <Col style={{ minWidth: 80, marginRight: 8, textAlign: "end" }}>
              <Typography variant="subtitle1">
                <span>{ticket.arrivalTimeStr}</span>
              </Typography>
              <Typography variant="caption" color="textSecondary">
                {moment(ticket.arrivalDayStr, DATE_FORMAT).format("L")}
              </Typography>
            </Col>
          </Col>
          <Col
            style={{
              alignItems: "center",
              height: "100%",
              justifyContent: "space-between",
              paddingBottom: 28,
            }}
          >
            <IconPlane
              className="svgFillAll"
              style={{
                height: "32px",
                width: "16px",
                fill: theme.palette.blue.blueLight8,
                marginBottom: 4,
              }}
            />
            <div
              style={{
                borderLeft: `1px solid ${theme.palette.blue.blueLight8}`,
                height: "100%",
                paddingBottom: 24,
                width: 1,
                paddingTop: 4,
              }}
            />
            <IconLocation
              className="svgFillAll"
              style={{
                height: "32px",
                width: "16px",
                fill: theme.palette.blue.blueLight8,
              }}
            />
          </Col>
          <Col
            style={{
              height: "100%",
              justifyContent: "space-between",
              marginLeft: 8,
            }}
          >
            <Col>
              <Typography variant="subtitle1">
                {ticket.departureCity}
                &nbsp;&#40;
                {ticket.departureAirport}
                &#41;
              </Typography>
              <Typography variant="caption" color="textSecondary">
                {ticket.departureAirportName}
              </Typography>
            </Col>
            <BoxLogoAirline
              style={{
                alignItems: onTicket ? "flex-start" : undefined,
                marginTop: 8,
              }}
            >
              <Row style={{ alignItems: "center", marginBottom: 8 }}>
                {!ticket.ticketdetail.numStops && (
                  <img
                    alt=""
                    style={{ width: "32px" }}
                    src={airlineInfo.logo}
                  />
                )}
                <Col style={{ textAlign: "left", marginLeft: 8 }}>
                  {!ticket.ticketdetail.numStops && (
                    <Typography variant="subtitle2">
                      {airlineInfo.name}
                    </Typography>
                  )}
                  {ticket?.operatingAirlineId && (
                    <Typography
                      component={"span"}
                      variant="body2"
                      style={{
                        color: theme.palette.secondary.main,
                        margin: "4px 0px",
                      }}
                    >
                      {ticket?.operatingAirlineId ? "Bay Pacific Airlines" : ""}
                    </Typography>
                  )}
                  {aircraft && (
                    <Row style={{ alignItems: "center" }}>
                      <Typography variant="body2">
                        {ticket.flightNumber}
                      </Typography>
                      &nbsp; &bull; &nbsp;
                      <Typography variant="body2">
                        {aircraft.description.split("-")[0]}
                      </Typography>
                    </Row>
                  )}
                  {!!ticket.ticketdetail.numStops && (
                    <Typography variant="body2" style={{ marginLeft: -8 }}>{`${
                      ticket.ticketdetail.numStops
                    } ${listString.IDS_TEXT_NUM_STOPS_1} ${getStops(ticket)
                      .map((ele, i) => `${ele.code} (${ele.city})`)
                      .join(",")}`}</Typography>
                  )}
                </Col>
              </Row>
              {isSeat && (
                <Row
                  style={{
                    margin: "8px 0px 4px",
                  }}
                >
                  <IconSeat16
                    style={{
                      marginRight: "4px",
                    }}
                  />
                  <Typography variant="body2">
                    {getTicketClass(ticket, ticketClass)}
                  </Typography>
                  <div
                    style={{
                      height: "18px",
                      marginLeft: "5px",
                      minWidth: "18px",
                      overflow: "hidden",
                      alignItems: "center",
                      borderRadius: "2px",
                      padding: "0 5px",
                      display: "flex",
                      justifyContent: "center",
                      color: "white",
                      backgroundColor: theme.palette.primary.main,
                      fontWeight: 500,
                      fontSize: "14px",
                    }}
                  >
                    {ticket.ticketdetail.detailTicketClass}
                  </div>
                </Row>
              )}
            </BoxLogoAirline>
            {!isSimple && (
              <div
                style={{
                  display: "flex",
                  flexWrap: "wrap",
                  padding: "4px 0px",
                  flex: 1.5,
                  flexFlow: "column",
                }}
              >
                {ticket.ticketdetail?.polices &&
                  ticket.ticketdetail.polices?.map((item, index) => (
                    <Policy item={item} key={index} isMobile={true} />
                  ))}
              </div>
            )}
            <Col>
              <Typography
                variant="subtitle1"
                style={{ marginTop: !isSimple ? 4 : 16 }}
              >
                {ticket.arrivalCity}
                &nbsp;&#40;
                {ticket.arrivalAirport}
                &#41;
              </Typography>
              <Typography variant="caption" color="textSecondary">
                {ticket.arrivalAirportName}
              </Typography>
            </Col>
          </Col>
        </BoxInfoFlight>
      </div>
    </div>
  );
};

export default FlightTicketDetail;
