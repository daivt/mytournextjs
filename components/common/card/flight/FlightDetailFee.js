import { Divider, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import { listString } from "@utils/constants";
import { useRouter } from "next/router";
import styled from "styled-components";
import { isEmpty } from "utils/helpers";

export const BoxLogoAirline = styled.div`
  display: flex;
  flex-flow: column;
  text-align: center;
  align-items: center;
`;

export const BoxInfoFlight = styled.div`
  display: flex;
  align-items: start;
`;

const Row = styled.div`
  display: flex;
  align-items: center;
`;

const Col = styled.div`
  display: flex;
  flex-direction: column;
`;

export const GREY_500 = "#9e9e9e";

const useStyles = makeStyles((theme) => ({
  footer: {
    position: "fixed",
    backgroundColor: theme.palette.white.main,
    zIndex: 20,
    bottom: 0,
    width: "93%",
    marginLeft: "-4px",
    borderTop: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  button: {
    marginTop: 8,
    width: "100%",
    height: 48,
    color: "#fff",
    fontWeight: "bold",
    backgroundColor: theme.palette.secondary.main,
    "& i": {
      paddingRight: 5,
    },
    "&:hover": {
      backgroundColor: theme.palette.secondary.main,
    },
  },
}));

const FlightDetailFee = (props) => {
  const { ticket, ticketInBound, ticketPersion, data, title } = props;
  const theme = useTheme();
  const classes = useStyles();
  const router = useRouter();

  const { numAdults, numChildren, numInfants } = data?.searchRequest;
  const { farePrice, grandTotal } = ticket.ticketdetail;
  const currency = "đ";
  const finalPrice =
    parseInt(grandTotal) +
    (ticketInBound ? parseInt(ticketInBound?.ticketdetail?.grandTotal) : 0);
  if (!ticket) {
    return <div />;
  }

  // New
  let detailPriceTicket = {};
  const isTwoWay = ticketInBound ? true : false;
  if (!isEmpty(ticket)) {
    detailPriceTicket = {
      outbound: {
        numAdult: ticketPersion?.numAdults,
        adultMoney: ticket?.ticketdetail?.farePrice,
        numChildren: ticketPersion?.numChildren,
        childMoney: ticket?.ticketdetail?.farePrice,
        numInfants: ticketPersion?.numInfants,
        babyMoney: ticket?.ticketdetail?.priceInfants,
        taxFee:
          parseInt(grandTotal) -
          parseInt(
            farePrice * (ticketPersion?.numAdults + ticketPersion?.numChildren)
          ),
        total: grandTotal,
      },
      inbound: ticketInBound
        ? {
            numAdult: ticketPersion?.numAdults,
            adultMoney: ticketInBound?.ticketdetail?.farePrice,
            numChildren: ticketPersion?.numChildren,
            childMoney: ticketInBound?.ticketdetail?.farePrice,
            numInfants: ticketPersion?.numInfants,
            babyMoney: ticketInBound?.ticketdetail?.priceInfants,
            taxFee:
              parseInt(ticketInBound?.ticketdetail?.grandTotal) -
              parseInt(
                ticketInBound?.ticketdetail?.farePrice *
                  (ticketPersion?.numAdults + ticketPersion?.numChildren)
              ),
            total: ticketInBound?.ticketdetail?.grandTotal,
          }
        : null,
      totalPayment: finalPrice,
    };
  }

  return (
    <>
      <Typography variant="subtitle1" style={{ marginTop: 8, marginBottom: 8 }}>
        <span>
          {title}&nbsp;
          {isTwoWay ? listString.IDS_MT_TEXT_DEPARTURE_LOWCASE : ""}
        </span>
      </Typography>
      <Col
        style={{
          height: "100%",
          justifyContent: "space-between",
        }}
      >
        {/* <Typography variant="subtitle1">
          <span>{totalPriceAfterDiscount.formatMoney()}</span>đ
        </Typography> */}
        {!!detailPriceTicket?.outbound?.numAdult && (
          <Row
            style={{
              justifyContent: "space-between",
              margin: "12px 0px",
            }}
          >
            <Typography variant="caption">
              {listString.IDS_MT_TEXT_ADULT} x{" "}
              {detailPriceTicket?.outbound?.numAdult}
            </Typography>
            <Typography variant="caption">
              {detailPriceTicket?.outbound?.adultMoney.formatMoney()}
              {currency}
            </Typography>
          </Row>
        )}
        <Divider />
        {!!detailPriceTicket?.outbound?.numChildren && (
          <>
            <Row
              style={{
                justifyContent: "space-between",
                margin: "12px 0px",
              }}
            >
              <Typography variant="caption">
                {listString.IDS_MT_TEXT_CHILDREN} x{" "}
                {detailPriceTicket?.outbound?.numChildren}
              </Typography>
              <Typography variant="caption">
                {detailPriceTicket?.outbound?.childMoney.formatMoney()}
                {currency}
              </Typography>
            </Row>
            <Divider />
          </>
        )}
        {!!detailPriceTicket?.outbound?.numInfants && (
          <>
            <Row
              style={{
                justifyContent: "space-between",
                margin: "12px 0px",
              }}
            >
              <Typography variant="caption">
                {listString.IDS_MT_TEXT_BABY} x{" "}
                {detailPriceTicket?.outbound?.numInfants}
              </Typography>
              <Typography variant="caption">
                {listString.IDS_MT_TEXT_FILTER_FREE}
              </Typography>
            </Row>
            <Divider />
          </>
        )}
        <Row
          style={{
            justifyContent: "space-between",
            margin: "12px 0px",
          }}
        >
          <Typography variant="caption">
            {listString.IDS_MT_TEXT_TAX_FEE}
          </Typography>
          <Typography variant="caption">
            {detailPriceTicket?.outbound?.taxFee.formatMoney()}
            {currency}
          </Typography>
        </Row>
        <Divider />
        <Row
          style={{
            justifyContent: "space-between",
            margin: "12px 0px",
          }}
        >
          <Typography variant="caption">
            {listString.IDS_MT_TEXT_TOTAL_PRICE_OUTBOUND}
          </Typography>
          <Typography variant="caption">
            {detailPriceTicket?.outbound?.total.formatMoney()}
            {currency}
          </Typography>
        </Row>
        {isTwoWay && (
          <>
            <Typography
              variant="subtitle1"
              style={{ marginTop: 8, marginBottom: 8 }}
            >
              <span>
                {title}&nbsp;
                {isTwoWay ? listString.IDS_MT_TEXT_RETURN_LOWCASE : ""}
              </span>
            </Typography>

            {!!detailPriceTicket?.inbound?.numAdult && (
              <Row
                style={{
                  justifyContent: "space-between",
                  margin: "12px 0px",
                }}
              >
                <Typography variant="caption">
                  {listString.IDS_MT_TEXT_ADULT} x{" "}
                  {detailPriceTicket?.inbound?.numAdult}
                </Typography>
                <Typography variant="caption">
                  {detailPriceTicket?.inbound?.adultMoney.formatMoney()}
                  {currency}
                </Typography>
              </Row>
            )}
            <Divider />
            {!!detailPriceTicket?.inbound?.numChildren && (
              <>
                <Row
                  style={{
                    justifyContent: "space-between",
                    margin: "12px 0px",
                  }}
                >
                  <Typography variant="caption">
                    {listString.IDS_MT_TEXT_CHILDREN} x{" "}
                    {detailPriceTicket?.inbound?.numChildren}
                  </Typography>
                  <Typography variant="caption">
                    {detailPriceTicket?.inbound?.childMoney.formatMoney()}
                    {currency}
                  </Typography>
                </Row>
                <Divider />
              </>
            )}
            {!!detailPriceTicket?.inbound?.numInfants && (
              <>
                <Row
                  style={{
                    justifyContent: "space-between",
                    margin: "12px 0px",
                  }}
                >
                  <Typography variant="caption">
                    {listString.IDS_MT_TEXT_BABY} x{" "}
                    {detailPriceTicket?.inbound?.numInfants}
                  </Typography>
                  <Typography variant="caption">
                    {listString.IDS_MT_TEXT_FILTER_FREE}
                  </Typography>
                </Row>
                <Divider />
              </>
            )}
            <Row
              style={{
                justifyContent: "space-between",
                margin: "12px 0px",
              }}
            >
              <Typography variant="caption">
                {listString.IDS_MT_TEXT_TAX_FEE}
              </Typography>
              <Typography variant="caption">
                {detailPriceTicket?.inbound?.taxFee.formatMoney()}
                {currency}
              </Typography>
            </Row>
            <Divider />
            <Row
              style={{
                justifyContent: "space-between",
                margin: "12px 0px",
              }}
            >
              <Typography variant="caption">
                {listString.IDS_MT_TEXT_TOTAL_PRICE_INBOUND}
              </Typography>
              <Typography variant="caption">
                {detailPriceTicket?.inbound?.total.formatMoney()}
                {currency}
              </Typography>
            </Row>
          </>
        )}
        <Row
          style={{
            justifyContent: "space-between",
            background: theme.palette.gray.grayLight22,
            alignItems: "flex-start",
            margin: "0px -16px 0px -24px",
          }}
        >
          <Col style={{ margin: "12px 16px" }}>
            <Typography variant="subtitle1">Tổng giá vé</Typography>
            <Typography variant="caption">
              {listString.IDS_MT_TEXT_INCLUDE_TAX_FEE_VAT}
            </Typography>
          </Col>
          <Typography variant="subtitle1" style={{ margin: "12px 16px" }}>
            {detailPriceTicket.totalPayment.formatMoney()}&nbsp;
            {currency}
          </Typography>
        </Row>
      </Col>
    </>
  );
};

export default FlightDetailFee;
