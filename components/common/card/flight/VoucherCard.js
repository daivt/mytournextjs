import { Box, Grid, makeStyles, Typography } from "@material-ui/core";
import { listString } from "@utils/constants";
import clsx from "clsx";
import { DATE_FORMAT } from "@utils/moment";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  wrapCard: {
    display: "flex",
    flexDirection: "column",
    padding: 12,
    borderLeft: `solid 5px ${theme.palette.blue.blueLight8}`,
    borderRadius: 8,
    minHeight: 108,
  },

  wrapDiscountPercent: {
    borderLeft: `1px dashed ${theme.palette.gray.grayLight25}`,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    color: theme.palette.blue.blueLight8,
    height: "100%",
    borderRadius: 8,
    cursor: "pointer",
  },

  circle: (props) => ({
    height: 21,
    width: 21,
    backgroundColor: props.bgCircle,
    borderRadius: "50%",
    position: "absolute",
    left: -10,
  }),
  containerDiscount: {
    position: "relative",
  },
  wraper: {
    borderRadius: 8,
    display: "flex",
    flexDirection: "row",
    backgroundColor: theme.palette.white.main,
  },
  brDefault: {
    border: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  activeVoucher: {
    backgroundColor: theme.palette.blue.blueLight11,
    "& .circle": {
      border: `solid 1px ${theme.palette.blue.blueLight8}`,
    },
    "& .voucherUsed": {
      borderTop: `solid 1px ${theme.palette.blue.blueLight8}`,
      borderBottom: `solid 1px ${theme.palette.blue.blueLight8}`,
      borderRight: `solid 1px ${theme.palette.blue.blueLight8}`,
    },
    "& .voucherSelected": {
      borderTop: `solid 1px ${theme.palette.blue.blueLight8}`,
      borderBottom: `solid 1px ${theme.palette.blue.blueLight8}`,
    },
  },
  expiredDate: {
    fontSize: 12,
    fontWeight: 400,
    lineHeight: "14px",
    color: theme.palette.gray.grayDark7,
    paddingTop: 8,
    position: "absolute",
    bottom: 12,
  },
  voucherCode: {
    color: theme.palette.gray.grayDark7,
    textTransform: "uppercase",
    paddingBottom: 6,
  },
  containerV: {
    overflow: "hidden",
    borderRadius: 8,
    position: "relative",
  },
}));
const VoucherCard = ({
  item = {},
  bgCircle = "",
  hasBorder = false,
  voucherUsing = {},
  handleSetVoucher = () => {},
}) => {
  const propsStyle = {
    bgCircle,
  };
  const classes = useStyles(propsStyle);
  return (
    <Box className={clsx(classes.wraper)}>
      <Grid
        container
        className={clsx(
          classes.containerV,
          voucherUsing?.id === item?.id && classes.activeVoucher,
          hasBorder && classes.brDefault
        )}
      >
        <Grid item lg={9} md={9} sm={9} xs={9}>
          <Box className={`voucherSelected ${classes.wrapCard}`}>
            <Typography variant="subtitle2" className={classes.voucherCode}>
              {item.code}
            </Typography>
            <Typography variant="subtitle1" className={classes.voucherDesc}>
              {item.description.length > 46
                ? `${item.description.slice(0, 46)} ...`
                : item.description}
            </Typography>
            <Typography variant="body2" className={classes.expiredDate}>
              {listString.IDS_TEXT_DISCOUNT_CARD_EXPIRED}:{" "}
              {moment(item.expired).format(DATE_FORMAT)}
            </Typography>
          </Box>
        </Grid>
        <Grid
          item
          lg={3}
          md={3}
          sm={3}
          xs={3}
          className={classes.containerDiscount}
        >
          <Box className={`circle ${classes.circle}`} top={-9}></Box>
          <Box
            className={`voucherUsed ${classes.wrapDiscountPercent}`}
            onClick={() => handleSetVoucher(item)}
          >
            {voucherUsing?.id === item?.id ? (
              <Typography variant="subtitle2">
                {listString.IDS_MT_TEXT_CANCEL_VN}
              </Typography>
            ) : (
              <Typography variant="subtitle2">
                {listString.IDS_TEXT_USE}
              </Typography>
            )}
          </Box>
          <Box className={`circle ${classes.circle}`} top="90%"></Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default VoucherCard;
