import { Box, Grid, makeStyles, Typography, useTheme } from "@material-ui/core";
import {
  IconArrowFlightLong,
  IconDot,
  IconFlightTransit,
  IconRunningOut,
} from "@public/icons";
import Image from "@src/image/Image";
import { listString } from "@utils/constants";
import { durationFlightTime, getStops } from "@utils/helpers";
import { DATE_FORMAT } from "@utils/moment";
import moment from "moment";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    borderRadius: 8,
    backgroundColor: theme.palette.white.main,
    padding: 12,
  },
  wrapCard: {
    display: "flex",
  },
  wrapCardLeft: {},
  wrapCardRight: {
    textAlign: "right",
  },
  airlineCode: {
    color: theme.palette.gray.grayDark7,
  },
  airlineFromTo: {
    color: theme.palette.black.black3,
    fontSize: 18,
    lineHeight: "22px",
    display: "flex",
  },
  flightTimeBox: {
    display: "flex",
    color: theme.palette.gray.grayDark7,
  },
  wrapFlight: {
    display: "flex",
    marginTop: 4,
    alignItems: "center",
  },
  airlineLogo1: {
    width: 18,
    height: 18,
    marginRight: 4,
    objectFit: "contain",
  },
  airlineLogo: {
    width: 20,
    height: 20,
    objectFit: "contain",
    paddingBottom: 1,
    marginRight: 8,
  },
  iconDot: {
    margin: 6,
  },
  airlineName: {
    color: theme.palette.black.black3,
    whiteSpace: "nowrap",
  },
  flightCode: {
    color: theme.palette.black.black3,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  warpAirBrand: {
    display: "flex",
    alignItems: "center",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    overflow: "hidden",
    marginLeft: 8,
    color: theme.palette.black.black3,
  },
  wrapDiscountPrice: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
  },
  iconArrow: {
    margin: "0 12px",
  },
  subPrice: {
    textDecorationLine: "line-through",
    textAlign: "right",
    color: theme.palette.gray.grayDark8,
    marginTop: 4,
  },
  mainPrice: {
    color: theme.palette.black.black3,
    textAlign: "right",
    fontSize: 18,
    lineHeight: "22px",
  },
  bestPriceBox_1: {
    padding: "2px 6px",
    display: "flex",
    alignItems: "center",
    border: `1px solid ${theme.palette.brown.brownLight1}`,
    fontSize: "14px",
    borderRadius: 4,
    marginBottom: "8px",
    color: theme.palette.brown.brownLight1,
  },
  bestPriceBox_2: {
    padding: "2px 6px",
    display: "flex",
    alignItems: "center",
    border: `1px solid ${theme.palette.green.greenLight6}`,
    fontSize: "14px",
    borderRadius: 4,
    marginBottom: "8px",
    color: theme.palette.green.greenLight6,
  },
  bestPriceBox_4: {
    padding: "2px 6px",
    display: "flex",
    alignItems: "center",
    border: `1px solid ${theme.palette.red.redDark5}`,
    fontSize: "14px",
    borderRadius: 4,
    marginBottom: "8px",
    color: theme.palette.red.redDark5,
  },
  runningOut: {
    display: "flex",
    justifyContent: "flex-end",
    height: 20,
    width: 96,
    border: "none",
    color: theme.palette.red.redLight5,
  },
}));

const TicketFlightCard = ({ item, airline }) => {
  const classes = useStyles();
  const theme = useTheme();
  let transitAirPortsStr = "";
  let transitStopsStr = "";
  if (item.ticketdetail?.transitTickets) {
    transitAirPortsStr = item.ticketdetail.transitTickets
      .map((el) => {
        return el.facilities?.length
          ? getAirtCraftInfo(el.facilities)
          : undefined;
      })
      .join(", ");
    transitStopsStr = item.ticketdetail.transitTickets
      .map((el) => {
        return el.departureAirport;
      })
      .join(", ");
  }
  const getAirtCraftInfo = (facilities) => {
    const airCraftInfo = facilities?.find((fa) => fa?.code === "AIRCRAFT_INFO");
    return airCraftInfo ? airCraftInfo.description : "";
  };
  const airCraft = getAirtCraftInfo(item?.ticketdetail?.facilities);

  const arrivalDay = moment(item.arrivalDayStr, DATE_FORMAT);
  const departureDay = moment(item.departureDayStr, DATE_FORMAT);
  const diffDay = arrivalDay.diff(departureDay, "days");

  return (
    <Box className={classes.wrapper}>
      <Grid container>
        <Grid item xs={8}>
          <Box className={classes.wrapCard}>
            <Box className={classes.wrapCardLeft}>
              <Typography
                variant="subtitle1"
                component="span"
                className={classes.airlineFromTo}
              >
                {item.departureTimeStr}
              </Typography>
              <Typography
                variant="caption"
                component="span"
                className={classes.airlineCode}
              >
                {item.departureAirport}
              </Typography>
            </Box>
            <IconArrowFlightLong className={classes.iconArrow} />
            <Box className={classes.wrapCardRight}>
              <Typography
                variant="subtitle1"
                component="span"
                className={classes.airlineFromTo}
              >
                {item.arrivalTimeStr}
                {!!diffDay && (
                  <Typography
                    variant="caption"
                    style={{
                      position: "relative",
                      top: -6,
                      left: 4,
                      color: theme.palette.pink.main,
                    }}
                  >
                    +{diffDay}d
                  </Typography>
                )}
              </Typography>
              <Typography
                variant="caption"
                component="span"
                className={classes.airlineCode}
              >
                {item.arrivalAirport}
              </Typography>
            </Box>
          </Box>
          <Box className={classes.flightTimeBox} mt={1 / 2}>
            <Typography variant="body2">
              {durationFlightTime(item.duration)}
            </Typography>
            <IconDot className={classes.iconDot} />
            {item.ticketdetail.numStops === 0 ? (
              <Typography variant="body2">
                {listString.IDS_TEXT_NUM_STOPS_0}
              </Typography>
            ) : (
              <Typography variant="body2">{`${item.ticketdetail.numStops} ${
                listString.IDS_TEXT_NUM_STOPS_1
              } ${getStops(item)
                .map((ele, i) => `${ele.code} (${ele.city})`)
                .join(",")}`}</Typography>
            )}
          </Box>
          <Box className={classes.wrapFlight}>
            {transitAirPortsStr.length ? (
              <>
                <IconFlightTransit />
                <Box className={classes.warpAirBrand}>
                  <Typography
                    component={"span"}
                    variant="body2"
                    className={classes.airlineName}
                  >
                    {transitAirPortsStr}
                  </Typography>
                </Box>
              </>
            ) : (
              <>
                {item?.operatingAirlineId ? (
                  <Box display="flex" flexDirection="column">
                    <Box display="flex" alignItems="center">
                      <Image
                        srcImage={airline.logo}
                        className={classes.airlineLogo}
                      />
                      <Typography
                        component={"span"}
                        variant="body2"
                        className={classes.airlineName}
                      >
                        {airline.name}
                      </Typography>
                    </Box>
                    <Box display="flex">
                      <Typography
                        component={"span"}
                        variant="body2"
                        className={classes.flightCode}
                        style={{
                          color: theme.palette.secondary.main,
                          marginLeft: 28,
                        }}
                      >
                        {item?.operatingAirlineId ? "Bay Pacific Airlines" : ""}
                      </Typography>
                      <IconDot className={classes.iconDot} />
                      <Typography
                        component={"span"}
                        variant="body2"
                        className={classes.flightCode}
                      >
                        {airCraft
                          ? `${item.flightNumber}/ ${airCraft.split("-")[0]}`
                          : item.flightNumber}
                      </Typography>
                    </Box>
                  </Box>
                ) : (
                  <>
                    <Image
                      srcImage={airline.logo}
                      className={classes.airlineLogo}
                    />
                    <Typography
                      component={"span"}
                      variant="body2"
                      className={classes.airlineName}
                    >
                      {airline.name}
                    </Typography>
                    {item.flightNumber && (
                      <IconDot className={classes.iconDot} />
                    )}
                    <Typography
                      component={"span"}
                      variant="body2"
                      className={classes.flightCode}
                    >
                      {airCraft
                        ? `${item.flightNumber}/ ${airCraft.split("-")[0]}`
                        : item.flightNumber}
                    </Typography>{" "}
                  </>
                )}
              </>
            )}
          </Box>
        </Grid>
        <Grid item xs={4} className={classes.wrapDiscountPrice}>
          <Typography variant="subtitle1" className={classes.mainPrice}>
            {item.ticketdetail.discountAdultUnit < item.ticketdetail.farePrice
              ? `${(
                  item.ticketdetail.farePrice -
                  item.ticketdetail.discountAdultUnit
                ).formatMoney()}đ`
              : `${item.ticketdetail.farePrice.formatMoney()}đ`}
          </Typography>
          {!!item.ticketdetail.discountAdultUnit &&
            item.ticketdetail.discountAdultUnit <
              item.ticketdetail.farePrice && (
              <Typography variant="body2" className={classes.subPrice}>
                {`${item.ticketdetail.farePrice.formatMoney()}đ`}
              </Typography>
            )}
          <Box
            display="flex"
            flexDirection="column"
            justifyContent="flex-end"
            alignItems="flex-end"
            mt={1 / 2}
          >
            {item.ticketdetail.isBestPrice === 1 ? ( // for label best price
              <Box
                className={
                  item?.aid === 1
                    ? classes.bestPriceBox_1
                    : item?.aid === 2
                    ? classes.bestPriceBox_2
                    : classes.bestPriceBox_4
                }
              >
                <Image
                  srcImage={airline.logo}
                  className={classes.airlineLogo1}
                />
                <Typography variant="body2">
                  {listString.IDS_TEXT_FLIGHT_BEST_PRICE}
                </Typography>
              </Box>
            ) : null}
            {item?.isRunningOut && item.isRunningOut === 1 && (
              <>
                <Box className={classes.runningOut}>
                  <Box marginRight={"6px"}>
                    <IconRunningOut />
                  </Box>
                  o
                  <Typography variant="body2">
                    {listString.IDS_TEXT_FLIGHT_RUNNING_OUT}
                  </Typography>
                </Box>
              </>
            )}
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default TicketFlightCard;

TicketFlightCard.propTypes = {
  item: PropTypes.object.isRequired,
  airline: PropTypes.object,
};
