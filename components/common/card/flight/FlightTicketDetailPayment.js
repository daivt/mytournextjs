import { Typography } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { IconLocation, IconPlane } from "@public/icons";
import { durationFlightTime } from "@utils/helpers";
import { DATE_FORMAT } from "@utils/moment";
import moment from "moment";
import styled from "styled-components";

export const BoxLogoAirline = styled.div`
  display: flex;
  flex-flow: column;
  text-align: center;
  align-items: center;
`;

export const BoxInfoFlight = styled.div`
  display: flex;
  align-items: start;
`;

const Row = styled.div`
  display: flex;
  align-items: center;
`;

const Col = styled.div`
  display: flex;
  flex-direction: column;
`;
const useStyles = makeStyles((theme) => ({
  textGray: {
    color: theme.palette.gray.grayDark7,
  },
}));

const FlightTicketDetailPayment = (props) => {
  const { ticket, airlines } = props;
  const theme = useTheme();
  const classes = useStyles();
  if (!ticket) {
    return <div />;
  }

  let airlineInfo = {};
  if (airlines) {
    airlineInfo = airlines.find((al) => al.aid === ticket?.airlineId);
  }

  return (
    <div
      style={{
        padding: 0,
      }}
    >
      <div
        style={{
          display: "flex",
        }}
      >
        <BoxInfoFlight style={{ flex: 1.5 }}>
          <Col
            style={{
              alignItems: "center",
              height: "100%",
              justifyContent: "space-between",
            }}
          >
            <Col style={{ minWidth: 80, marginRight: 8, textAlign: "end" }}>
              <Typography variant="subtitle1">
                <span>{ticket.departureTime}</span>
              </Typography>
              <Typography variant="caption" className={classes.textGray}>
                {moment(ticket.departureDate, DATE_FORMAT).format("L")}
              </Typography>
            </Col>
            <Col style={{ minWidth: 80, marginRight: 8, textAlign: "end" }}>
              <Typography variant="caption" className={classes.textGray}>
                {durationFlightTime(ticket?.duration)}
              </Typography>
            </Col>
            <Col style={{ minWidth: 80, marginRight: 8, textAlign: "end" }}>
              <Typography variant="subtitle1">
                <span>{ticket.arrivalTime}</span>
              </Typography>
              <Typography variant="caption" className={classes.textGray}>
                {moment(ticket.arrivalDate, DATE_FORMAT).format("L")}
              </Typography>
            </Col>
          </Col>
          <Col
            style={{
              alignItems: "center",
              height: "100%",
              justifyContent: "space-between",
              paddingBottom: 28,
            }}
          >
            <IconPlane
              className="svgFillAll"
              style={{
                height: "32px",
                width: "16px",
                fill: theme.palette.blue.blueLight8,
                marginBottom: 4,
              }}
            />
            <div
              style={{
                borderLeft: `1px solid ${theme.palette.blue.blueLight8}`,
                height: "100%",
                paddingBottom: 24,
                width: 1,
                paddingTop: 4,
              }}
            />
            <IconLocation
              className="svgFillAll"
              style={{
                height: "32px",
                width: "16px",
                fill: theme.palette.blue.blueLight8,
              }}
            />
          </Col>
          <Col
            style={{
              height: "100%",
              justifyContent: "space-between",
              marginLeft: 8,
            }}
          >
            <Col>
              <Typography variant="subtitle1">
                {ticket.departureCity}
                &nbsp;&#40;
                {ticket.fromAirport}
                &#41;
              </Typography>
              <Typography variant="caption" className={classes.textGray}>
                {ticket.fromAirportName}
              </Typography>
            </Col>
            <BoxLogoAirline
              style={{
                alignItems: "flex-start",
                marginTop: 8,
              }}
            >
              <Row style={{ alignItems: "center", marginBottom: 8 }}>
                <img alt="" style={{ width: "32px" }} src={airlineInfo?.logo} />
                <Col style={{ textAlign: "left", marginLeft: 8 }}>
                  <Typography variant="subtitle2">
                    {airlineInfo?.name}
                  </Typography>
                  <Row style={{ alignItems: "center" }}>
                    <Typography variant="body2">{ticket.flightCode}</Typography>
                  </Row>
                </Col>
              </Row>
            </BoxLogoAirline>

            <Col>
              <Typography variant="subtitle1">
                {ticket.arrivalCity}
                &nbsp;&#40;
                {ticket.toAirport}
                &#41;
              </Typography>
              <Typography variant="caption" className={classes.textGray}>
                {ticket.toAirportName}
              </Typography>
            </Col>
          </Col>
        </BoxInfoFlight>
      </div>
    </div>
  );
};

export default FlightTicketDetailPayment;
