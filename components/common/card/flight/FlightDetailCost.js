import { Box, Divider, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import ButtonComponent from "@src/button/Button";
import { listString, listEventFlight } from "@utils/constants";
import { useRouter } from "next/router";
import React from "react";
import styled from "styled-components";
import { getTicketClass, Policy } from "utils/helpers";
import * as gtm from "@utils/gtm";
export const BoxLogoAirline = styled.div`
  display: flex;
  flex-flow: column;
  text-align: center;
`;

export const BoxInfoFlight = styled.div`
  display: flex;
  align-items: start;
`;

const Row = styled.div`
  display: flex;
  align-items: center;
`;

const Col = styled.div`
  display: flex;
  flex-direction: column;
`;

export const GREY_500 = "#9e9e9e";

const useStyles = makeStyles((theme) => ({
  footer: {
    height: "100%",
  },
  button: {
    marginTop: 12,
    width: "100%",
    color: "#fff",
    fontWeight: "bold",
    height: 36,
    backgroundColor: theme.palette.secondary.main,
    "&:hover": {
      backgroundColor: theme.palette.secondary.main,
    },
  },
  countNumberBooking: {
    marginLeft: 2,
    paddingTop: 2,
    paddingRight: 4,
  },
  wrapCountBooking_1: {
    width: 100,
    height: 24,
    border: `1px solid ${theme.palette.brown.brownLight1}`,
    color: theme.palette.brown.brownLight1,
    borderRadius: 4,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 12,
  },
  wrapCountBooking_2: {
    width: 100,
    height: 24,
    border: `1px solid ${theme.palette.green.greenLight6}`,
    color: theme.palette.green.greenLight6,
    borderRadius: 4,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 12,
  },
  wrapCountBooking_4: {
    width: 100,
    height: 24,
    border: `1px solid ${theme.palette.red.redDark5}`,
    color: theme.palette.red.redDark5,
    borderRadius: 4,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 12,
  },
}));

const FlightDetailCost = (props) => {
  const {
    ticket,
    data,
    isTransit,
    title,
    isSimple,
    isSeat,
    ticketId,
    isTwoWay,
    airlineInfo,
  } = props;
  const theme = useTheme();
  const classes = useStyles();
  const router = useRouter();

  const tempData = isTwoWay
    ? data?.outbound?.tickets
    : router?.query?.ticketOutBoundId
    ? data?.inbound?.tickets
    : data?.tickets;
  const filterDataTickets = tempData
    ?.filter((element) => {
      return element?.outbound?.flightNumber === ticket?.flightNumber;
    })
    .slice(0, 2);
  const onTicket = true;
  const ticketClass = data?.ticketClasses;
  const transitTickets = ticket?.transitTickets;

  function getTotalPriceAfterDiscount(ticket) {
    // const discountPrice = ticket.ticketdetail?.discountAdultUnit;
    return ticket.ticketdetail?.farePrice;
  }

  if (!ticket) {
    return <div />;
  }
  return (
    <>
      <Typography variant="subtitle1" style={{ marginTop: 8 }}>
        <span>{title}</span>
      </Typography>
      {filterDataTickets &&
        filterDataTickets.map((element, index) => (
          <Box key={index}>
            <Divider
              style={{
                height: 1,
                color: theme.palette.gray.grayLight22,
                margin: "12px 0px",
              }}
            />
            <div
              style={{
                display: "flex",
              }}
            >
              <Row style={{ width: "100%" }}>
                <BoxInfoFlight style={{ flex: 1.5 }}>
                  <Col
                    style={{
                      height: "100%",
                      justifyContent: "space-between",
                      marginLeft: 8,
                      width: "100%",
                    }}
                  >
                    {!index && (
                      <Box
                        className={
                          element?.outbound?.aid === 1
                            ? classes.wrapCountBooking_1
                            : element?.outbound?.aid === 2
                            ? classes.wrapCountBooking_2
                            : classes.wrapCountBooking_4
                        }
                      >
                        <img
                          alt=""
                          style={{ width: 16, marginLeft: 4 }}
                          src={airlineInfo.logo}
                        />
                        <Typography
                          variant="body2"
                          component="span"
                          className={classes.countNumberBooking}
                        >
                          {listString.IDS_TEXT_FLIGHT_BEST_PRICE}
                        </Typography>
                      </Box>
                    )}
                    <Row
                      style={{ width: "100%", justifyContent: "space-between" }}
                    >
                      <BoxLogoAirline
                        style={{
                          alignItems: onTicket ? "flex-start" : undefined,
                          marginTop: 8,
                        }}
                      >
                        <Typography variant="subtitle1">
                          <span>
                            {getTotalPriceAfterDiscount(
                              element?.outbound
                            ).formatMoney()}
                          </span>
                          đ
                        </Typography>
                        {isSeat && (
                          <Row
                            style={{
                              marginTop: "8px",
                            }}
                          >
                            <Typography variant="subtitle1">
                              {getTicketClass(element?.outbound, ticketClass)}
                            </Typography>
                            <div
                              style={{
                                height: "18px",
                                marginLeft: "5px",
                                minWidth: "18px",
                                overflow: "hidden",
                                alignItems: "center",
                                borderRadius: "2px",
                                padding: "0 5px",
                                display: "flex",
                                justifyContent: "center",
                                color: "white",
                                backgroundColor: theme.palette.primary.main,
                                fontWeight: 500,
                                fontSize: "14px",
                              }}
                            >
                              {transitTickets
                                ? transitTickets[0].detailTicketClass
                                : element?.outbound?.ticketdetail
                                    ?.detailTicketClass}
                            </div>
                          </Row>
                        )}
                      </BoxLogoAirline>
                      <div style={{ width: 80 }}>
                        <ButtonComponent
                          type="submit"
                          className={classes.button}
                          backgroundColor={theme.palette.secondary.main}
                          fontSize={14}
                          fontWeight={600}
                          borderRadius={8}
                          handleClick={() => {
                            if (isTwoWay) {
                              router.push({
                                pathname: "/ve-may-bay/result/inbound",
                                query: {
                                  ...router?.query,
                                  agencyOutBoundId: element?.outbound?.aid,
                                  ticketOutBoundId: element?.tid,
                                  needPassport: element?.outbound
                                    ? element?.outbound?.ticketdetail
                                        ?.needPassport
                                    : false,
                                },
                              });
                            } else {
                              const query = router?.query || {};
                              gtm.addEventGtm(listEventFlight.FlightAddToCart);
                              if (router?.query?.ticketOutBoundId) {
                                router.push({
                                  pathname: "/ve-may-bay/payment",
                                  query: {
                                    ...router?.query,
                                    agencyOutBoundId:
                                      router?.query?.agencyOutBoundId,
                                    ticketOutBoundId:
                                      router?.query?.ticketOutBoundId,
                                    agencyInBoundId: ticket?.aid,
                                    requestId: data.requestId,
                                    ticketInBoundId: element?.tid,
                                    needPassport: element?.outbound
                                      ? element?.outbound?.ticketdetail
                                          ?.needPassport
                                      : false,
                                  },
                                });
                              } else {
                                router.push({
                                  pathname: "/ve-may-bay/payment",
                                  query: {
                                    ...router?.query,
                                    agencyOutBoundId: element?.outbound?.aid,
                                    requestId: data.requestId,
                                    ticketOutBoundId: element?.tid,
                                    needPassport: element?.outbound
                                      ? element?.outbound?.ticketdetail
                                          ?.needPassport
                                      : false,
                                  },
                                });
                              }
                            }
                          }}
                        >
                          {listString.IDS_MT_TEXT_SELECT}
                        </ButtonComponent>
                      </div>
                    </Row>

                    {!isSimple && (
                      <div
                        style={{
                          display: "flex",
                          flexWrap: "wrap",
                          padding: "10px 8px",
                          flex: 1.5,
                          flexFlow: "column",
                        }}
                      >
                        {isTransit &&
                          element?.outbound?.transitTickets &&
                          element?.outbound?.transitTickets[0].polices.map(
                            (item, index) => (
                              <Policy item={item} key={index} isMobile={true} />
                            )
                          )}

                        {!isTransit &&
                          element?.outbound?.ticketdetail?.polices &&
                          element?.outbound?.ticketdetail.polices?.map(
                            (item, index) => (
                              <Policy item={item} key={index} isMobile={true} />
                            )
                          )}
                      </div>
                    )}
                  </Col>
                </BoxInfoFlight>
              </Row>
            </div>
          </Box>
        ))}
    </>
  );
};

export default FlightDetailCost;
