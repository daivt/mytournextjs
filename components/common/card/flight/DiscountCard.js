import clsx from "clsx";
import { DATE_FORMAT } from "@utils/moment";
import moment from "moment";
import { Box, makeStyles, Typography } from "@material-ui/core";
import { listString } from "@utils/constants";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  container: {
    maxWidth: 298,
    minHeight: 108,
    padding: 12,
    margin: "0 8px",
    background: theme.palette.white.main,
    borderRadius: 8,
    borderLeft: `5px Solid ${theme.palette.blue.blueLight8}`,
    boxShadow:
      "0px 0px 8px rgba(0, 0, 0, 0.05), 0px 8px 8px rgba(0, 0, 0, 0.05);",
    display: "flex",
    flexDirection: "column",
    marginBottom: 2,
    position: "relative",
  },
  codeItem: {
    color: theme.palette.gray.grayDark7,
  },
  titleItem: {
    color: theme.palette.black.black3,
    padding: "6px 0 8px",
  },
  expiredItem: {
    color: theme.palette.gray.grayDark7,
    position: "absolute",
    bottom: 12,
  },
}));
const DiscountCard = ({ cardInfo = {}, classNameCard = "" }) => {
  const classes = useStyles();
  return (
    <Box
      className={clsx(classes.container, classNameCard)}
      id={cardInfo.id ? cardInfo.id : 0}
    >
      <Typography
        variant="subtitle2"
        component="span"
        className={classes.codeItem}
      >
        {cardInfo?.codeDetail}
      </Typography>
      <Typography
        variant="subtitle1"
        component="span"
        className={classes.titleItem}
      >
        {cardInfo?.rewardProgram?.title.length > 58
          ? `${cardInfo?.rewardProgram?.title.slice(0, 58)} ...`
          : cardInfo?.rewardProgram?.title}
      </Typography>
      <Typography
        variant="body2"
        component="span"
        className={classes.expiredItem}
      >
        {`${listString.IDS_TEXT_DISCOUNT_CARD_EXPIRED}: ${moment(
          cardInfo?.responseData?.validTo
        ).format(DATE_FORMAT)}`}
      </Typography>
    </Box>
  );
};

DiscountCard.propTypes = {
  cardInfo: PropTypes.object.isRequired,
};

export default DiscountCard;
