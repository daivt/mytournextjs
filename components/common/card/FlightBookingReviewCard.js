import { Box, Grid, makeStyles, Typography } from "@material-ui/core";
import { IconArrowFlight } from "@public/icons";
import { listString } from "@utils/constants";
import clsx from "clsx";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  wrapCard: {
    marginTop: 15.5,
    marginLeft: 12,
    display: "flex",
  },
  airlineFromTo: {
    color: theme.palette.black.black3,
    display: "flex",
    alignItems: "center",
  },
  date: {
    color: theme.palette.gray.grayDark8,
    margin: "8px 0 21px 12px",
  },
  airlineLogo: {
    height: 24,
    width: 24,
    marginLeft: 10,
    objectFit: "contain",
  },
  airlineName: {
    color: theme.palette.black.black3,
    marginLeft: 10,
    display: "flex",
    alignItems: "center",
    lineHeight: "16px",
  },
  warpAirBrand: {
    display: "flex",
    marginTop: 75,
  },
  wrapDiscountPercent: {
    border: `1px dashed ${theme.palette.gray.grayLight23}`,
    boxSizing: "border-box",
    borderBottom: "none",
    borderTop: "none",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    height: 107,
    borderRight: "none",
    justifyContent: "center",
  },
  IconArrowFlight: {
    margin: "0 6px",
  },
  discountPercent: {
    background: theme.palette.pink.main,
    color: theme.palette.white.main,
    width: 45,
    marginTop: 21,
    borderRadius: 8,
    textAlign: "center",
    marginRight: 12,
    padding: "4px 3px 3px 2px",
  },
  subPrice: {
    textDecorationLine: "line-through",
    marginTop: 8,
    textAlign: "right",
    marginRight: 12,
    color: theme.palette.gray.grayDark8,
  },
  mainPrice: {
    color: theme.palette.black.black3,
    marginTop: 2,
    textAlign: "right",
    marginRight: 12,
  },
  circle: (props) => ({
    height: 21,
    width: 21,
    backgroundColor: props.bgCircle,
    borderRadius: "50%",
    position: "absolute",
    left: -10,
  }),
  containerDiscount: {
    position: "relative",
  },
  wraper: {
    minHeight: 108,
    borderRadius: 8,
    display: "flex",
    flexDirection: "row",
    backgroundColor: theme.palette.white.main,
  },
}));
const FlightBookingReviewCard = ({
  item,
  bgCircle = "#C4C4C4",
  styleWrapper = "",
  handleClickTicket = () => {},
}) => {
  const propsStyle = {
    bgCircle,
  };
  const classes = useStyles(propsStyle);
  return (
    <Box className={clsx(classes.wraper, styleWrapper)}>
      <Grid
        container
        className={classes.container}
        onClick={handleClickTicket(item)}
      >
        <Grid item xs={8}>
          <Box className={classes.wrapCard}>
            <Typography
              variant="subtitle2"
              component="span"
              className={classes.airlineFromTo}
            >
              {item.flightFrom}
            </Typography>
            <Box
              component="span"
              display="flex"
              alignItems="center"
              className={classes.IconArrowFlight}
            >
              <IconArrowFlight />
            </Box>
            <Typography
              variant="subtitle2"
              component="span"
              className={classes.airlineFromTo}
            >
              {item.flightTo}
            </Typography>
          </Box>
          <Typography variant="body2" className={classes.date}>
            {listString.IDS_MT_TEXT_FLIGHT_START_DATE}: {item.date}
          </Typography>
          <Box className="warpAirBrand">
            <Box display="flex">
              <img src={item.airlineLogo} className={classes.airlineLogo} />
              <Typography variant="caption" className={classes.airlineName}>
                {item.airlineName}
              </Typography>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={4} className={classes.containerDiscount}>
          <Box className={classes.circle} top={-9}></Box>
          <Box className={classes.wrapDiscountPercent}>
            {item.discountPercent > 0 && (
              <>
                <Typography
                  variant="subtitle2"
                  className={classes.discountPercent}
                >
                  {item?.discountPercent}%
                </Typography>
                <Typography className={classes.subPrice}>
                  {item?.subPrice.formatMoney()}đ
                </Typography>
              </>
            )}
            <Typography variant="subtitle1" className={classes.mainPrice}>
              {item?.farePrice.formatMoney()}đ
            </Typography>
          </Box>
          <Box className={classes.circle} top="90%"></Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default FlightBookingReviewCard;
// component Flight Booking Review in slide show

FlightBookingReviewCard.propTypes = {
  item: PropTypes.object.isRequired,
  bgCircle: PropTypes.string,
};
