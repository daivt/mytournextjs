import { Box, makeStyles, Typography } from "@material-ui/core";
import {
  IconPayment,
  IconSecurePayment,
  IconTotalHotel,
  IconWallet,
} from "@public/icons";
import {
  FLIGHT_SERVICE,
  listIcons,
  listString,
  prefixUrlIcon,
} from "@utils/constants";
import { listWhyFlight } from "utils/dataFake";

const useStyles = makeStyles((theme) => ({
  wrapBoxWhy: {
    padding: "32px 0",
    display: "flex",
    flexDirection: "column",
  },
  titleBox: {
    color: theme.palette.black.black3,
  },
  boxItem: {
    display: "flex",
    flexDirection: "row",
    paddingTop: 12,
  },
  boxRight: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    padding: "0 0 12px",
    width: "100%",
  },
  boxLeft: {
    marginRight: 16,
  },
  leftTitle: {
    color: theme.palette.black.black3,
  },
  leftDesc: {
    color: theme.palette.black.black4,
  },
}));

const listWhyHotel = [
  {
    id: 0,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconSupport247}`}
        alt="logo_support_247"
        style={{ width: 32, height: 32 }}
      />
    ),
    title: listString.IDS_MT_TEXT_SUPPORT_247,
    desc: listString.IDS_TEXT_WHY_BOOK_HOTEL_SUPPORT_247_DESC,
  },
  {
    id: 1,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconBestPrice}`}
        alt="logo_best_price"
        style={{ width: 32, height: 32 }}
      />
    ),
    title: listString.IDS_MT_TEXT_BEST_PRICE_OF_DAY,
    desc: listString.IDS_MT_TEXT_COMMIT_BEST_PRICE_OF_DAY,
  },
  {
    id: 2,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconPayment}`}
        alt="logo_payment"
        style={{ width: 32, height: 32 }}
      />
    ),
    title: listString.IDS_MT_TEXT_PAYMENT_EASY,
    desc: listString.IDS_MT_TEXT_SPEED_ORDER_MANY_PAYMENT,
  },
  {
    id: 3,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconTotalHotel}`}
        alt="logo_total_hotel"
        style={{ width: 32, height: 32 }}
      />
    ),
    title: listString.IDS_TEXT_WHY_BOOK_HOTEL_TICKET_TITLE,
    desc: listString.IDS_TEXT_WHY_BOOK_HOTEL_TICKET_DESC,
  },
];

const WhyBookTicket = ({ source = FLIGHT_SERVICE }) => {
  const classes = useStyles();
  const listItems = source === FLIGHT_SERVICE ? listWhyFlight : listWhyHotel;
  return (
    <>
      <Box className={classes.wrapBoxWhy}>
        <Typography className={classes.titleBox} variant="h5">
          {source === FLIGHT_SERVICE
            ? listString.IDS_TEXT_WHY_BOOK_FLIGHT
            : listString.IDS_TEXT_WHY_BOOK_HOTEL}
        </Typography>
        {listItems &&
          listItems.map((item, idx) => (
            <Box className={classes.boxItem} key={idx}>
              <Box className={classes.boxLeft}>{item.icon}</Box>
              <Box className={classes.boxRight}>
                <Typography className={classes.leftTitle} variant="subtitle1">
                  {item.title}
                </Typography>
                <Typography className={classes.leftDesc} variant="body2">
                  {item.desc}
                </Typography>
              </Box>
            </Box>
          ))}
      </Box>
    </>
  );
};

export default WhyBookTicket;
