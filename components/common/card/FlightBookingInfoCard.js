import { Box } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import makeStyles from "@material-ui/styles/makeStyles";
import {
  IconArrowDownLong,
  IconArrowDownUp,
  IconBluePoint,
  IconDot,
  IconLocationHistory,
} from "@public/icons";
import { listString } from "@utils/constants";
import PropTypes from "prop-types";
import { useRouter } from "next/router";
import moment from "moment";
import { DATE_FORMAT, DATE_FORMAT_BACK_END } from "@utils/moment";

const useStyles = makeStyles((theme) => ({
  historySearch: {
    backgroundColor: theme.palette.white.main,
    boxShadow:
      "0px 0px 8px rgba(0, 0, 0, 0.05), 0px 8px 8px rgba(0, 0, 0, 0.05)",
    borderRadius: 8,
    padding: 16,
    minWidth: 260,
  },
  titlePlace: {
    paddingBottom: 10,
    display: "flex",
  },
  iconArrowDown: {
    marginTop: 6,
  },
  textNormal: {
    color: theme.palette.gray.grayDark8,
    overflow: "hidden",
    whiteSpace: "nowrap",
  },
  showDetail: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  iconShowDetail: {
    fontSize: 26,
    color: theme.palette.blue.blueLight8,
  },
  listIcon: {
    display: "flex",
    flexDirection: "column",
  },
  iconDot: {
    margin: "10px 6px",
    display: "flex",
  },
}));

const itemTemp = {
  from: "TP HCM (SGN)",
  to: "Đà Nẵng (DAD)",
  date: "Thứ 5, 22/11",
  guest: 2,
  seat: "Economy",
  type: 0,
};

const FlightBookingInfoCard = ({ item = itemTemp }) => {
  const classes = useStyles();
  const router = useRouter();
  const handleClickTicket = (item) => () => {
    router.push({
      pathname: "/ve-may-bay/result/outbound",
      query: {
        adultCount: item?.adult || 1,
        childCount: item?.child || 0,
        infantCount: item?.baby || 0,
        seatSearch: item?.seatCode,
        departureDate: item?.dateDep
          ? moment(item.dateDep, DATE_FORMAT).format(DATE_FORMAT_BACK_END)
          : moment().format(DATE_FORMAT_BACK_END),
        returnDate:
          item?.dateRet &&
          moment(item.dateRet, DATE_FORMAT).format(DATE_FORMAT_BACK_END),
        origin_code: item?.origin,
        origin_location: item?.flightFrom,
        destination_code: item?.destination,
        destination_location: item?.flightTo,
      },
    });
  };
  return (
    <div className={classes.historySearch}>
      <Grid container>
        <Grid item lg={1} md={1} sm={1} xs={1} className={classes.listIcon}>
          <IconBluePoint className={classes.iconArrowDown} />
          {item.type ? (
            <IconArrowDownUp className={classes.iconArrowDown} />
          ) : (
            <IconArrowDownLong className={classes.iconArrowDown} />
          )}
          <IconLocationHistory className={classes.iconArrowDown} />
        </Grid>
        <Grid
          item
          lg={10}
          md={10}
          sm={10}
          xs={10}
          onClick={handleClickTicket(item)}
        >
          <Typography variant="subtitle1" className={classes.titlePlace}>
            {item.from}
          </Typography>
          <Typography variant="subtitle1" className={classes.titlePlace}>
            {item.to}
          </Typography>
          <Typography variant="caption" className={classes.textNormal}>
            {item.date}
          </Typography>
          <Box display="flex" flexDirection="row">
            <Typography variant="caption" className={classes.textNormal}>
              {item.guest} {listString.IDS_MT_TEXT_SINGLE_CUSTOMER}
            </Typography>
            <IconDot className={classes.iconDot} />
            <Typography variant="caption" className={classes.textNormal}>
              {item.seat}
            </Typography>
          </Box>
        </Grid>
        <Grid item lg={1} md={1} sm={1} xs={1} className={classes.showDetail}>
          <ChevronRightIcon className={classes.iconShowDetail} />
        </Grid>
      </Grid>
    </div>
  );
};

FlightBookingInfoCard.propTypes = {
  item: PropTypes.object.isRequired,
};

export default FlightBookingInfoCard;
