import { Typography } from "@material-ui/core";
import { IconArrowDownToggle } from "@public/icons";
import { isEmpty } from "@utils/helpers";
import moment from "moment";
import { useState } from "react";
import styled from "styled-components";

const GREY_400 = "#BDBDBD";
const WHITE = "#ffffff";
const DATE_FORMAT_BACK_END = "DD-MM-YYYY";
const Row = styled.div`
  display: flex;
  align-items: center;
`;
const Cell = styled.td`
  height: 49px;
  width: calc(100% / 7);
  box-sizing: border-box;
  padding: 0px;
  user-select: none;
`;
const headerStyle = {
  padding: 8,
  fontWeight: 600,
  fontSize: 12,
  lineHeight: "14px",
  textAlign: "center",
  color: "#1A202C",
  background: "#EDF2F7",
};

const CalendarHome = (props) => {
  const {
    startDate,
    endDate,
    setStartDate,
    setEndDate,
    isRoundTrip = false,
    isStartDateSelected = false,
  } = props;
  const [monthStart, setMonthStart] = useState(0);
  const [hoverDate, setHoverDate] = useState(null);

  const isOutsideRange = (date) => {
    return (
      date.isBefore(moment(), "days") ||
      date.isSameOrAfter(moment().add(1, "year"), "days")
    );
  };
  const handleSelectDate = (day) => {
    if (!isRoundTrip) {
      setStartDate(day);
    } else if (startDate && !endDate && isStartDateSelected) {
      setStartDate(day);
      setEndDate(null);
    } else if (startDate && endDate && !startDate.isSame(endDate, "days")) {
      setStartDate(day);
      setEndDate(null);
    } else if (startDate) {
      if (day.isBefore(startDate, "days")) {
        setEndDate(startDate);
        setStartDate(day);
      } else {
        setEndDate(day);
      }
    } else if (endDate) {
      if (day.isBefore(endDate, "days")) {
        setStartDate(day);
      } else {
        setStartDate(startDate);
        setEndDate(day);
      }
    } else {
      setStartDate(day);
      // setEndDate(day);
    }
  };
  const getColorDay = (day, isEdge) => {
    if (isOutsideRange(day)) return GREY_400;
    if (day.isSame(moment(), "days")) return isEdge ? "white" : "#00B6F3";
    return undefined;
  };
  const clearHoverDate = (e) => {
    if (hoverDate) setHoverDate(null);
    e.preventDefault();
    e.stopPropagation();
  };
  const getBlank = (key) => {
    return (
      <Cell
        isFocus={false}
        hover={false}
        key={`blank${key}`}
        onMouseOver={clearHoverDate}
      />
    );
  };
  const renderCalendar = (m, iMonth) => {
    const totalCells = [];
    let week = [];
    const allWeeks = [];
    const firstDayOfMonth = m.startOf("month");
    for (let d = 1; d < firstDayOfMonth.day(); d += 1) {
      totalCells.push(getBlank(d));
    }
    for (let d = 0; d < m.daysInMonth(); d += 1) {
      const day = m.clone().add(d, "days");
      const isSelected =
        startDate &&
        isRoundTrip &&
        day.isSameOrAfter(startDate, "days") &&
        ((endDate && day.isSameOrBefore(endDate, "days")) ||
          (hoverDate && day.isSameOrBefore(hoverDate, "days")));

      const isEdge =
        (startDate && day.isSame(startDate, "days")) ||
        (endDate && day.isSame(endDate, "days")) ||
        (hoverDate && day.isSame(hoverDate, "days") && isRoundTrip);
      let cellBG = WHITE;
      let cellClass = "";
      if (isSelected) cellBG = "rgba(0, 182, 243, 0.2)";
      if (isEdge) cellBG = "#00B6F3";
      if (startDate && !isOutsideRange(day)) {
        if (
          day.isBefore(startDate, "days") ||
          (!isRoundTrip && !day.isSame(startDate, "days"))
        )
          cellClass = "normal-date";
      }
      if (endDate && day.isAfter(endDate, "days") && !isOutsideRange(day)) {
        cellClass = "normal-date";
      }
      totalCells.push(
        <Cell
          draggable={false}
          hover
          key={`day_${day.format(DATE_FORMAT_BACK_END)}`}
          style={{
            backgroundColor: cellBG,
            color: isEdge ? "white" : "#1A202C",
            borderTopLeftRadius:
              startDate && day.isSame(startDate, "days") ? 8 : 0,
            borderBottomLeftRadius:
              startDate && day.isSame(startDate, "days") ? 8 : 0,
            borderTopRightRadius:
              (endDate && day.isSame(endDate, "days")) ||
              !isRoundTrip ||
              (hoverDate && day.isSame(hoverDate, "days"))
                ? 8
                : 0,

            borderBottomRightRadius:
              (endDate && day.isSame(endDate, "days")) ||
              !isRoundTrip ||
              (hoverDate && day.isSame(hoverDate, "days"))
                ? 8
                : 0,
            cursor: isOutsideRange(day) ? "not-allowed" : "pointer",
          }}
          onClick={() => {
            if (isOutsideRange(day)) return;
            handleSelectDate(day);
          }}
          onMouseOver={(e) => {
            if (
              startDate &&
              isEmpty(endDate) &&
              day.isAfter(startDate, "days") &&
              !isOutsideRange(day) &&
              (isEmpty(hoverDate) ||
                (hoverDate && !hoverDate.isSame(day, "days")))
            ) {
              setHoverDate(day);
            }
            if (
              hoverDate &&
              !hoverDate.isSame(day, "days") &&
              day.isBefore(startDate, "days")
            ) {
              setHoverDate(null);
            }
            e.preventDefault();
            e.stopPropagation();
          }}
        >
          <Row
            className={cellClass}
            style={{
              height: "100%",
              padding: 4,
              justifyContent: "center",
              flexDirection: "column",
            }}
          >
            {((startDate && startDate.isSame(day, "days")) ||
              (endDate && endDate.isSame(day, "days")) ||
              (hoverDate && hoverDate.isSame(day, "days"))) &&
              isRoundTrip && (
                <span
                  style={{
                    fontSize: 8,
                    lineHeight: "10px",
                    fontWeight: 600,
                    marginBottom: 3,
                  }}
                >
                  NGÀY {startDate.isSame(day, "days") ? "ĐI" : "VỀ"}
                </span>
              )}

            <Typography
              style={{
                width: "100%",
                textAlign: "center",
                color: getColorDay(day, isEdge),
              }}
              variant="body2"
            >
              {d + 1}
            </Typography>
          </Row>
        </Cell>
      );
    }
    for (
      let d = 1;
      d <= (7 - ((m.daysInMonth() + firstDayOfMonth.day()) % 7)) % 7;
      d += 1
    ) {
      totalCells.push(getBlank(d + firstDayOfMonth.day()));
    }

    totalCells.forEach((day, index) => {
      if (index % 7 !== 0) {
        week.push(day);
      } else {
        allWeeks.push(week);
        week = [];
        week.push(day);
      }
      if (index === totalCells.length - 1) {
        allWeeks.push(week);
      }
    });
    return (
      <table
        style={{ borderCollapse: "collapse", width: "100%" }}
        draggable={false}
      >
        <thead>
          <tr>
            <td colSpan={7}>
              <div
                style={{
                  display: "flex",
                  padding: "6px 0 16px 0",
                  alignItems: "center",
                }}
              >
                {iMonth === 0 && monthStart !== 0 && (
                  <IconArrowDownToggle
                    style={{
                      marginLeft: 12,
                      transform: "rotate(90deg)",
                      cursor: monthStart === 0 ? "not-allowed" : "pointer",
                    }}
                    onClick={() =>
                      setMonthStart(
                        monthStart > 0 ? monthStart - 1 : monthStart
                      )
                    }
                  />
                )}
                <Typography
                  variant="subtitle1"
                  style={{
                    userSelect: "none",
                    fontSize: 16,
                    lineHeight: "19px",
                    width: "100%",
                    textAlign: "center",
                  }}
                >
                  Tháng {m.format("MM")}, {m.format("YYYY")}
                </Typography>
                {iMonth === 1 && (
                  <IconArrowDownToggle
                    style={{
                      marginRight: 12,
                      transform: "rotate(-90deg)",
                      cursor: "pointer",
                    }}
                    onClick={() => setMonthStart(monthStart + 1)}
                  />
                )}
              </div>
            </td>
          </tr>
        </thead>
        <thead>
          <tr onMouseOver={clearHoverDate} onFocus={clearHoverDate}>
            <td style={{ ...headerStyle, borderRadius: "8px 0 0 8px" }}>T2</td>
            <td style={headerStyle}>T3</td>
            <td style={headerStyle}>T4</td>
            <td style={headerStyle}>T5</td>
            <td style={headerStyle}>T6</td>
            <td style={{ ...headerStyle, color: "#FF1284" }}>T7</td>
            <td
              style={{
                ...headerStyle,
                color: "#FF1284",
                borderRadius: "0 8px 8px 0",
              }}
            >
              CN
            </td>
          </tr>
        </thead>
        <tbody>
          {allWeeks.map((w, i) => {
            return <tr key={`week${i} `}>{w}</tr>;
          })}
        </tbody>
      </table>
    );
  };

  return (
    <div
      style={{
        display: "flex",
        width: "calc(100% + 24px)",
        justifyContent: "space-between",
        margin: "0 -12px",
      }}
      onMouseOver={clearHoverDate}
      onFocus={clearHoverDate}
    >
      {[monthStart, monthStart + 1].map((v, index) => {
        return (
          <div key={index} style={{ width: "100%", margin: "0 12px" }}>
            {renderCalendar(
              moment()
                .add(v, "months")
                .startOf("month"),
              index
            )}
          </div>
        );
      })}
    </div>
  );
};

export default CalendarHome;
