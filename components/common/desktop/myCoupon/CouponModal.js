import { Box, Dialog, makeStyles, Slide } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { listString, optionClean } from "@utils/constants";
import { unEscape } from "@utils/helpers";
import { useRouter } from "next/router";
import React from "react";
import sanitizeHtml from "sanitize-html";

const useStyles = makeStyles((theme) => ({
  dialogContent: {
    display: "flex",
    padding: "0 24px 24px 24px",
    color: "#1A202C",
  },
  appBar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "16px 16px 16px 24px",
    color: "#1A202C",
  },
  titleHeader: {
    fontSize: 16,
    fontWeight: 600,
    lineHeight: "19px",
  },
}));
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
const CouponModal = ({ item = {}, handleClose }) => {
  const classes = useStyles();
  const router = useRouter();
  return (
    <Dialog
      maxWidth="md"
      fullWidth
      open
      onClose={handleClose}
      TransitionComponent={Transition}
    >
      <Box className={classes.appBar}>
        <Box component="span" className={classes.titleHeader}>
          {listString.IDS_MT_CONDITION_COUPON}
        </Box>
        <CloseIcon
          style={{ fontSize: 24, cursor: "pointer" }}
          onClick={handleClose}
        />
      </Box>
      <Box className={classes.dialogContent}>
        <Box component="span" className={classes.titleHeader}>
          {item.title}
          <Box
            component="span"
            fontSize={14}
            fontWeight={400}
            color="gray.grayDark1"
            lineHeight="22px"
            dangerouslySetInnerHTML={{
              __html: sanitizeHtml(
                unEscape(unescape(item.policy || "")),
                optionClean
              ),
            }}
          />
        </Box>
      </Box>
    </Dialog>
  );
};
export default CouponModal;
