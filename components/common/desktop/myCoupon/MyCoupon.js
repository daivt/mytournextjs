import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import DiscountCard from "@components/common/desktop/card/DiscountCard";
import { listString } from "@utils/constants";
import { useState, useEffect } from "react";
import { getPromotions } from "@api/hotels";
import { isEmpty } from "@utils/helpers";
import { IconArrowRight } from "@public/icons";
import SlideShow from "@components/common/slideShow/SlideShow";
import CouponModal from "./CouponModal";

const useStyles = makeStyles((theme) => ({
  container: { background: "white", width: "100%" },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    marginBottom: 40,
  },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
  },
  cardGroup: {
    display: "flex",
    margin: "0",
    flexWrap: "wrap",
    justifyContent: "space-between",
  },
  cardBox: { margin: 12, width: "calc(100% - 24px) !important" },
  iconArrow: {
    width: 16,
    height: 17,
    stroke: theme.palette.black.black3,
  },
  wrapArrow: {
    "&:hover": {
      "& svg": {
        stroke: theme.palette.blue.blueLight8,
        transition: "all ease .2s",
      },
    },
  },
}));
const customStyle = {
  background: "white",
  height: "40px",
  width: "40px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 2,
  borderRadius: "100px",
  boxShadow: "0px 8px 12px rgb(0 0 0 / 10%)",
  cursor: "pointer",
  position: "absolute",
  top: "50%",
  transform: "translate(-50%, -50%)",
};

function SamplePrevArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, left: 12 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight
        className={`svgFillAll ${classes.iconArrow}`}
        style={{ transform: "rotate(180deg)" }}
      />
    </div>
  );
}

function SampleNextArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, right: "-23px" }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight className={`svgFillAll ${classes.iconArrow}`} />
    </div>
  );
}
const MyCoupon = () => {
  const classes = useStyles();
  const [couponList, setCouponList] = useState({});
  const [couponActive, setCouponActive] = useState(null);
  const [idxActive, setActive] = useState(0);
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 2,
    nextArrow: <SampleNextArrow customStyleArrow={customStyle} />,
    prevArrow:
      idxActive > 0 ? (
        <SamplePrevArrow customStyleArrow={customStyle} />
      ) : (
        <></>
      ),
    beforeChange: (current, next) => setActive(next),
  };

  const fetchData = async () => {
    try {
      const { data } = await getPromotions({ module: "hotel" });
      if (data.code === 200) {
        setCouponList(data.data);
      }
    } catch (error) {}
  };
  useEffect(() => {
    fetchData(); // eslint-disable-next-line
  }, []);
  if (isEmpty(couponList.items)) return null;
  return (
    <>
      <Box className={classes.container}>
        <Box className={classes.content}>
          <Box style={{ marginBottom: 24 }}>
            <Typography variant="body2" className={classes.titleText}>
              {listString.IDS_MT_MY_COUPON}
            </Typography>
          </Box>
          {!isEmpty(couponList.items) &&
            (couponList.items.length > 2 ? (
              <SlideShow settingProps={settings} style={{ margin: "0 -12px" }}>
                {!isEmpty(couponList.items) &&
                  couponList.items.map((el) => {
                    return (
                      <Box key={el.id} className={classes.cardBox}>
                        <DiscountCard
                          item={el}
                          setCouponActive={setCouponActive}
                        />
                      </Box>
                    );
                  })}
              </SlideShow>
            ) : (
              <Box className={classes.cardGroup} style={{ marginBottom: 32 }}>
                {!isEmpty(couponList.items) &&
                  couponList.items.map((el) => (
                    <Box
                      key={el.id}
                      style={{
                        width:
                          couponList.items.length > 1
                            ? "calc(50% - 12px)"
                            : "100%",
                        margin: "12px 0",
                      }}
                    >
                      <DiscountCard
                        item={el}
                        setCouponActive={setCouponActive}
                      />
                    </Box>
                  ))}
              </Box>
            ))}
        </Box>
      </Box>
      {!isEmpty(couponActive) && (
        <CouponModal
          handleClose={() => setCouponActive(null)}
          item={couponActive}
        />
      )}
    </>
  );
};

export default MyCoupon;
