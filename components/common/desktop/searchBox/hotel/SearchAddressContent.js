import { Box, Typography } from "@material-ui/core";
import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import {
  LAST_LOCATION_RECENT_SEARCH_HISTORY_ITEMS,
  listString,
} from "@utils/constants";
import { IconLocationBorder, IconHistory, IconHotel } from "@public/icons";
import { handleSaveItemStorage, isEmpty } from "@utils/helpers";
import NoResultSearchModal from "@components/common/modal/hotel/componentDestinationSearchModal/NoResultSearchModal";

const useStyles = makeStyles((theme) => ({
  contentContainer: {
    width: "100%",
    color: "#1A202C",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    padding: 16,
    zIndex: 1,
  },
  textTitle: {
    fontWeight: 600,
    fontSize: 14,
    lineHeight: "17px",
    marginBottom: 8,
  },
  textItem: {
    fontSize: 14,
    lineHeight: "17px",
    marginLeft: 12,
    textAlign: "left",
  },
  searchItem: {
    display: "flex",
    marginBottom: 4,
    marginLeft: -8,
    marginRight: -8,
    padding: "10px 8px",
    alignItems: "center",
    borderRadius: 8,
    cursor: "pointer",
    width: "calc(100% + 16px)",
    "&:hover": { background: "#EDF2F7" },
  },
  noData: { marginTop: 30, marginBottom: 30 },
  subTextItem: {
    fontSize: 12,
    lineHeight: "14px",
    color: "#718096",
    marginTop: 4,
    marginLeft: 12,
    textAlign: "left",
  },
}));
const SearchAddressContent = ({
  topLocation = [],
  addressSearch = [],
  inputSearch = "",
  handleSelectedItem,
}) => {
  const classes = useStyles();
  const [historySearch, setHistorySearch] = useState([]);

  useEffect(() => {
    setHistorySearch(
      JSON.parse(
        localStorage.getItem(LAST_LOCATION_RECENT_SEARCH_HISTORY_ITEMS)
      ) || []
    );
  }, []);
  const handleClick = (it) => {
    handleSaveItemStorage(it);
    handleSelectedItem(it);
  };
  const hightLightText = (text = "", subText = "") => {
    const result = text.replace(
      subText.trim(),
      `<b style='color:#00B6F3;'>${subText}</b>`
    );
    // eslint-disable-next-line react/no-danger
    return <span dangerouslySetInnerHTML={{ __html: result }} />;
  };
  if (!isEmpty(inputSearch.trim()) && isEmpty(addressSearch)) {
    return (
      <NoResultSearchModal keyword={inputSearch} className={classes.noData} />
    );
  }
  if (!isEmpty(addressSearch)) {
    const addressList = [];
    const hotelList = [];
    addressSearch.forEach((el) => {
      if (el.type === "HOTEL") hotelList.push(el);
      else addressList.push(el);
    });
    return (
      <Box className={classes.contentContainer}>
        {addressList.map((el, idx) => (
          <Box
            className={classes.searchItem}
            key={idx}
            onClick={() => handleClick(el)}
          >
            <Box style={{ width: 20, height: 20 }}>
              <IconLocationBorder
                className="svgFillAll"
                style={{ stroke: "#718096", width: 18, height: 20 }}
              />
            </Box>
            <Box>
              <Typography variant="body2" className={classes.textItem}>
                {hightLightText(el.name || "", inputSearch.trim() || "")}
              </Typography>
              {el.searchType === "chain" && (
                <Typography variant="body2" className={classes.subTextItem}>
                  Chuỗi khách sạn
                </Typography>
              )}
            </Box>
          </Box>
        ))}
        {hotelList.map((el, idx) => (
          <Box
            className={classes.searchItem}
            key={idx}
            onClick={() => handleClick(el)}
          >
            <Box style={{ width: 20, height: 20 }}>
              <IconHotel />{" "}
            </Box>
            <Box>
              <Typography variant="body2" className={classes.textItem}>
                {hightLightText(el.name || "", inputSearch.trim() || "")}
              </Typography>
              <Typography variant="body2" className={classes.subTextItem}>
                {!isEmpty(el.label) ? el.label.trim() : ""}
              </Typography>
            </Box>
          </Box>
        ))}
      </Box>
    );
  }
  return (
    <Box className={classes.contentContainer}>
      {!isEmpty(historySearch) && (
        <Typography variant="body2" className={classes.textTitle}>
          {listString.IDS_TEXT_RECENT_SEARCH}
        </Typography>
      )}
      {!isEmpty(historySearch) &&
        historySearch.map((el, i) => (
          <Box
            className={classes.searchItem}
            key={i}
            onClick={() => handleClick(el)}
          >
            <Box style={{ width: 20, height: 20 }}>
              <IconHistory />
            </Box>
            <Typography variant="body2" className={classes.textItem}>
              {el.name}
            </Typography>
          </Box>
        ))}

      <Typography
        variant="body2"
        className={classes.textTitle}
        style={{ marginTop: 8 }}
      >
        {listString.IDS_MT_HIGHT_LIGHT_ADDRESS}
      </Typography>
      {!isEmpty(topLocation) &&
        topLocation.map((el, idx) => (
          <Box
            className={classes.searchItem}
            onClick={() => handleClick(el)}
            key={idx}
          >
            <IconLocationBorder
              className="svgFillAll"
              style={{ stroke: "#718096", width: 18, height: 20 }}
            />
            <Typography variant="body2" className={classes.textItem}>
              {el.name}
            </Typography>
          </Box>
        ))}
    </Box>
  );
};

export default SearchAddressContent;
