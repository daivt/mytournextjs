import { Box, Typography } from "@material-ui/core";
import { useState } from "react";
import SingleSelect from "@src/form/SingleSelect";
import { makeStyles } from "@material-ui/styles";
import { listString } from "@utils/constants";
import { IconArrowDownSmall } from "@public/icons";

const useStyles = makeStyles((theme) => ({
  contentContainer: {
    width: "100%",
    color: "#1A202C",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    padding: 16,
    zIndex: 1,
  },
  textTitle: {
    fontWeight: 600,
    fontSize: 14,
    lineHeight: "17px",
    marginBottom: 8,
  },
  textItem: { fontSize: 14, lineHeight: "17px" },
  searchItem: {
    display: "flex",
    marginBottom: 4,
    marginLeft: -8,
    marginRight: -8,
    padding: "10px 8px",
    alignItems: "center",
    borderRadius: 8,
    cursor: "pointer",
    width: "calc(100% + 16px)",
    justifyContent: "space-between",
    borderBottom: "solid 1px #EDF2F7",
    // "&:hover": { background: "#EDF2F7" },
  },
  searchRightItem: { display: "flex" },
  leftItem: { display: "flex" },
  rightAction: {
    display: "flex",
    alignItems: "center",
    fontWeight: 600,
    fontSize: 16,
    lineHeight: "19px",
  },
  iconAction: {
    width: 32,
    height: 32,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "50%",
    background: "#E2E8F0",
    cursor: "pointer",
  },
  valueItem: { width: 57 },
  inputChildContainer: {
    display: "flex",
    width: "100%",
    flexWrap: "wrap",
    justifyContent: "space-between",
  },
  itemChild: {
    display: "flex",
    width: "48%",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 12,
  },
}));
const ageData = [
  { id: 1, label: "1 tuổi" },
  { id: 2, label: "2 tuổi" },
  { id: 3, label: "3 tuổi" },
  { id: 4, label: "4 tuổi" },
  { id: 5, label: "5 tuổi" },
  { id: 6, label: "6 tuổi" },
  { id: 7, label: "7 tuổi" },
  { id: 8, label: "8 tuổi" },
  { id: 9, label: "9 tuổi" },
  { id: 10, label: "10 tuổi" },
  { id: 11, label: "11 tuổi" },
  { id: 12, label: "12 tuổi" },
  { id: 13, label: "13 tuổi" },
  { id: 14, label: "14 tuổi" },
  { id: 15, label: "15 tuổi" },
  { id: 16, label: "16 tuổi" },
  { id: 17, label: "17 tuổi" },
];
const SearchRoomContent = ({
  room,
  adult,
  child,
  setRoom,
  setAdult,
  setChild,
  listAge,
  setListAge,
}) => {
  const classes = useStyles();
  const updateListAge = (value) => {
    setListAge(value);
  };
  return (
    <Box className={classes.contentContainer}>
      <Box className={classes.searchItem}>
        <Box className={classes.leftItem}>
          <Typography variant="body2" className={classes.textItem}>
            {listString.IDS_MT_TEXT_ROOM}
          </Typography>
        </Box>
        <Box className={classes.rightAction}>
          <Box
            className={classes.iconAction}
            style={{
              color: room === 1 ? "#A0AEC0" : "#1A202C",
              cursor: room === 1 ? "not-allowed" : "pointer",
            }}
            onClick={() => {
              if (room > 1) setRoom(room - 1);
            }}
          >
            -
          </Box>
          <Box className={classes.valueItem}>{room}</Box>
          <Box
            className={classes.iconAction}
            style={{
              color: room === 9 ? "#A0AEC0" : "#1A202C",
              cursor: room === 9 ? "not-allowed" : "pointer",
            }}
            onClick={() => {
              if (room < 9) {
                setRoom(room + 1);
                if (room + 1 > adult) setAdult(room + 1);
              }
            }}
          >
            +
          </Box>
        </Box>
      </Box>
      <Box className={classes.searchItem}>
        <Box className={classes.leftItem}>
          <Typography variant="body2" className={classes.textItem}>
            {listString.IDS_MT_TEXT_ADULT}
          </Typography>
        </Box>
        <Box className={classes.rightAction}>
          <Box
            className={classes.iconAction}
            style={{
              color: adult === 1 ? "#A0AEC0" : "#1A202C",
              cursor: adult === 1 ? "not-allowed" : "pointer",
            }}
            onClick={() => {
              if (adult > 1) {
                setAdult(adult - 1);
                if (adult - 1 < room) setRoom(adult - 1);
              }
            }}
          >
            -
          </Box>
          <Box className={classes.valueItem}>{adult}</Box>
          <Box
            className={classes.iconAction}
            style={{
              color: adult === 36 ? "#A0AEC0" : "#1A202C",
              cursor: adult === 36 ? "not-allowed" : "pointer",
            }}
            onClick={() => {
              if (adult < 36) setAdult(adult + 1);
            }}
          >
            +
          </Box>
        </Box>
      </Box>
      <Box className={classes.searchItem}>
        <Box className={classes.leftItem}>
          <Typography variant="body2" className={classes.textItem}>
            {listString.IDS_MT_TEXT_CHILDREN}
          </Typography>
        </Box>
        <Box className={classes.rightAction}>
          <Box
            className={classes.iconAction}
            style={{
              color: child === 0 ? "#A0AEC0" : "#1A202C",
              cursor: child === 0 ? "not-allowed" : "pointer",
            }}
            onClick={() => {
              if (child > 0) {
                setChild(child - 1);
                const listAgeTemp = [];
                listAge.forEach((v, idx) => {
                  if (idx >= child - 1) listAgeTemp.push(0);
                  else listAgeTemp.push(v);
                });
                updateListAge(listAgeTemp);
              }
            }}
          >
            -
          </Box>
          <Box className={classes.valueItem}>{child}</Box>
          <Box
            className={classes.iconAction}
            style={{
              color: child === 9 ? "#A0AEC0" : "#1A202C",
              cursor: child === 9 ? "not-allowed" : "pointer",
            }}
            onClick={() => {
              if (child < 9) {
                setChild(child + 1);
                const listAgeTemp = [...listAge];
                listAgeTemp[child] = 10;
                updateListAge(listAgeTemp);
              }
            }}
          >
            +
          </Box>
        </Box>
      </Box>
      {child !== 0 && (
        <Box className={classes.searchItem} style={{ borderBottom: "none" }}>
          <Box className={classes.leftItem}>
            <Typography variant="body2" className={classes.textItem}>
              {listString.IDS_MT_INPUT_CHILDREN}
            </Typography>
          </Box>
        </Box>
      )}
      <Box className={classes.inputChildContainer}>
        {Array(child)
          .fill(0)
          .map((el, i) => (
            <Box className={classes.itemChild} key={i}>
              {listString.IDS_MT_TEXT_CHILDREN}&nbsp;{i + 1}
              <SingleSelect
                options={ageData}
                placeholder={listString.IDS_MT_AGE}
                getOptionLabel={(v) => v.label}
                onSelectOption={(value) => {
                  const listAgeTemp = [...listAge];
                  listAgeTemp[i] = value;
                  updateListAge(listAgeTemp);
                }}
                value={listAge[i]}
                formControlStyle={{ width: "fit-content" }}
                hideHelperText
                inputStyle={{ minHeight: 30, borderRadius: 8 }}
                formControlStyle={{ minWidth: 93, width: 93, marginRight: 0 }}
                iconRight={<IconArrowDownSmall />}
              />
            </Box>
          ))}
      </Box>
    </Box>
  );
};

export default SearchRoomContent;
