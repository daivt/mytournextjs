import { Box, Button, Typography, Input } from "@material-ui/core";
import OutsideClickHandler from "react-outside-click-handler";
import { useRouter } from "next/router";
import moment from "moment";
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import { IconMoonlight, IconSearch } from "@public/icons";
import { formatDate, listString, routeStatic } from "@utils/constants";
import { hotelAutoComplete } from "@api/homes";
import {
  checkAndChangeCheckIO,
  isEmpty,
  paramsDefaultHotel,
} from "@utils/helpers";
import SearchChainsContent from "./SearchChainsContent";
import SearchRoomContent from "./SearchRoomContent";
import SearchDateContent from "./SearchDateContent";

const useStyles = makeStyles((theme) => ({
  headerContainer: {
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    flexDirection: "column",
    margin: "0 auto",
  },
  headerLeft: { display: "flex", alignItems: "center" },
  divider: {
    height: 3,
    width: 40,
    margin: "auto",
    marginTop: 12,
    marginBottom: 12,
  },
  textItem: { marginLeft: 24, marginBottom: 12, cursor: "pointer" },
  searchContainer: {
    width: "100%",
    background: "white",
    // boxShadow: "0px 15px 15px rgba(0, 0, 0, 0.2)",
    boxShadow: "0px 10px 10px rgba(26, 32, 44, 0.05)",
    borderRadius: 8,
    padding: 8,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  input: {
    fontSize: 16,
    lineHeight: "19px",
    fontWeight: 600,
    color: "#1A202C",
    borderRadius: 8,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    zIndex: 2,
    width: 300,
    marginBottom: -6,
  },
  nightDay: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 28,
    height: 28,
    borderRadius: "50%",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    backgroundColor: theme.palette.white.main,
    margin: "0 50px",
    color: theme.palette.black.black3,
  },
  textSub: {
    fontSize: 12,
    lineHeight: "14px",
    color: theme.palette.gray.grayDark8,
    fontWeight: 400,
    textAlign: "left",
  },
  textMain: {
    fontSize: 16,
    lineHeight: "19px",
    fontWeight: 600,
    color: "#1A202C",
    paddingTop: 6,
  },
  textItemInput: { marginLeft: 16 },
  textItemCheckIn: {
    borderLeft: `1px solid ${theme.palette.gray.grayLight23}`,
  },
  dividerSearch: {
    height: 3,
    width: "100%",
    margin: "auto",
    marginTop: 12,
    marginBottom: -17,
  },
  wrapInput: { position: "relative" },
  subItem: {
    position: "absolute",
    zIndex: 1,
    top: 64,
    left: 0,
    width: "100%",
    background: "white",
    border: "1px solid #E2E8F0",
    boxSizing: "border-box",
    boxShadow: "0px 8px 8px rgba(0, 0, 0, 0.1)",
    borderRadius: 8,
    maxHeight: 418,
    overflow: "auto",
  },
}));
const TYPE_DROPDOWN = {
  ADDRESS: "address",
  CHECK_IN: "checkIn",
  CHECK_OUT: "checkOut",
  ROOM: "room",
};

let timeoutSearch = null;
let addressItem = {};

const SectionSearchMyTourMall = ({
  type,
  topLocation = [],
  paramsUrl = paramsDefaultHotel(),
  location = {},
  hotelDetail = {},
  paramsQuery = {},
  cityInfo = {},
}) => {
  const classes = useStyles();
  const router = useRouter();
  const [typeDropDown, setTypeDropDown] = useState(null);
  const [room, setRoom] = useState(paramsUrl.rooms || 1);
  const [adult, setAdult] = useState(paramsUrl.adults || 2);
  const [child, setChild] = useState(paramsUrl.children || 0);
  const initListAge = (i) => {
    if (!isEmpty(paramsQuery.childrenAges))
      return paramsQuery.childrenAges[i] || 0;
    return 0;
  };
  const [listAge, setListAge] = useState([
    initListAge(0),
    initListAge(0),
    initListAge(0),
    initListAge(0),
    initListAge(0),
    initListAge(0),
    initListAge(0),
    initListAge(0),
    initListAge(0),
    initListAge(0),
  ]);
  const [startDate, setStartDate] = useState(
    moment(
      checkAndChangeCheckIO(paramsUrl.checkIn, "checkIn"),
      formatDate.firstDay
    )
  );
  const [endDate, setEndDate] = useState(
    moment(
      checkAndChangeCheckIO(paramsUrl.checkOut, "checkOut"),
      formatDate.firstDay
    )
  );
  const getInitialSearch = () => {
    if (type === "detail") return hotelDetail.name || "";
    if (type === "top_hotel") return cityInfo.name || "";
    return location.name || "";
  };
  const [inputSearch, setInputSearch] = useState(getInitialSearch());
  const [addressSearch, setAddressSearch] = useState([]);

  const getDividerColor = (value) => {
    return typeDropDown === value ? "#00B6F3" : " transparent";
  };
  useEffect(() => {
    if (isEmpty(endDate)) setTypeDropDown(TYPE_DROPDOWN.CHECK_OUT);
    if (!isEmpty(endDate) && typeDropDown !== null) setTypeDropDown(null); // eslint-disable-next-line
  }, [endDate]);
  useEffect(() => {
    if (
      isEmpty(endDate) &&
      !isEmpty(startDate) &&
      typeDropDown !== TYPE_DROPDOWN.CHECK_IN &&
      typeDropDown !== TYPE_DROPDOWN.CHECK_OUT
    ) {
      setEndDate(moment(startDate).add("days", 1));
    } // eslint-disable-next-line
  }, [typeDropDown]);
  const handleFetchData = async (params = {}) => {
    const { data } = await hotelAutoComplete({ ...params, type: "chain" });
    if (data.code === 200) setAddressSearch(data.data.items);
  };
  const handleChangeInput = (e) => {
    const { value } = e.target;
    if (value.trim() !== inputSearch.trim()) addressItem = {};
    setInputSearch(value);
    const text = value.trim();
    clearTimeout(timeoutSearch);
    if (!isEmpty(text)) {
      timeoutSearch = setTimeout(() => {
        const params = { term: text || undefined, size: 20 };
        handleFetchData(params);
      }, 300);
    }
  };
  // useEffect(() => {
  //   handleFetchData({ term: "", size: 20 }); // eslint-disable-next-line
  // }, []);
  const handleSelectedItem = (item) => {
    addressItem = { ...item };
    setTypeDropDown(null);
    setInputSearch(item.name);
  };
  const handleSearch = () => {
    setTypeDropDown(null);
    let paramsSelect = {
      checkIn: startDate.format(formatDate.firstDay),
      checkOut: endDate.format(formatDate.firstDay),
      adults: adult,
      rooms: room,
      children: child,
    };
    const tempAges = listAge.filter((v) => v !== 0);
    if (!isEmpty(tempAges)) {
      paramsSelect = { ...paramsSelect, childrenAges: tempAges.join(",") };
    }
    router.push({
      pathname: routeStatic.LISTING_HOTEL.href,
      query: {
        slug: [
          `${addressItem.aliasCode}`,
          `khach-san-tai-${addressItem.name.stringSlug()}.html`,
        ],
        ...paramsSelect,
        locationCode: addressItem.locationCode,
        searchType: addressItem.searchType,
        type: "chain",
      },
    });
  };
  return (
    <Box style={{ width: "100%" }}>
      <Box className={classes.headerContainer}>
        <OutsideClickHandler
          onOutsideClick={(e) => {
            if (
              typeDropDown === TYPE_DROPDOWN.ADDRESS &&
              isEmpty(addressItem)
            ) {
              addressItem = { ...addressSearch[0] };
            }
            if (!(e.target.innerText && e.target.innerText.includes("tuổi"))) {
              setTypeDropDown(null);
            }
          }}
        >
          <Box
            className={classes.searchContainer}
            style={{
              border:
                type === "listing" || type === "detail"
                  ? "1px solid #CBD5E0"
                  : "none",
            }}
            onClick={(e) => e.stopPropagation()}
          >
            <Box className={classes.textItemInput}>
              <Box className={classes.wrapInput}>
                <Box className={classes.textSub}>
                  {listString.IDS_MT_MALL_BRAND}
                </Box>
                <Box className={classes.text}>
                  <Input
                    className={classes.input}
                    placeholder={listString.IDS_MT_MALL_INPUT_BRAND_NAME}
                    disableUnderline
                    value={inputSearch}
                    onFocus={() => {
                      if (!isEmpty(inputSearch.trim())) {
                        handleFetchData({ term: inputSearch.trim(), size: 20 });
                      }
                      setTypeDropDown(TYPE_DROPDOWN.ADDRESS);
                    }}
                    onChange={handleChangeInput}
                  />
                  <div
                    className={classes.dividerSearch}
                    style={{
                      background: getDividerColor(TYPE_DROPDOWN.ADDRESS),
                    }}
                  />
                </Box>
                {typeDropDown === TYPE_DROPDOWN.ADDRESS &&
                  !isEmpty(inputSearch.trim()) && (
                    <Box
                      className={classes.subItem}
                      style={{ width: "calc(100% + 72px)", left: -24 }}
                    >
                      <SearchChainsContent
                        topLocation={topLocation}
                        addressSearch={addressSearch}
                        inputSearch={inputSearch}
                        handleSelectedItem={handleSelectedItem}
                      />
                    </Box>
                  )}
              </Box>
            </Box>

            <Box className={classes.textItemCheckIn}>
              <Box
                display="flex"
                alignItems="center"
                style={{ marginLeft: 24, minWidth: 368 }}
                className={classes.wrapInput}
                onClick={() => setTypeDropDown(TYPE_DROPDOWN.CHECK_IN)}
              >
                <Box className={classes.textSub}>
                  {listString.IDS_MT_TEXT_DATE_CHECKIN}
                  <Box className={classes.textMain}>
                    {moment(startDate)
                      .locale("vi_VN")
                      .format("ddd")}
                    ,&nbsp;
                    {moment(startDate).format("DD")} Tháng&nbsp;
                    {moment(startDate).format("MM")}
                  </Box>
                  <div
                    className={classes.dividerSearch}
                    style={{
                      background: getDividerColor(TYPE_DROPDOWN.CHECK_IN),
                    }}
                  />
                </Box>
                <Box className={classes.nightDay}>
                  <Box
                    component="span"
                    fontSize={11}
                    lineHeight="13px"
                    pr={2 / 8}
                  >
                    {endDate &&
                      Math.abs(
                        moment(startDate)
                          .startOf("day")
                          .diff(endDate.startOf("day"), "days")
                      )}
                  </Box>
                  <IconMoonlight />
                </Box>
                <Box className={classes.textSub}>
                  {listString.IDS_MT_TEXT_DATE_CHECKOUT}
                  {endDate ? (
                    <Box className={classes.textMain}>
                      {endDate &&
                        moment(endDate)
                          .locale("vi_VN")
                          .format("ddd")}
                      ,&nbsp;
                      {endDate && moment(endDate).format("DD")} Tháng&nbsp;
                      {endDate && moment(endDate).format("MM")}
                    </Box>
                  ) : (
                    <Box className={classes.textMain}>Chọn ngày về</Box>
                  )}

                  <div
                    className={classes.dividerSearch}
                    style={{
                      background: getDividerColor(TYPE_DROPDOWN.CHECK_OUT),
                    }}
                  />
                </Box>
                {(typeDropDown === TYPE_DROPDOWN.CHECK_IN ||
                  typeDropDown === TYPE_DROPDOWN.CHECK_OUT) && (
                  <Box
                    className={classes.subItem}
                    style={{ width: "calc(200% + 72px)", left: -24 }}
                    onClick={(e) => e.stopPropagation()}
                  >
                    <SearchDateContent
                      startDate={startDate}
                      endDate={endDate}
                      setStartDate={setStartDate}
                      setEndDate={(end) => {
                        if (!moment(end).isSame(startDate, "days")) {
                          setEndDate(end);
                        }
                      }}
                    />
                  </Box>
                )}
              </Box>
            </Box>

            <Box className={classes.textItemCheckIn}>
              <Box
                display="flex"
                alignItems="center"
                style={{ marginLeft: 24 }}
                onClick={() => setTypeDropDown(TYPE_DROPDOWN.ROOM)}
              >
                <Box className={classes.wrapInput}>
                  <Box className={classes.textSub}>
                    {listString.IDS_MT_TEXT_NUM_ROOMS_OF_GUEST}
                  </Box>
                  <Box className={classes.textMain}>
                    {room} phòng, {adult} người lớn, {child} trẻ em
                  </Box>
                  <div
                    className={classes.dividerSearch}
                    style={{ background: getDividerColor(TYPE_DROPDOWN.ROOM) }}
                  />
                  {typeDropDown === TYPE_DROPDOWN.ROOM && (
                    <Box
                      className={classes.subItem}
                      style={{ width: 372, left: -24 }}
                    >
                      <SearchRoomContent
                        room={room}
                        adult={adult}
                        child={child}
                        listAge={listAge}
                        setRoom={setRoom}
                        setAdult={setAdult}
                        setChild={setChild}
                        setListAge={setListAge}
                      />
                    </Box>
                  )}
                </Box>
              </Box>
            </Box>
            <Button
              variant="contained"
              color="secondary"
              style={{
                width: 100,
                height: 56,
                background: "#1A202C",
                cursor:
                  isEmpty(inputSearch) || isEmpty(addressItem)
                    ? "not-allowed"
                    : "pointer",
              }}
              // disabled={isEmpty(inputSearch)}
              onClick={() => {
                if (!isEmpty(inputSearch) && !isEmpty(addressItem))
                  handleSearch();
              }}
            >
              <IconSearch className="svgFillAll" style={{ stroke: "white" }} />
            </Button>
          </Box>
        </OutsideClickHandler>
      </Box>
    </Box>
  );
};

export default SectionSearchMyTourMall;
