import { Box, Typography } from "@material-ui/core";
import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import {
  LAST_LOCATION_RECENT_SEARCH_HISTORY_ITEMS,
  listString,
} from "@utils/constants";
import { IconLocationBorder } from "@public/icons";
import { isEmpty } from "@utils/helpers";
import NoResultSearchModal from "@components/common/modal/hotel/componentDestinationSearchModal/NoResultSearchModal";

const useStyles = makeStyles((theme) => ({
  contentContainer: {
    width: "100%",
    color: "#1A202C",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    padding: 16,
    zIndex: 1,
  },
  textTitle: {
    fontWeight: 600,
    fontSize: 14,
    lineHeight: "17px",
    marginBottom: 8,
  },
  textItem: { fontSize: 14, lineHeight: "17px", marginLeft: 12 },
  searchItem: {
    display: "flex",
    marginBottom: 4,
    marginLeft: -8,
    marginRight: -8,
    padding: "10px 8px",
    alignItems: "center",
    borderRadius: 8,
    cursor: "pointer",
    width: "calc(100% + 16px)",
    "&:hover": { background: "#EDF2F7" },
  },
  noData: { marginTop: 30, marginBottom: 30 },
}));
const SearchChainsContent = ({
  topLocation = [],
  addressSearch = [],
  inputSearch = "",
  handleSelectedItem,
}) => {
  const classes = useStyles();
  const handleClick = (it) => {
    handleSelectedItem(it);
  };
  const hightLightText = (text = "", subText = "") => {
    const result = text.replace(
      subText.trim(),
      `<b style='color:#00B6F3;'>${subText}</b>`
    );
    // eslint-disable-next-line react/no-danger
    return <span dangerouslySetInnerHTML={{ __html: result }} />;
  };
  if (!isEmpty(inputSearch.trim()) && isEmpty(addressSearch)) {
    return (
      <NoResultSearchModal keyword={inputSearch} className={classes.noData} />
    );
  }

  return (
    <Box className={classes.contentContainer}>
      <Typography
        variant="body2"
        className={classes.textTitle}
        style={{ marginTop: 8 }}
      >
        {listString.IDS_MT_MALL_BRAND}
      </Typography>
      {!isEmpty(addressSearch) &&
        addressSearch.map((el, idx) => (
          <Box
            className={classes.searchItem}
            onClick={() => handleClick(el)}
            key={idx}
          >
            <IconLocationBorder
              className="svgFillAll"
              style={{ stroke: "#718096", width: 18, height: 20 }}
            />
            <Typography variant="body2" className={classes.textItem}>
              {hightLightText(el.name || "", inputSearch.trim() || "")}
            </Typography>
          </Box>
        ))}
    </Box>
  );
};

export default SearchChainsContent;
