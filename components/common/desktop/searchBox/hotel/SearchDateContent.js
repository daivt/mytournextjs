import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import CalendarHome from "@components/common/desktop/calendar/CalendarHome";

const useStyles = makeStyles((theme) => ({
  contentContainer: {
    width: "100%",
    color: "#1A202C",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    padding: 16,
    zIndex: 1,
  },
}));
const SearchDateContent = ({
  startDate,
  endDate,
  setStartDate,
  setEndDate,
}) => {
  const classes = useStyles();

  return (
    <Box className={classes.contentContainer}>
      <CalendarHome
        startDate={startDate}
        endDate={endDate}
        setStartDate={setStartDate}
        setEndDate={setEndDate}
      />
    </Box>
  );
};

export default SearchDateContent;
