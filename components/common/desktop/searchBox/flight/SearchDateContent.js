import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import CalendarHomeFlight from "@components/common/desktop/calendar/CalendarHomeFlight";

const useStyles = makeStyles((theme) => ({
  contentContainer: {
    width: "100%",
    color: theme.palette.black.black3,
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    padding: 16,
    zIndex: 1,
  },
}));
const SearchDateContent = ({
  startDate,
  endDate,
  setStartDate,
  setEndDate,
  isRoundTrip,
  isStartDateSelected,
}) => {
  const classes = useStyles();

  return (
    <Box className={classes.contentContainer}>
      <CalendarHomeFlight
        startDate={startDate}
        isStartDateSelected={isStartDateSelected}
        endDate={endDate}
        setStartDate={setStartDate}
        setEndDate={setEndDate}
        isRoundTrip={isRoundTrip}
      />
    </Box>
  );
};

export default SearchDateContent;
