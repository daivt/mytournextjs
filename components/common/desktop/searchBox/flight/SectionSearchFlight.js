import { Box, Button, Input } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/styles";
import { IconFlightSearch, IconSwitch } from "@public/icons";
import { listString, SEAT_TYPE_LIST, listEventFlight } from "@utils/constants";
import { useState, useEffect, useCallback } from "react";
import moment from "moment";
import { withStyles } from "@material-ui/core/styles";
import Switch from "@material-ui/core/Switch";
import { DATE_FORMAT_BACK_END, DATE_FORMAT, DATE_MONTH } from "@utils/moment";
import { SEARCH_PREV, SEARCH_RECENT } from "@utils/constants";
import { searchAirports, getGeneralInformation } from "@api/flight";
import { useRouter } from "next/router";
import OutsideClickHandler from "react-outside-click-handler";
import SearchAirportContent from "./SearchAirportContent";
import SearchDateContent from "./SearchDateContent";
import SearchSeat from "./SearchSeat";
import * as gtm from "@utils/gtm";
const useStyles = makeStyles((theme) => ({
  headerContainer: {
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    flexDirection: "column",
    margin: "0 auto",
  },
  headerLeft: { display: "flex", alignItems: "center" },
  divider: {
    height: 3,
    width: 40,
    margin: "auto",
    marginTop: 12,
    marginBottom: 12,
  },
  textItem: { marginLeft: 24, marginBottom: 12, cursor: "pointer" },
  searchContainer: {
    width: "100%",
    background: "white",
    borderRadius: 8,
    padding: 8,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  input: {
    ...theme.typography.subtitle1,
    color: theme.palette.black.black3,
    borderRadius: 8,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    zIndex: 2,
    width: 200,
    marginBottom: -6,
    "& ::placeholder": {
      color: theme.palette.gray.grayDark7,
      opacity: 1,
    },
  },
  switchIcon: {},
  textSub: {
    fontSize: 12,
    lineHeight: "14px",
    color: theme.palette.gray.grayDark8,
    fontWeight: 400,
    textAlign: "left",
  },
  textMain: {
    fontSize: 16,
    lineHeight: "19px",
    fontWeight: 600,
    color: theme.palette.black.black3,
    paddingTop: 6,
  },
  textItemInput: { marginLeft: 16 },
  textItemCheckIn: {
    borderLeft: `1px solid ${theme.palette.gray.grayLight23}`,
  },
  dividerSearch: {
    height: 3,
    width: "100%",
    margin: "auto",
    marginTop: 12,
    marginBottom: -17,
  },
  wrapInput: { position: "relative" },
  subItem: {
    position: "absolute",
    top: 64,
    left: 0,
    width: "100%",
    background: "white",
    border: `1px solid ${theme.palette.gray.grayLight23}`,
    boxSizing: "border-box",
    boxShadow: "0px 8px 8px rgba(0, 0, 0, 0.1)",
    borderRadius: 8,
  },
  roundTrip: {},
  placeEndDate: {
    width: 12,
    height: 2,
    background: theme.palette.gray.grayDark7,
    display: "block",
    borderRadius: 100,
  },
  placeEndDateWrap: {
    color: theme.palette.gray.grayDark7,
  },
  wrapFakeInput: {
    position: "relative",
  },
  fakeInput: {
    position: "absolute",
    color: theme.palette.black.black3,
    top: 5,
    left: 0,
    maxWidth: 200,
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    overflow: "hidden",
    ...theme.typography.body1,
  },
}));
const TYPE_DROPDOWN = {
  DEPARTURE: "departure",
  DESTINATION: "destination",
  CHECK_IN: "checkIn",
  CHECK_OUT: "checkOut",
  SEAT: "seat",
};

const LIST_SEARCH = [
  { code: "HAN", location: "Hà Nội", name: "Nội Bài" },
  { code: "SGN", location: "Hồ Chí Minh", name: "Tân Sơn Nhất" },
  { code: "DAD", location: "Đà Nẵng", name: "Quốc tế Đà Nẵng" },
  { code: "CXR", location: "Nha Trang", name: "Cam Ranh" },
  { code: "PQC", location: "Phú Quốc", name: "Quốc tế Phú Quốc" },
  { code: "HUI", location: "Huế", name: "Quốc tế Phú Bài" },
  { code: "HPH", location: "Hải Phòng", name: "Quốc tế Cát bi" },
];

const CustomStyleSwitch = withStyles((theme) => ({
  root: {
    position: "absolute",
    top: -12,
    left: 60,
  },
  checked: {
    color: `${theme.palette.primary.main} !important`,
  },
}))(Switch);
const SectionSearchFlight = ({
  type = "flight",
  codeFlight = {},
  dataQuery = {},
  query = {},
}) => {
  const classes = useStyles();
  const [active, setActive] = useState("home");
  const [typeDropDown, setTypeDropDown] = useState(null);
  const [isSearchParamsValid, setIsSearchParamsValid] = useState(false);
  const [
    searchDepartureAirportsList,
    setSearchDepartureAirportsList,
  ] = useState(LIST_SEARCH);
  const [
    searchDestinationAirportsList,
    setSearchDestinationAirportsList,
  ] = useState(LIST_SEARCH);
  const router = useRouter();

  const [seatTypeList, setSeatTypeList] = useState(SEAT_TYPE_LIST);
  const [searchHistory, setSearchHistory] = React.useState([]);

  const defaultFlightDataSearch = (codeFlight, dataQuery) => {
    return {
      origin: {
        code: dataQuery?.origin_code
          ? dataQuery.origin_code
          : codeFlight?.code && query?.slug[0] !== "den"
          ? codeFlight?.code?.split("-")[0]
          : codeFlight?.code && query?.slug[0] === "den"
          ? ""
          : searchHistory.length
          ? searchHistory[0]?.origin
          : "SGN",
        location: dataQuery?.origin_location
          ? dataQuery.origin_location
          : codeFlight?.depart && query?.slug[0] !== "den"
          ? codeFlight?.depart
          : codeFlight?.code && query?.slug[0] === "den"
          ? ""
          : searchHistory.length
          ? searchHistory[0]?.flightFrom
          : "Hồ Chí Minh",
        name: dataQuery?.originName
          ? dataQuery.originName
          : codeFlight?.depart && query?.slug[0] !== "den"
          ? codeFlight?.depart
          : codeFlight?.code && query?.slug[0] === "den"
          ? ""
          : searchHistory.length
          ? searchHistory[0]?.flightFrom
          : "Tân Sơn Nhất",
      },
      destination: {
        code: dataQuery?.destination_code
          ? dataQuery.destination_code
          : codeFlight?.code
          ? codeFlight?.code?.split("-")[1]
          : searchHistory.length
          ? searchHistory[0]?.destination
          : "",
        location: dataQuery?.destination_location
          ? dataQuery.destination_location
          : codeFlight?.arrival
          ? codeFlight?.arrival
          : searchHistory.length
          ? searchHistory[0]?.flightTo
          : "",
        name: dataQuery?.destinationName
          ? dataQuery.destinationName
          : codeFlight?.arrival
          ? codeFlight?.arrival
          : searchHistory.length
          ? searchHistory[0]?.flightTo
          : "",
      },
    };
  };

  const [destinationData, setDestinationData] = useState(
    defaultFlightDataSearch(codeFlight, dataQuery)
  );

  const [isRoundTrip, setIsRoundTrip] = useState(
    (dataQuery && dataQuery?.slug && dataQuery?.slug[0] === "chang-bay") ||
      dataQuery.returnDate === ""
      ? false
      : (searchHistory?.length && searchHistory[0]?.type) || false
  );
  let defaultStartDate, defaultEndDate;
  if (dataQuery && dataQuery?.departureDate) {
    defaultStartDate = moment(
      dataQuery?.departureDate,
      DATE_FORMAT_BACK_END,
      true
    ).isSameOrAfter(moment(), "days")
      ? moment(dataQuery?.departureDate, DATE_FORMAT_BACK_END, true)
      : moment();

    defaultEndDate =
      dataQuery?.returnDate &&
      moment(dataQuery.returnDate, DATE_FORMAT_BACK_END, true).isSameOrAfter(
        moment(),
        "days"
      )
        ? moment(dataQuery.returnDate, DATE_FORMAT_BACK_END, true)
        : dataQuery?.returnDate
        ? moment()
        : undefined;
  } else {
    defaultStartDate =
      searchHistory?.length &&
      searchHistory[0]?.dateDep &&
      moment(searchHistory[0]?.dateDep, DATE_FORMAT, true).isSameOrAfter(
        moment(),
        "days"
      )
        ? moment(searchHistory[0]?.dateDep, DATE_FORMAT, true)
        : moment();

    defaultEndDate =
      searchHistory?.length &&
      searchHistory[0]?.dateRet &&
      searchHistory[0]?.type &&
      moment(searchHistory[0]?.dateRet, DATE_FORMAT, true).isSameOrAfter(
        moment(),
        "days"
      )
        ? moment(searchHistory[0]?.dateRet, DATE_FORMAT, true)
        : searchHistory?.length &&
          searchHistory[0]?.dateRet &&
          searchHistory[0]?.type
        ? moment()
        : undefined;
  }

  const [startDate, setStartDate] = useState(defaultStartDate);
  const [endDate, setEndDate] = useState(defaultEndDate);

  const [destination, setDestination] = useState(
    destinationData?.destination?.code
      ? `${destinationData?.destination?.code} (${destinationData?.destination?.location})`
      : ""
  );
  const [departure, setDeparture] = useState(
    destinationData?.origin?.code
      ? `${destinationData?.origin?.code} (${destinationData?.origin?.location})`
      : ""
  );

  const handleSearchParams = {
    adultCount: dataQuery?.adultCount
      ? parseInt(dataQuery.adultCount)
      : searchHistory?.length
      ? searchHistory[0].adult
      : 1,
    childCount: dataQuery?.childCount
      ? parseInt(dataQuery.childCount)
      : searchHistory?.length
      ? searchHistory[0].child
      : 0,
    infantCount: dataQuery?.infantCount
      ? parseInt(dataQuery.infantCount)
      : searchHistory?.length
      ? searchHistory[0].baby
      : 0,
    seatSearch: dataQuery.seatSearch
      ? seatTypeList.find((seat) => seat?.name === dataQuery.seatSearch)?.cid
      : searchHistory?.length
      ? seatTypeList.find(
          (seat) => seat?.code === searchHistory[0]?.seatCode[0]
        )?.cid
      : 1,
  };

  const [paramsSearch, setParamsSearch] = useState(handleSearchParams);

  useEffect(() => {
    const ISSERVER = typeof window === "undefined";

    if (!ISSERVER) {
      const temp = JSON.parse(localStorage.getItem(SEARCH_PREV));
      setSearchHistory(temp || []);
    }
  }, []);

  useEffect(() => {
    destinationData?.destination?.code
      ? setDestination(
          `${destinationData?.destination?.code} (${destinationData?.destination?.location})`
        )
      : setDestination("");
  }, [destinationData.destination]);

  useEffect(() => {
    setParamsSearch({
      ...paramsSearch,
      seatSearch:
        seatTypeList.find((seat) => seat?.code === dataQuery.seatSearch)?.cid ||
        1,
    });
  }, [seatTypeList]);

  useEffect(() => {
    setDeparture(
      destinationData?.origin?.code
        ? `${destinationData?.origin?.code} (${destinationData?.origin?.location})`
        : setDeparture("")
    );
  }, [destinationData.origin]);

  useEffect(() => {
    setDestinationData(defaultFlightDataSearch(codeFlight, dataQuery));
  }, [query, searchHistory]);

  useEffect(() => {
    !destination &&
      setDestinationData({ ...destinationData, destination: undefined });
  }, [destination]);

  useEffect(() => {
    !departure && setDestinationData({ ...destinationData, origin: undefined });
  }, [departure]);

  useEffect(() => {
    setIsRoundTrip(
      (dataQuery && dataQuery?.slug && dataQuery?.slug[0] === "chang-bay") ||
        dataQuery.returnDate === ""
        ? false
        : (searchHistory?.length && searchHistory[0]?.type) || false
    );
    setStartDate(defaultStartDate);
    setEndDate(defaultEndDate);
    setParamsSearch(handleSearchParams);
  }, [searchHistory]);

  const theme = useTheme();

  const getDividerColor = (value) => {
    return typeDropDown === value ? "#00B6F3" : " transparent";
  };

  const handleChangeInput = (event) => {
    event.target.name === "destination"
      ? setDestination(event.target.value)
      : setDeparture(event.target.value);
  };

  const actionDepartureSearchAirports = useCallback(
    debounce(async (term) => {
      if (!term) {
        const ISSERVER = typeof window === "undefined";
        if (!ISSERVER) {
          const recentList = JSON.parse(localStorage.getItem(SEARCH_RECENT));
          if (recentList?.length) {
            const recentFrom = recentList.slice(0, 4).map((item) => {
              return {
                code: item?.origin,
                location: item?.flightFrom,
                name: item?.originName || item?.flightFrom,
              };
            });
            let codes = [];
            const recentFromList = recentFrom.filter((item) => {
              if (codes?.includes(item?.code)) {
                return false;
              } else {
                codes.push(item?.code);
                return true;
              }
            });
            setSearchDepartureAirportsList(recentFromList);
          }
        }
        return;
      }
      const args = {
        num_items: 5,
        term,
      };
      const { data } = await searchAirports(args);
      setSearchDepartureAirportsList(
        data?.data?.length ? data.data : LIST_SEARCH
      );
    }),
    []
  );

  const actionDestinationSearchAirports = useCallback(
    debounce(async (term) => {
      if (!term) {
        const ISSERVER = typeof window === "undefined";
        if (!ISSERVER) {
          const recentList = JSON.parse(localStorage.getItem(SEARCH_RECENT));
          if (recentList?.length) {
            const recentTo = recentList.slice(0, 4).map((item) => {
              return {
                code: item?.destination,
                location: item?.flightTo,
                name: item?.destinationName || item?.flightTo,
              };
            });
            let codes = [];
            const recentToList = recentTo.filter((item) => {
              if (codes?.includes(item?.code)) {
                return false;
              } else {
                codes.push(item?.code);
                return true;
              }
            });
            setSearchDestinationAirportsList(recentToList);
          }
        }
        return;
      }
      const args = {
        num_items: 5,
        term,
      };
      const { data } = await searchAirports(args);
      setSearchDestinationAirportsList(
        data?.data?.length ? data.data : LIST_SEARCH
      );
    }),
    []
  );

  const seatTypeName = (seatId) => {
    return SEAT_TYPE_LIST.find((item) => item?.cid === seatId)?.name;
  };

  const switchFunction = () => {
    [destinationData.origin, destinationData.destination] = [
      destinationData.destination,
      destinationData.origin,
    ];
    setDestinationData({ ...destinationData });
  };

  const totalGuest =
    paramsSearch.adultCount +
    paramsSearch.childCount +
    paramsSearch.infantCount;

  const actionsGeneralInformation = async () => {
    const { data } = await getGeneralInformation();
    const arr3 = seatTypeList.map((item, i) =>
      Object.assign({}, item, data?.data?.ticketclass[i])
    );
    setSeatTypeList([...arr3]);
  };
  const actionSearch = () => {
    if (!isSearchParamsValid) {
      return;
    }
    setTypeDropDown(null);
    //Save history
    let tempSearchHistory = [];
    if (JSON.parse(localStorage.getItem(SEARCH_RECENT))) {
      tempSearchHistory = tempSearchHistory.concat(
        JSON.parse(localStorage.getItem(SEARCH_RECENT))
      );
    }
    let tempArr = tempSearchHistory.slice(0, 9);
    let formatDateFromHistory = startDate
      ? moment(startDate, DATE_FORMAT_BACK_END).format(DATE_MONTH)
      : moment().format(DATE_MONTH);
    let formatDateFromString = startDate
      .locale("vi_VN")
      .format("ddd")
      .replace("T", "Thứ ")
      .replace("CN", "Chủ Nhật");
    let strReturnDate = "";
    let formatDateToHistory = "";
    if (endDate) {
      formatDateToHistory = endDate
        ? moment(endDate, DATE_FORMAT_BACK_END).format(DATE_MONTH)
        : moment().format(DATE_MONTH);
      let formatDateToString = endDate
        .locale("vi_VN")
        .format("ddd")
        .replace("T", "Thứ ")
        .replace("CN", "Chủ Nhật");
      strReturnDate = ` - ${formatDateToString}, ${formatDateToHistory}`;
    }
    const strFrom = `${destinationData.origin?.location} (${destinationData.origin?.code})`;
    const strTo = `${destinationData.destination?.location} (${destinationData.destination?.code})`;
    const checkLoop = tempSearchHistory.find(
      (el) =>
        el.from === strFrom &&
        el.to === strTo &&
        el.guest === totalGuest &&
        el.type === isRoundTrip
    );
    tempArr.unshift({
      adult: paramsSearch.adultCount,
      child: paramsSearch.childCount,
      baby: paramsSearch.infantCount,
      origin: destinationData.origin?.code,
      destination: destinationData.destination?.code,
      originName: destinationData.origin?.name,
      destinationName: destinationData.destination?.name,
      flightFrom: destinationData.origin?.location,
      flightTo: destinationData.destination?.location,
      from: strFrom,
      to: strTo,
      date: `${formatDateFromString}, ${formatDateFromHistory} ${strReturnDate}`,
      dateDep: startDate
        ? moment(startDate, DATE_FORMAT_BACK_END).format(DATE_FORMAT)
        : moment().format(DATE_FORMAT),
      dateRet: endDate
        ? moment(endDate, DATE_FORMAT_BACK_END).format(DATE_FORMAT)
        : undefined,
      guest: totalGuest,
      seatCode: seatTypeList
        .map((el) => (el?.cid === paramsSearch.seatSearch ? el?.code : ""))
        .filter((element) => element),
      seat: seatTypeList
        .map((el) => (el?.cid === paramsSearch.seatSearch ? el?.i_name : ""))
        .filter((element) => element),
      type: isRoundTrip,
    });
    localStorage.setItem(SEARCH_PREV, JSON.stringify(tempArr.slice(0, 1)));
    if (!checkLoop) {
      localStorage.setItem(SEARCH_RECENT, JSON.stringify(tempArr));
    }
    if (isRoundTrip) {
      gtm.addEventGtm(listEventFlight.FlightSearchRoundTrip, {
        cityName: destinationData?.destination?.code || "",
      });
    } else {
      gtm.addEventGtm(listEventFlight.FlightSearchOneDirection, {
        cityName: destinationData?.destination?.code || "",
      });
    }

    router.push({
      pathname: "/ve-may-bay/result/outbound",
      query: {
        ...paramsSearch,
        ...{
          seatSearch: seatTypeList
            .map((el) => (el?.cid === paramsSearch.seatSearch ? el?.code : ""))
            .filter((element) => element),
          departureDate: startDate
            ? moment(startDate).format(DATE_FORMAT_BACK_END)
            : moment().format(DATE_FORMAT_BACK_END),
          returnDate:
            endDate && isRoundTrip
              ? moment(endDate).format(DATE_FORMAT_BACK_END)
              : undefined,
          origin_code: destinationData.origin?.code,
          origin_location: destinationData.origin?.location,
          destination_code: destinationData.destination?.code,
          destination_location: destinationData.destination?.location,
          originName: destinationData.origin?.name,
          destinationName: destinationData.destination?.name,
        },
      },
    });
  };

  const parseDateText = (date) => {
    return `${moment(date)
      .locale("vi_VN")
      .format("ddd")}, ${moment(date).format("DD")} Tháng ${moment(date).format(
      "MM"
    )}`;
  };

  const hightLightText = (text) => {
    if (text && text.indexOf("(") >= 0 && text.indexOf(")" > 0)) {
      return `<strong>${text.replace("(", "</strong>(")}`;
    }
    return text;
  };

  const handleClearHistory = () => {
    localStorage.removeItem(SEARCH_RECENT);
    if (!departure) {
      setSearchDepartureAirportsList(LIST_SEARCH);
    }
    if (!destination) {
      setSearchDestinationAirportsList(LIST_SEARCH);
    }
  };

  useEffect(() => {
    actionsGeneralInformation();
  }, []);

  function debounce(func, timeout = 200) {
    let timer;
    return (...args) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        func.apply(this, args);
      }, timeout);
    };
  }
  useEffect(() => {
    actionDepartureSearchAirports(departure);
  }, [departure, actionDepartureSearchAirports]);

  useEffect(() => {
    actionDestinationSearchAirports(destination);
  }, [destination, actionDestinationSearchAirports]);

  useEffect(() => {
    const roundTrip =
      !!departure &&
      !!destination &&
      !!startDate &&
      destinationData?.destination?.code !== destinationData?.origin?.code &&
      (!isRoundTrip || (isRoundTrip && !!endDate)) &&
      !!destinationData?.destination?.code &&
      !!destinationData?.origin?.code;
    setIsSearchParamsValid(roundTrip);
  }, [
    destinationData,
    departure,
    destination,
    startDate,
    endDate,
    isRoundTrip,
  ]);

  useEffect(() => {
    TYPE_DROPDOWN.CHECK_IN === typeDropDown &&
      isRoundTrip &&
      setTypeDropDown(TYPE_DROPDOWN.CHECK_OUT);
  }, [startDate]);

  useEffect(() => {
    if (
      isRoundTrip &&
      startDate &&
      endDate &&
      (typeDropDown === TYPE_DROPDOWN.CHECK_IN ||
        typeDropDown === TYPE_DROPDOWN.CHECK_OUT)
    ) {
      setTypeDropDown(null);
    } else if (
      !isRoundTrip &&
      startDate &&
      (typeDropDown === TYPE_DROPDOWN.CHECK_IN ||
        typeDropDown === TYPE_DROPDOWN.CHECK_OUT)
    ) {
      setTypeDropDown(null);
    }
  }, [startDate, endDate]);

  return (
    <Box style={{ width: "100%" }}>
      <Box className={classes.headerContainer}>
        <OutsideClickHandler
          onOutsideClick={() => {
            setTypeDropDown(null);
          }}
        >
          <Box
            className={classes.searchContainer}
            onClick={(e) => e.stopPropagation()}
            style={{
              border: type === "listing" ? "1px solid #CBD5E0" : "none",
              marginTop: type === "listing" ? 10 : 0,
              boxShadow:
                type === "listing"
                  ? "0px 10px 10px rgba(26, 32, 44, 0.05)"
                  : "0px 15px 15px rgba(0, 0, 0, 0.2)",
            }}
          >
            <Box className={classes.textItemInput}>
              <Box className={classes.wrapInput}>
                <Box className={classes.textSub}>Từ</Box>
                <Box className={classes.text} style={{ position: "relative" }}>
                  <div
                    className={classes.fakeInput}
                    style={{
                      opacity:
                        TYPE_DROPDOWN.DEPARTURE === typeDropDown || !departure
                          ? 0
                          : 1,
                    }}
                    dangerouslySetInnerHTML={{
                      __html: hightLightText(departure),
                    }}
                  />
                  <Input
                    className={classes.input}
                    style={{
                      opacity:
                        TYPE_DROPDOWN.DEPARTURE === typeDropDown || !departure
                          ? 1
                          : 0,
                    }}
                    placeholder="Thành phố, sân bay"
                    disableUnderline
                    onFocus={(e) => {
                      e.target.select();
                      setTypeDropDown(TYPE_DROPDOWN.DEPARTURE);
                    }}
                    name="departure"
                    onChange={handleChangeInput}
                    value={
                      TYPE_DROPDOWN.DEPARTURE === typeDropDown ? departure : ""
                    }
                    autoComplete="off"
                  />
                  <div
                    className={classes.dividerSearch}
                    style={{
                      background: getDividerColor(TYPE_DROPDOWN.DEPARTURE),
                    }}
                  />
                </Box>
                {typeDropDown === TYPE_DROPDOWN.DEPARTURE && (
                  <Box
                    className={classes.subItem}
                    style={{ width: 415, left: -20, zIndex: 1 }}
                  >
                    <SearchAirportContent
                      setDestinationData={(data) => {
                        typeDropDown === TYPE_DROPDOWN.DEPARTURE
                          ? setDestinationData({
                              ...destinationData,
                              origin: data,
                            })
                          : setDestinationData({
                              ...destinationData,
                              destination: data,
                            });
                        setTypeDropDown(null);
                      }}
                      recentSearchAirport={searchDepartureAirportsList}
                      inputSearch={departure}
                      handleClearHistory={handleClearHistory}
                    />
                  </Box>
                )}
              </Box>
            </Box>
            <Box className={classes.switchIcon}>
              <IconSwitch
                style={{ height: 32, cursor: "pointer" }}
                onClick={switchFunction}
              />
            </Box>
            <Box>
              <Box className={classes.wrapInput}>
                <Box className={classes.textSub}>Đến</Box>
                <Box className={classes.text} style={{ position: "relative" }}>
                  <div
                    className={classes.fakeInput}
                    style={{
                      opacity:
                        TYPE_DROPDOWN.DESTINATION === typeDropDown ||
                        !destination
                          ? 0
                          : 1,
                    }}
                    dangerouslySetInnerHTML={{
                      __html: hightLightText(destination),
                    }}
                  />
                  <Input
                    className={classes.input}
                    style={{
                      opacity:
                        TYPE_DROPDOWN.DESTINATION === typeDropDown ||
                        !destination
                          ? 1
                          : 0,
                    }}
                    placeholder="Thành phố, sân bay"
                    value={destination}
                    disableUnderline
                    onFocus={(e) => {
                      e.target.select();
                      setTypeDropDown(TYPE_DROPDOWN.DESTINATION);
                    }}
                    name="destination"
                    onChange={handleChangeInput}
                    autoComplete="off"
                  />
                  <div
                    className={classes.dividerSearch}
                    style={{
                      background: getDividerColor(TYPE_DROPDOWN.DESTINATION),
                    }}
                  />
                </Box>
                {typeDropDown === TYPE_DROPDOWN.DESTINATION && (
                  <Box
                    className={classes.subItem}
                    style={{ width: 415, left: -20, zIndex: 1 }}
                  >
                    <SearchAirportContent
                      setDestinationData={(data) => {
                        typeDropDown === TYPE_DROPDOWN.DEPARTURE
                          ? setDestinationData({
                              ...destinationData,
                              origin: data,
                            })
                          : setDestinationData({
                              ...destinationData,
                              destination: data,
                            });
                        setTypeDropDown(null);
                      }}
                      recentSearchAirport={searchDestinationAirportsList}
                      inputSearch={destination}
                      handleClearHistory={handleClearHistory}
                    />
                  </Box>
                )}
              </Box>
            </Box>

            <Box className={classes.textItemCheckIn}>
              <Box
                display="flex"
                alignItems="center"
                style={{ marginLeft: 24, position: "relative", minWidth: 275 }}
              >
                <Box
                  className={classes.textSub}
                  pr={4}
                  onClick={() => setTypeDropDown(TYPE_DROPDOWN.CHECK_IN)}
                >
                  {listString.IDS_TEXT_DAY_OUT}
                  <Box className={classes.textMain}>
                    {moment(startDate)
                      .locale("vi_VN")
                      .format("ddd")}
                    ,&nbsp;
                    {moment(startDate).format("DD")} Tháng&nbsp;
                    {moment(startDate).format("MM")}
                  </Box>
                  <div
                    className={classes.dividerSearch}
                    style={{
                      background: getDividerColor(TYPE_DROPDOWN.CHECK_IN),
                    }}
                  />
                </Box>
                <Box
                  className={classes.textSub}
                  style={{ position: "relative" }}
                >
                  {listString.IDS_TEXT_DAY_IN}
                  <CustomStyleSwitch
                    checked={isRoundTrip}
                    onChange={(e) => {
                      setEndDate(
                        e.target.checked
                          ? setTypeDropDown(TYPE_DROPDOWN.CHECK_OUT)
                          : null
                      );
                      setIsRoundTrip(e.target.checked);
                    }}
                    color="primary"
                    name="checkedA"
                    inputProps={{ "aria-label": "secondary checkbox" }}
                  />
                  <Box
                    className={classes.textMain}
                    onClick={() => {
                      setTypeDropDown(TYPE_DROPDOWN.CHECK_OUT);
                      setIsRoundTrip(true);
                    }}
                  >
                    {!endDate ? (
                      <div className={classes.placeEndDateWrap}>
                        Chọn ngày về
                      </div>
                    ) : (
                      parseDateText(endDate)
                    )}
                  </Box>
                  <div
                    className={classes.dividerSearch}
                    style={{
                      background: getDividerColor(TYPE_DROPDOWN.CHECK_OUT),
                    }}
                  />
                </Box>

                {(typeDropDown === TYPE_DROPDOWN.CHECK_IN ||
                  typeDropDown === TYPE_DROPDOWN.CHECK_OUT) && (
                  <Box
                    className={classes.subItem}
                    style={{ width: "calc(200% + 150px)", left: -90 }}
                  >
                    <SearchDateContent
                      startDate={startDate}
                      endDate={endDate}
                      setStartDate={setStartDate}
                      setEndDate={setEndDate}
                      isRoundTrip={isRoundTrip}
                      isStartDateSelected={
                        typeDropDown === TYPE_DROPDOWN.CHECK_IN
                      }
                    />
                  </Box>
                )}
              </Box>
            </Box>

            <Box className={classes.textItemCheckIn}>
              <Box
                display="flex"
                alignItems="center"
                style={{ marginLeft: 24, position: "relative" }}
                onClick={() => setTypeDropDown(TYPE_DROPDOWN.SEAT)}
              >
                <Box className={classes.wrapInput} style={{}}>
                  <Box className={classes.textSub}>
                    {listString.IDS_MT_TEXT_NUMBER_CUSTOMER_SEAT}
                  </Box>
                  <Box
                    className={classes.textMain}
                    style={{
                      textAlign: "left",
                      width: 180,
                      textOverflow: "ellipsis",
                      overflow: "hidden",
                      whiteSpace: "nowrap",
                    }}
                  >
                    {totalGuest} {listString.IDS_MT_TEXT_SINGLE_CUSTOMER},{" "}
                    {seatTypeName(paramsSearch.seatSearch)?.toLowerCase()}
                  </Box>
                  <div
                    className={classes.dividerSearch}
                    style={{ background: getDividerColor(TYPE_DROPDOWN.SEAT) }}
                  />
                </Box>
                {typeDropDown === TYPE_DROPDOWN.SEAT && (
                  <Box
                    className={classes.subItem}
                    style={{ width: 704, right: -135, left: "auto" }}
                  >
                    <SearchSeat
                      paramsSearch={paramsSearch}
                      setParamsSearch={setParamsSearch}
                    />
                  </Box>
                )}
              </Box>
            </Box>
            <Button
              variant="contained"
              color="secondary"
              style={{
                width: 100,
                height: 56,
                cursor: isSearchParamsValid ? "pointer" : "not-allowed",
              }}
              onClick={actionSearch}
            >
              <IconFlightSearch
                className="svgFillAll"
                style={{ stroke: "white" }}
              />
            </Button>
          </Box>
        </OutsideClickHandler>
      </Box>
    </Box>
  );
};

export default SectionSearchFlight;
