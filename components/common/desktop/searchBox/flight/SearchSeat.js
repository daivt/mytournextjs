import {
  Box,
  Typography,
  Grid,
  makeStyles,
  IconButton,
} from "@material-ui/core";
import { listString, SEAT_TYPE_LIST } from "@utils/constants";
import CheckIcon from "@material-ui/icons/Check";
import clsx from "clsx";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import ButtonComponent from "@src/button/Button";

const useStyles = makeStyles((theme) => ({
  contentContainer: {
    width: "100%",
    color: theme.palette.black.black3,
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    padding: 16,
    zIndex: 1,
  },
  textItem: { fontWeight: 400 },
  searchItem: {
    display: "flex",
    marginBottom: 4,
    marginLeft: -8,
    marginRight: -8,
    padding: "10px 8px",
    alignItems: "center",
    borderRadius: 8,
    cursor: "pointer",
    width: "calc(100% + 16px)",
    justifyContent: "space-between",
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    // "&:hover": { background: "#EDF2F7" },
  },
  leftItem: { display: "flex" },
  rightAction: {
    display: "flex",
    alignItems: "center",
    ...theme.typography.subtitle1,
  },
  iconAction: {
    width: 32,
    height: 32,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "50%",
    background: theme.palette.gray.grayLight23,
    color: theme.palette.black.black3,
    cursor: "pointer",
  },
  valueItem: { width: 57 },
  explain: {
    color: theme.palette.gray.grayDark7,
    display: "inline-block",
    marginLeft: 5,
  },
  textLeft: {
    textAlign: "left",
  },
  shortDes: {
    color: theme.palette.gray.grayDark7,
  },
  activeS: {
    color: theme.palette.blue.blueLight8,
  },
  rightBox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    color: theme.palette.blue.blueLight8,
  },
  hideIcon: {
    display: "none",
  },
  showIcon: {
    display: "flex",
  },
  seatItem: {
    cursor: "pointer",
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
    padding: "10px 0",
    "&:last-child": {
      border: "none",
    },
  },
  notAvail: {
    color: theme.palette.gray.grayLight24,
  },
  slBox: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  rightBox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  btn: {
    backgroundColor: theme.palette.gray.grayLight22,
    width: 32,
    height: 32,
    "&:hover": {
      backgroundColor: theme.palette.gray.grayLight22,
    },
    "& svg": {
      width: 18,
    },
  },
  numAdult: {
    paddingTop: 10,
    display: "flex",
  },
  btnCancel: {
    backgroundColor: theme.palette.gray.grayLight22,
    color: theme.palette.black.black3,
    marginRight: 12,
  },
  btnSubmit: {
    backgroundColor: theme.palette.pink.main,
    color: theme.palette.white.main,
  },
  btnBottom: {
    paddingTop: 8,
    display: "flex",
    flexDirection: "row",
  },
  disabledButtonIcon: {
    backgroundColor: theme.palette.gray.grayLight23,
    color: theme.palette.gray.grayLight24,
    "&:disabled": {
      backgroundColor: theme.palette.gray.grayLight23,
      color: theme.palette.gray.grayLight24,
    },
  },
  flRight: {
    display: "flex",
    justifyContent: "flex-end",
  },
}));

const RenderButton = (props) => {
  const { input, value, type } = props;
  const classes = useStyles();
  return (
    <Box className={classes.rightBox}>
      {type ? (
        <IconButton
          className={classes.btn}
          onClick={() => props.onChange(input, value + 1)}
          disabled={
            (input === "childCount" && value === 8) ||
            (input === "adultCount" && value === 9)
          }
          classes={{ disabled: classes.disabledButtonIcon }}
        >
          <AddIcon />
        </IconButton>
      ) : (
        <IconButton
          className={classes.btn}
          onClick={() => props.onChange(input, value - 1)}
          disabled={
            (input === "adultCount" && value === 1) ||
            ((input === "childCount" || input === "infantCount") && value === 0)
          }
          classes={{ disabled: classes.disabledButtonIcon }}
        >
          <RemoveIcon />
        </IconButton>
      )}
    </Box>
  );
};

const SearchSeatAndPerson = ({ seat, paramsSearch, setParamsSearch }) => {
  const classes = useStyles();

  const dataTemp = SEAT_TYPE_LIST;

  const handleChangeValue = (field, value) => {
    const maxLimit = 9;
    if (value < 0) return false;
    if (field === "adultCount") {
      if (
        value + paramsSearch.childCount >= maxLimit &&
        paramsSearch.childCount > maxLimit - value
      ) {
        setParamsSearch({ ...paramsSearch, adultCount: value, childCount: maxLimit - value });
      } else {
        if (value < paramsSearch.infantCount) {
          setParamsSearch({ ...paramsSearch, adultCount: value, infantCount: value });
        } else {
          setParamsSearch({ ...paramsSearch, adultCount: value });
        }
      }
    }
    if (field === "childCount") {
      if (
        value + paramsSearch.adultCount >= maxLimit &&
        paramsSearch.adultCount > maxLimit - value &&
        paramsSearch.adultCount - 1 >= paramsSearch.infantCount
      ) {
        setParamsSearch({ ...paramsSearch, childCount: value, adultCount: maxLimit - value });
      } else if(paramsSearch.adultCount + value - 1 < maxLimit) {
        setParamsSearch({ ...paramsSearch, childCount: value });
      }
    }
    if (field === "infantCount") {
      if (value > paramsSearch.adultCount) {
        setParamsSearch({
          ...paramsSearch,
          infantCount: paramsSearch.adultCount,
        });
      } else {
        setParamsSearch({ ...paramsSearch, infantCount: value });
      }
    }
  };

  return (
    <Box className={classes.contentContainer}>
      <Grid container spacing={3}>
        <Grid item md={6}>
          <Typography className={classes.textLeft} variant="subtitle2">
            {listString.IDS_TEXT_COUNT_CUSTOMER}
          </Typography>
          <Box className={classes.searchItem}>
            <Box className={classes.leftItem}>
              <Typography variant="subtitle2" className={classes.textItem}>
                {listString.IDS_MT_TEXT_ADULT}
              </Typography>
            </Box>
            <Box className={classes.rightAction}>
              <Box>
                <RenderButton
                  input="adultCount"
                  type={0}
                  value={paramsSearch.adultCount}
                  onChange={handleChangeValue}
                />
              </Box>
              <Box className={classes.valueItem}>{paramsSearch.adultCount}</Box>
              <Box>
                <RenderButton
                  input="adultCount"
                  type={1}
                  value={paramsSearch.adultCount}
                  onChange={handleChangeValue}
                />
              </Box>
            </Box>
          </Box>
          <Box className={classes.searchItem}>
            <Box className={classes.leftItem}>
              <Typography variant="subtitle2" className={classes.textItem}>
                {listString.IDS_MT_TEXT_CHILDREN}
                <span className={classes.explain}>
                  {listString.IDS_MT_TEXT_CHILDREN_AGE}
                </span>
              </Typography>
            </Box>
            <Box className={classes.rightAction}>
              <Box>
                <Box>
                  <RenderButton
                    input="childCount"
                    type={0}
                    value={paramsSearch.childCount}
                    onChange={handleChangeValue}
                  />
                </Box>
              </Box>
              <Box className={classes.valueItem}>{paramsSearch.childCount}</Box>
              <RenderButton
                input="childCount"
                type={1}
                value={paramsSearch.childCount}
                onChange={handleChangeValue}
              />
            </Box>
          </Box>
          <Box className={classes.searchItem}>
            <Box className={classes.leftItem}>
              <Typography variant="subtitle2" className={classes.textItem}>
                {listString.IDS_MT_TEXT_BABY}
                <span className={classes.explain}>
                  {listString.IDS_MT_TEXT_BABY_AGE}
                </span>
              </Typography>
            </Box>
            <Box className={classes.rightAction}>
              <RenderButton
                input="infantCount"
                type={0}
                value={paramsSearch.infantCount}
                onChange={handleChangeValue}
              />
              <Box className={classes.valueItem}>
                {paramsSearch.infantCount}
              </Box>
              <RenderButton
                input="infantCount"
                type={1}
                value={paramsSearch.infantCount}
                onChange={handleChangeValue}
              />
            </Box>
          </Box>
        </Grid>
        <Grid item md={6}>
          <Typography variant="subtitle2" className={classes.textLeft}>
            {listString.IDS_MT_TEXT_FILTER_SEAT_TYPE}
          </Typography>
          <Box className={(classes.container, classes.textLeft)}>
            {dataTemp.map((el, index) => (
              <Grid container className={classes.seatItem} key={index} 
              onClick={() => {
                setParamsSearch({
                  ...paramsSearch,
                  seatSearch: el?.cid,
                });
              }}>
                <Grid
                  item
                  lg={10}
                  md={10}
                  sm={10}
                  xs={10}
                >
                  <Box className={classes.leftBox}>
                    <Typography
                      className={clsx(
                        paramsSearch.seatSearch === el?.cid && classes.activeS
                      )}
                      variant="body1"
                    >
                      {el.name}
                    </Typography>
                    <Typography className={classes.shortDes} variant="caption">
                      {el.description}
                    </Typography>
                  </Box>
                </Grid>
                <Grid
                  item
                  lg={2}
                  md={2}
                  sm={2}
                  xs={2}
                  className={classes.rightBox}
                >
                  <Box>
                    <CheckIcon
                      className={clsx(
                        paramsSearch.seatSearch === el?.cid && classes.showIcon,
                        classes.hideIcon
                      )}
                    />
                  </Box>
                </Grid>
              </Grid>
            ))}
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default SearchSeatAndPerson;
