import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { listString } from "@utils/constants";
import { IconHistory, IconLocationAirport } from "@public/icons";
import { isEmpty } from "@utils/helpers";
import NoResultSearchModal from "@components/common/modal/hotel/componentDestinationSearchModal/NoResultSearchModal";
import { ChipDes } from "@components/common/mobile/SearchDestinationsBox";

const useStyles = makeStyles((theme) => ({
  contentContainer: {
    width: 415,
    color: theme.palette.black.black3,
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    padding: 16,
    zIndex: 1,
  },
  textTitle: {
    fontWeight: 600,
    fontSize: 14,
    lineHeight: "17px",
    marginBottom: 8,
  },
  textItem: { marginLeft: 12 },
  searchItem: {
    display: "flex",
    marginBottom: 4,
    alignItems: "center",
    marginLeft: -8,
    marginRight: -8,
    padding: "10px 8px",
    borderRadius: 8,
    cursor: "pointer",
    width: "calc(100% + 16px)",
    "&:hover": { background: theme.palette.gray.grayLight22 },
  },
  noData: { marginTop: 30, marginBottom: 30 },
  clearHistory: {
    color: theme.palette.blue.blueLight8,
    ...theme.typography.subtitle2,
    fontWeight: 400,
    marginLeft: 30,
    marginTop: 5,
    marginBottom: 10,
    cursor: "pointer"
  }
}));

const LIST_SEARCH = [
  { code: "HAN", location: "Hà Nội", name: "Nội Bài" },
  { code: "SGN", location: "Hồ Chí Minh", name: "Tân Sơn Nhất" },
  { code: "DAD", location: "Đà Nẵng", name: "Quốc tế Đà Nẵng" },
  { code: "CXR", location: "Nha Trang", name: "Cam Ranh" },
  { code: "PQC", location: "Phú Quốc", name: "Quốc tế Phú Quốc" },
  { code: "HUI", location: "Huế", name: "Quốc tế Phú Bài" },
  { code: "HPH", location: "Hải Phòng", name: "Quốc tế Cát bi" },
];

const SearchAirportContent = ({
  recentSearchAirport = [],
  inputSearch = "",
  setDestinationData,
  handleClearHistory=() => {}
}) => {
  const classes = useStyles();

  if (!isEmpty(inputSearch.trim()) && isEmpty(recentSearchAirport)) {
    return (
      <NoResultSearchModal keyword={inputSearch} className={classes.noData} />
    );
  }
  if (!isEmpty(recentSearchAirport) && !isEmpty(inputSearch.trim())) {
    return (
      <Box className={classes.contentContainer}>
        {recentSearchAirport.map((el, index) => (
          <Box
            className={classes.searchItem}
            key={index}
            onClick={() => setDestinationData(el)}
          >
            {inputSearch ? <IconLocationAirport /> : <IconHistory /> }
            <Box style={{ textAlign: "left" }}>
              <Typography
                variant="subtitle2"
                style={{ fontWeight: 400 }}
                className={classes.textItem}
              >
                {el.location || ""}
                {/* {hightLightText(el.name || "", inputSearch.trim() || "")} */}
              </Typography>
              <Typography variant="body2" className={classes.textItem}>
                {el.code} - Sân bay {el.name}
              </Typography>
            </Box>
          </Box>
        ))}
      </Box>
    );
  }
  return (
    <Box className={classes.contentContainer}>
      <Typography variant="body2" className={classes.textTitle}>
        {listString.IDS_TEXT_RECENT_SEARCH}
      </Typography>
      {recentSearchAirport.map((el, index) => (
        <Box
          className={classes.searchItem}
          key={index}
          onClick={() => setDestinationData(el)}
        >
          <IconHistory />
          <Box style={{ textAlign: "left" }}>
            <Typography
              variant="subtitle2"
              style={{ fontWeight: 400 }}
              className={classes.textItem}
            >
              {el.location || ""}
              {/* {hightLightText(el.name || "", inputSearch.trim() || "")} */}
            </Typography>
            <Typography variant="body2" className={classes.textItem}>
              {el.code} - {el.name}
            </Typography>
          </Box>
        </Box>
      ))}
      {JSON.parse(localStorage?.getItem('SEARCH_RECENT'))?.length > 0 && <Typography onClick={handleClearHistory} className={classes.clearHistory}>Xóa lịch sử tìm kiếm</Typography>}
      <Typography
        variant="body2"
        className={classes.textTitle}
        style={{ marginTop: 8 }}
      >
        {listString.IDS_MT_TEXT_COMMON}
      </Typography>
      <Box flexWrap="wrap" display="flex" mt={1}>
        {LIST_SEARCH.map((element, index) => {
          return (
            <Box
              mr={1}
              mb={1}
              key={index}
              onClick={() => setDestinationData(element)}
            >
              <ChipDes style={{ cursor: "pointer" }} label={element.location} />
            </Box>
          );
        })}
      </Box>
    </Box>
  );
};

export default SearchAirportContent;
