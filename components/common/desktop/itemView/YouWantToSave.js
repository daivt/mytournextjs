import { useState } from "react";
import { Box, Grid, Typography } from "@material-ui/core";
import Input from "@material-ui/core/Input";
import { makeStyles, useTheme } from "@material-ui/core/styles";

import ButtonComponent from "@src/button/Button";
import { listIcons, listString, prefixUrlIcon } from "@utils/constants";
import Image from "@src/image/Image";
import AuthWrapper from "@components/layout/desktop/components/AuthWrapper";

const useStyles = makeStyles((theme) => ({
  container: {
    boxShadow: "0px 8px 8px rgba(0, 0, 0, 0.03)",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    background: theme.palette.white.main,
    display: "flex",
    borderRadius: 16,
    justifyContent: "space-between",
  },
  wrapBoxIcon: {
    margin: "21px 0 21px 24px",
  },
  wrapToSave: {
    margin: "19px 0 0 29px",
  },
  description: {
    fontSize: 14,
    lineHeight: "22px",
    fontWeight: 400,
    marginTop: 6,
    paddingBottom: 18,
  },
  input: {
    fontSize: 16,
    lineHeight: "19px",
    fontWeight: 600,
    background: theme.palette.gray.grayLight22,
    color: theme.palette.black.black3,
    borderRadius: 8,
    height: 44,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    zIndex: 2,
    width: 350,
    paddingLeft: 12,
    "&::placeholder": {
      fontWeight: 400,
    },
  },
  rootForm: {
    display: "flex",
    alignItems: "center",
    margin: "31px 31px  31px 58px",
  },
  wrapContent: {
    display: "flex",
  },
  wrapBtn: {
    marginLeft: -5,
    whiteSpace: "nowrap",
  },
  iconMailBox: {
    height: 64,
    width: 64,
  },
}));
const YouWantToSave = () => {
  const classes = useStyles();
  const theme = useTheme();
  const [inputPhone, setInputPhone] = useState("");
  return (
    <>
      <Grid container>
        <Box className={classes.container}>
          <Grid item lg={7} sm={7} md={7} className={classes.wrapContent}>
            <Box className={classes.wrapBoxIcon}>
              <Image
                srcImage={`${prefixUrlIcon}${listIcons.IconBoxMail}`}
                className={classes.iconMailBox}
              />
            </Box>
            <Box className={classes.wrapToSave}>
              <Typography variant="subtitle1">
                {listString.IDS_MT_TEXT_YOU_WANT_TO_SAVE}
              </Typography>
              <Typography className={classes.description}>
                {listString.IDS_MT_TEXT_YOU_PRESS_PHONE_NUMBER}
              </Typography>
            </Box>
          </Grid>

          <Grid item lg={5} sm={5} md={5}>
            <Box className={classes.wrapInput}>
              <form className={classes.rootForm} noValidate autoComplete="off">
                <Input
                  className={classes.input}
                  placeholder={
                    listString.IDS_MT_TEXT_PLACEHOLDER_ENTER_PHONE_NUMBER
                  }
                  disableUnderline
                  value={inputPhone}
                  onChange={(e) => setInputPhone(e.target.value)}
                />
                <AuthWrapper
                  typeModal="MODAL_REGISTER"
                  isScrollTop
                  phoneNumberRegister={inputPhone}
                >
                  <ButtonComponent
                    backgroundColor={theme.palette.secondary.main}
                    height={44}
                    fontSize={16}
                    fontWeight={600}
                    borderRadius={8}
                    width={101}
                    margin={-10}
                    className={classes.wrapBtn}
                  >
                    {listString.IDS_MT_TEXT_SIGNUP}
                  </ButtonComponent>
                </AuthWrapper>
              </form>
            </Box>
          </Grid>
        </Box>
      </Grid>
    </>
  );
};

export default YouWantToSave;
