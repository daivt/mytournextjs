import { Box, makeStyles, Typography } from "@material-ui/core";
import React from "react";
import { listString } from "@utils/constants";
import moment from "moment";
import { DATE_FORMAT, DATE_FORMAT_BACK_END } from "@utils/moment";
import { useRouter } from "next/router";
import { handleColorAirlineFrom } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  location: {
    color: theme.palette.black.black3,
  },
  flightInfo: {
    padding: 15,
    background: theme.palette.white.main,
  },
  wrapItem: {
    borderRadius: 8,
    overflow: "hidden",
    cursor: "pointer",
  },
  time: {
    color: theme.palette.gray.grayDark8,
    fontWeight: "normal",
  },
  price: {
    color: theme.palette.black.black3,
    marginTop: 8,
    textAlign: "right",
  },
  airplaneCompany: {
    color: theme.palette.black.black3,
    fontWeight: 400,
  },
  airplaneName: {
    color: theme.palette.gray.grayDark8,
    fontWeight: 400,
  },
  bestPrice: {
    borderRadius: 4,
    padding: "3px 6px",
    ...theme.typography.body2,
  },
  wrapInfo: {
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    paddingTop: 16,
    marginTop: 10,
  },
}));
const FlightHomeSeo = ({
  flightTo,
  flightFrom,
  airlineLogo,
  airlineName,
  date,
  mainPrice,
  farePrice,
  airlineId,
  origin,
  destination,
}) => {
  const classes = useStyles();

  const router = useRouter();

  const handleClickTicket = (item) => {
    router.push({
      pathname: "/ve-may-bay/result/outbound",
      query: {
        adultCount: 1,
        childCount: 0,
        infantCount: 0,
        seatSearch: item?.seat || "economy",
        departureDate: item?.date
          ? moment(item.date, DATE_FORMAT).format(DATE_FORMAT_BACK_END)
          : moment().format(DATE_FORMAT_BACK_END),
        returnDate: "",
        origin_code: item?.origin,
        origin_location: item?.flightFrom,
        destination_code: item?.destination,
        destination_location: item?.flightTo,
      },
    });
  };
  return (
    <Box
      onClick={() => {
        handleClickTicket({ date, flightFrom, flightTo, origin, destination });
      }}
      className={classes.wrapItem}
    >
      <Box className={classes.flightInfo}>
        <Box display="flex" style={{ marginBottom: 5 }}>
          <Typography variant="subtitle1">
            {moment(date, DATE_FORMAT_BACK_END).format("ddd, D ")} tháng{" "}
            {moment(date, DATE_FORMAT_BACK_END).format("M")}
          </Typography>
        </Box>
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="flex-start"
          className={classes.wrapInfo}
        >
          <Box display="flex" alignItems="center" style={{ marginTop: 3 }}>
            <img
              style={{ width: 24, height: "auto", marginRight: 8 }}
              src={airlineLogo}
              alt=""
            />
            <Box>
              <Typography
                variant="subtitle2"
                className={classes.airplaneCompany}
              >
                {airlineName}
              </Typography>
            </Box>
          </Box>
          <Box>
            <Box
              display="flex"
              className={classes.bestPrice}
              style={{
                color: handleColorAirlineFrom(airlineId),
                border: `1px solid ${handleColorAirlineFrom(airlineId)}`,
              }}
              alignItems="center"
            >
              <img
                style={{ width: 18, height: "auto", marginRight: 8 }}
                src={airlineLogo}
                alt=""
              />
              <Typography variant="body2">
                {listString.IDS_TEXT_FLIGHT_BEST_PRICE}
              </Typography>
            </Box>
            <Typography variant="subtitle1" className={classes.price}>
              {farePrice.formatMoney()}đ
            </Typography>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default FlightHomeSeo;
