import { Box, makeStyles, Typography } from "@material-ui/core";
import Link from "@src/link/Link";
import Image from "@src/image/Image";
import { useRouter } from "next/router";
import { routeStatic } from "utils/constants";
import { IconStar } from "@public/icons";
import Vote from "@components/common/desktop/vote/Vote";

const useStyles = makeStyles((theme) => ({
  image: { height: 133, width: 178, borderRadius: 8 },
  star: { marginTop: 4, width: 14, height: 14 },
  address: {
    fontSize: 14,
    lineHeight: "17px",
    fontWeight: 400,
    marginTop: 6,
  },
  linkHotelDetail: {
    textDecoration: "none",
    color: "#1A202C",
    "&:hover": { textDecoration: "none" },
  },
  wrapRatingStar: {
    marginTop: 4,
    marginBottom: 13,
    display: "flex",
  },
}));
const arrStar = [1, 2, 3, 4, 5];

const RecentHotelItem = ({ item = {} }) => {
  const classes = useStyles();
  const router = useRouter();
  return (
    <Link
      href={{
        pathname: routeStatic.HOTEL_DETAIL.href,
        query: {
          ...router.query,
          alias: `${item.id}-${item.hotelName.stringSlug()}.html`,
        },
      }}
      className={classes.linkHotelDetail}
    >
      <Box className={classes.container}>
        <Image
          srcImage={item.hotelImage}
          className={classes.image}
          borderRadiusProp="8px"
        />
        <Typography
          variant="subtitle2"
          style={{
            marginTop: "8px",
            width: "178px",
            display: "-webkit-box",
            WebkitLineClamp: 2,
            WebkitBoxOrient: "vertical",
            overflow: "hidden",
            minHeight: 32,
          }}
        >
          {item.hotelName}
        </Typography>
        <Box className={classes.wrapRatingStar}>
          <Vote maxValue={item.hotelStar} value={item.hotelStar} />
        </Box>
        <Typography className={classes.address}>{item.localtion}</Typography>
      </Box>
    </Link>
  );
};

export default RecentHotelItem;
