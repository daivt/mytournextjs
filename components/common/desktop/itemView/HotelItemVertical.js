import { addFavorite, removeFavorite } from "@api/user";
import { Box, IconButton, makeStyles, Typography } from "@material-ui/core";
import { IconHeart, IconSightSeeing, IconStar } from "@public/icons";
import Image from "@src/image/Image";
import Link from "@src/link/Link";
import { listIcons, listString, prefixUrlIcon } from "@utils/constants";
import {
  checkAndChangeCheckIO,
  getMemberDeal,
  getMessageHotelReviews,
  isEmpty,
  paramsDefaultHotel,
} from "@utils/helpers";
import clsx from "clsx";
import cookie from "js-cookie";
import moment from "moment";
import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import {
  LAST_HOTEL_FAVORITES,
  LAST_SEARCH_HISTORY,
  routeStatic,
  TOKEN,
} from "utils/constants";
import Vote from "@components/common/desktop/vote/Vote";

const useStyles = makeStyles((theme) => ({
  linkHotelDetail: {
    textDecoration: "none !important",
  },
  wrapCardHotel: {
    position: "relative",
    overflow: "hidden",
    width: "100%",
    borderRadius: "8px 8px 0px 0px",
  },
  imageHotel: {
    width: 279,
    height: 155,
    borderRadius: "8px 8px 0px 0px",
    position: "relative",
  },
  hotelDefault: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%,-50%)",
  },
  discountPercent: {
    width: 80,
    height: 80,
    borderRadius: "50%",
    background: "rgba(255,18,132,0.9)",
    position: "absolute",
    top: -35,
    left: -23,
  },
  discountPercentNumber: {
    display: "flex",
    alignItems: "center",
    color: theme.palette.white.main,
    marginTop: 46,
    marginLeft: 32,
    position: "absolute",
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
  },
  discountPrice: {
    width: 85,
    background: "rgba(26, 32, 44, 0.7)",
    borderRadius: "0px 8px",
    position: "absolute",
    bottom: 0,
    margin: "6px 6px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    height: 38,
  },
  subPrice: {
    textDecorationLine: "line-through",
    color: theme.palette.white.main,
    opacity: 0.8,
    marginLeft: 8,
    marginTop: 4,
  },
  mainPrice: {
    color: theme.palette.white.main,
    margin: "0 8px",
  },
  iconHeart: {
    position: "absolute",
    top: -1,
    right: 0,
    zIndex: 2,
  },
  wrapHotelInfo: {
    width: 279,
    padding: "0 12px",
    minHeight: 193,
    position: "relative",
  },
  hotelName: {
    color: theme.palette.black.black3,
    marginTop: 12,
    fontSize: 16,
    lineHeight: "19px",
    fontWeight: 600,
    display: "-webkit-box",
    WebkitLineClamp: 2,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
  },
  point: {
    height: 16,
    width: 27,
    fontWeight: 600,
    color: theme.palette.white.main,
    background: theme.palette.blue.blueLight8,
    padding: "2px 4px",
    borderRadius: 4,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    fontSize: 12,
    lineHeight: "14px",
  },
  rateLevel: {
    fontSize: 14,
    marginLeft: 4,
    lineHeight: "16px",
    fontWeight: 400,
    color: "#4A5568",
    display: "flex",
    alignItems: "center",
  },
  wrapPoint: {
    display: "flex",
    alignItems: "center",
    marginTop: 6,
  },
  location: {
    color: theme.palette.black.black3,
    display: "flex",
    alignItems: "center",
    minHeight: 28,
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
  },
  wrapCountBooking: {
    width: 90,
    height: 18,
    border: `1px solid ${theme.palette.green.greenLight7}`,
    color: theme.palette.green.greenLight7,
    borderRadius: 4,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 12,
    marginTop: 12,
  },
  countNumberBooking: {
    marginLeft: 4.7,
    paddingTop: 2,
    paddingRight: 4,
  },
  discountPriceFavorite: {
    color: theme.palette.black.black3,
    fontSize: 16,
    lineHeight: "19px",
    marginTop: 2,
    fontWeight: 600,
    textAlign: "right",
  },
  wrapDiscountPriceFavorite: {
    display: "flex",
    padding: "16px 0 12px 0",
    minHeight: 61,
    flexDirection: "column",
    position: "absolute",
    right: 12,
    bottom: 0,
  },
  discountSubPriceFavorite: {
    textDecorationLine: "line-through",
    marginLeft: 4,
    lineHeight: "12px",
    fontWeight: 400,
    textAlign: "right",
    fontSize: 12,
  },
  wrapFlagTopHotel: {
    background: theme.palette.red.redLight5,
    fontSize: 14,
    height: 20,
    color: "#FFF",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    lineHeight: "16px",
    fontWeight: 600,
    padding: "3px 4px 3px 4px",
  },
  favorite: {
    fill: "#FF1284",
    stroke: theme.palette.white.main,
  },
  notFavorite: {
    fill: "rgba(0, 0, 0, 0.6)",
    stroke: theme.palette.white.main,
  },
  wrapRatingStar: {
    marginTop: 4,
    marginBottom: 2,
    display: "flex",
  },
  container: {
    background: theme.palette.white.main,
    borderRadius: 8,
    width: 279,
    boxShadow: "0px 5px 5px rgba(0, 0, 0, 0.05)",
    margin: "0 auto",
    marginBottom: 10,
  },
  wrapHeaderImage: {
    position: "relative",
  },
  boxTripadvisor: {
    height: 14,
    width: 1,
    background: theme.palette.gray.grayLight23,
    margin: "0 8px",
  },
  textLink: {
    "&:hover": {
      textDecoration: "none",
    },
  },
  backgroundDefault: { background: theme.palette.gray.grayLight22 },
  loginText: {
    fontSize: 16,
    lineHeight: "19x",
    fontWeight: 400,
    color: theme.palette.black.black3,
  },
  lastBooking: {
    background: "#E5F8FE",
    padding: "2px 6px",
    width: "fit-content",
    marginTop: 12,
    borderRadius: 4,
    fontSize: 14,
    lineHeight: "16px",
  },
  lastTime: {
    marginLeft: 2,
  },
  discountMember: {
    marginTop: 6,
    fontSize: 14,
    lineHeight: "17px",
    fontStyle: "italic",
    color: theme.palette.black.black3,
  },
  review: {
    fontSize: 14,
    lineHeight: "16px",
    marginLeft: "4px",
    color: theme.palette.black.black3,
  },
  wrapTags: {
    display: "flex",
    position: "absolute",
    bottom: 6,
    left: 12,
  },
  tags: {
    fontSize: 14,
    height: 20,
    color: "#FFF",
    display: "inline-block",
    borderRadius: 4,
    lineHeight: "20px",
    fontWeight: 600,
    paddingLeft: 4,
    background: "rgb(255, 188, 57)",
    marginRight: 4,
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap",
    maxWidth: 140,
  },
  iconTripadvisorMall: {
    marginRight: 3,
    width: 18,
    height: 18,
  },
}));
const HotelItemVertical = ({
  item = {},
  isShowStatus = false,
  isHotelInterest = true,
  isCountBooking = true,
  isShowDiscountPercent = false,
  isShowDistance = false,
  paramsFilterInit = paramsDefaultHotel(),
  styleTripAdvisor = false,
  stylePoint = "",
  styleMessageReview = "",
  isShowLastBook = false,
}) => {
  const classes = useStyles();
  const [isFavorite, setIsFavorite] = useState(item.isFavoriteHotel);
  const [paramsQuery, setParamsQuery] = useState({});
  useEffect(() => {
    checkFavoriteHotel();
    getParamsQuery();
  }, []);

  useEffect(() => {
    setIsFavorite(item.isFavoriteHotel);
  }, [item]);

  const checkFavoriteHotel = () => {
    const listHotelFavorites =
      JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
    if (isEmpty(cookie.get(TOKEN)) && !isEmpty(listHotelFavorites)) {
      setIsFavorite(listHotelFavorites.includes(item.id));
    }
  };

  const handleFavorite = async (event) => {
    event.preventDefault();
    if (!isEmpty(cookie.get(TOKEN))) {
      changeFavorite();
    } else {
      let listHotelFavoritesTemp =
        JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
      if (isFavorite) {
        listHotelFavoritesTemp = listHotelFavoritesTemp.filter(
          (el) => el !== item.id
        );
      } else {
        listHotelFavoritesTemp.unshift(item.id);
      }
      localStorage.setItem(
        LAST_HOTEL_FAVORITES,
        JSON.stringify(listHotelFavoritesTemp)
      );
      setIsFavorite(!isFavorite);
    }
  };

  const changeFavorite = async () => {
    try {
      if (isFavorite) {
        const { data } = await removeFavorite({ hotelId: item.id });
        if (data.code === 200) {
          setIsFavorite(!isFavorite);
        } else {
          data?.message &&
            enqueueSnackbar(
              data?.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
        }
      } else {
        const { data } = await addFavorite({ hotelId: item.id });
        if (data.code === 200) {
          setIsFavorite(!isFavorite);
        } else {
          data?.message &&
            enqueueSnackbar(
              data?.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
        }
      }
    } catch (error) {}
  };

  const getParamsQuery = () => {
    let paramsQuery = {};
    if (!isEmpty(paramsFilterInit)) {
      paramsQuery = paramsFilterInit;
    } else {
      const lastSearchHistory =
        JSON.parse(localStorage.getItem(LAST_SEARCH_HISTORY)) || {};
      if (!isEmpty(lastSearchHistory)) {
        paramsQuery = lastSearchHistory;
        delete paramsQuery.itemLocationSelect;
      } else {
        paramsQuery = paramsDefaultHotel();
      }
    }
    setParamsQuery(paramsQuery);
  };
  const arrStar = [1, 2, 3, 4, 5];
  return (
    <Link
      href={{
        pathname: routeStatic.HOTEL_DETAIL.href,
        query: {
          alias: `${item.id}-${item?.hotelName?.stringSlug()}.html`,
          checkIn: checkAndChangeCheckIO(paramsFilterInit.checkIn),
          checkOut: checkAndChangeCheckIO(paramsFilterInit.checkOut),
          adults: Number(paramsFilterInit.adults),
          rooms: Number(paramsFilterInit.rooms),
          children: Number(paramsFilterInit.children),
        },
      }}
      target="_blank"
      className={classes.textLink}
    >
      <Box className={classes.container}>
        <Box className={classes.wrapCardHotel}>
          <Box className={classes.wrapHeaderImage}>
            {!isEmpty(item.srcImage) || !isEmpty(item.srcImageAvailability) ? (
              <Image
                srcImage={
                  !isEmpty(item.srcImage)
                    ? item.srcImage
                    : item.srcImageAvailability
                }
                className={classes.imageHotel}
                borderRadiusProp="8px 8px 0px 0px"
              />
            ) : (
              <Box
                className={clsx(classes.imageHotel, classes.backgroundDefault)}
                style={{ position: "relative" }}
              />
            )}

            {isHotelInterest && (
              <Box className={classes.iconHeart}>
                <IconButton onClick={handleFavorite}>
                  <IconHeart
                    className={`svgFillAll ${
                      isFavorite ? classes.favorite : classes.notFavorite
                    }`}
                  />
                </IconButton>
              </Box>
            )}
          </Box>
          {!isEmpty(item.discountPercent) && !item.hiddenPrice && (
            <Box className={classes.discountPercent}>
              <Box component="span" className={classes.discountPercentNumber}>
                {!isEmpty(item.discountPercent) && item.discountPercent}
                <Box component="span" fontSize={12}>
                  %
                </Box>
              </Box>
            </Box>
          )}
          <Box className={classes.wrapTags}>
            {!isEmpty(item.tags) && (
              <Box className={classes.tags}>{item.tags[0].name}</Box>
            )}
            {isShowStatus &&
            !isEmpty(item.lastBookedTime) && //isShowStatus để check title là : Bán chạy, quan tâm, tìm kiếm nhiều
              moment().diff(moment(item.lastBookedTime)) <= 7200000 && (
                <Box className={classes.wrapFlagTopHotel}>Bán chạy</Box>
              )}
          </Box>
        </Box>
        <Box className={classes.wrapHotelInfo}>
          <Typography variant="subtitle2" className={classes.hotelName}>
            {item?.hotelName}
          </Typography>
          <Box className={classes.wrapRatingStar}>
            <Vote maxValue={item.ratingStar} value={item.ratingStar} />
          </Box>
          {isShowDistance ? (
            <Box
              fontSize={12}
              lineHeight="14px"
              color="gray.grayDark7"
              my={4 / 8}
            >
              {`Cách bạn ${item.distanceToSearchLocation.toFixed(1)}km`}
            </Box>
          ) : (
            <Typography variant="body2" className={classes.location}>
              <span style={{ marginRight: "4px" }}>
                <IconSightSeeing />{" "}
              </span>
              {item?.location}
            </Typography>
          )}
          <Box className={classes.wrapPoint}>
            <Typography className={clsx(classes.point, stylePoint)}>
              {!isEmpty(item.ratingPoint) && item?.ratingPoint}
            </Typography>
            <Box
              component="span"
              className={clsx(classes.review, styleMessageReview)}
            >
              {getMessageHotelReviews(
                !isEmpty(item.ratingPoint) && item.ratingPoint
              )}
            </Box>
            <Box component="span" className={classes.rateLevel}>
              {!isEmpty(item.countComment) &&
                item.countComment > 0 &&
                `(${item.countComment} đánh giá)`}
              <Box className={classes.boxTripadvisor} />
              {styleTripAdvisor ? (
                <Image
                  srcImage={`${prefixUrlIcon}${listIcons.IconTripadvisorMall}`}
                  className={classes.iconTripadvisorMall}
                />
              ) : (
                <Image
                  srcImage={`${prefixUrlIcon}${listIcons.IconTripadvisor}`}
                  className={classes.iconTripadvisorMall}
                />
              )}
              <Box
                style={{ color: "#1A202C", fontSize: 14, lineHeight: "16px" }}
              >
                {!isEmpty(item.taRating) &&
                  (2 * item.taRating === 10
                    ? 10
                    : (2 * item.taRating).toFixed(1))}
              </Box>
            </Box>
          </Box>
          {/* {isCountBooking &&
            !isEmpty(item.countBooking) &&
            item.countBooking > 0 && (
              <Box className={classes.wrapCountBooking}>
                <IconGroupUser />
                <Typography
                  variant="body2"
                  component="span"
                  className={classes.countNumberBooking}
                >
                  {item.countBooking} Đã đặt
                </Typography>
              </Box>
            )} */}

          {isShowLastBook &&
            !isEmpty(item.lastBookedTime) &&
            moment().diff(moment(item.lastBookedTime)) <= 86400000 && (
              <Box className={classes.lastBooking}>
                <Box display="flex" alignItems="center">
                  {listString.IDS_MT_TEXT_BOOKING_LAST_TIME}
                  <Box className={classes.lastTime}>
                    {moment().diff(moment(item.lastBookedTime)) <= 86400000 &&
                      moment(item.lastBookedTime).fromNow()}
                  </Box>
                </Box>
              </Box>
            )}

          <Box className={classes.wrapDiscountPriceFavorite}>
            {!isEmpty(item.subPrice) && item.subPrice > 0 && (
              <Box
                color="#718096"
                fontSize={11}
                className={classes.discountSubPriceFavorite}
              >
                {`${item?.subPrice.formatMoney()}đ`}
              </Box>
            )}
            <Box
              color="black.black3"
              fontWeight={600}
              fontSize={16}
              className={classes.discountPriceFavorite}
            >
              {!isEmpty(item.mainPrice) &&
                !item.hiddenPrice &&
                item.mainPrice > 0 &&
                `${item.mainPrice.formatMoney()}đ`}
            </Box>
            {item.hiddenPrice && (
              <Box display="flex" alignItems="flex-end" flexDirection="column">
                <Box display="flex" alignItems="center">
                  <Box className={classes.loginText}>
                    {listString.IDS_MT_TEXT_LOGIN_USE_PROMO}
                  </Box>
                  <Typography
                    variant="subtitle1"
                    color="secondary"
                    style={{ marginLeft: 5 }}
                    component="span"
                  >
                    {`Giảm ${
                      !isEmpty(item.basePromotionInfo)
                        ? getMemberDeal(item.basePromotionInfo, item.mainPrice)
                        : "10%"
                    }`}
                  </Typography>
                </Box>
                <Box className={classes.discountMember}>
                  {listString.IDS_MT_TEXT_DISCOUNT_ON_MEMBER}
                </Box>
              </Box>
            )}
          </Box>
        </Box>
      </Box>
    </Link>
  );
};

HotelItemVertical.propTypes = {
  isTopSale: PropTypes.bool,
  isHotelInterest: PropTypes.bool,
  isCountBooking: PropTypes.bool,
  isShowStatus: PropTypes.bool,
};

export default HotelItemVertical;
