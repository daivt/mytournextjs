import { Box, makeStyles, Typography } from "@material-ui/core";
import React from "react";
import Image from "@src/image/Image";
import { IconHomeFlightArrow } from "@public/icons";
import { listString } from "@utils/constants";
import moment from "moment";
import { DATE_FORMAT, DATE_FORMAT_BACK_END } from "@utils/moment";
import { domesticInBound } from "@utils/domestic";
import { useRouter } from "next/router";

const useStyles = makeStyles((theme) => ({
  image: { height: 184, width: "100%" },
  location: {
    color: theme.palette.black.black3,
  },
  flightInfo: {
    padding: 15,
    background: "#fff",
  },
  wrapItem: {
    borderRadius: 8,
    overflow: "hidden",
    cursor: "pointer",
  },
  time: {
    color: theme.palette.gray.grayDark8,
    fontWeight: "normal",
  },
  price: {
    color: theme.palette.black.black3,
    marginTop: 5,
  },
  airplaneCompany: {
    color: theme.palette.black.black3,
    fontWeight: 400,
  },
  locationIcon: {
    marginTop: "-4px",
    marginLeft: 10,
    marginRight: 10,
  },
}));
const FlightHomeCard = ({
  flightTo,
  flightFrom,
  airlineLogo,
  airlineName,
  date,
  mainPrice,
  farePrice,
  origin,
  destination,
}) => {
  const classes = useStyles();

  const router = useRouter();

  const currentFlight = domesticInBound.find(
    (item) => item.code === `${origin}-${destination}`
  );

  const handleClickTicket = (item) => {
    router.push({
      pathname: "/ve-may-bay/result/outbound",
      query: {
        adultCount: 1,
        childCount: 0,
        infantCount: 0,
        seatSearch: item?.seat || "economy",
        departureDate: item?.date
          ? moment(item.date, DATE_FORMAT).format(DATE_FORMAT_BACK_END)
          : moment().format(DATE_FORMAT_BACK_END),
        returnDate: "",
        origin_code: item?.origin,
        origin_location: item?.flightFrom,
        destination_code: item?.destination,
        destination_location: item?.flightTo,
      },
    });
  };
  return (
    <Box
      onClick={() => {
        handleClickTicket({ date, flightFrom, flightTo, destination, origin });
      }}
      className={classes.wrapItem}
    >
      <Box>
        <Image
          srcImage={currentFlight?.thumb}
          alt=""
          title=""
          className={classes.image}
        />
      </Box>
      <Box className={classes.flightInfo}>
        <Box display="flex" style={{ marginBottom: 5 }}>
          <Typography variant="subtitle1">{flightFrom}</Typography>
          <Typography
            variant="subtitle1"
            className={(classes.location, classes.locationIcon)}
          >
            <IconHomeFlightArrow />
          </Typography>
          <Typography variant="subtitle1" className={classes.location}>
            {flightTo}
          </Typography>
        </Box>
        <Typography variant="subtitle2" className={classes.time}>
          {listString.IDS_MT_TEXT_FLIGHT_START_DATE}: {date}
        </Typography>
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="flex-end"
          style={{ marginTop: 12 }}
        >
          <Box display="flex" alignItems="center">
            <img
              style={{ width: 24, height: "auto", marginRight: 8 }}
              src={airlineLogo}
              alt=""
            />
            <Typography variant="subtitle1" className={classes.airplaneCompany}>
              {airlineName}
            </Typography>
          </Box>
          <Box>
            <Typography variant="subtitle2" className={classes.time}>
              {listString.IDS_MT_TEXT_PRICE_FROM}
            </Typography>
            <Typography variant="subtitle1" className={classes.price}>
              {farePrice.formatMoney()}đ
            </Typography>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default FlightHomeCard;
