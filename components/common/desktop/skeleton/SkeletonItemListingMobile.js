import { Box } from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";

const styleSkeleton = {
  background: "linear-gradient(90deg, #EDF2F7 0%, #F7FAFC 100%)",
};
const SkeletonItemListingMobile = () => {
  return (
    <Box
      style={{
        margin: "12px 0",
        display: "flex",
        padding: 12,
        borderRadius: 8,
        background: "white",
      }}
    >
      <Skeleton
        variant="rect"
        width={115}
        height={180}
        animation="wave"
        style={{ ...styleSkeleton, minWidth: 115, marginRight: 8 }}
      />
      <Box style={{ width: 115 }}>
        <Skeleton
          width={115}
          height={24}
          style={{ ...styleSkeleton }}
          animation="wave"
        />
        <Skeleton
          width={115}
          height={24}
          style={{ ...styleSkeleton }}
          animation="wave"
        />
        <Skeleton
          width={115}
          height={24}
          style={{ ...styleSkeleton }}
          animation="wave"
        />
        <Skeleton
          width={115}
          height={24}
          style={{ ...styleSkeleton }}
          animation="wave"
        />
      </Box>
      <Box
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "flex-end",
          alignItems: "flex-end",
        }}
      >
        <Skeleton
          animation="wave"
          width={115}
          height={85}
          style={{ ...styleSkeleton }}
        />
      </Box>
    </Box>
  );
};

export default SkeletonItemListingMobile;
