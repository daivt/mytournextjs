import { Box } from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";

const styleSkeleton = {
  background: "linear-gradient(90deg, #EDF2F7 0%, #F7FAFC 100%)",
};
const SkeletonItemListingFlight = () => {
  return (
    <Box
      style={{
        margin: "12px 0",
        display: "flex",
        padding: 12,
        borderRadius: 8,
        background: "white",
      }}
      width="100%"
    >
      <Skeleton
        variant="rect"
        width={240}
        height={180}
        animation="wave"
        style={{ ...styleSkeleton, minWidth: 240, marginRight: 16 }}
      />
      <Box style={{ width: 365 }}>
        <Skeleton
          animation="wave"
          width={365}
          height={40}
          style={{ ...styleSkeleton }}
        />
        <Skeleton
          width={365}
          height={40}
          style={{ ...styleSkeleton }}
          animation="wave"
        />
        <Skeleton
          width={220}
          height={24}
          style={{ ...styleSkeleton }}
          animation="wave"
        />
        <Skeleton
          width={220}
          height={24}
          style={{ ...styleSkeleton }}
          animation="wave"
        />
      </Box>
      <Box
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "flex-end",
          alignItems: "flex-end",
        }}
      >
        <Skeleton
          animation="wave"
          width={178}
          height={95}
          style={{ ...styleSkeleton }}
        />
      </Box>
    </Box>
  );
};

export default SkeletonItemListingFlight;
