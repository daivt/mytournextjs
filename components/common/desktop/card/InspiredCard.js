import { Box, makeStyles, Typography } from "@material-ui/core";

import Image from "@src/image/Image";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  image: {
    width: "100%",
    height: "100%",
    position: "relative",
    borderRadius: 8,
  },
  title: {
    lineHeight: "22px",
    marginTop: 8,
    display: "-webkit-box",
    WebkitLineClamp: 2,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
  },
  linkContainer: {
    cursor: "pointer",
    color: theme.palette.black.black3,
    fontSize: 20,
    fontWeight: 600,
    "&:hover": {
      textDecoration: "none",
      color: theme.palette.primary.main,
    },
  },
}));

const InspiredCard = ({
  item = {},
  styleWrapperImage = "",
  imgSmall = "",
  styleFontSize = "",
}) => {
  const classes = useStyles();
  return (
    <a
      href={`/location/${item.id}-${item.slug}.html`}
      target="_blank"
      className={classes.linkContainer}
    >
      <Box className={classes.wrapBox}>
        <Image
          srcImage={item?.picture}
          className={clsx(classes.image, styleWrapperImage, imgSmall)}
          borderRadiusProp="8px"
        />
        <Typography
          variant="subtitle1"
          className={clsx(classes.title, styleFontSize)}
          title={item?.title}
        >
          {item?.title}
        </Typography>
      </Box>
    </a>
  );
};

export default InspiredCard;
