import { Box, makeStyles, Typography } from "@material-ui/core";

import { listInfoDescriptionHome } from "utils/dataFake";
import { isEmpty } from "@utils/helpers";

const initItem = {};

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
  },
  wrapBox: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    margin: "0 33px",
  },
  borderIcon: {
    background: theme.palette.gray.grayDark7,
    height: 1,
    width: 64,
    marginTop: 16,
  },
  wrapDescription: {
    fontSize: 12,
    lineHeight: "16px",
    fontWeight: 400,
    marginTop: 8,
    textAlign: "center",
  },
  wrapIcon: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
}));

const InfoDescriptionHomeCard = () => {
  const classes = useStyles();
  return (
    <Box className={classes.container}>
      {!isEmpty(listInfoDescriptionHome) &&
        listInfoDescriptionHome.map((item, index) => (
          <Box key={index.toString()} className={classes.wrapBox}>
            <Box component="span" className={classes.wrapIcon}>
              {item.icon}
              <Box className={classes.borderIcon} />
            </Box>
            <Typography
              component="span"
              variant="subtitle2"
              style={{ marginTop: "24px", whiteSpace: "nowrap" }}
            >
              {item.title}
            </Typography>
            <Typography component="span" className={classes.wrapDescription}>
              {item.description}
            </Typography>
          </Box>
        ))}
    </Box>
  );
};

export default InfoDescriptionHomeCard;
