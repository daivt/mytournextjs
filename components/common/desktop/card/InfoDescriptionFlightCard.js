import { Box, makeStyles, Typography } from "@material-ui/core";

import { listWhyFlight } from "utils/dataFake";
import { isEmpty } from "@utils/helpers";

const initItem = {};

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
  },
  wrapBox: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    margin: "0 100px",
  },
  borderIcon: {
    background: theme.palette.gray.grayDark7,
    height: 1,
    width: 64,
    marginTop: 16,
  },
  wrapDescription: {
    fontSize: 12,
    lineHeight: "16px",
    fontWeight: 400,
    marginTop: 8,
    textAlign: "center",
  },
}));

const InfoDescriptionFlightCard = () => {
  const classes = useStyles();
  return (
    <>
      <Box className={classes.container}>
        {!isEmpty(listWhyFlight) &&
          listWhyFlight.map((item, index) => (
            <Box key={index.toString()} className={classes.wrapBox}>
              <Box component="span" className={classes.wrapIcon}>
                {" "}
                {item.icon}
                <Box className={classes.borderIcon} />
              </Box>
              <Typography
                component="span"
                variant="subtitle2"
                style={{ marginTop: "24px", whiteSpace: "nowrap" }}
              >
                {item.title}
              </Typography>
              <Typography component="span" className={classes.wrapDescription}>
                {item.desc}
              </Typography>
            </Box>
          ))}
      </Box>
    </>
  );
};

export default InfoDescriptionFlightCard;
