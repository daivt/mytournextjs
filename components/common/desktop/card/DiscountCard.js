import clsx from "clsx";
import { DATE_FORMAT } from "@utils/moment";
import moment from "moment";
import { Box, makeStyles, Typography } from "@material-ui/core";
import { listString } from "@utils/constants";
import PropTypes from "prop-types";
import { isEmpty } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  container: {
    borderRadius: 8,
    boxShadow:
      "0px 0px 8px rgba(0, 0, 0, 0.05), 0px 8px 8px rgba(0, 0, 0, 0.05);",
    display: "flex",
    flexDirection: "column",
    width: "100%",
    border: "1px dashed rgba(255, 18, 132, 0.5)",
    background: "rgba(255, 18, 132, 0.05)",
    padding: "16px 24px",
    "&:hover": {
      cursor: "pointer",
    },
  },
  codeItem: {
    display: "flex",
    alignItems: "center",
  },
  titleItem: {
    color: theme.palette.black.black3,
    padding: "6px 0 10px",
    lineHeight: "22px",
  },
  expiredItem: {
    color: theme.palette.gray.grayDark7,
  },
  codeDetail: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    color: theme.palette.black.black3,
  },
  code: {
    textTransform: "uppercase",
    color: theme.palette.white.main,
    background: "#FF1284",
    padding: "2px 4px 1px 4px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    marginLeft: 6,
    fontSize: 16,
    lineHeight: "19px",
    fontWeight: "normal",
  },
  titleTextGroup: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    minHeight: 60,
  },
  termText: {
    fontWeight: "normal",
    fontSize: 14,
    lineHeight: "17px",
    textAlign: "right",
    color: "#FF1284",
    marginLeft: 30,
    minWidth: 118,
  },
}));
const DiscountCard = ({
  item = {},
  classNameCard = "",
  setCouponActive = () => {},
}) => {
  const classes = useStyles();
  return (
    <Box className={clsx(classes.container, classNameCard)}>
      <Typography
        variant="subtitle2"
        component="span"
        className={classes.codeItem}
      >
        <Typography component="span" className={classes.codeDetail}>
          {listString.IDS_MT_TEXT_TYPE_PROMO_CODE}
        </Typography>{" "}
        <Typography
          variant="subtitle2"
          component="span"
          className={classes.code}
          onClick={() => {
            navigator.clipboard.writeText(item?.code);
          }}
        >
          {!isEmpty(item.code) && item.code}
        </Typography>
      </Typography>
      <Box className={classes.titleTextGroup}>
        <Typography
          variant="subtitle1"
          component="span"
          className={classes.titleItem}
        >
          {!isEmpty(item.title) && item.title}
        </Typography>
        <Typography
          variant="subtitle1"
          component="span"
          className={classes.termText}
          onClick={() => setCouponActive(item)}
        >
          {listString.IDS_MT_TERM_CONDITION_COUPON_TEXT}
        </Typography>
      </Box>

      <Typography
        variant="body2"
        component="span"
        className={classes.expiredItem}
      >
        {listString.IDS_TEXT_DISCOUNT_CARD_EXPIRED}:
        {moment(item?.fromDate).format("DD/MM")} -
        {moment(item?.toDate).format("DD/MM")} |{" "}
        {listString.IDS_TEXT_USE_CODE_WHEN_PAYMENT}
      </Typography>
    </Box>
  );
};

DiscountCard.propTypes = {
  cardInfo: PropTypes.object.isRequired,
};

export default DiscountCard;
