import { makeStyles } from "@material-ui/styles";
import { IconStar } from "@public/icons";
import { useTheme } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme) => ({
  voteContainer: { display: "flex" },
  rankingHotel: { width: 14, height: 14, marginRight: 3 },
}));

const Vote = ({ maxValue = 5, value = 0 }) => {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <Box className={classes.voteContainer}>
      {Array(maxValue)
        .fill(0)
        .map((it, idx) => (
          <IconStar
            key={idx}
            className={`svgFillAll ${classes.rankingHotel}`}
            style={{
              stroke:
                idx >= value ? "#718096" : theme.palette.yellow.yellowLight3,
              fill:
                idx >= value ? "#718096" : theme.palette.yellow.yellowLight3,
            }}
          />
        ))}
    </Box>
  );
};

export default React.memo(Vote);
