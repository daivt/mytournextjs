import clsx from "clsx";
import cookie from "js-cookie";
import { Fragment, useState } from "react";
import PropTypes from "prop-types";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { Box } from "@material-ui/core";
import GoogleLogin from "react-google-login";
import makeStyles from "@material-ui/styles/makeStyles";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

import {
  TOKEN,
  listString,
  socialAccount,
  APP_ID,
  routeStatic,
  LAST_BOOKING_HOTEL,
} from "@utils/constants";
import { LoginAccountViaFbGg } from "@api/user";
import { IconGoogle, IconFacebook } from "@public/icons";
import * as actionTypes from "@contextProvider/system/ActionTypes";
import { useDispatchSystem } from "@contextProvider/ContextProvider";

import ButtonComponent from "@src/button/Button";
import snackbarSetting from "@src/alert/Alert";

const useStyles = makeStyles((theme) => ({
  btnLogin: (props) => ({
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    minWidth: props.minWidthBtn,
  }),
  btnLoginViaFb: (props) => ({
    margin: "16px 0",
  }),
  iconFb: {
    fontSize: 18,
  },
}));

const ButtonLoginViaGGFB = ({
  minWidthBtn = 212,
  isLogin = true,
  isShowCircularProgress = true,
  heightBtn = 52,
  fontSizeText = 16,
  isPc = false,
  redirectUri = "https://mytour.vn/",
}) => {
  const styleProps = {
    minWidthBtn,
  };
  const classes = useStyles(styleProps);
  const router = useRouter();
  const dispatch = useDispatchSystem();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const responseOnSuccessGoogle = async (response) => {
    const dataViaFbGg = {
      access_token: response.tokenId || "",
      provider: "google",
    };
    handleOauth(dataViaFbGg);
  };

  const responseOnFailureGoogle = (response) => {};

  const responseFacebook = async (response) => {
    if (response.status === "unknown" || response.status === "not_authorized") {
    } else {
      const dataViaFbGg = {
        access_token: response.accessToken || "",
        provider: "facebook",
      };
      handleOauth(dataViaFbGg);
    }
  };

  const handleOauth = async (dataViaFbGg = {}) => {
    try {
      let deviceId = null;
      const key = `${process.env.NEXT_PUBLIC_APP_ID}_v2021-05-03`;
      deviceId = `${new Date().valueOf()}-${Math.random()}`;
      const value = localStorage.getItem(key);
      if (value === null) {
        localStorage.setItem(key, deviceId);
      } else {
        deviceId = value;
      }
      const dataDTO = {
        ...dataViaFbGg,
        device_id: deviceId,
      };
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: true,
      });
      const { data } = await LoginAccountViaFbGg(dataDTO);
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
      if (data.code === 200) {
        localStorage.removeItem(LAST_BOOKING_HOTEL);
        cookie.set(TOKEN, data.access_token, { expires: 365 });
        if (isPc) {
          router.reload();
        } else {
          router.back();
          if (dataViaFbGg.provider === "facebook") {
            router.back();
          }
          dispatch({
            type: actionTypes.SET_INFORMATION_USER,
            payload: data.data || {},
          });
        }
        data?.message &&
          enqueueSnackbar(
            isLogin
              ? listString.IDS_MT_TEXT_LOGIN_ACCOUNT_SUCCESS
              : listString.IDS_MT_TEXT_CREATE_ACCOUNT_SUCCESS,
            snackbarSetting((key) => closeSnackbar(key), {
              color: "success",
            })
          );
      } else {
        data?.message &&
          enqueueSnackbar(
            data?.message,
            snackbarSetting((key) => closeSnackbar(key), {
              color: "error",
            })
          );
      }
    } catch (error) {
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
    }
  };
  return (
    <Fragment>
      <FacebookLogin
        appId={socialAccount.facebook_id}
        callback={responseFacebook}
        fields="name,email,picture"
        redirectUri={redirectUri}
        render={(renderProps) => (
          <ButtonComponent
            className={clsx(classes.btnLogin, classes.btnLoginViaFb)}
            typeButton="outlined"
            backgroundColor="#fff"
            borderColor="#A0AEC0"
            height={heightBtn}
            fontWeight={600}
            fontSize={fontSizeText}
            color="#1A202C"
            handleClick={renderProps.onClick}
            isShowCircularProgress={isShowCircularProgress}
            borderRadius={8}
          >
            <IconFacebook />
            <Box component="span" pl={2}>
              {isLogin
                ? listString.IDS_MT_TEXT_LOGIN_VIA_FB
                : listString.IDS_MT_TEXT_SIGN_UP_VIA_FB}
            </Box>
          </ButtonComponent>
        )}
      />
      <GoogleLogin
        clientId={socialAccount.gg_plus_id}
        buttonText="Login"
        onSuccess={responseOnSuccessGoogle}
        onFailure={responseOnFailureGoogle}
        cookiePolicy={"single_host_origin"}
        render={(renderProps) => (
          <ButtonComponent
            className={classes.btnLogin}
            backgroundColor="#fff"
            height={heightBtn}
            fontWeight={600}
            fontSize={fontSizeText}
            color="#1A202C"
            borderColor="#A0AEC0"
            typeButton="outlined"
            handleClick={renderProps.onClick}
            isShowCircularProgress={isShowCircularProgress}
            borderRadius={8}
          >
            <IconGoogle />
            <Box component="span" pl={2}>
              {isLogin
                ? listString.IDS_MT_TEXT_LOGIN_VIA_GG
                : listString.IDS_MT_TEXT_SIGN_UP_VIA_GG}
            </Box>
          </ButtonComponent>
        )}
      />
    </Fragment>
  );
};

ButtonLoginViaGGFB.propTypes = {
  minWidthBtn: PropTypes.number,
  isLogin: PropTypes.bool,
  isShowCircularProgress: PropTypes.bool,
  loginSuccessFacebook: PropTypes.func,
  loginSuccessGoogle: PropTypes.func,
};
export default ButtonLoginViaGGFB;
