import { makeStyles } from "@material-ui/styles";
import { Box, CircularProgress } from "@material-ui/core";

import { listString } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  loadMoreBox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    fontSize: 14,
    height: 30,
  },
}));
const LoadingMore = () => {
  const classes = useStyles();
  return (
    <Box className={classes.loadMoreBox} key={0}>
      <CircularProgress size={24} color="secondary" />
      <Box component="span" pl={1}>
        {`${listString.IDS_MT_TEXT_LOADING_MORE} ...`}
      </Box>
    </Box>
  );
};

export default LoadingMore;
