import { makeStyles } from "@material-ui/styles";
import { Box, CircularProgress } from "@material-ui/core";
import { useSystem } from "@contextProvider/ContextProvider";

const useStyles = makeStyles((theme) => ({
  loadMoreContainer: {
    position: "absolute",
    zIndex: 2000,
    top: 0,
    left: 0,
    width: "100%",
    height: "100vh",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(255,255,255,0.7)",
  },
}));
const LoadingApp = () => {
  const classes = useStyles();
  const { systemReducer } = useSystem();
  if (!systemReducer.fetching) return null;
  return (
    <Box className={classes.loadMoreContainer}>
      {/* <CircularProgress size={56} color="secondary" /> */}
      <img
        src="https://staticproxy.mytourcdn.com/0x0/themes/images/mloading.gif"
        className={classes.imgLoading}
      />
    </Box>
  );
};

export default LoadingApp;
