import { Box, ButtonBase, Drawer, Grid, makeStyles } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import { IconArrowRight } from "@public/icons";
import utilStyles from "@styles/utilStyles";
import { listString } from "@utils/constants";
import clsx from "clsx";
import { useState, useEffect } from "react";
import { isEmpty } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    marginTop: 10,
    width: "100vw",
  },
  headerBox: {
    padding: "14px 0",
    borderTop: `solid 1px ${theme.palette.gray.grayLight22}`,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  bodyBox: {
    padding: 16,
    display: "flex",
    flexDirection: "column",
  },
  bagItem: {
    border: `solid 1px ${theme.palette.gray.grayLight25}`,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
    padding: "8px 14px",
    color: theme.palette.black.black3,
    minWidth: 90,
  },
  active: {
    border: `solid 1px ${theme.palette.blue.blueLight8}`,
    backgroundColor: theme.palette.blue.blueLight9,
    "& span": {
      color: theme.palette.blue.blueLight8,
    },
    "& h6": {
      color: theme.palette.blue.blueLight8,
    },
  },
  footerBox: {
    padding: "8px 0",
    borderTop: `solid 1px ${theme.palette.gray.grayLight22}`,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  btnFilter: {
    backgroundColor: theme.palette.pink.main,
    color: theme.palette.white.main,
    borderRadius: 8,
    padding: "14px 0",
    height: 48,
    width: "100%",
  },
  fromTitle: {
    borderRadius: 4,
    padding: 4,
    backgroundColor: theme.palette.blue.blueLight8,
    color: theme.palette.white.main,
  },
  fs12: {
    fontSize: 12,
    fontWeight: 600,
    lineHeight: "14px",
  },
  uppercase: {
    textTransform: "uppercase",
  },
  arrowRight: {
    margin: "2px 8px 0 8px;",
  },
  btnFixed: {
    position: "fixed",
    width: "100%",
    bottom: 0,
  },
}));

const RenderListBaggage = ({
  listLuggage,
  cusInfo,
  isReturn,
  keyCus,
  cusName,
  handleCheckBaggage = () => {},
}) => {
  const classesUtils = utilStyles();
  const classes = useStyles();

  const listLugs = listLuggage;
  let fieldName = "departureBaggage";
  if (isReturn) {
    fieldName = "returnBaggage";
  }
  return (
    <Box className={classesUtils.scrollViewHorizontal}>
      {listLugs.map((item, index) => (
        <Box
          key={index}
          mr={1}
          className={clsx(
            classes.bagItem,
            cusInfo[keyCus][fieldName] === item.id && classes.active
          )}
          onClick={() =>
            handleCheckBaggage(
              item.id,
              cusName,
              item.price,
              item.weight,
              isReturn
            )
          }
        >
          <Box>
            <Typography variant="caption">{item.weight}kg</Typography>
          </Box>
          <Box>
            {item.weight ? (
              <Typography variant="caption">
                {item.price.formatMoney()}đ
              </Typography>
            ) : (
              <div></div>
            )}
          </Box>
        </Box>
      ))}
    </Box>
  );
};

const AddBaggageModal = ({
  data = {},
  open = false,
  toggleDrawer = () => {},
  handleSubmit = () => {},
}) => {
  const classes = useStyles();
  const theme = useTheme();
  let customerInfo = data?.arrayCustomer;
  let [totalMoney, setTotalMoney] = useState(0);
  const [customerByBaggage, setCustomerByBaggage] = useState(
    data?.arrayCustomer
  );

  useEffect(() => {
    if (!isEmpty(customerInfo)) {
      let totalMoneyShow = 0;
      customerInfo.forEach((el, index) => {
        totalMoneyShow = totalMoneyShow + (el.returnMoney + el.departureMoney);
      });
      if (totalMoneyShow !== totalMoney) {
        setTotalMoney(totalMoneyShow);
      }
      setCustomerByBaggage(customerInfo);
    }
  }, [customerInfo]);

  const handleCheckBaggage = (
    bagId,
    customerName,
    money = 0,
    weight = 0,
    isReturn
  ) => {
    let baggages = [...customerByBaggage];
    let totalMoneyShow = 0;
    baggages.forEach((el, index) => {
      if (el.keyPosition === customerName) {
        if (isReturn) {
          el.returnBaggage = bagId;
          el.returnMoney = money;
          el.returnWeight = weight;
        } else {
          el.departureBaggage = bagId;
          el.departureMoney = money;
          el.departureWeight = weight;
        }
      }
      totalMoneyShow = totalMoneyShow + (el.returnMoney + el.departureMoney);
    });
    if (totalMoneyShow !== totalMoney) {
      setTotalMoney(totalMoneyShow);
    }
    setCustomerByBaggage(baggages);
  };
  const listLugsIB = data?.inboundBaggageInfo?.baggages;
  const listLugsOB = data?.outboundBaggageInfo?.baggages;

  return (
    <Drawer
      anchor="right"
      open={open}
      onClose={toggleDrawer(false)}
      classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
    >
      <Box className={classes.container}>
        <Grid container className={classes.headerBox}>
          <Grid item lg={4} md={4} sm={4} xs={4} onClick={toggleDrawer(false)}>
            <CloseIcon style={{ marginLeft: 16 }} fontSize={"small"} />
          </Grid>
          <Grid item lg={8} md={8} sm={8} xs={8}>
            <Box display="flex">
              <Typography variant="subtitle1">
                {listString.IDS_MT_TEXT_ADD_BAGGAGE}
              </Typography>
            </Box>
          </Grid>
        </Grid>
        {!isEmpty(listLugsOB) && (
          <Box px={2}>
            <Box display="flex" mt={3}>
              <Box
                className={`${classes.fromTitle} ${classes.uppercase}`}
                mr={6 / 8}
              >
                <Typography component="span" className={classes.fs12}>
                  {listString.IDS_MT_TEXT_DEPARTURE}
                </Typography>
              </Box>
              <Box display="flex" flexDirection="row">
                <Typography variant="subtitle1">
                  {data?.outboundBaggageInfo?.departureCity}
                </Typography>
                <IconArrowRight className={classes.arrowRight} />
                <Typography variant="subtitle1">
                  {data?.outboundBaggageInfo?.arrivalCity}
                </Typography>
              </Box>
            </Box>

            {customerByBaggage.map(
              (item, index) =>
                !!item?.name && (
                  <Box key={index}>
                    <Box pt={2} pb={1} className={classes.uppercase}>
                      <Typography variant="caption">{item.name}</Typography>
                    </Box>
                    <RenderListBaggage
                      keyCus={index}
                      cusInfo={customerByBaggage}
                      listLuggage={listLugsOB}
                      cusName={item.keyPosition}
                      isReturn={false}
                      handleCheckBaggage={handleCheckBaggage}
                    />
                  </Box>
                )
            )}
          </Box>
        )}

        {data.return && !isEmpty(listLugsIB) && (
          <Box px={2}>
            <Box display="flex" mt={3}>
              <Box
                className={`${classes.fromTitle} ${classes.uppercase}`}
                mr={6 / 8}
              >
                <Typography component="span" className={classes.fs12}>
                  {listString.IDS_MT_TEXT_RETURN}
                </Typography>
              </Box>
              <Box display="flex" flexDirection="row">
                <Typography variant="subtitle1">
                  {data?.inboundBaggageInfo?.departureCity}
                </Typography>
                <IconArrowRight className={classes.arrowRight} />
                <Typography variant="subtitle1">
                  {data?.inboundBaggageInfo?.arrivalCity}
                </Typography>
              </Box>
            </Box>

            {customerByBaggage.map(
              (item, index) =>
                !!item?.name && (
                  <Box key={index}>
                    <Box pt={2} pb={1} className={classes.uppercase}>
                      <Typography variant="caption">{item.name}</Typography>
                    </Box>
                    <RenderListBaggage
                      keyCus={index}
                      cusInfo={customerByBaggage}
                      listLuggage={listLugsIB}
                      cusName={item.keyPosition}
                      isReturn={true}
                      handleCheckBaggage={handleCheckBaggage}
                    />
                  </Box>
                )
            )}
          </Box>
        )}

        {isEmpty(listLugsOB) && isEmpty(listLugsIB) ? (
          <>
            <Box
              alignItems="center"
              display="flex"
              justifyContent="center"
              pt={3}
            >
              <Typography variant="caption">
                {listString.IDS_MT_TEXT_NO_SUPPORT_BAGGAGE}
              </Typography>
            </Box>
            <Box className={clsx(classes.footerBox, classes.btnFixed)} mt={5}>
              <Box mx={2}>
                <Box display="flex">
                  <ButtonBase
                    className={classes.btnFilter}
                    onClick={toggleDrawer(false)}
                  >
                    <Typography variant="subtitle1">
                      {listString.IDS_MT_TEXT_CLOSE}
                    </Typography>
                  </ButtonBase>
                </Box>
              </Box>
            </Box>
          </>
        ) : (
          <Box className={classes.footerBox} mt={5}>
            <Box mx={2}>
              <Grid container>
                <Grid item lg={5} md={5} sm={5} xs={5}>
                  <Box>
                    {listString.IDS_MT_TEXT_TOTAL}
                    <Typography variant="body1">
                      {totalMoney.formatMoney()}đ
                    </Typography>
                  </Box>
                </Grid>
                <Grid item lg={7} md={7} sm={7} xs={7}>
                  <Box display="flex">
                    <ButtonBase
                      className={classes.btnFilter}
                      onClick={handleSubmit(customerByBaggage)}
                    >
                      <Typography variant="subtitle1">
                        {listString.IDS_MT_TEXT_APPLY}
                      </Typography>
                    </ButtonBase>
                  </Box>
                </Grid>
              </Grid>
            </Box>
          </Box>
        )}
      </Box>
    </Drawer>
  );
};

export default AddBaggageModal;
