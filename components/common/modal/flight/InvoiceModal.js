import { getEnterpriseInfo } from "@api/flight";
import ConditionInvoice from "@components/common/modal/flight/ConditionInvoice";
import {
  Box,
  Divider,
  Drawer,
  Grid,
  IconButton,
  makeStyles,
} from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { IconBack, IconSearch } from "@public/icons";
import ButtonComponent from "@src/button/Button";
import FieldTextContent from "@src/form/FieldTextContent";
import { listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import { validTaxCodeRegex } from "@utils/regex";
import clsx from "clsx";
import { Form, Formik } from "formik";
import React, { useState } from "react";
import * as yup from "yup";

const useStyles = makeStyles((theme) => ({
  mainDrawerFilter: {
    width: "100vw",
    fontSize: 16,
    overflow: "auto",
  },
  wrapHeader: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 16,
  },
  container: {
    padding: 16,
  },
  slBox: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  leftBox: {
    padding: "30px 0",
  },
  rightBox: {
    padding: "20px 0",
    textAlign: "center",
  },
  btnClose: { padding: 0 },
  paperAnchorBottom: {
    borderRadius: "16px 16px 0 0",
  },
  buttonCountry: {
    width: "100%",
    height: 40,
    borderRadius: 0,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight25}`,
    backgroundColor: theme.palette.white.main,
    "&:hover": {
      backgroundColor: theme.palette.white.main,
    },
    color: theme.palette.gray.grayDark7,
    display: "flex",
    justifyContent: "flex-start",
    marginBottom: 26,
  },
  iconSearchTax: {
    padding: 0,
    width: 23,
    height: 20,
  },
  styleLabel: {
    fontSize: 16,
  },
  divider: {
    height: 1,
    margin: "40px -16px 0",
    color: theme.palette.gray.grayLight22,
  },
}));

const InvoiceModal = ({
  open = false,
  toggleDrawer = () => {},
  invoiceInfo = {},
  handleInvoiceInfo = () => {},
  recipientEmail,
  recipientName,
  isEdit,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const [taxCode, setTaxCode] = useState("");
  const [openCondition, setOpenCondition] = useState(false);
  const handleClickCondition = (flag) => () => {
    setOpenCondition(flag);
  };
  const handleApply = (values) => {
    handleInvoiceInfo(values);
    toggleDrawer({
      open: false,
      isExport: true,
    });
  };

  const getStyleInput = {
    color: theme.palette.gray.grayDark7,
    borderTop: "none",
    borderRight: "none",
    borderLeft: "none",
    borderRadius: 0,
  };
  const storeSchema = yup.object().shape({
    taxIdNumber: yup
      .string()
      .trim()
      .max(50, listString.IDS_MT_TEXT_VALID_MAX_LENGTH_DEFAULT)
      .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    companyName: yup
      .string()
      .trim()
      .max(250, listString.IDS_MT_TEXT_VALID_MAX_LENGTH_DEFAULT)
      .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    companyAddress: yup
      .string()
      .trim()
      .max(250, listString.IDS_MT_TEXT_VALID_MAX_LENGTH_DEFAULT)
      .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    recipientAddress: yup
      .string()
      .trim()
      .max(250, listString.IDS_MT_TEXT_VALID_MAX_LENGTH_DEFAULT)
      .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    recipientEmail: yup
      .string()
      .trim()
      .max(50, listString.IDS_MT_TEXT_VALIDATE_EMAIL_MAX)
      .email(listString.IDS_MT_TEXT_INVALID_FORMAT_EMAIL)
      .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    recipientName: yup
      .string()
      .trim()
      .max(50, listString.IDS_MT_TEXT_VALID_MAX_LENGTH_DEFAULT)
      .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    note: yup
      .string()
      .trim()
      .max(1000, listString.IDS_MT_TEXT_VALID_MAX_LENGTH_DEFAULT),
  });

  return (
    <Drawer
      anchor="bottom"
      open={open}
      onClose={() =>
        toggleDrawer({
          open: false,
          fromEdit: isEdit,
        })
      }
      classes={{
        paperAnchorBottom: classes.paperAnchorBottom,
      }}
    >
      <Box className={classes.mainDrawerFilter}>
        <Grid container className={clsx(classes.wrapHeader, classes.slBox)}>
          <Grid item xs={4}>
            <IconButton
              onClick={() =>
                toggleDrawer({
                  open: false,
                  fromEdit: isEdit,
                })
              }
              className={classes.btnClose}
            >
              <IconBack />
            </IconButton>
          </Grid>
          <Grid item xs={8}>
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="center"
            >
              <Typography component="span" variant="subtitle1">
                {listString.IDS_TEXT_EXPORT_INVOICE}
              </Typography>
              <ButtonComponent
                fontSize={14}
                fontWeight="normal"
                color={theme.palette.blue.blueLight8}
                typeButton="text"
                width="fit-content"
                padding={0}
                handleClick={handleClickCondition(true)}
              >
                <Typography component="span" variant="caption">
                  {listString.IDS_TEXT_CONDITION_EXPORT_INVOICE}
                </Typography>
              </ButtonComponent>
              <ConditionInvoice
                open={openCondition}
                handleClose={handleClickCondition}
              />
            </Box>
          </Grid>
        </Grid>
        <Box className={classes.wrapper}>
          <Box className={classes.container}>
            <Formik
              initialValues={{
                companyAddress: invoiceInfo?.companyAddress,
                companyName: invoiceInfo?.companyName,
                note: invoiceInfo?.note || "",
                recipientAddress: invoiceInfo?.recipientAddress,
                recipientEmail: invoiceInfo?.recipientEmail
                  ? invoiceInfo?.recipientEmail
                  : recipientEmail,
                recipientName: invoiceInfo?.recipientName
                  ? invoiceInfo?.recipientName
                  : recipientName,
                taxIdNumber: invoiceInfo?.taxIdNumber,
              }}
              validationSchema={storeSchema}
              onSubmit={(values) => {
                handleApply(values);
              }}
            >
              {({ values, setFieldValue, setFieldError, errors }) => {
                const actionSearchTaxCode = async (inputSearch) => {
                  const args = {
                    taxCode: inputSearch,
                  };
                  const { data } = await getEnterpriseInfo(args);
                  if (data.code == 200 && data?.data) {
                    const resultSearch = data.data;
                    setFieldValue(
                      "taxIdNumber",
                      resultSearch?.taxCode || inputSearch
                    );
                    setFieldValue("companyName", resultSearch?.companyName);
                    setFieldValue(
                      "companyAddress",
                      resultSearch?.companyAddress
                    );
                    setFieldValue(
                      "recipientAddress",
                      resultSearch?.recipientAddress
                    );
                    setFieldValue(
                      "recipientEmail",
                      isEmpty(values?.recipientEmail)
                        ? resultSearch?.recipientEmail
                        : values?.recipientEmail
                    );

                    setFieldValue(
                      "recipientName",
                      isEmpty(values?.recipientName)
                        ? resultSearch.recipientName
                        : values.recipientName
                    );
                  } else {
                    data?.message &&
                      setFieldError(
                        "taxIdNumber",
                        listString.IDS_MT_TEXT_NOT_FOUND_TAX_CODE
                      );
                  }
                };
                const handleSearchTaxCode = () => {
                  actionSearchTaxCode(taxCode);
                };
                return (
                  <Form autoComplete="off">
                    <FieldTextContent
                      placeholder={
                        listString.IDS_TEXT_PLACEHOLDER_SEARCH_TAX_CODE
                      }
                      inputStyle={getStyleInput}
                      inputProps={{
                        autoComplete: "off",
                      }}
                      helperTextStyle={{ paddingTop: 8 }}
                      name="taxIdNumber"
                      value={values.taxIdNumber}
                      onChange={(e) => {
                        if (validTaxCodeRegex.test(e.target.value)) {
                          setTaxCode(e.target.value);
                          setFieldValue("taxIdNumber", e.target.value);
                        }
                      }}
                      endAdornment={
                        <IconButton
                          className={classes.iconSearchTax}
                          aria-label="search"
                          onClick={handleSearchTaxCode}
                        >
                          <IconSearch />
                        </IconButton>
                      }
                    />
                    <FieldTextContent
                      name="companyName"
                      placeholder={listString.IDS_TEXT_PLACEHOLDER_INVOICE_NAME}
                      inputStyle={getStyleInput}
                      inputProps={{
                        autoComplete: "off",
                      }}
                      helperTextStyle={{ paddingTop: 8 }}
                    />
                    <FieldTextContent
                      name="companyAddress"
                      placeholder={
                        listString.IDS_TEXT_PLACEHOLDER_INVOICE_ADDRESS
                      }
                      inputStyle={getStyleInput}
                      inputProps={{
                        autoComplete: "off",
                      }}
                      helperTextStyle={{ paddingTop: 8 }}
                    />
                    <FieldTextContent
                      name="recipientAddress"
                      placeholder={
                        listString.IDS_TEXT_PLACEHOLDER_INVOICE_RECEIVER
                      }
                      inputStyle={getStyleInput}
                      inputProps={{
                        autoComplete: "off",
                      }}
                      helperTextStyle={{ paddingTop: 8 }}
                    />
                    <FieldTextContent
                      name="recipientName"
                      placeholder={
                        listString.IDS_TEXT_PLACEHOLDER_INVOICE_CUSTOMER_NAME
                      }
                      inputStyle={getStyleInput}
                      inputProps={{
                        autoComplete: "off",
                      }}
                      helperTextStyle={{ paddingTop: 8 }}
                    />
                    <FieldTextContent
                      name="recipientEmail"
                      placeholder={listString.IDS_MT_TEXT_EMAIL}
                      inputStyle={getStyleInput}
                      inputProps={{
                        autoComplete: "off",
                      }}
                      helperTextStyle={{ paddingTop: 8 }}
                    />
                    <FieldTextContent
                      name="note"
                      placeholder={listString.IDS_TEXT_PLACEHOLDER_INVOICE_NOTE}
                      inputStyle={getStyleInput}
                      inputProps={{
                        autoComplete: "off",
                      }}
                    />
                    <Divider className={classes.divider} />
                    <Box pt={1}>
                      <ButtonComponent
                        type="submit"
                        backgroundColor={theme.palette.secondary.main}
                        height={48}
                        borderRadius={8}
                      >
                        <Typography variant="subtitle1">
                          {listString.IDS_MT_TEXT_APPLY}
                        </Typography>
                      </ButtonComponent>
                    </Box>
                  </Form>
                );
              }}
            </Formik>
          </Box>
        </Box>
      </Box>
    </Drawer>
  );
};

export default InvoiceModal;
