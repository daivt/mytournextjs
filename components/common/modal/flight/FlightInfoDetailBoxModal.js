import {
  Box,
  Divider,
  Drawer,
  Grid,
  IconButton,
  makeStyles,
} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import FlightDetailFee from "../../card/flight/FlightDetailFee";
import FlightTicketDetail from "../../card/flight/FlightTicketDetail";
import FlightTicketDetailTransit from "../../card/flight/FlightTicketDetailTransit";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    width: "100vw",
  },
  headerBox: {
    borderTop: `solid 1px ${theme.palette.gray.grayLight22}`,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  divider: {
    height: 6,
    color: theme.palette.gray.grayLight22,
    margin: "12px -16px",
  },
  bodyBox: {
    padding: "16px 16px 0px 16px",
    display: "flex",
    flexDirection: "column",
    height: `calc(100vh - 53px)`,
  },
}));

const FlightInfoDetailBoxModal = ({
  open = false,
  setOpen = () => {},
  ticket = {},
  data,
  isTransit,
  airlineInfo,
  ticketInBound,
  dataInBound,
  ticketPersion,
}) => {
  const classes = useStyles();

  return (
    <Drawer
      anchor="right"
      open={open}
      onClose={() => {}}
      classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
    >
      <Box className={classes.container}>
        <Grid container className={classes.headerBox}>
          <Grid item lg={4} md={4} sm={4} xs={4}>
            <IconButton
              onClick={() => {
                setOpen(false);
              }}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
          </Grid>
          <Grid item lg={8} md={8} sm={8} xs={8}>
            <Box display="flex">
              <Typography variant="subtitle1">Chi tiết chuyến bay</Typography>
            </Box>
          </Grid>
        </Grid>
        <Box className={classes.bodyBox}>
          {!ticket.transitTickets ? (
            <FlightTicketDetail
              ticket={ticket}
              data={data}
              isTransit={isTransit}
              airlineInfo={airlineInfo}
              title="Thông tin vé"
              isSimple={false}
              isSeat
              title2Ways={ticketInBound ? "CHIỀU ĐI" : null}
            />
          ) : (
            <FlightTicketDetailTransit
              ticket={ticket}
              data={data}
              isTransit={isTransit}
              airlineInfo={airlineInfo}
              title="Thông tin vé"
              isSimple={false}
              isSeat
              title2Ways={ticketInBound ? "CHIỀU ĐI" : null}
            />
          )}
          <Box mt={2}></Box>
          {ticketInBound && !ticket.transitTickets && (
            <>
              <FlightTicketDetail
                ticket={ticketInBound}
                data={dataInBound}
                isTransit={isTransit}
                airlineInfo={airlineInfo}
                title=""
                isSimple={false}
                isSeat
                title2Ways={ticketInBound ? "CHIỀU VỀ" : null}
              />
            </>
          )}
          {ticketInBound && ticket.transitTickets && (
            <>
              <FlightTicketDetailTransit
                ticket={ticketInBound}
                data={dataInBound}
                isTransit={isTransit}
                airlineInfo={airlineInfo}
                title=""
                isSimple={false}
                isSeat
                title2Ways={ticketInBound ? "CHIỀU VỀ" : null}
              />
            </>
          )}
          <Divider className={classes.divider} />
          <FlightDetailFee
            ticket={ticket}
            ticketInBound={ticketInBound}
            data={data}
            ticketPersion={ticketPersion}
            title="Chi tiết giá"
          />
        </Box>
      </Box>
    </Drawer>
  );
};

export default FlightInfoDetailBoxModal;
