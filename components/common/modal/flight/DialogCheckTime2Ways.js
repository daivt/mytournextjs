import { Backdrop, Box, Fade, makeStyles, Typography } from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import { useTheme } from "@material-ui/core/styles";
import ButtonComponent from "@src/button/Button";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    outline: "none",
    backgroundColor: theme.palette.white.main,
    borderRadius: 16,
    display: "flex",
    justifyContent: "center",
    padding: "24px 16px",
    flexDirection: "column",
    alignItems: "center",
    width: 290,
  },
  btnFilter: {
    width: 124,
    marginTop: 8,
  },
}));

const DialogCheckTime2Ways = ({ open = false, toggleDrawer }) => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={open}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <Box className={classes.paper}>
          <Typography variant="subtitle1">Chọn chuyến bay về khác</Typography>
          <Typography
            style={{ marginTop: 8, textAlign: "center" }}
            variant="caption"
          >
            Để thuận tiện, bạn nên chọn chuyến bay về có giờ khởi hành cách thời
            điểm chuyến bay đi hạ cách ít nhất 3 tiếng.
          </Typography>
          <Box mx={2}>
            <Box display="flex">
              <ButtonComponent
                className={classes.btnFilter}
                handleClick={() => toggleDrawer()}
                fontSize={14}
                height={36}
                backgroundColor={theme.palette.secondary.main}
              >
                <Typography>Đồng ý</Typography>
              </ButtonComponent>
            </Box>
          </Box>
        </Box>
      </Fade>
    </Modal>
  );
};

export default DialogCheckTime2Ways;
