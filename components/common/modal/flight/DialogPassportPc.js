import BirthDayField from "@components/common/BirthDayField/BirthDayField";
import { Box, IconButton, makeStyles } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { IconClose } from "@public/icons";
import ButtonComponent from "@src/button/Button";
import FieldTextAutocomplete from "@src/form/FieldTextAutocomplete";
import FieldTextContent from "@src/form/FieldTextContent";
import { defaultCountry, listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import { DATE_FORMAT_BACK_END } from "@utils/moment";
import { useFormikContext } from "formik";
import find from "lodash/find";
import moment from "moment";
import React, { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
  },
  dialogTitle: {
    margin: 0,
    padding: theme.spacing(3),
  },
  dialogContent: {
    padding: "0 24px 24px 24px",
  },
  inputStyle: {
    "& > input": {
      padding: 0,
    },
  },
}));
const DialogPassportPc = ({
  open = false,
  toggleDrawer = () => {},
  index = 0,
  handleSubmitPassport = () => {},
  countries = {},
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const { setFieldValue, values } = useFormikContext();

  const [passportCountry, setPassportCountry] = useState({});
  const [passportResidence, setPassportResidence] = useState({});
  useEffect(() => {
    let country = find(
      countries,
      (c) => c.id === (values[`passport_country_${index}`] || defaultCountry.id)
    );
    if (isEmpty(country)) {
      country = defaultCountry;
    }

    let residence = find(
      countries,
      (c) =>
        c.id === (values[`passport_residence_${index}`] || defaultCountry.id)
    );
    if (isEmpty(residence)) {
      residence = defaultCountry;
    }

    setFieldValue(`passport_country_${index}`, country.id);
    setFieldValue(`passport_residence_${index}`, residence.id);
    setPassportCountry(country);
    setPassportResidence(residence);
  }, [index]);

  const getStyleInput = {
    color: theme.palette.black.black3,
    background: theme.palette.white.main,
    borderTop: "none",
    borderRight: "none",
    borderLeft: "none",
    borderRadius: 0,
  };

  return (
    <Dialog onClose={toggleDrawer(false)} open={open} maxWidth="xs" fullWidth>
      <DialogTitle
        onClose={toggleDrawer(false)}
        disableTypography
        classes={{ root: classes.dialogTitle }}
      >
        <Typography variant="h6">
          {listString.IDS_TEXT_ADD_PASSPORT_MODAL_TITLE}
        </Typography>
        <IconButton
          onClick={toggleDrawer(false)}
          aria-label="close"
          className={classes.closeButton}
        >
          <IconClose />
        </IconButton>
      </DialogTitle>
      <DialogContent classes={{ root: classes.dialogContent }}>
        <Autocomplete
          options={countries}
          classes={{
            option: classes.option,
          }}
          value={passportResidence}
          onChange={(event, newValue) => {
            setFieldValue(`passport_residence_${index}`, newValue.id);
            setPassportResidence(find(countries, (c) => c.id === newValue.id));
          }}
          autoHighlight
          getOptionSelected={(option) =>
            option.id === passportResidence?.id || defaultCountry.id
          }
          getOptionLabel={(option) => option.name}
          renderOption={(option) => (
            <>
              <Box
                display="flex"
                width="100%"
                justifyContent="space-between"
                borderBottom="1px solid #ced4da"
                pb={1}
              >
                <Typography variant="body1">{`${option.name} (${option.code})`}</Typography>
                <Typography
                  variant="body1"
                  style={{ color: theme.palette.blue.blueLight8 }}
                >{`+${option.phonecode}`}</Typography>
              </Box>
            </>
          )}
          renderInput={(params) => (
            <FieldTextAutocomplete
              fullWidth
              name={`passport_residence_${index}`}
              {...params}
              label={listString.IDS_TEXT_PASSPORT_RESIDENCE}
              inputProps={{
                ...params.inputProps,
                autoComplete: "off",
              }}
            />
          )}
        />
        <FieldTextContent
          name={`passport_${index}`}
          placeholder={listString.IDS_TEXT_PASSPORT_NUMBER}
          inputStyle={getStyleInput}
          className={classes.inputStyle}
          inputProps={{
            autoComplete: "off",
          }}
        />
        <Autocomplete
          options={countries}
          classes={{
            option: classes.option,
          }}
          value={passportCountry}
          onChange={(event, newValue) => {
            setFieldValue(`passport_country_${index}`, newValue.id);
            setPassportCountry(find(countries, (c) => c.id === newValue.id));
          }}
          autoHighlight
          getOptionSelected={(option) =>
            option.id === passportCountry?.id || defaultCountry.id
          }
          getOptionLabel={(option) => option.name}
          renderOption={(option) => (
            <>
              <Box
                display="flex"
                width="100%"
                justifyContent="space-between"
                borderBottom="1px solid #ced4da"
                pb={1}
              >
                <Typography variant="body1">{`${option.name} (${option.code})`}</Typography>
                <Typography
                  variant="body1"
                  style={{ color: theme.palette.blue.blueLight8 }}
                >{`+${option.phonecode}`}</Typography>
              </Box>
            </>
          )}
          renderInput={(params) => (
            <FieldTextAutocomplete
              fullWidth
              name={`passport_country_${index}`}
              {...params}
              label={listString.IDS_TEXT_PASSPORT_COUNTRY}
              inputProps={{
                ...params.inputProps,
                autoComplete: "off",
              }}
            />
          )}
        />
        <BirthDayField
          name={`passport_expired_${index}`}
          date={
            values[`passport_expired_${index}`] &&
            moment(values[`passport_expired_${index}`], DATE_FORMAT_BACK_END)
          }
          update={(value) => {
            setFieldValue(
              `passport_expired_${index}`,
              moment(value).format(DATE_FORMAT_BACK_END)
            );
          }}
          placeholder={listString.IDS_TEXT_PASSPORT_EXPIRED}
          inputStyle={getStyleInput}
          inputProps={{
            autoComplete: "off",
          }}
        />
        <Box padding={0}>
          <ButtonComponent
            backgroundColor={theme.palette.secondary.main}
            height={48}
            borderRadius={8}
            handleClick={handleSubmitPassport(values, index)}
          >
            <Typography variant="subtitle1">
              {listString.IDS_TEXT_BUTTON_ADD}
            </Typography>
          </ButtonComponent>
        </Box>
      </DialogContent>
    </Dialog>
  );
};

export default DialogPassportPc;
