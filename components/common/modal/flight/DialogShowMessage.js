import { Box, makeStyles, IconButton } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import {
  listString,
  IMAGE_LOADING_BOOK_TICKET,
  DIALOG_MESSAGE_TYPE,
  optionClean,
} from "@utils/constants";
import { useRouter } from "next/router";
import CloseIcon from "@material-ui/icons/Close";
import sanitizeHtml from "sanitize-html";
import { unEscape } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  sizeBox: {
    "& .MuiDialog-paperFullWidth": {
      width: 460,
      maxHeight: 520,
    },
  },
  container: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
  loadMoreBox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  closeButton: {
    position: "absolute",
    right: 0,
    top: 0,
  },
}));

const DialogShowMessage = ({
  message,
  title = "",
  type = DIALOG_MESSAGE_TYPE.ALERT,
  open = false,
  toggleDrawer = () => {},
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  if (message) {
    open = true;
  } else {
    open = false;
    return "";
  }

  return (
    <Dialog open={open} maxWidth="sm" className={classes.sizeBox} fullWidth>
      <DialogTitle
        disableTypography
        disableTypography
        classes={{ root: classes.dialogTitle }}
      >
        {title && <Typography variant="h6">{title}</Typography>}
        <IconButton
          onClick={toggleDrawer(false)}
          aria-label="close"
          className={classes.closeButton}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent classes={{ root: classes.dialogContent }}>
        {type === DIALOG_MESSAGE_TYPE.ALERT && (
          <Box className={classes.container} pb={4} pt={2} textAlign="center">
            <Typography variant="body1">{message}</Typography>
          </Box>
        )}
        {type === DIALOG_MESSAGE_TYPE.HTML && (
          <Box
            className={classes.container}
            pb={2}
            fontSize={14}
            lineHeight={"16px"}
            dangerouslySetInnerHTML={{
              __html: sanitizeHtml(
                unEscape(unescape(message || "")),
                optionClean
              ),
            }}
          ></Box>
        )}
      </DialogContent>
    </Dialog>
  );
};

export default DialogShowMessage;
