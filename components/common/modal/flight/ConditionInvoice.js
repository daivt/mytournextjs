import { Grid, Typography } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import Slide from "@material-ui/core/Slide";
import makeStyles from "@material-ui/styles/makeStyles";
import { IconClose } from "@public/icons";
import { listString } from "@utils/constants";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  dialogTitle: {
    maxHeight: 48,
    padding: 0,
    color: theme.palette.black.black3,
  },
  dialogContent: {
    padding: "12px 16px",
  },
  contentText: {
    color: theme.palette.black.main,
    marginBottom: 20,
  },
}));
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
const ConditionInvoice = ({ open = false, handleClose = () => {} }) => {
  const classes = useStyles();
  return (
    <Dialog
      onClose={handleClose(false)}
      open={open}
      TransitionComponent={Transition}
      fullScreen
    >
      <DialogTitle disableTypography classes={{ root: classes.dialogTitle }}>
        <Grid container style={{ height: 48 }}>
          <Grid item style={{ position: "absolute" }}>
            <IconButton onClick={handleClose(false)}>
              <IconClose />
            </IconButton>
          </Grid>
          <Grid item style={{ margin: "auto" }}>
            <Typography component="span" variant="subtitle1">
              {listString.IDS_TEXT_CONDITION_INVOICE_TITLE}
            </Typography>
          </Grid>
        </Grid>
      </DialogTitle>
      <DialogContent dividers classes={{ root: classes.dialogContent }}>
        <Typography
          component="p"
          variant="caption"
          className={classes.contentText}
        >
          {listString.IDS_TEXT_CONDITION_INVOICE_CONTENT_1}
        </Typography>
        <Typography
          component="p"
          variant="caption"
          className={classes.contentText}
        >
          {listString.IDS_TEXT_CONDITION_INVOICE_CONTENT_2}
        </Typography>
        <Typography
          component="p"
          variant="caption"
          className={classes.contentText}
        >
          {listString.IDS_TEXT_CONDITION_INVOICE_CONTENT_3}
        </Typography>
        <Typography
          component="p"
          variant="caption"
          className={classes.contentText}
        >
          {listString.IDS_TEXT_CONDITION_INVOICE_CONTENT_4}
        </Typography>
        <Typography
          component="p"
          variant="caption"
          className={classes.contentText}
        >
          {listString.IDS_TEXT_CONDITION_INVOICE_CONTENT_5}
        </Typography>
      </DialogContent>
    </Dialog>
  );
};

ConditionInvoice.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func,
};

export default ConditionInvoice;
