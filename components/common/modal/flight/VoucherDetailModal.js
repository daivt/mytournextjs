import { Box, ButtonBase, Drawer, makeStyles } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import { listString, optionClean } from "@utils/constants";
import { useState } from "react";
import moment from "moment";
import { DATE_FORMAT } from "@utils/moment";
import sanitizeHtml from "sanitize-html";
import { unEscape } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    width: "100vw",
  },
  bannerTop: {
    position: "relative",
    "& img": {
      width: "100vw",
      maxHeight: 170,
    },
  },
  btnClose: {
    position: "absolute",
    top: "20%",
  },
  voucherCode: {
    padding: "4px 8px",
    borderRadius: 4,
    backgroundColor: theme.palette.blue.blueLight8,
    color: theme.palette.white.main,
  },
  expiredItem: {
    color: theme.palette.gray.grayDark7,
  },
  footer: {
    position: "fixed",
    bottom: 0,
    width: "100%",
    borderTop: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  copyVoucher: {
    width: "100%",
    padding: "14px 0 14px 0",
    backgroundColor: theme.palette.pink.main,
    color: theme.palette.white.main,
    borderRadius: 8,
  },
}));

const VoucherDetailModal = ({
  data = {},
  promoCodeSelected = {},
  openPromo = "",
  toggleDrawerPromo = () => {},
}) => {
  const classes = useStyles();
  const theme = useTheme();

  const handleCopyVoucher = (code) => {
    toggleDrawerPromo("");
    navigator.clipboard.writeText(code);
  };

  const listVoucher = data;
  return (
    <Drawer
      anchor="right"
      open={openPromo}
      onClose={() => toggleDrawerPromo("")}
      classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
    >
      <Box className={classes.container}>
        <Box className={classes.bannerTop}>
          <CloseIcon
            style={{ marginLeft: 16, color: theme.palette.white.main }}
            fontSize={"small"}
            className={classes.btnClose}
            onClick={() => toggleDrawerPromo("")}
          />
          <img alt="" src={promoCodeSelected?.rewardProgram?.banner} />
        </Box>
        <Box mx={2} display="flex" flexDirection="column">
          <Box pt={20 / 8} display="flex">
            <Typography variant="subtitle2" className={classes.voucherCode}>
              {promoCodeSelected?.codeDetail}
            </Typography>
          </Box>
          <Box pt={1}>
            <Typography variant="h5">
              {promoCodeSelected?.rewardProgram?.title}
            </Typography>
          </Box>
          <Box pt={12 / 8}>
            <Typography variant="caption" className={classes.expiredItem}>
              {`${listString.IDS_TEXT_DISCOUNT_CARD_EXPIRED}: ${moment(
                promoCodeSelected?.responseData?.validTo
              ).format(DATE_FORMAT)}`}
            </Typography>
          </Box>

          <Box
            pt={3}
            pb={8}
            fontSize={14}
            dangerouslySetInnerHTML={{
              __html: sanitizeHtml(
                unEscape(
                  unescape(promoCodeSelected?.rewardProgram?.description || "")
                ),
                optionClean
              ),
            }}
          />
        </Box>
      </Box>
      <Box className={classes.footer}>
        <Box py={1} px={2}>
          <ButtonBase
            className={classes.copyVoucher}
            onClick={() => handleCopyVoucher(promoCodeSelected?.codeDetail)}
          >
            <Typography variant="subtitle1">
              {listString.IDS_MT_TEXT_INFO_COPPY}
            </Typography>
          </ButtonBase>
        </Box>
      </Box>
    </Drawer>
  );
};
export default VoucherDetailModal;
