import { Box, Drawer, Grid, IconButton, makeStyles } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import ButtonComponent from "@src/button/Button";
import { listString } from "@utils/constants";
import { Form, Formik } from "formik";

const useStyles = makeStyles((theme) => ({
  mainDrawerFilter: {
    width: "100vw",
    fontSize: 16,
    overflow: "auto",
    padding: "0 16px",
  },
  container: {
    padding: "16px 0px 8px 0px",
    display: "flex",
    flexDirection: "column",
    borderRadius: "16px 16px 0px 0px",
  },
  slBox: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  leftBox: {
    padding: "30px 0",
  },
  rightBox: {
    padding: "20px 0",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  btn: {
    backgroundColor: theme.palette.gray.grayLight22,
    padding: 20,
    width: 40,
    height: 40,
    "&:hover": {
      backgroundColor: theme.palette.gray.grayLight22,
      padding: 20,
      width: 40,
      height: 40,
    },
  },
  numAdult: {
    paddingTop: 10,
    display: "flex",
  },
  btnCancel: {
    backgroundColor: theme.palette.gray.grayLight22,
    color: theme.palette.black.black3,
    marginRight: 12,
  },
  btnSubmit: {
    backgroundColor: theme.palette.pink.main,
    color: theme.palette.white.main,
  },
  btnBottom: {
    paddingTop: 8,
    display: "flex",
    flexDirection: "row",
  },
  disabledButtonIcon: {
    backgroundColor: theme.palette.gray.grayLight23,
    color: theme.palette.gray.grayLight24,
    "&:disabled": {
      backgroundColor: theme.palette.gray.grayLight23,
      color: theme.palette.gray.grayLight24,
    },
  },
  flRight: {
    display: "flex",
    justifyContent: "flex-end",
  },
}));

const RenderButton = (props) => {
  const { input, value, type } = props;
  const classes = useStyles();
  return (
    <Box className={classes.rightBox}>
      {type ? (
        <IconButton
          className={classes.btn}
          onClick={() => props.onChange(input, value + 1)}
          disabled={
            (input === "childCount" && value === 8) ||
            (input === "adultCount" && value === 9)
          }
          classes={{ disabled: classes.disabledButtonIcon }}
        >
          <AddIcon />
        </IconButton>
      ) : (
        <IconButton
          className={classes.btn}
          onClick={() => props.onChange(input, value - 1)}
          disabled={
            (input === "adultCount" && value === 1) ||
            ((input === "childCount" || input === "infantCount") && value === 0)
          }
          classes={{ disabled: classes.disabledButtonIcon }}
        >
          <RemoveIcon />
        </IconButton>
      )}
    </Box>
  );
};

const PersonSelectionModal = ({
  open = false,
  toggleDrawer = () => {},
  handleOk = () => {},
  paramsSearch = {},
}) => {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <Drawer
      anchor="bottom"
      open={open}
      onClose={toggleDrawer("")}
      classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
    >
      <Box className={classes.mainDrawerFilter}>
        <Box className={classes.wrapper}>
          <Formik
            initialValues={{
              adultCount: paramsSearch.adultCount,
              childCount: paramsSearch.childCount,
              infantCount: paramsSearch.infantCount,
            }}
          >
            {({ values, isSubmitting, resetForm, setFieldValue }) => {
              const handleChangeValue = (field, value) => {
                const maxLimit = 9;
                if (value < 0) return false;
                if (field === "adultCount") {
                  if (
                    value + values.childCount >= maxLimit &&
                    values.childCount > maxLimit - value
                  ) {
                    setFieldValue("adultCount", value);
                    setFieldValue("childCount", maxLimit - value);
                  } else {
                    if (value < values.infantCount) {
                      setFieldValue("adultCount", value);
                      setFieldValue("infantCount", value);
                    } else {
                      setFieldValue(field, value);
                    }
                  }
                }
                if (field === "childCount") {
                  if (
                    value + values.adultCount >= maxLimit &&
                    values.adultCount > maxLimit - value
                  ) {
                    setFieldValue("childCount", value);
                    setFieldValue("adultCount", maxLimit - value);
                    if (values.infantCount > maxLimit - value) {
                      setFieldValue("infantCount", maxLimit - value);
                    }
                  } else {
                    setFieldValue(field, value);
                  }
                }
                if (field === "infantCount") {
                  if (value > values.adultCount) {
                    setFieldValue(field, values.adultCount);
                  } else {
                    setFieldValue(field, value);
                  }
                }
              };

              return (
                <Form style={{ width: "100%" }}>
                  <Box className={classes.container}>
                    <Grid container className={classes.slBox}>
                      <Grid item lg={7} md={7} sm={7} xs={7}>
                        <Box className={classes.leftBox}>
                          <Typography variant="body1" component="span">
                            {listString.IDS_MT_TEXT_ADULT}
                          </Typography>
                        </Box>
                      </Grid>
                      <Grid
                        item
                        lg={2}
                        md={2}
                        sm={2}
                        xs={2}
                        className={classes.flRight}
                      >
                        <RenderButton
                          input="adultCount"
                          type={0}
                          value={values.adultCount}
                          onChange={handleChangeValue}
                        />
                      </Grid>
                      <Grid
                        item
                        lg={1}
                        md={1}
                        sm={1}
                        xs={1}
                        className={classes.flRight}
                      >
                        <Box className={classes.rightBox}>
                          <Typography
                            className={classes.numAdult}
                            variant="subtitle1"
                          >
                            {values.adultCount}
                          </Typography>
                        </Box>
                      </Grid>
                      <Grid
                        item
                        lg={2}
                        md={2}
                        sm={2}
                        xs={2}
                        className={classes.flRight}
                      >
                        <RenderButton
                          input="adultCount"
                          type={1}
                          value={values.adultCount}
                          onChange={handleChangeValue}
                        />
                      </Grid>
                    </Grid>

                    <Grid container className={classes.slBox}>
                      <Grid item lg={7} md={7} sm={7} xs={7}>
                        <Box className={classes.leftBox}>
                          <Typography variant="body1" component="span">
                            {listString.IDS_MT_TEXT_CHILDREN}{" "}
                          </Typography>
                          <Typography
                            variant="body1"
                            component="span"
                            style={{ color: theme.palette.gray.grayDark7 }}
                          >
                            {listString.IDS_MT_TEXT_CHILDREN_AGE}
                          </Typography>
                        </Box>
                      </Grid>
                      <Grid
                        item
                        lg={2}
                        md={2}
                        sm={2}
                        xs={2}
                        className={classes.flRight}
                      >
                        <RenderButton
                          input="childCount"
                          type={0}
                          value={values.childCount}
                          onChange={handleChangeValue}
                        />
                      </Grid>
                      <Grid
                        item
                        lg={1}
                        md={1}
                        sm={1}
                        xs={1}
                        className={classes.flRight}
                      >
                        <Box className={classes.rightBox}>
                          <Typography
                            className={classes.numAdult}
                            variant="subtitle1"
                          >
                            {values.childCount}
                          </Typography>
                        </Box>
                      </Grid>
                      <Grid
                        item
                        lg={2}
                        md={2}
                        sm={2}
                        xs={2}
                        className={classes.flRight}
                      >
                        <RenderButton
                          input="childCount"
                          type={1}
                          value={values.childCount}
                          onChange={handleChangeValue}
                        />
                      </Grid>
                    </Grid>

                    <Grid container>
                      <Grid item lg={7} md={7} sm={7} xs={7}>
                        <Box className={classes.leftBox}>
                          <Typography variant="body1" component="span">
                            {listString.IDS_MT_TEXT_BABY}{" "}
                          </Typography>
                          <Typography
                            variant="body1"
                            component="span"
                            style={{ color: theme.palette.gray.grayDark7 }}
                          >
                            {listString.IDS_MT_TEXT_BABY_AGE}
                          </Typography>
                        </Box>
                      </Grid>
                      <Grid
                        item
                        lg={2}
                        md={2}
                        sm={2}
                        xs={2}
                        className={classes.flRight}
                      >
                        <RenderButton
                          input="infantCount"
                          type={0}
                          value={values.infantCount}
                          onChange={handleChangeValue}
                        />
                      </Grid>
                      <Grid
                        item
                        lg={1}
                        md={1}
                        sm={1}
                        xs={1}
                        className={classes.flRight}
                      >
                        <Box className={classes.rightBox}>
                          <Typography
                            variant="subtitle1"
                            className={classes.numAdult}
                          >
                            {values.infantCount}
                          </Typography>
                        </Box>
                      </Grid>
                      <Grid
                        item
                        lg={2}
                        md={2}
                        sm={2}
                        xs={2}
                        className={classes.flRight}
                      >
                        <RenderButton
                          input="infantCount"
                          type={1}
                          value={values.infantCount}
                          onChange={handleChangeValue}
                        />
                      </Grid>
                    </Grid>

                    <Box className={classes.btnBottom}>
                      <ButtonComponent
                        className={classes.btnCancel}
                        handleClick={toggleDrawer("")}
                      >
                        <Typography variant="subtitle1">
                          {listString.IDS_MT_TEXT_CANCEL_VN}
                        </Typography>
                      </ButtonComponent>
                      <ButtonComponent
                        className={classes.btnSubmit}
                        handleClick={handleOk(values)}
                      >
                        <Typography variant="subtitle1">
                          {listString.IDS_MT_TEXT_SELECT}
                        </Typography>
                      </ButtonComponent>
                    </Box>
                  </Box>
                </Form>
              );
            }}
          </Formik>
        </Box>
      </Box>
    </Drawer>
  );
};

export default PersonSelectionModal;
