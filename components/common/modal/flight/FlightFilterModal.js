import {
  Box,
  Drawer,
  Grid,
  makeStyles,
  withStyles,
  ButtonBase,
} from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import Slider from "@material-ui/core/Slider";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import { IconCheckBox, IconCheckBoxActive } from "@public/icons";
import ButtonComponent from "@src/button/Button";
import Image from "@src/image/Image";
import { LIFT_TIME_LIST, listString, NUM_STOP_LIST } from "@utils/constants";
import { getFlightTimeText } from "@utils/helpers";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
  },
  headerBox: {
    padding: "14px 0",
    borderTop: `solid 1px ${theme.palette.gray.grayLight22}`,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    position: "fixed",
    top: 0,
    backgroundColor: theme.palette.white.main,
    zIndex: 10,
  },
  bodyBox: {
    padding: 16,
    display: "flex",
    flexDirection: "column",
  },
  partnerItem: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    padding: "12px 0",
  },
  liftItem: {
    border: `solid 1px ${theme.palette.gray.grayLight25}`,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
    padding: 8,
    "&.liftName": {
      color: theme.palette.gray.grayDark8,
    },
    "&.liftTime": {
      color: theme.palette.black.black3,
    },
  },
  active: {
    border: `solid 1px ${theme.palette.blue.blueLight8}`,
    backgroundColor: theme.palette.blue.blueLight9,
    "& span": {
      color: theme.palette.blue.blueLight8,
    },
    "& h6": {
      color: theme.palette.blue.blueLight8,
    },
  },
  footerBox: {
    padding: "8px 0",
    borderTop: `solid 1px ${theme.palette.gray.grayLight22}`,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    position: "fixed",
    bottom: 0,
    backgroundColor: theme.palette.white.main,
    zIndex: 10,
    width: "100%",
  },
  partnerLogo: {
    height: 24,
    width: 24,
    objectFit: "contain",
  },
  btnFilter: {
    backgroundColor: theme.palette.pink.main,
    color: theme.palette.white.main,
    borderRadius: 8,
    padding: "14px 0",
    height: 48,
    width: "100%",
  },
}));

const iOSBoxShadow =
  "0px 0.5px 4px rgba(0, 0, 0, 0.12), 0px 6px 13px rgba(0, 0, 0, 0.12)";

const marks = [
  {
    value: 0,
  },
  {
    value: 5000000,
  },
];

const IOSSlider = withStyles((theme) => ({
  thumb: {
    height: 28,
    width: 28,
    backgroundColor: theme.palette.white.main,
    boxShadow: iOSBoxShadow,
    border: "none",
    marginTop: -14,
    marginLeft: -14,
    "&:focus, &:hover, &$active": {
      boxShadow:
        "0px 0.5px 4px rgba(0, 0, 0, 0.12), 0px 6px 13px rgba(0, 0, 0, 0.12)",
      // Reset on touch devices, it doesn't add specificity
      "@media (hover: none)": {
        boxShadow: iOSBoxShadow,
      },
    },
  },
  active: {},
  track: {
    height: 4,
    backgroundColor: theme.palette.blue.blueLight8,
  },
  rail: {
    height: 4,
    opacity: 0.5,
    backgroundColor: theme.palette.gray.grayLight22,
  },
  root: {
    width: "95%",
  },
}))(Slider);

const RenderPrice = (props) => {
  const classes = useStyles();
  let maxPrice = props?.data?.maxPrice;

  const handleChangePrice = (event, price) => {
    props.handleSetSingleParam("priceFilter", price);
  };

  let paramChecked = props.paramFilter.priceFilter;
  if (paramChecked === 0) {
    paramChecked = maxPrice;
  }

  return (
    <Box className={classes.priceFilter}>
      <Typography variant="subtitle1">
        {listString.IDS_MT_TEXT_FILTER_PRICE_RANGE}
      </Typography>
      <Box mt={1}>
        <Typography variant="caption">
          0đ - {paramChecked.formatMoney()}đ
        </Typography>
      </Box>
      <div className={classes.root}>
        <IOSSlider
          key={`slider-${paramChecked}`}
          aria-label="ios slider"
          defaultValue={paramChecked}
          valueLabelDisplay="off"
          onChangeCommitted={handleChangePrice}
          step={1000}
          min={0}
          max={maxPrice}
        />
      </div>
    </Box>
  );
};

const FilterCheckbox = withStyles((theme) => ({
  root: {
    padding: 0,
    color: theme.palette.gray.grayLight25,
    "&$checked": {
      color: theme.palette.blue.blueLight8,
    },
  },
  checked: {},
}))((props) => (
  <Checkbox
    color="default"
    {...props}
    icon={<IconCheckBox />}
    checkedIcon={props.checked ? <IconCheckBoxActive /> : <div></div>}
  />
));

const RenderAirline = ({ handleSetArrayParam, paramFilter, branchAir }) => {
  const classes = useStyles();
  const handleSelectPartner = (newValue) => {
    handleSetArrayParam("airlines", newValue);
  };

  const paramChecked = paramFilter.airlines;
  return (
    <Box className={classes.partnerFilter} pt={2}>
      <Typography variant="subtitle1">
        {listString.IDS_MT_TEXT_FILTER_PARTNER}
      </Typography>
      <Box className={classes.listPartner} pt={1}>
        {branchAir?.map((item, index) => (
          <Grid
            container
            className={classes.partnerItem}
            key={index}
            onClick={() => handleSelectPartner(item?.id)}
          >
            <Grid item lg={1} md={1} sm={1} xs={1}>
              <Image srcImage={item.logo} className={classes.partnerLogo} />
            </Grid>
            <Grid item lg={10} md={10} sm={10} xs={10}>
              <Typography variant="body1" style={{ paddingLeft: 8 }}>
                {item.name}
              </Typography>
            </Grid>
            <Grid item lg={1} md={1} sm={1} xs={1}>
              <FilterCheckbox
                checked={paramChecked.includes(item?.id)}
                name="partnerCheckbox"
              />
            </Grid>
          </Grid>
        ))}
      </Box>
    </Box>
  );
};

const RenderFlightTime = (props) => {
  const classes = useStyles();
  const maxFlightTime = props?.data?.maxFlightTime;
  const handleChangeFlightTime = (event, newValue) => {
    props.handleSetSingleParam("flyTimeFilter", newValue);
  };

  let paramChecked = props.paramFilter.flyTimeFilter;
  if (paramChecked === 0) {
    paramChecked = maxFlightTime;
  }

  return (
    <Box className={classes.timeFilter} pt={5}>
      <Typography variant="subtitle1">
        {listString.IDS_MT_TEXT_FILTER_FLY_TIME}
      </Typography>
      <Box mt={1}>
        <Typography variant="caption">
          0h - {getFlightTimeText(paramChecked)}
          {"’"}
        </Typography>
      </Box>
      <Box className={classes.root}>
        <IOSSlider
          value={paramChecked}
          valueLabelDisplay="off"
          onChange={handleChangeFlightTime}
          step={1}
          min={0}
          max={maxFlightTime}
        />
      </Box>
    </Box>
  );
};

const RenderLiftTime = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const liftTimeList = LIFT_TIME_LIST;

  const handleSelectLiftTime = (liftType) => {
    props.handleCheckLift("liftTimeFilter", liftType);
  };

  const paramChecked = props.paramFilter.liftTimeFilter;

  return (
    <Box className={classes.liftTimeFilter} pt={5}>
      <Typography variant="subtitle1">
        {listString.IDS_MT_TEXT_FILTER_LIFT_TIME}
      </Typography>
      <Box display="flex" flexDirection="row" pt={2}>
        <Grid container spacing={1}>
          {liftTimeList.map((item, index) => (
            <Grid
              item
              lg={6}
              md={6}
              sm={6}
              xs={6}
              key={index}
              onClick={() => handleSelectLiftTime(item?.id)}
            >
              <Box
                className={clsx(
                  classes.liftItem,
                  paramChecked.includes(item?.id) && classes.active
                )}
              >
                <Typography variant="caption" className={classes.liftName}>
                  {item.name}
                </Typography>
                <Typography variant="subtitle2" className={classes.liftTime}>
                  {item.from} - {item.to}
                </Typography>
              </Box>
            </Grid>
          ))}
        </Grid>
      </Box>
    </Box>
  );
};

const RenderSeat = (props) => {
  const classes = useStyles();
  const handleSelectSeat = (newValue) => {
    props.handleCheckSeat("seatFilter", newValue);
  };
  const seatList = props.data;
  const paramChecked = props.paramFilter.seatFilter;

  return (
    <Box className={classes.partnerFilter} pt={5}>
      <Typography variant="subtitle1">
        {listString.IDS_MT_TEXT_FILTER_SEAT_TYPE}
      </Typography>
      <Box className={classes.listPartner} pt={1}>
        {seatList.map((item, index) => (
          <Grid
            container
            className={classes.partnerItem}
            key={index}
            onClick={() => handleSelectSeat(item?.code)}
          >
            <Grid item lg={11} md={11} sm={11} xs={11}>
              <Typography variant="body1">{item.v_name}</Typography>
            </Grid>
            <Grid item lg={1} md={1} sm={1} xs={1}>
              <FilterCheckbox
                name="seatTypeCheckbox"
                checked={paramChecked.includes(item?.code)}
              />
            </Grid>
          </Grid>
        ))}
      </Box>
    </Box>
  );
};

const RenderNumStop = ({ numStops, handleCheckNumStop, paramFilter }) => {
  const classes = useStyles();
  const handleSelectNumStop = (newValue) => {
    handleCheckNumStop("numStops", newValue);
  };
  const paramChecked = paramFilter.numStops;
  const tempListNumStop = NUM_STOP_LIST.slice(0, numStops?.length);

  return (
    <Box className={classes.partnerFilter} pt={5}>
      <Typography variant="subtitle1">Điểm dừng</Typography>
      <Box className={classes.listPartner}>
        {tempListNumStop?.map((item, index) => (
          <Grid
            container
            className={classes.partnerItem}
            key={index}
            onClick={() => handleSelectNumStop(item?.id)}
          >
            <Grid item lg={1} md={1} sm={1} xs={1}>
              <FilterCheckbox
                name="numStopCheckbox"
                checked={paramChecked.includes(item?.id)}
              />
            </Grid>
            <Grid item lg={11} md={11} sm={11} xs={11}>
              <Typography style={{ marginLeft: 8 }} variant="body1">
                {item.name}
              </Typography>
            </Grid>
          </Grid>
        ))}
      </Box>
    </Box>
  );
};

const FlightFilterModal = ({
  dataFilter = {},
  paramFilter = {},
  open = false,
  toggleDrawer = () => {},
  handleFilter = () => {},
  handleSetSingleParam = () => {},
  handleSetArrayParam = () => {},
  handleReset = () => {},
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const branchPartner = dataFilter.branchPartner;
  const ticketClass = dataFilter.ticketClass;
  const filterPriceLimit = dataFilter.filterPriceLimit;
  const numStops = dataFilter?.numStops;

  return (
    <Drawer
      anchor="right"
      open={open}
      onClose={toggleDrawer("")}
      classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
    >
      <Box className={classes.container}>
        <Grid container className={classes.headerBox}>
          <Grid item lg={4} md={4} sm={4} xs={4} onClick={toggleDrawer("")}>
            <CloseIcon style={{ fontSize: 20, marginLeft: 16 }} />
          </Grid>
          <Grid item lg={4} md={4} sm={4} xs={4}>
            <Box display="flex" justifyContent="center">
              <Typography variant="subtitle1">
                {listString.IDS_MT_TEXT_FILTER_SINGLE}
              </Typography>
            </Box>
          </Grid>
          <Grid item lg={4} md={4} sm={4} xs={4}>
            <Box
              display="flex"
              justifyContent="flex-end"
              color={theme.palette.blue.blueLight8}
              mr={2}
              onClick={handleReset(1)}
            >
              <Typography variant="body1">
                {listString.IDS_MT_TEXT_CLEAR_FILTER}
              </Typography>
            </Box>
          </Grid>
        </Grid>
        <Box className={classes.bodyBox} mt={7} mb={15}>
          <RenderPrice
            data={filterPriceLimit}
            handleSetSingleParam={handleSetSingleParam}
            paramFilter={paramFilter}
          />
          <RenderAirline
            branchAir={branchPartner}
            handleSetArrayParam={handleSetArrayParam}
            paramFilter={paramFilter}
          />
          {/* <RenderFlightTime
            data={filterPriceLimit}
            handleSetSingleParam={handleSetSingleParam}
            paramFilter={paramFilter}
          /> */}
          <RenderLiftTime
            handleCheckLift={handleSetArrayParam}
            paramFilter={paramFilter}
          />
          <RenderSeat
            data={ticketClass}
            handleCheckSeat={handleSetArrayParam}
            paramFilter={paramFilter}
          />
          <RenderNumStop
            numStops={numStops}
            handleCheckNumStop={handleSetArrayParam}
            paramFilter={paramFilter}
          />
        </Box>
        <Box className={classes.footerBox}>
          <Box mx={[2]}>
            <ButtonBase className={classes.btnFilter} onClick={handleFilter(1)}>
              <Typography variant="subtitle1">
                {listString.IDS_MT_TEXT_FILTER_SINGLE}
              </Typography>
            </ButtonBase>
          </Box>
        </Box>
      </Box>
    </Drawer>
  );
};

export default FlightFilterModal;
