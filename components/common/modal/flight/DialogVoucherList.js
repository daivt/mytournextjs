import VoucherCard from "@components/common/card/flight/VoucherCard";
import { Box, ButtonBase, IconButton, makeStyles } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import FieldTextContent from "@src/form/FieldTextContent";
import { listString } from "@utils/constants";
import { Form, Formik } from "formik";
import { useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { isEmpty } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  sizeBox: {
    "& .MuiDialog-paperFullWidth": {
      width: 480,
      height: 520,
    },
  },
  container: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
  headerBox: {
    padding: "14px 0",
    borderTop: `solid 1px ${theme.palette.gray.grayLight22}`,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  searchVoucher: {
    backgroundColor: theme.palette.pink.main,
    color: theme.palette.white.main,
    padding: 12,
    borderRadius: 8,
    width: "25%",
  },
  inputBox: {
    paddingBottom: 4,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight25}`,
  },
  wrapper: {},
  voucherItem: {
    boxShadow:
      "0px 0px 6px rgba(0, 0, 0, 0.06), 0px 6px 6px rgba(0, 0, 0, 0.06)",
    borderRadius: 8,
  },
  closeButton: {
    position: "absolute",
    right: 0,
    top: 0,
  },
  inputVoucher: {
    paddingLeft: 0,
  },
}));

const DialogVoucherList = ({
  data = {},
  open = false,
  toggleDrawer = () => {},
  handleSetVoucher = () => {},
  voucherUsing = {},
  totalVoucher = 0,
  setInputSearchVoucher,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const [inputSearch, setInputSearch] = useState("");
  const handleChangeInput = (event) => {
    const value = event.target.value;
    setInputSearch(value);
  };

  const handleSearchVoucher = (event) => {
    setInputSearchVoucher(inputSearch);
  };

  const listVoucher = data;
  return (
    <Dialog
      onClose={toggleDrawer(false)}
      open={open}
      className={classes.sizeBox}
      maxWidth={"sm"}
      fullWidth
    >
      <DialogTitle
        disableTypography
        onClose={toggleDrawer(false)}
        disableTypography
        classes={{ root: classes.dialogTitle }}
      >
        <Typography variant="h6">
          {listString.IDS_TEXT_VOUCHER_SELECT}
        </Typography>
        <IconButton
          onClick={toggleDrawer(false)}
          aria-label="close"
          className={classes.closeButton}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent classes={{ root: classes.dialogContent }}>
        <Box className={classes.container}>
          <Box className={classes.inputBox}>
            <Formik>
              {() => {
                return (
                  <Form autoComplete="off">
                    <FieldTextContent
                      name="searchVoucher"
                      placeholder={
                        listString.IDS_MT_TEXT_PLACEHOLDER_ENTER_CODE_SALE_OFF
                      }
                      inputProps={{
                        autoComplete: "off",
                      }}
                      inputStyle={{
                        border: 0,
                        borderRadius: 0,
                        margin: "2px 0 0 0",
                      }}
                      helperTextStyle={{ display: "none" }}
                      formControlStyle={{ marginRight: 0, width: "75%" }}
                      optional
                      value={inputSearch}
                      onChange={handleChangeInput}
                      className={classes.inputVoucher}
                      onKeyPress={(event) => {
                        if (event.key === "Enter") {
                          handleSearchVoucher();
                          event.preventDefault();
                        }
                      }}
                    />
                    <ButtonBase
                      className={classes.searchVoucher}
                      onClick={handleSearchVoucher}
                    >
                      <Typography variant="subtitle2">
                        {listString.IDS_TEXT_USE}
                      </Typography>
                    </ButtonBase>
                  </Form>
                );
              }}
            </Formik>
          </Box>
          <Box className={classes.wrapper} mt={20 / 8}>
            <Box display="flex" alignItems="center">
              <Box pr={4 / 8}>
                <Typography variant="subtitle2">
                  {listString.IDS_TEXT_VOUCHER_TIPS}{" "}
                </Typography>
              </Box>
              <Typography variant="caption">({totalVoucher} mã)</Typography>
            </Box>

            <Box className={classes.listVoucher}>
              {!isEmpty(listVoucher) ? (
                listVoucher.map((item, index) => (
                  <Box className={classes.voucherItem} key={index} mt={2}>
                    <VoucherCard
                      item={item}
                      bgCircle={theme.palette.gray.grayLight29}
                      hasBorder={false}
                      voucherUsing={voucherUsing}
                      handleSetVoucher={handleSetVoucher}
                    />
                  </Box>
                ))
              ) : (
                <Box display="flex" py={3}>
                  <Typography variant="caption">
                    Không tìm thấy mã giảm giá nào.
                  </Typography>
                </Box>
              )}
            </Box>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
};
export default DialogVoucherList;
