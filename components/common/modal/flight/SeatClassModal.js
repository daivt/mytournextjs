import { Box, Drawer, Grid, makeStyles } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import CheckIcon from "@material-ui/icons/Check";
import { SEAT_TYPE_LIST } from "@utils/constants";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    borderRadius: "16px 16px 0px 0px",
  },
  wrapper: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    padding: "12px 0",
    "&:last-child": {
      borderBottom: 0,
    },
  },
  shortDes: {
    color: theme.palette.gray.grayDark7,
  },
  activeS: {
    color: theme.palette.blue.blueLight8,
  },
  rightBox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    color: theme.palette.blue.blueLight8,
  },
  hideIcon: {
    display: "none",
  },
  showIcon: {
    display: "flex",
  },
  mainDrawerFilter: {
    width: "100vw",
    fontSize: 16,
    overflow: "auto",
    padding: 16,
  },
}));

const SeatClassModal = ({
  open = false,
  toggleDrawer = () => {},
  handleOk = () => {},
  paramsSearch = {},
}) => {
  const classes = useStyles();
  const dataTemp = SEAT_TYPE_LIST;
  const theme = useTheme();

  return (
    <Drawer
      anchor="bottom"
      open={open}
      onClose={toggleDrawer("")}
      classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
    >
      <Box className={(classes.container, classes.mainDrawerFilter)}>
        {dataTemp.map((el, index) => (
          <Grid container className={classes.wrapper} key={index}>
            <Grid
              item
              lg={10}
              md={10}
              sm={10}
              xs={10}
              onClick={handleOk(el?.cid)}
            >
              <Box className={classes.leftBox}>
                <Typography
                  className={clsx(
                    paramsSearch?.seatSearch === el?.cid && classes.activeS
                  )}
                  variant="body1"
                >
                  {el.name}
                </Typography>
                <Typography className={classes.shortDes} variant="caption">
                  {el.description}
                </Typography>
              </Box>
            </Grid>
            <Grid item lg={2} md={2} sm={2} xs={2} className={classes.rightBox}>
              <Box>
                <CheckIcon
                  className={clsx(
                    paramsSearch?.seatSearch === el?.cid && classes.showIcon,
                    classes.hideIcon
                  )}
                />
              </Box>
            </Grid>
          </Grid>
        ))}
      </Box>
    </Drawer>
  );
};

export default SeatClassModal;
