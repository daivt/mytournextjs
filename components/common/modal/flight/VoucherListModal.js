import VoucherCard from "@components/common/card/flight/VoucherCard";
import { Box, ButtonBase, Drawer, Grid, makeStyles } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import FieldTextContent from "@src/form/FieldTextContent";
import { listString } from "@utils/constants";
import { Form, Formik } from "formik";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    marginTop: 10,
    width: "100vw",
  },
  headerBox: {
    padding: "14px 0",
    borderTop: `solid 1px ${theme.palette.gray.grayLight22}`,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  searchVoucher: {
    backgroundColor: theme.palette.pink.main,
    color: theme.palette.white.main,
    padding: 12,
    borderRadius: 8,
    width: "25%",
  },
  inputBox: {
    paddingBottom: 4,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight25}`,
  },
  wrapper: {
    backgroundColor: theme.palette.gray.grayLight22,
    padding: 16,
  },
  voucherItem: {
    boxShadow:
      "0px 0px 6px rgba(0, 0, 0, 0.06), 0px 6px 6px rgba(0, 0, 0, 0.06)",
    borderRadius: 8,
  },
}));

const VoucherListModal = ({
  data = {},
  open = false,
  toggleDrawer = () => {},
  handleSetVoucher = () => {},
  voucherUsing = {},
  totalVoucher = 0,
  setInputSearchVoucher,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const [inputSearch, setInputSearch] = useState("");
  const handleChangeInput = (event) => {
    const value = event.target.value;
    setInputSearch(value);
  };

  const handleSearchVoucher = (event) => {
    setInputSearchVoucher(inputSearch);
  };

  const listVoucher = data;
  return (
    <Drawer
      anchor="right"
      open={open}
      onClose={toggleDrawer(false)}
      classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
    >
      <Box className={classes.container}>
        <Grid container className={classes.headerBox}>
          <Grid item lg={4} md={4} sm={4} xs={4} onClick={toggleDrawer(false)}>
            <CloseIcon style={{ marginLeft: 16 }} fontSize={"small"} />
          </Grid>
          <Grid item lg={8} md={8} sm={8} xs={8}>
            <Box display="flex">
              <Typography variant="subtitle1">
                {listString.IDS_TEXT_VOUCHER}
              </Typography>
            </Box>
          </Grid>
        </Grid>
        <Box>
          <Box className={classes.inputBox} m={2}>
            <Formik>
              {() => {
                return (
                  <Form autoComplete="off">
                    <FieldTextContent
                      name="searchVoucher"
                      placeholder={
                        listString.IDS_MT_TEXT_PLACEHOLDER_ENTER_CODE_SALE_OFF
                      }
                      inputProps={{
                        autoComplete: "off",
                      }}
                      inputStyle={{
                        border: 0,
                        borderRadius: 0,
                        margin: "2px 0 0 0",
                      }}
                      helperTextStyle={{ display: "none" }}
                      formControlStyle={{ marginRight: 0, width: "75%" }}
                      optional
                      value={inputSearch}
                      onChange={handleChangeInput}
                      onKeyPress={(event) => {
                        if (event.key === "Enter") {
                          handleSearchVoucher();
                          event.preventDefault();
                        }
                      }}
                    />
                    <ButtonBase
                      className={classes.searchVoucher}
                      onClick={handleSearchVoucher}
                    >
                      <Typography variant="subtitle2">
                        {listString.IDS_TEXT_USE}
                      </Typography>
                    </ButtonBase>
                  </Form>
                );
              }}
            </Formik>
          </Box>
          <Box className={classes.wrapper}>
            <Box display="flex" alignItems="center">
              <Box pr={4 / 8}>
                <Typography variant="subtitle2">
                  {listString.IDS_TEXT_VOUCHER_TIPS}{" "}
                </Typography>
              </Box>
              <Typography variant="caption">({totalVoucher} mã)</Typography>
            </Box>

            <Box className={classes.listVoucher}>
              {listVoucher.map((item, index) => (
                <Box className={classes.voucherItem} key={index} mt={2}>
                  <VoucherCard
                    item={item}
                    bgCircle={theme.palette.gray.grayLight29}
                    hasBorder={false}
                    voucherUsing={voucherUsing}
                    handleSetVoucher={handleSetVoucher}
                  />
                </Box>
              ))}
            </Box>
          </Box>
        </Box>
      </Box>
    </Drawer>
  );
};
export default VoucherListModal;
