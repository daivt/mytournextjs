import { Box, makeStyles } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { listString, IMAGE_LOADING_BOOK_TICKET } from "@utils/constants";
import { useRouter } from "next/router";

const useStyles = makeStyles((theme) => ({
  sizeBox: {
    "& .MuiDialog-paperFullWidth": {
      width: 460,
      maxHeight: 520,
    },
  },
  container: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
  loadMoreBox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

const DialogBookTicketProcessing = ({ open = false }) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();

  return (
    <Dialog open={open} maxWidth="sm" className={classes.sizeBox} fullWidth>
      <DialogTitle
        disableTypography
        disableTypography
        classes={{ root: classes.dialogTitle }}
      >
        <Typography variant="h6">
          {listString.IDS_TEXT_ORDER_PAYMENT_STATUS_PROCESSING}
        </Typography>
      </DialogTitle>
      <DialogContent classes={{ root: classes.dialogContent }}>
        <Box className={classes.loadMoreBox}>
          <img width="100px" src={IMAGE_LOADING_BOOK_TICKET} />
        </Box>
        <Box className={classes.container} pb={4} pt={2} textAlign="center">
          <Typography variant="caption">
            {listString.IDS_TEXT_NOTE_PROCESSING_PAYMENT}
          </Typography>
        </Box>
      </DialogContent>
    </Dialog>
  );
};

export default DialogBookTicketProcessing;
