import { Box, Grid, Typography } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import Slide from "@material-ui/core/Slide";
import makeStyles from "@material-ui/styles/makeStyles";
import {
  IconAdultFlight,
  IconBabyFlight,
  IconChildrenFlight,
  IconClose,
} from "@public/icons";
import { CUSTOMER_TYPE_TEXT, genderLabel, listString } from "@utils/constants";
import { DATE_FORMAT_BACK_END, MONTH_YEAR } from "@utils/moment";
import moment from "moment";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  dialogTitle: {
    maxHeight: 48,
    padding: 0,
    color: theme.palette.black.black3,
  },
  dialogContent: {
    padding: "12px 16px",
  },
  contentText: {
    color: theme.palette.black.main,
    marginBottom: 20,
  },
  textName: {
    marginLeft: 10,
    wordWrap: "break-word",
    maxWidth: "300px",
  },
}));
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
const OrderCustomerDetailModal = ({
  open = false,
  handleClose = () => {},
  guests,
}) => {
  const classes = useStyles();
  return (
    <Dialog
      onClose={handleClose(false)}
      open={open}
      TransitionComponent={Transition}
      fullScreen
    >
      <DialogTitle disableTypography classes={{ root: classes.dialogTitle }}>
        <Grid container alignItems="center">
          <Grid item xs={3}>
            <IconButton onClick={handleClose(false)}>
              <IconClose />
            </IconButton>
          </Grid>
          <Grid item xs={6} style={{ textAlign: "center" }}>
            <Typography component="span" variant="subtitle1">
              {listString.IDS_MT_TEXT_INFO_CUSTOMER_FLIGHT}
            </Typography>
          </Grid>
        </Grid>
      </DialogTitle>
      <DialogContent dividers classes={{ root: classes.dialogContent }}>
        {guests.length &&
          guests.map((item, index) => {
            return (
              <Box
                display="flex"
                flexDirection="column"
                pb={20 / 8}
                key={index}
              >
                <Box display="flex" alignItems="flex-end">
                  {item.ageCategory === CUSTOMER_TYPE_TEXT.ADULT ? (
                    <IconAdultFlight />
                  ) : item.ageCategory === CUSTOMER_TYPE_TEXT.CHILDREN ? (
                    <IconChildrenFlight />
                  ) : item.ageCategory === CUSTOMER_TYPE_TEXT.INFANT ? (
                    <IconBabyFlight />
                  ) : null}
                  <Typography variant="subtitle2" className={classes.textName}>
                    {item?.fullName.toUpperCase()}
                  </Typography>
                </Box>
                <Box
                  display="flex"
                  justifyContent="space-between"
                  pl={3}
                  pt={6 / 8}
                >
                  <Typography variant="caption">
                    {listString.IDS_MT_TEXT_GENDER}
                  </Typography>
                  <Typography variant="caption">
                    {genderLabel[item.gender]}
                  </Typography>
                </Box>
                {item.ageCategory === CUSTOMER_TYPE_TEXT.ADULT ? (
                  <>
                    {item?.passport && (
                      <Box
                        display="flex"
                        justifyContent="space-between"
                        pl={3}
                        pt={6 / 8}
                      >
                        <Typography variant="caption">
                          {listString.IDS_TEXT_PASSPORT_NUMBER}
                        </Typography>
                        <Typography variant="caption">
                          {item.passport}
                        </Typography>
                      </Box>
                    )}
                    {item?.nationality && (
                      <Box
                        display="flex"
                        justifyContent="space-between"
                        pl={3}
                        pt={6 / 8}
                      >
                        <Typography variant="caption">
                          {listString.IDS_TEXT_PASSPORT_RESIDENCE}
                        </Typography>
                        <Typography variant="caption">
                          {item.nationality}
                        </Typography>
                      </Box>
                    )}
                    {item?.passportCountry && (
                      <Box
                        display="flex"
                        justifyContent="space-between"
                        pl={3}
                        pt={6 / 8}
                      >
                        <Typography variant="caption">
                          {listString.IDS_TEXT_PASSPORT_COUNTRY}
                        </Typography>
                        <Typography variant="caption">
                          {item.passportCountry}
                        </Typography>
                      </Box>
                    )}
                    {item?.passportExpiry && (
                      <Box
                        display="flex"
                        justifyContent="space-between"
                        pl={3}
                        pt={6 / 8}
                      >
                        <Typography variant="caption">
                          {listString.IDS_TEXT_PASSPORT_EXPIRED}
                        </Typography>
                        <Typography variant="caption">
                          {item.passportExpiry
                            ? moment(
                                item.passportExpiry,
                                DATE_FORMAT_BACK_END
                              ).format(MONTH_YEAR)
                            : null}
                        </Typography>
                      </Box>
                    )}
                  </>
                ) : (
                  <>
                    {item?.dob && (
                      <Box
                        display="flex"
                        justifyContent="space-between"
                        pl={3}
                        pt={6 / 8}
                      >
                        <Typography variant="caption">
                          {listString.IDS_MT_TEXT_DATE_OF_BIRTH}
                        </Typography>
                        <Typography variant="caption">
                          {moment(item.dob, DATE_FORMAT_BACK_END).format("L")}
                        </Typography>
                      </Box>
                    )}
                  </>
                )}
              </Box>
            );
          })}
      </DialogContent>
    </Dialog>
  );
};

OrderCustomerDetailModal.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func,
  guests: PropTypes.array,
};

export default OrderCustomerDetailModal;
