import {
  Box,
  Drawer,
  Grid,
  makeStyles,
  IconButton,
  Divider,
} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import FlightDetailCost from "../../card/flight/FlightDetailCost";
import FlightTicketDetail from "../../card/flight/FlightTicketDetail";
import FlightTicketDetailTransit from "../../card/flight/FlightTicketDetailTransit";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    width: "100vw",
  },
  headerBox: {
    borderTop: `solid 1px ${theme.palette.gray.grayLight22}`,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  divider: {
    height: 6,
    color: theme.palette.gray.grayLight22,
    margin: "12px -16px",
  },
  bodyBox: {
    padding: 16,
    display: "flex",
    flexDirection: "column",
  },
}));

const FlightInfoBoxModal = ({
  open = false,
  setOpen = () => {},
  ticket = {},
  data,
  isTransit,
  airlineInfo,
  ticketId,
  isTwoWay,
}) => {
  const classes = useStyles();
  return (
    <Drawer
      anchor="right"
      open={open}
      onClose={() => {}}
      classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
    >
      <Box className={classes.container}>
        <Grid container className={classes.headerBox}>
          <Grid item lg={4} md={4} sm={4} xs={4}>
            <IconButton
              onClick={() => {
                setOpen(false);
              }}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
          </Grid>
          <Grid item lg={4} md={4} sm={4} xs={4}>
            <Box display="flex" justifyContent="center">
              <Typography variant="subtitle1">Thông tin vé</Typography>
            </Box>
          </Grid>
          <Grid item lg={4} md={4} sm={4} xs={4} />
        </Grid>
        <Box className={classes.bodyBox}>
          {!ticket.transitTickets ? (
            <FlightTicketDetail
              ticket={ticket}
              data={data}
              airlineInfo={airlineInfo}
              title="Chuyến bay"
              isSimple
              isSeat={false}
            />
          ) : (
            <FlightTicketDetailTransit
              ticket={ticket}
              data={data}
              isTransit={isTransit}
              airlineInfo={airlineInfo}
              title="Chuyến bay"
              isSimple
              isSeat={false}
            />
          )}
          <Divider className={classes.divider} />
          <FlightDetailCost
            isTwoWay={isTwoWay}
            ticket={ticket}
            data={data}
            isTransit={isTransit}
            airlineInfo={airlineInfo}
            title="Chọn hạng ghế và giá vé"
            isSimple={false}
            isSeat
            ticketId={ticketId}
          />
        </Box>
      </Box>
    </Drawer>
  );
};

export default FlightInfoBoxModal;
