import { Box, Drawer, Grid, makeStyles } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import CheckIcon from "@material-ui/icons/Check";
import { SORTER_FLIGHT_LIST } from "@utils/constants";
import clsx from "clsx";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  container: {
    padding: "22px 16px",
  },
  wrapper: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    padding: "14px 0",
    "&:last-child": {
      borderBottom: 0,
    },
  },
  rightBox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    color: theme.palette.blue.blueLight8,
    height: 20,
  },
  activeS: {
    color: theme.palette.blue.blueLight8,
  },
  hideIcon: {
    display: "none",
  },
  showIcon: {
    display: "flex",
  },
}));

const FlightSortModal = ({
  open = false,
  toggleDrawer = () => {},
  handleSorter = () => {},
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const sortList = SORTER_FLIGHT_LIST;
  const [activeSorter, setActiveSorter] = useState(1);
  const handleSelectSorter = (value) => {
    setActiveSorter(value);
  };

  return (
    <Drawer anchor="bottom" open={open} onClose={toggleDrawer("")}>
      <Box className={classes.container}>
        {sortList.map((el, index) => (
          <Grid
            container
            className={classes.wrapper}
            key={index}
            onClick={() => handleSelectSorter(el?.id)}
          >
            <Grid
              item
              lg={10}
              md={10}
              sm={10}
              xs={10}
              onClick={handleSorter(el?.id)}
            >
              <Box className={classes.leftBox}>
                <Typography
                  className={clsx(activeSorter === el?.id && classes.activeS)}
                  variant="body1"
                >
                  {el.name}
                </Typography>
                <Typography className={classes.shortDes} variant="caption">
                  {el.description}
                </Typography>
              </Box>
            </Grid>
            <Grid item lg={2} md={2} sm={2} xs={2} className={classes.rightBox}>
              <Box>
                <CheckIcon
                  className={clsx(
                    activeSorter === el?.id && classes.showIcon,
                    classes.hideIcon
                  )}
                />
              </Box>
            </Grid>
          </Grid>
        ))}
      </Box>
    </Drawer>
  );
};

export default FlightSortModal;
