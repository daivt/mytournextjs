import {
  Box,
  ButtonBase,
  Divider,
  Drawer,
  makeStyles,
  IconButton,
} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import {
  IconAdultFlightPc,
  IconBabyFlightPc,
  IconChildrenFlightPc,
  IconDot,
} from "@public/icons";
import clsx from "clsx";
import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import CloseIcon from "@material-ui/icons/Close";
import { listString, GENDER_LIST, CUSTOMER_TYPE } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  sizeBox: {
    "& .MuiDialog-paperFullWidth": {
      maxHeight: 520,
    },
  },
  mainDrawerFilter: {
    fontSize: 16,
    overflow: "auto",
  },
  wrapHeader: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 16,
  },
  container: {},
  slBox: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  paperAnchorBottom: {
    borderRadius: "16px 16px 0 0",
  },
  divider: {
    height: 1,
    margin: 12,
    color: theme.palette.gray.grayLight22,
  },
  IconUser2: {
    width: 12,
    height: 12,
    marginRight: 12,
  },
  closeButton: {
    position: "absolute",
    right: 0,
    top: 0,
  },
  rowInfo: {
    "&:hover": {
      backgroundColor: theme.palette.gray.grayLight22,
      borderRadius: 8,
    },
  },
  genderText: {
    color: theme.palette.gray.grayDark7,
  },
  iconDot: {
    margin: "10px 6px",
    display: "flex",
  },
}));

function getIconCustomer(type) {
  switch (type) {
    case CUSTOMER_TYPE.BABY:
      return <IconBabyFlightPc />;
      break;
    case CUSTOMER_TYPE.CHILDREN:
      return <IconChildrenFlightPc />;
      break;
    default:
      return <IconAdultFlightPc />;
      break;
  }
}

const DialogDirectory = ({
  directory = [],
  open = false,
  toggleDrawer = () => {},
  pickContact = () => {},
}) => {
  const classes = useStyles();
  return (
    <Dialog
      onClose={toggleDrawer(false)}
      open={open}
      maxWidth="xs"
      className={classes.sizeBox}
      fullWidth
    >
      <DialogTitle
        disableTypography
        onClose={toggleDrawer(false)}
        disableTypography
        classes={{ root: classes.dialogTitle }}
      >
        <Typography variant="h6">{listString.IDS_MT_TEXT_CLIENT}</Typography>
        <IconButton
          onClick={toggleDrawer(false)}
          aria-label="close"
          className={classes.closeButton}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent classes={{ root: classes.dialogContent }}>
        <Box className={classes.wrapper}>
          <Box className={classes.container}>
            {directory.map((item, index) => (
              <Box
                key={index}
                className={classes.rowInfo}
                margin="0px -12px 0 -12px"
              >
                <Box
                  display="flex"
                  onClick={() => pickContact(item)}
                  padding="12px 18px 0 12px"
                >
                  <Box>{getIconCustomer(item.type)}</Box>
                  <Box pl={18 / 8}>
                    <Box>
                      <Typography component="span" variant="body1">
                        {item?.name}
                      </Typography>
                    </Box>
                    <Box display="flex">
                      <Typography
                        component="span"
                        variant="body1"
                        className={classes.genderText}
                      >
                        {
                          GENDER_LIST.find((el) => el.id === item?.gender)
                            ?.label_1
                        }
                      </Typography>

                      {item?.dob && (
                        <>
                          <IconDot className={classes.iconDot} />
                          <Typography
                            component="span"
                            variant="body1"
                            className={classes.genderText}
                          >
                            {`${listString.IDS_MT_TEXT_DATE_OF_BIRTH}: ${item?.dob}`}
                          </Typography>
                        </>
                      )}
                      {item?.passport && (
                        <>
                          <IconDot className={classes.iconDot} />
                          <Typography
                            component="span"
                            variant="body1"
                            className={classes.genderText}
                          >
                            {`${listString.IDS_MT_TEXT_PASSPORT}: ${item?.passport}`}
                          </Typography>
                        </>
                      )}
                    </Box>
                  </Box>
                </Box>
                <Divider className={classes.divider} />
              </Box>
            ))}
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
};

export default DialogDirectory;
