import {
  Box,
  Divider,
  Drawer,
  Grid,
  IconButton,
  makeStyles,
} from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { IconClose } from "@public/icons";
import { listString } from "@utils/constants";
import clsx from "clsx";
import { isEmpty } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  container: {
    overflow: "auto",
  },
  boxPriceDetail: {
    padding: "0 16px",
  },
  wrapHeader: {
    alignItems: "center",
    height: 48,
    padding: 16,
  },
  slBox: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  btnClose: { padding: 0 },
  paperAnchorBottom: {
    borderRadius: 0,
    height: "100vh",
  },
  divider: {
    height: 6,
    backgroundColor: theme.palette.gray.grayLight22,
  },
  shortDivider: {
    height: 5,
    width: 42,
    borderRadius: 100,
    color: theme.palette.gray.grayLight23,
  },
  wrapperContent: {
    display: "flex",
    justifyContent: "space-between",
    padding: "12px 0",
  },
  boxTotalPrice: {
    padding: "12px 16px",
    display: "flex",
    flexDirection: "column",
  },
  totalPrice: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  boxDiscount: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "12px 16px",
  },
  boxDiscountLeft: {
    display: "flex",
    alignItems: "center",
  },
  discountTxt: {
    marginRight: 8,
  },
  discountCode: {
    height: 24,
    borderRadius: 4,
    border: `1px solid ${theme.palette.gray.grayLight24}`,
    alignItems: "center",
    display: "flex",
    padding: 4,
  },
  discountPrice: {
    color: theme.palette.green.greenLight7,
  },
  boxSubFee: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "12px 16px",
  },
}));

const PriceDetailsTwoWayModal = ({
  open = false,
  toggleDrawer = () => {},
  item = {},
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const isTwoWay = !isEmpty(item?.inbound);
  if (!item) return <div />;
  return (
    <Drawer
      anchor="bottom"
      open={open}
      onClose={toggleDrawer(false)}
      classes={{
        paperAnchorBottom: classes.paperAnchorBottom,
      }}
    >
      <Grid container className={clsx(classes.wrapHeader, classes.slBox)}>
        <Grid item style={{ position: "absolute" }}>
          <IconButton
            onClick={toggleDrawer(false)}
            className={classes.btnClose}
          >
            <IconClose />
          </IconButton>
        </Grid>
        <Grid item style={{ margin: "auto" }}>
          <Typography variant="subtitle1">
            {listString.IDS_MT_TEXT_PRICE_DETAIL}
          </Typography>
        </Grid>
      </Grid>
      <Box className={classes.container}>
        <Box className={classes.boxPriceDetail}>
          <Box pt={2}>
            <Typography variant="subtitle1">
              {isTwoWay
                ? listString.IDS_MT_TEXT_PRICE_DETAILS_OUTBOUND
                : listString.IDS_MT_TEXT_PRICE_DETAIL}
            </Typography>
          </Box>
          {item?.outbound?.numAdult > 0 ? (
            <Box className={clsx(classes.wrapperContent, classes.slBox)}>
              <Typography variant="caption">
                {`${listString.IDS_MT_TEXT_ADULT} x${item.outbound.numAdult}`}
              </Typography>
              <Typography variant="caption">
                {item.outbound.adultMoney.formatMoney()}đ
              </Typography>
            </Box>
          ) : null}
          {item?.outbound?.numChildren ? (
            <Box className={clsx(classes.wrapperContent, classes.slBox)}>
              <Typography variant="caption">
                {`${listString.IDS_MT_TEXT_CHILDREN} x${item.outbound.numChildren}`}
              </Typography>
              <Typography variant="caption">
                {item.outbound.childMoney.formatMoney()}đ
              </Typography>
            </Box>
          ) : null}
          {item?.outbound?.numInfants ? (
            <Box className={clsx(classes.wrapperContent, classes.slBox)}>
              <Typography variant="caption">
                {`${listString.IDS_MT_TEXT_BABY} x${item.outbound.numInfants}`}
              </Typography>
              <Typography variant="caption">
                {listString.IDS_MT_TEXT_FILTER_FREE}
              </Typography>
            </Box>
          ) : null}
          {item?.outbound?.taxFee ? (
            <Box className={clsx(classes.wrapperContent, classes.slBox)}>
              <Typography variant="caption">
                {listString.IDS_MT_TEXT_TAX_FEE}
              </Typography>
              <Typography variant="caption">{`${item?.outbound?.taxFee.formatMoney()}đ`}</Typography>
            </Box>
          ) : null}
          {item?.outbound?.baggageMoney > 0 && (
            <Box className={clsx(classes.wrapperContent, classes.slBox)}>
              <Typography variant="caption">
                {listString.IDS_MT_TEXT_PRICE_ADD_BAGGAGE}
              </Typography>
              <Typography variant="caption">
                {item?.outbound?.baggageMoney.formatMoney()}đ
              </Typography>
            </Box>
          )}
          <Box className={clsx(classes.wrapperContent, classes.slBox)}>
            <Typography variant="subtitle2">
              {listString.IDS_MT_TEXT_TOTAL_PRICE_OUTBOUND}
            </Typography>
            <Typography variant="subtitle2">
              {item?.outbound?.total.formatMoney()}đ
            </Typography>
          </Box>
          {isTwoWay && (
            <>
              <Box pt={2}>
                <Typography variant="subtitle1">
                  {listString.IDS_MT_TEXT_PRICE_DETAILS_INBOUND}
                </Typography>
              </Box>
              {item?.inbound?.numAdult > 0 ? (
                <Box className={clsx(classes.wrapperContent, classes.slBox)}>
                  <Typography variant="caption">
                    {`${listString.IDS_MT_TEXT_ADULT} x${item.inbound.numAdult}`}
                  </Typography>
                  <Typography variant="caption">
                    {item.inbound.adultMoney.formatMoney()}đ
                  </Typography>
                </Box>
              ) : null}
              {item?.inbound?.numChildren ? (
                <Box className={clsx(classes.wrapperContent, classes.slBox)}>
                  <Typography variant="caption">
                    {`${listString.IDS_MT_TEXT_CHILDREN} x${item.inbound.numChildren}`}
                  </Typography>
                  <Typography variant="caption">
                    {item.inbound.childMoney.formatMoney()}đ
                  </Typography>
                </Box>
              ) : null}
              {item?.inbound?.numInfants ? (
                <Box className={clsx(classes.wrapperContent, classes.slBox)}>
                  <Typography variant="caption">
                    {`${listString.IDS_MT_TEXT_BABY} x${item.inbound.numInfants}`}
                  </Typography>
                  <Typography variant="caption">
                    {listString.IDS_MT_TEXT_FILTER_FREE}
                  </Typography>
                </Box>
              ) : null}
              {item?.inbound?.taxFee ? (
                <Box className={clsx(classes.wrapperContent, classes.slBox)}>
                  <Typography variant="caption">
                    {listString.IDS_MT_TEXT_TAX_FEE}
                  </Typography>
                  <Typography variant="caption">{`${item?.inbound?.taxFee.formatMoney()}đ`}</Typography>
                </Box>
              ) : null}
              {item?.inbound?.baggageMoney > 0 && (
                <Box className={clsx(classes.wrapperContent, classes.slBox)}>
                  <Typography variant="caption">
                    {listString.IDS_MT_TEXT_PRICE_ADD_BAGGAGE}
                  </Typography>
                  <Typography variant="caption">
                    {item?.inbound?.baggageMoney.formatMoney()}đ
                  </Typography>
                </Box>
              )}
              <Box className={clsx(classes.wrapperContent, classes.slBox)}>
                <Typography variant="subtitle2">
                  {listString.IDS_MT_TEXT_TOTAL_PRICE_INBOUND}
                </Typography>
                <Typography variant="subtitle2">
                  {item?.inbound?.total.formatMoney()}đ
                </Typography>
              </Box>
            </>
          )}
        </Box>
        <Divider className={classes.divider} />
        {item?.insurance ? (
          <Box className={clsx(classes.boxSubFee, classes.slBox)}>
            <Typography variant="caption">
              {listString.IDS_TEXT_INSURANCE_TITLE}
            </Typography>
            <Typography variant="caption">{`${item.insurance.formatMoney()}đ`}</Typography>
          </Box>
        ) : null}
        {item?.voucherMoney ? (
          <Box className={clsx(classes.boxDiscount, classes.slBox)}>
            <Box className={classes.boxDiscountLeft}>
              <Typography variant="caption" className={classes.discountTxt}>
                {listString.IDS_MT_TEXT_DISCOUNT_CODE}
              </Typography>
              <Box className={classes.discountCode}>
                <Typography variant="caption">{item.voucherCode}</Typography>
              </Box>
            </Box>
            <Typography variant="caption" className={classes.discountPrice}>
              {`-${item.voucherMoney.formatMoney()}đ`}
            </Typography>
          </Box>
        ) : null}
        {item?.surchargeMoney > 0 ? (
          <Box className={clsx(classes.boxSubFee, classes.slBox)}>
            <Typography variant="caption">
              {listString.IDS_MT_TEXT_SUB_FEE}
            </Typography>
            <Typography variant="caption">{`${item.surchargeMoney.formatMoney()}đ`}</Typography>
          </Box>
        ) : null}
        {(item?.insurance > 0 ||
          item?.surchargeMoney > 0 ||
          item?.voucherMoney > 0) && <Divider className={classes.divider} />}
        {item?.totalPayment && (
          <Box className={classes.boxTotalPrice}>
            <Box className={classes.totalPrice}>
              <Typography variant="subtitle2">
                {listString.IDS_TEXT_ORDER_PAYMENT_TOTAL_PRICE}
              </Typography>
              <Typography variant="subtitle1">{`${item?.totalPayment.formatMoney()}đ`}</Typography>
            </Box>
            <Typography variant="caption">
              {listString.IDS_MT_TEXT_INCLUDE_TAX_FEE_VAT}
            </Typography>
          </Box>
        )}
      </Box>
    </Drawer>
  );
};

export default PriceDetailsTwoWayModal;
