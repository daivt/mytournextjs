import {
  Box,
  ButtonBase,
  Divider,
  Drawer,
  makeStyles,
} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { IconUser2 } from "@public/icons";
import clsx from "clsx";
import React from "react";

const useStyles = makeStyles((theme) => ({
  mainDrawerFilter: {
    width: "100vw",
    fontSize: 16,
    overflow: "auto",
  },
  wrapHeader: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 16,
  },
  container: {
    padding: "16px 16px 0px",
  },
  slBox: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  paperAnchorBottom: {
    borderRadius: "16px 16px 0 0",
  },
  divider: {
    height: 1,
    margin: "12px 0px",
    color: theme.palette.gray.grayLight22,
  },
  IconUser2: {
    width: 12,
    height: 12,
    marginRight: 12,
  },
}));

const DirectoryModal = ({
  directory = [],
  open = false,
  toggleDrawer = () => {},
  pickContact = () => {},
}) => {
  const classes = useStyles();
  return (
    <Drawer
      anchor="bottom"
      open={open}
      onClose={toggleDrawer(false)}
      classes={{
        paperAnchorBottom: classes.paperAnchorBottom,
      }}
    >
      <Box className={classes.mainDrawerFilter}>
        <Box className={clsx(classes.wrapHeader, classes.slBox)}>
          <Typography component="span" variant="subtitle1">
            Hành khách
          </Typography>
        </Box>
        <Box className={classes.wrapper}>
          <Box className={classes.container}>
            {directory.map((item, index) => (
              <Box key={index}>
                <Box>
                  <ButtonBase
                    style={{
                      width: "100%",
                      height: "100%",
                      display: "flex",
                      justifyContent: "flex-start",
                    }}
                    onClick={() => pickContact(item)}
                  >
                    <IconUser2 className={classes.IconUser2} />
                    <Typography
                      component="span"
                      style={{
                        whiteSpace: "nowrap",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        maxWidth: 316,
                      }}
                    >
                      {item?.name}
                    </Typography>
                  </ButtonBase>
                </Box>
                <Divider className={classes.divider} />
              </Box>
            ))}
          </Box>
        </Box>
      </Box>
    </Drawer>
  );
};

export default DirectoryModal;
