import BirthDayField from "@components/common/BirthDayField/BirthDayField";
import {
  Box,
  Divider,
  Drawer,
  Grid,
  IconButton,
  makeStyles,
} from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { IconClose } from "@public/icons";
import ButtonComponent from "@src/button/Button";
import FieldTextAutocomplete from "@src/form/FieldTextAutocomplete";
import FieldTextContent from "@src/form/FieldTextContent";
import { defaultCountry, listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import { DATE_FORMAT_BACK_END } from "@utils/moment";
import { useFormikContext } from "formik";
import find from "lodash/find";
import moment from "moment";
import { useEffect, useState } from "react";
const useStyles = makeStyles((theme) => ({
  mainDrawerFilter: {
    width: "100vw",
    overflow: "auto",
  },
  wrapHeader: {
    alignItems: "center",
    height: 48,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  container: {
    padding: "12px 16px 32px",
    display: "flex",
    flexDirection: "column",
  },
  paperAnchorBottom: {
    borderRadius: "16px 16px 0 0",
  },
  styleLabel: {
    fontSize: 12,
    color: theme.palette.gray.grayDark8,
  },
  divider: {
    height: 1,
    color: theme.palette.gray.grayLight22,
  },
  buttonAdd: {
    padding: "8px 16px",
  },
}));

const AddPassportModal = ({
  open = false,
  toggleDrawer = () => {},
  index = 0,
  handleSubmitPassport = () => {},
  countries = {},
}) => {
  const { setFieldValue, values } = useFormikContext();

  const [passportCountry, setPassportCountry] = useState({});
  const [passportResidence, setPassportResidence] = useState({});
  useEffect(() => {
    let country = find(
      countries,
      (c) =>
        c.code === (values[`passport_country_${index}`] || defaultCountry.id)
    );
    if (isEmpty(country)) {
      country = defaultCountry;
    }

    let residence = find(
      countries,
      (c) =>
        c.id === (values[`passport_residence_${index}`] || defaultCountry.id)
    );
    if (isEmpty(residence)) {
      residence = defaultCountry;
    }

    setFieldValue(`passport_country_${index}`, country.id);
    setFieldValue(`passport_residence_${index}`, residence.id);
    setPassportCountry(country);
    setPassportResidence(residence);
  }, [index]);
  const classes = useStyles();
  const theme = useTheme();

  const getStyleInput = {
    marginTop: 0,
    fontSize: 16,
    color: theme.palette.black.black3,
    background: theme.palette.white.main,
    borderTop: "none",
    borderRight: "none",
    borderLeft: "none",
    borderRadius: 0,
  };

  return (
    <Drawer
      anchor="bottom"
      open={open}
      onClose={toggleDrawer(false)}
      classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
    >
      <Box className={classes.mainDrawerFilter}>
        <Grid container className={classes.wrapHeader}>
          <Grid item style={{ position: "absolute" }}>
            <IconButton onClick={toggleDrawer(false)}>
              <IconClose />
            </IconButton>
          </Grid>
          <Grid item style={{ margin: "auto" }}>
            <Typography component="span" variant="subtitle1">
              {listString.IDS_TEXT_ADD_PASSPORT_MODAL_TITLE}
            </Typography>
          </Grid>
        </Grid>
        <Box className={classes.wrapper}>
          <Box className={classes.container}>
            <Autocomplete
              options={countries}
              classes={{
                option: classes.option,
              }}
              value={passportResidence}
              onChange={(event, newValue) => {
                setFieldValue(`passport_residence_${index}`, newValue.id);
                setPassportResidence(
                  find(countries, (c) => c.id === newValue.id)
                );
              }}
              autoHighlight
              getOptionSelected={(option) =>
                option.id === passportResidence?.id || defaultCountry.id
              }
              getOptionLabel={(option) => option.name}
              renderOption={(option) => (
                <>
                  <Box
                    display="flex"
                    width="100%"
                    justifyContent="space-between"
                    borderBottom="1px solid #ced4da"
                    pb={1}
                  >
                    <Typography variant="body1">{`${option.name} (${option.code})`}</Typography>
                    <Typography
                      variant="body1"
                      style={{ color: theme.palette.blue.blueLight8 }}
                    >{`+${option.phonecode}`}</Typography>
                  </Box>
                </>
              )}
              renderInput={(params) => (
                <FieldTextAutocomplete
                  fullWidth
                  name={`passport_residence_${index}`}
                  {...params}
                  label={listString.IDS_TEXT_PASSPORT_RESIDENCE}
                  inputProps={{
                    ...params.inputProps,
                    autoComplete: "off",
                  }}
                />
              )}
            />
            <FieldTextContent
              name={`passport_${index}`}
              placeholder={listString.IDS_TEXT_PASSPORT_NUMBER}
              inputStyle={getStyleInput}
              inputProps={{
                autoComplete: "off",
              }}
            />
            <Autocomplete
              options={countries}
              classes={{
                option: classes.option,
              }}
              value={passportCountry}
              onChange={(event, newValue) => {
                setFieldValue(`passport_country_${index}`, newValue.id);
                setPassportCountry(
                  find(countries, (c) => c.id === newValue.id)
                );
              }}
              autoHighlight
              getOptionSelected={(option) =>
                option.id === passportCountry?.id || defaultCountry.id
              }
              getOptionLabel={(option) => option.name}
              renderOption={(option) => (
                <>
                  <Box
                    display="flex"
                    width="100%"
                    justifyContent="space-between"
                    borderBottom="1px solid #ced4da"
                    pb={1}
                  >
                    <Typography variant="body1">{`${option.name} (${option.code})`}</Typography>
                    <Typography
                      variant="body1"
                      style={{ color: theme.palette.blue.blueLight8 }}
                    >{`+${option.phonecode}`}</Typography>
                  </Box>
                </>
              )}
              renderInput={(params) => (
                <FieldTextAutocomplete
                  fullWidth
                  name={`passport_country_${index}`}
                  {...params}
                  label={listString.IDS_TEXT_PASSPORT_COUNTRY}
                  inputProps={{
                    ...params.inputProps,
                    autoComplete: "off",
                  }}
                />
              )}
            />
            <BirthDayField
              name={`passport_expired_${index}`}
              date={
                values[`passport_expired_${index}`] &&
                moment(
                  values[`passport_expired_${index}`],
                  DATE_FORMAT_BACK_END
                )
              }
              update={(value) => {
                setFieldValue(
                  `passport_expired_${index}`,
                  moment(value).format(DATE_FORMAT_BACK_END)
                );
              }}
              placeholder={listString.IDS_TEXT_PASSPORT_EXPIRED}
              inputStyle={getStyleInput}
              inputProps={{
                autoComplete: "off",
              }}
            />
          </Box>
          <Divider className={classes.divider} />
          <Box className={classes.buttonAdd}>
            <ButtonComponent
              backgroundColor={theme.palette.secondary.main}
              height={48}
              borderRadius={8}
              handleClick={handleSubmitPassport(values, index)}
            >
              <Typography variant="subtitle1">
                {listString.IDS_TEXT_BUTTON_ADD}
              </Typography>
            </ButtonComponent>
          </Box>
        </Box>
      </Box>
    </Drawer>
  );
};

export default AddPassportModal;
