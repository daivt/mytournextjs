import {
  Box,
  Divider,
  Drawer,
  IconButton,
  makeStyles,
} from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { listString } from "@utils/constants";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  boxPriceDetail: {
    padding: "8px 16px 0",
  },
  wrapHeader: {
    display: "flex",
    alignItems: "center",
    marginTop: 12,
  },
  slBox: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  btnClose: { padding: 0 },
  paperAnchorBottom: {
    borderRadius: "16px 16px 0 0",
  },
  divider: {
    height: 6,
    color: theme.palette.gray.grayLight22,
  },
  shortDivider: {
    height: 5,
    width: 42,
    borderRadius: 100,
    color: theme.palette.gray.grayLight23,
  },
  wrapperContent: {
    display: "flex",
    justifyContent: "space-between",
    padding: "12px 0",
  },
  boxTotalPrice: {
    padding: "12px 16px",
    display: "flex",
    flexDirection: "column",
  },
  totalPrice: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  boxDiscount: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "12px 16px",
  },
  boxDiscountLeft: {
    display: "flex",
    alignItems: "center",
  },
  discountTxt: {
    marginRight: 8,
  },
  discountCode: {
    height: 24,
    borderRadius: 4,
    border: `1px solid ${theme.palette.gray.grayLight24}`,
    alignItems: "center",
    display: "flex",
    padding: 4,
  },
  discountPrice: {
    color: theme.palette.green.greenLight7,
  },
  boxSubFee: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "12px 16px",
  },
}));

const PriceDetailsOneWayModal = ({
  open = false,
  toggleDrawer = () => {},
  item = {},
}) => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Drawer
      anchor="bottom"
      open={open}
      onClose={toggleDrawer(false)}
      classes={{
        paperAnchorBottom: classes.paperAnchorBottom,
      }}
    >
      <Box className={classes.boxPriceDetail}>
        <Box display="flex" justifyContent="center">
          <IconButton
            onClick={toggleDrawer(false)}
            className={classes.btnClose}
          >
            <Divider className={classes.shortDivider} />
          </IconButton>
        </Box>
        <Box className={classes.wrapHeader}>
          <Typography variant="subtitle1">
            {listString.IDS_MT_TEXT_PRICE_DETAIL}
          </Typography>
        </Box>
        {item.numAdults > 0 ? (
          <Box className={clsx(classes.wrapperContent, classes.slBox)}>
            <Typography variant="caption">
              {`${listString.IDS_MT_TEXT_ADULT} x${item.numAdults}`}
            </Typography>
            <Typography variant="caption">1.150.000đ</Typography>
          </Box>
        ) : null}
        {item.numChildren ? (
          <Box className={clsx(classes.wrapperContent, classes.slBox)}>
            <Typography variant="caption">
              {`${listString.IDS_MT_TEXT_CHILDREN} x${item.numChildren}`}
            </Typography>
            <Typography variant="caption">750.000đ</Typography>
          </Box>
        ) : null}
        {item.numInfants ? (
          <Box className={clsx(classes.wrapperContent, classes.slBox)}>
            <Typography variant="caption">
              {`${listString.IDS_MT_TEXT_BABY} x${item.numInfants}`}
            </Typography>
            <Typography variant="caption">
              {listString.IDS_MT_TEXT_FILTER_FREE}
            </Typography>
          </Box>
        ) : null}
        {item?.processingFeeTax ? (
          <Box className={clsx(classes.wrapperContent, classes.slBox)}>
            <Typography variant="caption">
              {listString.IDS_MT_TEXT_TAX_FEE}
            </Typography>
            <Typography variant="caption">{`${item?.processingFeeTax.formatMoney()}đ`}</Typography>
          </Box>
        ) : null}
        <Box className={clsx(classes.wrapperContent, classes.slBox)}>
          <Typography variant="caption">
            {listString.IDS_MT_TEXT_PRICE_ADD_BAGGAGE}
          </Typography>
          <Typography variant="caption">150.000đ</Typography>
        </Box>
        <Box className={clsx(classes.wrapperContent, classes.slBox)}>
          <Typography variant="subtitle2">
            {listString.IDS_MT_TEXT_TOTAL_PRICE_OUTBOUND}
          </Typography>
          <Typography variant="subtitle2">2.850.000đ</Typography>
        </Box>
      </Box>
      <Divider className={classes.divider} />
      {item?.insuranceAmount ? (
        <Box className={clsx(classes.boxSubFee, classes.slBox)}>
          <Typography variant="caption">
            {listString.IDS_TEXT_INSURANCE_TITLE}
          </Typography>
          <Typography variant="caption">{`${item.insuranceAmount.formatMoney()}đ`}</Typography>
        </Box>
      ) : null}
      {item?.promotionCode ? (
        <Box className={clsx(classes.boxDiscount, classes.slBox)}>
          <Box className={classes.boxDiscountLeft}>
            <Typography variant="caption" className={classes.discountTxt}>
              {listString.IDS_MT_TEXT_DISCOUNT_CODE}
            </Typography>
            <Box className={classes.discountCode}>
              <Typography variant="caption">{item.promotionCode}</Typography>
            </Box>
          </Box>
          <Typography variant="caption" className={classes.discountPrice}>
            {`${item.discount.formatMoney()}đ`}
          </Typography>
        </Box>
      ) : null}
      {item?.paymentMethodFee ? (
        <Box className={clsx(classes.boxSubFee, classes.slBox)}>
          <Typography variant="caption">
            {listString.IDS_MT_TEXT_SUB_FEE}
          </Typography>
          <Typography variant="caption">{`${item.paymentMethodFee.formatMoney()}đ`}</Typography>
        </Box>
      ) : null}
      <Divider className={classes.divider} />
      <Box className={classes.boxTotalPrice}>
        <Box className={classes.totalPrice}>
          <Typography variant="subtitle2">
            {listString.IDS_TEXT_ORDER_PAYMENT_TOTAL_PRICE}
          </Typography>
          <Typography variant="subtitle1">{`${item.finalPrice.formatMoney()}đ`}</Typography>
        </Box>
        <Typography variant="caption">
          {listString.IDS_MT_TEXT_INCLUDE_TAX_FEE_VAT}
        </Typography>
      </Box>
    </Drawer>
  );
};

export default PriceDetailsOneWayModal;
