import { useState } from "react";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import { Box, Drawer, IconButton } from "@material-ui/core";

import { hotelAutoComplete } from "@api/homes";
import { listString } from "@utils/constants";
import { IconClose, IconSearch, IconCloseInBackGround } from "@public/icons";

import FormControlTextField from "@src/form/FormControlTextField";
import ResultSearch from "@components/common/modal/hotel/componentDestinationSearchModal/ResultSearch";
import HistoryAndDestination from "@components/common/modal/hotel/componentDestinationSearchModal/HistoryAndDestination";
import { isEmpty } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  mainDrawerFilter: {
    width: "100vw",
    color: theme.palette.black.black3,
    fontSize: 14,
    height: "100vh",
    overflow: "auto",
    fontWeight: "normal",
    "&::-webkit-scrollbar": {
      display: "none",
    },
  },
  paperAnchorLeft: {
    borderRadius: 0,
  },
  iconSearch: {
    width: 23,
    height: 20,
  },
  headerSearch: {
    position: "relative",
    height: 48,
  },
  contentHeaderSearch: {
    position: "fixed",
    backgroundColor: theme.palette.white.main,
    top: 0,
    left: 0,
    zIndex: 20000,
    width: "100%",
    padding: "0 16px",
  },
}));

let timeoutSearch = null;

const DestinationSearchModal = ({
  open = false,
  toggleDrawer = () => {},
  handleSelectedItem = () => {},
  isLocation = false,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const [inputSearch, setInputSearch] = useState("");
  const [listLocation, setListLocation] = useState([]);
  const [listHotel, setListHotel] = useState([]);
  const handleChangeInput = (event) => {
    const value = event.target.value;
    setInputSearch(value);
    const text = value.trim();
    clearTimeout(timeoutSearch);
    timeoutSearch = setTimeout(() => {
      const params = {
        term: text || undefined,
        size: 20,
      };
      handleFetchData(params);
    }, 300);
  };

  const handleFetchData = async (params = {}) => {
    const { data } = await hotelAutoComplete(params);
    filterItemSearch(data.data.items);
  };
  const handleSelectedLocation = (item) => {
    handleSelectedItem(item);
  };
  const filterItemSearch = (data = []) => {
    let hotels = [];
    let locations = [];
    data.forEach((el) => {
      if (el.type === "HOTEL") {
        hotels.push(el);
      } else {
        locations.push(el);
      }
    });
    setListHotel(hotels);
    setListLocation(locations);
  };

  return (
    <Drawer
      anchor="left"
      open={open}
      onClose={toggleDrawer("")}
      classes={{ paperAnchorLeft: classes.paperAnchorLeft }}
    >
      <Box className={classes.mainDrawerFilter}>
        <Box>
          <Box className={classes.headerSearch}>
            <Box
              display="flex"
              alignItems="center"
              className={classes.contentHeaderSearch}
            >
              <IconButton
                edge="start"
                color="inherit"
                onClick={toggleDrawer("")}
                aria-label="close"
              >
                <IconClose style={{ fontSize: 24 }} />
              </IconButton>
              <FormControlTextField
                value={inputSearch}
                hideHelperText
                formControlStyle={{ marginRight: 0 }}
                inputStyle={{
                  paddingLeft: 12,
                  backgroundColor: theme.palette.gray.grayLight22,
                  borderRadius: 8,
                }}
                placeholder={listString.IDS_MT_PLACE_HOLDER_INPUT_SEARCH_MOBILE}
                startAdornment={<IconSearch className={classes.iconSearch} />}
                endAdornment={
                  isEmpty(inputSearch) ? null : (
                    <IconButton onClick={() => setInputSearch("")}>
                      <IconCloseInBackGround />
                    </IconButton>
                  )
                }
                onChange={handleChangeInput}
              />
            </Box>
          </Box>
          <Box px={2} py={2}>
            {isEmpty(inputSearch) ? (
              <HistoryAndDestination
                handleSelectedLocation={handleSelectedLocation}
              />
            ) : (
              <ResultSearch
                listLocation={listLocation}
                listHotel={listHotel}
                inputSearch={inputSearch}
                handleSelectedItem={handleSelectedItem}
              />
            )}
          </Box>
        </Box>
      </Box>
    </Drawer>
  );
};

export default DestinationSearchModal;
// component search box use home fight page and listing fight page
