import { useState, useEffect } from "react";
import { Form, Formik } from "formik";
import { Remove, Add } from "@material-ui/icons";
import {
  Box,
  Grid,
  IconButton,
  makeStyles,
  Drawer,
  Typography,
} from "@material-ui/core";

import { IconClose, IconArrowDownSmall } from "@public/icons";
import { listString } from "@utils/constants";

import ButtonComponent from "@src/button/Button";
import SingleSelect from "@src/form/SingleSelect";

const useStyles = makeStyles((theme) => ({
  mainDrawerFilter: {
    width: "100vw",
    fontSize: 16,
    overflow: "auto",
    "&::-webkit-scrollbar": {
      display: "none",
    },
  },
  wrapHeader: {
    position: "relative",
    display: "flex",
    alignItems: "center",
    marginTop: 12,
    height: 48,
  },
  container: {
    padding: "14px 16px 8px 16px",
    display: "flex",
    flexDirection: "column",
    borderRadius: "16px 16px 0px 0px",
  },
  slBox: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  leftBox: {
    padding: "30px 0",
  },
  rightBox: {
    padding: "20px 0",
    position: "relative",
  },
  btn: {
    backgroundColor: theme.palette.gray.grayLight22,
    padding: 20,
    width: 40,
    height: 40,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    "&:hover": {
      backgroundColor: theme.palette.gray.grayLight22,
      padding: 20,
      width: 40,
      height: 40,
    },
  },
  numAdult: {
    paddingTop: 10,
  },
  btnCancel: {
    backgroundColor: theme.palette.gray.grayLight22,
    color: theme.palette.black.black3,
    marginRight: 12,
  },
  btnSubmit: {
    backgroundColor: theme.palette.pink.main,
    color: theme.palette.white.main,
  },
  btnBottom: {
    paddingTop: 8,
    display: "flex",
    flexDirection: "row",
  },
  disabledButtonIcon: {
    backgroundColor: theme.palette.gray.grayLight23,
    color: theme.palette.gray.grayLight24,
    "&:disabled": {
      backgroundColor: theme.palette.gray.grayLight23,
      color: theme.palette.gray.grayLight24,
    },
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
  },
  wrapDialog: {
    width: "100%",
    height: "100%",
    margin: 0,
    maxWwidth: "100%",
    maxHeight: "50%",
    borderRadius: 0,
    position: "fixed",
    bottom: 0,
  },
  btnClose: {
    position: "absolute",
    top: 0,
    left: 0,
  },
  paperAnchorBottom: {
    height: "100vh",
  },
  itemBtn: {
    textAlign: "center",
  },
  wrapBtnChoose: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
  },
}));

const RenderButton = (props) => {
  const classes = useStyles();
  return (
    <Box className={classes.rightBox}>
      {props.type ? (
        <IconButton
          className={classes.btn}
          onClick={() => props.onChange(props.input, props.value + 1)}
          disabled={
            ((props.input === "roomSearch" || props.input === "childSearch") &&
              props.value === 9) ||
            (props.input === "adultSearch" && props.value === 36)
          }
          classes={{ disabled: classes.disabledButtonIcon }}
        >
          <Box className={classes.wrapBtnChoose}>
            {" "}
            <Add />
          </Box>
        </IconButton>
      ) : (
        <IconButton
          className={classes.btn}
          onClick={() => props.onChange(props.input, props.value - 1)}
          disabled={
            ((props.input === "roomSearch" || props.input === "adultSearch") &&
              props.value === 1) ||
            (props.input === "childSearch" && props.value === 0)
          }
          classes={{ disabled: classes.disabledButtonIcon }}
        >
          <Box className={classes.wrapBtnChoose}>
            <Remove />
          </Box>
        </IconButton>
      )}
    </Box>
  );
};
const listAgeChildren = [0, 1, 2, 3, 4, 5, 6, 7, 8];
const ageData = [
  { id: 1, label: "1" },
  { id: 2, label: "2" },
  { id: 3, label: "3" },
  { id: 4, label: "4" },
  { id: 5, label: "5" },
  { id: 6, label: "6" },
  { id: 7, label: "7" },
  { id: 8, label: "8" },
  { id: 9, label: "9" },
  { id: 10, label: "10" },
  { id: 11, label: "11" },
  { id: 12, label: "12" },
  { id: 13, label: "13" },
  { id: 14, label: "14" },
  { id: 15, label: "15" },
  { id: 16, label: "16" },
  { id: 17, label: "17" },
];
const GuestInfoBoxModal = ({
  open = false,
  toggleDrawer = () => {},
  handleOk = () => {},
  paramsSearch = {},
}) => {
  const classes = useStyles();
  const [listAge, setListAge] = useState([10, 10, 10, 10, 10, 10, 10, 10, 10]);

  useEffect(() => {
    let childrenAgeTemp = [...listAge];
    paramsSearch.childrenAges.forEach((el, index) => {
      childrenAgeTemp[index] = el;
    });
    setListAge(childrenAgeTemp);
  }, [paramsSearch]);

  const handleOkValues = (values) => {
    let childrenAges = [...listAge];
    let listAgeTemp = [...listAge];
    listAge.forEach((el, index) => {
      if (el === 0 && index < values.childSearch) {
        childrenAges[index] = 10;
        listAgeTemp[index] = 10;
      }
    });
    childrenAges.splice(values.childSearch, 10 - values.childSearch);
    setListAge(listAgeTemp);
    handleOk({ ...values, childrenAges });
  };

  return (
    <Drawer
      anchor="bottom"
      open={open}
      onClose={toggleDrawer("")}
      classes={{ paperAnchorBottom: classes.paperAnchorBottom }}
    >
      <Box className={classes.mainDrawerFilter}>
        <Box className={classes.wrapHeader}>
          <IconButton onClick={toggleDrawer("")} className={classes.btnClose}>
            <IconClose />
          </IconButton>
          <Box width="100%" textAlign="center">
            <Typography component="span" variant="subtitle1">
              Số khách mỗi phòng
            </Typography>
          </Box>
        </Box>
        <Box className={classes.wrapper}>
          <Formik
            initialValues={{
              roomSearch: paramsSearch.rooms,
              adultSearch: paramsSearch.adults,
              childSearch: paramsSearch.children,
            }}
          >
            {({ values, isSubmitting, resetForm, setFieldValue }) => {
              const _handleChangeValue = (value, field) => () => {
                var maxLimit = 10;
                // if (
                //   field === "roomSearch" &&
                //   value === "asc" &&
                //   values.adultSearch === values.roomSearch
                // ) {
                //   setFieldValue("adultSearch", values.adultSearch + 1);
                // } else if (
                //   field === "adultSearch" &&
                //   value === "desc" &&
                //   values.adultSearch === values.roomSearch
                // ) {
                //   setFieldValue("roomSearch", values.roomSearch - 1);
                // }
                // Case All
                if (value === "asc") {
                  setFieldValue([field], values[field] + 1);
                } else {
                  setFieldValue([field], values[field] - 1);
                }
              };

              return (
                <Form style={{ width: "100%" }}>
                  <Box className={classes.container}>
                    <Grid container className={classes.slBox}>
                      <Grid item lg={7} md={7} sm={7} xs={7}>
                        <Box className={classes.leftBox}>
                          <Typography variant="body1">
                            {listString.IDS_MT_TEXT_ROOM}
                          </Typography>
                        </Box>
                      </Grid>
                      <Grid item lg={2} md={2} sm={2} xs={2}>
                        <RenderButton
                          input={"roomSearch"}
                          type={0}
                          value={values.roomSearch}
                          onChange={_handleChangeValue("desc", "roomSearch")}
                        />
                      </Grid>
                      <Grid item lg={1} md={1} sm={1} xs={1}>
                        <Box className={classes.rightBox}>
                          <Typography
                            className={classes.numAdult}
                            variant="subtitle1"
                          >
                            {values.roomSearch}
                          </Typography>
                        </Box>
                      </Grid>
                      <Grid item lg={2} md={2} sm={2} xs={2}>
                        <RenderButton
                          input={"roomSearch"}
                          type={1}
                          value={values.roomSearch}
                          onChange={_handleChangeValue("asc", "roomSearch")}
                        />
                      </Grid>
                    </Grid>

                    <Grid container className={classes.slBox}>
                      <Grid item lg={7} md={7} sm={7} xs={7}>
                        <Box className={classes.leftBox}>
                          <Typography variant="body1">
                            {listString.IDS_MT_TEXT_ADULT}
                          </Typography>
                        </Box>
                      </Grid>
                      <Grid item lg={2} md={2} sm={2} xs={2}>
                        <RenderButton
                          input={"adultSearch"}
                          type={0}
                          value={values.adultSearch}
                          onChange={_handleChangeValue("desc", "adultSearch")}
                        />
                      </Grid>
                      <Grid item lg={1} md={1} sm={1} xs={1}>
                        <Box className={classes.rightBox}>
                          <Typography
                            className={classes.numAdult}
                            variant="subtitle1"
                          >
                            {values.adultSearch}
                          </Typography>
                        </Box>
                      </Grid>
                      <Grid item lg={2} md={2} sm={2} xs={2}>
                        <RenderButton
                          input={"adultSearch"}
                          type={1}
                          value={values.adultSearch}
                          onChange={_handleChangeValue("asc", "adultSearch")}
                        />
                      </Grid>
                    </Grid>

                    <Grid container>
                      <Grid item lg={7} md={7} sm={7} xs={7}>
                        <Box className={classes.leftBox}>
                          <Typography variant="body1">
                            {listString.IDS_MT_TEXT_CHILDREN}
                          </Typography>
                        </Box>
                      </Grid>
                      <Grid item lg={2} md={2} sm={2} xs={2}>
                        <RenderButton
                          input={"childSearch"}
                          type={0}
                          value={values.childSearch}
                          onChange={_handleChangeValue("desc", "childSearch")}
                        />
                      </Grid>
                      <Grid item lg={1} md={1} sm={1} xs={1}>
                        <Box className={classes.rightBox}>
                          <Typography
                            variant="subtitle1"
                            className={classes.numAdult}
                          >
                            {values.childSearch}
                          </Typography>
                        </Box>
                      </Grid>
                      <Grid item lg={2} md={2} sm={2} xs={2}>
                        <RenderButton
                          input={"childSearch"}
                          type={1}
                          value={values.childSearch}
                          onChange={_handleChangeValue("asc", "childSearch")}
                        />
                      </Grid>
                    </Grid>
                    {values.childSearch > 0 && (
                      <Box display="flex" flexDirection="column" p="18px 0">
                        <Box component="span" fontWeight={600} pb={6 / 8}>
                          {listString.IDS_MT_INPUT_CHILDREN}
                        </Box>
                        {listAgeChildren.map((el) => {
                          if (el >= values.childSearch) return null;
                          return (
                            <Box
                              display="flex"
                              alignItems="center"
                              justifyContent="space-between"
                              p="6px 0"
                            >
                              <Box
                                component="span"
                                fontSize={16}
                                lineHeight="19px"
                              >
                                {`${listString.IDS_MT_TEXT_CHILDREN} ${el + 1}`}
                              </Box>
                              <SingleSelect
                                options={ageData}
                                placeholder={listString.IDS_MT_AGE}
                                getOptionLabel={(v) => `${v.label} tuổi`}
                                onSelectOption={(value) => {
                                  let listAgeTemp = [...listAge];
                                  listAgeTemp[el] = value;
                                  setListAge(listAgeTemp);
                                }}
                                value={listAge[el]}
                                formControlStyle={{ width: "fit-content" }}
                                hideHelperText
                                inputStyle={{ minHeight: 30, borderRadius: 8 }}
                                formControlStyle={{
                                  minWidth: 93,
                                  width: 93,
                                  marginRight: 0,
                                  maxHeight: 200,
                                }}
                                iconRight={<IconArrowDownSmall />}
                              />
                            </Box>
                          );
                        })}
                      </Box>
                    )}

                    <Box className={classes.btnBottom}>
                      <ButtonComponent
                        borderRadius={8}
                        height={48}
                        className={classes.btnCancel}
                        handleClick={toggleDrawer("")}
                      >
                        <Typography variant="subtitle1">
                          {listString.IDS_MT_TEXT_CANCEL_VN}
                        </Typography>
                      </ButtonComponent>

                      <ButtonComponent
                        borderRadius={8}
                        height={48}
                        className={classes.btnSubmit}
                        handleClick={() => handleOkValues(values)}
                      >
                        <Typography variant="subtitle1">
                          {listString.IDS_MT_TEXT_SELECT}
                        </Typography>
                      </ButtonComponent>
                    </Box>
                  </Box>
                </Form>
              );
            }}
          </Formik>
        </Box>
      </Box>
    </Drawer>
  );
};

export default GuestInfoBoxModal;
