import Modal from "@material-ui/core/Modal";
import { useTheme } from "@material-ui/core/styles";
import { Box, makeStyles, Backdrop, Fade, Typography } from "@material-ui/core";

import { listString } from "@utils/constants";

import ButtonComponent from "@src/button/Button";
import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  wrapIconGPS: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  paper: {
    outline: "none",
    backgroundColor: theme.palette.white.main,
    borderRadius: 16,
    height: 385,
    width: "100%",
    display: "flex",
    justifyContent: "center",
    margin: "0 16px",
  },
  wrapPaper: {
    margin: "0 24px",
  },
  descAllowGPS: {
    fontSize: 14,
    fontWeight: 400,
    marginBottom: "16px ",
    textAlign: "center",
    lineHeight: "22px",
  },
  buttonSearch: {
    height: 48,
    width: "100%",
    margin: 0,
    borderRadius: 8,
    color: theme.palette.white.main,
    fontSize: 16,
    backgroundColor: theme.palette.secondary.main,
    "&:hover": {
      backgroundColor: "#de0262",
    },
    [theme.breakpoints.down("sm")]: {
      position: "unset",
      height: 56,
    },
  },
  anotherTime: {
    fontWeight: 400,
  },
  allowGPS: {
    width: 179,
    height: 150,
  },
}));

const AllowGPSModal = ({
  open = false,
  toggleDrawer = () => {},
  handleAllowGps = () => {},
}) => {
  const theme = useTheme();
  const classes = useStyles();
  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={open}
      onClose={toggleDrawer(false)}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <Box className={classes.paper}>
          <Box className={classes.wrapPaper}>
            <Box className={classes.wrapIconGPS}>
              <Image
                srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_allow_gps.svg"
                className={classes.allowGPS}
              />
              <Box className={classes.descAllowGPS}>
                {listString.IDS_MT_TEXT_ALLOW_GPS}
              </Box>
              <ButtonComponent
                backgroundColor={theme.palette.secondary.main}
                height={48}
                fontSize={16}
                fontWeight={600}
                borderRadius={8}
                handleClick={handleAllowGps}
              >
                {listString.IDS_MT_TEXT_ALLOW_GPS_SUBMIT}
              </ButtonComponent>
              <Box marginTop={3}>
                <Typography
                  variant="body1"
                  className={classes.anotherTime}
                  onClick={toggleDrawer(false)}
                >
                  {listString.IDS_MT_TEXT_FOR_ANOTHER_TIME}
                </Typography>
              </Box>
            </Box>
          </Box>
        </Box>
      </Fade>
    </Modal>
  );
};
AllowGPSModal.propTypes = {};
export default AllowGPSModal;
