import { useState, useEffect } from "react";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import { Box } from "@material-ui/core";

import { isEmpty, handleSaveItemStorage } from "@utils/helpers";
import { useSystem } from "@contextProvider/ContextProvider";
import { IconDirection, IconHistory } from "@public/icons";
import {
  listString,
  LAST_LOCATION_RECENT_SEARCH_HISTORY_ITEMS,
} from "@utils/constants";

import ButtonComponent from "@src/button/Button";

const useStyles = makeStyles((theme) => ({
  hotelNearWhenNoGps: {
    display: "flex",
    alignItems: "center",
  },
  divider: {
    height: 8,
    backgroundColor: theme.palette.gray.grayLight22,
    margin: "12px -16px 16px",
    width: "calc(100% + 32px)",
  },
  btnRemoveHistorySearch: {
    justifyContent: "left",
    paddingLeft: 0,
  },
  btnSelectDestination: {
    margin: 4,
  },
}));

const HistoryAndDestination = ({ handleSelectedLocation = () => {} }) => {
  const classes = useStyles();
  const theme = useTheme();
  const { hotelReducer } = useSystem();
  const [recentSearchHistory, setRecentSearchHistory] = useState([]);
  useEffect(() => {
    setRecentSearchHistory(
      JSON.parse(
        localStorage.getItem(LAST_LOCATION_RECENT_SEARCH_HISTORY_ITEMS)
      ) || []
    );
  }, []);

  const handleSelectLocation = (item) => {
    handleSaveItemStorage(item);
    handleSelectedLocation(item);
  };

  const handleRemoveHistorySearch = () => {
    setRecentSearchHistory([]);
    localStorage.removeItem(LAST_LOCATION_RECENT_SEARCH_HISTORY_ITEMS);
  };
  const handleSearchHotelNear = () => {
    handleSelectedLocation({
      name: listString.IDS_MT_TEXT_HOTEL_NEAR_YOUR,
      type: "near-hotel",
    });
  };
  return (
    <Box>
      <Box className={classes.hotelNear} onClick={handleSearchHotelNear}>
        <Box className={classes.hotelNearWhenNoGps}>
          <IconDirection />
          <Box component="span" lineHeight="17px" pl={12 / 8}>
            {listString.IDS_MT_TEXT_HOTEL_NEAR_YOUR}
          </Box>
        </Box>
      </Box>
      <Box className={classes.divider} />
      {!isEmpty(recentSearchHistory) && (
        <Box>
          <Box pb={12 / 8} fontWeight={600} fontSize={18} lineHeight="22px">
            {listString.IDS_TEXT_RECENT_SEARCH}
          </Box>
          {recentSearchHistory.map((el, index) => (
            <Box
              className={classes.hotelNearWhenNoGps}
              key={index.toString()}
              py={12 / 8}
              onClick={() => handleSelectedLocation(el)}
            >
              <Box width={20} height={20}>
                <IconHistory />
              </Box>
              <Box
                component="span"
                lineHeight="17px"
                pl={12 / 8}
                textAlign="left"
              >
                {el.name}
              </Box>
            </Box>
          ))}
          <ButtonComponent
            typeButton="text"
            color={theme.palette.blue.blueLight8}
            className={classes.btnRemoveHistorySearch}
            handleClick={handleRemoveHistorySearch}
          >
            {listString.IDS_MT_TEXT_REMOVE_RECENT_SEARCH}
          </ButtonComponent>
        </Box>
      )}

      <Box>
        <Box pb={12 / 8} fontWeight={600} fontSize={18} lineHeight="22px">
          {listString.IDS_MT_TEXT_COMMON}
        </Box>
        <Box flexWrap="wrap" display="flex" mt={1} ml={-4 / 8}>
          {hotelReducer.topLocations.map((el, index) => {
            return (
              <ButtonComponent
                key={index.toString()}
                color="initial"
                width="fit-content"
                borderRadius={8}
                padding="8px 16px"
                backgroundColor={theme.palette.gray.grayLight22}
                className={classes.btnSelectDestination}
                handleClick={() => handleSelectLocation(el)}
              >
                {el.name}
              </ButtonComponent>
            );
          })}
        </Box>
      </Box>
    </Box>
  );
};

export default HistoryAndDestination;
