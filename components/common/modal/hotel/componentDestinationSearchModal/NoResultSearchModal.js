import { Box, Typography } from "@material-ui/core";
import makeStyles from "@material-ui/styles/makeStyles";
import Image from "@src/image/Image";
import { listString } from "@utils/constants";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  wrapResultSearch: {
    marginTop: 200,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
  result: {
    marginTop: 16,
    fontSize: 18,
    fontWeight: 400,
    color: "#1A202C",
    lineHeight: "20px",
  },
  textSearchAgain: {
    marginTop: 4,
    color: theme.palette.gray.grayDark7,
    fontWeight: "normal",
  },
  noResult: {
    width: 189,
    height: 170,
  },
}));

const NoResultSearchModal = ({ keyword, className }) => {
  const classes = useStyles();
  return (
    <>
      <Box className={clsx(classes.wrapResultSearch, className)}>
        <Box className={classes.wrapIconResult}>
          <Image
            srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_no_result_search.svg"
            className={classes.noResult}
          />
        </Box>
        <Box className={classes.result}>
          {" "}
          {listString.IDS_MT_TEXT_NO_RESULT_ABOUT} {`"${keyword}"`}
        </Box>
        <Typography variant="subtitle2" className={classes.textSearchAgain}>
          {listString.IDS_MT_TEXT_TRY_AGAIN_SEARCH_KEYWORD}
        </Typography>
      </Box>
    </>
  );
};
NoResultSearchModal.propTypes = {};
export default NoResultSearchModal;
