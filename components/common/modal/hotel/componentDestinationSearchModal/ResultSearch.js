import { makeStyles } from "@material-ui/styles";
import { Box, Typography } from "@material-ui/core";

import { isEmpty, handleSaveItemStorage } from "@utils/helpers";
import { listString } from "@utils/constants";
import { IconHotel, IconLocationBorder } from "@public/icons";

import NoResultSearchModal from "@components/common/modal/hotel/componentDestinationSearchModal/NoResultSearchModal";

const useStyles = makeStyles((theme) => ({
  addressHotel: {
    color: theme.palette.gray.grayDark7,
  },
  iconLocation: {
    stroke: theme.palette.gray.grayDark7,
  },
}));

const ResultSearch = ({
  listLocation = [],
  listHotel = [],
  inputSearch = "",
  handleSelectedItem = () => {},
}) => {
  const classes = useStyles();
  const handleSelectItem = (item) => () => {
    handleSaveItemStorage(item);
    handleSelectedItem(item);
  };
  return (
    <Box>
      {!isEmpty(listLocation) && (
        <Box pb={2}>
          <Box color="gray.grayDark7" pb={12 / 8}>
            <Typography variant="subtitle2">
              {listString.IDS_MT_TEXT_FILTER_LOCATION}
            </Typography>
          </Box>
          {listLocation.map((el, index) => (
            <Box
              display="flex"
              justifyContent="space-between"
              py={12 / 8}
              key={index.toString()}
              onClick={handleSelectItem(el)}
            >
              <Box display="flex" alignItems="center">
                <IconLocationBorder
                  className={`svgFillAll ${classes.iconLocation}`}
                />
                <Box component="span" pl={12 / 8} lineHeight="17px">
                  {el.name}
                </Box>
              </Box>
              <Box display="flex" alignItems="center">
                <Box component="span" pr={1}>
                  <Typography variant="body2">{el.numHotels}</Typography>
                </Box>
                <IconHotel />
              </Box>
            </Box>
          ))}
        </Box>
      )}
      {!isEmpty(listHotel) && (
        <Box>
          <Box color="gray.grayDark7" pb={12 / 8}>
            <Typography variant="subtitle2">
              {listString.IDS_MT_TEXT_HOTEL_TYPE_HOME_HOTEL}
            </Typography>
          </Box>
          {listHotel.map((el) => (
            <Box
              display="flex"
              py={12 / 8}
              key={el.hotelId}
              onClick={handleSelectItem(el)}
            >
              <Box component="span">
                <IconHotel />
              </Box>
              <Box display="flex" flexDirection="column" pl={12 / 8}>
                <Box component="span" pb={4 / 8} lineHeight="17px">
                  {el.name}
                </Box>
                <Box component="span" className={classes.addressHotel}>
                  <Typography variant="body2">{el?.label}</Typography>
                </Box>
              </Box>
            </Box>
          ))}
        </Box>
      )}
      {isEmpty(listLocation) && isEmpty(listHotel) && (
        <NoResultSearchModal keyword={inputSearch} />
      )}
    </Box>
  );
};

export default ResultSearch;
