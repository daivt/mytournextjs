import MomentUtils from "@date-io/moment";
import { createMuiTheme, IconButton, InputAdornment } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { IconCalender } from "@public/icons";
import FormControlTextFieldBase from "@src/form/FormControlTextFieldBase";
import { DATE_FORMAT, DATE_FORMAT_BACK_END } from "@utils/moment";
import moment from "moment";
import React, { useMemo, useState } from "react";
import MaskedInput from "react-text-mask";
import { ThemeProvider } from "styled-components";

export const DateMaskCustomSingle = (props) => {
  const { inputRef, placeholder, ...other } = props;

  return (
    <MaskedInput
      {...other}
      ref={(ref) => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={[/\d/, /\d/, "/", /\d/, /\d/, "/", /\d/, /\d/, /\d/, /\d/]}
      placeholder={placeholder}
      guide={false}
    />
  );
};

const BirthDayField = ({
  errorMessage,
  id,
  label,
  date,
  update,
  optional,
  inputStyle,
  disableFuture,
  disablePast,
  okLabel,
  cancelLabel,
  placeholder,
}) => {
  const [dateFormatStr, setDateFormatStr] = React.useState(
    `${date ? date.format(DATE_FORMAT) : ""}`
  );
  const [open, setOpen] = useState(false);
  const themeConfig = useTheme();
  const theme = useMemo(
    () =>
      createMuiTheme({
        palette: { primary: { main: themeConfig.palette.secondary.main } },
      }),
    []
  );

  const textChange = React.useCallback(
    (text) => {
      const dateTmp = moment(text, DATE_FORMAT_BACK_END, true);
      if (dateTmp?.isValid()) {
        update(dateTmp);
      } else {
        update(undefined);
      }
    },
    [update]
  );

  const resetText = React.useCallback(() => {
    setDateFormatStr(`${date ? date.format(DATE_FORMAT_BACK_END) : ""}`);
  }, [date]);

  React.useEffect(() => {
    resetText();
  }, [date, resetText]);

  return (
    <div>
      <FormControlTextFieldBase
        id={id}
        onBlur={resetText}
        formControlStyle={inputStyle}
        label={label}
        fullWidth
        value={dateFormatStr}
        style={{
          background: "white",
          width: "100%",
          ...inputStyle,
        }}
        optional={optional}
        inputProps={{ style: { width: "100%" } }}
        placeholder={placeholder ? placeholder : "Ngày sinh"}
        onChange={(e) => {
          setDateFormatStr(e.target.value);
          textChange(e.target.value);
        }}
        inputComponent={DateMaskCustomSingle}
        onClick={() => setOpen(true)}
        errorMessage={errorMessage}
        endAdornment={
          <InputAdornment position="end" style={{ marginRight: 8 }}>
            <IconButton size="small" edge="start" tabIndex={-1}>
              <IconCalender />
            </IconButton>
          </InputAdornment>
        }
      />
      <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils}>
        <ThemeProvider theme={theme}>
          <DatePicker
            variant="dialog"
            autoOk
            open={open}
            disableFuture={disableFuture}
            disablePast={disablePast}
            maxDate={disableFuture ? moment() : undefined}
            minDate={disablePast ? moment() : undefined}
            openTo="year"
            format="DD-MM-YYYY"
            label="Date of birth"
            views={["year", "month", "date"]}
            value={date || null}
            onChange={(newDate) => {
              update(newDate || undefined);
            }}
            TextFieldComponent={() => <></>}
            onAccept={() => setOpen(false)}
            onClose={() => setOpen(false)}
            okLabel={okLabel || "Áp dụng"}
            cancelLabel={cancelLabel || ""}
          />
        </ThemeProvider>
      </MuiPickersUtilsProvider>
    </div>
  );
};
export default BirthDayField;
