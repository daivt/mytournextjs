import React, { useEffect, useRef } from "react";
import {
  AppBar,
  Box,
  Chip,
  Dialog,
  IconButton,
  List,
  ListItem,
  ListItemText,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { createStyles, makeStyles, withStyles } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";
import SearchIcon from "@material-ui/icons/Search";
import { IconHistory, IconLocation } from "@public/icons";
import FormControlTextField from "@src/form/FormControlTextField";
import { listString, SEARCH_PREV, SEARCH_RECENT } from "@utils/constants";
import { searchAirports } from "@api/flight";

const useStyles = makeStyles((theme) =>
  createStyles({
    appBar: {
      position: "relative",
      borderRadius: 0,
      background: "white",
      color: "#1A202C",
      boxShadow: "none",
    },
    title: {
      width: "calc(100% - 52px)",
      textAlign: "center",
      fontSize: 16,
      lineHeight: "19px",
    },
    clearHistory: {
      color: theme.palette.blue.blueLight8,
      ...theme.typography.subtitle2,
      fontWeight: 400,
      marginLeft: 30,
      marginTop: 5,
      marginBottom: 10,
      cursor: "pointer",
    },
  })
);

export const ChipDes = withStyles((theme) => ({
  root: {
    borderRadius: "8px",
    backgroundColor: theme.palette.gray.grayLight22,
  },
  label: {
    height: 20,
    paddingTop: 3,
    // Fix error text in chip hidden
  },
}))(Chip);

const LIST_POPULAR_DESTINATION = [
  {
    id: "0",
    name: "Hội An",
    iconUrl:
      "https://staticproxy.mytourcdn.com/0x0,q90/resources/pictures/cities/country1606212826.jpg",
  },
  {
    id: "1",
    name: "Phan Thiết",
    iconUrl:
      "https://staticproxy.mytourcdn.com/0x0,q90/resources/pictures/cities/country1606212795.jpg",
  },
  {
    id: "2",
    name: "Vịnh Hạ Long",
    iconUrl:
      "https://staticproxy.mytourcdn.com/0x0,q90/resources/pictures/cities/country1606212899.jpg",
  },
  {
    id: "3",
    name: "Nha Trang",
    iconUrl:
      "https://staticproxy.mytourcdn.com/0x0,q90/resources/pictures/cities/country1606212765.jpg",
  },
  {
    id: "4",
    name: "Cần Thơ",
    iconUrl:
      "https://staticproxy.mytourcdn.com/0x0,q90/resources/pictures/cities/country1606212826.jpg",
  },
  {
    id: "5",
    name: "Đà Nẵng",
    iconUrl:
      "https://staticproxy.mytourcdn.com/0x0,q90/resources/pictures/cities/country1606212826.jpg",
  },
  {
    id: "6",
    name: "Sa Pa",
    iconUrl:
      "https://staticproxy.mytourcdn.com/0x0,q90/resources/pictures/cities/country1606212826.jpg",
  },
  {
    id: "7",
    name: "Phú Quốc",
    iconUrl:
      "https://staticproxy.mytourcdn.com/0x0,q90/resources/pictures/cities/country1606212826.jpg",
  },
  {
    id: "8",
    name: "Ninh Bình",
    iconUrl:
      "https://staticproxy.mytourcdn.com/0x0,q90/resources/pictures/cities/country1606212826.jpg",
  },
  {
    id: "9",
    name: "Ninh Thuận",
    iconUrl:
      "https://staticproxy.mytourcdn.com/0x0,q90/resources/pictures/cities/country1606212826.jpg",
  },
];

const LIST_SEARCH = [
  { code: "HAN", location: "Hà Nội", name: "Nội Bài" },
  { code: "SGN", location: "Hồ Chí Minh", name: "Tân Sơn Nhất" },
  { code: "DAD", location: "Đà Nẵng", name: "Quốc tế Đà Nẵng" },
  { code: "DLI", location: "Đà Lạt", name: "Đà Lạt" },
  { code: "HPH", location: "Hải Phòng", name: "Quốc tế Cát bi" },
  { code: "PQC", location: "Phú Quốc", name: "Quốc tế Phú Quốc" },
  { code: "CXR", location: "Nha Trang", name: "Cam Ranh" },
  { code: "VCS", location: "Côn Đảo", name: "Côn Đảo" },
  { code: "UIH", location: "Quy Nhơn", name: "Quy Nhơn" },
  { code: "HUI", location: "Huế", name: "Quốc tế Phú Bài" },
  { code: "VDH", location: "Đồng Hới", name: "Đồng Hới" },
  { code: "VII", location: "Thành phố Vinh", name: "Vinh" },
];

const SearchDestinationsBox = ({
  open = false,
  handleClose = () => {},
  setDestinationData = () => {},
  isFrom = true,
}) => {
  const classes = useStyles();
  const [inputSearch, setInputSearch] = React.useState("");

  const inputEl = useRef(null);
  const ISSERVER = typeof window === "undefined";
  let historyLists = [];
  if (!ISSERVER) {
    const recentList = JSON.parse(localStorage.getItem(SEARCH_RECENT));
    if (recentList?.length) {
      const recentFrom = recentList.slice(0, 4).map((item) => {
        return {
          code: isFrom ? item?.origin : item?.destination,
          location: isFrom ? item?.flightFrom : item.flightTo,
          name: isFrom
            ? item?.originName || item?.flightFrom
            : item?.destinationName || item?.flightTo,
        };
      });
      let codes = [];
      historyLists = recentFrom.filter((item) => {
        if (codes?.includes(item?.code)) {
          return false;
        } else {
          codes.push(item?.code);
          return true;
        }
      });
    }
  }
  const [historyList, setHistoryList] = React.useState(historyLists);

  useEffect(() => {
    setHistoryList(historyLists)
  },[isFrom])
  const handleClearHistory = () => {
    localStorage.removeItem(SEARCH_RECENT);
    setHistoryList([]);
  };

  const [searchAirportsList, setSearchAirportsList] = React.useState(
    LIST_SEARCH
  );
  const handleChangeInput = (event) => {
    setInputSearch(event.target.value);
  };

  const actionSearchAirports = async (term) => {
    const args = {
      num_items: 5,
      term,
    };
    const { data } = await searchAirports(args);
    setSearchAirportsList(data.data);
  };

  useEffect(() => {
    actionSearchAirports(inputSearch);
  }, [inputSearch]);

  const handleSubmit = (e) => {
    e.preventDefault();
    inputEl?.current && inputEl?.current[0] && inputEl?.current[0].blur();
  };

  return (
    <Dialog fullScreen onClose={handleClose} open={open}>
      <AppBar className={classes.appBar}>
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            onClick={() => handleClose(!open)}
            aria-label="close"
          >
            <CloseIcon style={{ fontSize: 24 }} />
          </IconButton>
          <form
            ref={inputEl}
            style={{ width: "100%" }}
            onSubmit={(e) => handleSubmit(e)}
          >
            <FormControlTextField
              value={inputSearch}
              hideHelperText
              formControlStyle={{ marginRight: 0 }}
              placeholder={listString.IDS_MT_TEXT_FIND_CITY_OR_AIRPORT}
              startAdornment={<SearchIcon className={classes.iconSearch} />}
              onChange={handleChangeInput}
              endAdornment={
                !inputSearch.length ? null : (
                  <IconButton
                    style={{ padding: 2, marginRight: 6 }}
                    onClick={() => {
                      setInputSearch("");
                    }}
                  >
                    <CloseIcon />
                  </IconButton>
                )
              }
            />
          </form>
        </Toolbar>
      </AppBar>

      {!inputSearch && historyList.length > 0 && (
        <>
          <List component="nav" aria-labelledby="nested-list-subheader">
            {historyList.map((element, index) => (
              <ListItem
                button
                key={index}
                onClick={() => {
                  setDestinationData(element);
                  handleClose(!open);
                }}
              >
                <IconHistory />
                <ListItemText
                  style={{ marginLeft: 8 }}
                  primary={element.location}
                  secondary={`${element.code} - Sân bay ${element.name}`}
                />
              </ListItem>
            ))}
          </List>
          <Typography
            onClick={handleClearHistory}
            className={classes.clearHistory}
          >
            Xóa lịch sử tìm kiếm
          </Typography>
        </>
      )}

      {inputSearch && (
        <List component="nav" aria-labelledby="nested-list-subheader">
          {searchAirportsList.map((element, index) => (
            <ListItem
              button
              key={index}
              onClick={() => {
                setDestinationData(element);
                handleClose(!open);
              }}
            >
              <IconLocation />
              <ListItemText
                style={{ marginLeft: 8 }}
                primary={element.location}
                secondary={`${element.code} - Sân bay ${element.name}`}
              />
            </ListItem>
          ))}
        </List>
      )}

      <Box m={2}>
        <Typography variant="subtitle1">
          {listString.IDS_MT_TEXT_COMMON}
        </Typography>
        <Box flexWrap="wrap" display="flex" mt={1}>
          {LIST_SEARCH.map((element, index) => {
            return (
              <Box
                mr={1}
                mb={1}
                key={index}
                onClick={() => {
                  setDestinationData(element);
                  handleClose(!open);
                }}
              >
                <ChipDes label={element.location} />
              </Box>
            );
          })}
        </Box>
      </Box>
    </Dialog>
  );
};
export default SearchDestinationsBox;
