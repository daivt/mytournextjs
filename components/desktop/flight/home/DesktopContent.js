import InfoDescriptionFlightCard from "@components/common/desktop/card/InfoDescriptionFlightCard";
import MyCoupon from "@components/desktop/flight/home/components/MyCoupon";
import Layout from "@components/layout/desktop/Layout";
import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import FlightListSeo from "./components/FlightListSeo";
import FlightPartner from "./components/FlightPartner";
import HomeSeoContent from "./components/HomeSeoContent";
import RecentLocationSearchSlider from "./components/RecentLocationSearchSlider";
import TopPriceFlightList from "./components/TopPriceFlightList";

const useStyles = makeStyles((theme) => ({
  homeWrapper: { width: "100%", background: "white" },
  homeContainer: {
    maxWidth: 1188,
    margin: "0 auto",
    padding: "24px 0 16px 0",
  },
  heading: {
    marginBottom: 24,
  },
}));
const Home = ({
  topLocation = [],
  queryCodeFlight = {},
  query = {},
  isHomeFight = false,
  codeFlightList = [],
  title = "",
  description = "",
  keywords = "",
}) => {
  const classes = useStyles();
  return (
    <Layout
      isHeader
      isFooter
      type="flight"
      topLocation={topLocation}
      queryCodeFlight={queryCodeFlight}
      query={query}
      title={title}
      description={description}
      keywords={keywords}
    >
      <Box className={classes.homeWrapper}>
        <Box className={classes.homeContainer}>
          <InfoDescriptionFlightCard />
          <MyCoupon />
          <RecentLocationSearchSlider />
          {isHomeFight && <FlightPartner />}
        </Box>

        <TopPriceFlightList
          isHomeFight={isHomeFight}
          query={query}
          codeFlight={queryCodeFlight}
          codeFlightList={codeFlightList}
        />
        {!isHomeFight && (
          <Box className={classes.homeContainer}>
            <HomeSeoContent />
          </Box>
        )}
        <Box className={classes.homeContainer}>
          <Box style={{ marginBottom: "100px" }}>
            <Typography variant="h4" className={classes.heading}>
              Các chặng bay & hãng bay hàng đầu
            </Typography>
            <FlightListSeo />
          </Box>
        </Box>
      </Box>
    </Layout>
  );
};

export default Home;
