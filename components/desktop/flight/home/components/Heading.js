import { Typography } from "@material-ui/core";
import React from "react";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  title: {
    marginBottom: 24,
    marginTop: 64,
    stroke: theme.palette.black.black3,
  },
}));
const HomeHeading = ({ children, customStyle }) => {
  const classes = useStyles();
  return (
    <Typography
      className={classes.title}
      variant="h4"
      style={{
        ...customStyle,
      }}
    >
      {children}
    </Typography>
  );
};

export default HomeHeading;
