import { Box, Typography, Grid, makeStyles } from "@material-ui/core";
import { listString, DIALOG_MESSAGE_TYPE } from "@utils/constants";
import { useTheme } from "@material-ui/core/styles";
import clsx from "clsx";
import { getVoucherListPayment } from "@api/flight";
import React, { useEffect, useState } from "react";
import { isEmpty } from "@utils/helpers";
import { DATE_MONTH } from "@utils/moment";
import moment from "moment";
import DialogShowMessage from "@components/common/modal/flight/DialogShowMessage";
import { IconArrowRight } from "@public/icons";
import SlideShow from "@components/common/slideShow/SlideShow";

const useStyles = makeStyles((theme) => ({
  container: {
    background: theme.palette.white.main,
    width: "100%",
    marginTop: 36,
    marginBottom: 64,
  },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
  },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
  },
  cardGroup: {},
  cardItem: {
    margin: "0 12px",
    width: "calc(100% - 24px) !important",
  },
  itemContainer: {
    border: `dashed 1px ${theme.palette.pink.main}`,
    backgroundColor: theme.palette.pink.pink2,
    paddingLeft: 24,
    paddingRight: 24,
    borderRadius: 8,
    minHeight: 144,
  },
  voucherCode: {
    color: theme.palette.white.main,
    backgroundColor: theme.palette.pink.main,
    padding: "2px 6px",
    borderRadius: 4,
    cursor: "pointer",
  },
  voucherDesc: {
    fontSize: 18,
    fontWeight: 600,
    lineHeight: "22px",
    paddingTop: 10,
    color: theme.palette.black.black3,
  },
  voucherExp: {
    color: theme.palette.gray.grayDark7,
  },
  boxRight: {
    display: "flex",
    alignItems: "center",
    height: "100%",
    justifyContent: "flex-end",
    textAlign: "right",
    color: theme.palette.pink.main,
  },
  csPointer: {
    cursor: "pointer",
  },
  fullWidth: {
    width: "100%",
  },
  iconArrow: {
    width: 16,
    height: 17,
    stroke: theme.palette.black.black3,
  },
  wrapArrow: {
    "&:hover": {
      "& svg": {
        stroke: theme.palette.blue.blueLight8,
        transition: "all ease .2s",
      },
    },
  },
}));

const VoucherCard = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const { item } = props;

  return (
    <Grid container className={classes.itemContainer}>
      <Grid item lg={9} md={9} sm={9} xs={9}>
        <Box display="flex" flexDirection="column" py={2}>
          <Box display="flex" alignItems="center">
            <Typography variant="body1">
              {listString.IDS_MT_TEXT_TYPE_PROMO_CODE}
            </Typography>
            <Box
              className={classes.voucherCode}
              ml={6 / 8}
              onClick={() => {
                navigator.clipboard.writeText(item?.codeDetail);
              }}
            >
              <Typography variant="subtitle1">{item?.codeDetail}</Typography>
            </Box>
          </Box>
          <Typography variant="subtitle1" className={classes.voucherDesc}>
            {item?.rewardProgram?.title}
          </Typography>
          <Box pt={12 / 8} className={classes.voucherExp}>
            <Typography variant="caption">
              {listString.IDS_TEXT_DISCOUNT_CARD_EXPIRED}:{" "}
              {`${moment
                .unix(item?.responseData?.validFrom / 1000)
                .format(DATE_MONTH)} - ${moment
                .unix(item?.responseData?.validTo / 1000)
                .format(DATE_MONTH)}`}
              &nbsp;&nbsp;|&nbsp;&nbsp;
            </Typography>
            <Typography variant="caption" className={classes.csPointer}>
              {listString.IDS_TEXT_USE_CODE_WHEN_PAYMENT}
            </Typography>
          </Box>
        </Box>
      </Grid>
      <Grid item lg={3} md={3} sm={3} xs={3}>
        <Box
          className={clsx(classes.boxRight, classes.csPointer)}
          onClick={() =>
            props.setMessageAlert(item?.rewardProgram?.description)
          }
        >
          <Typography variant="caption">
            {listString.IDS_TEXT_CONDITION_AND_RULES}
          </Typography>
        </Box>
      </Grid>
    </Grid>
  );
};

const customStyle = {
  background: "white",
  height: "40px",
  width: "40px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 2,
  borderRadius: "100px",
  boxShadow: "0px 8px 12px rgb(0 0 0 / 10%)",
  cursor: "pointer",
  position: "absolute",
  top: "50%",
  transform: "translate(-50%, -50%)",
};

function SamplePrevArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, left: 12 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight
        className={`svgFillAll ${classes.iconArrow}`}
        style={{ transform: "rotate(180deg)" }}
      />
    </div>
  );
}

function SampleNextArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, right: "-23px" }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight className={`svgFillAll ${classes.iconArrow}`} />
    </div>
  );
}

const MyCoupon = () => {
  const classes = useStyles();
  const theme = useTheme();

  let onlyOne = true;
  const [dataVoucher, setDataVoucher] = useState({});
  const [messageAlert, setMessageAlert] = useState("");
  const [showMessageAlert, setShowMessageAlert] = useState(false);
  const [idxActive, setActive] = useState(0);

  const toggleDrawerAlert = (open) => () => {
    if (!open) {
      setMessageAlert("");
    }
    setShowMessageAlert(open);
  };
  const actionsVoucher = async () => {
    const dataPost = {
      info: { productType: "flight" },
      isUsed: false,
      page: 1,
      module: "flight",
      size: 20,
      term: "",
    };
    const { data } = await getVoucherListPayment(dataPost);
    let listVoucher = data?.data?.list;
    if (listVoucher) {
      setDataVoucher(listVoucher);
    }
  };

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 2,
    nextArrow: <SampleNextArrow customStyleArrow={customStyle} />,
    prevArrow:
      idxActive > 0 ? (
        <SamplePrevArrow customStyleArrow={customStyle} />
      ) : (
        <></>
      ),
    beforeChange: (current, next) => setActive(next),
  };

  useEffect(() => {
    actionsVoucher();
  }, []);

  if (isEmpty(dataVoucher)) {
    return <div />;
  } else {
    if (dataVoucher.length > 1) {
      onlyOne = false;
    }
  }
  return (
    <Box className={classes.container}>
      {!isEmpty(dataVoucher) && (
        <Box className={classes.content}>
          <Box className={clsx(classes.cardGroup)}>
            {dataVoucher.length >= 2 ? (
              <SlideShow settingProps={settings} style={{ margin: "0 -12px" }}>
                {dataVoucher.map((item, index) => (
                  <Box
                    className={clsx(
                      onlyOne ? classes.fullWidth : classes.cardItem
                    )}
                    key={index}
                  >
                    <VoucherCard
                      item={item}
                      setMessageAlert={setMessageAlert}
                    />
                  </Box>
                ))}
              </SlideShow>
            ) : (
              <>
                {dataVoucher.map((item, index) => (
                  <Box
                    className={clsx(
                      onlyOne ? classes.fullWidth : classes.cardItem
                    )}
                    key={index}
                  >
                    <VoucherCard
                      item={item}
                      setMessageAlert={setMessageAlert}
                    />
                  </Box>
                ))}
              </>
            )}
          </Box>
          <DialogShowMessage
            message={messageAlert}
            toggleDrawer={toggleDrawerAlert}
            type={DIALOG_MESSAGE_TYPE.HTML}
            title={listString.IDS_TEXT_CONDITION_AND_RULES}
          />
        </Box>
      )}
    </Box>
  );
};

export default MyCoupon;
