import { Box, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { routeStatic } from "@utils/constants";
import {
  listDestinationFirst,
  listDestinationSecond,
  listFlightsSecond,
  listFlightsFirst,
} from "@utils/dataFake";
import { generateSlugFromFlightInfo } from "@utils/helpers";
import Link from "@src/link/Link";


const useStyles = makeStyles((theme) => ({
  subTitle: {
    marginBottom: 10
  },
  caption: {
    marginBottom: 24,
    display: "block"
  },
  heading: {
    marginBottom: 24
  },
  listItem: {
    ...theme.typography.fontSize,
    lineHeight: "32px",
    color: theme.palette.black.black3,
  }
}));

const FlightListSeo = () => {
  const classes = useStyles()
  return (
    <Grid container  spacing={2}>
        <Grid item md={3}>
          {listDestinationFirst.map((el) => (
            <Box key={el.code}>
              <Link
                href={{
                  pathname: routeStatic.FLIGHT_LANDING.href,
                  query: {
                    slug: ["den", generateSlugFromFlightInfo(el, "den")],
                  },
                }}
              >
                <Typography
                  variant="caption"
                  className={classes.listItem}
                >{`Vé máy bay đến ${el.arrival}`}</Typography>
              </Link>
            </Box>
          ))}
        </Grid>
        <Grid item md={3}>
          {listDestinationSecond.map((el) => (
            <Box key={el.code}>
              <Link
                href={{
                  pathname: routeStatic.FLIGHT_LANDING.href,
                  query: {
                    slug: ["den", generateSlugFromFlightInfo(el, "den")],
                  },
                }}
              >
                <Typography
                  variant="caption"
                  className={classes.listItem}
                >{`Vé máy bay đến ${el.arrival}`}</Typography>
              </Link>
            </Box>
          ))}
        </Grid>
        <Grid item md={3}>
          {listFlightsFirst.map((el) => (
            <Box key={el.code}>
              <Link
                href={{
                  pathname: routeStatic.FLIGHT_LANDING.href,
                  query: {
                    slug: ["tu", generateSlugFromFlightInfo(el, "tu")],
                  },
                }}
              >
                <Typography
                  variant="caption"
                  className={classes.listItem}
                >{`Vé máy bay ${el.depart} - ${el.arrival}`}</Typography>
              </Link>
            </Box>
          ))}
        </Grid>
        <Grid item md={3}>
          {listFlightsSecond.map((el) => (
            <Box key={el.code}>
              <Link
                href={{
                  pathname: routeStatic.FLIGHT_LANDING.href,
                  query: {
                    slug: ["tu", generateSlugFromFlightInfo(el, "tu")],
                  },
                }}
              >
                <Typography
                  variant="caption"
                  className={classes.listItem}
                >{`Vé máy bay ${el.depart} - ${el.arrival}`}</Typography>
              </Link>
            </Box>
          ))}
        </Grid>
      </Grid>
  );
};

export default FlightListSeo;
