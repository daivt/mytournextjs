import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import React from "react";
import { IconLocationBorder, IconLocationFrom } from "@public/icons";
import { listString } from "@utils/constants";
import moment from "moment";
import { DATE_FORMAT, DATE_FORMAT_BACK_END } from "@utils/moment";
import { useRouter } from "next/router";

const useStyles = makeStyles((theme) => ({
  flightRecentSearch: {
    background: "#FFFFFF",
    boxShadow:
      "0px 0px 6px rgba(0, 0, 0, 0.05), 0px 6px 6px rgba(0, 0, 0, 0.06)",
    borderRadius: 8,
    padding: 14,
    paddingLeft: 35,
    cursor: "pointer",
  },
  airportName: {
    fontSize: 16,
    fontWeight: 600,
    lineHeight: "19px",
    color: theme.palette.black.black3,
  },
  info: {
    fontSize: 14,
    lineHeight: "17px",
    fontWeight: 400,
    marginTop: 10,
    color: theme.palette.gray.grayDark8,
  },
  location: {
    position: "relative",
  },
  locationIcon: {
    position: "absolute",
    left: "-22px",
    top: 0,
  },
  wrapLocation: {
    position: "relative",
    "&:before": {
      content: '""',
      position: "absolute",
      width: 1,
      height: 8,
      borderLeft: "1px dashed #4A5568",
      top: 16,
      left: "-15px",
    },
  },
  infoItem: {
    marginRight: 15,
    position: "relative",
    "&:before": {
      content: '""',
      width: 3,
      height: 3,
      background: theme.palette.gray.grayLight24,
      borderRadius: "50%",
      top: 16,
      left: "-8px",
      position: "absolute",
    },
    "&:first-child": {
      "&:before": {
        display: "none",
      },
    },
  },
}));
const FlightRecentSearch = (item) => {
  const classes = useStyles();

  const router = useRouter();
  const handleClickTicket = (item) => () => {
    router.push({
      pathname: "/ve-may-bay/result/outbound",
      query: {
        adultCount: item?.adult || 1,
        childCount: item?.child || 0,
        infantCount: item?.baby || 0,
        seatSearch: item?.seatCode,
        departureDate: item?.dateDep
          ? moment(item.dateDep, DATE_FORMAT).format(DATE_FORMAT_BACK_END)
          : moment().format(DATE_FORMAT_BACK_END),
        returnDate: item?.dateRet
          ? moment(item.dateRet, DATE_FORMAT).format(DATE_FORMAT_BACK_END)
          : "",
        origin_code: item?.origin,
        origin_location: item?.flightFrom,
        destination_code: item?.destination,
        destination_location: item?.flightTo,
      },
    });
  };
  return (
    <>
      <Box
        onClick={handleClickTicket(item)}
        className={classes.flightRecentSearch}
      >
        <Box className={classes.wrapLocation}>
          <Box className={classes.location} style={{ marginBottom: 8 }}>
            <IconLocationFrom className={classes.locationIcon} />
            <Typography className={classes.airportName}>{item.from}</Typography>
          </Box>
          <Box className={classes.location}>
            <IconLocationBorder className={classes.locationIcon} />
            <Typography className={classes.airportName}>{item.to}</Typography>
          </Box>
        </Box>
        <Box>
          <Typography className={classes.info}>{item.date}</Typography>
        </Box>
        <Box display="flex">
          <Box className={classes.infoItem}>
            <Typography className={classes.info}>
              {item.guest} {listString.IDS_MT_TEXT_SINGLE_CUSTOMER}
            </Typography>
          </Box>
          <Box className={classes.infoItem}>
            <Typography className={classes.info}>{item.seat}</Typography>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default FlightRecentSearch;
