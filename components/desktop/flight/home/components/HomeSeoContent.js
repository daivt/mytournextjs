import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
// import FlightListSeo from 'FlightListSeo'


const useStyles = makeStyles((theme) => ({
  wrapBox: {
    // padding: "56px 0",
  },
  subTitle: {
    marginBottom: 10
  },
  caption: {
    marginBottom: 24,
    display: "block"
  },
  heading: {
    marginBottom: 24
  },
  listItem: {
    ...theme.typography.fontSize,
    lineHeight: "32px",
    color: theme.palette.black.black3,
  }
}));

const HomeSeoContent = () => {
  const classes = useStyles()
  return (
    <Box className={classes.wrapBox}>
      {/* Tạm ẩn theo yêu cầu của a Quang Tran */}
      {/* <Typography variant="h4" className={classes.heading}>Vé máy bay Đà Nẵng giá rẻ mua ở đâu?</Typography>
      <Typography variant="caption" className={classes.caption}>Đà Nẵng là một trong những thành phố được rất nhiều du khách trong và ngoài nước yêu thích. Với cảnh sắc thiên nhiên tươi đẹp, những công trình kiến trúc đi vào lịch sử, bởi vậy vé máy bay Đà Nẵng mỗi năm luôn được rất nhiều người tìm kiếm.</Typography>
      <Typography variant="subtitle1" className={classes.subTitle}>
        Đà Nẵng điểm đến không thể bỏ qua
      </Typography>
      <Typography variant="caption" className={classes.caption}>
        Từ lâu, Đà Nẵng vẫn được mệnh danh là thành phố biển xinh đẹp, nơi có những bãi biển dài, trong xanh, bầu không khí trong lành và những món ăn vô cùng đặc sắc. Nhắc tới Đà Nẵng ta không thể bỏ qua Cầu Rồng, một trong những biểu tượng của thành phố biển xinh đẹp. Bất kỳ ai đến đây, đều muốn dừng chân để xem Rồng phun lửa và nước vào các ngày cuối tuần. Cây cầu bắc qua Sông Hàn, nối 2 bờ của thành phố, khi ngắm vào ban đêm bạn sẽ thấy hình ảnh cầu Rồng lung linh, sặc sỡ dưới những dải màu lấp lánh. 
        </Typography><Typography variant="caption" className={classes.caption}>
        Không chỉ dừng lại ở đó, Đà Nẵng còn nổi tiếng với Bà Nà Hill, một trong những điểm đến hấp dẫn nhất cả nước. Nơi đây được mệnh danh là một châu  u thu nhỏ, ngay trên đỉnh núi với công trình kiến trúc độc đáo mà ta phải kể đến như: Khu Làng Pháp, Cầu Vàng, Hầm Rượu Debay, Bảo tàng sáp… 
        </Typography><Typography variant="caption" className={classes.caption}>
        Nhưng nếu như yêu thích không khí sôi động, bạn có thể ghé thăm một số khu vui chơi giải trí hấp dẫn khác như: Asia Park, Bảo tàng 3D Trick Eye, công viên giải trí Fantasy Park, Cầu quay sông Hàn… Hoặc muốn khám phá văn hóa, lịch sử thì có thể đến phố cổ Hội An, thánh địa Mỹ Sơn là những điểm dừng chân không thể bỏ qua.
      </Typography>
      <Typography variant="subtitle1" className={classes.subTitle}>
        Vé máy bay đi Đà Nẵng phổ biến
      </Typography>
      <Typography variant="caption" className={classes.caption}>
        Sân bay Đà Nẵng là trạm trung chuyển của khoảng 50 đường nội địa và quốc tế. Trong đó, có một số hãng hàng không quốc tế khai thác như: China Airlines, Singapore Airlines, Eva Airlines và 4 hãng hàng không nội địa là Vietjet, Pacific Airlines, Bamboo Airways và Vietnam Airlines. 
        </Typography><Typography variant="caption" className={classes.caption}>
        Giá vé máy bay đi Đà Nẵng sẽ tùy phụ thuộc vào chặng bay và thời điểm đặt vé. Bạn có thể tham khảo giá vé máy bay của những chặng bay phổ biến đến Đà Nẵng bên dưới đây.
        Sài Gòn – Đà Nẵng: Giá vé từ: 480.000 - 2.050.000 VND/chiều. Tần suất 10 – 15 chuyến/ngày. Thời gian bay 1 giờ 20 phút.
        Hà Nội - Đà Nẵng: Giá vé từ: 580.000 - 2.020.000 VND/ một chiều. Tần suất 10 – 15 chuyến/ngày. Thời gian bay 1 giờ 20 phút.
        Lưu ý :
        Giá vé máy bay đi Đà Nẵng ở trên chỉ có tính chất tham khảo.
        Giá vé là 1 chặng và chưa bao gồm thuế và phí.
      </Typography>
      <Typography variant="subtitle1" className={classes.subTitle}>
      Sân bay quốc tế Đà Nẵng
      </Typography>
      <Typography variant="caption" className={classes.caption}>
        Sân bay quốc tế Đà Nẵng lớn thứ ba cả nước, sau sân bay Tân Sơn Nhất và sân bay Nội Bài. Đây là điểm dừng chân đông đúc nhất của khu vực miền Trung - Tây Nguyên với tần suất hơn 200 chuyến bay mỗi ngày. Sân bay có địa chỉ tại: Duy Tân, Hòa Thuận Tây, Quận Hải Châu, Thành phố Đà Nẵng. Được xây dựng vào năm 1940, với tổng diện tích sân bay vào khoảng 842 ha. Đến nay, sân bay hiện có 22 hãng hàng không quốc tế, và 4 hãng hàng không nội địa.
        </Typography><Typography variant="caption" className={classes.caption}>
        Nằm tại vị trí “đắc địa”, chỉ cách trung tâm 3km nên du khách chỉ mất vài phút di chuyển đến các địa điểm du lịch và bằng rất nhiều phương tiện như: Taxi, xe bus, xe ôm...
      </Typography>
      <Typography variant="subtitle1" className={classes.subTitle}>
        Kinh nghiệm đặt vé máy bay giá rẻ đi Đà Nẵng
      </Typography>
      <Typography variant="caption" className={classes.caption}>
        Sở hữu cảnh sắc thiên tươi đẹp cùng với nhiều khu du lịch nổi tiếng, cũng vì thế mà vé máy bay đi Đà Nẵng luôn hot và càng cao điểm vào các mùa lễ Tết. Trong đó phải kể đến dịp Tết Nguyên đán khi rất nhiều người đổ về quê ăn Tết, cùng với nhu cầu đi du lịch ngày càng nhiều. 
        Giá vé máy bay thời điểm này tăng rất cao, lại nhanh chóng hết, vì thế bạn nên quyết định lịch trình từ sớm, trước khoảng 2-3 tháng để dễ dàng săn vé máy bay giá rẻ lẫn đặt được khách sạn tốt nhất cho kỳ nghỉ lễ. 
        </Typography><Typography variant="caption" className={classes.caption}>
        Ngoài ra, một số thời điểm giá vé ổn định là tháng là tháng 8 đến tháng 12, lúc này Đà Nẵng đang là mùa mưa nên giá vé xuống rất thấp, nhưng du lịch rất dễ gặp phải tình trạng mưa kéo dài. Từ tháng 1 đến tháng 7 là mùa khô đặc biệt là tháng 4 đến tháng 7 là thời điểm vé máy bay Đà Nẵng sôi động nhất. Nhưng đổi lại đây là dịp thời tiết đẹp, thuận lợi cho mọi hoạt động vui chơi, giải trí của du khách. 
        </Typography><Typography variant="caption" className={classes.caption}>
        Bên cạnh hoạt động du lịch, còn rất nhiều hoạt động khác như công tác, họp mặt… Vì thế, nếu có dịp đến Đà Nẵng, bạn nên đặt vé từ sớm để có thể sở hữu tấm vé tốt, giờ đẹp, không làm lỡ dở mất công việc của mình.
      </Typography>
      <Typography variant="subtitle1" className={classes.subTitle}>
        Mua vé máy bay Đà Nẵng giá rẻ tại Mytour.vn
      </Typography>
      <Typography variant="caption" className={classes.caption}>
        Sắp tới bạn có một chuyến đi đến Đà Nẵng và vẫn chưa lựa chọn được địa chỉ mua vé máy bay uy tín? Vậy thì Mytour chính là địa điểm lý tưởng để bạn lựa chọn. Mytour luôn có những chính sách ưu đãi hấp dẫn về giá cùng với dịch vụ chăm sóc, hỗ trợ khách hàng chất lượng. 
        Chỉ cần truy cập vào trang web hoặc liên hệ qua nhân viên tư vấn bạn sẽ đặt được một tấm vé giá tốt như mong muốn.
      </Typography> */}
    </Box>
  );
};

export default HomeSeoContent;
