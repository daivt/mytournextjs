import { useState, useEffect } from "react";
import { Box, Grid } from "@material-ui/core";
import { searchHotTickets } from "@api/flight";
import { makeStyles } from "@material-ui/styles";
import { isEmpty, convertUrlFlight } from "@utils/helpers";
import { formatDate } from "@utils/constants";
import { domesticInBound } from "@utils/domestic";
import find from "lodash/find";
import minBy from "lodash/minBy";
import moment from "moment";
import take from "lodash/take";
import { useRouter } from "next/router";

import FlightHomeCard from "../../../../common/desktop/itemView/FlightItemVertical";
import FlightHomeSeo from "../../../../common/desktop/itemView/FlightItemSeo";
import HomeHeading from "./Heading";

const useStyles = makeStyles((theme) => ({
  container: {
    background: "#EDF6F9",
    width: "100%",
    maxWidth: "1300px",
    borderRadius: 12,
    margin: "70px auto 0 auto",
    padding: "56px 0 0 0",
  },
  homeContainer: {
    maxWidth: 1188,
    margin: "0 auto",
    padding: "0 0 56px 0",
  },
  content: {
    maxWidth: 1188,
    width: "calc(100% + 24px)",
    margin: "0 auto",
    display: "flex",
    // justifyContent: "space-between",
    flexDirection: "column",
  },
}));

const TopPriceFlightList = ({
  isHomeFight = false,
  codeFlight = {},
  codeFlightList = [],
  query = {},
}) => {
  const [listData, setListData] = useState([]);

  const classes = useStyles();
  const router = useRouter();
  let departSearch = convertUrlFlight(router?.query?.slug);
  const searchCode = departSearch;
  departSearch = departSearch?.split("-");
  let departParam =
    query?.slug && query?.slug[0] === "den" && departSearch && departSearch[1]
      ? departSearch[1]
      : departSearch && departSearch[0]
      ? departSearch[0]
      : "SGN";
  let listInBound = [];
  if (query?.slug && query?.slug[0] === "den") {
    domesticInBound.forEach((data, index) => {
      const airportCode = data.code.split("-");
      if (departParam === airportCode[1]) {
        listInBound.push({
          arrivalCity: data?.depart,
          departCity: departParam,
          fromAirport: airportCode[0],
          toAirport: departParam,
        });
      }
    });
  } else if (query?.slug && query?.slug[0] === "tu") {
    domesticInBound.forEach((data, index) => {
      const airportCode = data.code;
      if (searchCode === airportCode) {
        listInBound.push({
          arrivalCity: data?.arrival,
          departCity: data?.depart,
          fromAirport: airportCode.split("-")[0],
          toAirport: airportCode.split("-")[1],
        });
      }
    });
  } else {
    domesticInBound.forEach((data, index) => {
      const airportCode = data.code.split("-");
      if (departParam === airportCode[0]) {
        listInBound.push({
          arrivalCity: data?.arrival,
          departCity: departParam,
          fromAirport: departParam,
          toAirport: airportCode[1],
        });
      }
    });
  }
  const itinerariesParam = take(listInBound, 5);

  const getBestFly = async () => {
    try {
      const dataDTO = {
        adults: 1,
        children: 0,
        fromDate: moment().format(formatDate.firstYear),
        groupByDate: true,
        infants: 0,
        itineraries: itinerariesParam,
        toDate: moment()
          .add(6, "days")
          .format(formatDate.firstYear),
      };
      let listData = [];
      const { data } = await searchHotTickets(dataDTO);
      if (query?.slug && query?.slug[0] === "tu") {
        // trường hợp chặng bay cụ thể điểm đi, điểm đến
        data.data.hotTickets[0].priceOptions.forEach((el) => {
          const itinerary = {
            fromAirport: find(
              data?.data?.airports,
              (o) => o.code === data.data.hotTickets[0]?.itinerary?.fromAirport
            ),
            toAirport: find(
              data?.data?.airports,
              (o) => o.code === data.data.hotTickets[0]?.itinerary?.toAirport
            ),
          };
          const airlines = find(
            data?.data?.airlines,
            (o) => o.id === el.airlineId
          );
          listData.push({
            origin: itinerary?.fromAirport?.code,
            destination: itinerary?.toAirport?.code,
            flightFrom: itinerary?.fromAirport?.name,
            flightTo: itinerary?.toAirport?.name,
            date: el?.departureDate,
            airlineLogo: airlines?.logo,
            airlineName: airlines?.name,
            airlineId: airlines?.id,
            discountPercent: el?.discountAdult,
            subPrice: (el.totalPrice * (100 + el.discountAdult)) / 100,
            mainPrice: el?.totalPrice,
            farePrice: el?.farePrice || 0,
          });
        });
      } else {
        data.data.hotTickets.forEach((el) => {
          if (!isEmpty(el.priceOptions)) {
            const tickets = minBy(el.priceOptions, "totalPrice");
            const itinerary = {
              fromAirport: find(
                data?.data?.airports,
                (o) => o.code === el.itinerary?.fromAirport
              ),
              toAirport: find(
                data?.data?.airports,
                (o) => o.code === el.itinerary?.toAirport
              ),
            };
            const airlines = find(
              data?.data?.airlines,
              (o) => o.id === tickets.airlineId
            );
            listData.push({
              origin: el?.itinerary?.fromAirport,
              destination: el?.itinerary?.toAirport,
              flightFrom: itinerary?.fromAirport?.name,
              flightTo: itinerary?.toAirport?.name,
              date: tickets?.departureDate,
              airlineLogo: airlines?.logo,
              airlineName: airlines?.name,
              airlineId: airlines?.id,
              discountPercent: tickets?.discountAdult,
              subPrice:
                (tickets.totalPrice * (100 + tickets.discountAdult)) / 100,
              mainPrice: tickets?.totalPrice,
              farePrice: tickets?.farePrice || 0,
            });
          }
        });
      }
      setListData(listData.slice(0, 6));
    } catch (error) {
      console.log("error", error);
    }
  };

  useEffect(() => {
    getBestFly();
  }, [codeFlight]);

  if (!listData.length) {
    return <></>;
  }
  return (
    <Box style={{ paddingBottom: isHomeFight ? 64 : 0 }}>
      <Box className={classes.container}>
        <Box className={classes.homeContainer}>
          <HomeHeading customStyle={{ marginTop: 0 }}>
            Chuyến bay giá tốt{" "}
            {query?.slug && query?.slug[0] === "tu"
              ? `từ ${codeFlight.depart} đến ${codeFlight.arrival}`
              : query?.slug && query?.slug[0] === "den"
              ? `đến ${codeFlightList[0]?.arrivalCity}`
              : ` từ TP.Hồ Chí Minh`}
          </HomeHeading>
          <Grid container spacing={3}>
            {listData.map((ticket, index) => (
              <Grid key={index} item md={4}>
                {isHomeFight || query?.slug[0] === "den" ? (
                  <FlightHomeCard {...ticket} />
                ) : (
                  <FlightHomeSeo {...ticket} />
                )}
              </Grid>
            ))}
          </Grid>
        </Box>
      </Box>
    </Box>
  );
};

export default TopPriceFlightList;
