import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import SlideShow from "@components/common/slideShow/SlideShow";
import { IconArrowRight } from "@public/icons";
import HomeHeading from "./Heading";
import { listString } from "@utils/constants";
import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  iconArrow: {
    width: 16,
    height: 17,
    stroke: theme.palette.black.black3,
  },
  wrapArrow: {
    "&:hover": {
      "& svg": {
        stroke: theme.palette.blue.blueLight8,
        transition: "all ease .2s",
      },
    },
  },
  partnerItem: {
    width: 178,
    height: 88,
    lineHeight: "88px",
    border: `1px solid ${theme.palette.gray.grayLight23}`,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
    textAlign: "center",
    "& img": {
      display: "inline-block",
    },
  },
}));

const customStyle = {
  background: "white",
  height: "40px",
  width: "40px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 2,
  borderRadius: "100px",
  boxShadow: "0px 8px 12px rgb(0 0 0 / 10%)",
  cursor: "pointer",
};

const settings = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 6,
  rows: 2,
  slidesToScroll: 1,
  nextArrow: <SampleNextArrow customStyleArrow={customStyle} />,
  prevArrow: <SamplePrevArrow customStyleArrow={customStyle} />,
};

function SamplePrevArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{
        ...customStyleArrow,
        position: "absolute",
        top: "50%",
        left: 0,
        transform: "translate(-50%, -50%)",
      }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight
        className={`svgFillAll ${classes.iconArrow}`}
        style={{
          transform: "rotate(180deg)",
        }}
      />
    </div>
  );
}

function SampleNextArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{
        ...customStyleArrow,
        position: "absolute",
        right: "-24px",
        top: "50%",
        transform: "translate(0, -50%)",
      }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight className={`svgFillAll ${classes.iconArrow}`} />
    </div>
  );
}
const FlightPartner = () => {
  const listPartner = [
    {
      url:
        "https://storage.googleapis.com/vntravel-fe/4d19acf920476e7d7ffc02ac1603e705/32b3576399251fc0b8eb0d436baaac8d/f7eeb2d81c0d81aba3d2c2e57e929f65.png",
      name: "airasia.png",
    },
    {
      url:
        "https://storage.googleapis.com/vntravel-fe/d5e27c2e3e028f7252b7bbb9a6a08b11/2c5d7944faa217959f9698554bc23e82/1c53475664f55fa54958150f2624ee42.png",
      name: "asianair.png",
    },
    {
      url:
        "https://storage.googleapis.com/vntravel-fe/d5e27c2e3e028f7252b7bbb9a6a08b11/6bbc1da5f789d910f616a037d3955510/0d675a9fefbe04704235587e89f17849.png",
      name: "bamboo.png",
    },
    {
      url:
        "https://storage.googleapis.com/vntravel-fe/d5e27c2e3e028f7252b7bbb9a6a08b11/ff3a2bd6eefed1c1a118e05b121a73c8/98d29bd47e258a47bc7363fa356f31ec.png",
      name: "cathay.png",
    },
    {
      url:
        "https://storage.googleapis.com/vntravel-fe/d5e27c2e3e028f7252b7bbb9a6a08b11/a9a09bbba10f7af1ff67fcb90a5430e5/084ab0f545a4f56ff8b2d799c5e3b8ce.png",
      name: "emiratesair.png",
    },
    {
      url:
        "https://storage.googleapis.com/vntravel-fe/8471db2d83749108567db18f79a53e58/7423c332872bb0270c8989baab588ee9/1c7eeb88e9975894a771fe89a5aa87ed.png",
      name: "pacificair.png",
    },
    {
      url:
        "https://storage.googleapis.com/vntravel-fe/8471db2d83749108567db18f79a53e58/562b37160e7793de1f1b32c3c8fad83c/f748d35dc5caba0704bb8424ba75a455.png",
      name: "quataair.png",
    },
    {
      url:
        "https://storage.googleapis.com/vntravel-fe/8471db2d83749108567db18f79a53e58/0517004540318b19efeccf1250a1a2ab/5a137e306768055a663ecf67d0e50616.png",
      name: "silkair.png",
    },
    {
      url:
        "https://storage.googleapis.com/vntravel-fe/8471db2d83749108567db18f79a53e58/459dfb4e689998230096012b05c66a6d/b72516ac878edf0c14809c6a86cc8c47.png",
      name: "singaporeair.png",
    },
    {
      url:
        "https://storage.googleapis.com/vntravel-fe/baec8c66e54d69b6e1a478077deb2296/a70aa375932d19ee03f98b95cbb98c1e/75580504b51e2f5ae90fb03577494c9b.png",
      name: "thaiair.png",
    },
    {
      url:
        "https://storage.googleapis.com/vntravel-fe/baec8c66e54d69b6e1a478077deb2296/7b0d2f61fe99978522996795a5a38dba/0c58d57a8ff54d586625a72f5200f640.png",
      name: "vietjet.png",
    },
    {
      url:
        "https://storage.googleapis.com/vntravel-fe/baec8c66e54d69b6e1a478077deb2296/6b07489c766174cb167520036449a168/3fe76f0067a2b35deed76666f22074f1.png",
      name: "vnairline.png",
    },
    {
      url:
        "https://storage.googleapis.com/vntravel-fe/4d19acf920476e7d7ffc02ac1603e705/32b3576399251fc0b8eb0d436baaac8d/f7eeb2d81c0d81aba3d2c2e57e929f65.png",
      name: "airasia.png",
    },
    {
      url:
        "https://storage.googleapis.com/vntravel-fe/d5e27c2e3e028f7252b7bbb9a6a08b11/2c5d7944faa217959f9698554bc23e82/1c53475664f55fa54958150f2624ee42.png",
      name: "asianair.png",
    },
  ];
  const classes = useStyles();
  return (
    <>
      <HomeHeading>{listString.IDS_MT_FLIGHT_PARTNER_TITLE}</HomeHeading>
      <Box className="partnerSlider">
        <SlideShow settingProps={settings}>
          {listPartner.map((partner, index) => {
            return (
              <div className={classes.partnerItem}>
                <Image
                  srcImage={partner.url}
                  alt={partner.name}
                  title={partner.name}
                  style={{
                    width: "auto",
                    height: "auto",
                    left: "50%",
                    top: "50%",
                    transform: "translate(-50%,-50%)",
                  }}
                />
              </div>
            );
          })}
        </SlideShow>
      </Box>
    </>
  );
};

export default FlightPartner;
