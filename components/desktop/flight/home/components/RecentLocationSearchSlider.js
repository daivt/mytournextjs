import React from "react"
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import SlideShow from "@components/common/slideShow/SlideShow";
import { IconArrowRight } from "@public/icons";
import { listString } from "@utils/constants";
import moment from "moment";
import {DATE_FORMAT} from "@utils/moment";
import FlightRecentSearch from "./FlightRecentSearch";
import HomeHeading from "./Heading";

const useStyles = makeStyles((theme) => ({
  container: { background: "white", width: "100%" },
  iconArrow: {
    width: 16,
    height: 17,
    stroke: theme.palette.black.black3,
  },
  wrapArrow: {
    "&:hover": {
      "& svg": {
        stroke: theme.palette.blue.blueLight8,
        transition: "all ease .2s",
      },
    },
  },
  historySearchItem: {
    padding: "8px",
    width: "25%",
    "&:first-child": {
      padding: "8px 8px 8px 2px",
    },
  },
}));

const customStyle = {
  background: "white",
  height: "40px",
  width: "40px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 2,
  borderRadius: "100px",
  boxShadow: "0px 8px 12px rgb(0 0 0 / 10%)",
  cursor: "pointer",
};

const settings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 1,
  nextArrow: <SampleNextArrow customStyleArrow={customStyle} />,
  prevArrow: <SamplePrevArrow customStyleArrow={customStyle} />,
};

function SamplePrevArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{
        ...customStyleArrow,
        position: "absolute",
        top: "50%",
        left: 0,
        transform: "translate(-50%, -50%)",
      }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight
        className={`svgFillAll ${classes.iconArrow}`}
        style={{
          transform: "rotate(180deg)",
        }}
      />
    </div>
  );
}

function SampleNextArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{
        ...customStyleArrow,
        position: "absolute",
        right: "-24px",
        top: "50%",
        transform: "translate(0, -50%)",
      }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight className={`svgFillAll ${classes.iconArrow}`} />
    </div>
  );
}

const RecentLocationSearchSlider = () => {
  const classes = useStyles();
  const [searchHistory, setSearchHistory] = React.useState([]);

  React.useEffect(() => {
    const ISSERVER = typeof window === "undefined";

    if (!ISSERVER) {
      const temp = JSON.parse(localStorage.getItem(`SEARCH_RECENT`));
      setSearchHistory(temp?.filter(item => moment(item.dateDep, DATE_FORMAT).isSameOrAfter(moment(), "days")) || []);
    }
  }, []);
  if (!searchHistory.length) return <div></div>;
  return (
    <Box className={(classes.container, `customMargin`)}>
      <HomeHeading>{listString.IDS_TEXT_RECENT_SEARCH}</HomeHeading>
      <SlideShow settingProps={settings}>
        {searchHistory.map((el, index) => {
          return <FlightRecentSearch key={index} {...el} />;
        })}
      </SlideShow>
    </Box>
  );
};

export default RecentLocationSearchSlider;
