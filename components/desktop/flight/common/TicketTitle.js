import { Box, makeStyles, Typography, useTheme } from "@material-ui/core";
import { IconDot } from "@public/icons";
import Link from "@src/link/Link";
import { DATE_FORMAT_BACK_END } from "@utils/moment";
import moment from "moment";
import { isEmpty, CLASS_CODE } from "utils/helpers";

const useStyles = makeStyles((theme) => ({
  wrapper: {},
  textLink: {
    color: theme.palette.black.black3,
    "&:hover": {
      textDecoration: "none",
    },
  },
  iconDot: {
    margin: "6px",
  },
}));

const TicketTitle = ({
  isInBound,
  paramsUrl,
  codeFlight,
  numberTickets,
  date,
  dataQuery,
  isSmall,
  isTwoWays,
  isNoLink,
}) => {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <Box className={classes.wrapper}>
      <Box display="flex">
        {!isEmpty(dataQuery?.returnDate) && (
          <Typography
            variant="subtitle2"
            style={{
              backgroundColor: theme.palette.primary.main,
              color: theme.palette.white.main,
              width: 80,
              textAlign: "center",
              marginBottom: 12,
              padding: 4,
              borderRadius: 4,
              marginRight: 8,
            }}
          >
            {isTwoWays ? "CHIỀU VỀ" : "CHIỀU ĐI"}
          </Typography>
        )}
        {isNoLink ? (
          <Box display="flex" style={{ marginTop: isSmall ? 4 : undefined }}>
            <Typography variant={isSmall ? "subtitle1" : "h4"}>
              {codeFlight.depart} ({codeFlight.codeDep}) - {codeFlight.arrival}{" "}
              ({codeFlight.codeArr})
            </Typography>
            <Typography
              style={{ fontWeight: 400 }}
              variant={isSmall ? "subtitle1" : "h4"}
            >
              {numberTickets ? `: ${numberTickets} chuyến bay` : null}
            </Typography>
          </Box>
        ) : (
          <Link
            href={
              isInBound && !isTwoWays
                ? {
                    pathname: "/ve-may-bay/result/[...slug]",
                    query: {
                      slug: ["inbound", "ve-may-bay"],
                      ...paramsUrl,
                    },
                  }
                : {
                    pathname: "/ve-may-bay/result/[...slug]",
                    query: {
                      slug: ["outbound", "ve-may-bay"],
                      ...paramsUrl,
                    },
                  }
            }
            className={classes.textLink}
            style={{ marginTop: isSmall ? 4 : undefined }}
          >
            <Box display="flex">
              {isTwoWays ? (
                <Typography variant={isSmall ? "subtitle1" : "h4"}>
                  {codeFlight.arrival} ({codeFlight.codeArr}) -{" "}
                  {codeFlight.depart} ({codeFlight.codeDep})
                </Typography>
              ) : (
                <Typography variant={isSmall ? "subtitle1" : "h4"}>
                  {codeFlight.depart} ({codeFlight.codeDep}) -{" "}
                  {codeFlight.arrival} ({codeFlight.codeArr})
                </Typography>
              )}
              <Typography
                style={{ fontWeight: 400 }}
                variant={isSmall ? "subtitle1" : "h4"}
              >
                {numberTickets ? `: ${numberTickets} chuyến bay` : null}
              </Typography>
            </Box>
          </Link>
        )}
      </Box>
      <Box
        display="flex"
        alignItems="center"
        ml={!isEmpty(dataQuery?.returnDate) ? 11 : 0}
      >
        <Typography variant="caption">
          {moment(date, DATE_FORMAT_BACK_END, true)
            .locale("vi_VN")
            .format("ddd")
            .replace("T", "Thứ ")
            .replace("CN", "Chủ Nhật")}
          ,&nbsp;
          {moment(date, DATE_FORMAT_BACK_END).format("DD")}
          &nbsp; tháng&nbsp;
          {moment(date, DATE_FORMAT_BACK_END).format("MM")}
        </Typography>
        <IconDot className={classes.iconDot} />
        <Typography variant="caption">
          {Number(dataQuery.adultCount || 0) +
            Number(dataQuery.childCount || 0) +
            Number(dataQuery.infantCount || 0)}
          &nbsp;khách
        </Typography>
        <IconDot className={classes.iconDot} />
        <Typography variant="caption">
          {CLASS_CODE.find((element) => element?.code === dataQuery?.seatSearch)
            ?.v_name || "Phổ Thông"}
        </Typography>
      </Box>
    </Box>
  );
};

export default TicketTitle;
