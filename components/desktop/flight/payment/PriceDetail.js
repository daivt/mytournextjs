import { Box, Divider, makeStyles } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import clsx from "clsx";
import BootstrapTooltip from "@components/common/bootstrapTooltip/BootstrapTooltip";
import React, { useState } from "react";
import { findLastKey } from "lodash";

const useStyles = makeStyles((theme) => ({
  container: {
    overflow: "auto",
  },
  wrapHeader: {
    alignItems: "center",
    height: 48,
    padding: 16,
  },
  slBox: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  btnClose: { padding: 0 },
  paperAnchorBottom: {
    borderRadius: 0,
    height: "100vh",
  },
  divider: {
    height: 4,
    backgroundColor: theme.palette.gray.grayLight22,
  },
  shortDivider: {
    height: 5,
    width: 42,
    borderRadius: 100,
    color: theme.palette.gray.grayLight23,
  },
  wrapperContent: {
    display: "flex",
    justifyContent: "space-between",
    padding: "14px 0 12px 0",
  },
  boxTotalPrice: {
    paddingTop: "14px",
    display: "flex",
    flexDirection: "column",
  },
  totalPrice: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  boxDiscount: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "14px 0 12px 0",
  },
  boxDiscountLeft: {
    display: "flex",
    alignItems: "center",
  },
  discountTxt: {
    marginRight: 8,
  },
  discountCode: {
    height: 24,
    borderRadius: 4,
    border: `1px solid ${theme.palette.gray.grayLight24}`,
    alignItems: "center",
    display: "flex",
    padding: 4,
  },
  discountPrice: {
    color: theme.palette.green.greenLight7,
  },
  titleBox: {
    fontSize: 18,
    lineHeight: "22px",
  },
  freeText: {
    textTransform: "uppercase",
  },
  customTooltip: {
    maxWidth: 290,
    maxHeight: 100,
    background: theme.palette.green.greenLight7,
    padding: "8px 14px 8px 8px",
    boxSizing: "border-box",
    borderRadius: 6,
    fontSize: 14,
    lineHeight: "16px",
  },
  arrowTooltip: {
    color: theme.palette.green.greenLight7,
  },
}));

const PriceDetail = ({ item = {}, isOrder = false }) => {
  const classes = useStyles();
  const theme = useTheme();
  const isTwoWay = !isEmpty(item?.inbound);
  const [offTooltip, setOffTooltip] = useState(true);
  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  setTimeout(() => {
    setOffTooltip(false);
  }, 10000);

  if (!item) return <div />;
  return (
    <Box className={classes.container}>
      <Box className={classes.boxPriceDetail}>
        <Box pb={1}>
          <Typography
            variant="subtitle1"
            className={clsx(!isOrder && classes.titleBox)}
          >
            {isTwoWay
              ? listString.IDS_MT_TEXT_PRICE_DETAILS_OUTBOUND
              : !isOrder
              ? listString.IDS_MT_TEXT_PRICE_DETAIL
              : ""}
          </Typography>
        </Box>
        {item?.outbound?.numAdult > 0 ? (
          <Box className={clsx(classes.wrapperContent, classes.slBox)}>
            <Typography variant="caption">
              {`${listString.IDS_MT_TEXT_ADULT} x${item.outbound.numAdult}`}
            </Typography>
            <Typography variant="caption">
              {item.outbound.adultMoney.formatMoney()}đ
            </Typography>
          </Box>
        ) : null}
        {item?.outbound?.numChildren ? (
          <Box className={clsx(classes.wrapperContent, classes.slBox)}>
            <Typography variant="caption">
              {`${listString.IDS_MT_TEXT_CHILDREN} x${item.outbound.numChildren}`}
            </Typography>
            <Typography variant="caption">
              {item.outbound.childMoney.formatMoney()}đ
            </Typography>
          </Box>
        ) : null}
        {item?.outbound?.numInfants ? (
          <Box className={clsx(classes.wrapperContent, classes.slBox)}>
            <Typography variant="caption">
              {`${listString.IDS_MT_TEXT_BABY} x${item.outbound.numInfants}`}
            </Typography>
            <Typography
              variant="caption"
              className={clsx(classes.discountPrice, classes.freeText)}
            >
              {listString.IDS_MT_TEXT_FILTER_FREE}
            </Typography>
          </Box>
        ) : null}
        {item?.outbound?.taxFee ? (
          <Box className={clsx(classes.wrapperContent, classes.slBox)}>
            <Typography variant="caption">
              {listString.IDS_MT_TEXT_TAX_FEE}
            </Typography>
            <Typography variant="caption">{`${item?.outbound?.taxFee.formatMoney()}đ`}</Typography>
          </Box>
        ) : null}
        {item?.outbound?.baggageMoney > 0 && (
          <Box className={clsx(classes.wrapperContent, classes.slBox)}>
            <Typography variant="caption">
              {listString.IDS_MT_TEXT_PRICE_ADD_BAGGAGE}
            </Typography>
            <Typography variant="caption">
              {item?.outbound?.baggageMoney.formatMoney()}đ
            </Typography>
          </Box>
        )}
        <Box className={clsx(classes.wrapperContent)}>
          <Typography variant="subtitle2">
            {listString.IDS_MT_TEXT_TOTAL_PRICE_OUTBOUND}
          </Typography>
          <Typography variant="subtitle2">
            {item?.outbound?.total.formatMoney()}đ
          </Typography>
        </Box>
        {isTwoWay && (
          <>
            <Box pt={28 / 8} pb={1}>
              <Typography
                variant="subtitle1"
                className={clsx(!isOrder && classes.titleBox)}
              >
                {listString.IDS_MT_TEXT_PRICE_DETAILS_INBOUND}
              </Typography>
            </Box>
            {item?.inbound?.numAdult > 0 ? (
              <Box className={clsx(classes.wrapperContent, classes.slBox)}>
                <Typography variant="caption">
                  {`${listString.IDS_MT_TEXT_ADULT} x${item.inbound.numAdult}`}
                </Typography>
                <Typography variant="caption">
                  {item.inbound.adultMoney.formatMoney()}đ
                </Typography>
              </Box>
            ) : null}
            {item?.inbound?.numChildren ? (
              <Box className={clsx(classes.wrapperContent, classes.slBox)}>
                <Typography variant="caption">
                  {`${listString.IDS_MT_TEXT_CHILDREN} x${item.inbound.numChildren}`}
                </Typography>
                <Typography variant="caption">
                  {item.inbound.childMoney.formatMoney()}đ
                </Typography>
              </Box>
            ) : null}
            {item?.inbound?.numInfants ? (
              <Box className={clsx(classes.wrapperContent, classes.slBox)}>
                <Typography variant="caption">
                  {`${listString.IDS_MT_TEXT_BABY} x${item.inbound.numInfants}`}
                </Typography>
                <Typography
                  variant="caption"
                  className={clsx(classes.discountPrice, classes.freeText)}
                >
                  {listString.IDS_MT_TEXT_FILTER_FREE}
                </Typography>
              </Box>
            ) : null}
            {item?.inbound?.taxFee ? (
              <Box className={clsx(classes.wrapperContent, classes.slBox)}>
                <Typography variant="caption">
                  {listString.IDS_MT_TEXT_TAX_FEE}
                </Typography>
                <Typography variant="caption">{`${item?.inbound?.taxFee.formatMoney()}đ`}</Typography>
              </Box>
            ) : null}
            {item?.inbound?.baggageMoney > 0 && (
              <Box className={clsx(classes.wrapperContent, classes.slBox)}>
                <Typography variant="caption">
                  {listString.IDS_MT_TEXT_PRICE_ADD_BAGGAGE}
                </Typography>
                <Typography variant="caption">
                  {item?.inbound?.baggageMoney.formatMoney()}đ
                </Typography>
              </Box>
            )}
            <Box className={clsx(classes.wrapperContent, classes.slBox)}>
              <Typography variant="subtitle2">
                {listString.IDS_MT_TEXT_TOTAL_PRICE_INBOUND}
              </Typography>
              <Typography variant="subtitle2">
                {item?.inbound?.total.formatMoney()}đ
              </Typography>
            </Box>
          </>
        )}
      </Box>
      <Divider className={classes.divider} />
      {item?.insurance > 0 ? (
        <Box className={clsx(classes.boxDiscount, classes.slBox)}>
          <Typography variant="caption">
            {listString.IDS_TEXT_INSURANCE_TITLE}
          </Typography>
          <Typography variant="caption">{`${item.insurance.formatMoney()}đ`}</Typography>
        </Box>
      ) : null}
      {item?.voucherMoney ? (
        <Box className={clsx(classes.boxDiscount, classes.slBox)}>
          <Box className={classes.boxDiscountLeft}>
            <Typography variant="caption" className={classes.discountTxt}>
              {listString.IDS_MT_TEXT_DISCOUNT_CODE}
            </Typography>
            <BootstrapTooltip
              open={offTooltip ? true : open}
              onClose={handleClose}
              onOpen={handleOpen}
              title={item?.voucherDescription || ""}
              placement="top"
              classes={{
                tooltip: classes.customTooltip,
                arrow: classes.arrowTooltip,
              }}
            >
              <Box className={classes.discountCode}>
                <Typography variant="caption">{item.voucherCode}</Typography>
              </Box>
            </BootstrapTooltip>
          </Box>
          <Typography variant="caption" className={classes.discountPrice}>
            {`-${item.voucherMoney.formatMoney()}đ`}
          </Typography>
        </Box>
      ) : null}
      {item?.surchargeMoney > 0 ? (
        <Box className={clsx(classes.boxDiscount, classes.slBox)}>
          <Typography variant="caption">
            {listString.IDS_MT_TEXT_SUB_FEE}
          </Typography>
          <Typography variant="caption">{`${item.surchargeMoney.formatMoney()}đ`}</Typography>
        </Box>
      ) : null}
      {(item?.insurance > 0 ||
        item?.surchargeMoney > 0 ||
        item?.voucherMoney > 0) && <Divider className={classes.divider} />}
      {item?.totalPayment && (
        <Box className={classes.boxTotalPrice}>
          <Box className={classes.totalPrice}>
            <Typography variant="subtitle2">
              {isOrder
                ? listString.IDS_TEXT_ORDER_PAYMENT_TOTAL_PRICE
                : listString.IDS_MT_TEXT_TOTAL_PRICE_TICKET}
            </Typography>
            <Typography
              variant="subtitle1"
              className={classes.titleBox}
            >{`${item?.totalPayment.formatMoney()}đ`}</Typography>
          </Box>
          <Typography variant="body2">
            {listString.IDS_MT_TEXT_INCLUDE_TAX_FEE_VAT}
          </Typography>
        </Box>
      )}
    </Box>
  );
};

export default PriceDetail;
