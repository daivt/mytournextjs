import {
  getGeneralInformation,
  getTicketDetail,
  getVoucherListPayment,
  validateVoucherCode,
} from "@api/flight";
import FlightTicketDetail from "@components/common/card/flight/FlightTicketDetail";
import Coupon from "@components/desktop/flight/payment/Coupon";
import PaymentMethod from "@components/desktop/flight/payment/PaymentMethod";
import PriceDetail from "@components/desktop/flight/payment/PriceDetail";
import Layout from "@components/layout/desktop/Layout";
import TotalPriceStepTwoBox from "@components/mobile/flight/payment/TotalPriceStepTwoBox";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Divider,
  Typography,
} from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import { IconArrowDownSmall } from "@public/icons";
import snackbarSetting from "@src/alert/Alert";
import Link from "@src/link/Link";
import { BASE_URL_CONDITION_MYTOUR, listString } from "@utils/constants";
import {
  formatDetailPriceTicketData,
  getContactInfoTemp,
  getInfoGuestTemp,
  isEmpty,
} from "@utils/helpers";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import React, { useEffect, useRef, useState } from "react";
import TicketTitle from "../common/TicketTitle";

const useStyles = makeStyles((theme) => ({
  container: {
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    margin: "0 auto",
    alignItems: "flex-start",
    paddingTop: 24,
  },
  wrapLeft: {
    display: "flex",
    flexDirection: "column",
    marginRight: 24,
    width: "58%",
  },
  wrapRight: {
    display: "flex",
    width: "40%",
    position: "sticky",
    top: "84px",
  },
  stickyPrice: {
    position: "fixed",
    top: "24px",
    width: "31%",
    padding: 24,
    backgroundColor: theme.palette.white.main,
    borderRadius: 8,
  },
  boxPriceDetail: {
    padding: 24,
    backgroundColor: theme.palette.white.main,
    width: "100%",
    borderRadius: 8,
  },
  wrapText: {
    color: theme.palette.gray.grayDark8,
  },
  linkTerm: {
    color: theme.palette.blue.blueLight8,
    padding: "0 4px",
  },
  divider: {
    height: 1,
    color: theme.palette.gray.grayLight22,
    marginTop: 12,
  },
  collapseContainer: {
    boxShadow: "none",
    "&:before": { height: 0 },
    "&.Mui-expanded": { minHeight: 40, margin: "12px 0" },
  },
  summaryInfo: {
    minHeight: 40,
    "&.Mui-expanded": { minHeight: 40 },
    "& > .MuiAccordionSummary-content": { margin: "12px 0 8px 0" },
  },
  IconArrowToggle: {
    stroke: theme.palette.blue.blueLight8,
    marginLeft: 8,
    cursor: "pointer",
  },
}));
const PaymentMethodStepTwo = ({
  title = "",
  description = "",
  keywords = "",
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const ref = useRef(null);
  const queryData = router.query;

  const [totalMoney, setTotalMoney] = useState(0);
  const [totalSubMoney, setTotalSubMoney] = useState(0);
  const [surchargeMoney, setSurchargeMoney] = useState(0);
  const [insuranceMoney, setInsuranceMoney] = useState(0);
  const [bagMoney, setBagMoney] = useState(0);
  const [discountMoneyVoucher, setDiscountMoneyVoucher] = useState(0);
  const [descriptionVoucher, setDescriptionVoucher] = useState("");
  const [voucherUsing, setVoucherUsing] = useState({});
  const [listVouchers, setListVouchers] = useState([]);
  const [totalVoucher, setTotalVoucher] = useState(0);
  const [firstVoucher, setFirstVoucher] = useState({});
  const [inputSearchVoucher, setInputSearchVoucher] = useState("");
  const [paymentMethodChecked, setPaymentMethodChecked] = useState(0);
  const [paymentMethodCheckedText, setPaymentMethodCheckedText] = useState("");
  const [bankSelected, setBankSelect] = useState({});
  const [detailTicketOutBound, setDetailTicketOutBound] = useState([]);
  const [detailTicketInBound, setDetailTicketInBound] = useState(null);
  const [generalInfomation, setGeneralInformation] = useState();
  const [openRequire, setOpenRequire] = useState(false);
  const [openRequireInbound, setOpenRequireInbound] = useState(false);
  const [detailTicketStatus, setDetailTicketStatus] = useState(false);
  const [isFirstTimeError, setFirstTimeError] = useState(false);
  const [visibleDrawerTypeVoucher, setVisibleDrawerTypeVoucher] = useState(
    false
  );

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const handleCancelUseVoucher = (voucherUsing) => {
    if (voucherUsing?.code) {
      setVoucherUsing({});
      setDiscountMoneyVoucher(0);
    }
  };

  const actionsTicketDetail = async () => {
    let moneyTicket = 0;
    let moneyTicketPromo = 0;
    if (queryData?.ticketOutBoundId) {
      const params = {
        agencyId: queryData.agencyOutBoundId,
        requestId: queryData.requestId,
        ticketId: queryData.ticketOutBoundId,
      };
      const { data } = await getTicketDetail(params);
      if (data?.code === 400) {
        data?.message &&
          enqueueSnackbar(
            data?.message,
            snackbarSetting((key) => closeSnackbar(key), { color: "error" })
          );
        router.push({
          pathname: "/ve-may-bay/",
          query: {},
        });
      }
      setDetailTicketOutBound(data?.data);
      moneyTicket += parseInt(data?.data?.ticket?.totalPrice);
      if (data?.data?.ticket?.outbound?.ticketdetail?.discount) {
        moneyTicketPromo +=
          moneyTicket +
          parseInt(data?.data?.ticket?.outbound?.ticketdetail?.discount);
      }
    }
    if (queryData?.ticketInBoundId) {
      const params = {
        agencyId: queryData.agencyInBoundId,
        requestId: queryData.requestId,
        ticketId: queryData.ticketInBoundId,
      };
      const { data } = await getTicketDetail(params);
      setDetailTicketInBound(data?.data);
      moneyTicket += parseInt(data?.data?.ticket?.totalPrice);
      if (data?.data?.ticket?.outbound?.ticketdetail?.discount) {
        moneyTicketPromo +=
          moneyTicket +
          parseInt(data?.data?.ticket?.outbound?.ticketdetail?.discount);
      }
    }
    if (moneyTicket) setTotalMoney(moneyTicket);
    if (moneyTicketPromo) setTotalSubMoney(moneyTicketPromo);
    setDetailTicketStatus(true);
  };

  const actionsGeneralInformation = async () => {
    const { data } = await getGeneralInformation();
    setGeneralInformation(data?.data);
  };

  const actionsVoucher = async () => {
    const dataPost = {
      info: { productType: "flight" },
      isUsed: false,
      page: 1,
      module: "flight",
      size: 20,
      term: inputSearchVoucher,
    };
    const { data } = await getVoucherListPayment(dataPost);
    const firstVoucher = data?.data?.list[0];
    if (firstVoucher) {
      setVoucherUsing({
        id: firstVoucher?.id,
        code: firstVoucher?.codeDetail,
        description: firstVoucher?.rewardProgram?.title,
        expiredTo: firstVoucher?.responseData?.validTo,
        expiredFrom: firstVoucher?.responseData?.validFrom,
      });
      if (inputSearchVoucher) {
        setVisibleDrawerTypeVoucher(false);
      }
    }

    let dataTemp = [];
    const arrayVoucher = data?.data?.list;
    arrayVoucher?.forEach((value, key) => {
      dataTemp.push({
        id: value?.id,
        code: value?.codeDetail,
        description: value?.rewardProgram?.title,
        expired: value?.responseData?.validTo,
      });
    });

    if (isEmpty(listVouchers)) {
      setTotalVoucher(data?.data?.total);
      setListVouchers(dataTemp);
    }
  };

  let customerInfo = {};
  let priceInfo = {};

  const ISSERVER = typeof window === "undefined";
  if (!ISSERVER) {
    customerInfo = JSON.parse(
      localStorage.getItem(`CUSTOMER_INFO_${queryData.requestId}`)
    );
    priceInfo = JSON.parse(
      localStorage.getItem(`PRICE_INFO_${router.query.requestId}`)
    );
  }
  useEffect(() => {
    actionsTicketDetail();
    actionsGeneralInformation();
    if (priceInfo) {
      setInsuranceMoney(priceInfo?.totalPriceInsurance);
      setBagMoney(priceInfo?.bagPrice);
    }
  }, []);

  useEffect(() => {
    actionsVoucher();
  }, [inputSearchVoucher]);

  let totalGuest = 0;
  if (queryData.adultCount) {
    let numAdults = queryData.adultCount;
    let numChildren = queryData.childCount;
    let numInfants = queryData.infantCount;
    totalGuest =
      parseInt(numAdults) + parseInt(numChildren) + parseInt(numInfants);
  }

  let tempGuests = [];
  let tempContact = {};
  if (customerInfo) {
    tempGuests = getInfoGuestTemp(customerInfo, router, totalGuest);
    tempContact = getContactInfoTemp(customerInfo);
  }

  const validVoucher = async () => {
    try {
      if (voucherUsing?.code) {
        const paramCheck = {
          code: voucherUsing?.code,
          data: {
            contact: tempContact,
            guests: tempGuests,
            insuranceContact: customerInfo?.checkValidContactMail
              ? tempContact
              : null,
            paymentMethodId: paymentMethodChecked,
            point: 0,
            promoCode: "",
            tickets: {
              inbound: queryData?.ticketInBoundId
                ? {
                    agencyId: queryData.agencyInBoundId,
                    requestId: queryData.requestId,
                    ticketId: queryData.ticketInBoundId,
                  }
                : null,
              outbound: {
                agencyId: queryData.agencyOutBoundId,
                requestId: queryData.requestId,
                ticketId: queryData.ticketOutBoundId,
              },
            },
          },
          module: "flight",
          originPoint: 0,
        };

        const { data } = await validateVoucherCode(paramCheck);
        const dataValidVoucher = data;

        if (dataValidVoucher.code === 200) {
          setDiscountMoneyVoucher(dataValidVoucher?.data?.discount);
        } else {
          isFirstTimeError &&
            dataValidVoucher?.message &&
            enqueueSnackbar(
              dataValidVoucher?.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
          setFirstTimeError(true);
          setVoucherUsing({});
          setDiscountMoneyVoucher(0);
        }
      }
    } catch (error) {}
  };

  let paramsUrl = { ...queryData };
  delete paramsUrl?.slug;

  useEffect(() => {
    validVoucher();
  }, [voucherUsing]);

  let detailPriceTicket = {};
  if (!queryData || !detailTicketOutBound) {
    return <div />;
  } else {
    let totalMoneyBoxPrice = 0;
    if (priceInfo) {
      totalMoneyBoxPrice =
        totalMoney +
        insuranceMoney +
        priceInfo.bagPrice +
        surchargeMoney -
        discountMoneyVoucher;
    }

    let voucherDesc = "";
    if (voucherUsing?.code) {
      voucherDesc = (
        <Box>
          Đã áp dụng mã&nbsp;
          <b>
            {voucherUsing?.code}&nbsp;-&nbsp;{voucherUsing?.description}
          </b>
        </Box>
      );
    }

    detailPriceTicket = formatDetailPriceTicketData(
      detailTicketOutBound,
      detailTicketInBound,
      customerInfo,
      {
        totalGuest: totalGuest,
        totalMoneyInsurance: insuranceMoney,
        discountCode: voucherUsing?.code || "",
        discountDesc: voucherDesc || "",
        discountMoney: discountMoneyVoucher,
        surchargeMoney: surchargeMoney,
        totalMoneyBoxPrice: totalMoneyBoxPrice,
      }
    );
  }
  return (
    <Layout
      isHeader
      isFooterBottom
      type="bookingFlight"
      currentStep={1}
      title={title}
      description={description}
      keywords={keywords}
    >
      <Box
        bgcolor={theme.palette.gray.grayLight22}
        display="flex"
        flexDirection="column"
      >
        <Box className={classes.container}>
          <Box className={classes.wrapLeft} ref={ref} mb={12}>
            <Box
              padding={1 / 2}
              bgcolor={theme.palette.white.main}
              style={{
                boxShadow: "0px 1px 1px rgba(0, 0, 0, 0.05)",
                borderRadius: 8,
                paddingTop: 8,
              }}
            >
              <Accordion
                className={classes.collapseContainer}
                expanded={openRequire}
                onChange={() => setOpenRequire(!openRequire)}
              >
                <AccordionSummary
                  expandIcon={
                    <IconArrowDownSmall
                      className={`svgFillAll ${classes.IconArrowToggle}`}
                    />
                  }
                  className={classes.summaryInfo}
                >
                  <TicketTitle
                    isInBound={!!queryData?.returnDate}
                    paramsUrl={paramsUrl}
                    codeFlight={{
                      depart:
                        detailTicketOutBound?.ticket?.outbound?.departureCity,
                      arrival:
                        detailTicketOutBound?.ticket?.outbound?.arrivalCity,
                      codeDep:
                        detailTicketOutBound?.ticket?.outbound
                          ?.departureAirport,
                      codeArr:
                        detailTicketOutBound?.ticket?.outbound?.arrivalAirport,
                    }}
                    date={queryData?.departureDate}
                    dataQuery={queryData}
                    isSmall
                    isNoLink
                  />
                </AccordionSummary>
                <AccordionDetails className={classes.detailWrapper}>
                  <Divider className={classes.divider} />
                  <FlightTicketDetail
                    ticket={detailTicketOutBound?.ticket?.outbound}
                    data={detailTicketOutBound}
                    isTransit={
                      !!detailTicketOutBound?.ticket?.outbound?.transitTickets
                    }
                    airlineInfo={detailTicketOutBound?.agency}
                    title=""
                    isSimple={false}
                    isSeat={true}
                    isSlider
                  />
                </AccordionDetails>
              </Accordion>

              {detailTicketInBound && (
                <Box>
                  {queryData?.returnDate && (
                    <Accordion
                      className={classes.collapseContainer}
                      expanded={openRequireInbound}
                      onChange={() =>
                        setOpenRequireInbound(!openRequireInbound)
                      }
                    >
                      <AccordionSummary
                        expandIcon={
                          <IconArrowDownSmall
                            className={`svgFillAll ${classes.IconArrowToggle}`}
                          />
                        }
                        className={classes.summaryInfo}
                      >
                        <TicketTitle
                          isInBound={!!queryData?.returnDate}
                          paramsUrl={paramsUrl}
                          codeFlight={{
                            depart:
                              detailTicketInBound?.ticket?.outbound
                                ?.departureCity,
                            arrival:
                              detailTicketInBound?.ticket?.outbound
                                ?.arrivalCity,
                            codeDep:
                              detailTicketInBound?.ticket?.outbound
                                ?.departureAirport,
                            codeArr:
                              detailTicketInBound?.ticket?.outbound
                                ?.arrivalAirport,
                          }}
                          date={queryData?.returnDate}
                          dataQuery={queryData}
                          isSmall
                          isTwoWays
                          isNoLink
                        />
                      </AccordionSummary>
                      <AccordionDetails className={classes.detailWrapper}>
                        <FlightTicketDetail
                          ticket={detailTicketInBound?.ticket?.outbound}
                          data={detailTicketInBound}
                          isTransit={
                            !!detailTicketInBound?.ticket?.outbound
                              ?.transitTickets
                          }
                          airlineInfo={detailTicketInBound?.agency}
                          title=""
                          isSimple={false}
                          isSeat={true}
                          isSlider
                        />
                      </AccordionDetails>
                    </Accordion>
                  )}
                </Box>
              )}
            </Box>
            <Box mt={2} className={classes.boxPriceDetail}>
              <Coupon
                listVouchers={listVouchers}
                totalVoucher={totalVoucher}
                voucherUsing={voucherUsing}
                setVoucherUsing={setVoucherUsing}
                handleCancelUseVoucher={handleCancelUseVoucher}
                setInputSearchVoucher={setInputSearchVoucher}
                visibleDrawerType={visibleDrawerTypeVoucher}
                setVisibleDrawerType={setVisibleDrawerTypeVoucher}
              />
            </Box>
            <Box mt={2} className={classes.boxPriceDetail}>
              <PaymentMethod
                ticketOutBound={detailTicketOutBound}
                discountMoneyVoucher={discountMoneyVoucher}
                paymentMethodChecked={paymentMethodChecked}
                setPaymentMethodChecked={setPaymentMethodChecked}
                setPaymentMethodCheckedText={setPaymentMethodCheckedText}
                bankSelected={bankSelected}
                setBankSelect={setBankSelect}
                totalMoney={totalMoney}
                insuranceMoney={insuranceMoney}
                bagMoney={bagMoney}
                setSurchargeMoney={setSurchargeMoney}
                ticketStatus={detailTicketStatus}
              />
              <Box display="flex" justifyContent="flex-end" mt={3}>
                <TotalPriceStepTwoBox
                  isDesktop={true}
                  totalGuest={totalGuest}
                  totalMoney={totalMoney}
                  insuranceMoney={insuranceMoney}
                  bagMoney={bagMoney}
                  totalSubMoney={totalSubMoney}
                  voucherUsing={voucherUsing}
                  paymentMethodChecked={paymentMethodChecked}
                  paymentMethodCheckedText={paymentMethodCheckedText}
                  bankSelected={bankSelected}
                  surchargeMoney={surchargeMoney}
                  discountMoneyVoucher={discountMoneyVoucher}
                  outboundBaggageInfo={detailTicketOutBound?.ticket?.outbound}
                  inboundBaggageInfo={detailTicketInBound?.ticket?.outbound}
                />
              </Box>
              <Box mt={12 / 8} display="flex" flexDirection="column">
                <Box display="flex" justifyContent="flex-end">
                  <Typography
                    component="span"
                    variant="body2"
                    className={classes.wrapText}
                  >
                    {listString.IDS_MT_TEXT_FLIGHT_TERM_PAYMENT}
                  </Typography>
                </Box>
                <Box display="flex" justifyContent="flex-end">
                  <Link
                    href={BASE_URL_CONDITION_MYTOUR}
                    className={classes.linkTerm}
                  >
                    <Typography component="span" variant="body2">
                      {listString.IDS_MT_TEXT_PAYMENT_METHOD_3_DES_6}
                    </Typography>
                  </Link>
                  <Typography
                    component="span"
                    variant="body2"
                    className={classes.wrapText}
                  >
                    {listString.IDS_MT_TEXT_PAYMENT_METHOD_3_DES_7}
                  </Typography>
                </Box>
              </Box>
            </Box>
          </Box>
          <Box className={classes.wrapRight} mb={2}>
            <Box className={classes.boxPriceDetail}>
              <PriceDetail item={detailPriceTicket} />
            </Box>
          </Box>
        </Box>
      </Box>
    </Layout>
  );
};

export default PaymentMethodStepTwo;
