import { getPaymentMethodData } from "@api/flight";
import { Accordion, Box, Typography, withStyles } from "@material-ui/core";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import Checkbox from "@material-ui/core/Checkbox";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import {
  IconHoldTicket,
  IconMethodAtmCard,
  IconMethodBankTransfer,
  IconQRPay,
  IconRadio,
  IconRadioActive,
} from "@public/icons";
import snackbarSetting from "@src/alert/Alert";
import Image from "@src/image/Image";
import {
  listString,
  PAYMENT_ATM_CODE,
  PAYMENT_HOLDING_CODE,
  PAYMENT_TRANSFER_CODE,
  PAYMENT_VISA_CODE,
  PAYMENT_VNPAY_CODE,
  listEventFlight,
  listIcons,
  prefixUrlIcon,
} from "@utils/constants";
import { secondToString, getSurchargeMoney, isEmpty } from "@utils/helpers";
import clsx from "clsx";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { useEffect, useState } from "react";
import * as gtm from "@utils/gtm";
const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "column",
  },
  paymentItem: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight23}`,
    display: "flex",
    flexDirection: "row",
    padding: "16px 0",
    boxShadow: "none",
    flexDirection: "column",
    "&:before": { height: 0 },
    "& .MuiAccordionSummary-content": {
      minHeight: 40,
      margin: 0,
      width: "100%",
    },
    "& > .MuiAccordionSummary-root": { padding: 0, minHeight: 0 },
    "& .MuiAccordionDetails-root": { padding: 0 },
    "& .MuiAccordion-rounded": { borderRadius: 0 },
    "& > .MuiCollapse-container": {
      width: "100%",
    },
  },
  paymentIcon: {},
  paymentCenter: {
    width: "100%",
  },
  paymentDesc: {
    width: "76%",
  },
  paymentRadioCheck: {
    width: "24%",
    display: "flex",
    justifyContent: "flex-end",
  },
  paymentHoldTime: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: theme.palette.orange.orangeLight1,
    color: theme.palette.orange.main,
    padding: "4px 6px",
    borderRadius: 4,
    width: "40%",
  },
  noteFee: {
    color: theme.palette.gray.grayDark7,
    paddingTop: 6,
  },
  partnerLogo: {
    width: 48,
    height: 24,
    objectFit: "scale-down",
    borderRadius: 4,
    backgroundColor: theme.palette.white.main,
  },
  bankGroup: {
    display: "flex",
    flexWrap: "wrap",
    width: "100%",
    marginLeft: -4,
  },
  bankItem: {
    width: "calc(20% - 8px)",
    height: 52,
    padding: 10,
    border: "1px solid #E2E8F0",
    borderRadius: 8,
    margin: 4,
    cursor: "pointer",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    "&:hover": { border: "1px solid #00B6F3" },
  },
  activeItem: {
    border: `1px solid ${theme.palette.blue.blueLight8}`,
    "&:hover": {
      border: `1px solid ${theme.palette.blue.blueLight8}`,
    },
  },
  hideBank: {
    display: "none",
  },
  bankList: {
    height: 400,
    overflow: "auto",
  },
}));

const PaymentRadio = withStyles((theme) => ({
  root: {
    padding: 0,
    "&:hover": {
      backgroundColor: theme.palette.white.main,
    },
  },
  checked: {},
}))((props) => (
  <Checkbox
    color="default"
    {...props}
    icon={<IconRadio />}
    checkedIcon={props.checked ? <IconRadioActive /> : <div></div>}
  />
));

const listTypeModal = {
  MODAL_ATM: "MODAL_ATM",
  MODAL_BT: "MODAL_BT",
};

const PaymentMethod = ({
  ticketOutBound,
  discountMoneyVoucher,
  paymentMethodChecked,
  setPaymentMethodChecked,
  setPaymentMethodCheckedText,
  setBankSelect,
  bankSelected,
  insuranceMoney,
  totalMoney,
  setSurchargeMoney,
  bagMoney,
  ticketStatus,
}) => {
  const theme = useTheme();
  const classes = useStyles();
  const router = useRouter();
  let [dataMethod, setDataMethod] = useState([]);
  const [bankList, setBankList] = useState([]);
  const [bankListTransfer, setBankListTransfer] = useState([]);
  const [loadingDone, setLoadingDone] = useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const handleSelectBank = (bank = {}) => {
    setBankSelect(bank);
  };
  // Loại bỏ tiền giảm trừ voucher khi lấy phụ thu
  totalMoney -= discountMoneyVoucher;
  totalMoney += insuranceMoney;
  totalMoney += bagMoney;

  const getPaymentMethod = async () => {
    let dataPost = {
      supportCOD: true,
      tickets: {
        outbound: {
          agencyId: router?.query?.agencyOutBoundId,
          requestId: router?.query?.requestId,
          ticketId: router?.query?.ticketOutBoundId,
        },
      },
    };
    if (router?.query?.ticketInBoundId) {
      dataPost.tickets = {
        ...dataPost.tickets,
        inbound: {
          agencyId: router?.query?.agencyInBoundId,
          requestId: router?.query?.requestId,
          ticketId: router?.query?.ticketInBoundId,
        },
      };
    }

    const { data } = await getPaymentMethodData(dataPost);
    if (data?.code !== 200) {
      data?.message &&
        enqueueSnackbar(
          data?.message,
          snackbarSetting((key) => closeSnackbar(key), { color: "error" })
        );
    } else {
      let dataTemps = [];
      const arrayMethod = data?.data;
      let dataHoldingMethod = {};
      arrayMethod.forEach((value, key) => {
        if (value?.code === PAYMENT_HOLDING_CODE) {
          dataHoldingMethod = value;
        }
        switch (value?.code) {
          case PAYMENT_ATM_CODE:
            setBankList(value?.bankList);
            return dataTemps.push(value);
          case PAYMENT_VISA_CODE:
            return dataTemps.push(value);
          case PAYMENT_VNPAY_CODE:
            return dataTemps.push(value);
          case PAYMENT_TRANSFER_CODE:
            setBankListTransfer(value?.bankList);
            return dataTemps.push(value);
          default:
            return {};
        }
      });
      if (!isEmpty(dataHoldingMethod)) {
        dataTemps.push(dataHoldingMethod);
      }
      setPaymentMethodChecked(dataTemps[0]?.id);
      setPaymentMethodCheckedText(dataTemps[0]?.name);
      setDataMethod(dataTemps);
      setLoadingDone(true);
    }
  };

  const getIconMethod = (methodCode) => {
    switch (methodCode) {
      case PAYMENT_HOLDING_CODE:
        return <IconHoldTicket />;
        break;
      case PAYMENT_ATM_CODE:
        return <IconMethodAtmCard />;
        break;
      case PAYMENT_VNPAY_CODE:
        return <IconQRPay />;
        break;
      case PAYMENT_VISA_CODE:
        return (
          <img
            src={`${prefixUrlIcon}${listIcons.IconMethodVisa}`}
            alt="logo_payment_visa"
            style={{ width: 32, height: 32 }}
          />
        );
        break;
      case PAYMENT_TRANSFER_CODE:
        return <IconMethodBankTransfer />;
        break;

      default:
        break;
    }
  };

  useEffect(() => {
    getPaymentMethod();
  }, []);

  const handleChangeMethod = (method) => {
    gtm.addEventGtm(listEventFlight.FlightAddPaymentInfo);
    if (
      method?.code !== PAYMENT_ATM_CODE &&
      method?.code !== PAYMENT_TRANSFER_CODE
    ) {
      setBankSelect({});
    }
    dataMethod.map((item, index) => {
      if (item?.id === method?.id) {
        const moneySur = getSurchargeMoney(
          totalMoney,
          item?.confirmFee,
          item?.percentFee,
          item?.fixedFee
        );
        setSurchargeMoney(moneySur);
      }
    });
    setPaymentMethodChecked(method?.id);
    setPaymentMethodCheckedText(method?.name);
  };

  const recheckSurchargeMoney = () => {
    if (dataMethod.length) {
      let moneySurDefault = getSurchargeMoney(
        totalMoney,
        dataMethod[0]?.confirmFee,
        dataMethod[0]?.percentFee,
        dataMethod[0]?.fixedFee
      );

      setSurchargeMoney(moneySurDefault);
    }
  };

  useEffect(() => {
    if (loadingDone) {
      recheckSurchargeMoney();
    }
  }, [ticketStatus, loadingDone, discountMoneyVoucher]);

  return (
    <>
      <Box className={classes.wrapper}>
        <Box>
          <Typography variant="subtitle1">
            {listString.IDS_MT_TEXT_PAYMENT_MENTHODS}
          </Typography>
        </Box>
        <Box
          color={theme.palette.green.greenLight7}
          pt={1}
          pb={2}
          borderBottom={`solid 1px ${theme.palette.gray.grayLight22}`}
        >
          <Typography variant="caption">
            {listString.IDS_TEXT_SEND_MAIL_SMS}
          </Typography>
        </Box>
        <Box className={classes.paymentList}>
          {dataMethod.map((item, index) => (
            <Box key={index}>
              <Accordion
                className={classes.paymentItem}
                expanded={paymentMethodChecked === item.id}
                onClick={() => handleChangeMethod(item)}
              >
                <AccordionSummary className={classes.paymentCenter}>
                  <Box className={classes.paymentIcon}>
                    {getIconMethod(item?.code)}
                  </Box>
                  <Box className={classes.paymentDesc} pl={2}>
                    <Typography variant="body1">{item?.name}</Typography>

                    {item?.code === PAYMENT_HOLDING_CODE ? (
                      <>
                        <Box color={theme.palette.green.greenLight7} mt={6 / 8}>
                          <Typography variant="caption">
                            {listString.IDS_MT_TEXT_HOLDING_TICKET_PAY_LATER}
                          </Typography>
                        </Box>
                        <Box className={classes.paymentHoldTime} mt={6 / 8}>
                          <Typography variant="caption">
                            Giữ chỗ trong{" "}
                            {secondToString(
                              ticketOutBound?.ticket?.outbound?.ticketdetail
                                ?.holdingTime / 1000
                            )}
                          </Typography>
                        </Box>
                      </>
                    ) : (
                      <>
                        <Box className={classes.noteFee}>
                          {getSurchargeMoney(
                            totalMoney,
                            item?.confirmFee,
                            item?.percentFee,
                            item?.fixedFee
                          ) > 0 ? (
                            <Typography variant="body2">
                              {listString.IDS_MT_TEXT_SUB_FEE +
                                " " +
                                getSurchargeMoney(
                                  totalMoney,
                                  item?.confirmFee,
                                  item?.percentFee,
                                  item?.fixedFee
                                ).formatMoney() +
                                "đ"}
                            </Typography>
                          ) : (
                            <Box color={theme.palette.green.greenLight7}>
                              <Typography variant="body2">
                                {listString.IDS_MT_TEXT_FREE_SURCHANGE}
                              </Typography>
                            </Box>
                          )}
                        </Box>
                      </>
                    )}
                  </Box>
                  <Box className={classes.paymentRadioCheck}>
                    <PaymentRadio
                      checked={item?.id === paymentMethodChecked}
                      name="paymentCheck"
                    />
                  </Box>
                </AccordionSummary>

                {item?.code === PAYMENT_ATM_CODE && !isEmpty(bankList) && (
                  <AccordionDetails>
                    <Box>
                      <Box
                        color={theme.palette.black.black3}
                        pb={12 / 8}
                        pt={2}
                      >
                        <Typography variant="subtitle1">
                          {listString.IDS_MT_TEXT_SELECT_BANK}
                        </Typography>
                      </Box>
                      <Box className={classes.bankGroup}>
                        {bankList.map((el) => (
                          <Box
                            key={el.id}
                            className={clsx(
                              classes.bankItem,
                              bankSelected.id === el.id && classes.activeItem
                            )}
                            onClick={() => handleSelectBank(el)}
                          >
                            <Image
                              srcImage={el.logoUrl}
                              style={{ width: "100%", height: "100%" }}
                            />
                          </Box>
                        ))}
                      </Box>
                    </Box>
                  </AccordionDetails>
                )}

                {item?.code === PAYMENT_TRANSFER_CODE &&
                  !isEmpty(bankListTransfer) && (
                    <AccordionDetails>
                      <Box width="100%">
                        <Box color={theme.palette.black.black3} pb={12 / 8}>
                          <Typography variant="subtitle1">
                            {listString.IDS_MT_TEXT_SELECT_BANK}
                          </Typography>
                        </Box>
                        <Box className={classes.bankGroup} width="100%">
                          {bankListTransfer.map((el) => (
                            <Box
                              key={el.id}
                              className={clsx(
                                classes.bankItem,
                                bankSelected.id === el.id && classes.activeItem
                              )}
                              onClick={() => handleSelectBank(el)}
                            >
                              <img
                                src={el.logoUrl}
                                style={{
                                  width: "auto",
                                  height: "auto",
                                  maxWidth: 94,
                                  maxHeight: 28,
                                }}
                              />
                            </Box>
                          ))}
                        </Box>
                      </Box>
                    </AccordionDetails>
                  )}

                {item?.code === PAYMENT_VISA_CODE && (
                  <AccordionDetails>
                    <Box pl={6} pt={2}>
                      <img src="/images/image_visa_master_card.png" />
                    </Box>
                  </AccordionDetails>
                )}
              </Accordion>
            </Box>
          ))}
        </Box>
      </Box>
    </>
  );
};

export default PaymentMethod;
