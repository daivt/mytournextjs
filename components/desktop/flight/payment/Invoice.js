import { getEnterpriseInfo } from "@api/flight";
import BootstrapTooltip from "@components/common/bootstrapTooltip/BootstrapTooltip";
import { Box, IconButton, Typography } from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import MuiFormControlLabel from "@material-ui/core/FormControlLabel";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles, withStyles } from "@material-ui/styles";
import {
  IconCheckBox,
  IconCheckBoxActive,
  IconMinusFill,
  IconPlusFill,
  IconSearch,
} from "@public/icons";
import AccordionCustom from "@src/accordion/AccordionCustom";
import AccordionDetailsCustom from "@src/accordion/AccordionDetailsCustom";
import AccordionSummaryCustom from "@src/accordion/AccordionSummaryCustom";
import FieldTextContent from "@src/form/FieldTextContent";
import { listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import { validTaxCodeRegex } from "@utils/regex";
import { useFormikContext } from "formik";
import { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  container: {
    alignItems: "center",
  },
  boxTitle: {
    color: theme.palette.black.black3,
  },
  boxLeft: {
    display: "flex",
    flexDirection: "column",
    padding: 16,
  },
  boxLeftUp: {
    display: "flex",
    alignItems: "center",
  },
  boxLeftDown: {
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    maxWidth: "250px",
  },
  pencil: {
    stroke: theme.palette.blue.blueLight8,
  },
  boxRight: {
    textAlign: "right",
  },
  iconButton: {
    padding: "0 10px",
  },
  swBtn: {
    padding: 0,
  },
  summaryCustom: {
    margin: 0,
  },
  wrapContainer: {
    padding: "8px 24px",
    background: theme.palette.white.main,
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    boxSizing: "border-box",
    boxShadow: "0px 1px 1px rgba(0, 0, 0, 0.05)",
    borderRadius: "8px !important",
    marginTop: 0,
  },
  expandedCustom: {},
  wrapDetail: {
    marginTop: -8,
  },
  customTooltip: {
    maxWidth: 390,
    maxHeight: 315,
    background: theme.palette.white.main,
    color: theme.palette.black.black3,
    padding: 24,
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    boxSizing: "border-box",
    boxShadow: "0px 8px 8px rgba(0, 0, 0, 0.1)",
    borderRadius: 8,
  },
  arrowTooltip: {
    color: theme.palette.white.main,
  },
  iconSearchTax: {
    padding: 0,
  },
  inputStyle: {
    "& > input": {
      padding: 0,
    },
  },
}));

const FormControlLabel = withStyles((theme) => ({
  root: {
    margin: "0 0 0 -12px",
  },
  label: {
    fontSize: 14,
    fontWeight: "normal",
    lineHeight: "17px",
    marginRight: 2,
  },
}))(MuiFormControlLabel);
const InvoiceCheckbox = withStyles((theme) => ({
  root: {
    color: theme.palette.gray.grayLight25,
    "&$checked": {
      color: theme.palette.blue.blueLight8,
    },
  },
  checked: {},
}))((props) => (
  <Checkbox
    color="default"
    {...props}
    icon={<IconCheckBox />}
    checkedIcon={<IconCheckBoxActive />}
  />
));

const getStyleInput = {
  minHeight: 40,
  width: "100%",
  borderTop: "none",
  borderRight: "none",
  borderLeft: "none",
  borderRadius: 0,
};

const Invoice = () => {
  const classes = useStyles();
  const theme = useTheme();
  const [stateChecked, setStateChecked] = useState({
    checkedInvoice: false,
  });
  const { setFieldValue, setFieldError, values, errors } = useFormikContext();
  const [expanded, setExpanded] = useState("");
  const handleChangeExpand = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  const handleCheckbox = (event) => {
    setStateChecked({
      ...stateChecked,
      [event.target.name]: event.target.checked,
    });
    setFieldValue("checkValidInvoice", event.target.checked);
    if (isEmpty(values.recipientEmail)) {
      setFieldValue("recipientEmail", values.emailContact);
    }
    if (isEmpty(values.recipientName)) {
      setFieldValue("recipientName", values.nameContact);
    }
  };
  const [taxCode, setTaxCode] = useState("");
  const actionSearchTaxCode = async (inputSearch) => {
    const args = {
      taxCode: inputSearch,
    };
    const { data } = await getEnterpriseInfo(args);
    if (data.code == 200 && data?.data) {
      const resultSearch = data.data;
      setFieldValue("taxIdNumber", resultSearch?.taxCode || inputSearch);
      setFieldValue("companyName", resultSearch?.companyName);
      setFieldValue("companyAddress", resultSearch?.companyAddress);
      setFieldValue("recipientAddress", resultSearch?.recipientAddress);
      if (isEmpty(values?.recipientEmail)) {
        setFieldValue("recipientEmail", resultSearch?.recipientEmail);
      }
      if (isEmpty(values?.recipientName)) {
        setFieldValue("recipientName", resultSearch?.recipientName);
      }
    } else {
      data?.message &&
        setFieldError("taxIdNumber", listString.IDS_MT_TEXT_NOT_FOUND_TAX_CODE);
    }
  };

  const handleSearchTaxCode = () => {
    actionSearchTaxCode(taxCode);
  };
  const handleChange = (field, value) => {
    setFieldValue(field, value);
  };

  useEffect(() => {
    setStateChecked({
      checkedInvoice: values?.checkValidInvoice,
    });
    setExpanded(values?.checkValidInvoice ? "invoice" : "");
  }, [values?.checkValidInvoice]);

  const toolTipText = (
    <Box>
      <Typography variant="subtitle2" style={{ lineHeight: "17px" }}>
        {listString.IDS_TEXT_CONDITION_INVOICE_TITLE}
      </Typography>
      <ul style={{ padding: "0 16px" }}>
        <li>
          <Typography
            component="p"
            variant="caption"
            style={{ lineHeight: "17px", padding: "6px 0" }}
          >
            {listString.IDS_MT_REQUIRE_INVOICE_DES_1}
          </Typography>
        </li>
        <li>
          <Typography
            component="p"
            variant="caption"
            style={{ lineHeight: "17px", padding: "6px 0" }}
          >
            {listString.IDS_MT_REQUIRE_INVOICE_DES_2}
          </Typography>
        </li>
        <li>
          <Typography
            component="p"
            variant="caption"
            style={{ lineHeight: "17px", padding: "6px 0" }}
          >
            {listString.IDS_MT_REQUIRE_INVOICE_DES_3}
          </Typography>
        </li>
        <li>
          <Typography
            component="p"
            variant="caption"
            style={{ lineHeight: "17px", paddingTop: "6px" }}
          >
            {listString.IDS_MT_REQUIRE_INVOICE_DES_4}
          </Typography>
        </li>
      </ul>
    </Box>
  );

  return (
    <AccordionCustom
      expanded={expanded === "invoice"}
      onChange={handleChangeExpand("invoice")}
      classes={{
        root: classes.wrapContainer,
      }}
    >
      <AccordionSummaryCustom
        expandIcon={
          expanded === "invoice" ? <IconMinusFill /> : <IconPlusFill />
        }
        classes={{
          root: classes.summaryCustom,
          expanded: classes.expandedCustom,
        }}
      >
        <Typography className={classes.leftTitle} variant="subtitle1">
          {listString.IDS_MT_TEXT_INVOICE}
        </Typography>
      </AccordionSummaryCustom>
      <AccordionDetailsCustom
        classes={{
          root: classes.wrapDetail,
        }}
      >
        <Box display="flex" justifyContent="space-between" alignItems="center">
          <FormControlLabel
            control={
              <InvoiceCheckbox
                checked={stateChecked?.checkedInvoice}
                onChange={handleCheckbox}
                name="checkedInvoice"
              />
            }
            label={listString.IDS_MT_INVOICE_REQUIRE}
          />
          <BootstrapTooltip
            title={toolTipText}
            placement="bottom"
            classes={{
              tooltip: classes.customTooltip,
              arrow: classes.arrowTooltip,
            }}
          >
            <Typography
              component="span"
              variant="caption"
              style={{
                lineHeight: "17px",
                color: theme.palette.blue.blueLight8,
              }}
            >
              {listString.IDS_TEXT_CONDITION_INVOICE_TITLE}
            </Typography>
          </BootstrapTooltip>
        </Box>
        <Box display="flex" flexDirection="column" mt={1}>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
          >
            <FieldTextContent
              placeholder={listString.IDS_TEXT_PLACEHOLDER_SEARCH_TAX_CODE}
              inputStyle={getStyleInput}
              inputProps={{
                autoComplete: "off",
              }}
              name="taxIdNumber"
              value={values.taxIdNumber}
              onChange={(e) => {
                if (validTaxCodeRegex.test(e.target.value)) {
                  setTaxCode(e.target.value);
                  setFieldValue("taxIdNumber", e.target.value);
                }
              }}
              endAdornment={
                <IconButton
                  className={classes.iconSearchTax}
                  aria-label="search"
                  onClick={handleSearchTaxCode}
                >
                  <IconSearch />
                </IconButton>
              }
              className={classes.inputStyle}
              helperTextStyle={{ paddingTop: 8 }}
            />
            <FieldTextContent
              name="recipientName"
              placeholder={
                listString.IDS_TEXT_PLACEHOLDER_INVOICE_CUSTOMER_NAME
              }
              inputStyle={getStyleInput}
              inputProps={{
                autoComplete: "off",
              }}
              value={values.recipientName}
              helperTextStyle={{ paddingTop: 8 }}
              className={classes.inputStyle}
            />
          </Box>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
          >
            <FieldTextContent
              name="companyName"
              placeholder={listString.IDS_TEXT_PLACEHOLDER_INVOICE_NAME}
              inputStyle={getStyleInput}
              inputProps={{
                autoComplete: "off",
              }}
              value={values.companyName}
              onChange={(event) =>
                handleChange("companyName", event.target.value)
              }
              className={classes.inputStyle}
              helperTextStyle={{ paddingTop: 8 }}
            />
            <FieldTextContent
              name="recipientEmail"
              placeholder={listString.IDS_MT_TEXT_EMAIL}
              inputStyle={getStyleInput}
              inputProps={{
                autoComplete: "off",
              }}
              value={values.recipientEmail}
              helperTextStyle={{ paddingTop: 8 }}
              className={classes.inputStyle}
            />
          </Box>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
          >
            <FieldTextContent
              name="companyAddress"
              placeholder={listString.IDS_TEXT_PLACEHOLDER_INVOICE_ADDRESS}
              inputStyle={getStyleInput}
              inputProps={{
                autoComplete: "off",
              }}
              value={values.companyAddress}
              onChange={(event) =>
                handleChange("companyAddress", event.target.value)
              }
              className={classes.inputStyle}
              helperTextStyle={{ paddingTop: 8 }}
            />
            <FieldTextContent
              name="note"
              placeholder={listString.IDS_TEXT_PLACEHOLDER_INVOICE_NOTE}
              inputStyle={getStyleInput}
              inputProps={{
                autoComplete: "off",
              }}
              value={values.note}
              className={classes.inputStyle}
            />
          </Box>
          <Box display="flex" alignItems="center" width="50%">
            <FieldTextContent
              name="recipientAddress"
              placeholder={listString.IDS_TEXT_PLACEHOLDER_INVOICE_RECEIVER}
              inputStyle={getStyleInput}
              inputProps={{
                autoComplete: "off",
              }}
              value={values.recipientAddress}
              className={classes.inputStyle}
              helperTextStyle={{ paddingTop: 8 }}
            />
          </Box>
        </Box>
      </AccordionDetailsCustom>
    </AccordionCustom>
  );
};

export default Invoice;
