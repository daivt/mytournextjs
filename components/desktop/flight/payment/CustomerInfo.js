import CustomerInput from "@components/desktop/flight/payment/CustomerInput";
import { Box, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import { GENDER_LIST, listString } from "@utils/constants";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  boxTitle: {
    color: theme.palette.black.black3,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  boxDesc: {
    color: theme.palette.blue.blueLight8,
  },
  typeCustomer: {
    backgroundColor: theme.palette.gray.grayLight22,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    width: "80%",
  },
  boxInfo: {
    marginBottom: 24,
  },
  container: {
    backgroundColor: theme.palette.white.main,
    borderRadius: 8,
    padding: "24px 24px 0 24px",
  },
  leftTitle: {
    fontSize: 18,
    lineHeight: "22px",
  },
}));

const CustomerInfo = (props) => {
  const classes = useStyles();
  const theme = useTheme();

  const [expanded, setExpanded] = useState("panel1");

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };
  const { listInputTemp, customError } = props;

  return (
    <Box display="flex" flexDirection="column" className={classes.container}>
      <Box display="flex" mb={12 / 8}>
        <Typography className={classes.leftTitle} variant="subtitle1">
          {listString.IDS_MT_TEXT_INFO_CUSTOMER_FLIGHT}
        </Typography>
      </Box>
      <Box
        className={classes.note}
        borderRadius={8}
        bgcolor={theme.palette.blue.blueLight9}
        p={12 / 8}
      >
        <Typography className={classes.boxDesc} variant="caption">
          {listString.IDS_MT_TEXT_INFO_CUSTOMER_FLIGHT_NOTE_NO_ACCENT}
          <br />
          {listString.IDS_MT_TEXT_INFO_CUSTOMER_FLIGHT_NOTE_NAME}
        </Typography>
      </Box>
      <Box mt={2}>
        <CustomerInput
          listInputTemp={listInputTemp}
          listGender={GENDER_LIST}
          customError={customError}
        />
      </Box>
    </Box>
  );
};

export default CustomerInfo;
