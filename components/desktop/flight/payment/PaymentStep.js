import {
  Step,
  StepConnector,
  StepLabel,
  Stepper,
  Typography,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import { listString } from "@utils/constants";
import clsx from "clsx";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: 0,
  },
}));

const StepperCustom = withStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    padding: 0,
  },
}))(Stepper);

const ColorLibConnector = withStyles((theme) => ({
  active: {
    "& $line": {
      borderColor: theme.palette.blue.blueLight8,
    },
  },
  completed: {
    "& $line": {
      borderColor: theme.palette.blue.blueLight8,
    },
  },
  line: {
    height: 1,
    width: 24,
    backgroundColor: theme.palette.gray.grayDark7,
  },
}))(StepConnector);

const useStepIconComponentStyles = makeStyles((theme) => ({
  root: {
    background: theme.palette.gray.grayDark7,
    color: theme.palette.white.main,
    width: 24,
    height: 24,
    display: "flex",
    borderRadius: "50%",
    justifyContent: "center",
    alignItems: "center",
  },
  active: {
    background: theme.palette.blue.blueLight8,
  },
  completed: {
    background: theme.palette.blue.blueLight8,
  },
}));

const StepIconComponent = (props) => {
  const classes = useStepIconComponentStyles();
  const { active, completed, icon } = props;

  const stepNumbers = {
    1: 1,
    2: 2,
    3: 3,
  };
  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {stepNumbers[icon]}
    </div>
  );
};

const StepLabelCustom = withStyles((theme) => ({
  root: {
    color: theme.palette.gray.grayDark7,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  active: {
    color: `${theme.palette.blue.blueLight8} !important`,
  },
  completed: {
    color: `${theme.palette.blue.blueLight8} !important`,
  },
}))(StepLabel);

export default function PaymentStep({ currentStep = 0 }) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(currentStep);
  const steps = [
    listString.IDS_TEXT_PAYMENT_STEP_1,
    listString.IDS_TEXT_PAYMENT_STEP_2,
    listString.IDS_TEXT_PAYMENT_STEP_3,
  ];

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  return (
    <div className={classes.root}>
      <StepperCustom activeStep={activeStep} connector={<ColorLibConnector />}>
        {steps.map((label, index) => (
          <Step key={label}>
            <StepLabelCustom StepIconComponent={StepIconComponent}>
              <Typography variant="subtitle2">{label}</Typography>
            </StepLabelCustom>
          </Step>
        ))}
      </StepperCustom>
    </div>
  );
}
