import { getAllCountries } from "@api/flight";
import BirthDayField from "@components/common/BirthDayField/BirthDayField";
import DialogDirectory from "@components/common/modal/flight/DialogDirectory";
import DialogPassportPc from "@components/common/modal/flight/DialogPassportPc";
import { Box, Typography } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import { useTheme } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/Add";
import { makeStyles } from "@material-ui/styles";
import {
  IconAdultFlightPc,
  IconBabyFlightPc,
  IconChildrenFlightPc,
  IconContact,
  IconEdit,
} from "@public/icons";
import FieldSelectContent from "@src/form/FieldSingleSelectContent";
import FieldTextBaseContent from "@src/form/FieldTextBaseContent";
import { CUSTOMER_TYPE, listString } from "@utils/constants";
import { formatDateOfBirth, isEmpty } from "@utils/helpers";
import { DATE_FORMAT_BACK_END } from "@utils/moment";
import { validNameRegex } from "@utils/regex";
import { useFormikContext } from "formik";
import moment from "moment";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import voca from "voca";

const useStyles = makeStyles((theme) => ({
  boxTitle: {
    color: theme.palette.black.black3,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  boxDesc: {
    color: theme.palette.blue.blueLight8,
  },
  typeCustomer: {
    backgroundColor: theme.palette.gray.grayLight22,
    borderRadius: 1000,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: 32,
    width: 32,
  },
  boxInfo: {
    marginBottom: 24,
  },
  cusText: {
    color: theme.palette.gray.grayDark8,
    textAlign: "center",
  },
  inputField: {
    width: "100%",
  },
  boxGender: {
    width: 85,
    marginRight: 24,
  },
  hideTextFill: {
    "& .MuiFormHelperText-filled": {
      display: "none",
    },
  },
  slGender: {
    "& button": {
      display: "none",
    },
    "& .MuiInputBase-input": {
      padding: "14px 0 8px 0",
      borderBottom: `solid 1px ${theme.palette.gray.grayLight25}`,
      borderRadius: 0,
      fontSize: 14,
    },
  },
  dobBaby: {
    "& .MuiInputBase-input": {
      padding: 0,
    },
  },
  addPassport: {
    color: theme.palette.blue.blueLight8,
    padding: 0,
    "&:hover": {
      backgroundColor: theme.palette.white.main,
    },
  },
  blockPassport: {
    display: "flex",
    alignItems: "center",
    width: "100%",
  },
  editPassport: {
    marginLeft: 10,
  },
  inputBox: {
    position: "relative",
  },
  iconButton: {
    position: "absolute",
    padding: "32px 0 0 0",
    right: 0,
    top: 0,
    "&:hover": {
      backgroundColor: theme.palette.white.main,
    },
    cursor: "pointer",
  },
  brBottom: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight25}`,
  },
  textPassport: {
    color: theme.palette.black.black3,
  },
}));

const CustomerInput = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const { listInputTemp, listGender, customError } = props;
  const gender = listGender;
  const router = useRouter();
  const queryData = router.query;
  const [visibleDrawerAddPassport, setVisibleDrawerAddPassport] = useState(
    false
  );
  const { values, setFieldValue, errors, touched } = useFormikContext();
  const [activePassportModal, setActivePassportModal] = useState(0);
  const toggleDrawerPassPort = (open, index) => () => {
    setActivePassportModal(index);
    setVisibleDrawerAddPassport(open);
  };
  const [visibleDrawer, setVisibleDrawer] = useState("");
  const [directory, setDirectory] = useState([]);
  const [numberPickContact, setNumberPickContact] = useState(0);
  const [typePickContact, setTypePickContact] = useState(0);
  const [countries, setCountries] = useState({});

  const toggleDrawer = (open = false, index = 0, typePick = 0) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setNumberPickContact(index);
    setTypePickContact(typePick);
    setVisibleDrawer(open);
  };

  const handleSubmitPassport = (value, index) => () => {
    setVisibleDrawerAddPassport(false);
  };

  const handleBlurDOB = (event, field) => {
    let strDOB = event.target.value;
    strDOB = formatDateOfBirth(strDOB);
    setFieldValue(field, strDOB);
  };

  const pickContact = (contact) => {
    setFieldValue(`name_${numberPickContact}`, contact?.name);
    setFieldValue(`gender_${numberPickContact}`, contact?.gender);
    if (typePickContact === CUSTOMER_TYPE.ADULT) {
      if (queryData?.needPassport) {
        setFieldValue(`passport_${numberPickContact}`, contact?.passport);
      }
    } else {
      setFieldValue(`dob_${numberPickContact}`, contact?.dob);
    }
    setVisibleDrawer(false);
  };

  function getIconCustomer(type) {
    switch (type) {
      case CUSTOMER_TYPE.BABY:
        return <IconBabyFlightPc />;
        break;
      case CUSTOMER_TYPE.CHILDREN:
        return <IconChildrenFlightPc />;
        break;
      default:
        return <IconAdultFlightPc />;
        break;
    }
  }

  let markAdult = 1;
  let markChild = 1;
  let markBaby = 1;

  function getTextCustomer(type, getNumber) {
    switch (type) {
      case CUSTOMER_TYPE.BABY:
        return listString.IDS_MT_TEXT_BABY + ` ${markBaby++}`;
        break;
      case CUSTOMER_TYPE.CHILDREN:
        return listString.IDS_MT_TEXT_CHILDREN + ` ${markChild++}`;
        break;
      default:
        return listString.IDS_MT_TEXT_ADULT + ` ${markAdult++}`;
        break;
    }
  }

  const getCountries = async () => {
    try {
      const { data } = await getAllCountries();
      if (data?.code == 200) {
        setCountries(data?.data?.countries);
      }
    } catch (error) {}
  };

  useEffect(() => {
    const ISSERVER = typeof window === "undefined";

    if (!ISSERVER) {
      const tempDirectory = JSON.parse(localStorage.getItem(`DIRECTORY`)) || [];
      setDirectory(tempDirectory);
    }
    if (queryData?.needPassport) {
      getCountries();
    }
  }, []);

  return (
    <Box className={classes.partnerFilter}>
      {!isEmpty(listInputTemp) && (
        <>
          {listInputTemp.map((dataInput, index) => (
            <Box key={index} mb={3}>
              <Box display="flex" alignItems="center">
                <Box className={classes.typeCustomer}>
                  {getIconCustomer(dataInput?.type)}
                </Box>
                <Box className={classes.cusText} ml={1}>
                  <Typography variant="subtitle2">
                    {getTextCustomer(dataInput?.type)}
                  </Typography>
                </Box>
              </Box>
              <Box display="flex">
                <Box className={classes.boxGender} mt={12 / 8}>
                  <Box
                    component="span"
                    fontSize={12}
                    fontWeight={400}
                    pt={12 / 8}
                    color={theme.palette.gray.grayDark8}
                  >
                    {listString.IDS_MT_TEXT_GENDER}
                  </Box>
                  <FieldSelectContent
                    name={`gender_${index}`}
                    className={classes.slGender}
                    value={values[`gender_${index}`]}
                    options={gender || []}
                    getOptionLabel={(v) => v.label_1}
                    onSelectOption={(value) => {
                      setFieldValue(`gender_${index}`, value);
                    }}
                    inputStyle={{
                      minHeight: 40,
                      border: "none",
                    }}
                    formControlStyle={{
                      border: "none",
                      marginRight: 0,
                      minWidth: 85,
                    }}
                  />
                </Box>
                <Box
                  width="100%"
                  display="flex"
                  alignItems="flex-end"
                  className={classes.inputBox}
                >
                  <FieldTextBaseContent
                    name={`name_${index}`}
                    inputProps={{
                      autoComplete: "off",
                      padding: "0 8px 8px 8px",
                    }}
                    inputStyle={{}}
                    formControlStyle={{ marginRight: 0 }}
                    label={
                      values[`name_${index}`]
                        ? ""
                        : listString.IDS_MT_TEXT_CONTACT_NAME_SIMPLE
                    }
                    placeholder={listString.IDS_MT_TEXT_CONTACT_NAME}
                    value={values[`name_${index}`]}
                    onChange={(event) => {
                      if (
                        validNameRegex.test(voca.latinise(event.target.value))
                      ) {
                        setFieldValue(
                          `name_${index}`,
                          voca.latinise(event.target.value).toUpperCase()
                        );
                      }
                    }}
                    onBlur={(event) => {
                      setFieldValue(`name_${index}`, event.target.value.trim());
                    }}
                  />
                  {!isEmpty(directory) && (
                    <Box
                      className={classes.iconButton}
                      onClick={toggleDrawer(true, index, dataInput?.type)}
                    >
                      <IconContact />
                    </Box>
                  )}
                </Box>
                {dataInput?.type !== CUSTOMER_TYPE.ADULT && (
                  <Box display="flex" alignItems="flex-end">
                    <BirthDayField
                      disableFuture
                      date={
                        values[`dob_${index}`] &&
                        moment(values[`dob_${index}`], DATE_FORMAT_BACK_END)
                      }
                      update={(value) => {
                        setFieldValue(
                          `dob_${index}`,
                          moment(value).format(DATE_FORMAT_BACK_END)
                        );
                      }}
                      errorMessage={
                        !isEmpty(errors[`dob_${index}`]) &&
                        touched[`dob_${index}`]
                          ? listString.IDS_MT_TEXT_PLEASE_ENTER_DOB
                          : customError[`dob_${index}`]
                          ? customError[`dob_${index}`]
                          : ""
                      }
                      inputStyle={{
                        border: "none",
                        borderRadius: 0,
                        margin: "2px 0 0 10px",
                        padding: 0,
                        minWidth: 140,
                      }}
                    />
                  </Box>
                )}
              </Box>
              {queryData.needPassport === "true" && (
                <Box className={classes.blockPassport}>
                  <IconButton
                    className={classes.addPassport}
                    onClick={toggleDrawerPassPort(true, index)}
                  >
                    {values[`passport_${index}`] !== "" ? (
                      <Typography
                        variant="caption"
                        className={classes.textPassport}
                      >
                        {listString.IDS_MT_TEXT_PASSPORT}
                        {": "}
                        {values[`passport_${index}`]}
                      </Typography>
                    ) : (
                      <>
                        <AddIcon fontSize="small" />
                        <Typography variant="caption" style={{ marginLeft: 6 }}>
                          {listString.IDS_MT_TEXT_ADD_PASSPORT}
                        </Typography>
                      </>
                    )}
                  </IconButton>
                  {values[`passport_${index}`] !== "" && (
                    <IconEdit className={classes.editPassport} />
                  )}
                </Box>
              )}
            </Box>
          ))}
          {queryData.needPassport === "true" && (
            <DialogPassportPc
              open={visibleDrawerAddPassport}
              toggleDrawer={toggleDrawerPassPort}
              index={activePassportModal}
              handleSubmitPassport={handleSubmitPassport}
              countries={countries}
            />
          )}

          <DialogDirectory
            open={visibleDrawer}
            toggleDrawer={toggleDrawer}
            directory={directory}
            pickContact={pickContact}
          />
        </>
      )}
    </Box>
  );
};
export default CustomerInput;
