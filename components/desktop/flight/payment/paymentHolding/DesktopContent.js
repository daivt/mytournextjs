import {
  getFlightBookingDetail,
  getGeneralInformation,
  getVoucherListPayment,
  repayForHoldingBooking,
  validateVoucherCode,
} from "@api/flight";
import BootstrapTooltip from "@components/common/bootstrapTooltip/BootstrapTooltip";
import FlightTicketDetailPayment from "@components/common/card/flight/FlightTicketDetailPayment";
import Coupon from "@components/desktop/flight/payment/Coupon";
import PaymentMethodHolding from "@components/desktop/flight/payment/paymentHolding/PaymentMethodHolding";
import Layout from "@components/layout/desktop/Layout";
import { Box, Divider, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import { IconBack } from "@public/icons";
import snackbarSetting from "@src/alert/Alert";
import ButtonComponent from "@src/button/Button";
import Link from "@src/link/Link";
import utilStyles from "@styles/utilStyles";
import {
  listString,
  routeStatic,
  BASE_URL_CONDITION_MYTOUR,
} from "@utils/constants";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  container: {
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    margin: "0 auto",
    alignItems: "flex-start",
  },
  wrapStep: {
    padding: "28px 0",
    zIndex: 100,
    display: "flex",
    width: "100%",
    maxWidth: 1188,
    justifyContent: "flex-start",
    background: theme.palette.gray.grayLight22,
    position: "sticky",
    top: "60px",
    margin: "auto",
    color: theme.palette.blue.blueLight8,
    cursor: "pointer",
  },
  wrapLeft: {
    display: "flex",
    flexDirection: "column",
    marginRight: 24,
    width: "58%",
  },
  wrapRight: {
    display: "flex",
    width: "40%",
    position: "sticky",
    top: "140px",
  },
  divider: {
    height: 4,
    backgroundColor: theme.palette.gray.grayLight22,
  },
  titleTicket: {
    height: "18px",
    width: "fit-content",
    overflow: "hidden",
    alignItems: "center",
    borderRadius: "4px",
    display: "flex",
    justifyContent: "center",
    color: "white",
    backgroundColor: theme.palette.primary.main,
    fontWeight: 600,
    fontSize: "12px",
    padding: "2px 4px",
    marginBottom: 10,
  },
  boxPriceDetail: {
    padding: 24,
    backgroundColor: theme.palette.white.main,
    width: "100%",
    borderRadius: 8,
  },
  wrapText: {
    color: theme.palette.gray.grayDark8,
  },
  linkTerm: {
    color: theme.palette.blue.blueLight8,
    padding: "0 4px",
  },
  slBox: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
  },
  customTooltip: {
    maxWidth: 290,
    maxHeight: 100,
    background: theme.palette.green.greenLight7,
    padding: "8px 14px 8px 8px",
    boxSizing: "border-box",
    borderRadius: 6,
    fontSize: 14,
    lineHeight: "16px",
  },
  arrowTooltip: {
    color: theme.palette.green.greenLight7,
  },
}));

const DesktopContent = ({ title = "", description = "", keywords = "" }) => {
  const classes = useStyles();
  const theme = useTheme();
  const classesUtils = utilStyles();
  const router = useRouter();
  const queryData = router.query;

  const [totalMoney, setTotalMoney] = useState(0);
  const [grandTotal, setGrandTotal] = useState(0);
  const [surchargeMoney, setSurchargeMoney] = useState(0);
  const [discountMoneyVoucher, setDiscountMoneyVoucher] = useState(0);
  const [voucherUsing, setVoucherUsing] = useState({});
  const [listVouchers, setListVouchers] = useState([]);
  const [totalVoucher, setTotalVoucher] = useState(0);
  const [inputSearchVoucher, setInputSearchVoucher] = useState("");
  const [paymentMethodChecked, setPaymentMethodChecked] = useState(0);
  const [bankSelected, setBankSelect] = useState({});
  const [bookingInfo, setBookingInfo] = useState({});
  const [airlines, setAirlines] = useState();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const handleCancelUseVoucher = (voucherUsing) => {
    if (voucherUsing?.code) {
      setVoucherUsing({});
      setDiscountMoneyVoucher(0);
    }
  };

  const actionsGeneralInformation = async () => {
    const { data } = await getGeneralInformation();
    setAirlines(data?.data?.airlines);
  };

  const actionsTicketDetail = async () => {
    try {
      let granMoneyTicket = 0;
      const { data } = await getFlightBookingDetail({
        id: queryData?.bookingId,
      });
      if (data.code === 200) {
        setBookingInfo(data?.data || {});
        granMoneyTicket += parseInt(data?.data?.grandTotal);
        if (granMoneyTicket) setGrandTotal(granMoneyTicket);
        actionsGeneralInformation();
        actionsVoucher(data?.data?.id, data?.data?.promotionCode || "");
      } else {
        router.push({
          pathname: routeStatic.FLIGHT.href,
        });
      }
    } catch (error) {
      router.push({
        pathname: routeStatic.FLIGHT.href,
      });
    }
  };

  const actionsVoucher = async (bookId, code = "") => {
    const dataPost = {
      info: {
        bookingId: bookId,
        data: {
          isExtraServiceHandle: false,
          module: "flight",
          originPoint: 0,
        },
      },
      isUsed: false,
      page: 1,
      module: "flight",
      size: 20,
      term: inputSearchVoucher,
    };
    const { data } = await getVoucherListPayment(dataPost);
    let firstVoucher = {};
    if (code) {
      firstVoucher = data?.data?.list.find((item) => item?.codeDetail == code);
    } else {
      firstVoucher = data?.data?.list[0];
    }

    if (firstVoucher) {
      setVoucherUsing({
        id: firstVoucher?.id,
        code: firstVoucher?.codeDetail,
        description: firstVoucher?.rewardProgram?.title,
        expired: firstVoucher?.responseData?.validTo,
      });
    }

    let dataTemp = [];
    const arrayVoucher = data?.data?.list || [];
    arrayVoucher?.forEach((value, key) => {
      dataTemp.push({
        id: value?.id,
        code: value?.codeDetail,
        description: value?.rewardProgram?.title,
        expired: value?.responseData?.validTo,
      });
    });
    setTotalVoucher(data?.data?.total);
    setListVouchers(dataTemp);
  };

  const validVoucher = async (code = "") => {
    try {
      const paramCheck = {
        bookingId: queryData?.bookingId || 0,
        code: code ? code : voucherUsing?.code,
        data: {
          isExtraServiceHandle: false,
        },
        module: "flight",
        originPoint: 0,
      };
      const { data } = await validateVoucherCode(paramCheck);
      const dataValidVoucher = data;
      if (dataValidVoucher.code != 200) {
        setVoucherUsing({});
        dataValidVoucher?.data?.promotionDescription &&
          enqueueSnackbar(
            dataValidVoucher?.data?.promotionDescription,
            snackbarSetting((key) => closeSnackbar(key), { color: "error" })
          );
      } else {
        setDiscountMoneyVoucher(dataValidVoucher?.data?.discount);
      }
    } catch (error) {}
  };

  const handleRepay = async () => {
    try {
      const dataPost = {
        module: "flight",
        bookingId: queryData?.bookingId,
        paymentMethodBankId: bankSelected?.id,
        paymentMethodId: paymentMethodChecked,
        promotionCode: voucherUsing?.code,
      };
      const { data } = await repayForHoldingBooking(dataPost);

      if (data.code === 200 || data.code === "200") {
        const { paymentLink } = data.data;
        window.location.assign(paymentLink);
      } else {
        data?.message &&
          enqueueSnackbar(
            data?.message,
            snackbarSetting((key) => closeSnackbar(key), { color: "error" })
          );
      }
    } catch (error) {}
  };

  useEffect(() => {
    actionsTicketDetail();
  }, []);

  useEffect(() => {
    if (voucherUsing?.code) {
      validVoucher();
    }
  }, [voucherUsing]);

  useEffect(() => {
    setTotalMoney(grandTotal - discountMoneyVoucher + surchargeMoney);
  }, [grandTotal, discountMoneyVoucher, surchargeMoney]);

  useEffect(() => {
    if (bookingInfo?.id) {
      actionsVoucher(bookingInfo?.id);
    }
  }, [inputSearchVoucher]);

  if (!queryData?.bookingId || !bookingInfo?.outbound) return <div />;

  const getBagMoney = () => {
    let totalBagMoney = 0;
    if (bookingInfo?.guests) {
      bookingInfo?.guests.forEach((guest, idx) => {
        totalBagMoney +=
          guest?.outboundBaggage?.price ||
          0 + guest?.inboundBaggage?.price ||
          0;
      });
    }

    return totalBagMoney ? (
      <Box
        display="flex"
        justifyContent="space-between"
        padding="14px 0 12px 0"
        className={classes.slBox}
      >
        <Typography variant="caption">
          {listString.IDS_MT_TEXT_PRICE_ADD_BAGGAGE}
        </Typography>
        <Typography variant="caption">
          {totalBagMoney.formatMoney()}đ
        </Typography>
      </Box>
    ) : null;
  };

  return (
    <Layout
      isHeader
      isFooterBottom
      type="bookingFlight"
      currentStep={1}
      title={title}
      description={description}
      keywords={keywords}
    >
      <Box
        bgcolor={theme.palette.gray.grayLight22}
        display="flex"
        flexDirection="column"
      >
        <Box className={classes.wrapStep}>
          <Box
            onClick={() => {
              router.back();
            }}
          >
            <IconBack
              className="svgFillAll"
              style={{
                stroke: theme.palette.blue.blueLight8,
              }}
            />
          </Box>
          <Box ml={14 / 8}>
            <Typography variant="caption">
              {listString.IDS_MT_TEXT_RETURN_ORDER}
            </Typography>
          </Box>
        </Box>
        <Box className={classes.container}>
          <Box className={classes.wrapLeft} mb={12}>
            <Box
              padding={3}
              bgcolor={theme.palette.white.main}
              borderRadius="8px"
            >
              {bookingInfo?.isTwoWay ? (
                <>
                  <Box>
                    <Box className={classes.titleTicket}>
                      {listString.IDS_MT_TEXT_DEPARTURE.toUpperCase()}
                    </Box>
                    <FlightTicketDetailPayment
                      ticket={bookingInfo?.outbound}
                      airlines={airlines}
                    />
                  </Box>
                  <Box mt={12 / 8}>
                    <Box className={classes.titleTicket}>
                      {listString.IDS_MT_TEXT_RETURN.toUpperCase()}
                    </Box>
                    <FlightTicketDetailPayment
                      ticket={bookingInfo?.inbound}
                      airlines={airlines}
                    />
                  </Box>
                </>
              ) : (
                <Box>
                  <FlightTicketDetailPayment
                    ticket={bookingInfo?.outbound}
                    airlines={airlines}
                  />
                </Box>
              )}
            </Box>
            <Box mt={2} className={classes.boxPriceDetail}>
              <Coupon
                listVouchers={listVouchers}
                totalVoucher={totalVoucher}
                voucherUsing={voucherUsing}
                setVoucherUsing={setVoucherUsing}
                handleCancelUseVoucher={handleCancelUseVoucher}
                setInputSearchVoucher={setInputSearchVoucher}
              />
            </Box>
            <Box mt={2} className={classes.boxPriceDetail}>
              <PaymentMethodHolding
                discountMoneyVoucher={discountMoneyVoucher}
                paymentMethodChecked={paymentMethodChecked}
                setPaymentMethodChecked={setPaymentMethodChecked}
                paymentMethodSelected={bookingInfo?.paymentMethodId}
                bankSelected={bankSelected}
                setBankSelect={setBankSelect}
                grandTotal={grandTotal}
                setSurchargeMoney={setSurchargeMoney}
                bookingId={queryData?.bookingId}
              />
              <Box display="flex" justifyContent="flex-end" mt={3}>
                <ButtonComponent
                  type="submit"
                  backgroundColor={theme.palette.secondary.main}
                  height={48}
                  borderRadius={8}
                  handleClick={handleRepay}
                  width="fit-content"
                  padding="0 40px"
                >
                  <Typography variant="subtitle1">
                    {listString.IDS_TEXT_PAYMENT_STEP_2}
                  </Typography>
                </ButtonComponent>
              </Box>
              <Box mt={12 / 8} display="flex" flexDirection="column">
                <Box display="flex" justifyContent="flex-end">
                  <Typography
                    component="span"
                    variant="body2"
                    className={classes.wrapText}
                  >
                    {listString.IDS_MT_TEXT_FLIGHT_TERM_PAYMENT}
                  </Typography>
                </Box>
                <Box display="flex" justifyContent="flex-end">
                  <Link
                    href={BASE_URL_CONDITION_MYTOUR}
                    className={classes.linkTerm}
                  >
                    <Typography component="span" variant="body2">
                      {listString.IDS_MT_TEXT_PAYMENT_METHOD_3_DES_6}
                    </Typography>
                  </Link>
                  <Typography
                    component="span"
                    variant="body2"
                    className={classes.wrapText}
                  >
                    {listString.IDS_MT_TEXT_PAYMENT_METHOD_3_DES_7}
                  </Typography>
                </Box>
              </Box>
            </Box>
          </Box>
          <Box className={classes.wrapRight} mb={2}>
            <Box className={classes.boxPriceDetail}>
              <Box overflow="auto">
                <Typography
                  variant="subtitle1"
                  style={{ fontSize: 18, lineHeight: "22px" }}
                >
                  {listString.IDS_MT_TEXT_PRICE_DETAIL}
                </Typography>
                <Box
                  display="flex"
                  justifyContent="space-between"
                  padding="14px 0 12px 0"
                  className={classes.slBox}
                >
                  <Typography variant="subtitle2">
                    {listString.IDS_MT_TEXT_TOTAL_PRICE_OUTBOUND}
                  </Typography>
                  <Typography variant="subtitle2">
                    {bookingInfo?.outbound?.price.formatMoney()}đ
                  </Typography>
                </Box>
                {bookingInfo?.isTwoWay && (
                  <Box
                    display="flex"
                    justifyContent="space-between"
                    padding="14px 0 12px 0"
                    className={classes.slBox}
                  >
                    <Typography variant="subtitle2">
                      {listString.IDS_MT_TEXT_TOTAL_PRICE_INBOUND}
                    </Typography>
                    <Typography variant="subtitle2">
                      {bookingInfo?.inbound?.price.formatMoney()}đ
                    </Typography>
                  </Box>
                )}
                <Divider className={classes.divider} />
                {getBagMoney()}
                {bookingInfo?.insuranceAmount ? (
                  <Box
                    display="flex"
                    justifyContent="space-between"
                    padding="14px 0 12px 0"
                    className={classes.slBox}
                  >
                    <Typography
                      variant="caption"
                      style={{ lineHeight: "17px" }}
                    >
                      {listString.IDS_TEXT_INSURANCE_TITLE}
                    </Typography>
                    <Typography
                      variant="caption"
                      style={{ lineHeight: "17px" }}
                    >{`${bookingInfo?.insuranceAmount.formatMoney()}đ`}</Typography>
                  </Box>
                ) : null}
                {discountMoneyVoucher ? (
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                    padding="14px 0 12px 0"
                    className={classes.slBox}
                  >
                    <Box display="flex" alignItems="center">
                      <Typography
                        variant="caption"
                        style={{ lineHeight: "17px", marginRight: "8px" }}
                      >
                        {listString.IDS_MT_TEXT_DISCOUNT_CODE}
                      </Typography>
                      <BootstrapTooltip
                        title={
                          voucherUsing?.code ? (
                            <Box>
                              Đã áp dụng mã&nbsp;
                              <b>
                                {voucherUsing?.code}&nbsp;-&nbsp;
                                {voucherUsing?.description}
                              </b>
                            </Box>
                          ) : (
                            ""
                          )
                        }
                        placement="top"
                        classes={{
                          tooltip: classes.customTooltip,
                          arrow: classes.arrowTooltip,
                        }}
                      >
                        <Typography
                          variant="caption"
                          style={{
                            lineHeight: "17px",
                            marginRight: "8px",
                            padding: "4px 6px",
                            border: `1px solid ${theme.palette.gray.grayLight24}`,
                            boxSizing: "border-box",
                            borderRadius: "4px",
                          }}
                        >
                          {voucherUsing?.code}
                        </Typography>
                      </BootstrapTooltip>
                    </Box>
                    <Typography
                      variant="caption"
                      style={{
                        lineHeight: "17px",
                        color: theme.palette.green.greenLight7,
                      }}
                    >
                      {`-${discountMoneyVoucher.formatMoney()}đ`}
                    </Typography>
                  </Box>
                ) : null}
                <Box
                  display="flex"
                  justifyContent="space-between"
                  padding="14px 0 12px 0"
                  className={classes.slBox}
                >
                  <Typography variant="caption">
                    {listString.IDS_MT_TEXT_SUB_FEE}
                  </Typography>
                  {surchargeMoney && paymentMethodChecked !== 119 ? (
                    <Typography variant="caption">{`${surchargeMoney.formatMoney()}đ`}</Typography>
                  ) : paymentMethodChecked === 119 ? (
                    <Typography
                      variant="caption"
                      style={{
                        lineHeight: "17px",
                        color: theme.palette.green.greenLight7,
                      }}
                    >
                      {listString.IDS_MT_TEXT_FREE_SURCHANGE}
                    </Typography>
                  ) : null}
                </Box>

                <Box paddingTop="14px" display="flex" flexDirection="column">
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                  >
                    <Typography variant="subtitle2">
                      {listString.IDS_MT_TEXT_TOTAL_PRICE_TICKET}
                    </Typography>
                    <Typography
                      variant="subtitle1"
                      className={classes.titleBox}
                    >{`${totalMoney.formatMoney()}đ`}</Typography>
                  </Box>
                  <Typography variant="body2">
                    {listString.IDS_MT_TEXT_INCLUDE_TAX_FEE_VAT}
                  </Typography>
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </Layout>
  );
};

export default DesktopContent;
