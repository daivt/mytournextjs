import { getPaymentMethodOfHoldingBooking } from "@api/flight";
import { Accordion, Box, Typography, withStyles } from "@material-ui/core";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import Checkbox from "@material-ui/core/Checkbox";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import {
  IconMethodAtmCard,
  IconMethodBankTransfer,
  IconQRPay,
  IconRadio,
  IconRadioActive,
} from "@public/icons";
import snackbarSetting from "@src/alert/Alert";
import Image from "@src/image/Image";
import {
  flightPaymentMethodCode,
  listIcons,
  listString,
  prefixUrlIcon,
} from "@utils/constants";
import { getSurchargeMoney, isEmpty, secondToString } from "@utils/helpers";
import clsx from "clsx";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "column",
  },
  paymentItem: {
    borderBottom: `solid 1px ${theme.palette.gray.grayLight23}`,
    display: "flex",
    flexDirection: "row",
    padding: "16px 0",
    boxShadow: "none",
    flexDirection: "column",
    "&:before": { height: 0 },
    "& .MuiAccordionSummary-content": {
      minHeight: 40,
      margin: 0,
      width: "100%",
    },
    "& > .MuiAccordionSummary-root": { padding: 0, minHeight: 0 },
    "& .MuiAccordionDetails-root": { padding: 0 },
    "& .MuiAccordion-rounded": { borderRadius: 0 },
    "& > .MuiCollapse-container": {
      width: "100%",
    },
  },
  paymentIcon: {},
  paymentCenter: {
    width: "100%",
  },
  paymentDesc: {
    width: "76%",
  },
  paymentRadioCheck: {
    width: "24%",
    display: "flex",
    justifyContent: "flex-end",
  },
  paymentHoldTime: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: theme.palette.orange.orangeLight1,
    color: theme.palette.orange.main,
    padding: "4px 6px",
    borderRadius: 4,
    width: "40%",
  },
  noteFee: {
    color: theme.palette.gray.grayDark7,
    paddingTop: 6,
  },
  partnerLogo: {
    width: 48,
    height: 24,
    objectFit: "scale-down",
    borderRadius: 4,
    backgroundColor: theme.palette.white.main,
  },
  bankGroup: {
    display: "flex",
    flexWrap: "wrap",
    width: "100%",
    marginLeft: -4,
  },
  bankItem: {
    width: "calc(20% - 8px)",
    height: 52,
    padding: "12px 10px",
    border: "1px solid #E2E8F0",
    borderRadius: 8,
    margin: 4,
    cursor: "pointer",
    "&:hover": {
      border: `1px solid ${theme.palette.blue.blueLight8}`,
    },
  },
  activeItem: {
    border: `1px solid ${theme.palette.blue.blueLight8}`,
    "&:hover": {
      border: `1px solid ${theme.palette.blue.blueLight8}`,
    },
  },
  hideBank: {
    display: "none",
  },
  bankList: {
    height: 400,
    overflow: "auto",
  },
}));

const PaymentRadio = withStyles((theme) => ({
  root: {
    padding: 0,
    "&:hover": {
      backgroundColor: theme.palette.white.main,
    },
  },
  checked: {},
}))((props) => (
  <Checkbox
    color="default"
    {...props}
    icon={<IconRadio />}
    checkedIcon={props.checked ? <IconRadioActive /> : <div></div>}
  />
));

const listTypeModal = {
  MODAL_ATM: "MODAL_ATM",
  MODAL_BT: "MODAL_BT",
};

const PaymentMethodHolding = ({
  paymentMethodChecked,
  setPaymentMethodChecked,
  paymentMethodSelected,
  setBankSelect,
  bankSelected,
  grandTotal,
  setSurchargeMoney,
  bookingId,
  discountMoneyVoucher,
}) => {
  const theme = useTheme();
  const classes = useStyles();
  const router = useRouter();
  const [dataMethod, setDataMethod] = useState([]);
  const [bankList, setBankList] = useState([]);
  const [bankListTransfer, setBankListTransfer] = useState([]);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  useEffect(() => {
    getPaymentMethod();
  }, []);
  const totalMoney = grandTotal - discountMoneyVoucher;
  useEffect(() => {
    if (dataMethod.length) {
      recheckSurchargeMoney();
    }
  }, [discountMoneyVoucher, dataMethod]);

  const handleSelectBank = (bank = {}) => {
    setBankSelect(bank);
  };

  const getPaymentMethod = async () => {
    const dataPost = {
      module: "flight",
      bookingId: bookingId,
    };

    const { data } = await getPaymentMethodOfHoldingBooking(dataPost);
    if (data?.code === "200" || data?.code === 200) {
      let dataTemps = [];
      const arrayMethod = data?.data;
      arrayMethod.forEach((value, key) => {
        switch (value?.code) {
          case flightPaymentMethodCode.ATM:
            setBankList(value?.bankList);
            return dataTemps.push(value);
          case flightPaymentMethodCode.VISA_MASTER:
            return dataTemps.push(value);
          case flightPaymentMethodCode.QR_CODE:
            return dataTemps.push(value);
          case flightPaymentMethodCode.BANK_TRANSFER:
          case flightPaymentMethodCode.BANK_TRANSFER_2:
            setBankListTransfer(value?.bankList);
            return dataTemps.push(value);
          default:
            return {};
        }
      });
      if (paymentMethodSelected === 121 || paymentMethodSelected === 119) {
        if (paymentMethodSelected === 119) {
          dataTemps = dataTemps.filter((item) => item.id !== 119);
        }
        setPaymentMethodChecked(dataTemps[0]?.id);
      } else {
        setPaymentMethodChecked(paymentMethodSelected);
      }
      setDataMethod(dataTemps);
    } else {
      data?.message &&
        enqueueSnackbar(
          data?.message,
          snackbarSetting((key) => closeSnackbar(key), { color: "error" })
        );
      router.back();
    }
  };

  const getIconMethod = (methodCode) => {
    switch (methodCode) {
      case flightPaymentMethodCode.ATM:
        return <IconMethodAtmCard />;
      case flightPaymentMethodCode.QR_CODE:
        return <IconQRPay />;
      case flightPaymentMethodCode.VISA_MASTER:
        return (
          <img
            src={`${prefixUrlIcon}${listIcons.IconMethodVisa}`}
            alt="logo_payment_visa"
            style={{ width: 32, height: 32 }}
          />
        );
      case flightPaymentMethodCode.BANK_TRANSFER:
      case flightPaymentMethodCode.BANK_TRANSFER_2:
        return <IconMethodBankTransfer />;
      default:
        break;
    }
  };

  const handleChangeMethod = (method) => {
    if (
      method?.code !== flightPaymentMethodCode.ATM &&
      method?.code !== flightPaymentMethodCode.BANK_TRANSFER &&
      method?.code !== flightPaymentMethodCode.BANK_TRANSFER_2
    ) {
      setBankSelect({});
    }
    dataMethod.map((item, index) => {
      if (item?.id === method?.id) {
        const moneySur = getSurchargeMoney(
          totalMoney,
          item?.confirmFee,
          item?.percentFee,
          item?.fixedFee
        );
        setSurchargeMoney(moneySur);
      }
    });
    setPaymentMethodChecked(method?.id);
  };

  const recheckSurchargeMoney = () => {
    if (dataMethod.length) {
      const moneySurDefault = getSurchargeMoney(
        totalMoney,
        dataMethod[0]?.confirmFee,
        dataMethod[0]?.percentFee,
        dataMethod[0]?.fixedFee
      );
      setSurchargeMoney(moneySurDefault);
    }
  };

  return (
    <>
      <Box className={classes.wrapper}>
        <Box>
          <Typography variant="subtitle1">
            {listString.IDS_MT_TEXT_PAYMENT_MENTHODS}
          </Typography>
        </Box>
        <Box
          color={theme.palette.green.greenLight7}
          pt={1}
          pb={2}
          borderBottom={`solid 1px ${theme.palette.gray.grayLight22}`}
        >
          <Typography variant="caption">
            {listString.IDS_TEXT_SEND_MAIL_SMS}
          </Typography>
        </Box>
        <Box className={classes.paymentList}>
          {dataMethod.map((item, index) => (
            <Box key={index}>
              <Accordion
                className={classes.paymentItem}
                expanded={paymentMethodChecked === item.id}
                onClick={() => handleChangeMethod(item)}
              >
                <AccordionSummary className={classes.paymentCenter}>
                  <Box className={classes.paymentIcon}>
                    {getIconMethod(item?.code)}
                  </Box>
                  <Box className={classes.paymentDesc} pl={2}>
                    <Typography variant="body1">{item?.name}</Typography>

                    {item?.code === flightPaymentMethodCode.HOLDING ? (
                      <>
                        <Box color={theme.palette.green.greenLight7} mt={6 / 8}>
                          <Typography variant="caption">
                            {listString.IDS_MT_TEXT_HOLDING_TICKET_PAY_LATER}
                          </Typography>
                        </Box>
                        <Box className={classes.paymentHoldTime} mt={6 / 8}>
                          <Typography variant="caption">
                            Giữ chỗ trong{" "}
                            {secondToString(
                              ticketOutBound?.ticket?.outbound?.ticketdetail
                                ?.holdingTime / 1000
                            )}
                          </Typography>
                        </Box>
                      </>
                    ) : (
                      <>
                        <Box className={classes.noteFee}>
                          <Typography variant="body2">
                            {getSurchargeMoney(
                              totalMoney,
                              item?.confirmFee,
                              item?.percentFee,
                              item?.fixedFee
                            ) > 0 ? (
                              listString.IDS_MT_TEXT_SUB_FEE +
                              " " +
                              getSurchargeMoney(
                                totalMoney,
                                item?.confirmFee,
                                item?.percentFee,
                                item?.fixedFee
                              ).formatMoney() +
                              "đ"
                            ) : (
                              <Box color={theme.palette.green.greenLight7}>
                                {listString.IDS_MT_TEXT_FREE_SURCHANGE}
                              </Box>
                            )}
                          </Typography>
                        </Box>
                      </>
                    )}
                  </Box>
                  <Box className={classes.paymentRadioCheck}>
                    <PaymentRadio
                      checked={item?.id === paymentMethodChecked}
                      name="paymentCheck"
                    />
                  </Box>
                </AccordionSummary>

                {item?.code === flightPaymentMethodCode.ATM &&
                  !isEmpty(bankList) && (
                    <AccordionDetails>
                      <Box>
                        <Box
                          color={theme.palette.black.black3}
                          pb={12 / 8}
                          pt={2}
                        >
                          <Typography variant="subtitle1">
                            {listString.IDS_MT_TEXT_SELECT_BANK}
                          </Typography>
                        </Box>
                        <Box className={classes.bankGroup}>
                          {bankList.map((el) => (
                            <Box
                              key={el.id}
                              className={clsx(
                                classes.bankItem,
                                bankSelected.id === el.id && classes.activeItem
                              )}
                              onClick={() => handleSelectBank(el)}
                            >
                              <Image
                                srcImage={el.logoUrl}
                                style={{ width: "100%", height: "100%" }}
                              />
                            </Box>
                          ))}
                        </Box>
                      </Box>
                    </AccordionDetails>
                  )}
                {item?.code === flightPaymentMethodCode.BANK_TRANSFER ||
                  (item?.code === flightPaymentMethodCode.BANK_TRANSFER_2 &&
                    !isEmpty(bankListTransfer) && (
                      <AccordionDetails>
                        <Box width="100%">
                          <Box
                            color={theme.palette.black.black3}
                            pb={12 / 8}
                            pt={2}
                          >
                            <Typography variant="subtitle1">
                              {listString.IDS_MT_TEXT_SELECT_BANK}
                            </Typography>
                          </Box>
                          <Box className={classes.bankGroup}>
                            {bankListTransfer.map((el) => (
                              <Box
                                key={el.id}
                                className={clsx(
                                  classes.bankItem,
                                  bankSelected.id === el.id &&
                                    classes.activeItem
                                )}
                                onClick={() => handleSelectBank(el)}
                              >
                                <Image
                                  srcImage={el.logoUrl}
                                  style={{
                                    width: "100%",
                                    height: "100%",
                                    objectFit: "contain",
                                  }}
                                />
                              </Box>
                            ))}
                          </Box>
                        </Box>
                      </AccordionDetails>
                    ))}

                {item?.code === flightPaymentMethodCode.VISA_MASTER && (
                  <AccordionDetails>
                    <Box pl={6} pt={2}>
                      <img src="/images/image_visa_master_card.png" />
                    </Box>
                  </AccordionDetails>
                )}
              </Accordion>
            </Box>
          ))}
        </Box>
      </Box>
    </>
  );
};

export default PaymentMethodHolding;
