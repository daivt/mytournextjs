import OrderContact from "@components/desktop/account/order/flight/orderDetail/OrderContact";
import OrderCustomerInfo from "@components/desktop/account/order/flight/orderDetail/OrderCustomerInfo";
import OrderInfo from "@components/desktop/account/order/flight/orderDetail/OrderInfo";
import OrderInsurance from "@components/desktop/account/order/flight/orderDetail/OrderInsurance";
import OrderPaymentInfo from "@components/desktop/account/order/flight/orderDetail/OrderPaymentInfo";
import OrderPriceDetail from "@components/desktop/account/order/flight/orderDetail/OrderPriceDetail";
import OrderExportInvoice from "@components/desktop/account/order/flight/orderDetail/OrderExportInvoice";
import WayFlightItemDetail from "@components/desktop/account/order/flight/WayFlightItemDetail";
import {
  Box,
  Dialog,
  DialogContent,
  DialogTitle,
  Divider,
  IconButton,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { IconClose } from "@public/icons";
import ButtonComponent from "@src/button/Button";
import utilStyles from "@styles/utilStyles";
import { flightPaymentStatus, listString, routeStatic } from "@utils/constants";
import { getPaymentStatusBookingFlight, isEmpty } from "@utils/helpers";
import clsx from "clsx";
import { useRouter } from "next/router";
import React from "react";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "column",
    borderRadius: 8,
    backgroundColor: theme.palette.white.main,
  },
  backToOrder: {
    color: theme.palette.blue.blueLight8,
    cursor: "pointer",
  },
  imgOrder: {
    backgroundColor: theme.palette.gray.grayLight26,
    borderRadius: 8,
    padding: 24,
  },
  titleTicket: {
    padding: "4px 6px",
    borderRadius: 4,
    color: theme.palette.white.main,
    backgroundColor: theme.palette.blue.blueLight8,
    fontWeight: 600,
  },
  ticketDetail: {
    width: "45%",
  },
  lineBox: {
    display: "flex",
    width: "100%",
  },
  divider: {
    backgroundColor: theme.palette.gray.grayLight22,
    height: 1,
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
  },
  dialogTitle: {
    margin: 0,
    padding: "24px 24px 0 24px",
  },
  dialogContent: {
    padding: 8,
  },
}));

const OrderDetail = ({
  open = false,
  handleClose = () => {},
  orderDetail = {},
  airlines = {},
}) => {
  const theme = useTheme();
  const classes = useStyles();
  const classesUtils = utilStyles();
  const router = useRouter();

  const paymentStatus = getPaymentStatusBookingFlight(
    orderDetail.paymentStatus,
    orderDetail.paymentMethodCode,
    orderDetail?.expiredTime || 0
  );

  const getAirlineInfo = (ticketInfo) => {
    let result = {};
    if (!isEmpty(airlines)) {
      result = airlines.find((al) => al.aid === ticketInfo?.airlineId);
    }
    return result;
  };

  const handleClickPayment = () => {
    router.push({
      pathname: routeStatic.REPAY_BOOKING_FLIGHT.href,
      query: {
        bookingId: orderDetail?.orderCode.substr(1),
      },
    });
  };

  return (
    <Dialog
      Dialog
      onClose={handleClose(false)}
      open={open}
      maxWidth="lg"
      fullWidth
    >
      <DialogTitle
        onClose={handleClose(false)}
        disableTypography
        classes={{ root: classes.dialogTitle }}
      >
        <Typography variant="h6">{listString.IDS_TEXT_ORDER_DETAIL}</Typography>
        <IconButton
          onClick={handleClose(false)}
          aria-label="close"
          className={classes.closeButton}
        >
          <IconClose />
        </IconButton>
      </DialogTitle>
      <DialogContent classes={{ root: classes.dialogContent }}>
        <Box className={classes.wrapper}>
          <OrderInfo item={orderDetail} />
          <Box my={3} ml={2} display="flex">
            <Box>
              <Box className={classes.imgOrder}>
                <img src="/images/order_flight.png" />
              </Box>
            </Box>
            <Box ml={2} className={classes.lineBox} flexDirection="column">
              <Box className={classes.lineBox} mb={3}>
                <Box className={clsx(classes.ticketDetail)}>
                  {orderDetail.isTwoWay && (
                    <Box mb={12 / 8} display="flex">
                      <Box className={classes.titleTicket}>
                        <Typography variant="body2">
                          {listString.IDS_MT_TEXT_DEPARTURE.toUpperCase()}
                        </Typography>
                      </Box>
                    </Box>
                  )}
                  <WayFlightItemDetail
                    item={orderDetail?.outbound}
                    airlineInfo={getAirlineInfo(orderDetail?.outbound)}
                    isViewDetail={true}
                  />
                </Box>
                {orderDetail.isTwoWay && (
                  <Box className={clsx(classes.ticketDetail)}>
                    <Box mb={12 / 8} display="flex">
                      <Box className={classes.titleTicket}>
                        <Typography variant="body2">
                          {listString.IDS_MT_TEXT_RETURN.toUpperCase()}
                        </Typography>
                      </Box>
                    </Box>
                    <WayFlightItemDetail
                      item={orderDetail?.inbound}
                      airlineInfo={getAirlineInfo(orderDetail?.inbound)}
                      isViewDetail={true}
                    />
                  </Box>
                )}
              </Box>
              <Divider className={classes.divider} />
              <Box className={classes.lineBox} mt={3}>
                <OrderContact item={orderDetail?.mainContact} />
              </Box>
              <Box className={classes.lineBox} mt={3}>
                <OrderCustomerInfo
                  guests={orderDetail.guests}
                  isFromPayment={true}
                />
              </Box>
              {!isEmpty(orderDetail?.vatInvoiceInfo) && (
                <Box className={classes.lineBox} mt={3} flexDirection="column">
                  <OrderExportInvoice
                    invoice={orderDetail.vatInvoiceInfo}
                    paymentStatus={paymentStatus}
                  />
                </Box>
              )}
              <Box className={classes.lineBox} mt={3}>
                <OrderPaymentInfo
                  orderDetail={orderDetail}
                  paymentStatus={paymentStatus}
                />
              </Box>
              <Box className={classes.lineBox} mt={3}>
                <OrderInsurance
                  insuranceContact={orderDetail.insuranceContact}
                  insuranceInfo={orderDetail.guests[0].insuranceInfo}
                  numberInsurance={orderDetail.numGuests}
                />
              </Box>
              <Box className={classes.lineBox} mt={3}>
                <OrderPriceDetail orderDetail={orderDetail} />
              </Box>
              {(paymentStatus.type === flightPaymentStatus.HOLDING ||
                paymentStatus.type === flightPaymentStatus.PENDING) && (
                <Box
                  className={classes.lineBox}
                  mt={3}
                  justifyContent="flex-end"
                >
                  <Box mr={2}>
                    <ButtonComponent
                      type="submit"
                      backgroundColor={theme.palette.pink.main}
                      height={48}
                      borderRadius={8}
                      padding={"12px 36px"}
                      handleClick={handleClickPayment}
                      className={classes.rightBox}
                    >
                      <Typography variant="subtitle1">
                        {listString.IDS_MT_TEXT_PAYMENT}
                      </Typography>
                    </ButtonComponent>
                  </Box>
                </Box>
              )}
            </Box>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
};

export default OrderDetail;
