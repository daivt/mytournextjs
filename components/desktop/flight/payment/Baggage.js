import DialogAddBaggage from "@components/common/modal/flight/DialogAddBaggage";
import { Box, Grid, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import { IconPlusFill, IconLuggage, IconEdit } from "@public/icons";
import { listString } from "@utils/constants";
import { useFormikContext } from "formik";
import { useState, useEffect } from "react";
import {
  isEmpty,
  getDetailBaggageByCustomer,
  formatTotalMoneyBaggage,
} from "@utils/helpers";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: theme.palette.white.main,
    borderRadius: 8,
    padding: 24,
    cursor: "pointer",
  },
  viewDetail: {
    display: "flex",
    justifyContent: "flex-end",
    color: theme.palette.gray.grayDark8,
  },
  centerIcon: {
    alignItems: "center",
  },
  iconLuggage: {
    width: 32,
  },
  baggageNote: {
    color: theme.palette.gray.grayDark8,
  },
  containerInfoFilter: {
    fontSize: 18,
    lineHeight: "22px",
  },
  upperText: {
    textTransform: "uppercase",
  },
}));

const Baggage = ({
  outboundBaggageInfo = {},
  inboundBaggageInfo = {},
  totalGuest = 0,
  customerInfo,
  setCustomerInfo = () => {},
  bagInfo = {},
  setBagInfo = () => {},
  setBaggageCustomer = () => {},
  setMessageAlert = () => {},
  detailTicketStatus,
}) => {
  const theme = useTheme();
  const classes = useStyles();
  const { values, setFieldValue } = useFormikContext();
  const [visibleDrawerType, setVisibleDrawerType] = useState(false);
  const [detailData, setDetailData] = useState({});
  let customerInfoTemp = [];
  for (let index = 0; index < totalGuest; index++) {
    let obBagId = null;
    let ibBagId = null;
    if (inboundBaggageInfo?.baggages && values[`inbound_baggage_${index}`]) {
      ibBagId = inboundBaggageInfo?.baggages.find(
        (element) => element?.id === values[`inbound_baggage_${index}`]
      )?.id;
    }

    if (outboundBaggageInfo?.baggages && values[`outbound_baggage_${index}`]) {
      obBagId = outboundBaggageInfo?.baggages.find(
        (element) => element?.id === values[`outbound_baggage_${index}`]
      )?.id;
    }
    customerInfoTemp = [
      ...customerInfoTemp,
      {
        name: values[`name_${index}`],
        keyPosition: index,
        departureBaggage: obBagId,
        returnBaggage: ibBagId,
        returnMoney:
          inboundBaggageInfo?.baggages && ibBagId
            ? inboundBaggageInfo?.baggages.find(
                (element) => element?.id === ibBagId
              )?.price
            : 0,
        returnWeight:
          inboundBaggageInfo?.baggages && ibBagId
            ? inboundBaggageInfo?.baggages.find(
                (element) => element?.id === ibBagId
              )?.weight
            : 0,
        departureMoney:
          outboundBaggageInfo?.baggages && obBagId
            ? outboundBaggageInfo?.baggages.find(
                (element) => element?.id === obBagId
              )?.price
            : 0,
        departureWeight:
          outboundBaggageInfo?.baggages && obBagId
            ? outboundBaggageInfo?.baggages.find(
                (element) => element?.id === obBagId
              )?.weight
            : 0,
      },
    ];
  }

  const toggleDrawer = (open) => (event) => {
    let checkNameCustomer = false;
    for (let index = 0; index < totalGuest; index++) {
      if (values[`name_${index}`]) {
        checkNameCustomer = true;
      }
    }

    if (checkNameCustomer) {
      setVisibleDrawerType(open);
      setMessageAlert("");
    } else {
      setMessageAlert(
        "Vui lòng nhập thông tin hành khách trước khi chọn mua thêm hành lý. Xin cảm ơn!"
      );
    }
  };
  const handleSubmit = (data) => () => {
    let dataTempBaggage = {};
    if (data) {
      let numGuest = 0;
      let totalMoney = 0;
      let totalWeight = 0;
      data.forEach((element, index) => {
        let arrayTemp = [...customerInfo];
        if (element.departureWeight) {
          numGuest++;
          totalMoney += element.departureMoney;
          totalWeight += element.departureWeight;
          arrayTemp[index].outboundBaggageId = element.departureBaggage;
        } else {
          element.departureBaggage = undefined;
        }
        setFieldValue(`outbound_baggage_${index}`, element.departureBaggage);
        dataTempBaggage[`outbound_baggage_${index}`] = element.departureBaggage;
        if (element.returnWeight) {
          numGuest++;
          totalMoney += element.returnMoney;
          totalWeight += element.returnWeight;
          arrayTemp[index].inboundBaggageId = element.returnBaggage;
        } else {
          element.returnBaggage = undefined;
        }
        setFieldValue(`inbound_baggage_${index}`, element.returnBaggage);
        dataTempBaggage[`inbound_baggage_${index}`] = element.returnBaggage;
        setCustomerInfo(arrayTemp);
      });
      setBagInfo({
        numGuest: numGuest,
        weight: totalWeight,
        money: totalMoney,
      });
    }
    setVisibleDrawerType(false);
    setBaggageCustomer(dataTempBaggage);
    setDetailData(getDetailBaggageByCustomer(data));
  };

  const dataBaggage = {
    arrayCustomer: customerInfoTemp,
    outboundBaggageInfo,
    inboundBaggageInfo,
    return: !!inboundBaggageInfo,
  };

  useEffect(() => {
    if (detailTicketStatus) {
      let numGuest = 0;
      let totalMoney = 0;
      let totalWeight = 0;
      for (let index = 0; index < totalGuest; index++) {
        if (values[`name_${index}`]) {
          let obBagId = null;
          let ibBagId = null;
          if (
            inboundBaggageInfo?.baggages &&
            values[`inbound_baggage_${index}`]
          ) {
            ibBagId = inboundBaggageInfo?.baggages.find(
              (element) => element?.id === values[`inbound_baggage_${index}`]
            );
          }

          if (
            outboundBaggageInfo?.baggages &&
            values[`outbound_baggage_${index}`]
          ) {
            obBagId = outboundBaggageInfo?.baggages.find(
              (element) => element?.id === values[`outbound_baggage_${index}`]
            );
          }

          totalMoney += (obBagId?.price || 0) + (ibBagId?.price || 0);
          let weightTemp = 0;
          weightTemp = (obBagId?.weight || 0) + (ibBagId?.weight || 0);
          totalWeight += weightTemp;
          if (weightTemp) {
            numGuest++;
          }
        }
      }
      setBagInfo({
        numGuest: numGuest,
        weight: totalWeight,
        money: totalMoney,
      });
      setDetailData(getDetailBaggageByCustomer(customerInfoTemp));
    }
  }, [detailTicketStatus]);

  return (
    <>
      <Box className={classes.wrapper}>
        <Grid container onClick={toggleDrawer(true)}>
          <Grid item lg={11} md={11} sm={11} xs={11}>
            <Box>
              <Typography
                variant="subtitle1"
                className={classes.containerInfoFilter}
              >
                {listString.IDS_MT_TEXT_ADD_BAGGAGE}
              </Typography>
            </Box>
            <Box display="flex" flexDirection="column" mt={1}>
              <Box>
                <Typography variant="caption" className={classes.baggageNote}>
                  {listString.IDS_MT_TEXT_BAGGAGE_NOTE}
                </Typography>
              </Box>
            </Box>
          </Grid>
          <Grid
            item
            lg={1}
            md={1}
            sm={1}
            xs={1}
            className={clsx(
              classes.viewDetail,
              isEmpty(detailData) && classes.centerIcon
            )}
          >
            {!isEmpty(detailData) ? <IconEdit /> : <IconPlusFill />}
          </Grid>
        </Grid>

        {!isEmpty(detailData) && (
          <Box mt={1} display="flex" flexDirection="column">
            {detailData.map((item, index) => (
              <Box display="flex" width="100%" key={index} mt={1}>
                <Box width="76%">
                  <Typography variant="caption" className={classes.upperText}>
                    {item.name}
                  </Typography>
                </Box>
                <Box width="14%" display="flex">
                  <IconLuggage />
                  <Typography variant="caption" style={{ marginLeft: 6 }}>
                    {item.weight}kg
                  </Typography>
                </Box>
                <Box width="10%" display="flex" justifyContent="flex-end">
                  <Typography variant="caption">
                    {item.money.formatMoney()}đ
                  </Typography>
                </Box>
              </Box>
            ))}
          </Box>
        )}
      </Box>
      <DialogAddBaggage
        data={dataBaggage}
        open={visibleDrawerType}
        toggleDrawer={toggleDrawer}
        handleSubmit={handleSubmit}
      />
    </>
  );
};
Baggage.propTypes = {};

export default Baggage;
