import { getInsurancePackage } from "@api/flight";
import BootstrapTooltip from "@components/common/bootstrapTooltip/BootstrapTooltip";
import { Box, Typography } from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import MuiFormControlLabel from "@material-ui/core/FormControlLabel";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles, withStyles } from "@material-ui/styles";
import { IconCheckBox, IconCheckBoxActive, IconInfo } from "@public/icons";
import Link from "@src/link/Link";
import { listEventFlight, listString } from "@utils/constants";
import * as gtm from "@utils/gtm";
import { useFormikContext } from "formik";
import React, { useState } from "react";
import { isEmpty } from "@utils/helpers";
const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    padding: "24px 24px 12px 24px",
    background: theme.palette.white.main,
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    boxSizing: "border-box",
    boxShadow: "0px 1px 1px rgba(0, 0, 0, 0.05)",
    borderRadius: 8,
  },
  wrapTitle: {
    fontSize: 18,
    lineHeight: "22px",
  },
  wrapInfo: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    padding: "4px 0 8px 0",
  },
  wrapLeft: {
    display: "flex",
    alignItems: "center",
  },
  wrapRight: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "flex-end",
    width: "32%",
  },
  insPrice: {
    color: theme.palette.gray.grayDark7,
    lineHeight: "17px",
  },
  numCustomer: {
    lineHeight: "17px",
    color: theme.palette.black.black3,
  },
  insTotalMoney: {
    color: theme.palette.black.black3,
    lineHeight: "17px",
  },
  leftTitle: {
    color: theme.palette.gray.grayDark7,
    marginLeft: 12,
  },
  chubbContent: {
    color: theme.palette.gray.grayDark8,
  },
  chubbNote: {
    color: theme.palette.red.redLight5,
  },
  customTooltip: {
    maxWidth: 580,
    maxHeight: 250,
    background: theme.palette.white.main,
    padding: 16,
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    boxSizing: "border-box",
    boxShadow: "0px 8px 8px rgba(0, 0, 0, 0.1)",
    borderRadius: 8,
  },
  arrowTooltip: {
    color: theme.palette.white.main,
  },
}));

const FormControlLabel = withStyles((theme) => ({
  root: {
    margin: "0 0 0 -12px",
  },
  label: {
    fontSize: 14,
    fontWeight: "normal",
    lineHeight: "17px",
    marginRight: 2,
  },
}))(MuiFormControlLabel);
const InsuranceCheckbox = withStyles((theme) => ({
  root: {
    color: theme.palette.gray.grayLight25,
    "&$checked": {
      color: theme.palette.blue.blueLight8,
    },
  },
  checked: {},
}))((props) => (
  <Checkbox
    color="default"
    {...props}
    icon={<IconCheckBox />}
    checkedIcon={<IconCheckBoxActive />}
  />
));

const InsuranceBox = ({
  paramsInsurance = {},
  customerInfo,
  setTotalPrice = () => {},
  totalPrice,
  setBuyInsurance = () => {},
}) => {
  const [state, setState] = useState({
    checkedInsurance: false,
  });
  const [retailPrice, setRetailPrice] = useState(0);
  const [insuranceMoney, setInsuranceMoney] = useState(0);

  const { setFieldValue, values, errors } = useFormikContext();
  const classes = useStyles();
  const theme = useTheme();

  const handleCheckbox = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
    if (!event.target.checked) {
      setTotalPrice(0);
    } else {
      gtm.addEventGtm(listEventFlight.FlightAddInsurance);
      setTotalPrice(insuranceMoney);
    }
    setFieldValue("checkValidContactMail", event.target.checked);
    setBuyInsurance(event.target.checked);
  };

  const actionGetInsuranceInfo = async () => {
    const args = paramsInsurance;
    args.isMultipleInsurance = true;
    const { data } = await getInsurancePackage(args);
    if (data.code == 200) {
      const result = data?.data[0];
      setFieldValue("insurancePackageCode", result?.code);
      setRetailPrice(result?.retailPrice);
      setInsuranceMoney(paramsInsurance?.numGuest * result?.retailPrice);
      if (values?.checkValidContactMail) {
        setTotalPrice(paramsInsurance?.numGuest * result?.retailPrice);
      }
    }
  };
  React.useEffect(() => {
    setState({ ...state, checkedInsurance: values?.checkValidContactMail });
  }, [values?.checkValidContactMail]);

  React.useEffect(() => {
    actionGetInsuranceInfo();
  }, []);

  if (isEmpty(retailPrice)) return <></>;

  const toolTipText = (
    <Box className={classes.chubbContent}>
      <Typography variant="caption" style={{ lineHeight: "17px" }}>
        {listString.IDS_TEXT_CHUBB_CONTENT_HEAD}
      </Typography>
      <ul>
        <li>
          <Typography
            component="p"
            variant="caption"
            style={{ lineHeight: "17px", padding: "6px 0" }}
          >
            {listString.IDS_TEXT_CHUBB_CONTENT_LI_01}
          </Typography>
        </li>
        <li>
          <Typography
            component="p"
            variant="caption"
            style={{ lineHeight: "17px", padding: "6px 0" }}
          >
            {listString.IDS_TEXT_CHUBB_CONTENT_LI_02}
          </Typography>
        </li>
        <li>
          <Typography
            component="p"
            variant="caption"
            style={{ lineHeight: "17px", paddingTop: "6px" }}
          >
            {listString.IDS_TEXT_CHUBB_CONTENT_LI_03}
          </Typography>
        </li>
      </ul>
      <Typography
        component="p"
        variant="caption"
        style={{ lineHeight: "17px", padding: "6px 0" }}
      >
        {`${listString.IDS_TEXT_CHUBB_CONTENT_FOOTER_01} `}
        <Link href={listString.IDS_TEXT_CHUBB_TERM_HREF} target="_blank">
          <Typography variant="caption" style={{ lineHeight: "17px" }}>
            {listString.IDS_TEXT_CHUBB_CONTENT_LINK}
          </Typography>
        </Link>
        {` ${listString.IDS_TEXT_CHUBB_CONTENT_FOOTER_02}`}
      </Typography>
      <Typography
        variant="caption"
        className={classes.chubbNote}
        style={{ lineHeight: "17px", padding: "6px 0" }}
      >
        {listString.IDS_TEXT_CHUBB_CONTENT_NOTE}
      </Typography>
    </Box>
  );

  return (
    <Box className={classes.container} mt={2}>
      <Typography variant="subtitle1" className={classes.wrapTitle}>
        {listString.IDS_TEXT_BUY_INSURANCE}
      </Typography>
      <Box className={classes.wrapInfo}>
        <Box className={classes.wrapLeft}>
          <FormControlLabel
            control={
              <InsuranceCheckbox
                checked={state.checkedInsurance}
                onChange={handleCheckbox}
                name="checkedInsurance"
              />
            }
            label={
              <>
                <Typography variant="caption" style={{ lineHeight: "17px" }}>
                  {listString.IDS_TEXT_BUY_INSURANCE}
                </Typography>
                <Typography variant="caption" className={classes.insPrice}>
                  {` (${retailPrice.formatMoney()}đ/người)`}
                </Typography>
              </>
            }
          />
        </Box>
        <Box className={classes.wrapRight}>
          <Typography variant="caption" className={classes.numCustomer}>
            {`x ${paramsInsurance?.numGuest} ${listString.IDS_MT_TEXT_SINGLE_CUSTOMER}`}
          </Typography>
          <Typography variant="subtitle2" className={classes.insTotalMoney}>
            {insuranceMoney.formatMoney()}đ
          </Typography>
        </Box>
      </Box>
      <Box display="flex" alignItems="center" mt={12 / 8}>
        <img
          src="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_logo_chubb.svg"
          alt=""
        />
        <Box display="flex" alignItems="center">
          <Typography className={classes.leftTitle} variant="body2">
            {listString.IDS_TEXT_INSURANCE_CHUBB_TITLE}
          </Typography>
          <BootstrapTooltip
            title={toolTipText}
            placement="top"
            classes={{
              tooltip: classes.customTooltip,
              arrow: classes.arrowTooltip,
            }}
          >
            <Box>
              <IconInfo
                className="svgFillAll"
                style={{
                  stroke: theme.palette.blue.blueLight8,
                  marginLeft: 6,
                  cursor: "pointer",
                }}
              />
            </Box>
          </BootstrapTooltip>
        </Box>
      </Box>
    </Box>
  );
};

export default InsuranceBox;
