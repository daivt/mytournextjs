import { getVoucherListPayment } from "@api/flight";
import DialogVoucherList from "@components/common/modal/flight/DialogVoucherList";
import { Box, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import {
  IconCoupon,
  IconEdit,
  IconRadioActiveGreen,
  IconPlusFill,
} from "@public/icons";
import snackbarSetting from "@src/alert/Alert";
import utilStyles from "@styles/utilStyles";
import { listString, listEventFlight } from "@utils/constants";
import { useSnackbar } from "notistack";
import React, { useState } from "react";
import * as gtm from "@utils/gtm";
const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "column",
    cursor: "pointer",
  },
  itemVoucher: {
    boxShadow:
      "0px 0px 6px rgba(0, 0, 0, 0.06), 0px 6px 6px rgba(0, 0, 0, 0.06)",
    overflow: "hidden",
    borderRadius: 8,
    minWidth: 300,
  },
  divider: {
    height: 6,
    color: theme.palette.gray.grayLight22,
  },
  circle: {
    width: 4,
    height: 4,
    transform: "rotate(-45deg)",
    borderRadius: "50%",
    borderBottom: `solid 1px ${theme.palette.green.greenLight7}`,
    borderRight: `solid 1px ${theme.palette.green.greenLight7}`,
    background: theme.palette.white.main,
    marginBottom: 3,
  },
  listDot: {
    display: "flex",
    flexDirection: "column",
    position: "absolute",
    top: "4px",
    left: "-2px",
  },
  voucherCode: {
    position: "relative",
    display: "flex",
    alignItems: "center",
    border: `solid 1px ${theme.palette.green.greenLight7}`,
    borderRadius: 4,
    padding: "0 10px",
  },
}));

const Coupon = ({
  listVouchers,
  totalVoucher,
  voucherUsing,
  setVoucherUsing,
  handleCancelUseVoucher = () => {},
  setInputSearchVoucher,
  visibleDrawerType,
  setVisibleDrawerType,
}) => {
  const theme = useTheme();
  const classes = useStyles();
  const classesUtils = utilStyles();

  const toggleDrawer = (open) => (event) => {
    if (open) {
      gtm.addEventGtm(listEventFlight.FlightViewListPromo);
    }
    setVisibleDrawerType(open);
  };

  const handleSetVoucher = (info) => {
    if (info?.id === voucherUsing?.id) {
      info = {};
      handleCancelUseVoucher(voucherUsing);
    }
    setVoucherUsing(info);
    setVisibleDrawerType(false);
    gtm.addEventGtm(listEventFlight.FlightAddPromo);
  };

  const initItem = listVouchers;

  return (
    <Box className={classes.wrapper}>
      <Box display="flex" onClick={toggleDrawer(true)}>
        <Box display="flex" alignItems="center" width={"50%"}>
          <IconCoupon />
          <Box pl={14 / 8}>
            <Typography variant="subtitle1">
              {listString.IDS_TEXT_VOUCHER}
            </Typography>
          </Box>
        </Box>

        <Box
          display="flex"
          alignItems="center"
          justifyContent="flex-end"
          color={theme.palette.green.greenLight7}
          width="50%"
        >
          {voucherUsing?.code ? (
            <>
              <Box className={classes.voucherCode} mr={28 / 8}>
                <IconRadioActiveGreen />
                <Box className={classes.listDot}>
                  {Array(4)
                    .fill(0)
                    .map((it, index) => (
                      <Box key={index} className={classes.circle} />
                    ))}
                </Box>

                <Typography
                  variant="subtitle2"
                  style={{ padding: "8px 0 8px 6px" }}
                >
                  {voucherUsing?.code}
                </Typography>
              </Box>

              <IconEdit fontSize="small" />
            </>
          ) : (
            <IconPlusFill fontSize="small" />
          )}
        </Box>
      </Box>
      <DialogVoucherList
        data={initItem}
        open={visibleDrawerType}
        toggleDrawer={toggleDrawer}
        handleSetVoucher={handleSetVoucher}
        setInputSearchVoucher={setInputSearchVoucher}
        voucherUsing={voucherUsing}
        totalVoucher={totalVoucher}
      />
    </Box>
  );
};

export default Coupon;
