import {
  getGeneralInformation,
  getTicketDetail,
  getVoucherListPayment,
  validateVoucherCode,
} from "@api/flight";
import FlightTicketDetail from "@components/common/card/flight/FlightTicketDetail";
import DialogShowMessage from "@components/common/modal/flight/DialogShowMessage";
import Baggage from "@components/desktop/flight/payment/Baggage";
import ContactInfo from "@components/desktop/flight/payment/ContactInfo";
import CustomerInfo from "@components/desktop/flight/payment/CustomerInfo";
import InsuranceBox from "@components/desktop/flight/payment/InsuranceBox";
import Invoice from "@components/desktop/flight/payment/Invoice";
import PriceDetail from "@components/desktop/flight/payment/PriceDetail";
import Layout from "@components/layout/desktop/Layout";
import { Box, Button, Divider } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/styles";
import { IconArrowDownToggle } from "@public/icons";
import snackbarSetting from "@src/alert/Alert";
import ButtonComponent from "@src/button/Button";
import { CUSTOMER_TYPE, listString } from "@utils/constants";
import {
  formatDateOfBirth,
  getContactInfoTemp,
  getInfoGuestTemp,
  isEmpty,
} from "@utils/helpers";
import { DATE_FORMAT_BACK_END, DATE_TIME_FORMAT } from "@utils/moment";
import { Form, Formik } from "formik";
import moment from "moment";
import "moment/locale/vi";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import React, { useEffect, useRef, useState } from "react";
import * as yup from "yup";
import TicketTitle from "../common/TicketTitle";

const useStyle = makeStyles((theme) => ({
  container: {
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    margin: "0 auto",
    alignItems: "flex-start",
    paddingTop: 24,
  },
  wrapLeft: {
    display: "flex",
    flexDirection: "column",
    marginRight: 24,
    width: "58%",
  },
  wrapRight: {
    display: "flex",
    width: "40%",
    position: "sticky",
    top: "84px",
  },
  boxPriceDetail: {
    padding: 24,
    backgroundColor: theme.palette.white.main,
    width: "100%",
    borderRadius: 8,
  },
  iconDot: {
    margin: "10px",
  },
  divider: {
    height: 1,
    color: theme.palette.gray.grayLight22,
    marginTop: 12,
  },
  divider2: {
    height: 1,
    backgroundColor: theme.palette.gray.grayLight22,
    margin: "0px 12px",
  },
  button: {
    width: "100%",
    height: 48,
    color: theme.palette.primary.main,
    backgroundColor: theme.palette.white.main,
    borderRadius: "0px 0px 8px 8px",
    "& i": {
      paddingRight: 5,
    },
    "&:hover": {
      backgroundColor: theme.palette.white.main,
    },
  },
  IconArrowDownToggle: {
    stroke: theme.palette.blue.blueLight8,
    marginLeft: 8,
    marginTop: 2,
  },
  IconArrowUpToggle: {
    stroke: theme.palette.blue.blueLight8,
    marginLeft: 8,
    transform: "rotate(180deg)",
    marginTop: 2,
  },
}));
const DesktopContent = ({ title = "", description = "", keywords = "" }) => {
  const classes = useStyle();
  const theme = useTheme();
  const ref = useRef(null);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [detailTicketOutBound, setDetailTicketOutBound] = useState([]);
  const [totalMoney, setTotalMoney] = useState(0);
  const [totalSubMoney, setTotalSubMoney] = useState(0);
  const [voucherUsing, setVoucherUsing] = useState({});
  const [voucherSuggest, setVoucherSuggest] = useState({});
  const [discountMoneyVoucher, setDiscountMoneyVoucher] = useState(0);
  const [detailTicketInBound, setDetailTicketInBound] = useState(null);
  const [generalInformation, setGeneralInformation] = useState();
  const [bagInfo, setBagInfo] = useState({
    numGuest: 0,
    weight: 0,
    money: 0,
  });
  const [totalPrice, setTotalPrice] = React.useState(0);
  const [buyInsurance, setBuyInsurance] = React.useState("-1");
  const [baggageCustomer, setBaggageCustomer] = React.useState({});
  const [messageAlert, setMessageAlert] = useState("");
  const [showMessageAlert, setShowMessageAlert] = useState(false);
  const [detailTicketStatus, setDetailTicketStatus] = React.useState(false);
  const [customError, setCustomError] = useState({});

  const toggleDrawerAlert = (open) => () => {
    if (!open) {
      setMessageAlert("");
    }
    setShowMessageAlert(open);
  };

  const router = useRouter();
  const queryData = router.query;
  const fromDate = queryData.departureDate;
  const toDate = queryData?.returnDate || queryData.departureDate;
  const paramsInsurance = {
    fromAirportCode: queryData.origin_code,
    toAirportCode: queryData.destination_code,
    isOneWay: queryData.ticketInBoundId ? false : true,
    fromDate: moment(fromDate, DATE_FORMAT_BACK_END).format(DATE_TIME_FORMAT),
    toDate: moment(toDate, DATE_FORMAT_BACK_END).format(DATE_TIME_FORMAT),
    numGuest:
      parseInt(queryData.adultCount) +
      parseInt(queryData.childCount) +
      parseInt(queryData.infantCount),
  };
  const actionsTicketDetail = async () => {
    let moneyTicket = 0;
    let moneyTicketPromo = 0;
    if (queryData?.ticketOutBoundId) {
      const params = {
        agencyId: queryData.agencyOutBoundId,
        requestId: queryData.requestId,
        ticketId: queryData.ticketOutBoundId,
      };
      const { data } = await getTicketDetail(params);

      if (data?.code === 400) {
        data?.message &&
          enqueueSnackbar(
            data?.message,
            snackbarSetting((key) => closeSnackbar(key), { color: "error" })
          );

        router.push({
          pathname: "/ve-may-bay/",
          query: {},
        });
      }

      setDetailTicketOutBound(data?.data);
      moneyTicket += parseInt(data?.data?.ticket?.totalPrice);
      if (data?.data?.ticket?.outbound?.ticketdetail?.discount) {
        moneyTicketPromo +=
          moneyTicket +
          parseInt(data?.data?.ticket?.outbound?.ticketdetail?.discount);
      }
      if (!queryData?.ticketInBoundId) {
        setDetailTicketStatus(true);
      }
    }
    if (queryData?.ticketInBoundId) {
      const params = {
        agencyId: queryData.agencyInBoundId,
        requestId: queryData.requestId,
        ticketId: queryData.ticketInBoundId,
      };
      const { data } = await getTicketDetail(params);
      setDetailTicketInBound(data?.data);
      moneyTicket += parseInt(data?.data?.ticket?.totalPrice);
      if (data?.data?.ticket?.outbound?.ticketdetail?.discount) {
        moneyTicketPromo +=
          moneyTicket +
          parseInt(data?.data?.ticket?.outbound?.ticketdetail?.discount);
      }
      setDetailTicketStatus(true);
    }
    if (moneyTicket) setTotalMoney(moneyTicket);
    if (moneyTicketPromo) setTotalSubMoney(moneyTicketPromo);
  };

  const actionsGeneralInformation = async () => {
    const { data } = await getGeneralInformation();
    setGeneralInformation(data?.data);
  };

  const actionsVoucher = async (customerInfoValues) => {
    let firstVoucher = {};
    if (isEmpty(voucherSuggest)) {
      const dataPost = {
        info: { productType: "flight" },
        isUsed: false,
        page: 1,
        module: "flight",
        size: 2,
        term: "",
      };
      const { data } = await getVoucherListPayment(dataPost);
      firstVoucher = data?.data?.list[0];
      if (firstVoucher) {
        setVoucherUsing({
          id: firstVoucher?.id,
          code: firstVoucher?.codeDetail,
          description: firstVoucher?.rewardProgram?.title,
          expired: firstVoucher?.responseData?.validTo,
        });
        setVoucherSuggest(firstVoucher);
      }
    }
    if (firstVoucher?.codeDetail || !isEmpty(voucherSuggest)) {
      let tempGuests = [];
      let tempContact = {};
      if (customerInfoValues) {
        tempGuests = getInfoGuestTemp(customerInfoValues, router, totalGuest);
        tempContact = getContactInfoTemp(customerInfoValues);
      }

      const paramCheck = {
        code: firstVoucher?.codeDetail || voucherSuggest?.codeDetail,
        data: {
          contact: tempContact,
          guests: tempGuests,
          insuranceContact: customerInfoValues?.checkValidContactMail
            ? tempContact
            : null,
          point: 0,
          promoCode: "",
          tickets: {
            inbound: queryData?.ticketInBoundId
              ? {
                  agencyId: queryData.agencyInBoundId,
                  requestId: queryData.requestId,
                  ticketId: queryData.ticketInBoundId,
                }
              : null,
            outbound: {
              agencyId: queryData.agencyOutBoundId,
              requestId: queryData.requestId,
              ticketId: queryData.ticketOutBoundId,
            },
          },
        },
        module: "flight",
        originPoint: 0,
      };

      const { data } = await validateVoucherCode(paramCheck);
      const dataValidVoucher = data;

      if (dataValidVoucher.code === 200) {
        setDiscountMoneyVoucher(dataValidVoucher?.data?.discount);
      } else {
        setVoucherUsing({});
        setDiscountMoneyVoucher(0);
      }
    }
  };

  let listInputTemp = [];
  let totalGuest = 0;
  if (queryData.adultCount) {
    let numAdults = parseInt(queryData.adultCount);
    let numChildren = parseInt(queryData.childCount);
    let numInfants = parseInt(queryData.infantCount);
    totalGuest = numAdults + numChildren + numInfants;
    if (numAdults) {
      for (let index = 0; index < numAdults; index++) {
        listInputTemp = [
          ...listInputTemp,
          {
            gender: "",
            name: "",
            outboundBaggageId: undefined,
            inboundBaggageId: undefined,
            insuranceInfo: null,
            passport: "",
            passportExpiredDate: "",
            passportCountryId: null,
            nationalityCountryId: null,
            dob: "",
            type: CUSTOMER_TYPE.ADULT,
            isMultipleInsurance: true,
          },
        ];
      }
    }
    if (numChildren) {
      for (let index = 0; index < numChildren; index++) {
        listInputTemp = [
          ...listInputTemp,
          {
            gender: "",
            name: "",
            outboundBaggageId: undefined,
            inboundBaggageId: undefined,
            insuranceInfo: null,
            passport: "",
            passportExpiredDate: "",
            passportCountryId: null,
            nationalityCountryId: null,
            dob: "",
            type: CUSTOMER_TYPE.CHILDREN,
          },
        ];
      }
    }
    if (numInfants) {
      for (let index = 0; index < numInfants; index++) {
        listInputTemp = [
          ...listInputTemp,
          {
            gender: "",
            name: "",
            outboundBaggageId: undefined,
            inboundBaggageId: undefined,
            insuranceInfo: null,
            passport: "",
            passportExpiredDate: "",
            passportCountryId: null,
            nationalityCountryId: null,
            dob: "",
            type: CUSTOMER_TYPE.BABY,
          },
        ];
      }
    }
  }

  let customerInfo = null;
  const generateField = () => {
    const ISSERVER = typeof window === "undefined";

    if (!ISSERVER) {
      customerInfo = JSON.parse(
        localStorage.getItem(`CUSTOMER_INFO_${queryData.requestId}`)
      );
    }

    let result = {};
    listInputTemp.forEach((el, index) => {
      result = {
        ...result,
        genderContact: "M",
        nameContact: "",
        phoneNumberContact: "",
        emailContact: "",
        [`name_${index}`]: customerInfo ? customerInfo[`name_${index}`] : "",
        [`gender_${index}`]: customerInfo
          ? customerInfo[`gender_${index}`]
          : "M",
        [`outbound_baggage_${index}`]:
          customerInfo && customerInfo[`outbound_baggage_${index}`]
            ? customerInfo[`outbound_baggage_${index}`]
            : undefined,
        [`inbound_baggage_${index}`]:
          customerInfo && customerInfo[`inbound_baggage_${index}`]
            ? customerInfo[`inbound_baggage_${index}`]
            : undefined,
        [`passport_${index}`]: customerInfo
          ? customerInfo[`passport_${index}`]
          : "",
        [`passport_expired_${index}`]: "",
        [`passport_residence_${index}`]: "",
        [`passport_country_${index}`]: "",
        [`insurance_${index}`]: "",
        [`type_${index}`]: el.type,
        [`dob_${index}`]: customerInfo ? customerInfo[`dob_${index}`] : "",
        checkValidContactMail: customerInfo
          ? customerInfo.checkValidContactMail
          : false,
        checkedSame: customerInfo ? customerInfo.checkedSame : false,
      };
    });
    result = {
      ...result,
      checkValidInvoice: customerInfo?.checkValidInvoice,
      companyAddress: customerInfo?.companyAddress,
      companyName: customerInfo?.companyName,
      note: customerInfo?.note || "",
      recipientAddress: customerInfo?.recipientAddress,
      recipientEmail: customerInfo?.recipientEmail,
      recipientName: customerInfo?.recipientName,
      taxIdNumber: customerInfo?.taxIdNumber,
    };
    return result;
  };
  let arrValidate = {};
  listInputTemp.forEach((el, index) => {
    arrValidate = {
      ...arrValidate,
      [`name_${index}`]: yup
        .string()
        .trim()
        .test({
          name: `name_${index}`,
          message: listString.IDS_MT_TEXT_VALIDATE_FULLNAME,
          test: (value) => {
            return value ? value.split(" ").length > 1 : true;
          },
        })
        .required(listString.IDS_MT_TEXT_PLEASE_ENTER_NAME)
        .max(50, listString.IDS_MT_TEXT_VALIDATE_NAME_MAX),
    };
    arrValidate = {
      ...arrValidate,
      [`gender_${index}`]: yup
        .string()
        .trim()
        .required(listString.IDS_MT_TEXT_PLEASE_ENTER_GENDER),
    };

    if (el.type !== CUSTOMER_TYPE.ADULT) {
      arrValidate = {
        ...arrValidate,
        [`dob_${index}`]: yup
          .string()
          .trim()
          .required(listString.IDS_MT_TEXT_PLEASE_ENTER_DOB),
      };
    }
  });
  arrValidate = {
    ...arrValidate,
    nameContact: yup
      .string()
      .trim()
      .test({
        name: "nameContact",
        message: listString.IDS_MT_TEXT_VALIDATE_FULLNAME,
        test: (value) => {
          return value ? value.split(" ").length > 1 : true;
        },
      })
      .max(50, listString.IDS_MT_TEXT_VALIDATE_NAME_MAX)
      .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    phoneNumberContact: yup
      .string()
      .trim()
      .min(9, listString.IDS_MT_TEXT_INVALID_PHONE_NUMBER)
      .max(12, listString.IDS_MT_TEXT_INVALID_PHONE_NUMBER)
      .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    emailContact: yup
      .string()
      .trim()
      .email(listString.IDS_MT_TEXT_INVALID_FORMAT_EMAIL)
      .max(50, listString.IDS_MT_TEXT_VALIDATE_EMAIL_MAX)
      .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    taxIdNumber: yup.string().when("checkValidInvoice", {
      is: true,
      then: yup
        .string()
        .trim()
        .max(50, listString.IDS_MT_TEXT_VALID_MAX_LENGTH_DEFAULT)
        .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    }),
    companyName: yup.string().when("checkValidInvoice", {
      is: true,
      then: yup
        .string()
        .trim()
        .max(250, listString.IDS_MT_TEXT_VALID_MAX_LENGTH_DEFAULT)
        .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    }),
    companyAddress: yup.string().when("checkValidInvoice", {
      is: true,
      then: yup
        .string()
        .trim()
        .max(250, listString.IDS_MT_TEXT_VALID_MAX_LENGTH_DEFAULT)
        .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    }),
    recipientAddress: yup.string().when("checkValidInvoice", {
      is: true,
      then: yup
        .string()
        .trim()
        .max(250, listString.IDS_MT_TEXT_VALID_MAX_LENGTH_DEFAULT)
        .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    }),
    recipientEmail: yup.string().when("checkValidInvoice", {
      is: true,
      then: yup
        .string()
        .trim()
        .email(listString.IDS_MT_TEXT_INVALID_FORMAT_EMAIL)
        .max(50, listString.IDS_MT_TEXT_VALIDATE_EMAIL_MAX)
        .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    }),
    recipientName: yup.string().when("checkValidInvoice", {
      is: true,
      then: yup
        .string()
        .trim()
        .max(50, listString.IDS_MT_TEXT_VALID_MAX_LENGTH_DEFAULT)
        .required(listString.IDS_MT_TEXT_REQUIRED_FIELD),
    }),
    note: yup
      .string()
      .trim()
      .max(1000, listString.IDS_MT_TEXT_VALID_MAX_LENGTH_DEFAULT),
  };
  const storeSchema = yup.object().shape(arrValidate);

  let paramsUrl = { ...queryData };
  delete paramsUrl?.slug;

  useEffect(() => {
    actionsTicketDetail();
    actionsGeneralInformation();
  }, []);

  useEffect(() => {
    if (!isEmpty(customerInfo)) {
      if (!isEmpty(baggageCustomer)) {
        Object.keys(baggageCustomer).forEach((element) => {
          customerInfo[element] = baggageCustomer[element];
        });
      }
      if (buyInsurance !== "-1") {
        customerInfo.checkValidContactMail = buyInsurance;
      }
      actionsVoucher(customerInfo);
    }
  }, [baggageCustomer, buyInsurance]);

  return (
    <Layout
      isHeader
      isFooterBottom
      type="bookingFlight"
      currentStep={0}
      title={title}
      description={description}
      keywords={keywords}
    >
      <Box
        bgcolor={theme.palette.gray.grayLight22}
        display="flex"
        flexDirection="column"
      >
        {!isEmpty(listInputTemp) && (
          <Formik
            initialValues={generateField()}
            validationSchema={storeSchema}
            onSubmit={async (values) => {
              let allowSubmit = true;
              let tempDirectory = [];
              let arrayCustomError = {};
              if (JSON.parse(localStorage.getItem(`DIRECTORY`))) {
                tempDirectory = tempDirectory.concat(
                  JSON.parse(localStorage.getItem(`DIRECTORY`))
                );
              }
              listInputTemp.forEach((element, index) => {
                if (values[`name_${index}`] && values[`gender_${index}`]) {
                  tempDirectory.push({
                    name: values[`name_${index}`],
                    gender: values[`gender_${index}`],
                    passport: values[`passport_${index}`],
                    dob: values[`dob_${index}`],
                    type: values[`type_${index}`],
                  });
                }
                if (
                  element.type === CUSTOMER_TYPE.CHILDREN ||
                  element.type === CUSTOMER_TYPE.BABY
                ) {
                  let checkBOD = formatDateOfBirth(
                    values[`dob_${index}`],
                    element.type
                  );
                  if (checkBOD !== "") {
                    arrayCustomError = {
                      ...arrayCustomError,
                      [`dob_${index}`]: checkBOD,
                    };
                    allowSubmit = false;
                  }
                }
              });
              setCustomError(arrayCustomError);
              const key = "name";
              tempDirectory = [
                ...new Map(
                  tempDirectory.map((item) => [item[key], item])
                ).values(),
              ];
              localStorage.setItem(`DIRECTORY`, JSON.stringify(tempDirectory));
              localStorage.setItem(
                `LAST_REQUEST`,
                JSON.stringify({
                  id: queryData.requestId,
                  name: values?.nameContact,
                  phone: values?.phoneNumberContact,
                  email: values?.emailContact,
                  gender: values?.genderContact,
                  totalGuest: totalGuest,
                })
              );
              localStorage.setItem(
                `CUSTOMER_INFO_${queryData.requestId}`,
                JSON.stringify(values)
              );
              localStorage.setItem(
                `PRICE_INFO_${queryData.requestId}`,
                JSON.stringify({
                  totalPrice: totalMoney,
                  totalGuest: totalGuest || 0,
                  totalPriceInsurance: totalPrice || 0,
                  bagPrice: bagInfo.money || 0,
                })
              );
              if (allowSubmit) {
                router.push({
                  pathname: "/ve-may-bay/payment/paymentMethodList",
                  query: {
                    ...router?.query,
                  },
                });
              }
            }}
          >
            {({ values, errors }) => {
              let totalMoneyBoxPrice = 0;
              let totalMoneyInsurance = totalPrice;
              totalMoneyBoxPrice =
                totalMoney + totalPrice + bagInfo.money - discountMoneyVoucher;

              let detailPriceTicket = {};
              if (!isEmpty(detailTicketOutBound)) {
                const dataBagIn =
                  detailTicketInBound?.ticket?.outbound?.baggages;
                const dataBagOut =
                  detailTicketOutBound?.ticket?.outbound?.baggages;
                let totalInboundBagMoney = 0;
                let totalOutboundBagMoney = 0;
                for (let index = 0; index < totalGuest; index++) {
                  let bagInfoById = dataBagOut.filter(
                    (el) => el.id === values[`outbound_baggage_${index}`]
                  );

                  if (!isEmpty(bagInfoById)) {
                    totalOutboundBagMoney += bagInfoById[0]?.price;
                  }
                  if (detailTicketInBound) {
                    let bagInfoByIdInbound = dataBagIn.filter(
                      (el) => el.id === values[`inbound_baggage_${index}`]
                    );
                    if (!isEmpty(bagInfoByIdInbound)) {
                      totalInboundBagMoney += bagInfoByIdInbound[0]?.price;
                    }
                  }
                }
                let voucherDesc = "";
                if (voucherUsing?.code) {
                  voucherDesc = (
                    <Box>
                      Đã áp dụng mã&nbsp;
                      <b>
                        {voucherUsing?.code}&nbsp;-&nbsp;
                        {voucherUsing?.description}
                      </b>
                    </Box>
                  );
                }
                detailPriceTicket = {
                  outbound: {
                    numAdult: detailTicketOutBound?.searchRequest?.numAdults,
                    adultMoney:
                      parseInt(
                        detailTicketOutBound?.ticket?.outbound?.ticketdetail
                          ?.farePrice
                      ) *
                      parseInt(detailTicketOutBound?.searchRequest?.numAdults),
                    numChildren:
                      detailTicketOutBound?.searchRequest?.numChildren,
                    childMoney:
                      parseInt(
                        detailTicketOutBound?.ticket?.outbound?.ticketdetail
                          ?.farePrice
                      ) *
                      parseInt(
                        detailTicketOutBound?.searchRequest?.numChildren
                      ),
                    numInfants: detailTicketOutBound?.searchRequest?.numInfants,
                    babyMoney:
                      detailTicketOutBound?.ticket?.outbound?.ticketdetail
                        ?.priceInfants,
                    taxFee:
                      parseInt(detailTicketOutBound?.ticket?.totalPrice) -
                      parseInt(
                        detailTicketOutBound?.ticket?.outbound?.ticketdetail
                          ?.farePrice *
                          (detailTicketOutBound?.searchRequest?.numAdults +
                            detailTicketOutBound?.searchRequest?.numChildren)
                      ),
                    baggageMoney: totalOutboundBagMoney,
                    total:
                      detailTicketOutBound?.ticket?.totalPrice +
                      totalOutboundBagMoney,
                  },
                  inbound: detailTicketInBound
                    ? {
                        numAdult: detailTicketInBound?.searchRequest?.numAdults,
                        adultMoney:
                          parseInt(
                            detailTicketInBound?.ticket?.outbound?.ticketdetail
                              ?.farePrice
                          ) *
                          parseInt(
                            detailTicketInBound?.searchRequest?.numAdults
                          ),
                        numChildren:
                          detailTicketInBound?.searchRequest?.numChildren,
                        childMoney:
                          parseInt(
                            detailTicketInBound?.ticket?.outbound?.ticketdetail
                              ?.farePrice
                          ) *
                          parseInt(
                            detailTicketInBound?.searchRequest?.numChildren
                          ),
                        numInfants:
                          detailTicketInBound?.searchRequest?.numInfants,
                        babyMoney:
                          detailTicketInBound?.ticket?.outbound?.ticketdetail
                            ?.priceInfants,
                        taxFee:
                          parseInt(detailTicketInBound?.ticket?.totalPrice) -
                          parseInt(
                            detailTicketInBound?.ticket?.outbound?.ticketdetail
                              ?.farePrice *
                              (detailTicketInBound?.searchRequest?.numAdults +
                                detailTicketInBound?.searchRequest?.numChildren)
                          ),
                        baggageMoney: totalInboundBagMoney,
                        total:
                          detailTicketInBound?.ticket?.totalPrice +
                          totalInboundBagMoney,
                      }
                    : null,
                  insurance: totalMoneyInsurance,
                  voucherCode: voucherUsing?.code || "",
                  voucherMoney: discountMoneyVoucher,
                  voucherDescription: voucherDesc,
                  surchargeMoney: 0,
                  totalPayment: totalMoneyBoxPrice,
                };
              }

              return (
                <Form autoComplete="off" className={classes.container}>
                  <Box className={classes.wrapLeft} ref={ref}>
                    <Box
                      bgcolor={theme.palette.white.main}
                      style={{
                        boxShadow: "0px 1px 1px rgba(0, 0, 0, 0.05)",
                        borderRadius: 8,
                        padding: "24px 24px 0px",
                      }}
                    >
                      <TicketTitle
                        isInBound={!!queryData?.returnDate}
                        paramsUrl={paramsUrl}
                        codeFlight={{
                          depart:
                            detailTicketOutBound?.ticket?.outbound
                              ?.departureCity,
                          arrival:
                            detailTicketOutBound?.ticket?.outbound?.arrivalCity,
                          codeDep:
                            detailTicketOutBound?.ticket?.outbound
                              ?.departureAirport,
                          codeArr:
                            detailTicketOutBound?.ticket?.outbound
                              ?.arrivalAirport,
                        }}
                        date={queryData?.departureDate}
                        dataQuery={queryData}
                        isSmall
                        isTwoWays={false}
                      />
                      <Divider className={classes.divider} />
                      <FlightTicketDetail
                        ticket={detailTicketOutBound?.ticket?.outbound}
                        data={detailTicketOutBound}
                        isTransit={
                          !!detailTicketOutBound?.ticket?.outbound
                            ?.transitTickets
                        }
                        airlineInfo={detailTicketOutBound?.agency}
                        title=""
                        isSimple={
                          detailTicketInBound
                            ? detailTicketStatus
                            : !detailTicketStatus
                        }
                        isSeat={true}
                        isSlider
                        title2Ways={null}
                      />
                      {detailTicketInBound && (
                        <Box>
                          <Divider
                            style={{ height: 4, margin: "0px -24px 12px" }}
                          />
                          {queryData?.returnDate && (
                            <TicketTitle
                              isInBound={!!queryData?.returnDate}
                              paramsUrl={paramsUrl}
                              codeFlight={{
                                depart:
                                  detailTicketOutBound?.ticket?.outbound
                                    ?.departureCity,
                                arrival:
                                  detailTicketOutBound?.ticket?.outbound
                                    ?.arrivalCity,
                                codeDep:
                                  detailTicketOutBound?.ticket?.outbound
                                    ?.departureAirport,
                                codeArr:
                                  detailTicketOutBound?.ticket?.outbound
                                    ?.arrivalAirport,
                              }}
                              date={queryData?.returnDate}
                              dataQuery={queryData}
                              isSmall
                              isTwoWays
                            />
                          )}
                          <FlightTicketDetail
                            ticket={detailTicketInBound?.ticket?.outbound}
                            data={detailTicketInBound}
                            isTransit={
                              !!detailTicketInBound?.ticket?.outbound
                                ?.transitTickets
                            }
                            airlineInfo={detailTicketInBound?.agency}
                            title=""
                            isSimple={detailTicketStatus}
                            isSeat={true}
                            isSlider
                            title2Ways={null}
                          />
                        </Box>
                      )}
                      {!!queryData?.returnDate && (
                        <>
                          <Box className={classes.body}>
                            <Divider className={classes.divider2} />
                          </Box>
                          <Button
                            className={classes.button}
                            variant="contained"
                            disableElevation
                            onClick={() => {
                              setDetailTicketStatus(!detailTicketStatus);
                            }}
                          >
                            {detailTicketStatus
                              ? "Xem chi tiết chuyến bay"
                              : "Thu gọn"}
                            <IconArrowDownToggle
                              className={`svgFillAll ${
                                detailTicketStatus
                                  ? classes.IconArrowDownToggle
                                  : classes.IconArrowUpToggle
                              }`}
                            />
                          </Button>
                        </>
                      )}
                    </Box>
                    <Box className={classes.boxContact} mt={2}>
                      <ContactInfo />
                    </Box>
                    <Box mt={2}>
                      <CustomerInfo
                        listInputTemp={listInputTemp}
                        customError={customError}
                      />
                    </Box>
                    <Box mt={2}>
                      <Baggage
                        outboundBaggageInfo={
                          detailTicketOutBound?.ticket?.outbound
                        }
                        inboundBaggageInfo={
                          detailTicketInBound?.ticket?.outbound
                        }
                        totalGuest={
                          parseInt(queryData.adultCount) +
                          parseInt(queryData.childCount)
                        }
                        customerInfo={listInputTemp}
                        bagInfo={bagInfo}
                        setBagInfo={setBagInfo}
                        setBaggageCustomer={setBaggageCustomer}
                        setMessageAlert={setMessageAlert}
                        detailTicketStatus={detailTicketStatus}
                      />
                    </Box>
                    <InsuranceBox
                      setBuyInsurance={setBuyInsurance}
                      paramsInsurance={paramsInsurance}
                      totalPrice={totalPrice}
                      setTotalPrice={setTotalPrice}
                    />
                    <Box mt={2} mb={2}>
                      <Invoice />
                    </Box>
                    <Box display="flex" justifyContent="flex-end" mb={8}>
                      <ButtonComponent
                        type="submit"
                        backgroundColor={theme.palette.secondary.main}
                        height={48}
                        width={170}
                        borderRadius={8}
                        className={classes.rightBox}
                      >
                        <Typography variant="subtitle1">
                          {listString.IDS_MT_TEXT_CONTINUE}
                        </Typography>
                      </ButtonComponent>
                    </Box>
                  </Box>
                  <Box className={classes.wrapRight} mb={2}>
                    <Box className={classes.boxPriceDetail}>
                      <PriceDetail item={detailPriceTicket} />
                    </Box>
                  </Box>
                </Form>
              );
            }}
          </Formik>
        )}
        <DialogShowMessage
          message={messageAlert}
          toggleDrawer={toggleDrawerAlert}
        />
      </Box>
    </Layout>
  );
};

export default DesktopContent;
