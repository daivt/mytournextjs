import AuthWrapper from "@components/layout/desktop/components/AuthWrapper";
import { useSystem } from "@contextProvider/ContextProvider";
import { Box, Typography } from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import MuiFormControlLabel from "@material-ui/core/FormControlLabel";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles, withStyles } from "@material-ui/styles";
import {
  IconArrowDownToggle,
  IconCheckBox,
  IconCheckBoxActive,
  IconUserLogin,
} from "@public/icons";
import FieldSelectContent from "@src/form/FieldSingleSelectContent";
import FieldTextContent from "@src/form/FieldTextContent";
import { GENDER_LIST, listString, listEventFlight } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import { validPhoneNumberRegex, validNameRegex } from "@utils/regex";
import { useFormikContext } from "formik";
import { useEffect, useState } from "react";
import voca from "voca";
import * as gtm from "@utils/gtm";
const useStyles = makeStyles((theme) => ({
  wrapTitle: {
    fontSize: 18,
    lineHeight: "22px",
  },
  boxDesc: {
    color: theme.palette.gray.grayDark8,
    lineHeight: "17px",
    margin: "8px 0px 20px",
  },
  wrapContent: {
    display: "flex",
    flexDirection: "column",
    padding: 24,
    background: theme.palette.white.main,
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    boxSizing: "border-box",
    boxShadow: "0px 1px 1px rgba(0, 0, 0, 0.05)",
    borderRadius: 8,
  },
  inputStyle: {
    "& > input": {
      padding: 0,
    },
  },
  inputSelectStyle: {
    "& > input": {
      padding: 0,
      height: "auto",
    },
    marginRight: 16,
  },
  descriptionText: {
    display: "flex",
    alignItems: "center",
    background: "rgba(0, 182, 243, 0.1)",
    borderRadius: 8,
    padding: "10px 14px",
    lineHeight: "17px",
    marginBottom: 20,
    cursor: "pointer",
  },
}));

const FormControlLabel = withStyles(() => ({
  root: {
    margin: "0 0 0 -12px",
  },
  label: {
    fontSize: 14,
    fontWeight: "normal",
    lineHeight: "17px",
  },
}))(MuiFormControlLabel);
const MarkSameInfoCheckbox = withStyles((theme) => ({
  root: {
    color: theme.palette.gray.grayLight25,
    "&$checked": {
      color: theme.palette.blue.blueLight8,
    },
  },
  checked: {},
}))((props) => (
  <Checkbox
    color="default"
    {...props}
    icon={<IconCheckBox />}
    checkedIcon={<IconCheckBoxActive />}
  />
));

const ContactInfo = () => {
  const classes = useStyles();
  const theme = useTheme();
  const { setFieldValue, values, errors } = useFormikContext();
  const { systemReducer } = useSystem();
  const informationUser = systemReducer.informationUser || {};

  const [stateCheck, setStateCheck] = useState({
    checkedSameInfo: false,
  });

  const getStyleInput = {
    minHeight: 40,
    width: "100%",
    borderTop: "none",
    borderRight: "none",
    borderLeft: "none",
    borderRadius: 0,
  };

  const handleCheckbox = (event) => {
    setStateCheck({ ...stateCheck, [event.target.name]: event.target.checked });
    setFieldValue("checkedSame", event.target.checked);
    if (event.target.checked) {
      setFieldValue("gender_0", values.genderContact);
      setFieldValue("name_0", voca.latinise(values.nameContact));
    }
  };

  useEffect(() => {
    setStateCheck({
      ...stateCheck,
      checkedSameInfo: values?.checkedSame,
    });
  }, []);

  useEffect(() => {
    const ISSERVER = typeof window === "undefined";
    let lastBookData = {};
    if (!ISSERVER) {
      lastBookData = JSON.parse(localStorage.getItem(`LAST_REQUEST`));
    }
    if (!isEmpty(lastBookData)) {
      setFieldValue("genderContact", lastBookData.gender || "");
      setFieldValue("nameContact", lastBookData.name || "");
      setFieldValue("phoneNumberContact", lastBookData.phone || "");
      setFieldValue("emailContact", lastBookData.email || "");
    } else {
      if (!isEmpty(informationUser)) {
        if (isEmpty(values.nameContact)) {
          setFieldValue("nameContact", informationUser.name || "");
        }
        if (isEmpty(values.phoneNumberContact)) {
          setFieldValue("phoneNumberContact", informationUser.phoneInfo || "");
        }
        if (isEmpty(values.emailContact)) {
          setFieldValue("emailContact", informationUser.emailInfo || "");
        }
      }
    }
  }, [informationUser]);

  const handleBlur = (type = "") => {
    if (type === "phoneNumber") {
      gtm.addEventGtm(listEventFlight.FlightInitCheckout);
    } else {
      gtm.addEventGtm(listEventFlight.FlightInitCheckout);
    }
  };

  return (
    <Box className={classes.wrapContent}>
      <Typography className={classes.wrapTitle} variant="subtitle1">
        {listString.IDS_MT_TEXT_INFO_CONTACT}
      </Typography>
      <Typography
        className={classes.boxDesc}
        variant="caption"
        style={{ marginBottom: !isEmpty(informationUser) ? 20 : 8 }}
      >
        {listString.IDS_MT_TEXT_INFO_FLIGHT_CONTACT_DESC}
      </Typography>
      {isEmpty(informationUser) && (
        <AuthWrapper typeModal="MODAL_LOGIN">
          <Typography
            variant="caption"
            className={classes.descriptionText}
            onClick={() => {}}
          >
            <IconUserLogin />
            &nbsp;
            <Typography
              component="span"
              variant="subtitle2"
              style={{
                color: theme.palette.blue.blueLight8,
              }}
            >
              {listString.IDS_MT_TEXT_LOGIN}
            </Typography>
            &nbsp;
            {listString.IDS_MT_DES_LOGIN_FLIGHT}
          </Typography>
        </AuthWrapper>
      )}

      <Box display="flex">
        <FieldSelectContent
          name="genderContact"
          options={GENDER_LIST || []}
          getOptionLabel={(v) => v.label}
          onSelectOption={(value) => {
            setFieldValue("genderContact", value);
            if (stateCheck.checkedSameInfo) {
              setFieldValue("gender_0", value);
            }
          }}
          inputStyle={{
            minHeight: 40,
            width: 76,
            border: "none",
            borderBottom: `solid 1px ${theme.palette.gray.grayLight25}`,
            borderRadius: 0,
          }}
          formControlStyle={{
            minWidth: 76,
            width: 76,
          }}
          className={classes.inputSelectStyle}
          iconRight={<IconArrowDownToggle />}
        />
        <FieldTextContent
          formControlStyle={{ marginRight: 0 }}
          name="nameContact"
          placeholder={listString.IDS_MT_TEXT_CONTACT_NAME}
          inputStyle={getStyleInput}
          inputProps={{
            autoComplete: "off",
          }}
          onChange={(event) => {
            validNameRegex.test(voca.latinise(event.target.value)) &&
              setFieldValue(
                "nameContact",
                voca.latinise(event.target.value).toUpperCase()
              );
            if (stateCheck.checkedSameInfo) {
              setFieldValue(
                "name_0",
                voca.latinise(event.target.value).toUpperCase()
              );
            }
          }}
          className={classes.inputStyle}
          helperTextStyle={{ paddingTop: 8 }}
        />
      </Box>
      <Box display="flex">
        <FieldTextContent
          name="phoneNumberContact"
          placeholder={listString.IDS_MT_TEXT_PLACEHOLDER_ENTER_PHONE_NUMBER}
          inputStyle={getStyleInput}
          inputProps={{
            autoComplete: "off",
          }}
          formControlStyle={{
            marginRight: 24,
          }}
          onBlur={() => handleBlur("phoneNumber")}
          styleLabel={classes.styleLabel}
          className={classes.inputStyle}
          helperTextStyle={{ paddingTop: 8 }}
          onChange={(e) => {
            validPhoneNumberRegex.test(e.target.value) &&
              setFieldValue("phoneNumberContact", e.target.value);
          }}
        />
        <FieldTextContent
          name="emailContact"
          placeholder={listString.IDS_MT_TEXT_ENTER_EMAIL}
          inputStyle={getStyleInput}
          inputProps={{
            autoComplete: "off",
          }}
          formControlStyle={{
            marginRight: 0,
          }}
          onBlur={() => handleBlur("email")}
          styleLabel={classes.styleLabel}
          className={classes.inputStyle}
          helperTextStyle={{ paddingTop: 8 }}
        />
      </Box>

      <FormControlLabel
        control={
          <MarkSameInfoCheckbox
            checked={stateCheck.checkedSameInfo}
            onChange={handleCheckbox}
            name="checkedSameInfo"
          />
        }
        label={listString.IDS_MT_TEXT_CONTACT_SAME_INFO_CUSTOMER}
      />
    </Box>
  );
};

export default ContactInfo;
