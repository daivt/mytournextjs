/* eslint-disable no-nested-ternary */
import { getBankTransferInfo, getGeneralInformation } from "@api/flight";
import OrderDetail from "@components/desktop/flight/payment/orderDetail/OrderDetail";
import WayFlightItemDetail from "@components/desktop/flight/payment/result/WayFlightItemDetail";
import Layout from "@components/layout/desktop/Layout";
import FlightBankTransfer from "@components/mobile/flight/payment/result/FlightBankTransfer";
import { Box, Divider, makeStyles, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import {
  IconBookingFlightFail,
  IconBookingFlightSuccess,
  IconChevronRight,
} from "@public/icons";
import ButtonComponent from "@src/button/Button";
import Link from "@src/link/Link";
import {
  flightPaymentMethodCode,
  GENDER_LIST,
  listString,
  paymentTypes,
  routeStatic,
} from "@utils/constants";
import {
  getExpiredTimeMillisecond,
  isEmpty,
  resetDataAfterBookTicket,
} from "@utils/helpers";
import { C_DATE_TIME_FORMAT, DATE_FORMAT, HOUR_MINUTE } from "@utils/moment";
import moment from "moment";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  boxStatus: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    textAlign: "center",
  },
  boxTicket: {
    padding: "16px 16px 22px 16px",
    display: "flex",
    flexDirection: "column",
  },
  boxCode: {
    background: theme.palette.white.main,
    borderRadius: 20,
    display: "flex",
    justifyContent: "space-around",
    textAlign: "center",
    alignItems: "center",
    padding: "24px 0",
  },
  boxHolder: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: theme.palette.orange.orangeLight1,
    color: theme.palette.orange.main,
    padding: "4px 8px",
    borderRadius: 4,
    marginBottom: 8,
  },
  copy: {
    display: "flex",
    justifyContent: "flex-end",
    color: theme.palette.blue.blueLight8,
    alginItems: "center",
    fontSize: "14px",
    lineHeight: "17px",
  },
  iconBank: {
    width: "100%",
    height: "100%",
    objectFit: "contain",
  },
  bank: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
  },
  bankItem: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.gray.grayLight23}`,
    height: 24,
    width: 48,
    backgroundColor: theme.palette.white.main,
  },
  wrapBank: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    whiteSpace: "nowrap",
    fontSize: 14,
    lineHeight: "17px",
    marginTop: 12,
  },
  bankText: {
    fontWeight: 600,
    whiteSpace: "nowrap",
    overflow: "hidden !important",
    textOverflow: "ellipsis",
    width: 150,
  },
  infoBank: {
    borderBottom: `1px solid ${theme.palette.gray.grayLight23}`,
    marginBottom: 12,
  },
  divider: {
    height: 1,
    color: theme.palette.gray.grayLight22,
    width: "100%",
  },
  wrapContainer: {
    padding: "32px 0",
    background: theme.palette.gray.grayLight22,
    position: "relative",
  },
  container: {
    margin: "auto",
    maxWidth: 630,
    height: "auto",
    padding: 24,
    background: theme.palette.white.main,
    borderRadius: 8,
    borderTop: "solid 4px",
  },
  wrapBorder: {
    display: "flex",
    justifyContent: "space-around",
    margin: "auto",
    maxWidth: 630,
  },
  circle: {
    width: 16,
    height: 16,
    transform: "rotate(-45deg)",
    borderRadius: "50%",
    borderLeftColor: "transparent",
    borderBottomColor: "transparent",
    background: theme.palette.gray.grayLight22,
    marginTop: -8,
  },
  boxCodeInfo: {
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
    marginTop: 12,
  },
  orderCode: {
    padding: "0 16px",
    borderRadius: 8,
    boxSizing: "border-box",
    border: `1px dashed ${theme.palette.gray.grayLight25}`,
    background: theme.palette.gray.grayLight26,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    textAlign: "center",
    height: 84,
    width: "50%",
  },
  codeLeft: {
    color: theme.palette.blue.blueLight8,
    fontSize: 18,
    lineHeight: "22px",
  },
  codeRight: {
    fontSize: 18,
    lineHeight: "22px",
    color: theme.palette.pink.main,
  },
  ticketItem: {
    width: "100%",
    padding: "16px 0",
  },
  boxDownload: {
    margin: "22px auto 30px",
    maxWidth: "630px",
    display: "flex",
    justifyContent: "center",
  },
}));

const PaymentResult = ({
  orderDetail = {},
  paymentType = paymentTypes.FAIL,
  fromBankTransfer = false,
  title = "",
  description = "",
  keywords = "",
}) => {
  const router = useRouter();
  const theme = useTheme();
  const classes = useStyles();
  const [viewOrderDetail, setViewOrderDetail] = useState(false);
  const [bankTransferInfo, setBankTransferInfo] = useState({});
  const [bankTransferOptions, setBankTransferOptions] = useState([]);
  const toggleDrawerOrderDetail = (open) => () => {
    setViewOrderDetail(open);
  };

  const [airlines, setAirlines] = useState();
  const [airlineOutBoundInfo, setAirlinesOutBoundInfo] = useState();
  const [airlineInBoundInfo, setAirlinesInBoundInfo] = useState();
  const actionsGeneralInformation = async () => {
    const { data } = await getGeneralInformation();
    if (data?.code === 200 || data?.code === "200") {
      setAirlines(data?.data?.airlines);
      setAirlinesOutBoundInfo(
        data?.data?.airlines.find(
          (al) => al.aid === orderDetail.outbound?.airlineId
        )
      );
      if (orderDetail.isTwoWay) {
        setAirlinesInBoundInfo(
          data?.data?.airlines.find(
            (al) => al.aid === orderDetail.inbound?.airlineId
          )
        );
      }
    }
  };

  const actionGetBankTransfer = async () => {
    const { data } = await getBankTransferInfo(orderDetail?.bankTransferCode);
    if (data.code == 200) {
      setBankTransferInfo(data.data?.transferInfo);
      setBankTransferOptions(data.data?.transferOptions);
    }
  };

  useEffect(() => {
    actionsGeneralInformation();
    if (
      !isEmpty(orderDetail?.bankTransferCode) &&
      (orderDetail?.paymentMethodCode ===
        flightPaymentMethodCode.BANK_TRANSFER ||
        orderDetail?.paymentMethodCode ===
          flightPaymentMethodCode.BANK_TRANSFER_2)
    ) {
      actionGetBankTransfer();
    }
    //Clear book info
    const ISSERVER = typeof window === "undefined";
    let lastBookData = {};
    let customerData = {};
    if (!ISSERVER) {
      lastBookData = JSON.parse(localStorage.getItem(`LAST_REQUEST`));
    }

    if (lastBookData?.id) {
      customerData = JSON.parse(
        localStorage.getItem(`CUSTOMER_INFO_${lastBookData.id}`)
      );
      if (!isEmpty(customerData)) {
        resetDataAfterBookTicket(
          customerData,
          `CUSTOMER_INFO_${lastBookData.id}`,
          lastBookData.totalGuest
        );
      }
    }
  }, []);

  return (
    <Layout
      isHeader
      isFooterBottom
      type="bookingFlight"
      currentStep={2}
      title={title}
      description={description}
      keywords={keywords}
    >
      <Box className={classes.wrapContainer}>
        <Box
          className={classes.container}
          style={{
            borderTopColor:
              paymentType === paymentTypes.FAIL
                ? theme.palette.red.redLight5
                : theme.palette.green.greenLight7,
          }}
        >
          <Box className={classes.boxStatus}>
            {paymentType === paymentTypes.FAIL ? (
              <>
                <IconBookingFlightFail />
                <Typography
                  variant="subtitle1"
                  style={{ marginTop: 12, marginBottom: 8 }}
                >
                  {listString.IDS_TEXT_FLIGHT_BOOKING_FAIL}
                </Typography>
                <Typography variant="caption">
                  {listString.IDS_TEXT_FLIGHT_BOOKING_RESULT_DESC_FAIL}
                </Typography>
              </>
            ) : paymentType === paymentTypes.SUCCESS && !fromBankTransfer ? (
              <>
                <IconBookingFlightSuccess />
                <Typography
                  variant="subtitle1"
                  style={{ marginTop: 12, marginBottom: 8 }}
                >
                  {listString.IDS_TEXT_FLIGHT_BOOKING_SUCCESS}
                </Typography>
                <Typography variant="caption">
                  {listString.IDS_TEXT_FLIGHT_BOOKING_RESULT_DESC_SUCCESS}
                </Typography>
              </>
            ) : paymentType === paymentTypes.SUCCESS && fromBankTransfer ? (
              <>
                <IconBookingFlightSuccess />
                <Typography
                  variant="subtitle1"
                  style={{ marginTop: 12, marginBottom: 8 }}
                >
                  {listString.IDS_TEXT_FLIGHT_BOOKING_SUCCESS}
                </Typography>
                <Typography variant="caption">
                  {`${listString.IDS_TEXT_FLIGHT_BOOKING_BANK_TRANSFER_1} `}
                  <Typography variant="caption" style={{ fontWeight: 600 }}>
                    {orderDetail?.mainContact?.email}.
                    <Typography variant="caption">
                      {listString.IDS_TEXT_FLIGHT_BOOKING_BANK_TRANSFER_2}
                      <Typography
                        variant="caption"
                        style={{
                          fontWeight: 600,
                          color: theme.palette.red.redLight5,
                        }}
                      >
                        {!isEmpty(bankTransferInfo?.expiredTime)
                          ? ` ${moment(
                              bankTransferInfo?.expiredTime,
                              C_DATE_TIME_FORMAT
                            ).format(HOUR_MINUTE)}, `
                          : " "}
                        {!isEmpty(bankTransferInfo?.expiredTime)
                          ? moment(
                              bankTransferInfo?.expiredTime,
                              C_DATE_TIME_FORMAT
                            )
                              .format("ddd")
                              .replace("T2", "Thứ hai")
                              .replace("T3", "Thứ ba")
                              .replace("T4", "Thứ tư")
                              .replace("T5", "Thứ năm")
                              .replace("T6", "Thứ sáu")
                              .replace("T7", "Thứ bảy")
                              .replace("CN", "Chủ Nhật")
                          : "-"}
                        {","}
                        {!isEmpty(bankTransferInfo?.expiredTime)
                          ? ` ngày ${moment(
                              bankTransferInfo?.expiredTime,
                              C_DATE_TIME_FORMAT
                            ).format(DATE_FORMAT)}`
                          : ""}
                      </Typography>
                    </Typography>
                  </Typography>
                </Typography>
              </>
            ) : (
              <>
                <IconBookingFlightSuccess />
                <Typography
                  variant="subtitle1"
                  style={{ marginTop: 12, marginBottom: 8 }}
                >
                  {listString.IDS_TEXT_FLIGHT_BOOKING_HOLD_SUCCESS}
                </Typography>
                <Box className={classes.boxHolder}>
                  <Typography variant="caption">{`${
                    listString.IDS_TEXT_FLIGHT_BOOKING_HOLD_TIME
                  } ${getExpiredTimeMillisecond(
                    orderDetail?.expiredTime || 0
                  )}`}</Typography>
                </Box>
                <Typography variant="caption">
                  {listString.IDS_TEXT_FLIGHT_BOOKING_RESULT_DESC_SUCCESS_PC}
                </Typography>
                <Typography variant="caption">
                  {listString.IDS_TEXT_FLIGHT_BOOKING_RESULT_DESC_SUCCESS}
                </Typography>
              </>
            )}
            <Box className={classes.boxCodeInfo}>
              <Box
                className={classes.orderCode}
                style={{
                  width: paymentType === paymentTypes.FAIL ? "100%" : "50%",
                  marginRight: paymentType === paymentTypes.FAIL ? 0 : 16,
                }}
              >
                <Typography variant="caption" style={{ lineHeight: "17px" }}>
                  {listString.IDS_TEXT_FLIGHT_ORDER_CODE}
                </Typography>
                <Typography variant="subtitle1" className={classes.codeLeft}>
                  {orderDetail.orderCode}
                </Typography>
              </Box>
              {paymentType !== paymentTypes.FAIL && (
                <>
                  {orderDetail.isTwoWay ? (
                    <Box className={classes.orderCode} flexDirection="column">
                      <Box
                        display="flex"
                        justifyContent="space-between"
                        alignItems="center"
                        width="100%"
                        pt={2}
                      >
                        <Typography
                          variant="caption"
                          style={{ lineHeight: "17px" }}
                        >
                          {`${listString.IDS_TEXT_FLIGHT_BOOK_CODE_OUTBOUND} `}
                        </Typography>
                        <Typography className={classes.codeRight}>
                          {orderDetail?.outboundPnrCode}
                        </Typography>
                      </Box>
                      <Box
                        display="flex"
                        justifyContent="space-between"
                        alignItems="center"
                        width="100%"
                        pb={2}
                      >
                        <Typography
                          variant="caption"
                          style={{ lineHeight: "17px" }}
                        >
                          {`${listString.IDS_TEXT_FLIGHT_BOOK_CODE_INBOUND} `}
                        </Typography>
                        <Typography className={classes.codeRight}>
                          {orderDetail?.inboundPnrCode}
                        </Typography>
                      </Box>
                    </Box>
                  ) : (
                    <Box className={classes.orderCode}>
                      <Typography
                        variant="caption"
                        style={{ lineHeight: "17px" }}
                      >
                        {`${listString.IDS_TEXT_FLIGHT_BOOK_CODE} `}
                      </Typography>
                      <Typography className={classes.codeRight}>
                        {orderDetail?.outboundPnrCode}
                      </Typography>
                    </Box>
                  )}
                </>
              )}
            </Box>
            <ButtonComponent
              color={theme.palette.blue.blueLight8}
              typeButton="text"
              width="fit-content"
              handleClick={toggleDrawerOrderDetail(true)}
            >
              <Typography variant="caption" style={{ marginRight: 8 }}>
                {listString.IDS_TEXT_ORDER_DETAIL}
              </Typography>
              <IconChevronRight
                className="svgFillAll"
                stroke={theme.palette.blue.blueLight8}
              />
            </ButtonComponent>
            <OrderDetail
              open={viewOrderDetail}
              handleClose={toggleDrawerOrderDetail}
              orderDetail={orderDetail}
              airlines={airlines}
            />
            <Divider className={classes.divider} />
            {!isEmpty(bankTransferInfo) && (
              <Box
                mt={2}
                padding="16px 24px"
                width="100%"
                boxSizing="border-box"
                borderRadius="8px"
                border={`1px dashed ${theme.palette.gray.grayLight25}`}
                bgcolor={theme.palette.gray.grayLight26}
                display="flex"
                flexDirection="column"
                textAlign="left"
              >
                <FlightBankTransfer
                  bankTransferInfo={bankTransferInfo}
                  bankTransferOptions={bankTransferOptions}
                />
              </Box>
            )}
            <Box
              display="flex"
              justifyContent="space-between"
              width="100%"
              padding={"16px 0"}
            >
              <Box
                display="flex"
                textAlign="left"
                width="50%"
                flexDirection="column"
              >
                <Typography variant="subtitle1">
                  {listString.IDS_MT_TEXT_INFO_CONTACT}
                </Typography>
                <Box display="flex" flexDirection="column" mt={1}>
                  <Box display="flex">
                    <Box width="90px" mr={1}>
                      <Typography variant="caption">
                        {`${listString.IDS_MT_TEXT_CONTACT_NAME_SIMPLE}:`}
                      </Typography>
                    </Box>
                    <Box width="170px" wordWrap="break-word">
                      <Typography variant="caption">
                        {orderDetail?.mainContact?.fullName.toUpperCase()}
                      </Typography>
                    </Box>
                  </Box>
                  <Box display="flex">
                    <Box width="90px" mr={1}>
                      <Typography variant="caption">
                        {`${listString.IDS_MT_TEXT_PHONE_NUMBER}:`}
                      </Typography>
                    </Box>
                    <Box width="170px" wordWrap="break-word">
                      <Typography variant="caption">
                        {orderDetail?.mainContact?.phone1}
                      </Typography>
                    </Box>
                  </Box>
                  <Box display="flex">
                    <Box width="90px" mr={1}>
                      <Typography variant="caption">
                        {`${listString.IDS_MT_TEXT_EMAIL}:`}
                      </Typography>
                    </Box>
                    <Box width="170px" wordWrap="break-word">
                      <Typography variant="caption">
                        {orderDetail?.mainContact?.email}
                      </Typography>
                    </Box>
                  </Box>
                </Box>
              </Box>
              <Box
                display="flex"
                textAlign="left"
                width="50%"
                flexDirection="column"
              >
                <Typography variant="subtitle1">
                  {listString.IDS_MT_TEXT_INFO_CUSTOMER_FLIGHT}
                  <Typography
                    variant="caption"
                    style={{ color: theme.palette.gray.grayDark7 }}
                  >
                    {` (${orderDetail?.numGuests || 0} ${
                      listString.IDS_MT_TEXT_SINGLE_CUSTOMER
                    })`}
                  </Typography>
                </Typography>
                <Box display="flex" flexDirection="column" mt={1}>
                  {orderDetail?.guests.length &&
                    orderDetail?.guests.map((item, index) => {
                      return (
                        <>
                          <Box display="flex">
                            <Box width="32px" mr={1}>
                              <Typography variant="caption">
                                {`${
                                  GENDER_LIST.find(
                                    (el) => el.id === item?.gender
                                  )?.label
                                }:`}
                              </Typography>
                            </Box>
                            <Box width="170px" wordWrap="break-word">
                              <Typography variant="caption">
                                {item?.fullName.toUpperCase()}
                              </Typography>
                            </Box>
                          </Box>
                        </>
                      );
                    })}
                </Box>
              </Box>
            </Box>
            <Divider className={classes.divider} />
            <Box className={classes.ticketItem}>
              <WayFlightItemDetail
                item={orderDetail?.outbound}
                airlineInfo={airlineOutBoundInfo}
              />
              {orderDetail.isTwoWay && (
                <Box mt={2}>
                  <WayFlightItemDetail
                    item={orderDetail?.inbound}
                    airlineInfo={airlineInBoundInfo}
                  />
                </Box>
              )}
            </Box>
          </Box>
        </Box>
        <Box className={classes.wrapBorder}>
          {Array(26)
            .fill(0)
            .map((el, index) => (
              <Box key={index} className={classes.circle} />
            ))}
        </Box>
        <Box className={classes.boxDownload}>
          <Link href="https://k5jr5.app.goo.gl/53g6">
            <img
              style={{ width: "630px", height: "96px" }}
              src={
                "https://storage.googleapis.com/tripi-assets/mytour/banner/icon_download_banner.svg"
              }
            />
          </Link>
        </Box>
        <Box display="flex" flexDirection="column" alignItems="center">
          <ButtonComponent
            backgroundColor={theme.palette.secondary.main}
            height={40}
            borderRadius={8}
            width="fit-content"
            handleClick={() => {
              router.push({
                pathname: routeStatic.HOME.href,
              });
            }}
          >
            <Typography variant="subtitle2">
              {listString.IDS_MT_TEXT_NOT_FOUND_403_DES_3}
            </Typography>
          </ButtonComponent>
        </Box>
      </Box>
    </Layout>
  );
};

export default PaymentResult;
