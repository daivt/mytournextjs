import { Box, Grid, Typography, useTheme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconArrowFlightOrder, IconDot } from "@public/icons";
import Image from "@src/image/Image";
import { DATE_FORMAT } from "@utils/moment";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  airlineLogo: {
    width: 20,
    height: 20,
  },
  iconDot: {
    margin: 6,
  },
  airlineName: {
    color: theme.palette.black.black3,
    whiteSpace: "nowrap",
  },
  flightCode: {
    color: theme.palette.black.black3,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  warpAirBrand: {
    display: "flex",
    alignItems: "center",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    overflow: "hidden",
    marginLeft: 8,
    color: theme.palette.black.black3,
  },
}));

const WayFlightItemDetail = ({ item = {}, airlineInfo = {} }) => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Box display="flex" flexDirection="column">
      <Grid container>
        <Grid item xl={5} lg={5} sm={5} md={5} xs={5}>
          <Box display="flex" justifyContent="flex-start" textAlign="start">
            <Typography variant="subtitle1">{`${item?.departureCity} (${item?.fromAirport})`}</Typography>
          </Box>
        </Grid>
        <Grid item xl={2} lg={2} sm={2} md={2} xs={2}>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
          >
            <div
              style={{
                background:
                  "linear-gradient(270deg, #A0AEC0 0%, rgba(160, 174, 192, 0) 100%)",
                width: 24,
                height: 1,
                marginRight: 10,
              }}
            ></div>
            <IconArrowFlightOrder />
            <div
              style={{
                background:
                  "linear-gradient(270deg, rgba(160, 174, 192, 0) 0%, #A0AEC0 100%)",
                width: 24,
                height: 1,
                marginLeft: 10,
              }}
            ></div>
          </Box>
        </Grid>
        <Grid item xl={5} lg={5} sm={5} md={5} xs={5}>
          <Box display="flex" justifyContent="flex-end" textAlign="end">
            <Typography variant="subtitle1">{`${item?.arrivalCity} (${item?.toAirport})`}</Typography>
          </Box>
        </Grid>
      </Grid>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        mt={4 / 8}
      >
        <Typography
          variant="caption"
          style={{ color: theme.palette.gray.grayDark7, lineHeight: "17px" }}
        >
          {item?.fromAirportName}
        </Typography>
        <Typography
          variant="caption"
          style={{ color: theme.palette.gray.grayDark7, lineHeight: "17px" }}
        >
          {item?.toAirportName}
        </Typography>
      </Box>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        mt={4 / 8}
      >
        <Typography variant="caption" style={{ lineHeight: "17px" }}>
          {`${item?.departureTime}, ${moment(
            item?.departureDate,
            DATE_FORMAT
          ).format("DD")} tháng ${moment(item?.arrivalDate, DATE_FORMAT).format(
            "MM"
          )}`}
        </Typography>
        <Typography variant="caption" style={{ lineHeight: "17px" }}>
          {`${item?.arrivalTime}, ${moment(
            item?.arrivalDate,
            DATE_FORMAT
          ).format("DD")} tháng ${moment(item?.arrivalDate, DATE_FORMAT).format(
            "MM"
          )}`}
        </Typography>
      </Box>
      <Box display="flex" alignItems="center" mt={4 / 8}>
        <Image srcImage={airlineInfo.logo} className={classes.airlineLogo} />
        <Box className={classes.warpAirBrand}>
          <Typography
            variant="caption"
            style={{ lineHeight: "17px" }}
            className={classes.airlineName}
          >
            {airlineInfo.name}
          </Typography>
          <IconDot className={classes.iconDot} />
          <Typography
            variant="caption"
            style={{ lineHeight: "17px" }}
            className={classes.flightCode}
          >
            {item?.flightCode}
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};
export default WayFlightItemDetail;
