import { searchHotTickets } from "@api/flight";
import SlideShow from "@components/common/slideShow/SlideShow";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconDropDown } from "@public/icons";
import { C_DATE_FORMAT, DATE_FORMAT_BACK_END } from "@utils/moment";
import { uniqBy } from "lodash";
import moment from "moment";
import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import { isEmpty } from "utils/helpers";
import FlightDatePrice from "./FlightDatePrice";

const customStyle = {
  background: "#EDF2F7",
  height: 28,
  width: 28,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  borderRadius: "100px",
  boxShadow: "0px 8px 12px rgb(0 0 0 / 10%)",
  cursor: "pointer",
  position: "absolute",
  zIndex: 1,
  top: "50%",
  transform: "translate(-50%, -50%)",
};

function SamplePrevArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, left: 22, marginRight: 8 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconDropDown style={{ transform: "rotate(90deg)" }} />
    </div>
  );
}

function SampleNextArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, right: -4 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconDropDown style={{ transform: "rotate(270deg)" }} />
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  wrapHeaderListing: {
    backgroundColor: theme.palette.white.main,
    margin: 2,
    marginLeft: 24,
    borderRadius: 8,
  },
  containerInfoSearch: {
    height: 64,
    background: theme.palette.white.main,
    display: "flex",
    alignItems: "center",
    marginLeft: 24,
    borderRadius: 8,
  },
  btnBack: {
    position: "absolute",
    left: 0,
  },
  btnCalendar: {
    top: 49,
    position: "absolute",
    right: 0,
    backgroundColor: "#ffffff",
    borderRadius: 0,
    "&:hover": {
      backgroundColor: "#ffffff",
    },
  },
  iconArrowLeft: {
    display: "flex ",
    alignItems: "center",
  },
  iconArrowRight: {
    display: "flex ",
    alignItems: "center",
  },
  infoSearch: {
    display: "flex",
    color: theme.palette.black.black3,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: "0 auto",
  },
  localtion: {
    fontSize: 16,
    fontWeight: 600,
  },
  checkIn: {
    color: theme.palette.black.black4,
    fontSize: 12,
  },
  wrapInfoCheckIn: {
    display: "flex",
    alignItems: "center",
    marginTop: 2,
  },
  iconArrow: {
    width: 16,
    height: 17,
    stroke: theme.palette.black.black3,
  },
}));

const FlightHeaderListing = ({ isHideScroll, dataQuery, isInBound }) => {
  const classes = useStyles();
  const [isSelect, setIsSelect] = useState(0);
  const [hotTickets, setHotTickets] = useState([]);
  const [idxActive, setActive] = useState(0);

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    nextArrow:
      idxActive < 5 ? (
        <SampleNextArrow customStyleArrow={customStyle} />
      ) : (
        <></>
      ),
    prevArrow:
      idxActive > 0 ? (
        <SamplePrevArrow customStyleArrow={customStyle} />
      ) : (
        <></>
      ),
    beforeChange: (current, next) => setActive(next),
  };

  const getHotFly = async () => {
    try {
      let tempHotTickets = [];
      const start =
        moment().diff(
          moment(dataQuery.departureDate, DATE_FORMAT_BACK_END, true).subtract(
            4,
            "days"
          )
        ) < 0
          ? moment(
              dataQuery.departureDate,
              DATE_FORMAT_BACK_END,
              true
            ).subtract(4, "days")
          : moment();
      const end = moment(start).add(5, "days");

      let loop = start;
      while (end.diff(loop)) {
        const hotTicket = {
          airlineId: null,
          departureDate: moment(loop, DATE_FORMAT_BACK_END).format(
            DATE_FORMAT_BACK_END
          ),
          discountAdult: 0,
          farePrice: null,
          totalPrice: null,
        };
        tempHotTickets.push(hotTicket);
        const newDate = loop.add(1, "days");
        loop = newDate;
      }
      const dataDTO = {
        adults:
          dataQuery && dataQuery.adultCount
            ? parseInt(dataQuery.adultCount)
            : 1,
        children:
          dataQuery && dataQuery.childCount
            ? parseInt(dataQuery.childCount)
            : 0,
        fromDate:
          dataQuery &&
          dataQuery.departureDate &&
          moment().diff(
            moment(
              dataQuery.departureDate,
              DATE_FORMAT_BACK_END,
              true
            ).subtract(4, "days")
          ) < 0
            ? moment(dataQuery.departureDate, DATE_FORMAT_BACK_END, true)
                .subtract(4, "days")
                .format(C_DATE_FORMAT)
            : moment().format(C_DATE_FORMAT),
        toDate:
          dataQuery && dataQuery.returnDate
            ? moment(dataQuery.returnDate, DATE_FORMAT_BACK_END, true).format(
                C_DATE_FORMAT
              )
            : dataQuery && dataQuery.departureDate
            ? moment(dataQuery.departureDate, DATE_FORMAT_BACK_END, true)
                .add(5, "days")
                .format(C_DATE_FORMAT)
            : moment().format(C_DATE_FORMAT),
        groupByDate: true,
        infants:
          dataQuery && dataQuery.infantCount
            ? parseInt(dataQuery.infantCount)
            : 0,
        itineraries: [
          {
            arrivalCity:
              dataQuery && dataQuery.destination_location
                ? dataQuery.destination_location
                : "Hà Nội",
            departCity:
              dataQuery && dataQuery.origin_location
                ? dataQuery.origin_location
                : "Hồ Chí Minh",
            fromAirport:
              dataQuery && dataQuery.origin_code
                ? dataQuery.origin_code
                : "SGN",
            toAirport:
              dataQuery && dataQuery.destination_code
                ? dataQuery.destination_code
                : "HAN",
          },
        ],
      };
      const { data } = await searchHotTickets(dataDTO);
      let tempListHotTicket = uniqBy(
        [...data.data.hotTickets[0]?.priceOptions, ...tempHotTickets],
        "departureDate"
      ).sort(
        (a, b) =>
          moment(a.departureDate, DATE_FORMAT_BACK_END, true).format(
            "YYYYMMDD"
          ) -
          moment(b.departureDate, DATE_FORMAT_BACK_END, true).format("YYYYMMDD")
      );
      tempListHotTicket.find((element, index) => {
        if (element?.departureDate === dataQuery?.departureDate)
          setIsSelect(index);
      });
      setHotTickets(tempListHotTicket);
    } catch (error) {
      console.log("error", error);
    }
  };

  useEffect(() => {
    getHotFly();
  }, [dataQuery]);

  if (!dataQuery) {
    return <div />;
  }
  return (
    <Box className={classes.wrapHeaderListing}>
      <SlideShow settingProps={settings}>
        {!isHideScroll &&
          !isEmpty(hotTickets) &&
          hotTickets.length >= 2 &&
          hotTickets.map((el, index) => (
            <Box key={index} px={1}>
              <FlightDatePrice
                isSelect={isSelect === index}
                setIsSelect={setIsSelect}
                index={index}
                ticket={el}
                dataQuery={dataQuery}
                isHideScroll={isHideScroll}
              />
            </Box>
          ))}
      </SlideShow>
    </Box>
  );
};
FlightHeaderListing.propTypes = {
  isHideScroll: PropTypes.bool,
};
FlightHeaderListing.defaultProps = {
  isHideScroll: false,
};
export default FlightHeaderListing;
