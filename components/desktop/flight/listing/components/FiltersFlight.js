import {
  Box,
  ButtonBase,
  Divider,
  Grid,
  makeStyles,
  withStyles,
} from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import Slider from "@material-ui/core/Slider";
import { useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import {
  IconCheckBox,
  IconCheckBoxActive,
  IconMoonStarts,
  IconSunCloud,
  IconSunFog,
  IconSunFog2,
} from "@public/icons";
import Image from "@src/image/Image";
import Link from "@src/link/Link";
import {
  airlinesName,
  LIFT_TIME_LIST,
  listString,
  NUM_STOP_LIST,
} from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import clsx from "clsx";
import { useRouter } from "next/router";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    width: "25%",
  },
  headerBox: {
    padding: "14px 0",
    borderTop: `solid 1px ${theme.palette.gray.grayLight22}`,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    position: "fixed",
    top: 0,
    backgroundColor: theme.palette.white.main,
    zIndex: 10,
  },
  bodyBox: {
    padding: 16,
    display: "flex",
    flexDirection: "column",
    backgroundColor: theme.palette.white.main,
    boxShadow: "0px 1px 1px rgba(0, 0, 0, 0.05)",
    borderRadius: 8,
  },
  partnerItem: {
    // borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    padding: "8px 0",
  },
  liftItem: {
    border: `solid 1px ${theme.palette.gray.grayLight25}`,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
    padding: 8,
    "&.liftName": {
      color: theme.palette.gray.grayDark8,
    },
    "&.liftTime": {
      color: theme.palette.black.black3,
    },
  },
  active: {
    border: `solid 1px ${theme.palette.blue.blueLight8}`,
    backgroundColor: theme.palette.blue.blueLight9,
    "& span": {
      color: theme.palette.blue.blueLight8,
    },
    "& h6": {
      color: theme.palette.blue.blueLight8,
    },
  },
  footerBox: {
    padding: "8px 0",
    borderTop: `solid 1px ${theme.palette.gray.grayLight22}`,
    borderBottom: `solid 1px ${theme.palette.gray.grayLight22}`,
    position: "fixed",
    bottom: 0,
    backgroundColor: theme.palette.white.main,
    zIndex: 10,
    width: "100%",
  },
  partnerLogo: {
    height: 24,
    marginLeft: 4,
    width: 24,
    objectFit: "contain",
  },
  btnFilter: {
    backgroundColor: theme.palette.pink.main,
    color: theme.palette.white.main,
    borderRadius: 8,
    padding: "14px 0",
    height: 48,
    width: "100%",
  },
}));

const iOSBoxShadow =
  "0px 0.5px 4px rgba(0, 0, 0, 0.12), 0px 6px 13px rgba(0, 0, 0, 0.12)";

const IOSSlider = withStyles((theme) => ({
  thumb: {
    height: 28,
    width: 28,
    backgroundColor: theme.palette.white.main,
    boxShadow: iOSBoxShadow,
    border: "none",
    marginTop: -14,
    marginLeft: -14,
    "&:focus, &:hover, &$active": {
      boxShadow:
        "0px 0.5px 4px rgba(0, 0, 0, 0.12), 0px 6px 13px rgba(0, 0, 0, 0.12)",
      // Reset on touch devices, it doesn't add specificity
      "@media (hover: none)": {
        boxShadow: iOSBoxShadow,
      },
    },
  },
  active: {},
  track: {
    height: 4,
    backgroundColor: theme.palette.blue.blueLight8,
  },
  rail: {
    height: 4,
    opacity: 0.5,
    backgroundColor: theme.palette.gray.grayLight22,
  },
  root: {
    width: "95%",
  },
}))(Slider);

const RenderPrice = ({ maxPrice, handleSetSingleParam, paramFilter }) => {
  const classes = useStyles();

  const handleChangePrice = (event, price) => {
    handleSetSingleParam("priceFilter", price);
  };

  let paramChecked = paramFilter.priceFilter;
  if (paramChecked === 0) {
    paramChecked = maxPrice;
  }

  return (
    <Box className={classes.priceFilter}>
      <Typography variant="subtitle2">
        {listString.IDS_MT_TEXT_FILTER_PRICE_RANGE}
      </Typography>
      <Box mt={1}>
        <Typography variant="caption">
          0đ - {paramChecked?.formatMoney()}đ
        </Typography>
      </Box>
      <div className={classes.root}>
        <IOSSlider
          key={`slider-${paramChecked}`}
          aria-label="ios slider"
          defaultValue={paramChecked}
          valueLabelDisplay="off"
          onChangeCommitted={handleChangePrice}
          step={1000}
          min={0}
          max={maxPrice}
        />
      </div>
    </Box>
  );
};

const FilterCheckbox = withStyles((theme) => ({
  root: {
    padding: 0,
    color: theme.palette.gray.grayLight25,
    "&$checked": {
      color: theme.palette.blue.blueLight8,
    },
  },
  checked: {},
}))((props) => (
  <Checkbox
    size="small"
    color="default"
    {...props}
    icon={<IconCheckBox />}
    checkedIcon={props.checked ? <IconCheckBoxActive /> : <div></div>}
  />
));

const RenderAirline = ({
  handleSetArrayParam,
  paramFilter,
  branchAir,
  paramsUrl,
  airlinesId,
}) => {
  const classes = useStyles();
  const router = useRouter();

  const handleClick = (newValue, isChecked) => {
    handleSetArrayParam("airlines", newValue);
    if (!isEmpty(paramsUrl)) {
      router.push({
        pathname: "/ve-may-bay/result/[...slug]",
        query: {
          slug: ["outbound"],
          ...paramsUrl,
        },
      });
    }
  };

  const handleSelectPartner = (newValue) => {
    handleSetArrayParam("airlines", newValue);
  };

  const getLinkInfo = (item, isChecked) => {
    if (isEmpty(paramsUrl)) return;
    if (isEmpty(paramFilter?.airlines) && item) {
      const nameAirline = airlinesName.find((element) => element?.id === item)
        ?.name;
      return {
        pathname: "/ve-may-bay/result/[...slug]",
        query: {
          slug: ["outbound", `ve-may-bay-${nameAirline}`],
          ...paramsUrl,
        },
      };
    } else {
      if (paramFilter?.airlines.length === 3) return null;
      if (paramFilter?.airlines.includes(item)) {
        let tempCode = null;
        paramFilter?.airlines.map((element) => {
          if (element !== item) {
            tempCode = element;
          }
        });
        const nameAirline = airlinesName.find(
          (element) => element?.id === tempCode
        )?.name;
        return {
          pathname: "/ve-may-bay/result/[...slug]",
          query: {
            slug: ["outbound", `ve-may-bay-${nameAirline}`],
            ...paramsUrl,
          },
        };
      } else {
        return null;
      }
    }
  };

  const paramChecked = paramFilter?.airlines;
  return (
    <Box className={classes.partnerFilter} pt={2}>
      <Typography variant="subtitle2">
        {listString.IDS_MT_TEXT_FILTER_PARTNER}
      </Typography>
      <Box className={classes.listPartner} pt={1}>
        {branchAir?.map((item, index) => {
          if (getLinkInfo(item?.id, paramChecked.includes(item?.id)))
            return (
              <Grid
                container
                className={classes.partnerItem}
                key={index}
                onClick={() => handleSelectPartner(item?.id)}
              >
                <Grid item lg={1} md={1} sm={1} xs={1}>
                  <Link
                    key={item?.id}
                    color="inherit"
                    href={{
                      ...getLinkInfo(item?.id, paramChecked.includes(item?.id)),
                    }}
                  >
                    <FilterCheckbox
                      checked={paramChecked.includes(item?.id)}
                      name="partnerCheckbox"
                    />
                  </Link>
                </Grid>
                <Grid item lg={1} md={1} sm={1} xs={1}>
                  <Image srcImage={item.logo} className={classes.partnerLogo} />
                </Grid>
                <Grid item lg={10} md={10} sm={10} xs={10}>
                  <Typography style={{ marginLeft: 20 }} variant="caption">
                    {item.name}
                  </Typography>
                </Grid>
              </Grid>
            );
          return (
            <Grid
              container
              className={classes.partnerItem}
              key={index}
              onClick={() =>
                handleClick(item?.id, paramChecked.includes(item?.id))
              }
            >
              <Grid item lg={1} md={1} sm={1} xs={1}>
                <FilterCheckbox
                  checked={paramChecked.includes(item?.id)}
                  name="partnerCheckbox"
                />
              </Grid>
              <Grid item lg={1} md={1} sm={1} xs={1}>
                <Image srcImage={item.logo} className={classes.partnerLogo} />
              </Grid>
              <Grid item lg={10} md={10} sm={10} xs={10}>
                <Typography style={{ marginLeft: 20 }} variant="caption">
                  {item.name}
                </Typography>
              </Grid>
            </Grid>
          );
        })}
      </Box>
    </Box>
  );
};

// const RenderFlightTime = ({
//   maxDuration,
//   handleSetSingleParam,
//   paramFilter,
// }) => {
//   const classes = useStyles();
//   const handleChangeFlightTime = (event, newValue) => {
//     handleSetSingleParam("flyTimeFilter", newValue);
//   };

//   let paramChecked = paramFilter.flyTimeFilter;
//   if (paramChecked === 0) {
//     paramChecked = maxDuration;
//   }

//   return (
//     <Box className={classes.timeFilter}>
//       <Typography variant="subtitle1">
//         {listString.IDS_MT_TEXT_FILTER_FLY_TIME}
//       </Typography>
//       <Box mt={1}>
//         <Typography variant="caption">
//           0h - {getFlightTimeText(maxDuration / (1000 * 60))}
//           {"’"}
//         </Typography>
//       </Box>
//       <Box className={classes.root}>
//         <IOSSlider
//           value={maxDuration}
//           valueLabelDisplay="off"
//           onChange={handleChangeFlightTime}
//           step={1}
//           min={0}
//           max={maxDuration}
//         />
//       </Box>
//     </Box>
//   );
// };

const RenderLiftTime = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const liftTimeList = LIFT_TIME_LIST;

  const handleSelectLiftTime = (liftType) => {
    props.handleCheckLift("liftTimeFilter", liftType);
  };

  const paramChecked = props.paramFilter.liftTimeFilter;

  return (
    <Box className={classes.liftTimeFilter}>
      <Typography variant="subtitle2">
        {listString.IDS_MT_TEXT_FILTER_LIFT_TIME}
      </Typography>
      <Box display="flex" flexDirection="row" pt={2}>
        <Grid container spacing={1}>
          {liftTimeList.map((item, index) => (
            <Grid
              item
              lg={6}
              md={6}
              sm={6}
              xs={6}
              key={index}
              onClick={() => handleSelectLiftTime(item?.id)}
            >
              <Box
                className={clsx(
                  classes.liftItem,
                  paramChecked?.includes(item?.id) && classes.active
                )}
              >
                {!index ? (
                  <IconSunFog />
                ) : index === 1 ? (
                  <IconSunCloud />
                ) : index === 2 ? (
                  <IconSunFog2 />
                ) : (
                  <IconMoonStarts />
                )}
                <Typography variant="caption" className={classes.liftName}>
                  {item.name}
                </Typography>
                <Typography variant="subtitle2" className={classes.liftTime}>
                  {item.from} - {item.to}
                </Typography>
              </Box>
            </Grid>
          ))}
        </Grid>
      </Box>
    </Box>
  );
};

const RenderSeat = (props) => {
  const classes = useStyles();
  const handleSelectSeat = (newValue) => {
    props.handleCheckSeat("seatFilter", newValue);
  };
  const seatList = props.data;
  const paramChecked = props.paramFilter.seatFilter;

  return (
    <Box className={classes.partnerFilter}>
      <Typography variant="subtitle2">
        {listString.IDS_MT_TEXT_FILTER_SEAT_TYPE}
      </Typography>
      <Box className={classes.listPartner} pt={1}>
        {seatList?.map((item, index) => (
          <Grid
            container
            className={classes.partnerItem}
            key={index}
            onClick={() => handleSelectSeat(item?.code)}
          >
            <Grid item lg={1} md={1} sm={1} xs={1}>
              <FilterCheckbox
                name="seatTypeCheckbox"
                checked={paramChecked.includes(item?.code)}
              />
            </Grid>
            <Grid item lg={11} md={11} sm={11} xs={11}>
              <Typography style={{ marginLeft: 8 }} variant="caption">
                {item.v_name}
              </Typography>
            </Grid>
          </Grid>
        ))}
      </Box>
    </Box>
  );
};

const RenderNumStop = ({ numStops, handleCheckNumStop, paramFilter }) => {
  const classes = useStyles();
  const handleSelectNumStop = (newValue) => {
    handleCheckNumStop("numStops", newValue);
  };
  const paramChecked = paramFilter.numStops;
  const tempListNumStop = NUM_STOP_LIST.slice(0, numStops?.length);

  return (
    <Box className={classes.partnerFilter}>
      <Typography variant="subtitle2">Điểm dừng</Typography>
      <Box className={classes.listPartner} pt={1}>
        {tempListNumStop?.map((item, index) => (
          <Grid
            container
            className={classes.partnerItem}
            key={index}
            onClick={() => handleSelectNumStop(item?.id)}
          >
            <Grid item lg={1} md={1} sm={1} xs={1}>
              <FilterCheckbox
                name="numStopCheckbox"
                checked={paramChecked.includes(item?.id)}
              />
            </Grid>
            <Grid item lg={11} md={11} sm={11} xs={11}>
              <Typography style={{ marginLeft: 8 }} variant="caption">
                {item.name}
              </Typography>
            </Grid>
          </Grid>
        ))}
      </Box>
    </Box>
  );
};

const FiltersFlight = ({
  dataFilter = {},
  paramFilter = {},
  paramsUrl = {},
  airlinesId = 0,
  setParamFilter = () => {},
  handleSetSingleParam = () => {},
  handleSetArrayParam = () => {},
  handleReset = () => {},
  valueSort,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const ticketClass = dataFilter.ticketClasses;
  const maxPrice = dataFilter?.filters?.maxPrice;
  const maxFarePrice = dataFilter?.filters?.maxFarePrice;
  const branchAir = dataFilter?.airlines;
  const maxDuration = dataFilter?.filters?.maxDuration;
  const numStops = dataFilter?.filters?.numStops;

  return (
    <>
      <Box className={classes.container}>
        <Box className={classes.bodyBox}>
          <Box display="flex" justifyContent="space-between">
            <Typography variant="subtitle1">
              {listString.IDS_MT_TEXT_FILTER}
            </Typography>
            <ButtonBase
              style={{ color: theme.palette.blue.blueLight8 }}
              onClick={handleReset(1)}
            >
              <Typography variant="body1">
                {listString.IDS_MT_TEXT_CLEAR_FILTER}
              </Typography>
            </ButtonBase>
          </Box>
          <Divider
            style={{
              height: 1,
              color: theme.palette.gray.grayLight22,
              margin: "16px -16px",
            }}
          />
          <RenderPrice
            maxPrice={valueSort === "gross" ? maxPrice : maxFarePrice}
            handleSetSingleParam={handleSetSingleParam}
            paramFilter={paramFilter}
          />
          <Divider
            style={{
              height: 1,
              color: theme.palette.gray.grayLight22,
              margin: "16px -16px 8px",
            }}
          />
          <RenderAirline
            branchAir={branchAir}
            handleSetArrayParam={handleSetArrayParam}
            paramFilter={paramFilter}
            paramsUrl={paramsUrl}
            airlinesId={airlinesId}
          />
          <Divider
            style={{
              height: 1,
              color: theme.palette.gray.grayLight22,
              margin: "16px -16px",
            }}
          />
          {/* <RenderFlightTime
            maxDuration={maxDuration}
            handleSetSingleParam={handleSetSingleParam}
            paramFilter={paramFilter}
          />
          <Divider
            style={{
              height: 1,
              color: theme.palette.gray.grayLight22,
              margin: "16px -16px",
            }}
          /> */}
          <RenderLiftTime
            handleCheckLift={handleSetArrayParam}
            paramFilter={paramFilter}
          />
          <Divider
            style={{
              height: 1,
              color: theme.palette.gray.grayLight22,
              margin: "16px -16px",
            }}
          />
          <RenderSeat
            data={ticketClass}
            handleCheckSeat={handleSetArrayParam}
            paramFilter={paramFilter}
          />
          <Divider
            style={{
              height: 1,
              color: theme.palette.gray.grayLight22,
              margin: "16px -16px",
            }}
          />
          <RenderNumStop
            numStops={numStops}
            handleCheckNumStop={handleSetArrayParam}
            paramFilter={paramFilter}
          />
        </Box>
      </Box>
    </>
  );
};

export default FiltersFlight;
