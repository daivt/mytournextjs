import SkeletonItemListingFlight from "@components/common/desktop/skeleton/SkeletonItemListingFlight";
import {
  Box,
  FormControlLabel,
  Popover,
  Radio,
  RadioGroup,
  Typography,
  useTheme
} from "@material-ui/core";
import { createStyles, makeStyles, withStyles } from "@material-ui/styles";
import { IconDropDown } from "@public/icons";
import { listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import clsx from "clsx";
import { useState } from "react";
import FlightResultBoxOneWay from "./FlightResultBoxOneWay";
import FlightResultBoxTwoWay from "./FlightResultBoxTwoWay";

const useStyles = makeStyles((theme) => ({
  listContainer: { padding: "0 0 0 24px", width: "100%" },
  bannerImg: { width: "100%", maxHeight: 132, borderRadius: 8 },
  listHotel: { marginTop: 24 },
  filterContainer: { display: "flex", alignItems: "center", marginBottom: 24 },
  filterItem: {
    display: "flex",
    alignItems: "center",
    background: "#E2E8F0",
    borderRadius: 6,
    padding: "6px 8px",
    margin: "0 8px",
  },
  rankingHotel: {
    stroke: "#718096",
    fill: "#718096",
    width: 12,
    height: 12,
    marginRight: 3,
  },
  typeResultContainer: {
    display: "flex",
    fontWeight: 600,
    fontSize: 14,
    lineHeight: "17px",
    marginBottom: 16,
    borderBottom: "solid 1px #E2E8F0",
  },
  typeItem: {
    borderBottom: "solid 1px #F4F8FA",
    color: "#1A202C",
    cursor: "pointer",
    paddingBottom: 10,
    marginRight: 35,
    "&:hover": {
      color: "#00B6F3",
      borderBottom: "solid 1px #00B6F3",
    },
  },
  typeItem2: {
    borderBottom: "solid 1px #F4F8FA",
    color: "#1A202C",
    cursor: "pointer",
    paddingBottom: 10,
    "&:hover": {
      color: "#00B6F3",
      borderBottom: "solid 1px #00B6F3",
    },
  },
  activeItem: { color: "#00B6F3", borderBottom: "solid 1px #00B6F3" },
  textClearFilter: {
    fontSize: 14,
    lineHeight: "17px",
    color: "#00B6F3",
    cursor: "pointer",
  },
  paginationContainer: {
    display: "flex",
    justifyContent: "center",
    margin: "32px 0",
    "& .MuiPaginationItem-textPrimary.Mui-selected": { color: "white" },
    "& .MuiPaginationItem-page:hover": {
      background: "#E2E8F0",
      color: "#1A202C",
    },
  },
}));

const BlueRadio = withStyles((theme) =>
  createStyles({
    root: {
      color: theme.palette.blue.blueLight8,
      "&$checked": {
        color: theme.palette.blue.blueLight8,
      },
    },
  })
)((props) => <Radio color="default" {...props} />);

const ListFlight = ({
  dataFilter = {},
  filteredTickets = {},
  sortOptions = [],
  searchCompleted = 0,
  isInBound = false,
  data = {},
  setParamFilter = () => {},
  paramFilter = {},
  valueSort,
  setValueSort = () => {},
  tempTicketOutBound = {},
}) => {
  const theme = useTheme();
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = useState(null);

  const handleSelectedItemSort = (sortCode) => {
    setParamFilter({
      ...paramFilter,
      sortBy: sortCode,
    });
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleRadioChange = (event) => {
    setValueSort(event.target.value);
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  if (searchCompleted === 100 && isEmpty(filteredTickets))
    return (
      <Box className={classes.listContainer}>
        <Box id="banner_listing" className={classes.bannerImg} />
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          flexDirection="column"
          width="100%"
          height="100%"
          mt={3}
        >
          <img
            src="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_flight_not_found.svg"
            alt=""
          />
          <Box
            display="flex"
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
          >
            <Typography variant="subtitle1" component="span">
              {listString.IDS_TEXT_FLIGHT_NOT_FOUND}
            </Typography>
            <Typography variant="caption" component="span">
              {listString.IDS_TEXT_FLIGHT_CHANGE_SEARCH_INFO}
            </Typography>
          </Box>
        </Box>
      </Box>
    );
  if (isEmpty(filteredTickets) && searchCompleted !== 100)
    return (
      <Box className={classes.listContainer}>
        <Box id="banner_listing" className={classes.bannerImg} />
        <Box className={classes.listHotel}>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="center"
            flexDirection="column"
          >
            {[1, 2, 3, 4, 5].map((el) => (
              <SkeletonItemListingFlight key={el} />
            ))}
          </Box>
        </Box>
      </Box>
    );
  if (!isEmpty(filteredTickets))
    return (
      <Box className={classes.listContainer}>
        <Box id="banner_listing" className={classes.bannerImg} />
        <Box className={classes.listHotel}>
          <Box display="flex" width="100%">
            <Box className={classes.typeResultContainer} width="58%">
              {!isEmpty(sortOptions) &&
                sortOptions.map((el, index) => (
                  <Box
                    key={el.id}
                    className={clsx(
                      classes.typeItem,
                      el.id === paramFilter.sortBy && classes.activeItem
                    )}
                    onClick={() => handleSelectedItemSort(el.id)}
                  >
                    {el.name}
                  </Box>
                ))}
            </Box>
            <Box
              className={classes.typeResultContainer}
              justifyContent="flex-end"
              width="42%"
            >
              <Box
                aria-describedby={id}
                className={classes.typeItem2}
                onClick={handleClick}
                display="flex"
                alignItems="center"
              >
                <Typography
                  variant="caption"
                  style={{
                    lineHeight: "15px",
                  }}
                >
                  Chế độ hiển thị:
                </Typography>
                &nbsp;
                {valueSort === "net"
                  ? "Giá cơ bản cho 1 người lớn"
                  : "Giá gồm thuế và phí cho 1 người lớn"}
                <IconDropDown style={{ marginLeft: 4 }} />
              </Box>
              <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "center",
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "center",
                }}
                style={{ marginLeft: 8 }}
              >
                <RadioGroup
                  aria-label="quiz"
                  name="quiz"
                  value={valueSort}
                  onChange={handleRadioChange}
                >
                  <FormControlLabel
                    value="net"
                    control={<BlueRadio size="small" />}
                    label={
                      <Typography variant="caption">
                        Giá cơ bản cho 1 người lớn
                      </Typography>
                    }
                  />
                  <FormControlLabel
                    value="gross"
                    control={<BlueRadio size="small" />}
                    label={
                      <Typography variant="caption">
                        Giá gồm thuế và phí cho 1 người lớn
                      </Typography>
                    }
                  />
                </RadioGroup>
              </Popover>
            </Box>
          </Box>
          {isInBound ? (
            <FlightResultBoxTwoWay
              dataTickets={filteredTickets}
              dataFilter={dataFilter}
              data={data}
              setParamFilter={setParamFilter}
              paramFilter={paramFilter}
              isNetPrice={valueSort === "net"}
            />
          ) : (
            <FlightResultBoxOneWay
              dataTickets={filteredTickets}
              dataFilter={dataFilter}
              data={data}
              setParamFilter={setParamFilter}
              paramFilter={paramFilter}
              isNetPrice={valueSort === "net"}
              tempTicketOutBound={tempTicketOutBound}
            />
          )}
        </Box>
      </Box>
    );
  return <div />;
};

export default ListFlight;
