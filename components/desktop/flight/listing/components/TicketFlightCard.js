import DialogCheckTime2Ways from "@components/common/modal/flight/DialogCheckTime2Ways";
import {
  Box,
  Divider,
  makeStyles,
  Typography,
  useTheme,
} from "@material-ui/core";
import {
  IconArrowDownToggle,
  IconArrowFlightLong,
  IconFlightTransit,
  IconRunningOut,
} from "@public/icons";
import ButtonComponent from "@src/button/Button";
import Image from "@src/image/Image";
import { listEventFlight, listString } from "@utils/constants";
import * as gtm from "@utils/gtm";
import {
  checkFlightTime2Ways,
  durationFlightTime,
  getTicketClass,
  isEmpty,
  Policy,
} from "@utils/helpers";
import { DATE_FORMAT } from "@utils/moment";
import moment from "moment";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    backgroundColor: theme.palette.white.main,
    padding: "24px 24px 0px",
    boxShadow: "0px 1px 1px rgba(0, 0, 0, 0.05)",
    borderRadius: 8,
  },
  button: {
    width: 124,
    marginTop: 8,
  },
  divider: {
    height: 2,
    color: theme.palette.gray.grayLight22,
    margin: "16px -24px 32px",
  },
  wrapCard: {
    display: "flex",
    alignItems: "center",
    paddingBottom: 8,
  },
  wrapCardLeft: {},
  wrapCardRight: {},
  airlineCode: {
    color: theme.palette.gray.grayDark7,
  },
  airlineFromTo: {
    color: theme.palette.black.black3,
    fontSize: 18,
    lineHeight: "22px",
    display: "flex",
  },
  flightTimeBox: {
    display: "flex",
    color: theme.palette.black.black3,
  },
  wrapFlight: {
    display: "flex",
  },
  airlineLogo: {
    width: 32,
    height: 32,
    objectFit: "contain",
  },
  airlineLogo1: {
    width: 18,
    height: 18,
    marginRight: 4,
    objectFit: "contain",
  },
  iconDot: {
    margin: 6,
  },
  airlineName: {
    color: theme.palette.black.black3,
    whiteSpace: "nowrap",
  },
  flightCode: {
    color: theme.palette.gray.grayDark8,
    // whiteSpace: "nowrap",
    // overflow: "hidden",
    // textOverflow: "ellipsis",
  },
  wrapAirBrand: {
    display: "flex",
    overflow: "hidden",
    marginLeft: 8,
    color: theme.palette.black.black3,
  },
  iconArrow: {
    margin: "0 12px",
  },
  subPrice: {
    textDecorationLine: "line-through",
    textAlign: "right",
    color: theme.palette.gray.grayDark8,
    marginRight: 8,
  },
  mainPrice: {
    color: theme.palette.black.black3,
    textAlign: "right",
    fontSize: 18,
    lineHeight: "22px",
  },
  bestPriceBox_1: {
    padding: "2px 6px",
    display: "flex",
    alignItems: "center",
    border: `1px solid ${theme.palette.brown.brownLight1}`,
    fontSize: "14px",
    borderRadius: 4,
    marginBottom: "8px",
    color: theme.palette.brown.brownLight1,
  },
  bestPriceBox_2: {
    padding: "2px 6px",
    display: "flex",
    alignItems: "center",
    border: `1px solid ${theme.palette.green.greenLight6}`,
    fontSize: "14px",
    borderRadius: 4,
    marginBottom: "8px",
    color: theme.palette.green.greenLight6,
  },
  bestPriceBox_4: {
    padding: "2px 6px",
    display: "flex",
    alignItems: "center",
    border: `1px solid ${theme.palette.red.redDark5}`,
    fontSize: "14px",
    borderRadius: 4,
    marginBottom: "8px",
    color: theme.palette.red.redDark5,
  },
  runningOut: {
    display: "flex",
    justifyContent: "flex-end",
    height: 20,
    width: 96,
    border: "none",
    color: theme.palette.red.redLight5,
  },
}));

const TicketFlightCard = ({
  item,
  airline,
  transitTickets,
  ticketClass,
  tid,
  requestId,
  isExpanded,
  isTwoWay,
  data,
  onClickButton = () => {},
  isNetPrice,
  tempTicketOutBound,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();

  const [isDialogCheckTime2Ways, setDialogCheckTime2Ways] = useState(false);

  let transitAirPortsStr = "";
  let transitStopsStr = "";
  if (item.ticketdetail?.transitTickets) {
    transitAirPortsStr = item.ticketdetail.transitTickets
      .map((el) => {
        return el.facilities?.length
          ? getAirtCraftInfo(el.facilities)
          : undefined;
      })
      .join(", ");
    transitStopsStr = item.ticketdetail.transitTickets
      .map((el) => {
        return el.departureAirport;
      })
      .join(", ");
  }
  const getAirtCraftInfo = (facilities) => {
    const airCraftInfo = facilities?.find((fa) => fa?.code === "AIRCRAFT_INFO");
    return airCraftInfo ? airCraftInfo.description : "";
  };
  const airCraft = getAirtCraftInfo(item?.ticketdetail?.facilities);

  const isTransit = !!item?.outbound?.transitTickets;

  const tempData = isTwoWay
    ? data?.outbound?.tickets
    : router?.query?.ticketOutBoundId
    ? data?.inbound?.tickets
    : data?.tickets;
  const filterDataTickets = tempData
    ?.filter((element) => {
      return element?.outbound?.flightNumber === item?.flightNumber;
    })
    .slice(1, 3);

  const arrivalDay = moment(item.arrivalDayStr, DATE_FORMAT);
  const departureDay = moment(item.departureDayStr, DATE_FORMAT);
  const diffDay = arrivalDay.diff(departureDay, "days");

  return (
    <>
      <Box className={classes.wrapper}>
        <Box className={classes.wrapCard}>
          <Box className={classes.wrapFlight} width="35%">
            {transitAirPortsStr.length ? (
              <>
                <IconFlightTransit />
                <Box className={classes.wrapAirBrand}>
                  <Typography
                    component={"span"}
                    variant="body2"
                    className={classes.airlineName}
                  >
                    {transitAirPortsStr}
                  </Typography>
                </Box>
              </>
            ) : (
              <Box display="flex">
                <Box display="flex" alignItems="center" mr={1}>
                  <Image
                    srcImage={airline.logo}
                    className={classes.airlineLogo}
                  />
                </Box>
                <Box className={classes.wrapAirBrand} flexDirection="column">
                  <Typography
                    component={"span"}
                    variant="caption"
                    className={classes.airlineName}
                  >
                    {airline.name}
                  </Typography>
                  <Typography
                    component={"span"}
                    variant="caption"
                    className={classes.flightCode}
                    style={{ color: theme.palette.secondary.main }}
                  >
                    {item?.operatingAirlineId ? "Bay Pacific Airlines" : ""}
                  </Typography>
                  <Typography
                    component={"span"}
                    variant="caption"
                    className={classes.flightCode}
                  >
                    {airCraft
                      ? `${item.flightNumber}/ ${airCraft.split("-")[0]}`
                      : item.flightNumber}
                  </Typography>
                </Box>
              </Box>
            )}
          </Box>
          <Box display="flex" width="45%">
            <Box className={classes.wrapCardLeft}>
              <Typography
                variant="subtitle1"
                component="span"
                className={classes.airlineFromTo}
              >
                {item.departureTimeStr}
              </Typography>
              <Typography
                variant="caption"
                component="span"
                className={classes.airlineCode}
              >
                {item.departureAirport}
              </Typography>
            </Box>
            <IconArrowFlightLong className={classes.iconArrow} />
            <Box className={classes.wrapCardRight}>
              <Typography
                variant="subtitle1"
                component="span"
                className={classes.airlineFromTo}
              >
                {item.arrivalTimeStr}
              </Typography>
              <Typography
                variant="caption"
                component="span"
                className={classes.airlineCode}
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                }}
              >
                {item.arrivalAirport}
              </Typography>
            </Box>
            {!!diffDay && (
              <Typography
                variant="caption"
                style={{
                  position: "relative",
                  top: -6,
                  left: 4,
                  color: theme.palette.pink.main,
                }}
              >
                +{diffDay} ngày
              </Typography>
            )}
          </Box>
          <Box display="flex" flexDirection="column" width="20%">
            <Typography variant="caption">
              {durationFlightTime(item.duration)}
            </Typography>
            {item.ticketdetail.numStops === 0 ? (
              <Typography variant="caption">
                {listString.IDS_TEXT_NUM_STOPS_0}
              </Typography>
            ) : (
              <Typography variant="body2">{`${item.ticketdetail.numStops} ${listString.IDS_TEXT_NUM_STOPS_1} ${transitStopsStr}`}</Typography>
            )}
          </Box>
        </Box>
        <Divider className={classes.divider} />
        <Box className={classes.wrapCard}>
          <Box className={classes.wrapFlight} width="35%">
            <Box display="flex" alignItems="center" width="100%">
              <Typography variant="caption" style={{ marginLeft: 48 }}>
                {getTicketClass(item, ticketClass)}
              </Typography>
              <div
                style={{
                  height: "18px",
                  marginLeft: "5px",
                  minWidth: "18px",
                  overflow: "hidden",
                  alignItems: "center",
                  borderRadius: "2px",
                  padding: "0 5px",
                  display: "flex",
                  justifyContent: "center",
                  color: "white",
                  backgroundColor: theme.palette.primary.main,
                  fontWeight: 500,
                  fontSize: "14px",
                }}
              >
                {transitTickets
                  ? transitTickets[0].detailTicketClass
                  : item?.ticketdetail?.detailTicketClass}
              </div>
            </Box>
          </Box>
          <Box display="flex" width="45%" flexDirection="column">
            {isTransit &&
              item.polices &&
              item.polices.map((ele, index) => (
                <Policy item={ele} key={index} isMobile={false} />
              ))}

            {!isTransit &&
              item.ticketdetail?.polices &&
              item.ticketdetail.polices?.map((ele, index) => (
                <Policy item={ele} key={index} isMobile={false} />
              ))}
          </Box>
          <Box
            display="flex"
            flexDirection="column"
            width="20%"
            justifyContent="flex-end"
            alignItems="flex-end"
          >
            {item.ticketdetail.isBestPrice === 1 ? ( // for label best price
              <Box
                className={
                  item?.aid === 1
                    ? classes.bestPriceBox_1
                    : item?.aid === 2
                    ? classes.bestPriceBox_2
                    : classes.bestPriceBox_4
                }
              >
                <Image
                  srcImage={airline.logo}
                  className={classes.airlineLogo1}
                />
                <Typography variant="body2">
                  {listString.IDS_TEXT_FLIGHT_BEST_PRICE}
                </Typography>
              </Box>
            ) : null}
            <Box display="flex" alignItems="center">
              {!isNetPrice && item.ticketdetail.discountAdultUnit > 0 && (
                <Typography variant="caption" className={classes.subPrice}>
                  {`${(
                    item.ticketdetail.priceAdultUnit +
                    item.ticketdetail.discountAdultUnit
                  ).formatMoney()}đ`}
                </Typography>
              )}
              <Typography variant="subtitle1" className={classes.mainPrice}>
                {isNetPrice
                  ? `${item.ticketdetail.farePrice.formatMoney()}đ`
                  : `${item.ticketdetail.priceAdultUnit.formatMoney()}đ`}
              </Typography>
            </Box>
            {item?.isRunningOut && item.isRunningOut === 1 && (
              <>
                <Box className={classes.runningOut}>
                  <Box marginRight={"6px"}>
                    <IconRunningOut />
                  </Box>
                  <Typography variant="body2">
                    {listString.IDS_TEXT_FLIGHT_RUNNING_OUT}
                  </Typography>
                </Box>
              </>
            )}
            <ButtonComponent
              type="submit"
              className={classes.button}
              backgroundColor={theme.palette.secondary.main}
              fontSize={14}
              fontWeight={600}
              borderRadius={8}
              handleClick={() => {
                if (!checkFlightTime2Ways(tempTicketOutBound, item)) {
                  setDialogCheckTime2Ways(true);
                  return;
                }
                if (isTwoWay && !isEmpty(router?.query?.returnDate)) {
                  router.push({
                    pathname: "/ve-may-bay/result/inbound",
                    query: {
                      ...router?.query,
                      agencyOutBoundId: item?.aid,
                      ticketOutBoundId: tid,
                      needPassport: item
                        ? item?.ticketdetail?.needPassport
                        : false,
                    },
                  });
                } else {
                  const query = router?.query || {};
                  gtm.addEventGtm(listEventFlight.FlightAddToCart);
                  if (router?.query?.ticketOutBoundId) {
                    router.push({
                      pathname: "/ve-may-bay/payment",
                      query: {
                        ...router?.query,
                        agencyOutBoundId: router?.query?.agencyOutBoundId,
                        ticketOutBoundId: router?.query?.ticketOutBoundId,
                        agencyInBoundId: item?.aid,
                        requestId: requestId,
                        ticketInBoundId: tid,
                        needPassport: item
                          ? item?.ticketdetail?.needPassport
                          : false,
                      },
                    });
                  } else {
                    router.push({
                      pathname: "/ve-may-bay/payment",
                      query: {
                        ...router?.query,
                        agencyOutBoundId: item?.aid,
                        requestId: requestId,
                        ticketOutBoundId: tid,
                        needPassport: item
                          ? item?.ticketdetail?.needPassport
                          : false,
                      },
                    });
                  }
                }
              }}
            >
              {listString.IDS_MT_TEXT_SELECT}
            </ButtonComponent>
          </Box>
        </Box>
        {!!filterDataTickets?.length && (
          <>
            <Divider
              style={{
                height: 1,
                color: theme.palette.gray.grayLight22,
              }}
            />
            {!isExpanded && (
              <Box display="flex" justifyContent="center">
                <ButtonComponent
                  backgroundColor={theme.palette.white.main}
                  color={theme.palette.blue.blueLight8}
                  fontSize={14}
                  handleClick={onClickButton}
                  width={240}
                >
                  Xem thêm hạng ghế và giá vé
                  <IconArrowDownToggle
                    className="svgFillAll"
                    style={{
                      stroke: theme.palette.blue.blueLight8,
                      marginLeft: 8,
                    }}
                  />
                </ButtonComponent>
              </Box>
            )}
          </>
        )}
      </Box>
      <DialogCheckTime2Ways
        open={isDialogCheckTime2Ways}
        toggleDrawer={() => setDialogCheckTime2Ways(false)}
      />
    </>
  );
};

export default TicketFlightCard;

TicketFlightCard.propTypes = {
  item: PropTypes.object.isRequired,
  airline: PropTypes.object,
};
