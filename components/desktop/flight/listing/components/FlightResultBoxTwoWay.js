import { Box, Collapse, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { useSnackbar } from "notistack";
import React, { useState } from "react";
import FlightInfoExpand from "./FlightInfoExpand";
import TicketFlightCard from "./TicketFlightCard";

const useStyles = makeStyles((theme) => ({
  listFlightCard: {},
  divider: {
    height: 8,
    backgroundColor: theme.palette.gray.grayLight23,
  },
  footerListing: {
    position: "fixed",
    backgroundColor: theme.palette.white.main,
    zIndex: 20,
    bottom: 0,
  },
}));

const FlightResultBoxTwoWay = ({
  dataTickets,
  dataFilter,
  data,
  setParamFilter = () => {},
  isNetPrice,
}) => {
  const classes = useStyles();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [visibleDrawerType, setVisibleDrawerType] = useState(false);
  const [ticket, setTicket] = useState();
  const [ticketId, setTicketId] = useState();
  const [expanded, setExpanded] = React.useState(
    Array(dataTickets.length).fill(false)
  );

  const transitTickets = data?.transitTickets;
  const ticketClass = data?.ticketClasses;
  const requestId = data?.requestId;

  const handleExpandClick = (index) => {
    let tempExpanded = expanded;
    tempExpanded[index] = !expanded[index];
    setExpanded([...tempExpanded]);
  };

  const toggleDrawer = (open = false, index = 0) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    handleExpandClick(index);
    if (open) {
      setTicketId(dataTickets[index].tid);
      setTicket(dataTickets[index]);
    } else {
      setTicket();
    }
  };

  const getTicketAirline = (id) => {
    let airline = {};
    if (dataFilter?.airlines) {
      airline = dataFilter.airlines.find((al) => al.id === id);
    }
    return airline ? airline : {};
  };

  if (dataTickets && dataTickets.length) {
    return (
      <>
        <Box className={classes.listFlightCard}>
          {dataTickets.map((el, index) => (
            <Box key={index} mb={2}>
              <Box>
                <TicketFlightCard
                  item={el.outbound}
                  airline={getTicketAirline(el.outbound.aid)}
                  transitTickets={transitTickets}
                  ticketClass={ticketClass}
                  tid={el.tid}
                  requestId={requestId}
                  isExpanded={expanded[index]}
                  isTwoWay={true}
                  onClickButton={toggleDrawer(true, index)}
                  isNetPrice={isNetPrice}
                />
              </Box>
              <Collapse
                in={expanded[index]}
                timeout="auto"
                unmountOnExit
                style={{ marginTop: "-10px" }}
              >
                <FlightInfoExpand
                  item={el.outbound}
                  airline={getTicketAirline(el.outbound.aid)}
                  transitTickets={transitTickets}
                  ticketClass={ticketClass}
                  tid={el.tid}
                  requestId={requestId}
                  isTwoWay={true}
                  data={data}
                  isExpanded={expanded[index]}
                  onClickButton={toggleDrawer(true, index)}
                  isNetPrice={isNetPrice}
                />
              </Collapse>
            </Box>
          ))}
        </Box>
      </>
    );
  }
};

export default FlightResultBoxTwoWay;
