import DialogCheckTime2Ways from "@components/common/modal/flight/DialogCheckTime2Ways";
import {
  Box,
  Divider,
  makeStyles,
  Typography,
  useTheme,
} from "@material-ui/core";
import { IconArrowUp, IconRunningOut } from "@public/icons";
import ButtonComponent from "@src/button/Button";
import Image from "@src/image/Image";
import { listString } from "@utils/constants";
import { checkFlightTime2Ways, getTicketClass, Policy } from "@utils/helpers";
import { useRouter } from "next/router";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: theme.palette.white.main,
    padding: "0px 24px",
    boxShadow: "0px 1px 1px rgba(0, 0, 0, 0.05)",
    borderRadius: 8,
  },
  wrapper: {
    borderRadius: 8,
    backgroundColor: theme.palette.white.main,
    padding: 12,
  },
  button: {
    width: 124,
    marginTop: 8,
  },
  divider: {
    height: 2,
    color: theme.palette.gray.grayLight22,
    marginTop: 16,
  },
  wrapCard: {
    display: "flex",
    alignItems: "center",
  },
  wrapCardLeft: {
    // marginLeft: 72,
  },
  wrapCardRight: {
    textAlign: "right",
  },
  airlineCode: {
    color: theme.palette.gray.grayDark7,
  },
  airlineFromTo: {
    color: theme.palette.black.black3,
    fontSize: 18,
    lineHeight: "22px",
    display: "flex",
  },
  flightTimeBox: {
    display: "flex",
    color: theme.palette.black.black3,
  },
  wrapFlight: {
    display: "flex",
  },
  airlineLogo: {
    width: 32,
    height: 32,
  },
  iconDot: {
    margin: 6,
  },
  airlineName: {
    color: theme.palette.black.black3,
    whiteSpace: "nowrap",
  },
  flightCode: {
    color: theme.palette.gray.grayDark8,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  wrapAirBrand: {
    display: "flex",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    overflow: "hidden",
    marginLeft: 8,
    color: theme.palette.black.black3,
  },
  iconArrow: {
    margin: "0 12px",
  },
  subPrice: {
    textDecorationLine: "line-through",
    textAlign: "right",
    color: theme.palette.gray.grayDark8,
    marginRight: 8,
  },
  mainPrice: {
    color: theme.palette.black.black3,
    textAlign: "right",
    fontSize: 18,
    lineHeight: "22px",
  },
  bestPriceBox: {
    width: 96,
    height: 20,
    whiteSpace: "nowrap",
    padding: "2px 6px",
    borderRadius: 4,
    display: "flex",
    alignItems: "center",
    margin: "8px 0",
  },
  runningOut: {
    display: "flex",
    justifyContent: "flex-end",
    height: 20,
    width: 96,
    border: "none",
    color: theme.palette.red.redLight5,
  },
}));

const FlightInfoExpand = ({
  item,
  airline,
  transitTickets,
  ticketClass,
  tid,
  requestId,
  isTwoWay,
  data,
  isExpanded,
  onClickButton = () => {},
  isNetPrice,
  tempTicketOutBound,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();

  const [isDialogCheckTime2Ways, setDialogCheckTime2Ways] = useState(false);

  let transitAirPortsStr = "";
  let transitStopsStr = "";
  if (item.ticketdetail?.transitTickets) {
    transitAirPortsStr = item.ticketdetail.transitTickets
      .map((el) => {
        return el.facilities?.length
          ? getAirtCraftInfo(el.facilities)
          : undefined;
      })
      .join(", ");
    transitStopsStr = item.ticketdetail.transitTickets
      .map((el) => {
        return el.departureAirport;
      })
      .join(", ");
  }
  const getAirtCraftInfo = (facilities) => {
    const airCraftInfo = facilities?.find((fa) => fa?.code === "AIRCRAFT_INFO");
    return airCraftInfo ? airCraftInfo.description : "";
  };
  const airCraft = getAirtCraftInfo(item?.ticketdetail?.facilities);

  const isTransit = !!item?.outbound?.transitTickets;

  const tempData = isTwoWay
    ? data?.outbound?.tickets
    : router?.query?.ticketOutBoundId
    ? data?.inbound?.tickets
    : data?.tickets;
  const filterDataTickets = tempData
    ?.filter((element) => {
      return element?.outbound?.flightNumber === item?.flightNumber;
    })
    .slice(1, 3);
  return (
    <Box className={classes.container}>
      <Box className={classes.bodyBox}>
        <Divider
          style={{
            margin: "8px 0px",
            height: 1,
            color: theme.palette.gray.grayLight22,
          }}
        />
        {filterDataTickets &&
          filterDataTickets.map((element, index) => (
            <Box key={index}>
              <Box className={classes.wrapCard} pt={1}>
                <Box className={classes.wrapFlight} width="35%">
                  <Box display="flex" alignItems="center" width="100%">
                    <Typography variant="caption" style={{ marginLeft: 48 }}>
                      {getTicketClass(element?.outbound, ticketClass)}
                    </Typography>
                    <div
                      style={{
                        height: "18px",
                        marginLeft: "5px",
                        minWidth: "18px",
                        overflow: "hidden",
                        alignItems: "center",
                        borderRadius: "2px",
                        padding: "0 5px",
                        display: "flex",
                        justifyContent: "center",
                        color: "white",
                        backgroundColor: theme.palette.primary.main,
                        fontWeight: 500,
                        fontSize: "14px",
                      }}
                    >
                      {transitTickets
                        ? transitTickets[0].detailTicketClass
                        : element?.outbound?.ticketdetail?.detailTicketClass}
                    </div>
                  </Box>
                </Box>
                <Box display="flex" width="45%" flexDirection="column">
                  {isTransit &&
                    element?.outbound?.polices &&
                    element?.outbound?.polices.map((ele, index) => (
                      <Policy item={ele} key={index} isMobile={false} />
                    ))}

                  {!isTransit &&
                    element?.outbound?.ticketdetail?.polices &&
                    element?.outbound?.ticketdetail.polices?.map(
                      (ele, index) => (
                        <Policy item={ele} key={index} isMobile={false} />
                      )
                    )}
                </Box>
                <Box
                  display="flex"
                  flexDirection="column"
                  width="20%"
                  justifyContent="flex-end"
                  alignItems="flex-end"
                >
                  <Box display="flex" alignItems="center">
                    {element?.outbound?.ticketdetail?.discountAdultUnit > 0 && (
                      <Typography variant="body2" className={classes.subPrice}>
                        {`${(
                          element?.outbound?.ticketdetail?.priceAdultUnit +
                          element?.outbound?.ticketdetail?.discountAdultUnit
                        ).formatMoney()}đ`}
                      </Typography>
                    )}
                    <Typography
                      variant="subtitle1"
                      className={classes.mainPrice}
                    >
                      {isNetPrice
                        ? `${element?.outbound?.ticketdetail.farePrice.formatMoney()}đ`
                        : `${element?.outbound?.ticketdetail.priceAdultUnit.formatMoney()}đ`}
                    </Typography>
                  </Box>
                  {element?.outbound?.ticketdetail?.isBestPrice === 1 ? ( // for label best price
                    <Box className={classes.bestPriceBox}>
                      <Image srcImage={airline.logo} />
                      <Typography variant="body2">
                        {listString.IDS_TEXT_FLIGHT_BEST_PRICE}
                      </Typography>
                    </Box>
                  ) : null}
                  {element?.outbound?.isRunningOut &&
                    element?.outbound?.isRunningOut === 1 && (
                      <>
                        <Box className={classes.runningOut}>
                          <Box marginRight={"6px"}>
                            <IconRunningOut />
                          </Box>
                          <Typography variant="body2">
                            {listString.IDS_TEXT_FLIGHT_RUNNING_OUT}
                          </Typography>
                        </Box>
                      </>
                    )}
                  <ButtonComponent
                    type="submit"
                    className={classes.button}
                    backgroundColor={theme.palette.secondary.main}
                    fontSize={14}
                    fontWeight={600}
                    borderRadius={8}
                    handleClick={() => {
                      if (!checkFlightTime2Ways(tempTicketOutBound, item)) {
                        setDialogCheckTime2Ways(true);
                        return;
                      }
                      if (isTwoWay) {
                        router.push({
                          pathname: "/ve-may-bay/result/inbound",
                          query: {
                            ...router?.query,
                            agencyOutBoundId: element?.outbound?.aid,
                            ticketOutBoundId: element?.tid,
                            needPassport: element?.outbound
                              ? element?.outbound?.ticketdetail?.needPassport
                              : false,
                          },
                        });
                      } else {
                        if (router?.query?.ticketOutBoundId) {
                          router.push({
                            pathname: "/ve-may-bay/payment",
                            query: {
                              ...router?.query,
                              agencyOutBoundId: router?.query?.agencyOutBoundId,
                              ticketOutBoundId: router?.query?.ticketOutBoundId,
                              agencyInBoundId: element?.outbound?.aid,
                              requestId: requestId,
                              ticketInBoundId: element?.tid,
                              needPassport: element?.outbound
                                ? element?.outbound?.ticketdetail?.needPassport
                                : false,
                            },
                          });
                        } else {
                          router.push({
                            pathname: "/ve-may-bay/payment",
                            query: {
                              ...router?.query,
                              agencyOutBoundId: element?.outbound?.aid,
                              requestId: requestId,
                              ticketOutBoundId: element?.tid,
                              needPassport: element?.outbound
                                ? element?.outbound?.ticketdetail?.needPassport
                                : false,
                            },
                          });
                        }
                      }
                    }}
                  >
                    {listString.IDS_MT_TEXT_SELECT}
                  </ButtonComponent>
                </Box>
              </Box>
              <Divider
                style={{
                  margin: "8px 0px",
                  height: 1,
                  color: theme.palette.gray.grayLight22,
                }}
              />
            </Box>
          ))}
      </Box>
      {isExpanded && !!filterDataTickets.length && (
        <Box display="flex" justifyContent="center">
          <ButtonComponent
            backgroundColor={theme.palette.white.main}
            color={theme.palette.blue.blueLight8}
            fontSize={14}
            width={240}
            handleClick={onClickButton}
            style={{ paddingTop: 0 }}
          >
            Thu gọn
            <IconArrowUp
              className="svgFillAll"
              style={{ stroke: theme.palette.blue.blueLight8, marginLeft: 8 }}
            />
          </ButtonComponent>
        </Box>
      )}
      <DialogCheckTime2Ways
        open={isDialogCheckTime2Ways}
        toggleDrawer={() => setDialogCheckTime2Ways(false)}
      />
    </Box>
  );
};

export default FlightInfoExpand;
