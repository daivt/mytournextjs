import { Box, ButtonBase, Typography, useTheme } from "@material-ui/core";
import { DATE_FORMAT_BACK_END } from "@utils/moment";
import moment from "moment";
import { useRouter } from "next/router";
import React from "react";

const FlightDatePrice = ({
  isSelect,
  setIsSelect,
  index,
  ticket,
  dataQuery,
}) => {
  const router = useRouter();
  const theme = useTheme();

  const DAYS_BETWEEN = moment(
    dataQuery?.returnDate,
    DATE_FORMAT_BACK_END,
    true
  ).diff(moment(dataQuery?.departureDate, DATE_FORMAT_BACK_END, true), "days");
  return (
    <ButtonBase
      style={{
        backgroundColor: isSelect ? "#e5f8fe" : "#ffffff",
        borderRadius: "8px",
        height: 48,
        margin: "4px 2px",
        padding: 16,
      }}
      onClick={() => {
        router.push({
          pathname: "/ve-may-bay/result/outbound",
          query: {
            ...router?.query,
            departureDate: ticket?.departureDate,
            returnDate: dataQuery?.returnDate
              ? moment(ticket?.departureDate, DATE_FORMAT_BACK_END, true)
                  .add(DAYS_BETWEEN, "days")
                  .format(DATE_FORMAT_BACK_END)
              : null,
          },
        });
        setIsSelect(index);
      }}
    >
      <Box display="flex" flexDirection="column">
        <Typography
          variant="caption"
          style={{
            whiteSpace: "nowrap",
            display: "flex",
            justifyContent: "center",
            color: isSelect
              ? theme.palette.blue.blueLight8
              : theme.palette.gray.grayDark8,
          }}
        >
          {ticket?.departureDate && !dataQuery?.returnDate
            ? moment(ticket?.departureDate, DATE_FORMAT_BACK_END, true)
                .locale("vi_VN")
                .format("ddd")
                .replace("T", "T")
                .replace("CN", "CN")
            : ""}
          {ticket?.departureDate && !dataQuery?.returnDate ? ", " : ""}
          {moment(ticket?.departureDate, DATE_FORMAT_BACK_END, true).format(
            "DD"
          )}
          {" tháng "}
          {moment(ticket?.departureDate, DATE_FORMAT_BACK_END, true).format(
            "MM"
          )}{" "}
          {dataQuery?.returnDate ? "- " : ""}
          {dataQuery?.returnDate
            ? moment(ticket?.departureDate, DATE_FORMAT_BACK_END, true)
                .add(DAYS_BETWEEN, "days")
                .format("DD/MM")
            : ""}
        </Typography>
        <Typography
          style={{
            whiteSpace: "nowrap",
            display: "flex",
            justifyContent: "center",
            color: isSelect ? theme.palette.blue.blueLight8 : "#000000",
            fontWeight: 600,
            marginTop: 4,
            fontSize: 14,
          }}
        >
          {ticket?.farePrice
            ? Math.floor(ticket?.farePrice).formatMoney() + "đ"
            : "-"}
        </Typography>
      </Box>
    </ButtonBase>
  );
};

export default FlightDatePrice;
