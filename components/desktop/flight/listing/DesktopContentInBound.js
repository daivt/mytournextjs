import { searchOneWayTickets, searchRoundTripTickets } from "@api/flight";
import LayoutFlight from "@components/layout/desktop/LayoutFlight";
import { Box, Breadcrumbs, IconButton, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import { IconArrowFlightLong, IconEdit2 } from "@public/icons";
import snackbarSetting from "@src/alert/Alert";
import Image from "@src/image/Image";
import Link from "@src/link/Link";
import { SORTER_FLIGHT_LIST } from "@utils/constants";
import {
  filterAndSort,
  flightCheapestCmp,
  flightFastestCmp,
  flightTimeTakeOffCmp,
  handleTitleFlight,
} from "@utils/helpers";
import { DATE_FORMAT } from "@utils/moment";
import { debounce, uniqBy } from "lodash";
import moment from "moment";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import React, { useEffect, useState } from "react";
import TicketTitle from "../common/TicketTitle";
import FiltersFlight from "./components/FiltersFlight";
import ListFlight from "./components/ListFlight";

const useStyles = makeStyles((theme) => ({
  homeWrapper: { width: "100%", background: theme.palette.blue.blueLight14 },
  homeContainer: { maxWidth: 1188, margin: "0 auto" },
  homeMainLayout: { display: "flex", width: "100%", alignItems: "flex-start" },
  contentContainer: {
    display: "flex",
    alignItems: "flex-start",
    alignContent: "flex-start",
  },
  divider: {
    width: "100%",
    height: 4,
    background: theme.palette.gray.grayLight22,
    marginTop: 20,
  },
  airlineFromTo: {
    color: theme.palette.black.black3,
    fontSize: 18,
    lineHeight: "22px",
    display: "flex",
  },
  headerHome: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 24,
  },
  airlineCode: {
    color: theme.palette.gray.grayDark7,
  },
  locationBox: {
    display: "flex",
    alignItems: "center",
    background: theme.palette.gray.grayLight22,
    borderRadius: 12,
    padding: "16px 30px",
    fontWeight: 600,
    fontSize: 14,
    lineHeight: "17px",
    cursor: "pointer",
  },
  hotelInfoText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    width: "100%",
  },
  contentBlock: { background: "white" },
  textLink: {
    color: theme.palette.black.black3,
    "&:hover": {
      textDecoration: "none",
    },
  },
  iconDot: {
    margin: "6px",
  },
  airlineLogo: {
    width: 24,
    height: 24,
    objectFit: "contain",
  },
  iconArrow: {
    margin: "0 20px",
  },
  flightCode: {
    color: theme.palette.gray.grayDark8,
    marginTop: 6,
    lineHeight: "17px",
  },
  wrapAirBrand: {
    display: "flex",
    overflow: "hidden",
    color: theme.palette.black.black3,
  },
  airlineName: {
    color: theme.palette.black.black3,
    lineHeight: "17px",
    whiteSpace: "nowrap",
  },
  outboundInfo: {
    display: "flex",
    backgroundColor: theme.palette.white.main,
    marginLeft: -16,
    width: "calc(100% + 32px)",
    marginBottom: 40,
    boxShadow: "0px 1px 1px rgba(0, 0, 0, 0.05)",
    borderRadius: 8,
    padding: 16,
    justifyContent: "space-between",
  },
}));
const DesktopContentInBound = ({
  dataQuery = {},
  codeFlight = {},
  query = {},
}) => {
  const [paramsFilterAndSort, setParamsFilterAndSort] = useState({
    priceFilter: 0,
    airlines: [],
    flyTimeFilter: 0,
    liftTimeFilter: [],
    seatFilter: [],
    sortBy: 1,
    numStops: [],
  });
  const [searchCompleted, setSearchCompleted] = useState(0);
  const [dataTickets, setDataTickets] = useState([]);
  const [dataFilter, setDataFilter] = useState({});
  const [paramFilter, setParamFilter] = useState({ sortBy: 1 });
  const [data, setData] = useState();
  const [isInBound, setIsInBound] = useState(false);
  const [ticketOutBound, setTicketOutBound] = useState(null);
  const [valueSort, setValueSort] = useState("net");
  let breakPolling = true;

  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const handleSetSingleParam = (index, value) => {
    setParamFilter({
      ...paramsFilterAndSort,
      [index]: value,
    });
    setParamsFilterAndSort({
      ...paramsFilterAndSort,
      [index]: value,
    });
  };

  const handleSetArrayParam = (field, id) => {
    const fieldCheck = paramsFilterAndSort[field];
    if (fieldCheck.includes(id)) {
      setParamsFilterAndSort({
        ...paramsFilterAndSort,
        [field]: fieldCheck.filter((el) => el !== id),
      });
      setParamFilter({
        ...paramsFilterAndSort,
        [field]: fieldCheck.filter((el) => el !== id),
      });
    } else {
      setParamsFilterAndSort({
        ...paramsFilterAndSort,
        [field]: [...fieldCheck, id],
      });
      setParamFilter({
        ...paramsFilterAndSort,
        [field]: [...fieldCheck, id],
      });
    }
  };

  const handleResetFilter = (value) => () => {
    setParamsFilterAndSort({
      ...paramsFilterAndSort,
      priceFilter: 0,
      airlines: [],
      flyTimeFilter: 0,
      liftTimeFilter: [],
      seatFilter: [],
      sortBy: 1,
      numStops: [],
    });
  };

  const searchData = React.useCallback(
    debounce(
      async () => {
        if (!dataQuery) {
          return;
        }
        let waitFor = null;
        let polling = true;
        setDataTickets([]);
        setData();
        setSearchCompleted(0);
        let i = 1;
        let n = 1;
        while (polling && breakPolling) {
          const dataPost = {
            waitFor,
            from_airport: dataQuery && dataQuery.origin_code,
            to_airport: dataQuery && dataQuery.destination_code,
            depart: `${dataQuery.departureDate}`,
            return: dataQuery.returnDate
              ? `${dataQuery.returnDate}`
              : undefined,
            num_adults: dataQuery.adultCount,
            num_childs: dataQuery.childCount,
            num_infants: dataQuery.infantCount,
            one_way: !dataQuery.returnDate ? "1" : "0",
            currency: "VND",
            lang: "vi",
            filters: {
              airlines: [],
              paging: { itemsPerPage: 200, page: 1 },
              sort: { priceUp: true },
              ticketClassCodes: paramFilter?.seatFilter?.length
                ? paramFilter?.seatFilter
                : dataQuery.seatSearch
                ? [dataQuery.seatSearch]
                : [],
            },
            sort: "tripi_recommended",
          };
          const { data } = !dataQuery.returnDate
            ? await searchOneWayTickets(dataPost)
            : await searchRoundTripTickets(dataPost);
          if (data?.code !== 200) {
            data?.message &&
              enqueueSnackbar(
                data?.message,
                snackbarSetting((key) => closeSnackbar(key), { color: "error" })
              );
            break;
          }
          if (data?.data?.tickets) {
            setDataTickets(data?.data?.tickets);
          } else {
            setIsInBound(false);
            setTicketOutBound(data?.data?.outbound?.tickets);
            setDataTickets(data?.data?.inbound?.tickets);
          }
          setData(data?.data);
          const fieldFilterList = {
            airlines: data?.data?.airlines,
            airports: data?.data?.airports,
            filters: data?.data?.filters,
            ticketClasses: data?.data?.ticketClasses,
          };
          setDataFilter(fieldFilterList);
          waitFor = data?.data?.waitFor;
          polling = data?.data?.polling;
          if (i === 1) {
            n = 1 + waitFor.split(",").length;
          }
          const searchPercent = (100 * i) / n;

          setSearchCompleted(searchPercent < 100 ? searchPercent : 99);
          i += 1;
        }
        setSearchCompleted(100);
      },
      500,
      {
        trailing: true,
        leading: false,
      }
    ),
    [dataQuery]
  );

  const filterTickets = (dataTickets) => {
    let tempTickets = uniqBy(dataTickets, function(e) {
      return e?.outbound?.flightNumber && e?.outbound?.departureTimeStr;
    });
    dataFilter?.airlines?.map((ele) => {
      tempTickets.find((element, index) => {
        if (element?.outbound?.aid === ele?.id) {
          tempTickets[index].outbound.ticketdetail.isBestPrice = 1;
          return true;
        }
        return false;
      });
    });
    return tempTickets.slice(0, 100);
  };

  const filteredTickets = React.useMemo(() => {
    const { sortBy } = paramFilter;
    return filterAndSort(
      filterTickets(dataTickets),
      paramFilter,
      sortBy === SORTER_FLIGHT_LIST[1].id
        ? flightCheapestCmp
        : sortBy === SORTER_FLIGHT_LIST[3].id
        ? flightFastestCmp
        : sortBy === SORTER_FLIGHT_LIST[2].id
        ? flightTimeTakeOffCmp
        : undefined
    );
  }, [data, paramFilter, dataFilter]);

  const getAirtCraftInfo = (facilities) => {
    const airCraftInfo = facilities?.find((fa) => fa?.code === "AIRCRAFT_INFO");
    return airCraftInfo ? airCraftInfo.description : "";
  };
  const tempTicketOutBound = ticketOutBound?.find(
    (ele) => ele?.tid == dataQuery?.ticketOutBoundId
  );
  const airline = dataFilter?.airlines?.find(
    (element) => element.id === tempTicketOutBound?.outbound?.aid
  );

  const airCraft = getAirtCraftInfo(
    tempTicketOutBound?.outbound?.ticketdetail?.facilities
  );

  let paramsUrl = { ...dataQuery };
  delete paramsUrl?.slug;

  useEffect(() => {
    return () => {
      breakPolling = false;
    };
  }, []);

  useEffect(() => {
    searchData();
  }, [dataQuery]);

  return (
    <LayoutFlight
      isHeader
      isFooter
      type="listing"
      paramsUrl={dataQuery}
      query={query}
      dataQuery={dataQuery}
      {...handleTitleFlight(query, codeFlight)}
    >
      <Box className={classes.homeWrapper}>
        <Box className={classes.homeContainer}>
          <Box className={classes.homeMainLayout}>
            <Box
              style={{
                width: "100%",
                paddingTop: 24,
                marginLeft: "auto",
                paddingLeft: 0,
              }}
            >
              <Box className={classes.headerHome}>
                <Box className={classes.hotelInfoText}>
                  <Box>
                    <Breadcrumbs separator="›" style={{ marginBottom: 16 }}>
                      <Link color="body2" href={{ pathname: "/ve-may-bay" }}>
                        <Typography variant="body2">Chuyến bay</Typography>
                      </Link>
                      <Link color="body2" href={{ pathname: "" }}>
                        <Typography variant="body2">
                          Chuyến bay từ {codeFlight.depart} đến{" "}
                          {codeFlight.arrival}
                        </Typography>
                      </Link>
                    </Breadcrumbs>
                    <Box className={classes.outboundInfo}>
                      <Box width="48%">
                        <TicketTitle
                          isInBound={true}
                          paramsUrl={paramsUrl}
                          codeFlight={{
                            depart: codeFlight.depart,
                            arrival: codeFlight.arrival,
                            codeDep: codeFlight?.code?.split("-")[0],
                            codeArr: codeFlight?.code?.split("-")[1],
                          }}
                          numberTickets={data?.outbound?.total}
                          date={dataQuery?.departureDate}
                          dataQuery={dataQuery}
                          isSmall
                        />
                      </Box>
                      {airline && (
                        <Box display="flex" width="20%">
                          <Box display="flex" mr={12 / 8}>
                            <Image
                              srcImage={airline?.logo}
                              className={classes.airlineLogo}
                            />
                          </Box>
                          <Box
                            className={classes.wrapAirBrand}
                            flexDirection="column"
                          >
                            <Typography
                              component={"span"}
                              variant="caption"
                              className={classes.airlineName}
                            >
                              {airline?.name}
                            </Typography>
                            <Typography
                              component={"span"}
                              variant="caption"
                              className={classes.flightCode}
                            >
                              {airCraft
                                ? `${
                                    tempTicketOutBound?.outbound?.flightNumber
                                  }/ ${airCraft.split("-")[0]}`
                                : tempTicketOutBound?.outbound.flightNumber}
                            </Typography>
                          </Box>
                        </Box>
                      )}
                      {tempTicketOutBound && (
                        <Box display="flex">
                          <Box className={classes.wrapCardLeft}>
                            <Typography
                              variant="subtitle1"
                              component="span"
                              className={classes.airlineFromTo}
                            >
                              {tempTicketOutBound?.outbound?.departureTimeStr}
                            </Typography>
                            <Typography
                              variant="caption"
                              component="span"
                              className={classes.airlineCode}
                            >
                              {tempTicketOutBound?.outbound?.departureAirport}
                            </Typography>
                          </Box>
                          <IconArrowFlightLong className={classes.iconArrow} />
                          <Box className={classes.wrapCardRight}>
                            <Typography
                              variant="subtitle1"
                              component="span"
                              className={classes.airlineFromTo}
                            >
                              {tempTicketOutBound?.outbound?.arrivalTimeStr}
                              {!!moment(
                                tempTicketOutBound?.outbound?.arrivalDayStr,
                                DATE_FORMAT
                              ).diff(
                                moment(
                                  tempTicketOutBound?.outbound?.departureDayStr,
                                  DATE_FORMAT
                                ),
                                "days"
                              ) && (
                                <Typography
                                  variant="caption"
                                  style={{
                                    position: "relative",
                                    top: -6,
                                    left: 4,
                                    color: theme.palette.pink.main,
                                  }}
                                >
                                  +
                                  {moment(
                                    tempTicketOutBound?.outbound?.arrivalDayStr,
                                    DATE_FORMAT
                                  ).diff(
                                    moment(
                                      tempTicketOutBound?.outbound
                                        ?.departureDayStr,
                                      DATE_FORMAT
                                    ),
                                    "days"
                                  )}
                                  ngày
                                </Typography>
                              )}
                            </Typography>
                            <Typography
                              variant="caption"
                              component="span"
                              className={classes.airlineCode}
                            >
                              {tempTicketOutBound?.outbound?.arrivalAirport}
                            </Typography>
                          </Box>
                        </Box>
                      )}
                      <Box display="flex" alignItems="center" ml="50px">
                        <IconButton
                          style={{ padding: 0, borderRadius: "4px" }}
                          onClick={() => {
                            router.push({
                              pathname: "/ve-may-bay/result/outbound",
                              query: {
                                ...router?.query,
                              },
                            });
                          }}
                        >
                          <IconEdit2 style={{ marginRight: 8 }} />
                          <Typography variant="caption" color="primary">
                            Thay đổi chuyến bay
                          </Typography>
                        </IconButton>
                      </Box>
                    </Box>
                    <TicketTitle
                      isInBound={true}
                      paramsUrl={paramsUrl}
                      codeFlight={{
                        depart: codeFlight.depart,
                        arrival: codeFlight.arrival,
                        codeDep: codeFlight?.code?.split("-")[0],
                        codeArr: codeFlight?.code?.split("-")[1],
                      }}
                      numberTickets={data?.inbound?.total}
                      date={dataQuery?.returnDate}
                      dataQuery={dataQuery}
                      isTwoWays
                    />
                  </Box>
                </Box>
              </Box>
              <Box className={classes.contentContainer}>
                <FiltersFlight
                  dataFilter={dataFilter}
                  paramFilter={paramsFilterAndSort}
                  setParamFilter={setParamFilter}
                  handleSetSingleParam={handleSetSingleParam}
                  handleSetArrayParam={handleSetArrayParam}
                  handleReset={handleResetFilter}
                  searchCompleted={searchCompleted}
                  data={data}
                  valueSort={valueSort}
                />
                <Box display="flex" flexDirection="column" width="75%">
                  {/* <FlightHeaderListing
                    dataQuery={dataQuery}
                    isInBound={isInBound}
                    isHideScroll={false}
                  /> */}
                  <ListFlight
                    dataFilter={dataFilter}
                    setParamFilter={setParamFilter}
                    searchCompleted={searchCompleted}
                    isInBound={isInBound}
                    data={data}
                    filteredTickets={filteredTickets}
                    sortOptions={SORTER_FLIGHT_LIST}
                    dataQuery={dataQuery}
                    paramFilter={paramFilter}
                    valueSort={valueSort}
                    setValueSort={setValueSort}
                    tempTicketOutBound={tempTicketOutBound}
                  />
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </LayoutFlight>
  );
};

export default DesktopContentInBound;
