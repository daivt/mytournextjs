import { searchOneWayTickets, searchRoundTripTickets } from "@api/flight";
import LayoutFlight from "@components/layout/desktop/LayoutFlight";
import { Box, Breadcrumbs, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import snackbarSetting from "@src/alert/Alert";
import Link from "@src/link/Link";
import { SORTER_FLIGHT_LIST } from "@utils/constants";
import {
  filterAndSort,
  flightCheapestCmp,
  flightFastestCmp,
  flightTimeTakeOffCmp,
  handleTitleFlight,
  isEmpty,
} from "@utils/helpers";
import { debounce, uniqBy } from "lodash";
import moment from "moment";
import { useSnackbar } from "notistack";
import React, { useEffect, useState } from "react";
import TicketTitle from "../common/TicketTitle";
import FiltersFlight from "./components/FiltersFlight";
import FlightHeaderListing from "./components/FlightHeaderListing";
import ListFlight from "./components/ListFlight";

const useStyles = makeStyles((theme) => ({
  homeWrapper: { width: "100%", background: theme.palette.blue.blueLight14 },
  homeContainer: { maxWidth: 1188, margin: "0 auto" },
  homeMainLayout: { display: "flex", width: "100%", alignItems: "flex-start" },
  contentContainer: {
    display: "flex",
    alignItems: "flex-start",
    alignContent: "flex-start",
  },
  divider: {
    width: "100%",
    height: 4,
    background: theme.palette.gray.grayLight22,
    marginTop: 20,
  },
  headerHome: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 24,
  },
  locationBox: {
    display: "flex",
    alignItems: "center",
    background: theme.palette.gray.grayLight22,
    borderRadius: 12,
    padding: "16px 30px",
    fontWeight: 600,
    fontSize: 14,
    lineHeight: "17px",
    cursor: "pointer",
  },
  hotelInfoText: { fontWeight: 600, fontSize: 24, lineHeight: "29px" },
  contentBlock: { background: "white" },
  textLink: {
    color: theme.palette.black.black3,
    "&:hover": {
      textDecoration: "none",
    },
  },
  iconDot: {
    margin: "6px",
  },
}));
const DesktopContentOutBound = ({
  dataQuery = {},
  codeFlight = {},
  query = {},
  airlinesId = 0,
}) => {
  const [paramsFilterAndSort, setParamsFilterAndSort] = useState({
    priceFilter: 0,
    airlines: airlinesId ? [airlinesId] : [],
    flyTimeFilter: 0,
    liftTimeFilter: [],
    seatFilter: [],
    sortBy: 1,
    numStops: [],
  });
  const [searchCompleted, setSearchCompleted] = useState(0);
  const [time, setTime] = useState(moment());
  const [dataTickets, setDataTickets] = useState([]);
  const [dataFilter, setDataFilter] = useState({});
  const [data, setData] = useState();
  const [isInBound, setIsInBound] = useState(false);
  const [valueSort, setValueSort] = useState("net");
  let breakPolling = true;

  const classes = useStyles();
  const theme = useTheme();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const handleSetSingleParam = (index, value) => {
    setParamsFilterAndSort({
      ...paramsFilterAndSort,
      [index]: value,
    });
  };

  const handleSetArrayParam = (field, id) => {
    const fieldCheck = paramsFilterAndSort[field];
    if (fieldCheck.includes(id)) {
      setParamsFilterAndSort({
        ...paramsFilterAndSort,
        [field]: fieldCheck.filter((el) => el !== id),
      });
    } else {
      setParamsFilterAndSort({
        ...paramsFilterAndSort,
        [field]: [...fieldCheck, id],
      });
    }
  };

  const handleResetFilter = (value) => () => {
    setParamsFilterAndSort({
      ...paramsFilterAndSort,
      priceFilter: 0,
      airlines: [],
      flyTimeFilter: 0,
      liftTimeFilter: [],
      seatFilter: [],
      sortBy: 1,
      numStops: [],
    });
  };

  const searchData = React.useCallback(
    debounce(
      async () => {
        if (!dataQuery) {
          return;
        }
        let waitFor = null;
        let polling = true;
        setDataTickets([]);
        setData();
        setSearchCompleted(0);
        let i = 1;
        let n = 1;
        while (polling && breakPolling) {
          const dataPost = {
            waitFor,
            from_airport: dataQuery && dataQuery.origin_code,
            to_airport: dataQuery && dataQuery.destination_code,
            depart: `${dataQuery.departureDate}`,
            return: dataQuery.returnDate
              ? `${dataQuery.returnDate}`
              : undefined,
            num_adults: dataQuery.adultCount,
            num_childs: dataQuery.childCount,
            num_infants: dataQuery.infantCount,
            one_way: !dataQuery.returnDate ? "1" : "0",
            currency: "VND",
            lang: "vi",
            filters: {
              airlines: [],
              paging: { itemsPerPage: 200, page: 1 },
              sort: { priceUp: true },
              ticketClassCodes: paramsFilterAndSort?.seatFilter?.length
                ? paramsFilterAndSort?.seatFilter
                : dataQuery.seatSearch
                ? [dataQuery.seatSearch]
                : [],
            },
            sort: "tripi_recommended",
          };
          const { data } = !dataQuery.returnDate
            ? await searchOneWayTickets(dataPost)
            : await searchRoundTripTickets(dataPost);
          if (data?.code !== 200) {
            data?.message &&
              enqueueSnackbar(
                data?.message,
                snackbarSetting((key) => closeSnackbar(key), { color: "error" })
              );
            break;
          }
          if (data?.data?.tickets) {
            setDataTickets(data?.data?.tickets);
          } else {
            setIsInBound(true);
            setDataTickets(data?.data?.outbound?.tickets);
          }
          setData(data?.data);
          const fieldFilterList = {
            airlines: data?.data?.airlines,
            airports: data?.data?.airports,
            filters: data?.data?.filters,
            ticketClasses: data?.data?.ticketClasses,
          };
          setDataFilter(fieldFilterList);
          waitFor = data?.data?.waitFor;
          polling = data?.data?.polling;
          if (i === 1) {
            n = 1 + waitFor.split(",").length;
          }
          const searchPercent = (100 * i) / n;

          setSearchCompleted(searchPercent < 100 ? searchPercent : 99);
          i += 1;
        }
        setTime(moment());
        setSearchCompleted(100);
      },
      500,
      {
        trailing: true,
        leading: false,
      }
    ),
    [dataQuery]
  );

  const filterTickets = (dataTickets) => {
    let tempTickets = uniqBy(dataTickets, function(e) {
      return e?.outbound?.flightNumber && e?.outbound?.departureTimeStr;
    });
    dataFilter?.airlines?.map((ele) => {
      tempTickets.find((element, index) => {
        if (element?.outbound?.aid === ele?.id) {
          tempTickets[index].outbound.ticketdetail.isBestPrice = 1;
          return true;
        }
        return false;
      });
    });
    return tempTickets.slice(0, 100);
  };

  const filteredTickets = React.useMemo(() => {
    const { sortBy } = paramsFilterAndSort;
    return filterAndSort(
      filterTickets(dataTickets),
      paramsFilterAndSort,
      sortBy === SORTER_FLIGHT_LIST[1].id
        ? flightCheapestCmp
        : sortBy === SORTER_FLIGHT_LIST[3].id
        ? flightFastestCmp
        : sortBy === SORTER_FLIGHT_LIST[2].id
        ? flightTimeTakeOffCmp
        : undefined
    );
  }, [data, paramsFilterAndSort, dataFilter]);
  let paramsUrl = { ...dataQuery };
  delete paramsUrl?.slug;

  useEffect(() => {
    return () => {
      breakPolling = false;
    };
  }, []);

  useEffect(() => {
    searchData();
  }, [dataQuery]);
  return (
    <LayoutFlight
      isHeader
      isFooter
      paramsUrl={dataQuery}
      dataQuery={dataQuery}
      query={query}
      type="listing"
      {...handleTitleFlight(query, codeFlight)}
    >
      <Box className={classes.homeWrapper}>
        <Box className={classes.homeContainer}>
          <Box className={classes.homeMainLayout}>
            <Box
              style={{
                width: "100%",
                paddingTop: 24,
                marginLeft: "auto",
                paddingLeft: 0,
              }}
            >
              <Box className={classes.headerHome}>
                <Box className={classes.hotelInfoText}>
                  <Box>
                    <Breadcrumbs separator="›" style={{ marginBottom: 16 }}>
                      <Link color="inherit" href={{ pathname: "/ve-may-bay" }}>
                        <Typography variant="body2">Chuyến bay</Typography>
                      </Link>
                      <Link color="inherit" href={{ pathname: "" }}>
                        <Typography variant="body2">
                          Chuyến bay từ {codeFlight.depart} đến{" "}
                          {codeFlight.arrival}
                        </Typography>
                      </Link>
                    </Breadcrumbs>
                    <TicketTitle
                      isInBound={isInBound}
                      paramsUrl={paramsUrl}
                      codeFlight={{
                        depart: codeFlight.depart,
                        arrival: codeFlight.arrival,
                        codeDep: codeFlight?.code?.split("-")[0],
                        codeArr: codeFlight?.code?.split("-")[1],
                      }}
                      numberTickets={
                        !isEmpty(dataQuery.returnDate)
                          ? data?.outbound?.total
                          : data?.total
                      }
                      date={dataQuery?.departureDate}
                      dataQuery={dataQuery}
                    />
                  </Box>
                </Box>
              </Box>
              <Box className={classes.contentContainer}>
                <FiltersFlight
                  dataFilter={dataFilter}
                  paramFilter={paramsFilterAndSort}
                  setParamFilter={setParamsFilterAndSort}
                  handleSetSingleParam={handleSetSingleParam}
                  handleSetArrayParam={handleSetArrayParam}
                  handleReset={handleResetFilter}
                  searchCompleted={searchCompleted}
                  isInBound={isInBound}
                  data={data}
                  paramsUrl={paramsUrl}
                  airlinesId={airlinesId}
                  valueSort={valueSort}
                />
                <Box display="flex" flexDirection="column" width="75%">
                  <FlightHeaderListing
                    dataQuery={dataQuery}
                    isInBound={isInBound}
                    isHideScroll={false}
                  />
                  <ListFlight
                    dataFilter={dataFilter}
                    setParamFilter={setParamsFilterAndSort}
                    searchCompleted={searchCompleted}
                    isInBound={isInBound}
                    data={data}
                    filteredTickets={filteredTickets}
                    sortOptions={SORTER_FLIGHT_LIST}
                    paramFilter={paramsFilterAndSort}
                    valueSort={valueSort}
                    setValueSort={setValueSort}
                  />
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </LayoutFlight>
  );
};

export default DesktopContentOutBound;
