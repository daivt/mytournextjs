import { useState, useEffect } from "react";
import { Box, Typography } from "@material-ui/core";
import { getChainsHotels } from "@api/homes";
import { makeStyles } from "@material-ui/styles";
import Image from "@src/image/Image";
import { IconArrowRight } from "@public/icons";
import { isEmpty } from "@utils/helpers";
import Link from "@src/link/Link";
import { listString, routeStatic } from "@utils/constants";
import SlideShow from "@components/common/slideShow/SlideShow";
import ButtonComponent from "@src/button/Button";

const useStyles = makeStyles((theme) => ({
  container: {
    background: "#FBF3F7",
    width: "calc(100% - 48px)",
    borderRadius: 12,
    margin: "0 24px",
    padding: "56px 0",
    marginBottom: 24,
  },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "column",
  },
  hotelGroup: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between",
  },
  headerText: { textAlign: "center" },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
  },
  titleSubText: {
    fontSize: 14,
    lineHeight: "17px",
    color: "#4A5568",
    marginTop: 8,
  },
  imageContent: {
    display: "flex",
    alignItems: "center",
    "& > .slick-list": {
      width: "calc(100% + 16px)",
      margin: "0 -8px",
    },
  },
  imageSlick: { width: 279, height: 279, borderRadius: 8, margin: "0 auto" },
  imageContainer: {
    position: "relative",
    width: 279,
    height: 279,
    borderRadius: 8,
    display: "flex",
    alignItems: "center",
  },
  imageLogo: {
    position: "absolute",
    top: 0,
    left: "50%",
    transform: "translateX(-50%)",
    background: "white",
    width: 99,
    height: 99,
    display: "flex",
    flexDirection: "column",
    padding: 10,
    borderRadius: "0 0 8px 8px",
  },
  borderTopImage: {
    height: 3,
    width: "calc(100% + 20px)",
    background: "#F1B21A",
    margin: "-10px -10px 7px -10px",
    borderRadius: "0px 0px 12px 12px",
  },
  imgLogoContent: {
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  iconArrow: {
    width: 16,
    height: 17,
    stroke: theme.palette.black.black3,
  },
  wrapArrow: {
    "&:hover": {
      "& svg": {
        stroke: theme.palette.blue.blueLight8,
        transition: "all ease .2s",
      },
    },
  },
  linkBtn: {
    "&:hover": {
      textDecoration: "none",
    },
  },
  myTourMallLogo: {
    width: 173,
    height: 46,
  },
}));
const customStyle = {
  background: "white",
  height: "40px",
  width: "40px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 2,
  borderRadius: "100px",
  boxShadow: "0px 8px 12px rgb(0 0 0 / 10%)",
  cursor: "pointer",
  position: "absolute",
  top: "50%",
  transform: "translate(-50%, -50%)",
};

function SamplePrevArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, left: 0 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight
        className={`svgFillAll ${classes.iconArrow}`}
        style={{ transform: "rotate(180deg)" }}
      />
    </div>
  );
}

function SampleNextArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, right: -38 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight className={`svgFillAll ${classes.iconArrow}`} />
    </div>
  );
}
const MytourMall = ({ paramsUrl }) => {
  const classes = useStyles();
  const [chains, setChains] = useState([]);
  const [idxActive, setActive] = useState(0);
  const settings = {
    dots: false,
    infinite: chains.length > 4,
    lazyLoad: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    nextArrow: <SampleNextArrow customStyleArrow={customStyle} />,
    prevArrow:
      idxActive > 0 ? (
        <SamplePrevArrow customStyleArrow={customStyle} />
      ) : (
        <></>
      ),
    beforeChange: (current, next) => setActive(next),
  };
  const fetchChains = async () => {
    try {
      const { data } = await getChainsHotels();
      if (data.code === 200 && !isEmpty(data.data)) setChains(data.data.items);
    } catch (error) {}
  };
  useEffect(() => {
    fetchChains(); // eslint-disable-next-line
  }, []);
  if (isEmpty(chains)) return null;
  return (
    <Box className={classes.container}>
      <Box className={classes.content}>
        <Box className={classes.headerText} style={{ marginBottom: 32 }}>
          <Box style={{ display: "flex", justifyContent: "center" }}>
            <Image
              srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_mytour_mall.svg"
              className={classes.myTourMallLogo}
            />
          </Box>
          <Typography variant="body2" className={classes.titleSubText}>
            {listString.IDS_MT_TOP_BRANDING_HOTEL}
          </Typography>
        </Box>
        <SlideShow
          settingProps={settings}
          className={classes.imageContent}
          slickDotStyle={classes.slickDot}
        >
          {chains.map((el) => {
            return (
              <Box key={el.id} className={classes.imageContainer}>
                <Link
                  href={{
                    pathname: routeStatic.LISTING_HOTEL.href,
                    query: {
                      slug: [
                        `${el.id}`,
                        `khach-san-tai-${
                          !isEmpty(el.name) ? el.name.stringSlug() : ""
                        }.html`,
                      ],
                      ...paramsUrl,
                      chain: el.id,
                      type: "chain",
                    },
                  }}
                  className={classes.linkBtn}
                >
                  <Image
                    srcImage={el.thumbnail}
                    className={classes.imageSlick}
                    alt={el.description}
                    title={el.title}
                    borderRadiusProp="8px"
                  />
                  <Box className={classes.imageLogo}>
                    <Box className={classes.borderTopImage} />
                    {!isEmpty(el.logo) && (
                      <Box className={classes.imgLogoContent}>
                        <img
                          src={el.logo}
                          alt=""
                          style={{ maxWidth: 79, maxHeight: 79 }}
                        />
                      </Box>
                    )}
                  </Box>
                </Link>
              </Box>
            );
          })}
        </SlideShow>

        <Box display="flex" justifyContent="center" pt={2}>
          <Link
            href={{ pathname: routeStatic.TOP_HOTEL_BRAND.href }}
            className={classes.linkBtn}
          >
            <ButtonComponent
              typeButton="outlined"
              backgroundColor="inherit"
              color="#FF1284"
              width="fit-content"
              borderColor="#FF1284"
              borderRadius={8}
              padding="12px 50px"
              fontWeight={600}
            >
              {listString.IDS_MT_TEXT_DISCOVER_NOW}
            </ButtonComponent>
          </Link>
        </Box>
      </Box>
    </Box>
  );
};

export default MytourMall;
