import { useState } from "react";
import { Box, Typography } from "@material-ui/core";
import { useRouter } from "next/router";
import { makeStyles } from "@material-ui/styles";
import { listString, routeStatic } from "@utils/constants";
import { cityList } from "@utils/dataFake";
import Link from "@src/link/Link";
import FlightListSeo from "@components/desktop/flight/home/components/FlightListSeo";

const useStyles = makeStyles((theme) => ({
  headerContainer: {
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    flexDirection: "column",
    margin: "0 auto",
  },
  headerContent: {
    display: "flex",
    justifyContent: "center",
    color: "#1A202C",
    margin: "56px 0 16px 0",
  },
  divider: {
    height: 3,
    background: "#00B6F3",
    width: 40,
    margin: "auto",
    marginTop: 12,
    marginBottom: 12,
  },
  textItem: { marginRight: 32, cursor: "pointer" },
  cityContentGroup: { display: "flex", flexWrap: "wrap" },
  cityText: {
    fontSize: 14,
    lineHeight: "32px",
    color: "#1A202C",
    width: "calc(100% / 6)",
  },
  listItem: {
    ...theme.typography.fontSize,
    lineHeight: "32px",
  },
}));

const CityList = ({ paramsUrl = {} }) => {
  const classes = useStyles();
  const router = useRouter();
  const [type, setType] = useState("hotel");
  const menu = [
    { name: listString.IDS_MT_HOT_HOTEL_TEXT, alias: "hotel" },
    { name: listString.IDS_MT_HOT_FLIGHT_TEXT, alias: "flight" },
  ];
  return (
    <Box
      style={{
        width: "100%",
        paddingBottom: 126,
        paddingTop: 20,
        position: "relative",
        zIndex: 1,
      }}
    >
      <Box className={classes.headerContainer}>
        <Box className={classes.headerContent}>
          {menu.map((item, i) => (
            <Box
              className={classes.textItem}
              key={i}
              onClick={() => setType(item.alias)}
            >
              <Typography
                variant="body2"
                style={{
                  fontWeight: 600,
                  fontSize: 16,
                  lineHeight: "19px",
                  color: type === item.alias ? "#00B6F3" : "#1A202C",
                }}
              >
                {item.name}
              </Typography>
              {type === item.alias && <div className={classes.divider} />}
            </Box>
          ))}
        </Box>
        <Box className={classes.cityContentGroup}>
          <>
            {type === "hotel" ? (
              cityList.map((el) => (
                <Box key={el.aliasCode} className={classes.cityText}>
                  <Link
                    href={{
                      pathname: routeStatic.TOP_PRICE_HOTEL.href,
                      query: {
                        slug: [
                          `${el.aliasCode}`,
                          `khach-san-tai-${el.name.stringSlug()}.html`,
                        ],
                        // ...paramsUrl,
                      },
                    }}
                    className="link-text"
                  >
                    <Typography variant="caption" className={classes.listItem}>
                      {el.name}
                    </Typography>
                  </Link>
                </Box>
              ))
            ) : (
              <FlightListSeo />
            )}
          </>
        </Box>
      </Box>
    </Box>
  );
};

export default CityList;
