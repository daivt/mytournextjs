import { Box } from "@material-ui/core";
import { useRouter } from "next/router";
import { makeStyles } from "@material-ui/styles";
import Link from "@src/link/Link";
import Image from "@src/image/Image";
import SlideShow from "@components/common/slideShow/SlideShow";
import { IconArrowRight } from "@public/icons";
import { isEmpty } from "@utils/helpers";
import { getBannerNews } from "@api/homes";
import { useState, useEffect } from "react";

const useStyles = makeStyles((theme) => ({
  container: { background: "white", width: "100%" },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    marginBottom: 64,
    marginTop: 12,
  },
  imageContent: {
    borderRadius: 8,
    marginTop: 24,
    display: "flex",
    alignItems: "center",
    "& > .slick-list": {
      width: "calc(100% + 16px)",
      margin: "0 -8px",
    },
  },
  imageSlick: { width: 380, height: 160, borderRadius: 8, margin: "0 auto" },
  linkBtn: {
    color: theme.palette.black.black3,
    textDecoration: "none !important",
  },
  slickDot: {
    bottom: "-16px !important",
    "& li": {
      height: 2,
      width: 16,
      margin: "0 2px",
      "& div": {
        height: 2,
        width: 16,
        borderRadius: 100,
        backgroundColor: theme.palette.gray.grayLight22,
        border: "none",
      },
    },
    "&.slick-active": {
      "& div": {
        backgroundColor: theme.palette.blue.blueLight8,
      },
    },
  },
  previewIcon: {
    transform: "rotate(180deg)",
  },
  iconArrow: {
    width: 16,
    height: 17,
    stroke: theme.palette.black.black3,
  },
  wrapArrow: {
    "&:hover": {
      "& svg": {
        stroke: theme.palette.blue.blueLight8,
        transition: "all ease .2s",
      },
    },
  },
}));

const customStyle = {
  background: "white",
  height: "40px",
  width: "40px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 2,
  borderRadius: "100px",
  boxShadow: "0px 8px 12px rgb(0 0 0 / 10%)",
  cursor: "pointer",
  position: "absolute",
  top: "50%",
  transform: "translate(-50%, -50%)",
};

function SamplePrevArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, left: 0 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight
        className={`svgFillAll ${classes.iconArrow}`}
        style={{ transform: "rotate(180deg)" }}
      />
    </div>
  );
}

function SampleNextArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, right: -38 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight className={`svgFillAll ${classes.iconArrow}`} />
    </div>
  );
}

const BannerSlide = () => {
  const classes = useStyles();
  const router = useRouter();
  const [listBanner, setListBanner] = useState([]);
  const [idxActive, setActive] = useState(0);

  const settings = {
    dots: true,
    infinite: listBanner.length > 3,
    lazyLoad: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    nextArrow: <SampleNextArrow customStyleArrow={customStyle} />,
    prevArrow:
      idxActive > 0 ? (
        <SamplePrevArrow customStyleArrow={customStyle} />
      ) : (
        <></>
      ),
    beforeChange: (current, next) => setActive(next),
  };

  const fetBanner = async () => {
    try {
      const { data } = await getBannerNews();
      if (data.code === 200) {
        setListBanner(data.data || []);
      }
    } catch (error) {}
  };
  useEffect(() => {
    fetBanner(); // eslint-disable-next-line
  }, []);

  if (isEmpty(listBanner)) return null;
  const renderItem = (el) => {
    return (
      <Image
        srcImage={el.thumb}
        className={classes.imageSlick}
        alt={el.description}
        title={el.title}
        borderRadiusProp="8px"
      />
    );
  };
  return (
    <Box className={classes.container}>
      <Box className={classes.content}>
        <SlideShow
          settingProps={settings}
          className={classes.imageContent}
          slickDotStyle={classes.slickDot}
        >
          {listBanner
            .sort((a, b) => a.priority - b.priority)
            .map((el) => {
              return (
                <Box key={el.id}>
                  {el.action === "link" ? (
                    <Link href={el.content} target="_blank">
                      {renderItem(el)}
                    </Link>
                  ) : (
                    <>{renderItem(el)}</>
                  )}
                </Box>
              );
            })}
        </SlideShow>
      </Box>
    </Box>
  );
};

export default BannerSlide;
