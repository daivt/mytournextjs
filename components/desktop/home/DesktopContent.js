import { getHotelsHotDeal, getTopHotels } from "@api/homes";
import { getHotelsAvailability } from "@api/hotels";
import InfoDescriptionHomeCard from "@components/common/desktop/card/InfoDescriptionHomeCard";
import Layout from "@components/layout/desktop/Layout";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import LazyLoad from "react-lazyload";
import { DELAY_TIMEOUT_POLLING } from "@utils/constants";
import {
  adapterHotelHotDealPc,
  isEmpty,
  paramsDefaultHotel,
} from "@utils/helpers";
import { useEffect, useState } from "react";
import HotelNotExitsModal from "@components/mobile/homes/HotelNotExitsModal";
import BannerSlide from "./components/BannerSlide";
import BubbleHotel from "./components/BubbleHotel";
import CityList from "./components/CityList";
import DownloadInfo from "./components/DownloadInfo";
import FavoriteAddress from "./components/FavoriteAddress";
import FavoriteHotel from "./components/FavoriteHotel";
import MytourMall from "./components/MytourMall";
import TopPriceHotel from "./components/TopPriceHotel";

const useStyles = makeStyles((theme) => ({
  homeWrapper: { width: "100%", background: "white" },
  homeContainer: { maxWidth: 1188, margin: "0 auto", padding: "24px 0" },
}));
const Home = ({ topLocation = [] }) => {
  const classes = useStyles();
  let breakPolling = true;
  const [hotelHotDeal, setHotelHotDeal] = useState([]);
  const [topHotels, setTopHotels] = useState([]);
  const [isServerLoaded, setServerLoaded] = useState(false);

  const mergePriceToData = (data = [], type, result = []) => {
    const dataResult = [...result];
    data.forEach((el) => {
      dataResult.forEach((item, index) => {
        if (item.id === el.id) {
          dataResult[index] = {
            ...item,
            basePromotionInfo: el.basePromotionInfo,
            price: el.basePrice,
            hiddenPrice: el.hiddenPrice,
            lastBookedTime: el.lastBookedTime,
          };
        }
      });
    });
    if (type === "hotelHotDeal") setHotelHotDeal(dataResult);
    else if (type === "topHotels") setTopHotels(dataResult);
  };
  const fetchPriceHotel = async (ids, hotDealData, topHotelData) => {
    let polling = true;
    try {
      const dataDto = {
        ...paramsDefaultHotel(),
        hotelIds: ids,
        searchType: "hotel",
      };
      while (polling && breakPolling) {
        const { data } = await getHotelsAvailability(dataDto);
        if (data.code === 200) {
          if (data.data.completed) {
            mergePriceToData(data.data.items, "hotelHotDeal", hotDealData);
            mergePriceToData(data.data.items, "topHotels", topHotelData);
            polling = false;
            break;
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      polling = false;
    }
  };
  const fetchData = async (param = {}) => {
    try {
      const params = { page: 1, size: 20, ...paramsDefaultHotel() };
      let temp = [];
      let resHotDeal = {};
      if (isEmpty(param)) {
        resHotDeal = await getHotelsHotDeal(params);
        if (!isEmpty(resHotDeal.data) && resHotDeal.data.code === 200) {
          temp = [...temp, ...resHotDeal.data.data.items];
          setHotelHotDeal(
            !isEmpty(resHotDeal.data.data) ? resHotDeal.data.data.items : []
          );
        }
      }
      const resTopHotel = await getTopHotels({
        ...params,
        province: topLocation[0].provinceId,
        ...param,
      });
      if (!isEmpty(resTopHotel.data) && resTopHotel.data.code === 200) {
        temp = [...temp, ...resTopHotel.data.data.items];
        setTopHotels(
          !isEmpty(resTopHotel.data.data) ? resTopHotel.data.data.items : []
        );
      }
      if (!isEmpty(temp)) {
        fetchPriceHotel(
          temp.map((el) => el.id),
          !isEmpty(resHotDeal.data) ? resHotDeal.data.data.items : [],
          !isEmpty(resTopHotel.data) ? resTopHotel.data.data.items : []
        );
      }
    } catch (error) {}
  };

  useEffect(() => {
    setServerLoaded(true);
    fetchData();
    return () => {
      breakPolling = false;
    }; // eslint-disable-next-line
  }, []);

  return (
    <Layout isHeader isFooter type="home" topLocation={topLocation}>
      <Box className={classes.homeWrapper}>
        <Box className={classes.homeContainer}>
          <InfoDescriptionHomeCard />
        </Box>
        <BannerSlide />
        <FavoriteHotel
          hotelHotDeal={adapterHotelHotDealPc(hotelHotDeal)}
          paramsUrl={paramsDefaultHotel()}
        />
        {isServerLoaded && <MytourMall paramsUrl={paramsDefaultHotel()} />}
        <TopPriceHotel
          topHotels={adapterHotelHotDealPc(topHotels)}
          topLocation={topLocation}
          paramsUrl={paramsDefaultHotel()}
          fetchData={fetchData}
        />
        <BubbleHotel topLocation={topLocation} />
        <FavoriteAddress />
        {isServerLoaded && (
          <LazyLoad overflow>
            <DownloadInfo />
          </LazyLoad>
        )}
        <CityList paramsUrl={paramsDefaultHotel()} />
      </Box>
      <HotelNotExitsModal />
    </Layout>
  );
};

export default Home;
