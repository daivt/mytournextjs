import {
  Box,
  Step,
  StepConnector,
  StepLabel,
  Stepper,
  Typography,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import Image from "@src/image/Image";
import clsx from "clsx";
import { prefixUrlIcon, listIcons } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
}));

const ColorLibConnector = withStyles((theme) => ({
  active: {
    "& $line": {
      borderColor: "#E5F8FE",
    },
  },
  completed: {
    "& $line": {
      borderColor: "#E5F8FE",
    },
  },
  line: {
    height: 72,
    width: 2,
    marginLeft: 12,
    marginTop: -8,
    marginRight: 12,
    marginBottom: -16,
    padding: 0,
    borderColor: "#E5F8FE",
  },
}))(StepConnector);

const useStepIconComponentStyles = makeStyles((theme) => ({
  root: {
    background: "#E5F8FE",
    width: 52,
    height: 52,
    borderRadius: "21px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 8,
  },
  active: {
    background: "#E5F8FE",
  },
  completed: {
    background: "#E5F8FE",
  },
  partner_step_1: {
    width: 28,
    height: 28,
  },
}));
const StepIconComponent = (props) => {
  const classes = useStepIconComponentStyles();
  const { active, completed, icon } = props;

  const stepNumbers = {
    1: <img src={`${prefixUrlIcon}${listIcons.IconPartnerStep1}`} />,
    2: <img src={`${prefixUrlIcon}${listIcons.IconPartnerStep2}`} />,
    3: <img src={`${prefixUrlIcon}${listIcons.IconPartnerStep3}`} />,
    4: <img src={`${prefixUrlIcon}${listIcons.IconPartnerStep4}`} />,
  };
  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {stepNumbers[icon]}
    </div>
  );
};

export default function PartnershipStep() {
  const classes = useStyles();
  return (
    <Stepper
      activeStep={3}
      orientation="vertical"
      connector={<ColorLibConnector />}
    >
      <Step>
        <StepLabel StepIconComponent={StepIconComponent}>
          <Box display="flex" flexDirection="column">
            <Typography
              variant="subtitle2"
              style={{ fontSize: 12, lineHeight: "14px", color: "#00B6F3" }}
            >
              Bước 1
            </Typography>
            <Typography variant="h6" style={{ margin: "6px 0 8px" }}>
              Đăng ký theo mẫu
            </Typography>
            <Typography
              variant="caption"
              style={{ fontSize: 12, lineHeight: "17px", color: "#4A5568" }}
            >
              Khách sạn, resort hay homestay đều có thể được đăng miễn phí!
            </Typography>
          </Box>
        </StepLabel>
      </Step>
      <Step>
        <StepLabel StepIconComponent={StepIconComponent}>
          <Box display="flex" flexDirection="column">
            <Typography
              variant="subtitle2"
              style={{ fontSize: 12, lineHeight: "14px", color: "#00B6F3" }}
            >
              Bước 2
            </Typography>
            <Typography variant="h6" style={{ margin: "6px 0 8px" }}>
              Ký hợp đồng điện tử
            </Typography>
            <Typography
              variant="caption"
              style={{ fontSize: 12, lineHeight: "17px", color: "#4A5568" }}
            >
              Tiết kiệm thời gian. Đơn giản chỉ với một nhấp chuột!
            </Typography>
          </Box>
        </StepLabel>
      </Step>
      <Step>
        <StepLabel StepIconComponent={StepIconComponent}>
          <Box display="flex" flexDirection="column">
            <Typography
              variant="subtitle2"
              style={{ fontSize: 12, lineHeight: "14px", color: "#00B6F3" }}
            >
              Bước 3
            </Typography>
            <Typography variant="h6" style={{ margin: "6px 0 8px" }}>
              Cập nhật hệ thống
            </Typography>
            <Typography
              variant="caption"
              style={{ fontSize: 12, lineHeight: "17px", color: "#4A5568" }}
            >
              Chuyên viên của Mytour sẽ liên hệ để giúp bạn hoàn thiện!
            </Typography>
          </Box>
        </StepLabel>
      </Step>
      <Step>
        <StepLabel StepIconComponent={StepIconComponent}>
          <Box display="flex" flexDirection="column">
            <Typography
              variant="subtitle2"
              style={{ fontSize: 12, lineHeight: "14px", color: "#00B6F3" }}
            >
              Bước 4
            </Typography>
            <Typography variant="h6" style={{ margin: "6px 0 8px" }}>
              Mở bán và đón khách
            </Typography>
            <Typography
              variant="caption"
              style={{ fontSize: 12, lineHeight: "17px", color: "#4A5568" }}
            >
              Tối đa doanh thu! Tối thiểu chi phí!
            </Typography>
          </Box>
        </StepLabel>
      </Step>
    </Stepper>
  );
}
