import SlideShow from "@components/common/slideShow/SlideShow";
import { Box, Typography, useTheme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconArrowRight } from "@public/icons";
import Image from "@src/image/Image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  container: { background: "white", width: "100%" },
  imageContent: {
    borderRadius: 8,
    display: "flex",
    alignItems: "center",
  },
  imageSlick: { width: 480, height: 300, borderRadius: 8 },
  slickDot: {
    display: "none",
  },
  previewIcon: {
    transform: "rotate(180deg)",
  },
  iconArrow: {
    width: 16,
    height: 17,
    stroke: theme.palette.black.black3,
  },
  wrapArrow: {
    "&:hover": {
      "& svg": {
        stroke: theme.palette.blue.blueLight8,
        transition: "all ease .2s",
      },
    },
  },
  slideItem: {
    position: "relative",
  },
}));

const customStyle = {
  background: "white",
  height: "48px",
  width: "48px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 2,
  borderRadius: "100px",
  boxShadow: "0px 8px 12px rgb(0 0 0 / 10%)",
  cursor: "pointer",
  position: "absolute",
  top: "50%",
  transform: "translate(-50%, -50%)",
};

function SamplePrevArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, left: 0 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight
        className={`svgFillAll ${classes.iconArrow}`}
        style={{ transform: "rotate(180deg)" }}
      />
    </div>
  );
}

function SampleNextArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, right: -48 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight className={`svgFillAll ${classes.iconArrow}`} />
    </div>
  );
}

const BannerPartnershipSlide = () => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const [idxActive, setActive] = useState(0);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    nextArrow: <SampleNextArrow customStyleArrow={customStyle} />,
    prevArrow:
      idxActive > 0 ? (
        <SamplePrevArrow customStyleArrow={customStyle} />
      ) : (
        <></>
      ),
    beforeChange: (current, next) => setActive(next),
  };
  const listBanner = [
    {
      id: 0,
      thumb:
        "https://storage.googleapis.com/tripi-assets/mytour/banner/img_partnership_slide_hotel.jpg",
      description: "",
      title: "Khách sạn",
    },
    {
      id: 1,
      thumb:
        "https://storage.googleapis.com/tripi-assets/mytour/banner/img_partnership_slide_homestay.jpg",
      description: "",
      title: "Homestay",
    },
    {
      id: 2,
      thumb:
        "https://storage.googleapis.com/tripi-assets/mytour/banner/img_partnership_slide_resort.jpg",
      description: "",
      title: "Resort",
    },
    {
      id: 3,
      thumb:
        "https://storage.googleapis.com/tripi-assets/mytour/banner/img_partnership_slide_apartment.jpg",
      description: "",
      title: "Căn hộ",
    },
  ];

  return (
    <Box className={classes.container}>
      <SlideShow
        settingProps={settings}
        className={classes.imageContent}
        slickDotStyle={classes.slickDot}
      >
        {listBanner.map((el, idx) => {
          return (
            <Box key={idx} className={classes.slideItem}>
              <Image
                srcImage={el.thumb}
                className={classes.imageSlick}
                alt={el.description}
                title={el.title}
                borderRadiusProp="8px"
              />
              <Typography
                variant="h4"
                style={{
                  color: theme.palette.white.main,
                  position: "absolute",
                  left: "40%",
                  bottom: "24px",
                }}
              >
                {el.title}
              </Typography>
            </Box>
          );
        })}
      </SlideShow>
    </Box>
  );
};

export default BannerPartnershipSlide;
