/* eslint-disable jsx-a11y/alt-text */
import Layout from "@components/layout/desktop/Layout";
import { Box, Grid, Typography, useTheme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import {
  IconCheck,
  IconPartnerAirBalloon,
  IconPartnerBus,
  IconPartnerFlight,
  IconPartnerHotel,
  IconPartnerReview,
  IconQuote,
} from "@public/icons";
import ButtonComponent from "@src/button/Button";
import { prefixUrlIcon, listIcons } from "@utils/constants";
import Image from "@src/image/Image";
import clsx from "clsx";
import { useRouter } from "next/router";
import BannerPartnershipSlide from "./BannerPartnershipSlide";
import PartnershipStep from "./PartnershipStep";

const useStyles = makeStyles((theme) => ({
  homeWrapper: {
    width: "100%",
    background: "white",
  },
  homeContainer: {
    maxWidth: 1188,
    margin: "40px auto 124px",
  },
  topBanner: {
    display: "flex",
    margin: "0 auto",
    maxWidth: 1602,
  },
  leftBanner: {
    height: 542,
    width: 676,
    backgroundImage: `url("https://storage.googleapis.com/tripi-assets/mytour/banner/img_partnership_left_banner.jpg")`,
    color: theme.palette.white.main,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  menuItem: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    height: 106,
    padding: "16px 0",
    "&.active": {
      background: theme.palette.blue.blueLight15,
      borderRadius: 16,
    },
  },
  regisHotel: {
    display: "flex",
    flexDirection: "column",
  },
  regisStep: {
    display: "flex",
    flexDirection: "column",
  },
  slideHotel: {},
  partner_sale: {
    width: 72,
    height: 72,
  },
}));
const Partnership = () => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const menuItem = [
    {
      id: 0,
      name: "Khách sạn",
      icon: <IconPartnerHotel />,
    },
    {
      id: 1,
      name: "Vé máy bay",
      icon: <IconPartnerFlight />,
    },
    {
      id: 2,
      name: "Trải nghiệm",
      icon: <IconPartnerAirBalloon />,
    },
    {
      id: 3,
      name: "Phương tiện",
      icon: <IconPartnerBus />,
    },
    {
      id: 4,
      name: "Khuyến mãi",
      icon: <img src={`${prefixUrlIcon}${listIcons.IconPartnerDiscount}`} />,
    },
    {
      id: 5,
      name: "Hợp tác",
      icon: <IconPartnerReview />,
    },
  ];

  const whyUsList = [
    {
      id: 0,
      title: "25.000.000+",
      desc: "Khách hàng để tiếp cận",
      thumb:
        "https://storage.googleapis.com/tripi-assets/mytour/images/img_partnership_customer_access.jpg",
    },
    {
      id: 1,
      title: "10.000+",
      desc: "Đối tác truyền thông",
      thumb:
        "https://storage.googleapis.com/tripi-assets/mytour/banner/img_partnership_media.jpg",
    },
    {
      id: 2,
      title: "20.000+",
      desc: "Nhà cung cấp sản phẩm về du lịch",
      thumb:
        "https://storage.googleapis.com/tripi-assets/mytour/banner/img_partnership_supplier.jpg",
    },
    {
      id: 3,
      title: "50.000+",
      desc: "Booking mỗi tháng",
      thumb:
        "https://storage.googleapis.com/tripi-assets/mytour/images/img_partnership_booking.jpg",
    },
  ];

  const bonusList = [
    {
      id: 0,
      title: "Đăng ký bất kỳ chỗ nghỉ nào",
      desc:
        "Từ căn hộ cho đến biệt thự và các loại chỗ nghỉ khác đều có thể được đăng miễn phí.",
      icon: <img src={`${prefixUrlIcon}${listIcons.IconPartnerRegister}`} />,
    },
    {
      id: 1,
      title: "Nhập thông tin dễ dàng",
      desc:
        "Để tiết kiệm thời gian, Quý vị có thể nhập nhiều thông tin từ các đăng ký có sẵn.",
      icon: (
        <img
          src={`${prefixUrlIcon}${listIcons.IconPartnerInfo}`}
          alt="icon_partner_info"
          style={{ width: 72, height: 72 }}
        />
      ),
    },
    {
      id: 2,
      title: "Hướng dẫn từng bước",
      desc:
        "Quý vị sẽ được biết cách thức hoạt động của trang chúng tôi, các phương pháp thực hành tốt nhất và những điều cần chú ý.",
      icon: <img src={`${prefixUrlIcon}${listIcons.IconPartnerGuide}`} />,
    },
    {
      id: 3,
      title: "Giảm giá đặc biệt",
      desc:
        "Được giảm giá cho các sản phẩm và dịch vụ giúp tiết kiệm thời gian cho Quý vị và cải thiện trải nghiệm cho khách.",
      icon: (
        <Image
          srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_partner_sale.svg"
          className={classes.partner_sale}
        />
      ),
    },
  ];
  const partnerReviews = [
    {
      id: 0,
      name: "Hoàng Dũng",
      title: "Giám đốc điều hành",
      desc:
        "Tôi đánh giá hệ thống của Mytour thân thiện với người dùng và đầy đủ chức năng so với mặt bằng chung. Rất hài lòng.",
      thumb:
        "https://storage.googleapis.com/tripi-assets/mytour/images/img_partnership_avatar_01.jpg",
    },
    {
      id: 1,
      name: "Như Lan",
      title: "Trưởng phòng Sales - OTA",
      desc:
        "Tôi rất hài lòng về sản phẩm dịch vụ của Mytour. Hy vọng chúng ta hợp tác thành công hơn nữa trong tương lai.",
      thumb:
        "https://storage.googleapis.com/tripi-assets/mytour/images/img_partnership_avatar_02.jpg",
    },
    {
      id: 2,
      name: "Thế Thịnh",
      title: "Giám đốc khách sạn",
      desc:
        "Lượng đặt phòng của Mytour hiện đáp ứng đủ mong đợi của tôi ở thời điểm hiện tại. Chúc Mytour phát triển vững mạnh hơn.",
      thumb:
        "https://storage.googleapis.com/tripi-assets/mytour/images/img_partnership_avatar_03.jpg",
    },
  ];
  const ButtonRegister = (
    <ButtonComponent
      backgroundColor={theme.palette.secondary.main}
      height={48}
      fontSize={16}
      fontWeight={600}
      lineheight="19px"
      borderRadius={8}
      width={148}
      handleClick={() => {
        router.push({
          pathname: "https://phms.mytour.vn/register",
        });
      }}
    >
      Đăng ký ngay
    </ButtonComponent>
  );

  return (
    <Layout
      isHeader
      isFooter
      type="account"
      title="Hợp tác với chúng tôi - Đăng ký để phát triển kinh doanh của bạn"
    >
      <Box className={classes.homeWrapper}>
        <Box className={classes.topBanner}>
          <Box className={classes.leftBanner}>
            <Box display="flex" flexDirection="column" maxWidth="320px">
              <Typography style={{ fontSize: 38, lineHeight: "52px" }}>
                Phát triển Business của bạn với Mytour
              </Typography>
              <Box display="flex" alignItems="center">
                <IconCheck
                  className="svgFillAll"
                  style={{
                    stroke: "white",
                    marginRight: 12,
                  }}
                />
                <Typography variant="caption" style={{ lineHeight: "36px" }}>
                  Đăng ký hoàn toàn miễn phí trong 15 phút.
                </Typography>
              </Box>
              <Box display="flex" alignItems="center">
                <IconCheck
                  className="svgFillAll"
                  style={{
                    stroke: "white",
                    marginRight: 12,
                  }}
                />
                <Typography variant="caption" style={{ lineHeight: "36px" }}>
                  Tiếp cận hàng triệu khách hàng tiềm năng.
                </Typography>
              </Box>
              <Box display="flex" alignItems="center">
                <IconCheck
                  className="svgFillAll"
                  style={{
                    stroke: "white",
                    marginRight: 12,
                  }}
                />
                <Typography variant="caption" style={{ lineHeight: "36px" }}>
                  Dễ dàng quản lý mọi nơi
                </Typography>
              </Box>
            </Box>
          </Box>
          <Box>
            <img
              height="542px"
              width="926px"
              src="https://storage.googleapis.com/tripi-assets/mytour/banner/img_partnership_right_banner.jpg"
            />
          </Box>
        </Box>
        <Box className={classes.homeContainer}>
          <Grid container style={{ marginBottom: 32 }}>
            {menuItem.map((item, idx) => (
              <Grid
                key={idx}
                item
                lg={2}
                md={2}
                sm={2}
                xs={2}
                className={clsx(classes.menuItem, item.id == 0 ? "active" : "")}
                key={item.id}
              >
                {item.icon}
                <Box mt={2}>
                  <Typography
                    variant="subtitle2"
                    style={{ lineHeight: "17px" }}
                  >
                    {item.name}
                  </Typography>
                </Box>
              </Grid>
            ))}
          </Grid>
          <Grid container style={{ marginBottom: 104 }}>
            <Grid
              item
              lg={6}
              md={6}
              sm={6}
              xs={6}
              className={classes.regisHotel}
            >
              <Typography
                variant="h5"
                style={{ lineHeight: "36px", width: "400px" }}
              >
                Tự tin đăng ký chỗ nghỉ của bạn trên Mytour!
              </Typography>
              <Typography
                variant="caption"
                style={{
                  width: "500px",
                  marginBottom: "30px",
                  color: "#4A5568",
                }}
              >
                Từ căn hộ cho đến biệt thự và các loại chỗ nghỉ khác đều có thể
                được đăng miễn phí. Chỗ nghỉ của bạn cũng được giảm giá cho các
                sản phẩm và dịch vụ giúp tiết kiệm thời gian cho Quý vị và cải
                thiện trải nghiệm cho khách.
              </Typography>
              {ButtonRegister}
            </Grid>
            <Grid
              item
              lg={6}
              md={6}
              sm={6}
              xs={6}
              className={classes.slideHotel}
            >
              <BannerPartnershipSlide />
            </Grid>
          </Grid>
          <Grid container style={{ marginBottom: 124 }}>
            <Grid
              item
              lg={12}
              md={12}
              sm={12}
              xs={12}
              className={classes.regisStep}
            >
              <Typography variant="h4" style={{ lineHeight: "29px" }}>
                Tăng doanh thu với 4 bước đơn giản
              </Typography>
              <Typography
                variant="caption"
                style={{
                  margin: "8px 0 32px 0",
                  color: "#4A5568",
                  lineHeight: "17px",
                }}
              >
                Lấp đầy phòng trống của bạn với khách hàng từ Mytour
              </Typography>
            </Grid>
            <Grid item lg={6} md={6} sm={6} xs={6}>
              <Box borderRadius="8px">
                <img src="https://storage.googleapis.com/tripi-assets/mytour/banner/img_partnership_step_banner.jpg" />
              </Box>
            </Grid>
            <Grid item lg={6} md={6} sm={6} xs={6}>
              <PartnershipStep />
            </Grid>
          </Grid>
          <Grid container style={{ marginBottom: 124 }}>
            <Grid
              item
              lg={12}
              md={12}
              sm={12}
              xs={12}
              style={{ marginBottom: 32 }}
            >
              <Typography variant="h4" style={{ lineHeight: "29px" }}>
                Tại sao nên hợp tác cùng chúng tôi
              </Typography>
            </Grid>
            <Box display="flex">
              {whyUsList.map((item, idx) => (
                <Box
                  key={idx}
                  style={{
                    width: 280,
                    height: 364,
                    color: theme.palette.white.main,
                    backgroundImage: `url(${item.thumb})`,
                    backgroundRepeat: "no-repeat",
                    transition: "background-image 1s ease-in-out",
                    display: "flex",
                    flexDirection: "column",
                    borderRadius: 8,
                    marginRight: 24,
                    justifyContent: "flex-end",
                    padding: 24,
                  }}
                >
                  <Typography
                    style={{
                      fontSize: "30px",
                      lineHeight: "36px",
                      fontWeight: 600,
                    }}
                  >
                    {item.title}
                  </Typography>
                  <Typography variant="caption" style={{ lineHeight: "17px" }}>
                    {item.desc}
                  </Typography>
                </Box>
              ))}
            </Box>
          </Grid>
          <Grid container style={{ marginBottom: 124 }}>
            <Grid
              item
              lg={12}
              md={12}
              sm={12}
              xs={12}
              style={{ marginBottom: 60 }}
            >
              <Typography variant="h4" style={{ lineHeight: "29px" }}>
                Lợi ích khi hợp tác cùng chúng tôi
              </Typography>
              <Typography
                variant="caption"
                style={{
                  margin: "8px 0 32px 0",
                  color: "#4A5568",
                  lineHeight: "17px",
                }}
              >
                Chất lượng và uy tín là ưu tiên hàng đầu
              </Typography>
            </Grid>
            <Box display="flex">
              {bonusList.map((item, idx) => (
                <Box
                  key={idx}
                  style={{
                    width: 264,
                    background: theme.palette.white.main,
                    marginRight: 40,
                    textAlign: "start",
                  }}
                >
                  {item.icon}
                  <Box display="flex" flexDirection="column">
                    <Typography
                      style={{
                        fontSize: "18px",
                        lineHeight: "21px",
                        fontWeight: 600,
                        margin: "36px 0 12px",
                      }}
                    >
                      {item.title}
                    </Typography>
                    <Typography
                      variant="caption"
                      style={{
                        color: "#4A5568",
                      }}
                    >
                      {item.desc}
                    </Typography>
                  </Box>
                </Box>
              ))}
            </Box>
          </Grid>
        </Box>
        <Box
          style={{
            padding: 56,
            width: 1300,
            height: 364,
            margin: "0 auto 124px",
            backgroundImage: `url("https://storage.googleapis.com/tripi-assets/mytour/banner/img_partnership_about_banner.jpg")`,
            backgroundRepeat: "no-repeat",
            transition: "background-image 1s ease-in-out",
            display: "flex",
            flexDirection: "column",
            borderRadius: 8,
            justifyContent: "flex-end",
          }}
        >
          <Typography
            style={{
              color: theme.palette.white.main,
              lineheight: "29px",
              fontSize: "24px",
              fontWeight: 600,
              marginBottom: 32,
            }}
          >
            Đối tác Mytour nói gì?
          </Typography>
          <Box display="flex">
            {partnerReviews.map((item, idx) => (
              <Box
                key={idx}
                style={{
                  width: 380,
                  height: 190,
                  padding: "24px 0px 24px 24px",
                  background: theme.palette.white.main,
                  marginRight: 24,
                  textAlign: "start",
                  display: "flex",
                  borderRadius: 8,
                }}
              >
                <Box
                  style={{
                    borderRadius: 100,
                    marginRight: 16,
                  }}
                >
                  <img src={item.thumb} />
                </Box>
                <Box
                  display="flex"
                  flexDirection="column"
                  justifyContent="space-between"
                >
                  <Typography
                    variant="caption"
                    style={{
                      color: "#4A5568",
                      paddingRight: 24,
                    }}
                  >
                    {item.desc}
                  </Typography>
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                    pr={1}
                  >
                    <Box display="flex" flexDirection="column">
                      <Typography
                        variant="subtitle2"
                        style={{
                          lineheight: "17px",
                        }}
                      >
                        {item.name}
                      </Typography>
                      <Typography
                        variant="caption"
                        style={{
                          lineheight: "17px",
                          color: "#718096",
                        }}
                      >
                        {item.title}
                      </Typography>
                    </Box>
                    <IconQuote />
                  </Box>
                </Box>
              </Box>
            ))}
          </Box>
        </Box>
        <Box
          style={{
            padding: "146px 56px",
            width: 1300,
            margin: "0 auto 116px",
            backgroundImage: `url("https://storage.googleapis.com/tripi-assets/mytour/banner/img_partnership_with_you.jpg")`,
            backgroundRepeat: "no-repeat",
            transition: "background-image 1s ease-in-out",
            borderRadius: 8,
          }}
        >
          <Typography
            style={{
              color: theme.palette.white.main,
              lineHeight: "64px",
              fontSize: "44px",
              fontWeight: 600,
              marginBottom: 32,
              width: 500,
            }}
          >
            Hãy để Mytour đồng hành cùng bạn!
          </Typography>
          {ButtonRegister}
        </Box>
      </Box>
    </Layout>
  );
};

export default Partnership;
