import Layout from "@components/layout/desktop/Layout";
import { Box, Typography, useTheme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconArrowSmLeft, IconArrowSmRight } from "@public/icons";
import Image from "@src/image/Image";
import Link from "@src/link/Link";
import {
  LINK_DOWLOAD_APP,
  listString,
  prefixUrlIcon,
  listIcons,
} from "@utils/constants";
import { useRouter } from "next/router";

const useStyles = makeStyles((theme) => ({
  homeWrapper: {
    width: "100%",
    background: "white",
  },
  bannerTop: {
    backgroundImage:
      "url(https://storage.googleapis.com/tripi-assets/mytour/icons/bg_install_app.jpg)",
    width: "100%",
    backgroundColor: theme.palette.white.main,
    position: "relative",
    backgroundRepeat: "no-repeat",
  },
  contentBanner: {
    // position: "absolute",
    // top: 0,
  },
  wrapperBanner: {
    maxWidth: 1188,
    margin: "0 auto",
    display: "flex",
  },
  rightBanner: {},
  logoPhone: {
    backgroundImage:
      "url(https://storage.googleapis.com/tripi-assets/mytour/icons/phone_install.png)",
    height: "100%",
    backgroundRepeat: "no-repeat",
    marginLeft: "28%",
    position: "relative",
  },
  hiddenUm: {
    position: "absolute",
    top: 0,
    right: 250,
  },
  rvMytour: {
    backgroundColor: theme.palette.white.main,
    borderRadius: 16,
    display: "flex",
    flexDirection: "column",
    border: `solid 1px ${theme.palette.gray.grayLight23}`,
    boxShadow: "0px 12px 12px rgba(0, 0, 0, 0.06)",
  },
  iconSun: {
    position: "absolute",
    top: 24,
    left: -55,
    width: 112,
    height: 85,
  },
  iconBag: {
    position: "absolute",
    bottom: 44,
    left: -40,
    width: 85,
    height: 115,
  },
  iconTicket: {
    position: "absolute",
    top: "42%",
    left: 366,
    width: 104,
    height: 78,
  },
  topBar: {
    position: "absolute",
    top: 92,
    left: 78,
    display: "flex",
  },
  listBar: {
    boxShadow:
      "0px 0px 6.0349px rgba(0, 0, 0, 0.08), 0px 6.0349px 6.0349px rgba(0, 0, 0, 0.05)",
    backgroundColor: theme.palette.white.main,
    borderRadius: 6,
    padding: 6,
    display: "flex",
    alignItems: "center",
  },
  itemTutorial: {
    width: "33%",
    justifyContent: "center",
    display: "flex",
    alignItems: "center",
  },
  tutorial: {
    width: "68%",
    position: "relative",
    backgroundRepeat: "no-repeat",
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    minHeight: 260,
    backgroundPositionX: 40,
  },
  subText: {
    paddingTop: 12,
    color: theme.palette.gray.grayDark8,
  },
  hotelBar: {
    width: 35,
    height: 35,
  },
}));

const InstallApp = () => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();

  return (
    <Layout
      isHeader
      isFooter
      title="Mytour - Ứng dụng đặt phòng khách sạn, vé máy bay giá rẻ"
      type="account"
    >
      <Box className={classes.bannerTop}>
        <img
          src={`${prefixUrlIcon}${listIcons.IconHiddenInstall}`}
          alt="five_star"
          style={{ width: 870, height: 343 }}
          className={classes.hiddenUm}
        />
        <Box className={classes.wrapperBanner} pt={4}>
          <Box width="42%" pt={6}>
            <Box color={theme.palette.white.main}>
              <Typography variant="h3">
                {listString.IDS_MT_TEXT_DOWNLOAD_APP}&nbsp;
                {listString.IDS_MT_TEXT_MYTOUR}
              </Typography>
            </Box>
            <Box color={theme.palette.white.main} pt={12 / 8}>
              <Typography variant="body1" style={{ lineHeight: "28px" }}>
                {listString.IDS_MT_TEXT_MYTOUR_INTRO}
              </Typography>
            </Box>
            <Box mt={4} display="flex">
              <Box
                bgcolor={theme.palette.white.main}
                p={4 / 8}
                borderRadius={8}
              >
                <img src="/images/qr_code_mytour.png" alt="qr_code_mytour" />
              </Box>
              <Box ml={3}>
                <Link href={LINK_DOWLOAD_APP.ios}>
                  <Box mt={4 / 8}>
                    <img src="/images/btn_appstore.png" alt="btn_appstore" />
                  </Box>
                </Link>

                <Link href={LINK_DOWLOAD_APP.android}>
                  <Box mt={12 / 8}>
                    <img
                      src={`${prefixUrlIcon}${listIcons.IconFiveStar}`}
                      alt="five_star"
                      style={{ width: 100, height: 20 }}
                    />
                  </Box>
                </Link>
              </Box>
            </Box>
            <Box className={classes.rvMytour} mt={9} mb={4}>
              <Box display="flex" justifyContent="space-between">
                <Box ml={3} mt={2}>
                  <img
                    src={`${prefixUrlIcon}${listIcons.IconFiveStar}`}
                    alt="five_star"
                    style={{ width: 100, height: 20 }}
                  />
                </Box>
                <Box display="flex" mt={12 / 8} mr={2}>
                  <IconArrowSmLeft style={{ marginRight: 8 }} />
                  <IconArrowSmRight />
                </Box>
              </Box>
              <Box px={3} pt={1}>
                <Typography variant="caption">
                  {listString.IDS_MT_TEXT_MYTOUR_INTRO_REVIEW}
                </Typography>
              </Box>
              <Box
                padding="8px 0 16px 24px"
                color={theme.palette.gray.grayDark8}
              >
                <Typography variant="caption">27/3/2021</Typography>
              </Box>
            </Box>
          </Box>
          <Box className={classes.rightBanner} width="58%">
            <Box className={classes.logoPhone}>
              <Box className={classes.topBar}>
                <Box className={classes.listBar} mr={1}>
                  <Box>
                    <Image
                      srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_hotel_bar.svg"
                      className={classes.hotelBar}
                    />
                  </Box>
                  <Typography
                    variant="body2"
                    style={{ marginLeft: 6, marginRight: 8, fontWeight: 600 }}
                  >
                    Khách sạn
                  </Typography>
                </Box>
                <Box className={classes.listBar}>
                  <Box>
                    <img
                      src={`${prefixUrlIcon}${listIcons.IconFlightBar}`}
                      alt="icon_flight_bar"
                    />
                  </Box>
                  <Typography
                    variant="body2"
                    style={{ marginLeft: 6, marginRight: 8, fontWeight: 600 }}
                  >
                    Chuyến bay
                  </Typography>
                </Box>
              </Box>
              <Image
                srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_install_ticket.svg"
                className={classes.iconTicket}
              />
              <Image
                srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_install_sun.svg"
                className={classes.iconSun}
              />
              <Image
                srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_install_bag.svg"
                className={classes.iconBag}
              />
            </Box>
          </Box>
        </Box>
        <Box
          className={classes.wrapperBanner}
          flexWrap="wrap"
          justifyContent="center"
          pt={4}
          pb={5}
        >
          <Box className={classes.itemTutorial}>
            <Box
              className={classes.tutorial}
              style={{
                backgroundImage: `url("/images/bg_install_tutorial.png")`,
              }}
            >
              <Box mt={4} mb={3}>
                <img
                  src="/images/install_tutorial.png"
                  alt="install_tutorial"
                />
              </Box>
              <Typography variant="body1">
                Giá tốt hơn so với đặt phòng trực tiếp tại khách sạn
              </Typography>
              <Typography variant="body2" className={classes.subText}>
                Giá trên Mytour luôn thấp hơn giá niêm yết của khách sạn.
              </Typography>
            </Box>
          </Box>
          <Box className={classes.itemTutorial}>
            <Box
              className={classes.tutorial}
              style={{
                backgroundImage: `url("/images/bg_install_cs.png")`,
              }}
            >
              <Box mt={4} mb={3}>
                <img src="/images/install_cs.png" alt="install_cs" />
              </Box>
              <Typography variant="body1">
                Nhân viên chăm sóc, tư vấn nhiều kinh nghiệm
              </Typography>
              <Typography variant="body2" className={classes.subText}>
                Luôn sẵn sàng hỗ trợ bạn khi cần
              </Typography>
            </Box>
          </Box>
          <Box className={classes.itemTutorial}>
            <Box
              className={classes.tutorial}
              style={{
                backgroundImage: `url("/images/bg_install_bed.png")`,
              }}
            >
              <Box mt={4} mb={3}>
                <img src="/images/install_bed.png" alt="install_bed" />
              </Box>
              <Typography variant="body1">
                Hơn 5000 khách sạn tại Việt Nam với đánh giá thực
              </Typography>
              <Typography variant="body2" className={classes.subText}>
                Hàng nghìn khách sạn cho phép bạn thoải mái lựa chọn
              </Typography>
            </Box>
          </Box>
          <Box className={classes.itemTutorial}>
            <Box
              className={classes.tutorial}
              style={{
                backgroundImage: `url("/images/bg_install_sales.png")`,
              }}
            >
              <Box mt={4} mb={3}>
                <img src="/images/install_sales.png" alt="install_sales" />
              </Box>
              <Typography variant="body1">
                Nhiều chương trình khuyến mãi và tích luỹ điểm
              </Typography>
              <Typography variant="body2" className={classes.subText}>
                Nhiều chương trình và ưu đãi hấp dẫn khác dành riêng cho thành
                viên
              </Typography>
            </Box>
          </Box>
          <Box className={classes.itemTutorial}>
            <Box
              className={classes.tutorial}
              style={{
                backgroundImage: `url("/images/bg_install_card.png")`,
              }}
            >
              <Box mt={4} mb={3}>
                <img src="/images/install_card.png" alt="install_card" />
              </Box>
              <Typography variant="body1">
                Thanh toán dễ dàng, đa dạng
              </Typography>
              <Typography variant="body2" className={classes.subText}>
                Đặt phòng nhanh chóng, chỉ cần 2 thao tác đơn giản với nhiều
                phương thức thanh toán.
              </Typography>
            </Box>
          </Box>
        </Box>
      </Box>
    </Layout>
  );
};

export default InstallApp;
