import { useState, useEffect } from "react";
import { Box, Typography } from "@material-ui/core";
import { getChainsHotels } from "@api/homes";
import { makeStyles } from "@material-ui/styles";
import Image from "@src/image/Image";
import { IconArrowRight, IconMyTourMall } from "@public/icons";
import { isEmpty } from "@utils/helpers";
import Link from "@src/link/Link";
import { listString, routeStatic } from "@utils/constants";
import SlideShow from "@components/common/slideShow/SlideShow";
import ButtonComponent from "@src/button/Button";

const useStyles = makeStyles((theme) => ({
  container: {
    background: "#FBF3F7",
    width: "calc(100% - 48px)",
    borderRadius: 12,
    margin: "0 24px",
    padding: "56px 0",
    marginBottom: 24,
  },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "column",
  },
  hotelGroup: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between",
  },
  headerText: { textAlign: "center" },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
  },
  titleSubText: {
    fontSize: 14,
    lineHeight: "17px",
    color: "#4A5568",
    marginTop: 8,
  },
  imageSlick: { width: 279, height: 279, borderRadius: 8 },
  imageContainer: {
    position: "relative",
    width: 279,
    height: 279,
    borderRadius: 8,
    display: "flex",
    alignItems: "center",
  },
  imageLogo: {
    position: "absolute",
    top: 0,
    left: "50%",
    transform: "translateX(-50%)",
    borderTop: "solid 3px #F1B21A",
    background: "white",
    width: 99,
    height: 99,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  imgLogoContent: {
    width: "100%",
  },
  iconArrow: {
    width: 16,
    height: 17,
    stroke: theme.palette.black.black3,
  },
  wrapArrow: {
    "&:hover": {
      "& svg": {
        stroke: theme.palette.blue.blueLight8,
        transition: "all ease .2s",
      },
    },
  },
  myTourMallLogo: {
    width: 163,
    height: 46,
  },
}));
const customStyle = {
  background: "white",
  height: "40px",
  width: "40px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 2,
  borderRadius: "100px",
  boxShadow: "0px 8px 12px rgb(0 0 0 / 10%)",
  cursor: "pointer",
};

function SamplePrevArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{
        ...customStyleArrow,
        position: "absolute",
        top: "50%",
        left: 0,
        transform: "translate(-50%, -50%)",
      }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight
        className={`svgFillAll ${classes.iconArrow}`}
        style={{
          transform: "rotate(180deg)",
        }}
      />
    </div>
  );
}

function SampleNextArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{
        ...customStyleArrow,
        position: "absolute",
        right: "-23px",
        top: "50%",
        transform: "translate(-50%, -50%)",
      }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight className={`svgFillAll ${classes.iconArrow}`} />
    </div>
  );
}
const MytourMall = () => {
  const classes = useStyles();
  const [chains, setChains] = useState([]);
  const settings = {
    dots: false,
    infinite: chains.length > 4,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow customStyleArrow={customStyle} />,
    prevArrow: <SamplePrevArrow customStyleArrow={customStyle} />,
  };
  const fetchChains = async () => {
    try {
      const { data } = await getChainsHotels({ size: 100 });
      if (data.code === 200 && !isEmpty(data.data)) setChains(data.data.items);
    } catch (error) {}
  };

  useEffect(() => {
    fetchChains(); // eslint-disable-next-line
  }, []);
  if (isEmpty(chains)) return null;
  return (
    <Box className={classes.container}>
      <Box className={classes.content}>
        <Box className={classes.headerText} style={{ marginBottom: 32 }}>
          <Box style={{ display: "flex", justifyContent: "center" }}>
            <Image
              srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_mytour_mall.svg"
              className={classes.myTourMallLogo}
            />
          </Box>
          <Typography variant="body2" className={classes.titleSubText}>
            {listString.IDS_MT_TOP_BRANDING_HOTEL}
          </Typography>
        </Box>
        <SlideShow
          settingProps={settings}
          className={classes.imageContent}
          slickDotStyle={classes.slickDot}
        >
          {chains.map((el) => {
            return (
              <Box key={el.id} className={classes.imageContainer}>
                <Image
                  srcImage={el.thumbnail}
                  className={classes.imageSlick}
                  alt={el.description}
                  title={el.title}
                  borderRadiusProp="8px"
                />
                <Box className={classes.imageLogo}>
                  {!isEmpty(el.logo) && (
                    <img
                      src={el.logo}
                      className={classes.imgLogoContent}
                      alt=""
                    />
                  )}
                </Box>
              </Box>
            );
          })}
        </SlideShow>

        <Box display="flex" justifyContent="center" pt={2}>
          <Link
            href={{ pathname: routeStatic.TOP_HOTEL_BRAND.href }}
            className={classes.linkBtn}
          >
            <ButtonComponent
              typeButton="outlined"
              backgroundColor="inherit"
              color="#FF1284"
              width="fit-content"
              borderColor="#FF1284"
              borderRadius={8}
              padding="12px 50px"
            >
              {listString.IDS_TEXT_VIEW_ALL}
            </ButtonComponent>
          </Link>
        </Box>
      </Box>
    </Box>
  );
};

export default MytourMall;
