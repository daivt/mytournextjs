import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import HotelItemVertical from "@components/common/desktop/itemView/HotelItemVertical";
import { isEmpty } from "@utils/helpers";
import SlideShow from "@components/common/slideShow/SlideShow";
import { IconArrowRight } from "@public/icons";
import { listString } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  container: {
    background: "white",
    width: "calc(100% - 48px)",
    borderRadius: 12,
    marginBottom: 64,
  },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "column",
  },
  hotelGroup: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between",
  },
  hotelText: { display: "flex", flexDirection: "column" },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
  },
  imageContent: {
    borderRadius: 8,
    marginTop: 24,
    display: "flex",
    alignItems: "center",
  },
  titleSubText: {
    fontSize: 14,
    lineHeight: "17px",
    color: "#4A5568",
    marginTop: 8,
  },
  iconArrow: {
    width: 16,
    height: 17,
    stroke: theme.palette.black.black3,
  },
  wrapArrow: {
    "&:hover": {
      "& svg": {
        stroke: theme.palette.blue.blueLight8,
        transition: "all ease .2s",
      },
    },
  },
}));

const customStyle = {
  background: "white",
  height: "40px",
  width: "40px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 2,
  borderRadius: "100px",
  boxShadow: "0px 8px 12px rgb(0 0 0 / 10%)",
  cursor: "pointer",
};

function SamplePrevArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{
        ...customStyleArrow,
        position: "absolute",
        top: "50%",
        left: 0,
        transform: "translate(-50%, -50%)",
      }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight
        className={`svgFillAll ${classes.iconArrow}`}
        style={{
          transform: "rotate(180deg)",
        }}
      />
    </div>
  );
}

function SampleNextArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{
        ...customStyleArrow,
        position: "absolute",
        right: "-23px",
        top: "50%",
        transform: "translate(-50%, -50%)",
      }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight className={`svgFillAll ${classes.iconArrow}`} />
    </div>
  );
}
const FavoriteHotel = ({ hotelHotDeal = [], paramsUrl = {} }) => {
  const classes = useStyles();
  if (isEmpty(hotelHotDeal)) return null;
  const settings = {
    dots: false,
    infinite: hotelHotDeal.length > 4,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow customStyleArrow={customStyle} />,
    prevArrow: <SamplePrevArrow customStyleArrow={customStyle} />,
  };
  return (
    <Box className={classes.container}>
      <Box className={classes.content}>
        <Box className={classes.hotelText} style={{ marginBottom: 32 }}>
          <Typography variant="body2" className={classes.titleText}>
            {listString.IDS_MT_FAVORITE_HOTEL}
          </Typography>
          <Typography variant="body2" className={classes.titleSubText}>
            {listString.IDS_MT_FAVORITE_HOTEL_DESCRIPTION}
          </Typography>
        </Box>
        {!isEmpty(hotelHotDeal) && (
          <SlideShow
            settingProps={settings}
            className={classes.imageContent}
            slickDotStyle={classes.slickDot}
          >
            {hotelHotDeal.map((el) => {
              return (
                <Box style={{ marginBottom: 24 }} key={el.id}>
                  <HotelItemVertical
                    item={el}
                    isTopSale
                    isHotelInterest
                    // isShowStatus
                    isCountBooking={false}
                    paramsFilterInit={paramsUrl}
                  />
                </Box>
              );
            })}
          </SlideShow>
        )}
      </Box>
    </Box>
  );
};

export default FavoriteHotel;
