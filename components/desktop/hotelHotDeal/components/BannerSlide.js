import { Box } from "@material-ui/core";
import { useRouter } from "next/router";
import { makeStyles } from "@material-ui/styles";
import Link from "@src/link/Link";
import Image from "@src/image/Image";
import SlideShow from "@components/common/slideShow/SlideShow";
import { IconArrowRight } from "@public/icons";
import { isEmpty } from "@utils/helpers";
import { getBannerNews } from "@api/homes";
import { useState, useEffect } from "react";

const useStyles = makeStyles((theme) => ({
  container: { background: "white", width: "100%" },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    marginBottom: 64,
    marginTop: 12,
  },
  imageContent: {
    borderRadius: 8,
    marginTop: 24,
    display: "flex",
    alignItems: "center",
  },
  imageSlick: { width: 380, height: 160, borderRadius: 8 },
  linkBtn: {
    color: theme.palette.black.black3,
    textDecoration: "none !important",
  },
  slickDot: {
    bottom: "-16px !important",
    "& li": {
      height: 2,
      width: 16,
      margin: "0 2px",
      "& div": {
        height: 2,
        width: 16,
        borderRadius: 100,
        backgroundColor: theme.palette.gray.grayLight22,
        border: "none",
      },
    },
    "&.slick-active": {
      "& div": {
        backgroundColor: theme.palette.blue.blueLight8,
      },
    },
  },
  previewIcon: {
    transform: "rotate(180deg)",
  },
  iconArrow: {
    width: 16,
    height: 17,
    stroke: theme.palette.black.black3,
  },
  wrapArrow: {
    "&:hover": {
      "& svg": {
        stroke: theme.palette.blue.blueLight8,
        transition: "all ease .2s",
      },
    },
  },
}));

const customStyle = {
  background: "white",
  height: "40px",
  width: "40px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 2,
  borderRadius: "100px",
  boxShadow: "0px 8px 12px rgb(0 0 0 / 10%)",
  cursor: "pointer",
};

function SamplePrevArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{
        ...customStyleArrow,
        position: "absolute",
        top: "50%",
        left: 0,
        transform: "translate(-50%, -50%)",
      }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight
        className={`svgFillAll ${classes.iconArrow}`}
        style={{
          transform: "rotate(180deg)",
        }}
      />
    </div>
  );
}

function SampleNextArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{
        ...customStyleArrow,
        position: "absolute",
        right: "-23px",
        top: "50%",
        transform: "translate(-50%, -50%)",
      }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight className={`svgFillAll ${classes.iconArrow}`} />
    </div>
  );
}

const BannerSlide = () => {
  const classes = useStyles();
  const router = useRouter();
  const [listBanner, setListBanner] = useState([]);

  const settings = {
    dots: true,
    infinite: listBanner.length > 3,
    lazyLoad: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow customStyleArrow={customStyle} />,
    prevArrow: <SamplePrevArrow customStyleArrow={customStyle} />,
  };

  const fetBanner = async () => {
    try {
      const { data } = await getBannerNews();
      if (data.code === 200) {
        setListBanner(data.data || []);
      }
    } catch (error) {}
  };
  useEffect(() => {
    fetBanner();
  }, []);
  if (isEmpty(listBanner)) return null;
  return (
    <Box className={classes.container}>
      <Box className={classes.content}>
        <SlideShow
          settingProps={settings}
          className={classes.imageContent}
          slickDotStyle={classes.slickDot}
        >
          {listBanner
            .sort((a, b) => a.priority - b.priority)
            .map((el) => {
              if (el.action === "link") {
                return (
                  <Box key={el.id}>
                    <Link href={el.content} target="_blank">
                      <Image
                        srcImage={el.thumb}
                        className={classes.imageSlick}
                        alt={el.description}
                        title={el.title}
                        borderRadiusProp="8px"
                      />
                    </Link>
                  </Box>
                );
              }
              return (
                <Box key={el.id}>
                  <Image
                    srcImage={el.thumb}
                    className={classes.imageSlick}
                    alt={el.description}
                    title={el.title}
                    borderRadiusProp="8px"
                  />
                </Box>
              );
            })}
        </SlideShow>
      </Box>
    </Box>
  );
};

export default BannerSlide;
