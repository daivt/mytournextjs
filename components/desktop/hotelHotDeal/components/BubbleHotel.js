import { Box, Typography } from "@material-ui/core";
import Link from "@src/link/Link";
import clsx from "clsx";
import { makeStyles } from "@material-ui/styles";
import Image from "@src/image/Image";
import { listString, routeStatic } from "@utils/constants";
import { isEmpty, paramsDefaultHotel } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  container: { width: "100%", background: "white" },
  content: { maxWidth: 1188, margin: "0 auto", padding: "24px 0" },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
    marginBottom: 8,
  },
  titleDescription: {
    fontWeight: 400,
    fontSize: 14,
    lineHeight: "17px",
    color: "#4A5568",
  },
  bubbleContainer: { position: "relative", width: "100%", height: 600 },
  imgCommon: {
    position: "absolute",
    borderRadius: "50%",
    textAlign: "center",
    "&:hover": {
      opacity: 0.8,
    },
  },
  img1: { width: 266, height: 266, left: 0, top: 167 },
  img2: { width: 163, height: 163, left: 258, top: 23 },
  img3: { width: 163, height: 163, left: 320, top: 198 },
  img4: { width: 133, height: 133, left: 253, top: 408 },
  img5: { width: 266, height: 266, left: 473, top: 12 },
  img6: { width: 163, height: 163, left: 638, top: 310 },
  img7: { width: 200, height: 200, left: 421, top: 361 },
  img8: { width: 163, height: 163, left: 791, top: -20 },
  img9: { width: 133, height: 133, left: 769, top: 192 },
  img10: { width: 163, height: 163, left: 968, top: 54 },
  img11: { width: 266, height: 266, left: 892, top: 259 },
  img12: { width: 133, height: 133, left: 776, top: 440 },
  textItem: {
    fontWeight: 600,
    fontSize: 16,
    lineHeight: "19px",
    marginTop: -48,
    position: "absolute",
    color: "white",
    width: "100%",
    padding: "0 12px",
  },
}));
const BubbleHotel = ({ topLocation = [] }) => {
  const classes = useStyles();
  const getLinkInfo = (item) => {
    return {
      pathname: routeStatic.LISTING_HOTEL.href,
      query: {
        slug: [
          `${item.aliasCode}`,
          `khach-san-tai-${item.name.stringSlug()}.html`,
        ],
        ...paramsDefaultHotel(),
      },
    };
  };
  return (
    <Box className={classes.container}>
      <Box className={classes.content}>
        <Typography variant="body2" className={classes.titleText}>
          {listString.IDS_TEXT_DESTINATION_FAVORITE}
        </Typography>
        <Typography variant="body2" className={classes.titleDescription}>
          {listString.IDS_TEXT_DESTINATION_HOT_MYTOUR_RECOMMENDED}
        </Typography>
        <Box className={classes.bubbleContainer}>
          {!isEmpty(topLocation) &&
            topLocation.map((el, i) => {
              if (i >= 11) return null;
              let size = 266;
              if (i === 1 || i === 2 || i === 5 || i === 7 || i === 9)
                size = 163;
              else if (i === 3 || i === 8 || i === 11) size = 133;
              else if (i === 6) size = 200;
              return (
                <Box
                  className={clsx(classes.imgCommon, classes[`img${i + 1}`])}
                  key={el.aliasCode}
                >
                  <Link href={{ ...getLinkInfo(el) }}>
                    <Image
                      srcImage={el.thumb}
                      alt=""
                      style={{ width: size, height: size, borderRadius: "50%" }}
                      borderRadiusProp="50%"
                    />
                    <Typography variant="body2" className={classes.textItem}>
                      {el.name}
                    </Typography>
                  </Link>
                </Box>
              );
            })}

          {topLocation.length === 12 ? (
            <Box className={clsx(classes.imgCommon, classes.img11)}>
              <Link href={{ ...getLinkInfo(topLocation[11]) }}>
                <Image
                  srcImage={topLocation[11].thumb}
                  alt=""
                  style={{ width: 133, height: 133, borderRadius: "50%" }}
                  borderRadiusProp="50%"
                />
              </Link>
            </Box>
          ) : (
            <Box className={clsx(classes.imgCommon, classes.img12)}>
              <Box
                style={{
                  width: 133,
                  height: 133,
                  borderRadius: "50%",
                  background: "rgba(0, 182, 243, 0.1)",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  flexDirection: "column",
                  cursor: "pointer",
                }}
              >
                <p
                  style={{
                    margin: 0,
                    marginBottom: 2,
                    color: "#00B6F3",
                    fontWeight: 600,
                    fontSize: 18,
                    lineHeight: "21px",
                  }}
                >
                  +{topLocation.length - 11}
                </p>
                <p
                  style={{
                    margin: 0,
                    color: "#00B6F3",
                    fontSize: 14,
                    lineHeight: "17px",
                  }}
                >
                  Xem tất cả
                </p>
              </Box>
            </Box>
          )}
        </Box>
      </Box>
    </Box>
  );
};

export default BubbleHotel;
