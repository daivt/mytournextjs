import { useEffect, useState } from "react";
import { Box, Typography } from "@material-ui/core";
import { useRouter } from "next/router";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import HotelItemVertical from "@components/common/desktop/itemView/HotelItemVertical";
import Link from "@src/link/Link";
import { listString, routeStatic } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import clsx from "clsx";
import ButtonComponent from "@src/button/Button";

const useStyles = makeStyles((theme) => ({
  container: {
    background: "#EDF6F9",
    width: "calc(100% - 48px)",
    borderRadius: 12,
    margin: "0 24px",
    padding: "56px 0",
  },
  content: {
    maxWidth: 1188,
    width: "calc(100% + 24px)",
    margin: "0 auto",
    display: "flex",
    // justifyContent: "space-between",
    flexDirection: "column",
  },
  hotelGroup: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between",
    alignItems: "center",
  },
  galleryHotel: { display: "flex", flexWrap: "wrap", margin: "0 -12px" },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
  },
  buttonGroup: {
    display: "flex",
  },
  buttonBtn: {
    marginLeft: 12,
    color: "#1A202C",
    background: "white",
    borderColor: "#A0AEC0",
    "&:hover": {
      color: "white",
      background: "#00B6F3",
      borderColor: "#00B6F3",
    },
  },
  activeBtn: {
    color: "white",
    background: "#00B6F3",
    borderColor: "#00B6F3",
  },
}));

const TopPriceHotel = ({
  topHotels = [],
  paramsUrl = {},
  topLocation = [],
  fetchData,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const [activeId, setActive] = useState(null);
  useEffect(() => {
    if (fetchData) fetchData({ province: activeId }); // eslint-disable-next-line
  }, [activeId]);
  if (isEmpty(activeId) && isEmpty(topHotels)) return null;
  return (
    <Box className={classes.container}>
      <Box className={classes.content}>
        <Box className={classes.hotelGroup} style={{ marginBottom: 32 }}>
          <Typography variant="body2" className={classes.titleText}>
            {listString.IDS_MT_TOP_PRICE_HOTEL}
          </Typography>
          <Box className={classes.buttonGroup}>
            {!isEmpty(topLocation) &&
              topLocation.map((el, i) => {
                if (i > 6) return null;
                return (
                  <ButtonComponent
                    key={el.provinceId}
                    typeButton="outlined"
                    backgroundColor="inherit"
                    color="inherit"
                    width="fit-content"
                    borderColor={theme.palette.black.black5}
                    borderRadius={8}
                    padding="10px 12px"
                    className={
                      el.provinceId === activeId
                        ? clsx(classes.buttonBtn, classes.activeBtn)
                        : classes.buttonBtn
                    }
                    handleClick={() => setActive(el.provinceId)}
                  >
                    {el.name}
                  </ButtonComponent>
                );
              })}
          </Box>
        </Box>
        <Box className={classes.galleryHotel}>
          {!isEmpty(topHotels) &&
            topHotels.map((el) => (
              <Box style={{ margin: "0 12px", marginBottom: 24 }} key={el.id}>
                <HotelItemVertical
                  item={el}
                  isTopSale
                  isHotelInterest
                  isCountBooking
                  isShowStatus
                  paramsFilterInit={paramsUrl}
                />
              </Box>
            ))}
        </Box>
        <Box display="flex" justifyContent="center" pt={2}>
          <Link
            href={{
              pathname: routeStatic.TOP_HOTEL.href,
            }}
            className={classes.linkBtn}
          >
            <ButtonComponent
              typeButton="outlined"
              backgroundColor="inherit"
              color="inherit"
              width="fit-content"
              borderColor={theme.palette.black.black5}
              borderRadius={8}
              padding="12px 50px"
            >
              {listString.IDS_TEXT_VIEW_ALL}
            </ButtonComponent>
          </Link>
        </Box>
      </Box>
    </Box>
  );
};

export default TopPriceHotel;
