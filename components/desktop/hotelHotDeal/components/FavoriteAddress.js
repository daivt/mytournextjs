import { useState, useEffect } from "react";
import { Box, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { getListPost } from "@api/homes";
import InspiredCard from "@components/common/desktop/card/InspiredCard";
import { listString } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  container: { width: "100%", background: "white" },
  content: { maxWidth: 1188, margin: "0 auto", padding: "24px 0" },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
    marginBottom: 8,
  },
  titleDescription: {
    fontWeight: 400,
    fontSize: 14,
    lineHeight: "17px",
    color: "#4A5568",
  },
  locationDestinationBetween: { padding: "16px 0 8px" },
  itemBannerSmall: { height: "auto", marginBottom: 12 },
  styleWrapperImage: { height: 380 },
  imgSmall: { height: 152 },
  styleFontSize: { fontSize: 20 },
}));

const FavoriteAddress = () => {
  const classes = useStyles();
  const [locationList, setLocationList] = useState({});
  const fetchData = async () => {
    try {
      const { data } = await getListPost();
      if (data.code === 200) setLocationList(data.data.locationListing);
    } catch (error) {}
  };
  useEffect(() => {
    fetchData(); // eslint-disable-next-line
  }, []);
  const locationListing = locationList.posts || [];
  return (
    <Box className={classes.container}>
      <Box className={classes.content}>
        <Typography variant="body2" className={classes.titleText}>
          {listString.IDS_MT_TEXT_INSPIRE_HOTEL}
        </Typography>
        <Typography variant="body2" className={classes.titleDescription}>
          {listString.IDS_MT_TEXT_INSPIRE_DESCRIPTION_HOTEL}
        </Typography>
        <Box p="24px 0">
          <Grid
            container
            spacing={2}
            className={classes.locationDestinationBetween}
          >
            <Grid item lg={6} md={6} sm={6} style={{ paddingRight: "16px" }}>
              {locationListing.length >= 1 && (
                <InspiredCard
                  styleWrapperImage={classes.styleWrapperImage}
                  styleFontSize={classes.styleFontSize}
                  item={locationListing[0] || {}}
                />
              )}
            </Grid>

            <Grid item lg={6} md={6} sm={6}>
              <Grid container spacing={2}>
                <Grid
                  item
                  lg={6}
                  md={6}
                  sm={6}
                  className={classes.itemBannerSmall}
                >
                  {locationListing.length >= 2 && (
                    <InspiredCard
                      imgSmall={classes.imgSmall}
                      item={locationListing[1] || {}}
                    />
                  )}
                </Grid>
                <Grid
                  item
                  lg={6}
                  md={6}
                  sm={6}
                  className={classes.itemBannerSmall}
                >
                  {locationListing.length >= 3 && (
                    <InspiredCard
                      imgSmall={classes.imgSmall}
                      item={locationListing[2] || {}}
                    />
                  )}
                </Grid>
              </Grid>
              <Grid container spacing={2} style={{ paddingTop: 4 }}>
                <Grid
                  item
                  lg={6}
                  md={6}
                  sm={6}
                  className={classes.itemBannerSmall}
                >
                  {locationListing.length >= 4 && (
                    <InspiredCard
                      imgSmall={classes.imgSmall}
                      item={locationListing[3] || {}}
                    />
                  )}
                </Grid>
                <Grid
                  item
                  lg={6}
                  md={6}
                  sm={6}
                  className={classes.itemBannerSmall}
                >
                  {locationListing.length >= 5 && (
                    <InspiredCard
                      imgSmall={classes.imgSmall}
                      item={locationListing[4] || {}}
                    />
                  )}
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Box>
  );
};

export default FavoriteAddress;
