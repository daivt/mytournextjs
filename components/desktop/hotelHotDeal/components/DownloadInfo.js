import { Box, Typography } from "@material-ui/core";
import QRCode from "react-qr-code";
import { useRouter } from "next/router";
import Image from "@src/image/Image";
import { makeStyles } from "@material-ui/styles";
import { listString } from "@utils/constants";
import { IconCheckCircleWhite } from "@public/icons";

const useStyles = makeStyles((theme) => ({
  downloadContainer: { background: "#00B6F3", width: "100%" },
  downloadContent: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 106,
    marginTop: 79,
    height: 343,
  },
  imgContent: {
    marginTop: -50,
    width: 363,
    height: 571,
  },
  content: {
    width: "calc(50% - 182px)",
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    color: "white",
  },
  leftContent: { width: "100%", color: "white" },
  rightContent: { width: "100%", color: "white" },
  textItem: {
    fontSize: 14,
    lineHeight: "36px",
    display: "flex",
    alignItems: "center",
  },
  appleStore: {
    marginBottom: 10,
    width: 135,
    height: 40,
  },
  chPlay: {
    width: 135,
    height: 40,
  },
}));

const DownloadInfo = () => {
  const classes = useStyles();
  const router = useRouter();
  const description = [
    listString.IDS_MT_DOWNLOAD_TEXT_1,
    listString.IDS_MT_DOWNLOAD_TEXT_2,
    listString.IDS_MT_DOWNLOAD_TEXT_3,
    listString.IDS_MT_DOWNLOAD_TEXT_4,
    listString.IDS_MT_DOWNLOAD_TEXT_5,
  ];

  return (
    <Box className={classes.downloadContainer}>
      <Box className={classes.downloadContent}>
        <Box className={classes.content}>
          <Typography
            variant="body2"
            style={{
              fontWeight: 600,
              fontSize: 24,
              lineHeight: "29px",
              marginBottom: 8,
            }}
          >
            Tải ứng dụng Mytour.vn
          </Typography>
          <Typography
            variant="body2"
            style={{
              fontWeight: 400,
              fontSize: 14,
              lineHeight: "17px",
              marginBottom: 27,
            }}
          >
            Đặt vé máy bay, khách sạn hạng sang
          </Typography>
          <Box className={classes.leftContent}>
            <Box style={{ display: "flex", alignItems: "center" }}>
              <Box style={{ background: "white", padding: 8, width: 100 }}>
                <QRCode value="https://mytour.vn/" size={84} />
              </Box>
              <Box style={{ marginLeft: 20 }}>
                <Image
                  srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_app_store.svg"
                  className={classes.appleStore}
                />
                <Image
                  srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_chplay.svg"
                  className={classes.chPlay}
                />
              </Box>
            </Box>
          </Box>
        </Box>
        <Image
          srcImage="https://storage.googleapis.com/tripi-assets/mytour/images/Phone.png"
          alt=""
          className={classes.imgContent}
        />
        <Box className={classes.content}>
          <Box className={classes.rightContent}>
            {description.map((el, i) => (
              <Typography variant="body2" className={classes.textItem} key={i}>
                <IconCheckCircleWhite style={{ marginRight: 12 }} />
                {el}
              </Typography>
            ))}
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default DownloadInfo;
