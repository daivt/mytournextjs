import { useState, useEffect } from "react";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Layout from "@components/layout/desktop/Layout";
import InfoDescriptionHomeCard from "@components/common/desktop/card/InfoDescriptionHomeCard";
import { getHotelsAvailability } from "@api/hotels";
import { getTopHotels, getHotelsHotDeal } from "@api/homes";
import {
  isEmpty,
  adapterHotelHotDeal,
  paramsDefaultHotel,
  adapterHotelAvailability,
} from "@utils/helpers";
import { DELAY_TIMEOUT_POLLING } from "@utils/constants";
import CityList from "./components/CityList";
import BannerSlide from "./components/BannerSlide";
import DownloadInfo from "./components/DownloadInfo";
import BubbleHotel from "./components/BubbleHotel";
import FavoriteAddress from "./components/FavoriteAddress";
import MytourMall from "./components/MytourMall";
import TopPriceHotel from "./components/TopPriceHotel";
import FavoriteHotel from "./components/FavoriteHotel";

const useStyles = makeStyles((theme) => ({
  homeWrapper: { width: "100%", background: "white" },
  homeContainer: { maxWidth: 1188, margin: "0 auto", padding: "24px 0" },
}));
const Home = ({ topLocation = [] }) => {
  const classes = useStyles();
  let breakPolling = true;
  const [hotelHotDeal, setHotelHotDeal] = useState([]);
  const [topHotels, setTopHotels] = useState([]);
  const [isServerLoaded, setServerLoaded] = useState(false);

  const mergerPriceToData = (data = [], type, result = []) => {
    const dataResult = [...result];
    data.forEach((el) => {
      dataResult.forEach((item, index) => {
        if (item.id === el.id) {
          dataResult[index] = {
            ...item,
            promotionInfo: el.promotionInfo,
            price: el.basePrice,
          };
          return;
        }
      });
    });
    if (type === "hotelHotDeal") setHotelHotDeal(dataResult);
    else if (type === "topHotels") setTopHotels(dataResult);
  };
  const fetchPriceHotel = async (ids, hotDealData, topHotelData) => {
    let polling = true;
    try {
      const dataDto = {
        ...paramsDefaultHotel(),
        hotelIds: ids,
        searchType: "hotel",
      };
      while (polling && breakPolling) {
        const { data } = await getHotelsAvailability(dataDto);
        if (data.code === 200) {
          if (data.data.completed) {
            mergerPriceToData(data.data.items, "hotelHotDeal", hotDealData);
            mergerPriceToData(data.data.items, "topHotels", topHotelData);
            polling = false;
            break;
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      polling = false;
    }
  };
  const fetchData = async (param = {}) => {
    try {
      const params = { page: 1, size: 20, ...paramsDefaultHotel() };
      let temp = [];
      let resHotDeal = {};
      if (isEmpty(param)) {
        resHotDeal = await getHotelsHotDeal(params);
        if (!isEmpty(resHotDeal.data) && resHotDeal.data.code === 200) {
          temp = [...temp, ...resHotDeal.data.data.items];
          setHotelHotDeal(
            !isEmpty(resHotDeal.data.data) ? resHotDeal.data.data.items : []
          );
        }
      }
      const resTopHotel = await getTopHotels({ ...params, ...param });
      if (!isEmpty(resTopHotel.data) && resTopHotel.data.code === 200) {
        temp = [...temp, ...resTopHotel.data.data.items];
        setTopHotels(
          !isEmpty(resTopHotel.data.data) ? resTopHotel.data.data.items : []
        );
      }
      if (!isEmpty(temp)) {
        fetchPriceHotel(
          temp.map((el) => el.id),
          !isEmpty(resHotDeal.data) ? resHotDeal.data.data.items : [],
          resTopHotel.data.data.items
        );
      }
    } catch (error) {}
  };

  useEffect(() => {
    setServerLoaded(true);
    fetchData();
    return () => {
      breakPolling = false;
    }; // eslint-disable-next-line
  }, []);
  return (
    <Layout isHeader isFooter type="hotelHotDeal" topLocation={topLocation}>
      <Box className={classes.homeWrapper}>abc</Box>
    </Layout>
  );
};

export default Home;
