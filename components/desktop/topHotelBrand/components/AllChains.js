import { useState } from "react";
import {
  Box,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { isEmpty, paramsDefaultHotel } from "@utils/helpers";
import { listString, routeStatic } from "@utils/constants";
import ButtonComponent from "@src/button/Button";
import Image from "@src/image/Image";
import { IconLoadingHotel } from "@public/icons";
import Link from "@src/link/Link";

const useStyles = makeStyles((theme) => ({
  blockContainer: { color: "#1A202C", marginTop: 52 },
  collapseContainer: {
    boxShadow: "none",
    "& > .MuiAccordionDetails-root": {
      padding: 0,
      display: "flex",
      flexDirection: "column",
    },
  },
  buttonCommon: {
    margin: "0 auto",
    backgroundColor: "white",
    color: "#1A202C",
    borderColor: "#1A202C",
    width: "fit-content",
    borderRadius: 8,
    padding: "12px 50px",
    "&:hover": {
      backgroundColor: "white",
    },
  },
  brandTitle: {
    fontWeight: 600,
    fontSize: 22,
    lineHeight: "26px",
    margin: "0 auto",
    marginBottom: 24,
  },
  brandGroup: {
    display: "flex",
    flexWrap: "wrap",
    margin: "0 -8px",
  },
  brandItem: {
    border: "1px solid #EDF2F7",
    borderRadius: 8,
    padding: "16px",
    width: "calc(100%/7)",
    maxWidth: 156,
    height: 90,
    margin: 8,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    cursor: "pointer",
  },
  logo: {
    width: "auto",
    height: "auto",
    maxWidth: 124,
    maxHeight: 58,
    position: "relative",
  },
}));

const AllChains = ({ brandList }) => {
  const classes = useStyles();
  const [collapse, setCollapse] = useState(false);
  return (
    <Box className={classes.blockContainer}>
      <Accordion
        expanded={collapse}
        onChange={(e, expanded) => {}}
        className={classes.collapseContainer}
      >
        {!collapse && (
          <AccordionSummary expandIcon={null}>
            <ButtonComponent
              typeButton="outlined"
              className={classes.buttonCommon}
              handleClick={() => setCollapse(true)}
            >
              {listString.IDS_MT_VIEW_ALL_CHAINS}
            </ButtonComponent>
          </AccordionSummary>
        )}

        <AccordionDetails>
          {/* <Typography variant="p" className={classes.brandTitle}>
            {listString.IDS_MT_ALL_BRANDS}
          </Typography> */}
          <Box className={classes.brandGroup}>
            {brandList.map((item, index) => {
              if (index < 7) return null;
              return (
                <Box key={item.id} className={classes.brandItem}>
                  <Link
                    href={{
                      pathname: routeStatic.LISTING_HOTEL.href,
                      query: {
                        slug: [
                          `${item.id}`,
                          `khach-san-tai-${
                            !isEmpty(item.name) ? item.name.stringSlug() : ""
                          }.html`,
                        ],
                        ...paramsDefaultHotel(),
                        chain: item.id,
                        type: "chain",
                      },
                    }}
                  >
                    {!isEmpty(item.logo) ? (
                      <Image srcImage={item.logo} className={classes.logo} />
                    ) : (
                      <Box
                        className={classes.logo}
                        style={{ position: "relative" }}
                      >
                        <Box className={classes.hotelDefault}>
                          <IconLoadingHotel />
                        </Box>
                      </Box>
                    )}
                  </Link>
                </Box>
              );
            })}
          </Box>
        </AccordionDetails>
        {collapse && (
          <AccordionSummary expandIcon={null}>
            <ButtonComponent
              typeButton="outlined"
              className={classes.buttonCommon}
              style={{ marginTop: 20 }}
              handleClick={() => setCollapse(false)}
            >
              {listString.IDS_MT_TEXT_LOAD_COMPACT}
            </ButtonComponent>
          </AccordionSummary>
        )}
      </Accordion>
    </Box>
  );
};

export default AllChains;
