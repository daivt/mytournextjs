import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconArrowDownToggle, IconLoadingHotel } from "@public/icons";
import Image from "@src/image/Image";
import Link from "@src/link/Link";
import { routeStatic } from "@utils/constants";
import { isEmpty, paramsDefaultHotel } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  imgTop: {
    borderRadius: 8,
    filter: "drop-shadow(0px 5px 5px rgba(0, 0, 0, 0.1))",
  },
  itemTop: { position: "relative", width: 380, height: 360 },
  hotelTop: {
    position: "absolute",
    background: "#FFF",
    borderRadius: "0px 8px",
    boxShadow: "0px 8px 8px rgba(0, 0, 0, 0.15)",
    bottom: 16,
    left: 16,
    cursor: "pointer",
    width: 348,
  },
  logoTop: { width: 113, height: 64, padding: 12, objectFit: "contain" },
  divider: { width: 1, height: 48, background: "#E2E8F0", marginLeft: 12 },
  count: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    color: theme.palette.gray.grayDark8,
    marginTop: 6,
  },
  name: {
    marginLeft: 12,
    width: "calc(100% - 75px)",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  hotelDefault: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%,-50%)",
  },
  linkCommon: {
    color: "#1A202C",
    "&:hover": { textDecoration: "none" },
  },
}));
const ItemBrandListSmall = ({ item = {} }) => {
  const classes = useStyles();
  return (
    <Link
      href={{
        pathname: routeStatic.LISTING_HOTEL.href,
        query: {
          slug: [
            `${item.id}`,
            `khach-san-tai-${
              !isEmpty(item.name) ? item.name.stringSlug() : ""
            }.html`,
          ],
          ...paramsDefaultHotel(),
          chain: item.id,
          type: "chain",
        },
      }}
      className={classes.linkCommon}
    >
      <Image
        srcImage={item.thumbnail}
        className={classes.imgTop}
        borderRadiusProp="8px"
      />
      <Box className={classes.hotelTop}>
        <Box display="flex" alignItems="center">
          {!isEmpty(item.logo) ? (
            <Image srcImage={item.logo} className={classes.logoTop} />
          ) : (
            <Box className={classes.logoTop} style={{ position: "relative" }}>
              <Box className={classes.hotelDefault}>
                <IconLoadingHotel />
              </Box>
            </Box>
          )}
          <Box className={classes.divider} />
          <Box className={classes.name}>
            <Box display="flex" flexDirection="column">
              <Typography
                variant="subtitle1"
                style={{
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                  width: 160,
                }}
                title={item.name}
              >
                {item.name}
              </Typography>
              {!isEmpty(item.hotelsNum) && (
                <Box className={classes.count}>{item.hotelsNum} khách sạn</Box>
              )}
            </Box>
            <IconArrowDownToggle
              style={{ marginRight: "12px", transform: "rotate(270deg)" }}
            />
          </Box>
        </Box>
      </Box>
    </Link>
  );
};

export default ItemBrandListSmall;
