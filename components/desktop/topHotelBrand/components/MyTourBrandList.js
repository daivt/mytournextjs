import { useState, useEffect } from "react";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import { isEmpty } from "@utils/helpers";
import { getChainsHotels } from "@api/homes";
import clsx from "clsx";
import ItemBrandListSmall from "@components/desktop/topHotelBrand/components/ItemBrandListSmall";
import ItemBrandListBig from "@components/desktop/topHotelBrand/components/ItemBrandListBig";
import AllChains from "./AllChains";

const useStyles = makeStyles((theme) => ({
  homeWrapper: { width: "100%", background: "white" },
  homeContainer: { maxWidth: 1188, margin: "0 auto", padding: "24px 0" },
  listTop: { display: "flex" },
  itemTop: { position: "relative", width: 380, height: 360 },
  imgTop: { width: 380, height: 360, borderRadius: 8 },
  imgMiddle: { width: 784, height: 360, borderRadius: 8, marginRight: 24 },
  imgBottom: { width: 784, height: 360, borderRadius: 8, marginLeft: 24 },
  hotelTop: {
    position: "absolute",
    background: "#FFF",
    borderRadius: "0px 8px",
    boxShadow: "0px 8px 8px rgba(0, 0, 0, 0.15)",
    bottom: 16,
    left: 16,
    cursor: "pointer",
    width: 348,
  },
  hotelBottom: {
    position: "absolute",
    background: "#FFF",
    borderRadius: "0px 8px",
    boxShadow: "0px 8px 8px rgba(0, 0, 0, 0.15)",
    bottom: 16,
    width: 348,
    right: 16,
  },
  logoTop: { width: 113, height: 64, padding: 12, objectFit: "contain" },
  divider: { width: 1, height: 48, background: "#E2E8F0", marginLeft: 12 },
  count: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    color: theme.palette.gray.grayDark8,
    marginTop: 6,
  },
  name: {
    marginLeft: 12,
    width: "calc(100% - 75px)",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  hotelDefault: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%,-50%)",
  },
  styleHotelTop: {
    right: 16,
    left: "inherit",
  },
}));
const MyTourBrandList = (props) => {
  const classes = useStyles();
  const [brandList, setBrandList] = useState([]);
  const fetchChains = async () => {
    try {
      const { data } = await getChainsHotels({ size: 100 });
      if (data.code === 200 && !isEmpty(data.data))
        setBrandList(data.data.items);
    } catch (error) {}
  };
  useEffect(() => {
    fetchChains(); // eslint-disable-next-line
  }, []);
  if (isEmpty(brandList)) return null;
  return (
    <>
      <Box className={classes.wrapList}>
        <Box className={classes.listTop}>
          <Box className={classes.itemTop}>
            <ItemBrandListSmall item={brandList[0]} />
          </Box>
          <Box className={classes.itemTop} style={{ margin: "0 24px" }}>
            <ItemBrandListSmall item={brandList[1]} />
          </Box>
          <Box className={classes.itemTop}>
            <ItemBrandListSmall item={brandList[2]} />
          </Box>
        </Box>

        {/* Middle */}
        <Box className={classes.listTop} style={{ marginTop: 24 }}>
          <Box className={clsx(classes.itemTop, classes.imgMiddle)}>
            <ItemBrandListBig item={brandList[3]} />
          </Box>

          <Box className={classes.itemTop}>
            <ItemBrandListSmall item={brandList[4]} />
          </Box>
        </Box>

        {/* Bottom */}
        <Box className={classes.listTop} style={{ marginTop: 24 }}>
          <Box className={classes.itemTop}>
            <ItemBrandListSmall item={brandList[5]} />
          </Box>

          <Box className={clsx(classes.itemTop, classes.imgBottom)}>
            <ItemBrandListBig
              item={brandList[6]}
              styleHotelTop={classes.styleHotelTop}
            />
          </Box>
        </Box>

        {!isEmpty(brandList) && brandList.length > 7 && (
          <AllChains brandList={brandList} />
        )}
      </Box>
    </>
  );
};

export default MyTourBrandList;
