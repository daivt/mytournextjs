import clsx from "clsx";
import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { isEmpty } from "@utils/helpers";
import Image from "@src/image/Image";
import {
  IconRoomServiceCart,
  IconLoveGiftDiamond,
  IconEstateLocation,
  IconReception,
} from "@public/icons";
import { listIcons, listString, prefixUrlIcon } from "@utils/constants";

const bannerURl =
  "https://storage.googleapis.com/tripi-assets/mytour/images/room-01.jpg";

const useStyles = makeStyles((theme) => ({
  blockContainer: {
    backgroundImage: `url(${bannerURl})`,
    paddingTop: 140,
    marginTop: 120,
  },
  wrapBox: {
    background: "rgba(255, 255, 255, 0.96)",
    padding: "30px 56px",
    position: "relative",
  },
  special: {
    fontSize: 30,
    lineHeight: "35px",
    fontWeight: 600,
    color: theme.palette.black.black3,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 30,
  },
  content: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    opacity: 0.96,
    position: "relative",
    zIndex: 2,
  },
  title: { lineHeight: "19px", marginTop: 17 },
  description: {
    fontSize: 14,
    lineHeight: "22px",
    fontWeight: 400,
    color: theme.palette.gray.grayDark8,
    marginTop: 8,
  },
  itemNonMargin: {
    marginBottom: "0 !important",
  },
  itemList: {
    width: "32%",
    marginBottom: 38,
    marginRight: 15,
    zIndex: 2,
  },
  nonMarginRight: {
    marginRight: 0,
  },
  wrapMytourMall: {
    display: "flex",
    alignItems: "center",
    bottom: 0,
    left: 0,
    position: "absolute",
    zIndex: 1,
    width: "100%",
  },
  myTourMallLogo: {
    marginLeft: 9,
    width: 206,
    height: 55,
  },
  iconMallBackground: {
    width: 561,
    height: 167,
  },
  iconMytourBackground: {
    margin: "22px 0 0 0",
    width: 693,
    height: 100,
  },
}));
const dataMytourMall = [
  {
    id: 0,
    icon: <IconRoomServiceCart />,
    title: listString.IDS_MT_MALL_DESIGN_SPECICAL,
    description: listString.IDS_MT_MALL_DESIGN_SPECICAL_DETAILS,
  },
  {
    id: 1,

    icon: <IconLoveGiftDiamond />,
    title: listString.IDS_MT_MALL_CONVIENT_ROYAL,
    description: listString.IDS_MT_MALL_CONVIENT_ROYAL_DETAILS,
  },
  {
    id: 2,
    icon: <IconReception />,
    title: listString.IDS_MT_MALL_REQUEST_SERVICE,
    description: listString.IDS_MT_MALL_REQUEST_SERVICE_DETAILS,
  },
  {
    id: 3,
    icon: <IconEstateLocation />,
    title: listString.IDS_MT_MALL_TOP_LOCATION,
    description: listString.IDS_MT_MALL_TOP_LOCATION_DETAILS,
  },
  {
    id: 4,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconTransportTicket}`}
        alt="icon_transfer_ticket"
        style={{ width: 36, height: 36 }}
      />
    ),
    title: listString.IDS_MT_MALL_AIRPORT_SHUTTLE,
    description: listString.IDS_MT_MALL_AIRPORT_SHUTTLE_DETAILS,
  },
  {
    id: 5,
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconRatingMall}`}
        alt="icon_rating_mall"
        style={{ width: 36, height: 36 }}
      />
    ),
    title: listString.IDS_MT_MALL_CHECK_QUALITY_REAL,
    description: listString.IDS_MT_MALL_CHECK_QUALITY_REAL_DETAILS,
  },
];
const MyTourBrandInfo = (props) => {
  const classes = useStyles();
  return (
    <Box className={classes.blockContainer}>
      <Box style={{ maxWidth: 1300, margin: "0 auto" }}>
        <Box className={classes.wrapBox}>
          <Box className={classes.header}>
            <Box className={classes.special}>
              Sự khác biệt{" "}
              <Image
                srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_mytour_mall.svg"
                className={classes.myTourMallLogo}
              />
            </Box>
          </Box>
          <Box className={classes.content}>
            {!isEmpty(dataMytourMall) &&
              dataMytourMall.map((el, index) => (
                <Box
                  key={index}
                  display="flex"
                  flexDirection="column"
                  className={clsx(
                    classes.itemList,
                    index > 2 && classes.itemNonMargin,
                    index === 2 && classes.nonMarginRight,
                    index === 5 && classes.nonMarginRight
                  )}
                >
                  <Box>{el.icon}</Box>
                  <Typography variant="subtitle1" className={classes.title}>
                    {el.title}
                  </Typography>
                  <Box className={classes.description}>{el.description}</Box>
                </Box>
              ))}
          </Box>
          <Box className={classes.wrapMytourMall}>
            <Image
              srcImage={`${prefixUrlIcon}${listIcons.IconMytourBackground}`}
              className={classes.iconMytourBackground}
            />
            <Image
              srcImage={`${prefixUrlIcon}${listIcons.IconMallBackground}`}
              className={classes.iconMallBackground}
            />
          </Box>
        </Box>{" "}
      </Box>
    </Box>
  );
};

export default MyTourBrandInfo;
