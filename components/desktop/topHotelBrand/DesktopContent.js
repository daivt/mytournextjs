import { useState, useEffect } from "react";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Layout from "@components/layout/desktop/Layout";
import clsx from "clsx";
import { getHotelsAvailability, getChainsHotel } from "@api/hotels";
import {
  adapterHotelHotDeal,
  adapterHotelHotDealPc,
  isEmpty,
  paramsDefaultHotel,
} from "@utils/helpers";
import { DELAY_TIMEOUT_POLLING, listString } from "@utils/constants";
import { IconArrowDownToggle } from "@public/icons";

import HotelItemVertical from "@components/common/desktop/itemView/HotelItemVertical";

import MyTourBrandList from "./components/MyTourBrandList";
import MyTourBrandInfo from "./components/MyTourBrandInfo";

const useStyles = makeStyles((theme) => ({
  homeWrapper: { width: "100%", background: "white" },
  homeContainer: { maxWidth: 1188, margin: "0 auto", padding: "24px 0" },
  titlePageGroup: {
    color: "#1A202C",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "80px 0 48px 0",
  },
  leftText: { fontWeight: 600, fontSize: 30, lineHeight: "44px" },
  rightText: {
    fontWeight: "normal",
    fontSize: 18,
    lineHeight: "26px",
    textAlign: "right",
    marginLeft: 168,
  },
  topHotel: {
    fontSize: 30,
    lineHeight: "35px",
    fontWeight: 600,
    color: theme.palette.black.black3,
    display: "flex",
    alignItems: "center",
    paddingBottom: 30,
    justifyContent: "space-between",
    marginTop: 120,
  },
  textHeader: { width: "45%" },
  galleryHotel: { display: "flex", flexWrap: "wrap", margin: "0 -12px" },
  wrapArrow: {
    borderRadius: 100,
    position: "relative",
    background: theme.palette.gray.grayLight22,
    height: 48,
    width: 48,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    cursor: "pointer",
  },
  arrow: {
    position: "absolute",
  },
  wrapArrowRight: {
    marginLeft: 8,
  },
  arrowLeft: { transform: "rotate(90deg)" },
  arrowRight: { transform: "rotate(270deg)" },
  stylePoint: {
    color: theme.palette.black.black3,
    background: theme.palette.gray.grayLight23,
  },
  styleMessageReview: {
    color: theme.palette.black.black3,
  },
}));
let pageActive = 1;
const Home = ({ topLocation = [] }) => {
  const classes = useStyles();
  let breakPolling = true;
  const [topHotels, setTopHotels] = useState([]);
  const [totalHotels, setTotalHotels] = useState(0);
  const [isServerLoaded, setServerLoaded] = useState(false);

  const mergerPriceToData = (data = [], result = []) => {
    const dataResult = [...result];
    data.forEach((el) => {
      dataResult.forEach((item, index) => {
        if (item.id === el.id) {
          dataResult[index] = {
            ...item,
            basePromotionInfo: el.basePromotionInfo,
            price: el.basePrice,
            hiddenPrice: el.hiddenPrice,
          };
          return;
        }
      });
    });
    setTopHotels(dataResult);
  };
  const fetchPriceHotel = async (ids, topHotelData) => {
    let polling = true;
    try {
      const dataDto = {
        ...paramsDefaultHotel(),
        hotelIds: ids,
        searchType: "hotel",
      };
      while (polling && breakPolling) {
        const { data } = await getHotelsAvailability(dataDto);
        if (data.code === 200) {
          if (data.data.completed) {
            mergerPriceToData(data.data.items, topHotelData);
            polling = false;
            break;
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      polling = false;
    }
  };
  const fetchData = async (param = {}) => {
    try {
      const params = { page: pageActive, size: 8, ...paramsDefaultHotel() };
      let temp = [];
      const res = await getChainsHotel({ ...params, ...param });
      if (!isEmpty(res.data) && res.data.code === 200) {
        temp = [...res.data.data.items];
        setTopHotels(!isEmpty(res.data.data) ? res.data.data.items : []);
        setTotalHotels(res.data.data.total);
      }
      if (!isEmpty(temp)) {
        fetchPriceHotel(
          temp.map((el) => el.id),
          res.data.data.items
        );
      }
    } catch (error) {}
  };
  useEffect(() => {
    setServerLoaded(true);
    fetchData();
    return () => {
      breakPolling = false;
      pageActive = 1;
    }; // eslint-disable-next-line
  }, []);
  return (
    <Layout isHeader isFooter type="myTourMall" topLocation={topLocation}>
      <Box className={classes.homeWrapper}>
        <Box className={clsx(classes.homeContainer, classes.titlePageGroup)}>
          <Box className={classes.leftText}>
            {listString.IDS_MT_MALL_TOP_HOTEL_BRAND}
          </Box>
          <Box className={classes.rightText}>
            {listString.IDS_MT_MALL_TOP_HOTEL_BRAND_DESCRIPTION}
          </Box>
        </Box>
      </Box>
      <Box className={classes.homeWrapper}>
        <Box className={classes.homeContainer}>
          <MyTourBrandList />
        </Box>
      </Box>
      <Box className={classes.homeWrapper}>
        <MyTourBrandInfo />
      </Box>
      <Box className={classes.homeWrapper}>
        <Box className={classes.homeContainer}>
          <Box className={classes.topHotel}>
            <Box className={classes.textHeader}>
              Top khách sạn của các thương hiệu được nhiều người đặt
            </Box>
            {!isEmpty(topHotels) && (
              <Box display="flex" alignItems="center">
                <Box
                  className={classes.wrapArrow}
                  style={{
                    cursor: pageActive > 1 ? "pointer" : "not-allowed",
                    opacity: pageActive > 1 ? 1 : 0.5,
                  }}
                  onClick={() => {
                    if (pageActive > 1) {
                      pageActive -= 1;
                      fetchData();
                    }
                  }}
                >
                  <IconArrowDownToggle
                    className={clsx(classes.arrow, classes.arrowLeft)}
                  />
                </Box>
                <Box
                  className={clsx(classes.wrapArrow, classes.wrapArrowRight)}
                  style={{
                    cursor:
                      pageActive * 8 < totalHotels ? "pointer" : "not-allowed",
                    opacity: pageActive * 8 < totalHotels ? 1 : 0.5,
                  }}
                  onClick={() => {
                    if (pageActive * 8 < totalHotels) {
                      pageActive += 1;
                      fetchData();
                    }
                  }}
                >
                  <IconArrowDownToggle
                    className={clsx(classes.arrow, classes.arrowRight)}
                  />
                </Box>
              </Box>
            )}
          </Box>
          <Box className={classes.galleryHotel}>
            {!isEmpty(topHotels) &&
              adapterHotelHotDealPc(topHotels).map((el, index) => {
                if (index > 7) return null;
                return (
                  <Box
                    style={{ margin: "0 12px", marginBottom: 24 }}
                    key={index}
                  >
                    <HotelItemVertical
                      item={el}
                      styleTripAdvisor
                      isShowLastBook
                      isShowStatus
                      stylePoint={classes.stylePoint}
                      styleMessageReview={classes.styleMessageReview}
                    />
                  </Box>
                );
              })}
          </Box>
        </Box>
      </Box>
    </Layout>
  );
};

export default Home;
