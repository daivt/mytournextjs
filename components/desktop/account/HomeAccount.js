import clsx from "clsx";
import { useState, useEffect } from "react";
import { Box, Tabs, Tab } from "@material-ui/core";
import { useRouter } from "next/router";
import { makeStyles, withStyles } from "@material-ui/styles";

import utilStyles from "@styles/utilStyles";
import { IconDropDown } from "@public/icons";
import { listString, routeStatic } from "@utils/constants";
import { handleTitleFlight } from "@utils/helpers";

import Link from "@src/link/Link";
import Layout from "@components/layout/desktop/Layout";

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
  },
  topContainer: {
    backgroundColor: theme.palette.primary.main,
    height: 105,
    color: theme.palette.white.main,
  },
  iconDropDown: {
    stroke: theme.palette.white.main,
    transform: "rotate(270deg)",
    margin: "0 4px",
  },
  contentTopContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    height: "100%",
    padding: "16px 0",
  },
  linkHome: {
    color: theme.palette.white.main,
    "&:hover": {
      textDecoration: "none",
    },
  },
  tabItem: {
    textTransform: "none",
    fontWeight: 600,
    lineHeight: "17px",
    color: theme.palette.black.black3,
    opacity: 1,
    "&.Mui-selected": {
      color: theme.palette.blue.blueLight8,
    },
  },
  indicator: {
    backgroundColor: theme.palette.gray.grayDark1,
  },
  tabPanel: {
    height: "100%",
  },
}));

export const TabsInient = withStyles((theme) => ({
  indicator: {
    background: theme.palette.blue.blueLight8,
  },
}))(Tabs);

const getValueTab = (router) => {
  switch (router.route) {
    case routeStatic.ACCOUNT.href:
      return 0;
    case routeStatic.ACCOUNT_ORDER_BOOKING.href:
    case routeStatic.ACCOUNT_ORDER_BOOKING_DETAIL.href:
      return 1;
    case routeStatic.ACCOUNT_ORDER_FLIGHT.href:
    case routeStatic.ACCOUNT_ORDER_FLIGHT_DETAIL.href:
      return 2;
    case routeStatic.ACCOUNT_FAVORITES_HOTEL.href:
      return 3;
    default:
      return 0;
  }
};

const HomeAccount = ({ children = null, query = {}, codeFlight = {} }) => {
  const router = useRouter();
  const classes = useStyles();
  const classesUtils = utilStyles();
  const [currentTab, setCurrentTab] = useState(getValueTab(router));
  useEffect(() => {
    setCurrentTab(getValueTab(router));
  }, [router]);

  const handleChangeTab = (event, newValue) => {
    setCurrentTab(newValue);
    switch (newValue) {
      case 0: {
        router.push({
          pathname: routeStatic.ACCOUNT.href,
        });
        break;
      }
      case 1: {
        router.push({
          pathname: routeStatic.ACCOUNT_ORDER_BOOKING.href,
        });
        break;
      }
      case 2: {
        router.push({
          pathname: routeStatic.ACCOUNT_ORDER_FLIGHT.href,
        });
        break;
      }
      case 3: {
        router.push({
          pathname: routeStatic.ACCOUNT_FAVORITES_HOTEL.href,
        });
        break;
      }
    }
  };

  return (
    <Layout
      isHeader
      isFooter={false}
      type="account"
      {...handleTitleFlight(query, codeFlight)}
    >
      <Box className={classes.container}>
        <Box className={classes.topContainer}>
          <Box
            className={clsx(
              classesUtils.container,
              classes.contentTopContainer
            )}
          >
            <Box display="flex" alignItems="center" lineHeight="22px">
              <Link
                href={{
                  pathname: routeStatic.HOME.href,
                }}
                className={classes.linkHome}
              >
                <Box component="span">{listString.IDS_MT_TEXT_PAGE_HOME}</Box>
              </Link>

              <IconDropDown className={`svgFillAll ${classes.iconDropDown}`} />
              <Box component="span">{listString.IDS_MT_TEXT_ACCOUNT}</Box>
            </Box>
            <Box
              component="span"
              fontSize={30}
              lineHeight="36px"
              fontWeight={600}
            >
              {listString.IDS_MT_TEXT_ACCOUNT}
            </Box>
          </Box>
        </Box>
        <Box className={clsx(classesUtils.container)}>
          <TabsInient
            value={currentTab}
            onChange={handleChangeTab}
            classes={{ indicator: classes.indicator }}
            aria-label="full width tabs example"
          >
            <Tab
              label={listString.IDS_MT_TEXT_MANAGEMENT_ACCOUNT}
              className={classes.tabItem}
            />
            <Tab
              label={listString.IDS_MT_TEXT_ORDER_HOTEL}
              className={classes.tabItem}
            />
            <Tab
              label={listString.IDS_MT_TEXT_FLIGHT}
              className={classes.tabItem}
            />
            <Tab
              label={listString.IDS_MT_TEXT_HOTEL_FAVORITE}
              className={classes.tabItem}
            />
          </TabsInient>
          <Box pt={2}>
            <Box className={classes.content}>{children || null}</Box>
          </Box>
        </Box>
      </Box>
    </Layout>
  );
};

export default HomeAccount;
