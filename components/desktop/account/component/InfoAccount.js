import { Box } from "@material-ui/core";
import { useEffect } from "react";

import { isEmpty } from "@utils/helpers";
import { useFormikContext } from "formik";
import { listString } from "@utils/constants";
import { useSystem } from "@contextProvider/ContextProvider";

import ButtonComponent from "@src/button/Button";
import FieldTextBaseContent from "@src/form/FieldTextBaseContent";

const InfoAccount = () => {
  const { systemReducer } = useSystem();
  const { setFieldValue } = useFormikContext();
  useEffect(() => {
    const informationUser = systemReducer.informationUser || {};
    if (!isEmpty(informationUser)) {
      setFieldValue("name", informationUser?.name);
      setFieldValue("emailInfo", informationUser?.emailInfo || "");
      setFieldValue("phoneInfo", informationUser?.phoneInfo || "");
      setFieldValue("address", informationUser?.address || "");
    }
  }, [systemReducer.informationUser]);

  return (
    <>
      <FieldTextBaseContent
        id="field-name"
        name="name"
        inputProps={{
          autoComplete: "off",
        }}
        label={listString.IDS_MT_TEXT_CONTACT_NAME_SIMPLE}
      />
      <FieldTextBaseContent
        id="field-email"
        name="emailInfo"
        inputProps={{
          autoComplete: "off",
        }}
        label={listString.IDS_MT_TEXT_EMAIL}
      />
      <FieldTextBaseContent
        id="field-phoneNumber"
        disabled
        name="phoneInfo"
        inputProps={{
          autoComplete: "off",
        }}
        label={listString.IDS_MT_TEXT_PHONE_NUMBER}
      />
      <FieldTextBaseContent
        id="field-phoneNumber"
        name="address"
        inputProps={{
          autoComplete: "off",
        }}
        label="Địa chỉ"
      />
      {/* <FieldTextBaseContent
        id="field-phoneNumber"
        name="provice"
        inputProps={{
          autoComplete: "off",
        }}
        label="Tỉnh/thành phố"
      /> */}
      <Box>
        <ButtonComponent
          backgroundColor="#FF1284"
          borderRadius={8}
          height={44}
          padding="10px 40px"
          fontSize={16}
          fontWeight={600}
          type="submit"
        >
          Lưu lại
        </ButtonComponent>
      </Box>
    </>
  );
};

export default InfoAccount;
