import cookie from "js-cookie";
import { Box } from "@material-ui/core";
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";

import {
  isEmpty,
  paramsDefaultHotel,
  adapterHotelFavorite,
} from "@utils/helpers";
import { getListFavorite } from "@api/user";
import { getHotelsAvailability } from "@api/hotels";
import { TOKEN, LAST_HOTEL_FAVORITES } from "@utils/constants";
import * as actionTypes from "@contextProvider/system/ActionTypes";
import { useDispatchSystem } from "@contextProvider/ContextProvider";

import UsePagination from "@src/pagination/Pagination";
import HomeAccount from "@components/desktop/account/HomeAccount";
import NoHotelFavorite from "@components/mobile/homes/NoHotelFavorite";
import ItemHotelListing from "@components/desktop/hotels/listing/components/ItemHotelListing";

const useStyles = makeStyles((theme) => ({
  paginationContainer: {
    width: "100%",
    display: "flex",
    justifyContent: "flex-end",
    margin: "32px 0",
  },
  cusResultSearch: {
    margin: "95px 0 155px !important",
  },
}));

let listHotelFavorites = [];
const pageSize = 10;

const FavoriteHotel = () => {
  const classes = useStyles();
  const [page, setPage] = useState(1);
  const [listHotel, setListHotel] = useState([]);
  const [totalHotel, setTotalHotel] = useState(0);
  const [isShowEmpty, setIsShowEmpty] = useState(false);
  const dispatch = useDispatchSystem();

  useEffect(() => {
    if (!isEmpty(cookie.get(TOKEN))) {
      fetHotelFavoritesAuth();
    } else {
      listHotelFavorites =
        JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
      if (!isEmpty(listHotelFavorites)) {
        fetHotelFavorites(listHotelFavorites);
      }
    }
  }, [page]);

  const fetHotelFavoritesAuth = async () => {
    try {
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: true,
      });
      const { data } = await getListFavorite({ page, size: pageSize });
      setIsShowEmpty(true);
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
      if (data.code === 200) {
        setListHotel(data.data.items || []);
        setTotalHotel(data.data.total || 0);
      } else {
      }
    } catch {
      setIsShowEmpty(true);
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
    }
  };

  const fetHotelFavorites = async (listHotel = []) => {
    try {
      let polling = true;
      while (polling) {
        const { data } = await getHotelsAvailability({
          hotelIds: listHotel,
          ...paramsDefaultHotel(),
          searchType: "hotel",
          page,
          size: pageSize,
        });
        if (data.code === 200) {
          if (data.data.completed) {
            setIsShowEmpty(true);
            setListHotel(data?.data.items);
            setTotalHotel(data?.data.total);
            polling = false;
            break;
          } else {
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      setIsShowEmpty(true);
      polling = false;
    }
  };

  const handleChangePage = (page) => () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
    setTimeout(() => {
      setPage(page);
    }, 200);
  };

  return (
    <HomeAccount>
      {isShowEmpty && totalHotel === 0 ? (
        <NoHotelFavorite cusResultSearch={classes.cusResultSearch} />
      ) : (
        <Box minHeight="calc(100vh - 210px)">
          {adapterHotelFavorite(listHotel).map((el, i) => (
            <Box key={i} style={{ margin: "12px 0" }}>
              <ItemHotelListing
                paramsFilterInit={paramsDefaultHotel()}
                item={el}
              />
            </Box>
          ))}
          {totalHotel > 0 && (
            <UsePagination
              pageSize={pageSize}
              total={totalHotel}
              pageNum={page}
              className={classes.paginationContainer}
              handleChangePage={handleChangePage}
            />
          )}
        </Box>
      )}
    </HomeAccount>
  );
};

export default FavoriteHotel;
