import * as yup from "yup";
import cookie from "js-cookie";
import { Formik, Form } from "formik";
import { useSnackbar } from "notistack";
import { useState, useEffect } from "react";
import { useDropzone } from "react-dropzone";
import { makeStyles } from "@material-ui/styles";
import { Box, Avatar, Badge } from "@material-ui/core";

import { isEmpty } from "@utils/helpers";
import { IconCamera } from "@public/icons";
import { listString, TOKEN } from "@utils/constants";
import { uploadAvatar, updateUserInfo } from "@api/user";
import * as actionTypes from "@contextProvider/system/ActionTypes";
import { useSystem, useDispatchSystem } from "@contextProvider/ContextProvider";

import snackbarSetting from "@src/alert/Alert";
import HomeAccount from "@components/desktop/account/HomeAccount";
import InfoAccount from "@components/desktop/account/component/InfoAccount";

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: theme.palette.white.main,
    borderRadius: 8,
    boxShadow: "0px 1px 1px rgba(0, 0, 0, 0.05)",
    border: "1px solid #EDF2F7",
    padding: "32px 0 42px 40px",
    marginBottom: 100,
    display: "flex",
  },
  avatarDefault: {
    width: 124,
    height: 124,
    backgroundColor: theme.palette.blue.blueLight8,
    fontSize: 40,
    fontWeight: 600,
  },
  avatarUser: {
    width: 124,
    height: 124,
  },
  iconCamera: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 32,
    height: 32,
    borderRadius: "50%",
    backgroundColor: theme.palette.white.main,
    boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.15)",
  },
}));

const AccountManager = () => {
  const classes = useStyles();
  const { systemReducer } = useSystem();
  const dispatch = useDispatchSystem();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [profilePhoto, setProfilePhoto] = useState(
    !isEmpty(systemReducer.informationUser)
      ? systemReducer.informationUser.profilePhoto
      : ""
  );

  useEffect(() => {
    if (!isEmpty(systemReducer.informationUser)) {
      setProfilePhoto(systemReducer.informationUser.profilePhoto);
    }
  }, [systemReducer.informationUser]);

  const informationUser = systemReducer.informationUser || {};
  const nameUser = informationUser.name || "";
  const nameSplit = nameUser.split(" ") || [];
  const lastNameUSer = nameSplit.pop() || "";

  const storeSchema = yup.object().shape({
    name: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_YOUR_NAME),
    emailInfo: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_EMAIL)
      .email(listString.IDS_MT_TEXT_EMAIL_INVALIDATE),
  });

  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/*",
    maxFiles: 1,
    multiple: false,
    onDrop: (acceptedFiles) => {
      acceptedFiles.forEach((el) => {
        const formData = new FormData();
        formData.append("file", el);
        uploadImage(formData);
      });
    },
  });

  const uploadImage = async (dataDto = {}) => {
    const { data } = await uploadAvatar(dataDto);
    if (data.code === 200) {
      setProfilePhoto(data.photo.link);
    }
  };
  return (
    <HomeAccount>
      <Box className={classes.container}>
        <Box className={classes.userAccount}>
          <Badge
            overlap="circle"
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "right",
            }}
            badgeContent={
              <Box className={classes.iconCamera}>
                <IconCamera />
              </Box>
            }
            {...getRootProps({ className: "dropzone" })}
          >
            <input {...getInputProps()} />
            {!isEmpty(profilePhoto) ? (
              <Avatar
                alt="Remy Sharp"
                src={profilePhoto}
                className={classes.avatarUser}
              />
            ) : (
              <Avatar className={classes.avatarDefault}>
                {lastNameUSer[0] || "H"}
              </Avatar>
            )}
          </Badge>
        </Box>
        <Box pl={5} maxWidth={480}>
          <Formik
            initialValues={{
              name: informationUser.name || "",
              emailInfo: informationUser.emailInfo || "",
              phoneInfo: informationUser.phoneInfo || "",
              address: informationUser.address || "",
              return_user_info: true,
              access_token: cookie.get(TOKEN),
            }}
            onSubmit={async (values) => {
              try {
                const dataDTO = {
                  ...values,
                  profile_photo: profilePhoto,
                };
                const { data } = await updateUserInfo(dataDTO);
                if (data.code === 200) {
                  enqueueSnackbar(
                    "Thay đổi thông tin thành công!",
                    snackbarSetting((key) => closeSnackbar(key), {
                      color: "success",
                    })
                  );
                  dispatch({
                    type: actionTypes.SET_INFORMATION_USER,
                    payload: data.data,
                  });
                } else {
                  data?.message &&
                    enqueueSnackbar(
                      data?.message,
                      snackbarSetting((key) => closeSnackbar(key), {
                        color: "error",
                      })
                    );
                }
              } catch (error) {}
            }}
            validationSchema={storeSchema}
          >
            {() => {
              return (
                <Form>
                  <InfoAccount />
                </Form>
              );
            }}
          </Formik>
        </Box>
      </Box>
    </HomeAccount>
  );
};

export default AccountManager;
