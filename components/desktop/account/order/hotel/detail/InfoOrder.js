import clsx from "clsx";
import moment from "moment";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { Box, Grid, Typography, Dialog } from "@material-ui/core";

import { isEmpty, paymentStatus } from "@utils/helpers";
import { getBookingDetails, getPaymentInfo } from "@api/hotels";
import { makeStyles } from "@material-ui/styles";
import {
  listString,
  flightPaymentMethodCode,
  prefixUrlIcon,
  listIcons,
} from "@utils/constants";
import * as actionTypes from "@contextProvider/system/ActionTypes";
import { useDispatchSystem } from "@contextProvider/ContextProvider";
import {
  IconBack,
  IconStar,
  IconUser2,
  IconDot,
  IconBed,
  IconInfo,
  IconBreakFast,
  IconCancelPolicy,
  IconCalenderOrder,
  IconOrderCode,
} from "@public/icons";

import Image from "@src/image/Image";
import HomeAccount from "@components/desktop/account/HomeAccount";
import InfoPayment from "@components/desktop/account/order/hotel/detail/InfoPayment";
import PolicyRoomModal from "@components/desktop/hotels/details/components/PolicyRoomModal";
import Vote from "@components/common/desktop/vote/Vote";

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
    backgroundColor: theme.palette.white.main,
    borderRadius: 8,
    boxShadow: "0px 3px 3px rgba(0, 0, 0, 0.05)",
    padding: "20px 16px 40px",
    lineHeight: "17px",
  },
  topContainer: {
    display: "flex",
    alignItems: "center",
    color: theme.palette.primary.main,
    "&:hover": {
      cursor: "pointer",
    },
    paddingBottom: 20,
  },
  iconBack: {
    stroke: theme.palette.primary.main,
  },
  boxItemOder: {
    backgroundColor: theme.palette.gray.grayLight26,
    borderRadius: 8,
    padding: "16px 0 0 16px",
    display: "flex",
    flexDirection: "column",
    minHeight: 80,
    position: "relative",
  },
  paymentStatus: {
    borderRadius: 4,
    padding: "4px 8px",
    width: "fit-content",
    color: theme.palette.white.main,
  },
  imageHotel: {
    width: 136,
    height: 102,
    borderRadius: 8,
  },
  hotelName: {
    display: "flex",
    flexDirection: "column",
  },
  hotelName: {
    fontSize: 18,
    fontWeight: 600,
    lineHeight: "21px",
  },
  wrapRatingStar: {
    marginTop: 10,
    marginBottom: 12,
    display: "flex",
  },
  itemDateRoom: {
    display: "flex",
    flexDirection: "column",
    borderLeft: "1px solid #E2E8F0",
    padding: "0 40px 0 10px",
  },
  title: {
    fontSize: 16,
    fontWeight: 600,
    lineHeight: "19px",
  },
  title2: {
    fontSize: 18,
    fontWeight: 600,
    lineHeight: "21px",
    minWidth: 150,
  },
  iconUser2: {
    width: 14,
    height: 14,
    marginRight: 10,
  },
  iconBed: {
    width: 16,
    height: 16,
    marginRight: 10,
  },
  textCancelPolicy: {
    fontSize: 14,
    lineHeight: "17px",
    fontWeight: "normal",
    color: theme.palette.gray.grayDark8,
    marginLeft: 6,
  },
  textFreeBreakFast: {
    color: theme.palette.green.greenLight7,
  },
  iconBreakFast: {
    stroke: theme.palette.gray.grayDark8,
  },
  freeCancelPolicy: {
    stroke: theme.palette.green.greenLight7,
  },
  infoActive: {
    marginLeft: 4,
    stroke: theme.palette.green.greenLight7,
  },
  boxInfoBooking: {
    borderTop: "1px solid #EDF2F7",
    marginTop: 24,
    width: "100%",
  },
  itemPriceBooking: {
    borderBottom: "1px solid #EDF2F7",
    padding: "12px 0",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  wrapPromo: {
    height: 24,
    whiteSpace: "nowrap",
    display: "flex",
    alignItems: "center",
  },
  promo: {
    border: `1px solid ${theme.palette.gray.grayLight24}`,
    padding: "4px 6px",
    marginLeft: 8,
    borderRadius: 4,
  },
  textPromo: {
    fontSize: 14,
    lineHeight: "17px",
    display: "flex",
  },
  textUppercase: {
    textTransform: "uppercase",
  },
  totalPrice: {
    borderBottom: "none",
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
  },
  iconBoxItemOder: {
    position: "absolute",
    bottom: 0,
    right: 0,
  },
  iconStatusOrderHotel: {
    width: 86,
    height: 64,
  },
}));
const arrStar = [1, 2, 3, 4, 5];
const listTypeModal = {
  MODAL_CANCEL_POLICY: "MODAL_CANCEL_POLICY",
};
let interval = null;
const InfoOrder = () => {
  const router = useRouter();
  const classes = useStyles();
  const [infoOrder, setInfoOrder] = useState({});
  const [infoPayment, setInfoPayment] = useState({});
  const dispatch = useDispatchSystem();
  const [timer, setTimer] = useState({
    hours: "00",
    minutes: "00",
    seconds: "00",
  });
  const [isShowCountdown, setIsShowCountdown] = useState(false);
  const [visibleDrawerType, setVisibleDrawerType] = useState("");

  const roomBookings = !isEmpty(infoOrder.roomBookings)
    ? infoOrder.roomBookings
    : [];
  const roomBooking = !isEmpty(roomBookings) ? roomBookings[0] : {};

  useEffect(() => {
    const startTime = moment();
    const endTime = !isEmpty(infoOrder)
      ? moment(infoOrder?.expiredTime, "YYYY-MM-DD HH:mm:ss")
      : moment();
    const diffTime = endTime.valueOf() - startTime.valueOf();
    let duration = moment.duration(diffTime, "milliseconds");
    if (diffTime > 0) {
      setIsShowCountdown(true);
      interval = setInterval(() => {
        duration = moment.duration(duration - 1000, "milliseconds");
        setTimer({
          hours:
            duration.hours() < 10 ? `0${duration.hours()}` : duration.hours(),
          minutes:
            duration.minutes() < 10
              ? `0${duration.minutes()}`
              : duration.minutes(),
          seconds:
            duration.seconds() < 10
              ? `0${duration.seconds()}`
              : duration.seconds(),
        });
        if (
          duration.hours() === 0 &&
          duration.minutes() === 0 &&
          duration.seconds() === 0
        ) {
          setIsShowCountdown(false);
          clearInterval(interval);
        }
      }, 1000);
    }
    return () => {
      clearInterval(interval);
    };
  }, [infoOrder]);

  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };

  useEffect(() => {
    fetchOrderDetail();
  }, []);

  const fetchOrderDetail = async () => {
    try {
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: true,
      });
      const { data } = await getBookingDetails({
        id: router.query.id,
      });
      if (data.code === 200) {
        setInfoOrder(data.data || {});
        if (
          data.data.paymentMethodCode ===
          flightPaymentMethodCode.BANK_TRANSFER_2
        ) {
          fetchPaymentInfo({
            bookingCode: data.data.orderCode,
            paymentMethodId: data.data.paymentMethodId,
          });
        } else {
          dispatch({
            type: actionTypes.SET_VISIBLE_FETCHING,
            payload: false,
          });
        }
      } else {
        dispatch({
          type: actionTypes.SET_VISIBLE_FETCHING,
          payload: false,
        });
      }
    } catch (error) {
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
    }
  };

  const fetchPaymentInfo = async (dataQuery = {}) => {
    try {
      const { data } = await getPaymentInfo(dataQuery);
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
      if (data.code === 200) {
        setInfoPayment(data.data || {});
      }
    } catch (error) {
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
    }
  };

  const handleBack = () => {
    router.back();
  };

  const getListBed = (bedTypes = []) => {
    let listBed = "";
    bedTypes.forEach((el, index) => {
      listBed = `${listBed}${index !== 0 ? " hoặc " : ""}${el}`;
    });
    return listBed;
  };

  const getNight = () => {
    let checkIn = moment(infoOrder.checkIn, "DD-MM-YYYY");
    let checkOut = moment(infoOrder.checkOut, "DD-MM-YYYY");
    if (checkIn && checkOut) {
      return Math.abs(
        moment(checkIn)
          .startOf("day")
          .diff(checkOut.startOf("day"), "days")
      );
    }
    return 1;
  };

  // tong gia chua app ma khuyen mai
  const totalPrice = () => {
    if (infoOrder.numRooms && infoOrder.price && getNight()) {
      return infoOrder.numRooms * getNight() * infoOrder.price;
    } else {
      return 0;
    }
  };

  // tong gia cac dem
  const nightPrice = () => {
    if (infoOrder.numRooms && infoOrder.basePrice && getNight()) {
      return infoOrder.numRooms * getNight() * infoOrder.basePrice;
    } else {
      return 0;
    }
  };

  // giam gia makup
  const getDiscount = () => {
    return roomBookings
      .map((el) => Math.abs(el.totalDiscount))
      .reduce((a, b) => a + b, 0);
  };

  // thue va phi dich vu khach san
  const vatPrice = () => {
    return totalPrice() - nightPrice() + getDiscount() - totalSurcharge();
  };

  // phu phi
  const totalSurcharge = () => {
    return roomBookings
      .map((el) => el.totalSurcharge)
      .reduce((a, b) => a + b, 0);
  };

  const bookingDate = !isEmpty(infoOrder.bookingDate)
    ? moment(infoOrder?.bookingDate, "YYYY-MM-DD HH:mm:ss").format(
        "DD/MM/YYYY HH:mm:ss"
      )
    : "";

  const hotelDetail = !isEmpty(infoOrder) ? infoOrder.hotel : {};
  const customer = !isEmpty(infoOrder) ? infoOrder.customer : {};
  const invoiceInfo = !isEmpty(infoOrder) ? infoOrder.invoiceInfo : {};
  let checkIn = moment(infoOrder.checkIn, "DD-MM-YYYY");
  let checkOut = moment(infoOrder.checkOut, "DD-MM-YYYY");

  let expiredTime = !isEmpty(infoOrder?.expiredTime)
    ? moment(infoOrder?.expiredTime, "YYYY-MM-DD HH:mm:ss")
    : moment();

  const getTextShortCancelPolicies = () => {
    if (roomBooking.freeCancellation) {
      return "Miễn phí hoàn hủy";
    } else if (roomBooking.refundable) {
      return "Có thể hoàn hủy";
    } else {
      return "Không hoàn hủy";
    }
  };

  return (
    <HomeAccount>
      {!isEmpty(infoOrder) ? (
        <Box className={classes.container}>
          <Box onClick={handleBack} className={classes.topContainer}>
            <IconBack className={`svgFillAll ${classes.iconBack}`} />
            <Box component="span" pl={12 / 8}>
              {listString.IDS_MT_TEXT_BACK_MY_ORDER_HOTEL}
            </Box>
          </Box>
          <Grid container spacing={2}>
            <Grid item lg={3} md={3} sm={3} xs={3}>
              <Box className={classes.boxItemOder}>
                <Box className={classes.iconBoxItemOder}>
                  <IconOrderCode />
                </Box>
                <Box component="span" pb={1}>
                  {listString.IDS_TEXT_FLIGHT_ORDER_CODE}
                </Box>
                <Box
                  component="span"
                  fontSize={18}
                  fontWeight={600}
                  lineHeight="21px"
                  color="primary.main"
                >
                  {infoOrder.orderCode}
                </Box>
              </Box>
            </Grid>
            <Grid item lg={3} md={3} sm={3}>
              <Box className={classes.boxItemOder}>
                <Box className={classes.iconBoxItemOder}>
                  <IconOrderCode />
                </Box>
                <Box component="span" pb={1}>
                  {listString.IDS_TEXT_FLIGHT_ROOM_CODE}
                </Box>
                <Box
                  component="span"
                  fontSize={18}
                  fontWeight={600}
                  lineHeight="21px"
                  color="secondary.main"
                >
                  {infoOrder.partnerBookingCode || "Chưa có mã"}
                </Box>
              </Box>
            </Grid>
            <Grid item lg={3} md={3} sm={3}>
              <Box className={classes.boxItemOder}>
                <Box className={classes.iconBoxItemOder}>
                  <Image
                    srcImage={`${prefixUrlIcon}${listIcons.IconStatusOrderHotel}`}
                    className={classes.iconStatusOrderHotel}
                  />
                </Box>
                <Box component="span" pb={1}>
                  Trạng thái
                  {paymentStatus(infoOrder).isShowCountDown && isShowCountdown && (
                    <Box
                      pl={2 / 8}
                      component="span"
                      color="orange.main"
                      position="absolute"
                    >
                      <Box component="span">{`Còn `}</Box>
                      <Box component="span">{timer.hours}</Box>
                      <Box
                        component="span"
                        className={classes.itemBetween}
                      >{` giờ `}</Box>
                      <Box component="span">{timer.minutes}</Box>
                      <Box
                        component="span"
                        className={classes.itemBetween}
                      >{` phút `}</Box>
                      <Box component="span">{timer.seconds}</Box>
                      <Box
                        component="span"
                        className={classes.itemBetween}
                      >{` giây `}</Box>
                    </Box>
                  )}
                </Box>
                <Box
                  component="span"
                  className={classes.paymentStatus}
                  bgcolor={paymentStatus(infoOrder).color}
                >
                  {" "}
                  {paymentStatus(infoOrder).title}
                </Box>
              </Box>
            </Grid>
            <Grid item lg={3} md={3} sm={3}>
              <Box className={classes.boxItemOder}>
                <Box className={classes.iconBoxItemOder}>
                  <img src={`${prefixUrlIcon}${listIcons.IconCalenderOrder}`} />
                </Box>
                <Box component="span" pb={1}>
                  {listString.IDS_MT_TEXT_DATE_BOOKING}
                </Box>
                <Box component="span" fontSize={16} lineHeight="19px">
                  {bookingDate}
                </Box>
              </Box>
            </Grid>
          </Grid>
          {!isEmpty(infoPayment) && paymentStatus(infoOrder).isShowCountDown && (
            <Box pt={2}>
              <Box color="orange.main">
                <Box component="span">{`${
                  listString.IDS_TEXT_FLIGHT_BOOKING_BANK_TRANSFER_2
                } ${expiredTime.format("HH:mm")}, ${expiredTime
                  .locale("vi_VN")
                  .format("ddd")
                  .replace("T2", "Thứ hai")
                  .replace("T3", "Thứ ba")
                  .replace("T4", "Thứ tư")
                  .replace("T5", "Thứ năm")
                  .replace("T6", "Thứ sáu")
                  .replace("T7", "Thứ bảy")
                  .replace("CN", "Chủ Nhật")}, ngày ${expiredTime.format(
                  "DD/MM/YYYY"
                )}`}</Box>
              </Box>
              <InfoPayment infoPayment={infoPayment} />
            </Box>
          )}
          <Box display="flex" pt={3}>
            <Image
              srcImage={hotelDetail?.thumbnail?.image.small}
              className={classes.imageHotel}
              borderRadiusProp="8px"
            />
            <Box
              display="flex"
              flexDirection="column"
              width="calc(100% - 16px)"
              pl={2}
              ml={2}
            >
              <Box component="span" className={classes.hotelName}>
                {hotelDetail?.name}
              </Box>
              <Box className={classes.wrapRatingStar}>
                {!isEmpty(hotelDetail.starNumber) && (
                  <Vote
                    maxValue={hotelDetail.starNumber}
                    value={hotelDetail.starNumber}
                  />
                )}
              </Box>
              <Box display="flex">
                <Box className={classes.itemDateRoom}>
                  <Box component="span" fontWeight={600} pb={5 / 8}>
                    {listString.IDS_MT_TEXT_CHECK_IN_ROOM}
                  </Box>
                  <Box component="span" className={classes.timeCheckIn}>
                    {hotelDetail.checkInTime}
                    {", "}
                    {checkIn
                      ? checkIn
                          .locale("vi_VN")
                          .format("ddd")
                          .replace("T", "Thứ ")
                          .replace("CN", "Chủ Nhật")
                      : "-"}
                    <Box component="span">
                      {", "}
                      {checkIn.format("DD")} tháng {checkIn.format("MM")}
                    </Box>
                  </Box>
                </Box>
                <Box className={classes.itemDateRoom}>
                  <Box component="span" fontWeight={600} pb={5 / 8}>
                    {listString.IDS_MT_TEXT_CHECK_OUT_ROOM}
                  </Box>
                  <Box component="span" className={classes.timeCheckIn}>
                    {hotelDetail.checkOutTime}
                    {", "}
                    {checkOut
                      ? checkOut
                          .locale("vi_VN")
                          .format("ddd")
                          .replace("T", "Thứ ")
                          .replace("CN", "Chủ Nhật")
                      : "-"}
                    <Box component="span">
                      {", "}
                      {checkOut.format("DD")} tháng {checkIn.format("MM")}
                    </Box>
                  </Box>
                </Box>
                <Box className={classes.itemDateRoom}>
                  <Box component="span" fontWeight={600} pb={5 / 8}>
                    {listString.IDS_MT_TEXT_NUMBER_NIGHT}
                  </Box>
                  {checkIn && checkOut
                    ? Math.abs(
                        moment(checkIn)
                          .startOf("day")
                          .diff(checkOut.startOf("day"), "days")
                      ) > 9
                      ? Math.abs(
                          moment(checkIn)
                            .startOf("day")
                            .diff(checkOut.startOf("day"), "days")
                        )
                      : `0${Math.abs(
                          moment(checkIn)
                            .startOf("day")
                            .diff(checkOut.startOf("day"), "days")
                        )}`
                    : "-"}
                </Box>
              </Box>
              <Box component="span" className={classes.title} pt={2} pb={1}>
                {`${infoOrder?.numRooms}x`} {roomBooking?.name}
              </Box>
              <Box display="flex" alignItems="center" pb={1}>
                <IconUser2 className={classes.iconUser2} />
                <Box component="span">
                  {`${parseInt(infoOrder?.numAdult) +
                    parseInt(infoOrder?.numChildren)} người`}
                </Box>
              </Box>
              {!isEmpty(roomBooking.bedTypes) && (
                <Box display="flex" alignItems="center" pb={1}>
                  <IconBed className={classes.iconBed} />{" "}
                  <Box component="span">
                    {getListBed(roomBooking?.bedTypes)}
                  </Box>
                </Box>
              )}

              {roomBooking?.freeBreakfast ? (
                <Box style={{ display: "flex", alignItems: "center" }}>
                  <IconBreakFast />
                  <Typography
                    className={clsx(
                      classes.textCancelPolicy,
                      classes.textFreeBreakFast
                    )}
                  >
                    {listString.IDS_MT_TEXT_ROOM_FREE_BREAK_FAST}
                  </Typography>
                </Box>
              ) : (
                <Box style={{ display: "flex", alignItems: "center" }}>
                  <IconBreakFast
                    className={`svgFillAll ${classes.iconBreakFast}`}
                  />
                  <Typography className={classes.textCancelPolicy}>
                    {listString.IDS_MT_TEXT_ROOM_NO_FREE_BREAK_FAST}
                  </Typography>
                </Box>
              )}
              {roomBooking.freeCancellation ? (
                <Box
                  pt={1}
                  onClick={toggleDrawer(listTypeModal.MODAL_CANCEL_POLICY)}
                >
                  <Box style={{ display: "flex", alignItems: "center" }}>
                    <IconCancelPolicy
                      className={`svgFillAll ${classes.freeCancelPolicy}`}
                    />
                    <Typography
                      className={classes.textCancelPolicy}
                      style={{ color: "#48BB78" }}
                    >
                      {listString.IDS_MT_TEXT_FREE_CANCEL}
                    </Typography>
                    <IconInfo className={`svgFillAll ${classes.infoActive}`} />
                  </Box>
                  <Typography variant="body2" style={{ marginLeft: "22px" }}>
                    {!isEmpty(roomBooking.hotelCancellationPolicies) &&
                      roomBooking.hotelCancellationPolicies.map((el, i) => (
                        <Box
                          component="span"
                          className={classes.dateFreeCancel}
                          key={i}
                        >
                          {el.price === el.refundAmount &&
                            `Trước ${el.dateTo.split(" ")[0]}`}
                        </Box>
                      ))}
                  </Typography>
                </Box>
              ) : (
                <Box
                  style={{ display: "flex", alignItems: "center" }}
                  pt={1}
                  onClick={() => {
                    if (roomBooking.refundable) {
                      setVisibleDrawerType(listTypeModal.MODAL_CANCEL_POLICY);
                    }
                  }}
                >
                  <IconCancelPolicy />
                  <Typography className={classes.textCancelPolicy}>
                    {getTextShortCancelPolicies()}
                  </Typography>
                  {roomBooking.refundable && (
                    <IconInfo className={`svgFillAll ${classes.info}`} />
                  )}
                </Box>
              )}
              <Box className={classes.boxInfoBooking}>
                <Box display="flex" pt={3}>
                  <Box component="span" className={classes.title2}>
                    {listString.IDS_MT_TEXT_INFO_CONTACT}
                  </Box>
                  <Box display="flex" flexDirection="column" pl={140 / 8}>
                    <Box component="span">
                      {`${listString.IDS_MT_TEXT_CONTACT_NAME_SIMPLE}:`}
                    </Box>
                    <Box component="span" p="8px 0">{`Điện thoại:`}</Box>
                    <Box component="span">{`Email:`}</Box>
                  </Box>
                  <Box
                    display="flex"
                    flexDirection="column"
                    pl={12 / 8}
                    fontWeight={600}
                  >
                    <Box component="span">{customer.name}</Box>
                    <Box component="span" p="8px 0">
                      {customer.phone}
                    </Box>
                    <Box component="span">{customer.email}</Box>
                  </Box>
                </Box>
                {(roomBooking.isHighFloor ||
                  roomBooking.noSmoking ||
                  !isEmpty(roomBooking.specialRequest) ||
                  !isEmpty(invoiceInfo?.taxNumber)) && (
                  <Box display="flex" pt={4}>
                    <Box component="span" className={classes.title2}>
                      {listString.IDS_MT_TEXT_REQUEST_SPECIAL}
                    </Box>
                    <Box display="flex" flexDirection="column" pl={140 / 8}>
                      {roomBooking.isHighFloor && roomBooking.noSmoking && (
                        <Box component="span" fontWeight={600}>
                          {listString.IDS_MT_TEXT_REQUEST_SPECIAL}
                        </Box>
                      )}
                      {roomBooking.isHighFloor && (
                        <Box display="flex" alignItems="center">
                          <IconDot />
                          <Box component="span" lineHeight="24px" pl={1}>
                            {listString.IDS_MT_ROOM_HIGH_LEVEL}
                          </Box>
                        </Box>
                      )}
                      {roomBooking.noSmoking && (
                        <Box display="flex" alignItems="center">
                          <IconDot />
                          <Box component="span" lineHeight="24px" pl={1}>
                            {listString.IDS_MT_NO_SMOKE_ROOM}
                          </Box>
                        </Box>
                      )}
                      {!isEmpty(roomBooking.specialRequest) && (
                        <Box display="flex" alignItems="center">
                          <IconDot />
                          <Box component="span" lineHeight="24px" pl={1}>
                            {`${listString.IDS_MT_TEXT_BOOKING_REQUEST_PRIVATE}:`}
                          </Box>
                          <Box component="span" lineHeight="24px" pl={1}>
                            {roomBooking.specialRequest}
                          </Box>
                        </Box>
                      )}

                      {!isEmpty(invoiceInfo?.taxNumber) && (
                        <>
                          <Box component="span" fontWeight={600}>
                            {listString.IDS_MT_TEXT_INVOICE}
                          </Box>
                          <Box display="flex" pt={1}>
                            <Box component="span" color="gray.grayDark8">
                              {`${listString.IDS_MT_TEXT_COMPANY_NAME}:`}
                            </Box>
                            <Box component="span" pl={4 / 8}>
                              {invoiceInfo.companyName}
                            </Box>
                          </Box>
                          <Box display="flex" pt={1}>
                            <Box component="span" color="gray.grayDark8">
                              {`${listString.IDS_MT_TEXT_CODE_INVOICE}:`}
                            </Box>
                            <Box component="span" pl={4 / 8}>
                              {invoiceInfo.taxNumber}
                            </Box>
                          </Box>
                          <Box display="flex" pt={1}>
                            <Box component="span" color="gray.grayDark8">
                              {`${listString.IDS_MT_TEXT_ADDRESS_COMPANY}:`}
                            </Box>
                            <Box component="span" pl={4 / 8}>
                              {invoiceInfo.companyAddress}
                            </Box>
                          </Box>
                          <Box display="flex" pt={1}>
                            <Box component="span" color="gray.grayDark8">
                              {`${listString.IDS_MT_TEXT_ADDRESS_INVOICE}:`}
                            </Box>
                            <Box component="span" pl={4 / 8}>
                              {invoiceInfo.recipientAddress}
                            </Box>
                          </Box>
                          <Box display="flex" pt={1}>
                            <Box component="span" color="gray.grayDark8">
                              {`${listString.IDS_TEXT_PLACEHOLDER_INVOICE_CUSTOMER_NAME}:`}
                            </Box>
                            <Box component="span" pl={4 / 8}>
                              {invoiceInfo.recipientName}
                            </Box>
                          </Box>
                          <Box display="flex" pt={1}>
                            <Box component="span" color="gray.grayDark8">
                              {`${listString.IDS_MT_TEXT_PHONE_NUMBER}:`}
                            </Box>
                            <Box component="span" pl={4 / 8}>
                              {invoiceInfo.recipientPhone}
                            </Box>
                          </Box>
                          <Box display="flex" pt={1}>
                            <Box component="span" color="gray.grayDark8">
                              {`${listString.IDS_MT_TEXT_EMAIL}:`}
                            </Box>
                            <Box component="span" pl={4 / 8}>
                              {invoiceInfo.recipientEmail}
                            </Box>
                          </Box>
                        </>
                      )}
                    </Box>
                  </Box>
                )}

                <Box display="flex" pt={4}>
                  <Box component="span" className={classes.title2} pt={12 / 8}>
                    {listString.IDS_MT_TEXT_PRICE_DETAIL}
                  </Box>
                  <Box
                    display="flex"
                    flexDirection="column"
                    width="calc(100% - 140px)"
                    pl={140 / 8}
                  >
                    <Box className={classes.itemPriceBooking}>
                      <Box component="span">{`${
                        infoOrder?.numRooms
                      } phòng x ${getNight()} đêm`}</Box>
                      <Box component="span">{`${nightPrice().formatMoney()}đ`}</Box>
                    </Box>

                    {totalSurcharge() > 0 && (
                      <Box className={classes.itemPriceBooking}>
                        <Box component="span">{`Phụ phí`}</Box>
                        {`${totalSurcharge().formatMoney()}đ`}
                      </Box>
                    )}

                    {getDiscount() > 0 && (
                      <Box
                        className={classes.itemPriceBooking}
                        color="green.greenLight7"
                      >
                        <Box component="span">{`Chúng tôi khớp giá, giảm thêm`}</Box>
                        {`-${getDiscount().formatMoney()}đ`}
                      </Box>
                    )}

                    {vatPrice() > 2 && (
                      <Box className={classes.itemPriceBooking}>
                        <Box component="span">{`${
                          infoOrder.includedVAT ? "Thuế và phí " : "Phí "
                        }dịch vụ khách sạn`}</Box>
                        {`${vatPrice().formatMoney()}đ`}
                      </Box>
                    )}

                    <Box
                      className={classes.itemPriceBooking}
                      alignItems="center"
                    >
                      <Box display="flex" alignItems="center">
                        {listString.IDS_TEXT_VOUCHER}{" "}
                        {!isEmpty(infoOrder.promotionCode) && (
                          <Box component="span" className={classes.promo}>
                            <Typography
                              component="span"
                              className={classes.textPromo}
                            >
                              {infoOrder?.promotionCode?.toUpperCase()}
                            </Typography>
                          </Box>
                        )}
                      </Box>
                      <Box
                        component="span"
                        color="green.greenLight7"
                        className={classes.textPromo}
                      >
                        {infoOrder?.discount
                          ? `-${infoOrder?.discount?.formatMoney()}đ`
                          : "0đ"}
                      </Box>
                    </Box>

                    <Box className={classes.itemPriceBooking}>
                      <Box component="span">{`Phí giao dịch ngân hàng`}</Box>
                      <Box component="span">{`${infoOrder?.paymentMethodFee?.formatMoney()}đ`}</Box>
                    </Box>

                    <Box
                      className={clsx(
                        classes.itemPriceBooking,
                        classes.totalPrice
                      )}
                    >
                      <Box component="span">
                        {listString.IDS_MT_TEXT_TOTAL_PRICE}
                      </Box>
                      <Box component="span">
                        {`${infoOrder?.finalPrice?.formatMoney()}đ`}
                      </Box>
                    </Box>
                    {!infoOrder.includedVAT && (
                      <Box
                        fontSize={12}
                        lineHeight="14px"
                        color="gray.grayDark8"
                        textAlign="end"
                      >
                        Giá chưa bao gồm VAT
                      </Box>
                    )}
                  </Box>
                </Box>
                <Box display="flex" pt={4}>
                  <Box component="span" className={classes.title2} pt={12 / 8}>
                    {listString.IDS_MT_TEXT_PAYMENT}
                  </Box>
                  <Box
                    display="flex"
                    flexDirection="column"
                    width="calc(100% - 140px)"
                    pl={140 / 8}
                  >
                    <Box
                      className={classes.itemPriceBooking}
                      style={{ border: "none" }}
                    >
                      <Box component="span">{`${listString.IDS_MT_TEXT_PAYMENT_MENTHODS}`}</Box>
                      <Box
                        component="span"
                        textAlign="end"
                      >{`${infoOrder.paymentMethod}`}</Box>
                    </Box>
                    <Box
                      className={classes.itemPriceBooking}
                      style={{ border: "none" }}
                    >
                      <Box component="span">{`${listString.IDS_MT_TEXT_STATUS}`}</Box>

                      <Box
                        component="span"
                        textAlign="end"
                        p="6px 12px"
                        color={paymentStatus(infoOrder).paymentColor}
                        style={{
                          backgroundColor: paymentStatus(infoOrder)
                            .bgPaymentColor,
                        }}
                      >{`${paymentStatus(infoOrder).paymentStatus}`}</Box>
                    </Box>
                  </Box>
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      ) : (
        <Box minHeight={600}></Box>
      )}
      <Dialog
        maxWidth="sm"
        open={visibleDrawerType === listTypeModal.MODAL_CANCEL_POLICY}
        onClose={() => setVisibleDrawerType("")}
      >
        <PolicyRoomModal
          item={roomBooking}
          handleClose={() => setVisibleDrawerType("")}
        />
      </Dialog>
    </HomeAccount>
  );
};

export default InfoOrder;
