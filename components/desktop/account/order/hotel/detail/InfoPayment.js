import { Box, makeStyles, Typography } from "@material-ui/core";

import { isEmpty } from "@utils/helpers";
import { IconCopy } from "@public/icons";
import { listString } from "@utils/constants";

import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  wrapBox: {
    borderRadius: 8,
    border: `1px dashed ${theme.palette.gray.grayLight25}`,
    background: theme.palette.gray.grayLight26,
    marginTop: 12,
  },
  wrapContent: {
    margin: "0 12px",
  },
  title: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    minWidth: 300,
  },
  wrapBank: {
    display: "flex",
    fontSize: 14,
    lineHeight: "17px",
    margin: "12px 0 12px 0",
  },
  bankText: {
    fontWeight: 600,
    whiteSpace: "nowrap",
  },
  infoBank: {
    marginBottom: 12,
  },
  branchText: {
    whiteSpace: "break-spaces",
    textAlign: "end",
  },
  copy: {
    display: "flex",
    color: theme.palette.blue.blueLight8,
    cursor: "pointer",
    paddingLeft: 12,
  },
  bankItem: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.gray.grayLight23}`,
    height: 24,
    width: 48,
    backgroundColor: theme.palette.white.main,
    marginLeft: 12,
  },
  iconBank: {
    width: "100%",
    height: "100%",
    objectFit: "contain",
  },
}));

const InfoPayment = ({ infoPayment = {} }) => {
  const classes = useStyles();
  const transferOptions = infoPayment.transferOptions || [];
  const transferInfo = infoPayment.transferInfo || {};
  return (
    <Box className={classes.wrapBox}>
      <Box className={classes.wrapContent}>
        <Box className={classes.infoBank}>
          <Typography variant="subtitle2" style={{ marginTop: 12 }}>
            {listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER}
          </Typography>
          {transferOptions.map((el, index) => (
            <Box key={index}>
              <Box className={classes.wrapBank} alignItems="center">
                <Box className={classes.title}>
                  {listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_BANK}
                </Box>
                <Box display="flex" alignItems="center">
                  <Box component="span" className={classes.bankText}>
                    {el.bankName}
                  </Box>
                  <Box className={classes.bankItem}>
                    <Image
                      srcImage={el.bankLogo}
                      className={classes.iconBank}
                    />
                  </Box>
                </Box>
              </Box>
              <Box className={classes.wrapBank}>
                <Box className={classes.title}>
                  {listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_NUMBER_ACCOUNT}
                </Box>
                <Box display="flex" alignItems="center">
                  <Typography
                    className={classes.branchText}
                    variant="subtitle2"
                  >
                    {el.accountNumber}
                  </Typography>
                  <Box
                    className={classes.copy}
                    onClick={() => {
                      navigator.clipboard.writeText(el.accountNumber);
                    }}
                  >
                    <IconCopy />
                    <Box component="span" pl={4 / 8}>
                      {listString.IDS_MT_TEXT_INFO_COPPY}
                    </Box>
                  </Box>
                </Box>
              </Box>
              <Box className={classes.wrapBank}>
                <Box className={classes.title}>
                  {listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_NAME_ACCOUNT}
                </Box>
                <Typography className={classes.branchText} variant="subtitle2">
                  {el.accountName}
                </Typography>
              </Box>
            </Box>
          ))}
        </Box>

        <Box className={classes.wrapBank}>
          <Box className={classes.title}>
            {listString.IDS_MT_TEXT_AMOUNT_MONEY}
          </Box>
          <Box display="flex" alignItems="center">
            {!isEmpty(transferInfo) && (
              <Typography className={classes.branchText} variant="subtitle2">
                {`${transferInfo.totalAmount.formatMoney()}đ`}
              </Typography>
            )}
            <Box
              className={classes.copy}
              onClick={() => {
                navigator.clipboard.writeText(transferInfo.totalAmount);
              }}
            >
              <IconCopy />
              <Box component="span" pl={4 / 8}>
                {listString.IDS_MT_TEXT_INFO_COPPY}
              </Box>
            </Box>
          </Box>
        </Box>

        <Box className={classes.wrapBank}>
          <Box className={classes.title}>
            {listString.IDS_MT_TEXT_CONTENT_TRANSFER}
          </Box>
          <Box display="flex" alignItems="center">
            <Typography
              className={classes.branchText}
              variant="subtitle2"
              style={{ textAlign: "right", textTransform: "uppercase" }}
            >
              {transferInfo.message}
            </Typography>
            <Box
              className={classes.copy}
              onClick={() => {
                navigator.clipboard.writeText(transferInfo.message);
              }}
              style={{ border: "none" }}
            >
              <IconCopy />
              <Box component="span" pl={4 / 8}>
                {listString.IDS_MT_TEXT_INFO_COPPY}
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default InfoPayment;
