import clsx from "clsx";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { Box, Typography, Popover } from "@material-ui/core";

import { isEmpty } from "@utils/helpers";
import { getMyBooking } from "@api/hotels";
import { makeStyles } from "@material-ui/styles";
import { listString, routeStatic } from "@utils/constants";
import * as actionTypes from "@contextProvider/system/ActionTypes";
import { useDispatchSystem } from "@contextProvider/ContextProvider";
import { IconDropDown, IconActive } from "@public/icons";

import Image from "@src/image/Image";
import ButtonComponent from "@src/button/Button";
import UsePagination from "@src/pagination/Pagination";
import HomeAccount from "@components/desktop/account/HomeAccount";
import HotelItemOder from "@components/desktop/account/order/hotel/HotelItemOder";

const useStyles = makeStyles((theme) => ({
  iconArrowDownToggle: {
    stroke: theme.palette.black.black3,
    marginLeft: 5,
  },
  viewMessageReview: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.black.black3,
    display: "flex",
    alignItems: "center",
  },
  boxItem: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    height: 30,
    justifyContent: "space-between",
    padding: "6px 8px",
    borderRadius: 8,
    "&:hover": {
      backgroundColor: theme.palette.gray.grayLight22,
      cursor: "pointer",
    },
  },
  boxItemActive: {
    color: theme.palette.blue.blueLight8,
  },
  hideBorder: {
    borderBottom: "none",
  },
  textSort: {
    fontSize: 14,
    lineHeight: "17px",
  },
  popoverPaper: {
    borderRadius: 8,
    padding: 8,
  },
  paginationContainer: {
    width: "100%",
    display: "flex",
    justifyContent: "flex-end",
    margin: "32px 0",
  },
  orderEmptyFirst: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "calc(100vh - 230px)",
    color: theme.palette.black.black3,
    fontSize: 14,
  },
  imageEmpty: {
    width: 249,
    height: 210,
  },
}));

const listFilter = [
  {
    code: "all",
    label: "Tất cả",
  },
  {
    code: "success",
    label: "Thành công",
  },
  {
    code: "waiting",
    label: "Đang chờ thanh toán",
  },
  {
    code: "holding",
    label: "Đang giữ chỗ",
  },
  {
    code: "refunded",
    label: "Hoàn tiền",
  },
  {
    code: "fail",
    label: "Thất bại",
  },
];
const pageSize = 10;

const listStatus = {
  INIT_STATE: "INIT_STATE",
  FIRST_REQUEST: "FIRST_REQUEST",
  LAST_REQUEST: "LAST_REQUEST",
};
const HotelBooking = () => {
  const router = useRouter();
  const classes = useStyles();
  const [listBookingOrder, setListBookingOrder] = useState([]);
  const [valueFilter, setValueFilter] = useState("all");
  const [totalHotel, setTotalHotel] = useState(0);
  const [page, setPage] = useState(1);
  const [anchorEl, setAnchorEl] = useState(null);
  const [status, setStatus] = useState(listStatus.INIT_STATE);
  const dispatch = useDispatchSystem();
  useEffect(() => {
    let params = {
      page: 1,
      size: pageSize,
    };
    fetMyBookingHotel(params);
  }, []);

  const fetMyBookingHotel = async (params) => {
    try {
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: true,
      });
      const { data } = await getMyBooking(params);
      if (status === listStatus.INIT_STATE) {
        setStatus(listStatus.FIRST_REQUEST);
      } else if (status === listStatus.FIRST_REQUEST) {
        setStatus(listStatus.LAST_REQUEST);
      }
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
      if (data.code === 200) {
        setListBookingOrder(data.data.items);
        setTotalHotel(data.data.total);
      }
    } catch (error) {
      if (status === listStatus.INIT_STATE) {
        setStatus(listStatus.FIRST_REQUEST);
      } else if (status === listStatus.FIRST_REQUEST) {
        setStatus(listStatus.LAST_REQUEST);
      }
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
    }
  };

  const handleSelectedItemSort = (code = "") => (e) => {
    handleClose();
    setValueFilter(code);
    setPage(1);
    let params = {
      page: 1,
      size: pageSize,
    };
    if (code !== "all") {
      params = {
        ...params,
        filter: {
          status: [code],
        },
      };
    }
    fetMyBookingHotel(params);
  };

  const getNameFilter = () => {
    const filterSelected = listFilter.find((el) => el.code === valueFilter);
    return filterSelected.label;
  };
  const handleClickPopover = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleChangePage = (page) => () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
    setPage(page);
    setTimeout(() => {
      let params = {
        page: page,
        size: pageSize,
      };
      if (valueFilter !== "all") {
        params = {
          ...params,
          filter: {
            status: [valueFilter],
          },
        };
      }
      fetMyBookingHotel(params);
    }, 100);
  };

  const OrderEmptyFirst = (
    <Box className={classes.orderEmptyFirst}>
      <Image
        srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_empty_order_hotel.svg"
        className={classes.imageEmpty}
      />
      <Box component="span" pb={2} pt={12 / 8} lineHeight="19px" fontSize={16}>
        {listString.IDS_MT_TEXT_DES_ODER_HOTEL}
      </Box>
      <ButtonComponent
        backgroundColor="#FF1284"
        borderRadius={8}
        height={48}
        padding="16px 30px"
        fontSize={16}
        fontWeight={600}
        width="fit-content"
        handleClick={() => {
          router.push({
            pathname: routeStatic.HOTEL.href,
          });
        }}
      >
        {listString.IDS_MT_TEXT_SEARCH_HOTEL}
      </ButtonComponent>
    </Box>
  );

  const OrderEmptyFilter = (
    <Box className={classes.orderEmptyFirst}>
      <Image
        srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_empty_order_hotel.svg"
        className={classes.imageEmpty}
      />
      <Box
        component="span"
        color="gray.grayDark7"
        pb={2}
        pt={12 / 8}
        lineHeight="19px"
        fontSize={16}
      >
        Không có kết quả
      </Box>
    </Box>
  );

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <HomeAccount>
      <Box pl={1} mt={-2} display="flex" justifyContent="flex-end">
        <ButtonComponent
          typeButton="text"
          color="#1A202C"
          width="fit-content"
          handleClick={handleClickPopover}
          aria-describedby={id}
        >
          <Typography className={classes.viewMessageReview}>
            {getNameFilter()}
            <IconDropDown
              className={`svgFillAll ${classes.iconArrowDownToggle}`}
            />
          </Typography>
        </ButtonComponent>
      </Box>
      <Box minHeight="calc(100vh - 230px)">
        {status === listStatus.INIT_STATE
          ? ""
          : !isEmpty(listBookingOrder)
          ? listBookingOrder.map((el, index) => (
              <Box key={el.id} pb={2}>
                <HotelItemOder item={el} valueFilter={valueFilter} />
              </Box>
            ))
          : status === listStatus.FIRST_REQUEST
          ? OrderEmptyFirst
          : OrderEmptyFilter}
      </Box>
      {totalHotel > 0 && (
        <UsePagination
          pageSize={pageSize}
          total={totalHotel}
          pageNum={page}
          className={classes.paginationContainer}
          handleChangePage={handleChangePage}
        />
      )}
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        classes={{ paper: classes.popoverPaper }}
      >
        <Box width="100%" textAlign="center">
          {listFilter.map((item, index) => (
            <Box
              className={clsx(
                classes.boxItem,
                index === listFilter.length - 1 && classes.hideBorder
              )}
              key={item.code}
              onClick={handleSelectedItemSort(item.code)}
              color={valueFilter === item.code ? "#00B6F3" : "#1A202C"}
            >
              <Box component="span" className={classes.textSort}>
                {item.label}
              </Box>
              <Box className={classes.iconActive}>
                {valueFilter === item.code && <IconActive />}
              </Box>
            </Box>
          ))}
        </Box>
        <Box />
      </Popover>
    </HomeAccount>
  );
};

export default HotelBooking;
