import clsx from "clsx";
import moment from "moment";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import { Box, Typography } from "@material-ui/core";

import { listString, routeStatic } from "@utils/constants";
import { isEmpty, paymentStatus } from "@utils/helpers";
import { IconUser2, IconDropDown, IconMoonlight } from "@public/icons";

import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: theme.palette.white.main,
    borderRadius: 8,
    boxShadow: "0px 3px 3px rgba(0, 0, 0, 0.05)",
    padding: "12px 16px",
    color: theme.palette.black.black3,
    transition: "all .2s ",
    fontSize: 14,
    "&:hover": {
      cursor: "pointer",
      boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
      transition: "all .2s ",
    },
  },
  nameHotel: {
    display: "inline-block",
    maxWidth: 400,
    // whiteSpace: "nowrap",
    overflow: "hidden !important",
    textOverflow: "ellipsis",
  },
  header: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    color: theme.palette.primary.main,
  },
  iconArrowDownToggle: {
    stroke: theme.palette.primary.main,
    transform: "rotate(270deg)",
  },
  diver: {
    height: 1,
    backgroundColor: theme.palette.gray.grayLight22,
    width: "100%",
    margin: "12px 0",
  },
  infoHotel: {
    display: "flex",
    justifyContent: "space-between",
  },
  imageHotel: {
    width: 136,
    height: 102,
    borderRadius: 8,
  },
  iconUser2: {
    height: 12,
    width: 12,
  },
  diverFull: {
    width: "calc(100% + 32px)",
    margin: "12px -16px",
  },
  status: {
    display: "flex",
    alignItems: "center",
    borderRadius: 4,
    height: 22,
    padding: " 0 6px",
    textTransform: "uppercase",
    color: theme.palette.white.main,
    fontSize: 12,
    fontWeight: 600,
    lineHeight: "14px",
  },
  timeCheckIn: {
    fontSize: 14,
    fontWeight: 600,
    lineHeight: "17px",
  },
  nightDay: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 34,
    height: 28,
    borderRadius: "100px",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    backgroundColor: theme.palette.white.main,
    margin: "0 24px",
  },
}));

let interval = null;

const HotelItemOder = ({ item = {}, valueFilter = "" }) => {
  const classes = useStyles();
  const hotel = item.hotel || {};
  const router = useRouter();
  const roomBookings = item.roomBookings || [];
  const checkIn = moment(item.checkIn, "DD-MM-YYYY");
  const checkOut = moment(item.checkOut, "DD-MM-YYYY");
  const [timer, setTimer] = useState({
    hours: "00",
    minutes: "00",
    seconds: "00",
  });
  const [isShowCountdown, setIsShowCountdown] = useState(false);

  useEffect(() => {
    const startTime = moment();
    const endTime = !isEmpty(item?.expiredTime)
      ? moment(item?.expiredTime, "YYYY-MM-DD HH:mm:ss")
      : moment();
    const diffTime = endTime.valueOf() - startTime.valueOf();
    let duration = moment.duration(diffTime, "milliseconds");

    if (diffTime > 0) {
      setIsShowCountdown(true);
      interval = setInterval(() => {
        duration = moment.duration(duration - 1000, "milliseconds");
        setTimer({
          hours:
            duration.hours() < 10 ? `0${duration.hours()}` : duration.hours(),
          minutes:
            duration.minutes() < 10
              ? `0${duration.minutes()}`
              : duration.minutes(),
          seconds:
            duration.seconds() < 10
              ? `0${duration.seconds()}`
              : duration.seconds(),
        });
        if (
          duration.hours() === 0 &&
          duration.minutes() === 0 &&
          duration.seconds() === 0
        ) {
          setIsShowCountdown(false);
          clearInterval(interval);
        }
      }, 1000);
    }
  }, [item, valueFilter]);

  useEffect(() => {
    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <Box
      className={classes.container}
      onClick={() => {
        router.push({
          pathname: routeStatic.ACCOUNT_ORDER_BOOKING_DETAIL.href,
          query: {
            id: item.id,
          },
        });
      }}
    >
      <Box className={classes.header}>
        <Box display="flex" alignItems="center" lineHeight="17px">
          <Box
            component="span"
            color="black.black3"
          >{`${listString.IDS_TEXT_FLIGHT_ORDER_CODE}: `}</Box>
          <Box component="span" pl={2 / 8}>{`${item.orderCode}`}</Box>
        </Box>
        <Box display="flex" alignItems="center">
          <Box component="span" pr={4 / 8}>
            {listString.IDS_MT_TEXT_VIEW_DETAIL}
          </Box>
          <IconDropDown
            className={`svgFillAll ${classes.iconArrowDownToggle}`}
          />
        </Box>
      </Box>
      <Box className={clsx(classes.diver, classes.diverFull)} />
      <Box className={classes.infoHotel}>
        <Box display="flex">
          <Image
            srcImage={hotel?.thumbnail?.image.small}
            className={classes.imageHotel}
            borderRadiusProp="8px"
          />
          <Box display="flex" flexDirection="column" pl={2}>
            <Box
              component="span"
              fontSize={16}
              fontWeight={600}
              lineHeight="19px"
              className={classes.nameHotel}
            >
              {hotel?.name}
            </Box>
            <Box
              component="span"
              fontWeight={600}
              lineHeight="17px"
              py={1}
              className={classes.nameHotel}
            >{`${item.numRooms}x ${
              !isEmpty(roomBookings) ? roomBookings[0].name : ""
            }`}</Box>
            <Box display="flex">
              <IconUser2 className={classes.iconUser2} />
              <Box component="span" pl={4 / 8} fontSize={12}>
                {`${parseInt(item?.numAdult) +
                  parseInt(item?.numChildren)} người`}
              </Box>
            </Box>
          </Box>
        </Box>
        <Box display="flex">
          <Box>
            <Box
              display="flex"
              justifyContent="space-between"
              lineHeight="17px"
              color="gray.grayDark8"
            >
              <Box component="span">{listString.IDS_MT_TEXT_CHECK_IN_ROOM}</Box>
              <Box component="span">
                {listString.IDS_MT_TEXT_CHECK_OUT_ROOM}
              </Box>
            </Box>
            <Box
              display="flex"
              alignItems="center"
              justifyContent="space-between"
              py={2 / 8}
            >
              <Box>
                <Typography className={classes.timeCheckIn}>
                  {checkIn
                    ? checkIn
                        .locale("vi_VN")
                        .format("ddd")
                        .replace("T", "Thứ ")
                        .replace("CN", "Chủ Nhật")
                    : "-"}
                  <Box component="span">
                    {", "}
                    {checkIn.format("DD")} Tháng {checkIn.format("MM")}
                  </Box>
                </Typography>
              </Box>
              <Box className={classes.nightDay}>
                <Box
                  component="span"
                  fontSize={11}
                  lineHeight="13px"
                  pr={2 / 8}
                >
                  {checkIn && checkOut
                    ? Math.abs(
                        moment(checkIn)
                          .startOf("day")
                          .diff(checkOut.startOf("day"), "days")
                      )
                    : "-"}
                </Box>
                <IconMoonlight />
              </Box>
              <Box>
                <Typography className={classes.timeCheckIn}>
                  {checkOut
                    ? checkOut
                        .locale("vi_VN")
                        .format("ddd")
                        .replace("T", "Thứ ")
                        .replace("CN", "Chủ Nhật")
                    : "-"}
                  <Box component="span">
                    {", "}
                    {checkOut.format("DD")} Tháng {checkOut.format("MM")}
                  </Box>
                </Typography>
              </Box>
            </Box>
            <Box
              display="flex"
              justifyContent="space-between"
              lineHeight="17px"
              color="gray.grayDark8"
            >
              <Box component="span">{hotel.checkInTime}</Box>
              <Box component="span">{hotel.checkOutTime}</Box>
            </Box>
          </Box>
          <Box
            pl={12}
            display="flex"
            flexDirection="column"
            justifyContent="space-between"
            alignItems="flex-end"
            textAlign="end"
            minWidth={250}
          >
            <Box display="flex" flexDirection="column">
              <Box
                component="span"
                lineHeight="17px"
              >{`${listString.IDS_MT_TEXT_TOTAL_PRICE}`}</Box>
              <Box
                component="span"
                fontWeight={600}
                pt={4 / 8}
                lineHeight="19px"
              >{`${item.finalPriceFormatted}`}</Box>
            </Box>
            <Box className={classes.status} bgcolor={paymentStatus(item).color}>
              {paymentStatus(item).title}
            </Box>
            {paymentStatus(item).isShowCountDown && isShowCountdown && (
              <Box
                display="flex"
                justifyContent="flex-end"
                color="orange.main"
                pt={4 / 8}
              >
                <Box pl={2 / 8} component="span">
                  <Box component="span">{`Còn `}</Box>
                  <Box component="span">{timer.hours}</Box>
                  <Box
                    component="span"
                    className={classes.itemBetween}
                  >{` giờ `}</Box>
                  <Box component="span">{timer.minutes}</Box>
                  <Box
                    component="span"
                    className={classes.itemBetween}
                  >{` phút `}</Box>
                  <Box component="span">{timer.seconds}</Box>
                  <Box
                    component="span"
                    className={classes.itemBetween}
                  >{` giây `}</Box>
                </Box>
              </Box>
            )}
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default HotelItemOder;
