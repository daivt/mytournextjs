import { getFlightBookings, getGeneralInformation } from "@api/flight";
import HomeAccount from "@components/desktop/account/HomeAccount";
import FlightBookingItem from "@components/desktop/account/order/flight/FlightBookingItem";
import { useDispatchSystem } from "@contextProvider/ContextProvider";
import * as actionTypes from "@contextProvider/system/ActionTypes";
import { Box, Popover, Typography, useTheme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconActive, IconArrowDownToggle } from "@public/icons";
import ButtonComponent from "@src/button/Button";
import UsePagination from "@src/pagination/Pagination";
import { listString, routeStatic } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import clsx from "clsx";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { useEffect, useState } from "react";
import Image from "@src/image/Image";
const useStyles = makeStyles((theme) => ({
  orderEmptyFirst: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "calc(100vh - 230px)",
    color: theme.palette.black.black3,
  },
  iconArrowDownToggle: {
    stroke: theme.palette.black.black3,
    marginLeft: 5,
  },
  viewMessageReview: {
    lineHeight: "17px",
    color: theme.palette.black.black3,
    display: "flex",
    alignItems: "center",
  },
  boxItem: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    height: 30,
    justifyContent: "space-between",
    padding: "6px 8px",
    borderRadius: 8,
    "&:hover": {
      backgroundColor: theme.palette.gray.grayLight22,
      cursor: "pointer",
    },
  },
  boxItemActive: {
    color: theme.palette.blue.blueLight8,
  },
  hideBorder: {
    borderBottom: "none",
  },
  textSort: {
    fontSize: 14,
    lineHeight: "17px",
  },
  popoverPaper: {
    borderRadius: 8,
    padding: 8,
  },
  paginationContainer: {
    width: "100%",
    display: "flex",
    justifyContent: "flex-end",
    margin: "32px 0",
  },
  imageEmpty: {
    width: 249,
    height: 210,
  },
}));

const listFilter = [
  {
    code: "all",
    label: "Tất cả",
  },
  {
    code: "success",
    label: "Thành công",
  },
  {
    code: "holding",
    label: "Đang giữ chỗ",
  },
  {
    code: "fail",
    label: "Thất bại",
  },
];

const listStatus = {
  INIT_STATE: "INIT_STATE",
  FIRST_REQUEST: "FIRST_REQUEST",
  LAST_REQUEST: "LAST_REQUEST",
};

const firstPage = 1;
const pageSize = 20;
const OrderFlight = ({ query = {}, codeFlight = {} }) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [orderFlights, setOrderFlights] = useState([]);
  const [valueFilter, setValueFilter] = useState(["all"]);
  const [totalFlights, setTotalFlights] = useState(0);
  const [page, setPage] = useState(firstPage);
  const [airlines, setAirlines] = useState();
  const [anchorEl, setAnchorEl] = useState(null);
  const [status, setStatus] = useState(listStatus.INIT_STATE);
  const dispatch = useDispatchSystem();

  useEffect(() => {
    const params = {
      filters: {
        paymentStatuses: ["all"],
      },
      paging: {
        page: firstPage,
        size: pageSize,
      },
    };
    fetchBookingFlight(params);
    actionsGeneralInformation();
  }, []);

  const actionsGeneralInformation = async () => {
    const { data } = await getGeneralInformation();
    setAirlines(data?.data?.airlines);
  };

  const fetchBookingFlight = async (params) => {
    try {
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: true,
      });
      const { data } = await getFlightBookings(params);
      if (status === listStatus.INIT_STATE) {
        setStatus(listStatus.FIRST_REQUEST);
      } else if (status === listStatus.FIRST_REQUEST) {
        setStatus(listStatus.LAST_REQUEST);
      }
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });

      if (data?.code !== 200) {
        data?.message &&
          enqueueSnackbar(
            data?.message,
            snackbarSetting((key) => closeSnackbar(key), { color: "error" })
          );
      }
      setOrderFlights(data?.data);
      setTotalFlights(data?.total);
    } catch (error) {
      if (status === listStatus.INIT_STATE) {
        setStatus(listStatus.FIRST_REQUEST);
      } else if (status === listStatus.FIRST_REQUEST) {
        setStatus(listStatus.LAST_REQUEST);
      }
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
    }
  };

  const handleSelectedItemSort = (code = "") => (e) => {
    handleClose();
    let valueFilterTemp = [];
    valueFilterTemp.push(code);
    if (code === "success") {
      valueFilterTemp.push("completed");
    }
    setValueFilter(valueFilterTemp);
    setPage(firstPage);
    let params = {
      paging: {
        page: firstPage,
        size: pageSize,
      },
    };
    if (!valueFilterTemp.includes("all")) {
      params = {
        ...params,
        filters: {
          paymentStatuses: valueFilterTemp,
        },
      };
    }
    fetchBookingFlight(params);
  };
  const handleChangePage = (page) => () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
    setPage(page);
    setTimeout(() => {
      let params = {
        paging: {
          page: page,
          size: pageSize,
        },
      };
      if (!valueFilter.includes("all")) {
        params = {
          ...params,
          filter: {
            paymentStatuses: valueFilter,
          },
        };
      }
      fetchBookingFlight(params);
    }, 100);
  };

  const getNameFilter = () => {
    const filterSelected = listFilter.find((el) =>
      valueFilter.includes(el.code)
    );
    return filterSelected.label;
  };
  const handleClickPopover = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const OrderEmptyFirst = (
    <Box className={classes.orderEmptyFirst}>
      <Image
        srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_empty_order_flight.svg"
        className={classes.imageEmpty}
      />
      <Typography variant="body1" style={{ padding: "12px 0 16px 0" }}>
        {listString.IDS_MT_TEXT_DES_ODER_FLIGHT}
      </Typography>
      <ButtonComponent
        backgroundColor={theme.palette.pink.main}
        borderRadius={8}
        height={48}
        padding="0 12px"
        width="fit-content"
        handleClick={() => {
          router.push({
            pathname: routeStatic.FLIGHT.href,
          });
        }}
      >
        <Typography variant="subtitle1" component="span">
          {listString.IDS_MT_TEXT_FIND_FLIGHT}
        </Typography>
      </ButtonComponent>
    </Box>
  );

  const OrderEmptyFilter = (
    <Box className={classes.orderEmptyFirst}>
      <Image
        srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_empty_order_flight.svg"
        className={classes.imageEmpty}
      />
      <Box component="span" color="gray.grayDark7" pb={2} pt={12 / 8}>
        <Typography variant="body1" component="span">
          {listString.IDS_MT_TEXT_NO_RESULT}
        </Typography>
      </Box>
    </Box>
  );

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <HomeAccount query={query} codeFlight={codeFlight}>
      <Box
        display="flex"
        justifyContent="flex-end"
        mb={1}
        maxWidth="1188px"
        width="100%"
        margin="0 auto"
      >
        <ButtonComponent
          typeButton="text"
          color={theme.palette.black.black3}
          width="fit-content"
          handleClick={handleClickPopover}
          aria-describedby={id}
        >
          <Typography variant="caption" className={classes.viewMessageReview}>
            {getNameFilter()}
            <IconArrowDownToggle
              className={`svgFillAll ${classes.iconArrowDownToggle}`}
            />
          </Typography>
        </ButtonComponent>
      </Box>
      <Box minHeight="calc(100vh - 230px)">
        {status === listStatus.INIT_STATE
          ? ""
          : !isEmpty(orderFlights)
          ? orderFlights.map((el, index) => (
              <Box key={index} pb={2}>
                <FlightBookingItem item={el} airlines={airlines} />
              </Box>
            ))
          : status === listStatus.FIRST_REQUEST
          ? OrderEmptyFirst
          : OrderEmptyFilter}
      </Box>
      {totalFlights > 0 && (
        <UsePagination
          pageSize={pageSize}
          total={totalFlights}
          pageNum={page}
          className={classes.paginationContainer}
          handleChangePage={handleChangePage}
        />
      )}
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        classes={{ paper: classes.popoverPaper }}
      >
        <Box width="100%" textAlign="center">
          {listFilter.map((item, index) => (
            <Box
              className={clsx(
                classes.boxItem,
                index === listFilter.length - 1 && classes.hideBorder
              )}
              key={item.code}
              onClick={handleSelectedItemSort(item.code)}
              color={
                valueFilter.includes(item.code)
                  ? theme.palette.blue.blueLight8
                  : theme.palette.black.black3
              }
            >
              <Box component="span" className={classes.textSort}>
                {item.label}
              </Box>
              <Box className={classes.iconActive}>
                {valueFilter.includes(item.code) && <IconActive />}
              </Box>
            </Box>
          ))}
        </Box>
        <Box />
      </Popover>
    </HomeAccount>
  );
};
export default OrderFlight;
