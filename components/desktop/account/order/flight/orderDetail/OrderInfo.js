/* eslint-disable no-nested-ternary */
import {
  Box,
  Divider,
  makeStyles,
  Typography,
  useTheme,
} from "@material-ui/core";
import { IconCopy, IconOrderCode } from "@public/icons";
import Image from "@src/image/Image";
import {
  flightPaymentStatus,
  listIcons,
  listString,
  prefixUrlIcon,
} from "@utils/constants";
import {
  isEmpty,
  getPaymentStatusBookingFlight,
  getExpiredTimeMillisecond,
} from "@utils/helpers";
import { DATE_FORMAT_ALL_ISOLATE, DATE_TIME_FORMAT } from "@utils/moment";
import clsx from "clsx";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  listIcon: {
    backgroundColor: theme.palette.gray.grayLight26,
    borderRadius: 8,
    height: 80,
    padding: 16,
    marginRight: 16,
    position: "relative",
  },
  iconBoxItemOder: {
    position: "absolute",
    bottom: 0,
    right: 0,
  },
  widthNormal: {
    width: "25%",
  },
  widthTwoWay: {
    width: "20%",
  },
  widthTwoWayDouble: {
    width: "40%",
  },
  font18: {
    fontSize: 18,
    lineHeight: "22px",
  },
  timeHold: {
    color: theme.palette.orange.main,
    paddingLeft: 8,
  },
  frameStt: {
    padding: "4px 8px",
    borderRadius: 4,
    color: theme.palette.white.main,
    textTransform: "uppercase",
    zIndex: 100,
  },
  crushText: {
    zIndex: 100,
    position: "relative",
  },
  iconStatusOrderHotel: {
    width: 86,
    height: 64,
  },
}));

const OrderInfo = ({ item = {} }) => {
  const classes = useStyles();
  const theme = useTheme();

  const isTwoWay = item.isTwoWay;
  const paymentStatus = getPaymentStatusBookingFlight(
    item?.paymentStatus,
    item?.paymentMethodCode,
    item?.expiredTime || 0
  );
  return (
    <Box display="flex" pl={2}>
      <Box
        className={clsx(
          classes.listIcon,
          isTwoWay ? classes.widthTwoWay : classes.widthNormal
        )}
      >
        <Box className={classes.iconBoxItemOder}>
          <IconOrderCode />
        </Box>
        <Box>
          <Typography variant="caption">
            {listString.IDS_TEXT_FLIGHT_ORDER_CODE}
          </Typography>
        </Box>
        <Box color={theme.palette.blue.blueLight8} pt={6 / 8}>
          <Typography variant="subtitle1" className={classes.font18}>
            {item.orderCode}
          </Typography>
        </Box>
      </Box>
      <Box
        className={clsx(
          classes.listIcon,
          isTwoWay ? classes.widthTwoWayDouble : classes.widthNormal
        )}
        display="flex"
      >
        <Box className={classes.iconBoxItemOder}>
          <IconOrderCode />
        </Box>
        {isTwoWay ? (
          <>
            <Box width="50%">
              <Box>
                <Typography variant="caption">
                  {listString.IDS_TEXT_FLIGHT_BOOK_CODE_OUTBOUND}
                </Typography>
              </Box>
              <Box color={theme.palette.pink.main} pt={6 / 8}>
                <Typography variant="subtitle1" className={classes.font18}>
                  {item.outboundPnrCode
                    ? item.paymentStatus === flightPaymentStatus.REFUNDED ||
                      item.paymentStatus === flightPaymentStatus.REFUNDED_POSTED
                      ? listString.IDS_TEXT_ORDER_PAYMENT_STATUS_CANCELLED
                      : item.outboundPnrCode
                    : listString.IDS_TEXT_ORDER_PAYMENT_STATUS_PROCESSING}
                </Typography>
              </Box>
            </Box>
            <Box
              borderLeft={`solid 1px ${theme.palette.gray.grayLight23}`}
              pl={2}
            >
              <Box>
                <Typography variant="caption">
                  {listString.IDS_TEXT_FLIGHT_BOOK_CODE_INBOUND}
                </Typography>
              </Box>
              <Box color={theme.palette.pink.main} pt={6 / 8}>
                <Typography variant="subtitle1" className={classes.font18}>
                  {item.outboundPnrCode
                    ? item.paymentStatus === flightPaymentStatus.REFUNDED ||
                      item.paymentStatus === flightPaymentStatus.REFUNDED_POSTED
                      ? listString.IDS_TEXT_ORDER_PAYMENT_STATUS_CANCELLED
                      : item.outboundPnrCode
                    : listString.IDS_TEXT_ORDER_PAYMENT_STATUS_PROCESSING}
                </Typography>
              </Box>
            </Box>
          </>
        ) : (
          <Box>
            <Box>
              <Typography variant="caption">
                {listString.IDS_TEXT_FLIGHT_BOOK_CODE}
              </Typography>
            </Box>
            <Box color={theme.palette.pink.main} pt={6 / 8}>
              <Typography variant="subtitle1" className={classes.font18}>
                {item.outboundPnrCode
                  ? item.paymentStatus === flightPaymentStatus.REFUNDED ||
                    item.paymentStatus === flightPaymentStatus.REFUNDED_POSTED
                    ? listString.IDS_TEXT_ORDER_PAYMENT_STATUS_CANCELLED
                    : item.outboundPnrCode
                  : listString.IDS_TEXT_ORDER_PAYMENT_STATUS_PROCESSING}
              </Typography>
            </Box>
          </Box>
        )}
      </Box>
      <Box
        className={clsx(
          classes.listIcon,
          isTwoWay ? classes.widthTwoWay : classes.widthNormal
        )}
      >
        <Box className={classes.iconBoxItemOder}>
          <Image
            srcImage={`${prefixUrlIcon}${listIcons.IconStatusOrderHotel}`}
            className={classes.iconStatusOrderHotel}
          />
        </Box>
        <Box>
          <Typography variant="caption">
            {listString.IDS_MT_TEXT_STATUS}
          </Typography>
          {paymentStatus.type === flightPaymentStatus.HOLDING && (
            <Typography variant="caption" className={classes.timeHold}>
              {`Còn ${getExpiredTimeMillisecond(item.expiredTime)}`}
            </Typography>
          )}
        </Box>
        <Box display="flex" mt={6 / 8}>
          <Box className={classes.frameStt} bgcolor={paymentStatus.color}>
            <Typography variant="subtitle2">{paymentStatus.title}</Typography>
          </Box>
        </Box>
      </Box>
      <Box
        className={clsx(
          classes.listIcon,
          isTwoWay ? classes.widthTwoWay : classes.widthNormal
        )}
      >
        <Box className={classes.iconBoxItemOder}>
          <img src={`${prefixUrlIcon}${listIcons.IconCalenderOrder}`} />
        </Box>
        <Box>
          <Typography variant="caption">
            {listString.IDS_MT_TEXT_DATE_BOOKING}
          </Typography>
        </Box>
        <Box pt={1} className={classes.crushText}>
          <Typography variant="body1">
            {moment(item.bookedDate, DATE_TIME_FORMAT).format(
              DATE_FORMAT_ALL_ISOLATE
            )}
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default OrderInfo;
