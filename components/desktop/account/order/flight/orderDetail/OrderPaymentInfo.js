import { Grid, makeStyles, Typography, useTheme } from "@material-ui/core";
import { listString } from "@utils/constants";
import { useRouter } from "next/router";
const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    textAlign: "center",
    paddingBottom: 4,
  },
  contentBold: {
    fontWeight: 600,
  },
  font18: {
    fontSize: 18,
    lineHeight: "22px",
  },
}));

const OrderPaymentInfo = ({ orderDetail = {}, paymentStatus = {} }) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();

  return (
    <Grid container>
      <Grid item lg={3} md={3} sm={3} xs={3}>
        <Typography variant="subtitle1" className={classes.font18}>
          {listString.IDS_MT_TEXT_PAYMENT_ACTION}
        </Typography>
      </Grid>
      <Grid item lg={3} md={3} sm={3} xs={3}>
        <Typography variant="caption" className={classes.content}>
          {listString.IDS_TEXT_ORDER_PAYMENT_TOTAL_PRICE}:
        </Typography>
        <Typography variant="caption" className={classes.content}>
          {listString.IDS_MT_TEXT_PAYMENT_MENTHODS}:
        </Typography>
      </Grid>
      <Grid item lg={6} md={6} sm={6} xs={6}>
        <Typography variant="caption" className={classes.content}>
          {orderDetail.finalPriceFormatted}
        </Typography>
        <Typography variant="caption" className={classes.content}>
          {orderDetail.paymentMethod}
        </Typography>
      </Grid>
    </Grid>
  );
};

export default OrderPaymentInfo;
