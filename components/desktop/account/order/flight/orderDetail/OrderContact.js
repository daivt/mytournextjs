import { Grid, makeStyles, Typography } from "@material-ui/core";
import { listString } from "@utils/constants";
import clsx from "clsx";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    textAlign: "center",
    paddingBottom: 4,
  },
  contentBold: {
    fontWeight: 600,
  },
  font18: {
    fontSize: 18,
    lineHeight: "22px",
  },
}));

const OrderContact = ({ item }) => {
  const classes = useStyles();
  return (
    <Grid container>
      <Grid item lg={3} md={3} sm={3} xs={3}>
        <Typography variant="subtitle1" className={classes.font18}>
          {listString.IDS_MT_TEXT_INFO_CONTACT}
        </Typography>
      </Grid>
      <Grid item lg={3} md={3} sm={3} xs={3}>
        <Typography variant="caption" className={classes.content}>
          {listString.IDS_MT_TEXT_CONTACT_NAME_SIMPLE}:
        </Typography>
        <Typography variant="caption" className={classes.content}>
          {listString.IDS_MT_TEXT_PHONE_NUMBER}:
        </Typography>
        <Typography variant="caption" className={classes.content}>
          {listString.IDS_MT_TEXT_EMAIL}:
        </Typography>
      </Grid>
      <Grid item lg={6} md={6} sm={6} xs={6}>
        <Typography
          variant="caption"
          className={clsx(classes.contentBold, classes.content)}
        >
          {item?.fullName.toUpperCase()}
        </Typography>
        <Typography
          variant="caption"
          className={clsx(classes.contentBold, classes.content)}
        >
          {item?.phone1 ? item.phone1 : item?.phone2 ? item.phone2 : null}
        </Typography>
        <Typography
          variant="caption"
          className={clsx(classes.contentBold, classes.content)}
        >
          {item.email}
        </Typography>
      </Grid>
    </Grid>
  );
};
OrderContact.propTypes = {
  item: PropTypes.object.isRequired,
};
export default OrderContact;
