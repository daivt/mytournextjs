import { getBankTransferInfo, getGeneralInformation } from "@api/flight";
import FlightTicketDetailPayment from "@components/common/card/flight/FlightTicketDetailPayment";
import OrderContact from "@components/desktop/account/order/flight/orderDetail/OrderContact";
import OrderCustomerInfo from "@components/desktop/account/order/flight/orderDetail/OrderCustomerInfo";
import OrderInfo from "@components/desktop/account/order/flight/orderDetail/OrderInfo";
import WayFlightItemDetail from "@components/desktop/account/order/flight/WayFlightItemDetail";
import OrderInsurance from "@components/desktop/account/order/flight/orderDetail/OrderInsurance";
import OrderPriceDetail from "@components/desktop/account/order/flight/orderDetail/OrderPriceDetail";
import OrderPaymentInfo from "@components/desktop/account/order/flight/orderDetail/OrderPaymentInfo";
import OrderExportInvoice from "@components/desktop/account/order/flight/orderDetail/OrderExportInvoice";
import OrderBankTransfer from "@components/desktop/account/order/flight/orderDetail/OrderBankTransfer";
import {
  Box,
  Grid,
  IconButton,
  makeStyles,
  Typography,
  Divider,
} from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import {
  IconBack,
  IconChangeBaggage,
  IconChangeFlightTime,
  IconClock,
  IconRefundFlight,
  IconShare,
  IconSplitFlightCode,
} from "@public/icons";
import utilStyles from "@styles/utilStyles";
import {
  flightPaymentMethodCode,
  flightPaymentStatus,
  listString,
  routeStatic,
} from "@utils/constants";
import {
  getExpiredTimeMillisecond,
  getPaymentStatusBookingFlight,
  isEmpty,
} from "@utils/helpers";
import clsx from "clsx";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import ButtonComponent from "@src/button/Button";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "column",
    borderRadius: 8,
    backgroundColor: theme.palette.white.main,
  },
  backToOrder: {
    color: theme.palette.blue.blueLight8,
    cursor: "pointer",
  },
  imgOrder: {
    backgroundColor: theme.palette.gray.grayLight26,
    borderRadius: 8,
    padding: 24,
  },
  titleTicket: {
    padding: "4px 6px",
    borderRadius: 4,
    color: theme.palette.white.main,
    backgroundColor: theme.palette.blue.blueLight8,
    fontWeight: 600,
  },
  ticketDetail: {
    width: "45%",
  },
  lineBox: {
    display: "flex",
    width: "100%",
  },
  divider: {
    backgroundColor: theme.palette.gray.grayLight22,
    height: 1,
  },
}));

const FlightBookingDetail = ({ orderDetail = {} }) => {
  const theme = useTheme();
  const classes = useStyles();
  const classesUtils = utilStyles();
  const router = useRouter();
  const [bankTransferInfo, setBankTransferInfo] = useState();

  const [airlines, setAirlines] = useState();
  const actionsGeneralInformation = async () => {
    const { data } = await getGeneralInformation();
    setAirlines(data?.data?.airlines);
  };

  const actionGetBankTransfer = async () => {
    const { data } = await getBankTransferInfo(orderDetail.bankTransferCode);
    if (data.code == 200) {
      setBankTransferInfo(data.data);
    }
  };

  const paymentStatus = getPaymentStatusBookingFlight(
    orderDetail.paymentStatus,
    orderDetail.paymentMethodCode,
    orderDetail?.expiredTime || 0
  );

  useEffect(() => {
    actionsGeneralInformation();
    if (
      !isEmpty(orderDetail?.bankTransferCode) &&
      (orderDetail?.paymentMethodCode ===
        flightPaymentMethodCode.BANK_TRANSFER ||
        orderDetail?.paymentMethodCode ===
          flightPaymentMethodCode.BANK_TRANSFER_2)
    ) {
      actionGetBankTransfer();
    }
  }, []);

  let airlineOutBoundInfo = {};
  let airlineInBoundInfo = {};
  if (airlines) {
    airlineOutBoundInfo = airlines.find(
      (al) => al.aid === orderDetail.outbound?.airlineId
    );
    if (orderDetail.isTwoWay) {
      airlineInBoundInfo = airlines.find(
        (al) => al.aid === orderDetail.inbound?.airlineId
      );
    }
  }

  const handleClickPayment = () => {
    router.push({
      pathname: routeStatic.REPAY_BOOKING_FLIGHT.href,
      query: {
        bookingId: orderDetail?.orderCode.substr(1),
      },
    });
  };

  return (
    <Box className={classes.wrapper}>
      <Box padding="20px 0 0 16px" display="flex" flexDirection="column">
        <Box
          className={classes.backToOrder}
          display="flex"
          alignContent="center"
          mb={2}
        >
          <Box
            onClick={() => {
              router.back();
            }}
          >
            <IconBack
              className="svgFillAll"
              style={{
                stroke: theme.palette.blue.blueLight8,
              }}
            />
          </Box>
          <Box ml={14 / 8}>
            <Typography variant="caption">
              {listString.IDS_MT_TEXT_RETURN_ORDER}
            </Typography>
          </Box>
        </Box>
      </Box>
      <OrderInfo item={orderDetail} />
      {!isEmpty(bankTransferInfo) && (
        <Box padding="16px 16px 0 16px">
          <OrderBankTransfer bankTransferInfo={bankTransferInfo} />
        </Box>
      )}
      <Box my={3} ml={2} display="flex">
        <Box>
          <Box className={classes.imgOrder}>
            <img src="/images/order_flight.png" />
          </Box>
        </Box>
        <Box ml={2} className={classes.lineBox} flexDirection="column">
          <Box className={classes.lineBox} mb={3}>
            <Box className={clsx(classes.ticketDetail)}>
              {orderDetail.isTwoWay && (
                <Box mb={12 / 8} display="flex">
                  <Box className={classes.titleTicket}>
                    <Typography variant="body2">
                      {listString.IDS_MT_TEXT_DEPARTURE.toUpperCase()}
                    </Typography>
                  </Box>
                </Box>
              )}
              <WayFlightItemDetail
                item={orderDetail?.outbound}
                airlineInfo={airlineOutBoundInfo}
                isViewDetail={true}
              />
            </Box>
            {orderDetail.isTwoWay && (
              <Box className={clsx(classes.ticketDetail)}>
                <Box mb={12 / 8} display="flex">
                  <Box className={classes.titleTicket}>
                    <Typography variant="body2">
                      {listString.IDS_MT_TEXT_RETURN.toUpperCase()}
                    </Typography>
                  </Box>
                </Box>
                <WayFlightItemDetail
                  item={orderDetail?.inbound}
                  airlineInfo={airlineInBoundInfo}
                  isViewDetail={true}
                />
              </Box>
            )}
          </Box>
          <Divider className={classes.divider} />
          <Box className={classes.lineBox} mt={3}>
            <OrderContact item={orderDetail?.mainContact} />
          </Box>
          <Box className={classes.lineBox} mt={3}>
            <OrderCustomerInfo guests={orderDetail.guests} />
          </Box>
          {!isEmpty(orderDetail?.vatInvoiceInfo) && (
            <Box className={classes.lineBox} mt={3} flexDirection="column">
              <OrderExportInvoice
                invoice={orderDetail.vatInvoiceInfo}
                paymentStatus={paymentStatus}
              />
            </Box>
          )}
          <Box className={classes.lineBox} mt={3}>
            <OrderPaymentInfo
              orderDetail={orderDetail}
              paymentStatus={paymentStatus}
            />
          </Box>
          <Box className={classes.lineBox} mt={3}>
            <OrderInsurance
              insuranceContact={orderDetail.insuranceContact}
              insuranceInfo={orderDetail.guests[0].insuranceInfo}
              numberInsurance={orderDetail.numGuests}
            />
          </Box>
          <Box className={classes.lineBox} mt={3}>
            <OrderPriceDetail orderDetail={orderDetail} />
          </Box>
          {(paymentStatus.type === flightPaymentStatus.HOLDING ||
            paymentStatus.type === flightPaymentStatus.PENDING) && (
            <Box className={classes.lineBox} mt={3} justifyContent="flex-end">
              <Box mr={2}>
                <ButtonComponent
                  type="submit"
                  backgroundColor={theme.palette.pink.main}
                  height={48}
                  borderRadius={8}
                  padding={"12px 36px"}
                  handleClick={handleClickPayment}
                  className={classes.rightBox}
                >
                  <Typography variant="subtitle1">
                    {listString.IDS_MT_TEXT_PAYMENT}
                  </Typography>
                </ButtonComponent>
              </Box>
            </Box>
          )}
        </Box>
      </Box>
    </Box>
  );
};

export default FlightBookingDetail;
