import { Box, Grid, makeStyles, Typography } from "@material-ui/core";
import { listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import { DATE_FORMAT_BACK_END } from "@utils/moment";
import moment from "moment";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    textAlign: "center",
    paddingBottom: 4,
  },
  contentBold: {
    fontWeight: 600,
  },
  font18: {
    fontSize: 18,
    lineHeight: "22px",
  },
}));

const OrderInsurance = ({
  insuranceContact,
  insuranceInfo,
  numberInsurance,
}) => {
  const classes = useStyles();
  if (isEmpty(insuranceContact)) return null;
  return (
    <Grid container>
      <Grid item lg={3} md={3} sm={3} xs={3}>
        <Typography variant="subtitle1" className={classes.font18}>
          {listString.IDS_TEXT_ORDER_INSURANCE}
        </Typography>
      </Grid>
      <Grid item lg={3} md={3} sm={3} xs={3}>
        <Typography variant="caption" className={classes.content}>
          {listString.IDS_TEXT_ORDER_INSURANCE_PACKAGE}:
        </Typography>
        <Typography variant="caption" className={classes.content}>
          {listString.IDS_TEXT_ORDER_INSURANCE_START}:
        </Typography>
        <Typography variant="caption" className={classes.content}>
          {listString.IDS_TEXT_ORDER_INSURANCE_END}:
        </Typography>
        <Typography variant="caption" className={classes.content}>
          {listString.IDS_TEXT_ORDER_INSURANCE_NUMBER}:
        </Typography>
        <Typography variant="caption" className={classes.content}>
          {listString.IDS_TEXT_ORDER_INSURANCE_PRICE}:
        </Typography>
      </Grid>
      <Grid item lg={6} md={6} sm={6} xs={6}>
        <Typography variant="caption" className={classes.content}>
          {insuranceInfo?.insurancePackageName}
        </Typography>
        <Typography variant="caption" className={classes.content}>
          {moment(insuranceInfo?.fromDate, DATE_FORMAT_BACK_END).format("L")}
        </Typography>
        <Typography variant="caption" className={classes.content}>
          {moment(insuranceInfo?.toDate, DATE_FORMAT_BACK_END).format("L")}
        </Typography>
        <Typography variant="caption" className={classes.content}>
          {numberInsurance}
        </Typography>
        <Typography variant="caption" className={classes.content}>
          {`${insuranceContact?.totalPrice.formatMoney()}đ`}
        </Typography>
      </Grid>
    </Grid>
  );
};
OrderInsurance.propTypes = {
  insuranceContact: PropTypes.object,
  insuranceInfo: PropTypes.object,
  numberInsurance: PropTypes.number,
};
export default OrderInsurance;
