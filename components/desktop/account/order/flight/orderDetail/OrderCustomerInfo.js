import CustomerDetailModalDesktop from "@components/common/modal/flight/CustomerDetailModalDesktop";
import { Box, Grid, makeStyles, Typography, useTheme } from "@material-ui/core";
import ButtonComponent from "@src/button/Button";
import { CUSTOMER_TYPE_TEXT, GENDER_LIST, listString } from "@utils/constants";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    textAlign: "center",
    paddingBottom: 4,
  },
  contentBold: {
    fontWeight: 600,
  },
  font18: {
    fontSize: 18,
    lineHeight: "22px",
  },
  title: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  numGuest: {
    color: theme.palette.gray.grayDark7,
  },
  ellipsisText: {
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    maxWidth: "250px",
  },
}));

const OrderCustomerInfo = ({ guests = {}, isFromPayment = false }) => {
  const theme = useTheme();
  const classes = useStyles();
  const [openViewDetail, setOpenViewDetail] = useState(false);
  const handleViewDetail = (open) => () => {
    setOpenViewDetail(open);
  };
  let adultNo = 0;
  let childrenNo = 0;
  let infantNo = 0;
  return (
    <Grid container>
      <Grid item lg={3} md={3} sm={3} xs={3}>
        <Typography variant="subtitle1" className={classes.font18}>
          {listString.IDS_MT_TEXT_INFO_CUSTOMER_FLIGHT}
        </Typography>
        {!isFromPayment ? (
          <>
            <ButtonComponent
              fontSize={14}
              fontWeight="normal"
              color={theme.palette.blue.blueLight8}
              typeButton="text"
              width="fit-content"
              padding={0}
              handleClick={handleViewDetail(true)}
            >
              <Typography component="span" variant="caption">
                {listString.IDS_MT_TEXT_VIEW_DETAIL}
              </Typography>
            </ButtonComponent>
            <CustomerDetailModalDesktop
              open={openViewDetail}
              handleClose={handleViewDetail}
              guests={guests}
            />
          </>
        ) : null}
      </Grid>
      <Grid item lg={3} md={3} sm={3} xs={3}>
        {guests.map((item, index) => {
          return (
            <Typography
              variant="caption"
              className={classes.content}
              key={index}
            >
              {item.ageCategory.toLowerCase() === CUSTOMER_TYPE_TEXT.ADULT &&
                `${GENDER_LIST.find((el) => el.id === item?.gender)?.label}:`}
              {item.ageCategory.toLowerCase() === CUSTOMER_TYPE_TEXT.CHILDREN &&
                `${listString.IDS_MT_TEXT_CHILDREN} ${++childrenNo}:`}
              {item.ageCategory.toLowerCase() === CUSTOMER_TYPE_TEXT.INFANT &&
                `${listString.IDS_MT_TEXT_BABY} ${++infantNo}:`}
            </Typography>
          );
        })}
      </Grid>
      <Grid item lg={6} md={6} sm={6} xs={6}>
        {guests.length
          ? guests.map((item, index) => {
              return (
                <Typography
                  variant="caption"
                  className={classes.content}
                  key={index}
                >
                  {item?.fullName.toUpperCase()}
                </Typography>
              );
            })
          : null}
      </Grid>
    </Grid>
  );
};

export default OrderCustomerInfo;
