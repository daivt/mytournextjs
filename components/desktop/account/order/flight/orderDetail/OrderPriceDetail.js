import { Grid, makeStyles, Typography, useTheme } from "@material-ui/core";
import { listString } from "@utils/constants";
import { useRouter } from "next/router";
import PriceDetail from "@components/desktop/flight/payment/PriceDetail";
import { formatDetailPriceTicketBookingData } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    textAlign: "center",
    paddingBottom: 4,
  },
  contentBold: {
    fontWeight: 600,
  },
  font18: {
    fontSize: 18,
    lineHeight: "22px",
  },
}));

const OrderPriceDetail = ({ orderDetail = {} }) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const detailPriceTicket = formatDetailPriceTicketBookingData(orderDetail);

  return (
    <Grid container style={{ marginRight: 16 }}>
      <Grid item lg={3} md={3} sm={3} xs={3}>
        <Typography variant="subtitle1" className={classes.font18}>
          {listString.IDS_MT_TEXT_PRICE_DETAIL}
        </Typography>
      </Grid>
      <Grid item lg={9} md={9} sm={9} xs={9}>
        <PriceDetail item={detailPriceTicket} isOrder={true} />
      </Grid>
    </Grid>
  );
};

export default OrderPriceDetail;
