import { Box, Grid, makeStyles, Typography, useTheme } from "@material-ui/core";
import { listString } from "@utils/constants";
import clsx from "clsx";
import PropTypes, { element } from "prop-types";
import {
  C_DATE_TIME_FORMAT,
  DATE_FORMAT,
  DATE_FORMAT_BACK_END,
  HOUR_MINUTE,
} from "@utils/moment";
import moment from "moment";
import { getExpiredTimeMillisecond, isEmpty } from "@utils/helpers";
import Image from "@src/image/Image";
import { IconCopy } from "@public/icons";

const useStyles = makeStyles((theme) => ({
  borderBox: {
    display: "flex",
    backgroundColor: theme.palette.gray.grayLight26,
    padding: 24,
    border: `dashed 1px ${theme.palette.gray.grayLight25}`,
    borderRadius: 8,
    flexDirection: "column",
  },
  contentBold: {
    fontWeight: 600,
  },
  font18: {
    fontSize: 18,
    lineHeight: "22px",
  },
  lineBox: {
    paddingTop: 8,
    display: "flex",
  },
  lineLeft: {
    width: "16%",
    display: "flex",
    alignItems: "center",
  },
  iconBank: {
    width: 42,
    height: 18,
    objectFit: "contain",
  },
  copy: {
    display: "flex",
    justifyContent: "flex-end",
    color: theme.palette.blue.blueLight8,
    alginItems: "center",
    fontSize: "14px",
    lineHeight: "17px",
    marginLeft: 14,
    cursor: "pointer",
  },
}));

const OrderBankTransfer = ({ bankTransferInfo = {} }) => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Box display="flex" flexDirection="column">
      <Box color={theme.palette.orange.main} pb={12 / 8}>
        <Typography variant="body1">
          {listString.IDS_TEXT_FLIGHT_BOOKING_BANK_TRANSFER_2}
          {!isEmpty(bankTransferInfo?.transferInfo?.expiredTime)
            ? ` ${moment(
                bankTransferInfo?.transferInfo.expiredTime,
                C_DATE_TIME_FORMAT
              ).format(HOUR_MINUTE)}, `
            : " "}
          {!isEmpty(bankTransferInfo?.transferInfo?.expiredTime)
            ? moment(
                bankTransferInfo?.transferInfo.expiredTime,
                C_DATE_TIME_FORMAT
              )
                .format("ddd")
                .replace("T2", "Thứ hai")
                .replace("T3", "Thứ ba")
                .replace("T4", "Thứ tư")
                .replace("T5", "Thứ năm")
                .replace("T6", "Thứ sáu")
                .replace("T7", "Thứ bảy")
                .replace("CN", "Chủ Nhật")
            : "-"}
          {","}
          {!isEmpty(bankTransferInfo?.transferInfo?.expiredTime)
            ? ` ngày ${moment(
                bankTransferInfo?.transferInfo.expiredTime,
                C_DATE_TIME_FORMAT
              ).format(DATE_FORMAT)}.`
            : ""}
        </Typography>
      </Box>
      <Box className={classes.borderBox}>
        <Box>
          <Typography variant="subtitle1" className={classes.font18}>
            {listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER}
          </Typography>
        </Box>

        <Box>
          <Box className={classes.lineBox}>
            <Box className={classes.lineLeft}>
              <Typography variant="caption">
                {listString.IDS_MT_TEXT_CONTENT_TRANSFER}
              </Typography>
            </Box>
            <Box display="flex" alignItems="center">
              <Typography
                variant="subtitle2"
                style={{ color: theme.palette.red.redDark5 }}
              >
                {bankTransferInfo?.transferInfo?.message}
              </Typography>
              <Box
                className={classes.copy}
                onClick={() => {
                  navigator.clipboard.writeText(
                    bankTransferInfo?.transferInfo?.message
                  );
                }}
              >
                <IconCopy style={{ marginRight: 4 }} />
                {listString.IDS_MT_TEXT_INFO_COPPY}
              </Box>
            </Box>
          </Box>
          <Box className={classes.lineBox}>
            <Box className={classes.lineLeft}>
              <Typography variant="caption">
                {listString.IDS_MT_TEXT_AMOUNT_MONEY}
              </Typography>
            </Box>
            <Box display="flex" alignItems="center">
              <Typography
                variant="subtitle2"
                style={{ color: theme.palette.red.redDark5 }}
              >
                {`${bankTransferInfo?.transferInfo?.totalAmount.formatMoney()}đ`}
              </Typography>
              <Box
                className={classes.copy}
                onClick={() => {
                  navigator.clipboard.writeText(
                    bankTransferInfo?.transferInfo?.totalAmount.formatMoney()
                  );
                }}
              >
                <IconCopy style={{ marginRight: 4 }} />
                {listString.IDS_MT_TEXT_INFO_COPPY}
              </Box>
            </Box>
          </Box>
          {!isEmpty(bankTransferInfo?.transferOptions) &&
            bankTransferInfo?.transferOptions.map((el, index) => (
              <>
                <Box className={classes.lineBox} mt={2}>
                  <Box className={classes.lineLeft}>
                    <Typography variant="caption">
                      {listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_BANK}
                    </Typography>
                  </Box>
                  <Box display="flex" alignItems="center">
                    <Typography variant="subtitle2">{el.bankName}</Typography>
                    <Box
                      bgcolor={theme.palette.white.main}
                      border={`solid 1px ${theme.palette.gray.grayLight23}`}
                      padding={2 / 8}
                      marginLeft={12 / 8}
                      borderRadius={4}
                    >
                      <Image
                        srcImage={el?.bankLogo}
                        className={classes.iconBank}
                      />
                    </Box>
                  </Box>
                </Box>
                <Box className={classes.lineBox}>
                  <Box className={classes.lineLeft}>
                    <Typography variant="caption">
                      {
                        listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_NUMBER_ACCOUNT
                      }
                    </Typography>
                  </Box>
                  <Box display="flex" alignItems="center">
                    <Typography variant="subtitle2">
                      {el?.accountNumber}
                    </Typography>
                    <Box
                      className={classes.copy}
                      onClick={() => {
                        navigator.clipboard.writeText(el?.accountNumber);
                      }}
                    >
                      <IconCopy style={{ marginRight: 4 }} />
                      {listString.IDS_MT_TEXT_INFO_COPPY}
                    </Box>
                  </Box>
                </Box>
                <Box className={classes.lineBox}>
                  <Box className={classes.lineLeft}>
                    <Typography variant="caption">
                      {
                        listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_NAME_ACCOUNT
                      }
                    </Typography>
                  </Box>
                  <Box display="flex" alignItems="center">
                    <Typography variant="subtitle2">
                      {el?.accountName}
                    </Typography>
                  </Box>
                </Box>
              </>
            ))}
        </Box>
      </Box>
    </Box>
  );
};
OrderBankTransfer.propTypes = {
  item: PropTypes.object.isRequired,
};
export default OrderBankTransfer;
