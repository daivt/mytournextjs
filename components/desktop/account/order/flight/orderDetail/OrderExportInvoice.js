import { Box, makeStyles, Typography, Grid } from "@material-ui/core";
import { IconDownloadInvoice } from "@public/icons";
import Link from "@src/link/Link";
import { listString } from "@utils/constants";
import { getInvoiceStatus, isEmpty } from "@utils/helpers";
const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    paddingBottom: 4,
  },
  contentBold: {
    fontWeight: 600,
  },
  font18: {
    fontSize: 18,
    lineHeight: "22px",
  },
  ellipsisText: {
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    maxWidth: "250px",
  },
  attachInvoice: {
    color: theme.palette.blue.blueLight8,
    display: "flex",
  },
}));

function renderLine(textLeft, textRight) {
  const classes = useStyles();
  return (
    <Grid container>
      <Grid item lg={3} md={3} sm={3} xs={3}></Grid>
      <Grid item lg={3} md={3} sm={3} xs={3}>
        <Typography variant="caption" className={classes.content}>
          {textLeft}:
        </Typography>
      </Grid>
      <Grid item lg={6} md={6} sm={6} xs={6}>
        <Typography variant="caption" className={classes.content}>
          {textRight}
        </Typography>
      </Grid>
    </Grid>
  );
}

const OrderExportInvoice = ({ invoice = {}, paymentStatus = {} }) => {
  const classes = useStyles();
  const status = getInvoiceStatus(paymentStatus?.type, invoice?.status || "");

  return (
    <>
      <Grid container>
        <Grid item lg={3} md={3} sm={3} xs={3}>
          <Typography variant="subtitle1" className={classes.font18}>
            {listString.IDS_MT_TEXT_INFO_INVOICE}
          </Typography>
        </Grid>
        <Grid item lg={3} md={3} sm={3} xs={3}>
          <Typography variant="caption" className={classes.content}>
            {listString.IDS_TEXT_PLACEHOLDER_SEARCH_TAX_CODE}:
          </Typography>
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={6}>
          <Typography variant="caption" className={classes.content}>
            {invoice?.taxIdNumber}
          </Typography>
        </Grid>
      </Grid>
      {renderLine(
        listString.IDS_TEXT_PLACEHOLDER_INVOICE_NAME,
        invoice?.companyName
      )}
      {renderLine(
        listString.IDS_TEXT_PLACEHOLDER_INVOICE_ADDRESS,
        invoice?.companyAddress
      )}
      {renderLine(listString.IDS_MT_TEXT_CUSTOMER_NAME, invoice?.recipientName)}
      {renderLine(
        listString.IDS_TEXT_PLACEHOLDER_INVOICE_RECEIVER,
        invoice?.recipientAddress
      )}
      {renderLine(listString.IDS_MT_TEXT_EMAIL, invoice?.recipientEmail)}
      <Grid container>
        <Grid item lg={3} md={3} sm={3} xs={3}></Grid>
        <Grid item lg={3} md={3} sm={3} xs={3}>
          <Typography variant="caption" className={classes.content}>
            {listString.IDS_MT_TEXT_STATUS}:
          </Typography>
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={6}>
          <Typography
            variant="caption"
            className={classes.content}
            style={{ color: status?.textColor }}
          >
            {status?.text}
          </Typography>
        </Grid>
      </Grid>
      {!isEmpty(invoice?.outboundAttachLinks) && (
        <Grid container>
          <Grid item lg={3} md={3} sm={3} xs={3}></Grid>
          <Grid item lg={3} md={3} sm={3} xs={3}>
            <Typography variant="caption" className={classes.content}>
              {listString.IDS_MT_TEXT_INVOICE_OUTBOUND}:
            </Typography>
          </Grid>
          <Grid item lg={6} md={6} sm={6} xs={6}>
            <Link
              href={invoice?.outboundAttachLinks[0]}
              className={classes.attachInvoice}
            >
              <Typography variant="caption" className={classes.content}>
                <IconDownloadInvoice style={{ marginRight: 10 }} />
                {listString.IDS_MT_TEXT_INVOICE_ATTACH_OUTBOUND_NAME}
              </Typography>
            </Link>
          </Grid>
        </Grid>
      )}
      {!isEmpty(invoice?.inboundAttachLinks) && (
        <Grid container>
          <Grid item lg={3} md={3} sm={3} xs={3}></Grid>
          <Grid item lg={3} md={3} sm={3} xs={3}>
            <Typography variant="caption" className={classes.content}>
              {listString.IDS_MT_TEXT_INVOICE_INBOUND}:
            </Typography>
          </Grid>
          <Grid item lg={6} md={6} sm={6} xs={6}>
            <Link
              href={invoice?.inboundAttachLinks[0]}
              className={classes.attachInvoice}
            >
              <Typography variant="caption" className={classes.content}>
                <IconDownloadInvoice style={{ marginRight: 10 }} />
                {listString.IDS_MT_TEXT_INVOICE_ATTACH_INBOUND_NAME}
              </Typography>
            </Link>
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default OrderExportInvoice;
