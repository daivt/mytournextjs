import WayFlightItemDetail from "@components/desktop/account/order/flight/WayFlightItemDetail";
import { Box, Typography, useTheme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconArrowDownToggle } from "@public/icons";
import ButtonComponent from "@src/button/Button";
import Image from "@src/image/Image";
import { listString, routeStatic } from "@utils/constants";
import {
  getExpiredTimeMillisecond,
  getPaymentStatusBookingFlight,
  isEmpty,
} from "@utils/helpers";
import clsx from "clsx";
import moment from "moment";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  container: {
    width: "100%",
    maxWidth: "1188px",
    display: "flex",
    margin: "0 auto",
    flexDirection: "column",
    backgroundColor: theme.palette.white.main,
    borderRadius: 8,
    boxShadow: "0px 3px 3px rgba(0, 0, 0, 0.05)",
    padding: "0px 16px 16px 16px",
    color: theme.palette.black.black3,
  },
  header: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  iconArrowDownToggle: {
    transform: "rotate(270deg)",
    stroke: theme.palette.blue.blueLight8,
    marginLeft: 10,
    cursor: "pointer",
  },
  divider: {
    height: 1,
    backgroundColor: theme.palette.gray.grayLight22,
    width: "100%",
    margin: "12px 0",
  },
  dividerFull: {
    width: "calc(100% + 32px)",
    margin: "0 -16px 16px",
  },
  infoBooking: {
    display: "flex",
    flexDirection: "column",
  },
  status: {
    display: "flex",
    alignItems: "center",
    borderRadius: 4,
    height: 24,
    lineHeight: "17px",
    padding: "0 8px",
    textTransform: "uppercase",
    color: theme.palette.white.main,
  },
  airlineName: {
    color: theme.palette.black.black3,
    whiteSpace: "nowrap",
    lineHeight: "17px",
  },
  flightCode: {
    color: theme.palette.gray.grayDark8,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    lineHeight: "17px",
  },
  airlineLogo: {
    width: 32,
    height: 32,
    marginRight: 16,
  },
}));

const FlightBookingItem = ({ item = {}, airlines = {} }) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();

  const paymentStatus = getPaymentStatusBookingFlight(
    item.paymentStatus,
    item.paymentMethodCode,
    item?.expiredTime || 0
  );
  const [airlineOutBoundInfo, setAirlineOutBoundInfo] = useState({});
  const [airlineInBoundInfo, setAirlineInBoundInfo] = useState({});

  const getAirlineInfo = () => {
    const tempOutBoundInfo = airlines.find(
      (al) => al.aid === item.outbound?.airlineId
    );
    setAirlineOutBoundInfo(tempOutBoundInfo);
    if (item.isTwoWay) {
      const tempInBoundInfo = airlines.find(
        (al) => al.aid === item.inbound?.airlineId
      );
      setAirlineInBoundInfo(tempInBoundInfo);
    }
  };
  useEffect(() => {
    if (!isEmpty(airlines)) {
      getAirlineInfo();
    }
  }, []);

  return (
    <Box
      className={classes.container}
      onClick={() => {
        router.push({
          pathname: routeStatic.ACCOUNT_ORDER_FLIGHT_DETAIL.href,
          query: {
            id: item?.orderCode.substr(1),
          },
        });
      }}
    >
      <Box className={classes.header}>
        <Box display="flex" alignItems="center">
          <Typography
            variant="caption"
            style={{ lineHeight: "17px" }}
          >{`${listString.IDS_TEXT_FLIGHT_ORDER_CODE}: `}</Typography>
          <Typography
            variant="subtitle2"
            style={{
              lineHeight: "17px",
              color: theme.palette.blue.blueLight8,
              paddingLeft: "2px",
            }}
          >{`${item.orderCode}`}</Typography>
        </Box>
        <Box display="flex" alignItems="center">
          <ButtonComponent
            color={theme.palette.blue.blueLight8}
            typeButton="text"
            width="fit-content"
            padding={0}
          >
            <Typography variant="caption" style={{ lineHeight: "17px" }}>
              {listString.IDS_MT_TEXT_VIEW_DETAIL}
            </Typography>
          </ButtonComponent>
          <IconArrowDownToggle
            className={`svgFillAll ${classes.iconArrowDownToggle}`}
          />
        </Box>
      </Box>
      <Box className={clsx(classes.divider, classes.dividerFull)} />
      <Box display="flex">
        <Box className={classes.wrapLeft} width="50%">
          <WayFlightItemDetail
            item={item.outbound}
            airlineInfo={airlineOutBoundInfo}
          />
          {item.isTwoWay && (
            <Box mt={2}>
              <WayFlightItemDetail
                item={item?.inbound}
                airlineInfo={airlineInBoundInfo}
              />
            </Box>
          )}
        </Box>
        <Box display="flex" justifyContent="space-between" width="50%">
          <Box display="flex" flexDirection="column">
            <Box display="flex" alignItems="center">
              <Image
                srcImage={airlineOutBoundInfo?.logo}
                className={classes.airlineLogo}
              />
              <Box display="flex" flexDirection="column">
                <Typography variant="caption" className={classes.airlineName}>
                  {airlineOutBoundInfo?.name}
                </Typography>
                <Typography variant="caption" className={classes.flightCode}>
                  {item?.outbound?.flightCode}
                </Typography>
              </Box>
            </Box>
            {item.isTwoWay && (
              <Box display="flex" alignItems="center" mt={36 / 8}>
                <Image
                  srcImage={airlineInBoundInfo?.logo}
                  className={classes.airlineLogo}
                />
                <Box display="flex" flexDirection="column">
                  <Typography variant="caption" className={classes.airlineName}>
                    {airlineInBoundInfo?.name}
                  </Typography>
                  <Typography variant="caption" className={classes.flightCode}>
                    {item?.inbound?.flightCode}
                  </Typography>
                </Box>
              </Box>
            )}
          </Box>
          <Box display="flex" flexDirection="column">
            <Box
              display="flex"
              flexDirection="column"
              mb={2}
              alignItems="flex-end"
            >
              <Typography variant="caption" style={{ lineHeight: "17px" }}>
                {listString.IDS_MT_TEXT_TOTAL_PRICE}
              </Typography>
              <Typography
                variant="subtitle1"
                style={{ paddingLeft: "2px", marginTop: "4px" }}
              >
                {`${item.finalPriceFormatted}`}
              </Typography>
            </Box>
            <Typography
              variant="subtitle2"
              className={classes.status}
              style={{ background: paymentStatus.color }}
            >
              {paymentStatus.title}
            </Typography>
            {item.expiredTime > 0 && item.expiredTime / 1000 > moment().unix() && (
              <Typography
                variant="caption"
                style={{
                  marginTop: 8,
                  color: paymentStatus.color,
                  lineHeight: "17px",
                }}
              >
                {`Còn ${getExpiredTimeMillisecond(item.expiredTime)}`}
              </Typography>
            )}
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
export default FlightBookingItem;
