import { Box, makeStyles, Typography } from "@material-ui/core";

import { isEmpty } from "@utils/helpers";
import { IconCopy, IconPaymentSettledHotel } from "@public/icons";
import { listString } from "@utils/constants";
import moment from "moment";
import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  wrapBox: {
    borderRadius: 8,
    border: `1px dashed ${theme.palette.gray.grayLight25}`,
    background: theme.palette.gray.grayLight26,
    marginTop: 12,
  },
  wrapContent: {
    margin: "0 12px",
  },
  bank: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
  },
  wrapBank: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    whiteSpace: "nowrap",
    fontSize: 14,
    lineHeight: "17px",
    margin: "12px 0 12px 0",
  },
  bankText: {
    fontWeight: 600,
    whiteSpace: "nowrap",
    overflow: "hidden !important",
    textOverflow: "ellipsis",
    width: 150,
  },
  infoBank: {
    borderBottom: `1px solid ${theme.palette.gray.grayLight23}`,
    marginBottom: 12,
  },
  branchText: { whiteSpace: "break-spaces  " },
  copy: {
    display: "flex",
    justifyContent: "flex-end",
    color: theme.palette.blue.blueLight8,
    paddingBottom: 11,
    alignItems: "center",
    cursor: "pointer",
    fontSize: 14,
    lineHeight: "16px",
  },
  bankItem: {
    borderRadius: 4,
    border: `1px solid ${theme.palette.gray.grayLight23}`,
    height: 24,
    width: 48,
    backgroundColor: theme.palette.white.main,
  },
  iconBank: {
    width: "100%",
    height: "100%",
    objectFit: "contain",
  },
}));
const InfoPaymentTransferBooking = ({
  transactionInfo = {},
  transferInfo = {},
  bookingDetail = {},
  transferOptions = [],
}) => {
  const classes = useStyles();
  const expiredTime = moment(transferInfo.expiredTime, "YYYY-MM-DD");
  const timePayment = transferInfo.expiredTime
    ? transferInfo.expiredTime
        .toString()
        .split(" ")[1]
        .split(":")
    : "";
  // let timeExpired = moment(transferInfo.expiredTime, "DD-MM-YYYY");
  return (
    <>
      <Box>
        <Box display="flex" flexDirection="column" alignItems="center">
          <IconPaymentSettledHotel />
          <Box
            component="span"
            fontWeight={600}
            fontSize={16}
            lineHeight="19px"
            p="12px 0 8px"
          >
            {listString.IDS_MT_TEXT_BOOKING_SUCCESS}
          </Box>
          <Box
            component="span"
            lineHeight="17px"
            p="2px 4px"
            color="green.greenLight7"
            borderRadius={4}
            border="1px solid #48BB78"
          >
            {`Mã đặt phòng: ${transactionInfo.bookingCode}`}
          </Box>
        </Box>
        <Box display="flex" flexDirection="column" pt={12 / 8}>
          <Box
            component="span"
            lineHeight="22px"
            fontSize={14}
            textAlign="center"
          >
            {`Hướng dẫn chuyển khoản sẽ được gửi tới email`}{" "}
            <Box component="span" lineHeight="22px" fontWeight={600}>
              {" "}
              {!isEmpty(bookingDetail.customer) && bookingDetail.customer.email}
              .
              <Box lineHeight="22px" fontWeight="normal">
                {`Vui lòng thanh toán trước`}
                <Box
                  component="span"
                  lineHeight="22px"
                  fontWeight={600}
                  color="#E53E3E"
                >
                  {" "}
                  {`${timePayment[0]}:${timePayment[1]}`}
                  {","}
                </Box>
                <Box
                  component="span"
                  lineHeight="22px"
                  fontWeight={600}
                  color="#E53E3E"
                >
                  {" "}
                  {expiredTime
                    ? expiredTime
                        .locale("vi_VN")
                        .format("ddd")
                        .replace("T2", "Thứ hai")
                        .replace("T3", "Thứ ba")
                        .replace("T4", "Thứ tư")
                        .replace("T5", "Thứ năm")
                        .replace("T6", "Thứ sáu")
                        .replace("T7", "Thứ bảy")
                        .replace("CN", "Chủ Nhật")
                    : "-"}
                  {","}
                  <Box component="span">
                    {" "}
                    ngày {expiredTime.format("DD")}/{expiredTime.format("MM")}/
                    {expiredTime.format("YYYY")}.
                  </Box>
                </Box>
              </Box>
              <Box lineHeight="22px" fontSize={14} fontWeight={600}>
                Quá thời hạn thanh toán trên, đơn phòng sẽ tự động bị hủy.
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
      <Box className={classes.wrapBox}>
        <Box className={classes.wrapContent}>
          <Box className={classes.infoBank}>
            <Typography variant="subtitle2" style={{ marginTop: 12 }}>
              {listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER}
            </Typography>
            {transferOptions.map((el, index) => (
              <Box key={index}>
                <Box className={classes.wrapBank}>
                  <Box className={classes.bank}>
                    {listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_BANK}
                  </Box>
                  <Box display="flex" alignItems="center">
                    <Box component="span" className={classes.bankText}>
                      {el.bankName}
                    </Box>
                    <Box className={classes.bankItem}>
                      <Image
                        srcImage={el.bankLogo}
                        className={classes.iconBank}
                      />
                    </Box>
                  </Box>
                </Box>
                {/* <Box className={classes.wrapBank}>
                  <Box className={classes.branch}>
                    {listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_BRANCH}
                  </Box>
                  <Typography
                    className={classes.branchText}
                    variant="subtitle2"
                  ></Typography>
                </Box> */}
                <Box className={classes.wrapBank}>
                  <Box className={classes.branch}>
                    {
                      listString.IDS_MT_TEXT_INFO_PAYMENT_TRANSFER_NUMBER_ACCOUNT
                    }
                  </Box>
                  <Typography
                    className={classes.branchText}
                    variant="subtitle2"
                  >
                    {el.accountNumber}
                  </Typography>
                </Box>
                <Box
                  className={classes.copy}
                  onClick={() => {
                    navigator.clipboard.writeText(el.accountNumber);
                  }}
                >
                  <IconCopy style={{ marginRight: 6 }} />
                  {listString.IDS_MT_TEXT_INFO_COPPY}
                </Box>
              </Box>
            ))}
          </Box>

          <Box className={classes.wrapBank}>
            <Box className={classes.branch}>
              {listString.IDS_MT_TEXT_AMOUNT_MONEY}
            </Box>
            {!isEmpty(transferInfo) && (
              <Typography className={classes.branchText} variant="subtitle2">
                {`${transferInfo.totalAmount.formatMoney()}đ`}
              </Typography>
            )}
          </Box>
          <Box
            className={classes.copy}
            onClick={() => {
              navigator.clipboard.writeText(transferInfo.totalAmount);
            }}
          >
            <IconCopy style={{ marginRight: 6 }} />
            {listString.IDS_MT_TEXT_INFO_COPPY}
          </Box>
          <Box className={classes.wrapBank}>
            <Box className={classes.branch}>
              {listString.IDS_MT_TEXT_CONTENT_TRANSFER}
            </Box>
            <Typography
              className={classes.branchText}
              variant="subtitle2"
              style={{ textAlign: "right", textTransform: "uppercase" }}
            >
              {transferInfo.message}
            </Typography>
          </Box>
          <Box
            className={classes.copy}
            onClick={() => {
              navigator.clipboard.writeText(transferInfo.message);
            }}
            style={{ border: "none" }}
          >
            <IconCopy style={{ marginRight: 6 }} />
            {listString.IDS_MT_TEXT_INFO_COPPY}
          </Box>
        </Box>
      </Box>
    </>
  );
};
export default InfoPaymentTransferBooking;
