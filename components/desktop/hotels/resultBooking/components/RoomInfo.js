import { Box, Typography } from "@material-ui/core";
import { useRouter } from "next/router";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/styles";
import { listString } from "@utils/constants";
import { IconClock, IconCheck } from "@public/icons";
import moment from "moment";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  roomContainer: { width: "100%" },
  content: {
    width: "100%",
    borderRadius: 12,
    padding: "40px 24px 0 24px",
    display: "flex",
    flexDirection: "column",
    marginTop: -12,
  },
  divider: {
    width: "100%",
    height: 4,
    background: "#EDF2F7",
    margin: "12px 0",
  },
  imgHotel: { width: 432, height: 114, borderRadius: 8 },
  IconUser2: {
    width: 12,
    height: 12,
  },
  roomName: {
    fontSize: 18,
    fontWeight: 600,
    lineHeight: "21px",
  },
  lastBooking: {
    marginTop: 12,
    borderRadius: 8,
    background: theme.palette.blue.blueLight9,
    color: theme.palette.primary.main,
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
  },
  agreeRoom: {
    background: theme.palette.pink.pink1,
    color: theme.palette.pink.main,
    fontWeight: 400,
  },
  lastTime: {
    fontWeight: 600,
    marginLeft: 2,
  },
  iconClock: {
    stroke: theme.palette.primary.main,
    margin: "0 10px 0 14px",
  },
  iconCheck: {
    stroke: theme.palette.pink.main,
    margin: "0 10px 0 14px",
  },
}));

const RoomInfo = () => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const checkIn = moment(router.query.checkIn, "DD-MM-YYYY");
  const checkOut = moment(router.query.checkOut, "DD-MM-YYYY");

  return (
    <Box className={classes.roomContainer}>
      <Box className={classes.content}>
        <Box className={classes.lastBooking}>
          <Box
            display="flex"
            alignItems="center"
            style={{ padding: "8px 0 7px" }}
          >
            <Box>
              <IconClock className={`svgFillAll ${classes.iconClock}`} />
            </Box>
            {listString.IDS_MT_TEXT_BOOKING_LAST_TIME}
            <Box className={classes.lastTime}>aaa</Box>
          </Box>
        </Box>
        <Box className={clsx(classes.lastBooking, classes.agreeRoom)}>
          <Box
            display="flex"
            alignItems="center"
            style={{ padding: "8px 0 7px" }}
          >
            <Box>
              <IconCheck className={`svgFillAll ${classes.iconCheck}`} />
            </Box>
            <Box style={{ marginLeft: "2px" }}>
              {listString.IDS_MT_TEXT_AGREE_ROOM_EARLY}
            </Box>
          </Box>
        </Box>
        <Box className={clsx(classes.lastBooking, classes.agreeRoom)}>
          <Box
            display="flex"
            alignItems="center"
            style={{ padding: "8px 0 7px" }}
          >
            <Box style={{ margin: "0 12px 0 12px" }}>
              Đừng bỏ lỡ! Chúng tôi chỉ còn 5 phòng có giá này. Hãy đặt ngay!
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default RoomInfo;
