import PolicyRoomModal from "@components/desktop/hotels/details/components/PolicyRoomModal";
import {
  Box,
  Grid,
  makeStyles,
  Typography,
  withStyles,
  Dialog,
  Slide,
} from "@material-ui/core";
import MuiAccordion from "@material-ui/core/Accordion";
import MuiAccordionDetails from "@material-ui/core/AccordionDetails";
import MuiAccordionSummary from "@material-ui/core/AccordionSummary";
import {
  IconArrowDownSmall,
  IconBed,
  IconBreakFast,
  IconCancelPolicy,
  IconUser2,
  IconInfoRateRoom,
  IconDot,
} from "@public/icons";
import Image from "@src/image/Image";
import { listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import clsx from "clsx";
import moment from "moment";
import { forwardRef, useState } from "react";

const useStyles = makeStyles((theme) => ({
  wrapContent: {
    display: "flex",
    justifyContent: "space-between",
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  roomInfo: {
    width: "70%",
  },
  rateContainer: {
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    padding: "10px 0",
  },
  textCancelPolicy: {
    fontSize: 14,
    lineHeight: "17px",
    fontWeight: "normal",
    color: theme.palette.gray.grayDark8,
    marginLeft: 6,
  },
  textFreeBreakFast: {
    color: theme.palette.green.greenLight7,
  },
  boxLeft: { display: "flex", marginTop: 8, alignItems: "center" },
  boxRight: { display: "flex", whiteSpace: "nowrap" },
  iconBreakFast: {
    stroke: theme.palette.gray.grayDark8,
  },
  iconBed: {
    stroke: theme.palette.blue.blueLight8,
    color: theme.palette.blue.blueLight8,
  },
  numberAllotment: {
    color: theme.palette.red.redLight5,
    marginLeft: 0,
    textAlign: "left",
  },
  wrapRoomAndPrice: {
    fontSize: 12,
    color: theme.palette.black.black3,
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    width: "100%",
    justifyContent: "center",
  },
  warpIconShowPriceMobile: {
    display: "flex",
    background: theme.palette.blue.blueLight9,
    width: 107,
    height: 32,
    borderRadius: 4,
    color: theme.palette.primary.main,
    alignItems: "center",
    marginTop: 6,
  },
  wrapShowPriceMobile: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  freeCancelPolicy: {
    stroke: theme.palette.green.greenLight7,
  },
  contentRate: {
    paddingBottom: 12,
  },
  info: {
    cursor: "pointer",
    marginLeft: 4,
    stroke: theme.palette.gray.grayDark8,
    width: 16,
    height: 16,
  },
  infoActive: {
    width: 16,
    height: 16,
    marginLeft: 4,
    stroke: theme.palette.green.greenLight7,
    cursor: "pointer",
  },
  wrapRoomName: {
    display: "flex",
    alignItems: "center",
    color: theme.palette.black.black3,
  },
  infoRoom: {
    display: "flex",
    alignItems: "center",
    marginTop: 8,
  },
  IconBed: { width: 16, height: 16 },
  IconUser2: { width: 14, height: 14, marginLeft: 1 },
  wrapFreeCancelPolicy: { marginTop: 8, display: "flex" },
  textFreeCancelPolicy: {
    marginLeft: 6,
    fontWeight: "normal",
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark8,
  },
  policy: { width: 16, height: 16 },
  iconCar: { stroke: theme.palette.gray.grayDark8 },
  wrapRatingStar: { display: "flex", margin: "8px 12px" },
  imgRooms: { width: "30%", height: 128, marginBottom: 20 },
  infoPrice: { padding: "20px 0 " },
  accordionSum: { padding: 0 },
  content: { flexGrow: 0, justifyContent: "flex-start" },
  imageWrapper: { borderRadius: 8 },
  moreIcon: { stroke: theme.palette.blue.blueLight8, marginLeft: -4 },
  wrapContainer: {
    display: "flex",
    flexDirection: "column",
    fontSize: 14,
    color: theme.palette.black.black3,
    width: "100%",
  },
  itemPrice: {
    display: "flex",
    justifyContent: "space-between",
    lineHeight: "17px",
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
    padding: "14px 0 12px",
    position: "relative",
  },
  iconArrowRight: {
    transform: "rotate(270deg)",
    stroke: theme.palette.blue.blueLight8,
  },
  textUppercase: { textTransform: "uppercase" },
  totalPrice: { borderBottom: "none", marginBottom: 21 },
  boxFotter: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    zIndex: 1000,
    bottom: 0,
    left: 0,
    position: "fixed",
    color: "#000",
    background: "#fff",
    width: "100%",
    padding: "8px 16px",
    borderTop: "2px solid #EDF2F7",
  },
  promo: {
    border: `1px solid ${theme.palette.gray.grayLight24}`,
    padding: "4px 6px",
    marginLeft: 8,
    borderRadius: 8,
  },
  textPromo: { fontSize: 14, lineHeight: "17px", display: "flex" },
  wrapPromo: {
    display: "flex",
    alignItems: "center",
    marginTop: 3,
  },
  priceBeforePromo: {
    fontSize: 12,
    fontWeight: 400,
    lineHeight: "14px",
    color: theme.palette.gray.grayDark7,
    textDecoration: "line-through",
    marginLeft: 6,
  },
  priceInfo: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    textAlign: "right",
  },
  boxFinalPrice: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  requestSpecial: {
    display: "flex",
    flexDirection: "column",
  },
  userName: {
    marginTop: 8,
    lineHeight: "17px",
  },
  includedVat: {
    fontSize: 12,
    lineHeight: "14px",
    color: theme.palette.gray.grayDark7,
    fontWeight: 400,
    marginTop: 2,
    paddingBottom: 14,
  },
}));
const arrStar = [1, 2, 3, 4, 5];
const listTypeModal = {
  MODAL_CANCEL_POLICY: "MODAL_CANCEL_POLICY",
};
const Accordion = withStyles({
  rounded: {
    borderRadius: 0,
    boxShadow: "none",
  },
  expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
  root: {
    padding: 0,
    minHeight: 0,
    justifyContent: "flex-start",
  },
  content: {
    flexGrow: 0,
    justifyContent: "flex-start",
    margin: 0,
    paddingRight: 10,
  },
  expandIcon: {
    padding: 0,
  },
  expanded: {
    flexGrow: 0,
    justifyContent: "flex-start",
    margin: 0,
    paddingRight: 10,
  },
})(MuiAccordionSummary);
const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
const AccordionDetails = withStyles((theme) => ({}))(MuiAccordionDetails);

const RateRoomResultBooking = ({
  hotelDetail = {},
  handleViewRoomRate = () => {},
  handleBookRoomRate = () => {},
  showBedGroup = false,
  bookingDetail = {},
}) => {
  const classes = useStyles();
  const [visibleDrawerType, setVisibleDrawerType] = useState("");
  const [expanded, setExpanded] = useState(false);
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const toggleDrawer = (open = "") => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setVisibleDrawerType(open);
  };
  const roomBookings = !isEmpty(bookingDetail.roomBookings)
    ? bookingDetail.roomBookings
    : [];
  const roomBooking = !isEmpty(roomBookings) ? roomBookings[0] : {};
  let hasBookingDetails = !isEmpty(bookingDetail.roomBookings)
    ? bookingDetail.roomBookings[0]
    : {};
  const getNight = () => {
    let checkIn = moment(bookingDetail.checkIn, "DD-MM-YYYY");
    let checkOut = moment(bookingDetail.checkOut, "DD-MM-YYYY");
    if (checkIn && checkOut) {
      return Math.abs(
        moment(checkIn)
          .startOf("day")
          .diff(checkOut.startOf("day"), "days")
      );
    }
    return 1;
  };

  // Tổng giá chưa app mã khuyến mãi
  const totalPrice = () => {
    if (bookingDetail.numRooms && bookingDetail.price && getNight()) {
      return bookingDetail.numRooms * getNight() * bookingDetail.price;
    } else {
      return 0;
    }
  };

  // Tổng giá các đêm
  const nightPrice = () => {
    if (bookingDetail.numRooms && bookingDetail.basePrice && getNight()) {
      return bookingDetail.numRooms * getNight() * bookingDetail.basePrice;
    } else {
      return 0;
    }
  };

  // Giảm giá markup
  const getDiscount = () => {
    return roomBookings
      .map((el) => Math.abs(el.totalDiscount))
      .reduce((a, b) => a + b, 0);
  };

  // Thuế và dịch vụ khách hàng

  const vatPrice = () => {
    return totalPrice() - nightPrice() + getDiscount() - totalSurcharge();
  };

  // Phụ phí
  const totalSurcharge = () => {
    return roomBookings
      .map((el) => el.totalSurcharge)
      .reduce((a, b) => a + b, 0);
  };

  const getListBed = (bedTypes = []) => {
    let listBed = "";
    bedTypes.forEach((el, index) => {
      listBed = `${listBed}${index !== 0 ? " hoặc " : ""}${el}`;
    });
    return listBed;
  };

  const getTextShortCancelPolicies = () => {
    if (hasBookingDetails.freeCancellation) {
      return "Miễn phí hoàn hủy";
    } else if (hasBookingDetails.refundable) {
      return "Có thể hoàn hủy";
    } else {
      return "Không hoàn hủy";
    }
  };
  return (
    <>
      <Box className={classes.wrapContent}>
        <Box className={classes.roomInfo}>
          <Box className={clsx(classes.wrapRoomName)}>
            <Typography
              variant="subtitle1"
              component="span"
              style={{ marginRight: "3px" }}
            >
              {`${!isEmpty(bookingDetail.numRooms) && bookingDetail.numRooms}x`}
            </Typography>
            <Typography variant="subtitle1">
              {hasBookingDetails.name}
            </Typography>
          </Box>
          <Box className={classes.infoRoom}>
            <>
              <IconUser2 className={classes.IconUser2} />
              <Box marginLeft={1} className={classes.textCancelPolicy}>
                {`${parseInt(bookingDetail?.numAdult) +
                  parseInt(bookingDetail?.numChildren)} người`}
              </Box>
            </>
          </Box>
          {!isEmpty(hasBookingDetails.bedTypes) && (
            <Box
              display="flex"
              alignItems="center"
              style={{ marginTop: "10px" }}
            >
              <IconBed className={classes.iconBed} />
              <Box marginLeft={1} className={classes.textCancelPolicy}>
                {getListBed(hasBookingDetails.bedTypes)}
              </Box>
            </Box>
          )}
          {hasBookingDetails?.freeBreakfast ? (
            <Box mt={1} style={{ display: "flex", alignItems: "center" }}>
              <IconBreakFast />
              <Typography
                className={clsx(
                  classes.textCancelPolicy,
                  classes.textFreeBreakFast
                )}
              >
                {listString.IDS_MT_TEXT_ROOM_FREE_BREAK_FAST}
              </Typography>
            </Box>
          ) : (
            <Box mt={1} style={{ display: "flex", alignItems: "center" }}>
              <IconBreakFast
                className={`svgFillAll ${classes.iconBreakFast}`}
              />
              <Typography className={classes.textCancelPolicy}>
                {listString.IDS_MT_TEXT_ROOM_NO_FREE_BREAK_FAST}
              </Typography>
            </Box>
          )}

          {hasBookingDetails?.freeCancellation ? (
            <>
              <Box
                mt={1}
                style={{ display: "flex", alignItems: "center" }}
                onClick={handleOpen}
              >
                <IconCancelPolicy
                  className={`svgFillAll ${classes.freeCancelPolicy}`}
                />
                <Typography
                  className={classes.textCancelPolicy}
                  style={{ color: "#48BB78" }}
                >
                  {listString.IDS_MT_TEXT_FREE_CANCEL}
                </Typography>
                <IconInfoRateRoom
                  className={`svgFillAll ${classes.infoActive}`}
                />
              </Box>
              <Typography variant="body2" style={{ marginLeft: "22px" }}>
                {!isEmpty(hasBookingDetails.hotelCancellationPolicies) &&
                  hasBookingDetails.hotelCancellationPolicies.map((el, i) => (
                    <Box component="span" color="gray.grayDark8" key={i}>
                      {el.price === el.refundAmount &&
                        `Trước ${el.dateTo.split(" ")[0]}`}
                    </Box>
                  ))}
              </Typography>
            </>
          ) : (
            <Box mt={1} style={{ display: "flex", alignItems: "center" }}>
              <IconCancelPolicy />
              <Typography className={classes.textCancelPolicy}>
                {getTextShortCancelPolicies()}
              </Typography>
              {hasBookingDetails.refundable && (
                <IconInfoRateRoom
                  className={`svgFillAll ${classes.info}`}
                  onClick={handleOpen}
                />
              )}
            </Box>
          )}
        </Box>
        <Box className={classes.imgRooms}>
          <Image
            srcImage={bookingDetail.hotel.thumbnail.image.small}
            className={classes.imageWrapper}
            borderRadiusProp="8px"
          />
        </Box>
      </Box>

      {(roomBooking.isHighFloor || roomBooking.noSmoking) && (
        <Box
          className={classes.infoPrice}
          style={{ borderBottom: "1px solid #EDF2F7" }}
        >
          <Box className={classes.requestSpecial}>
            <Box fontWeight={600} fontSize={16} lineHeight="19px" pb={1}>
              {listString.IDS_MT_TEXT_REQUEST_SPECIAL}
            </Box>
            {roomBooking.isHighFloor && (
              <Box display="flex" alignItems="center">
                <IconDot />
                <Box component="span" lineHeight="24px" pl={1} fontSize={14}>
                  {listString.IDS_MT_ROOM_HIGH_LEVEL}
                </Box>
              </Box>
            )}
            {roomBooking.noSmoking && (
              <Box display="flex" alignItems="center">
                <IconDot />
                <Box component="span" lineHeight="24px" pl={1} fontSize={14}>
                  {listString.IDS_MT_NO_SMOKE_ROOM}
                </Box>
              </Box>
            )}
          </Box>
        </Box>
      )}
      {!isEmpty(roomBooking.specialRequest) && (
        <Box className={classes.infoPrice}>
          <Box className={classes.userName}>
            <Box
              fontWeight={600}
              fontSize={16}
              lineHeight="19px"
              component="span"
            >
              {`${listString.IDS_MT_TEXT_BOOKING_REQUEST_PRIVATE}:`}
            </Box>
            <Box component="span" pl={2 / 8}>
              {roomBooking.specialRequest}
            </Box>
          </Box>
        </Box>
      )}

      <Box className={classes.infoPrice}>
        <Accordion>
          <Grid container>
            <Grid item sm={10} md={10} lg={10}>
              <AccordionSummary
                expandIcon={
                  <IconArrowDownSmall
                    className={`svgFillAll ${classes.moreIcon}`}
                  />
                }
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography variant="subtitle2" className={classes.heading}>
                  {listString.IDS_MT_TEXT_PLEASE_TOTAL_MONEY}
                </Typography>
              </AccordionSummary>
              <Box className={classes.includedVat}>
                {bookingDetail.includedVAT
                  ? "Đã bao gồm thuế, phí, VAT"
                  : "Giá chưa bao gồm VAT"}
              </Box>
            </Grid>
            <Grid item sm={2} md={2} lg={2} className={classes.boxFinalPrice}>
              <Typography variant="subtitle1">
                {`${!isEmpty(bookingDetail.finalPrice) &&
                  bookingDetail.finalPrice.formatMoney()}đ`}
              </Typography>
            </Grid>
          </Grid>

          <AccordionDetails style={{ padding: 0 }}>
            <Box className={classes.wrapContainer}>
              <Box className={classes.itemPrice}>
                <Box component="span">{`${
                  bookingDetail.numRooms
                } phòng x ${getNight()} đêm`}</Box>
                <Box component="span" className={classes.priceInfo}>
                  <Box component="span">{`${nightPrice().formatMoney()}đ`}</Box>
                </Box>
              </Box>

              {totalSurcharge() > 0 && (
                <Box className={classes.itemPrice}>
                  <Box component="span">{`Phụ phí`}</Box>
                  {`${totalSurcharge().formatMoney()}đ`}
                </Box>
              )}

              {getDiscount() > 0 && (
                <Box className={classes.itemPrice} color="green.greenLight7">
                  <Box component="span">{`Chúng tôi khớp giá, giảm thêm`}</Box>
                  {`-${getDiscount().formatMoney()}đ`}
                </Box>
              )}

              {vatPrice() > 2 && (
                <Box className={classes.itemPrice}>
                  <Box component="span">{`${
                    bookingDetail.includedVAT ? "Thuế và phí " : "Phí "
                  }dịch vụ khách sạn`}</Box>
                  {`${vatPrice().formatMoney()}đ`}
                </Box>
              )}

              {!isEmpty(bookingDetail.paymentMethodFee) &&
                bookingDetail.paymentMethodFee > 0 && (
                  <Box className={classes.itemPrice}>
                    <Box component="span">{`Phí giao dịch ngân hàng`}</Box>
                    <Box component="span">{`${bookingDetail.paymentMethodFee.formatMoney()}đ`}</Box>
                  </Box>
                )}

              <Box className={classes.itemPrice}>
                <Box component="span" className={classes.wrapPromo}>
                  {listString.IDS_TEXT_VOUCHER}{" "}
                  {!isEmpty(bookingDetail.promotionCode) && (
                    <Box component="span" className={classes.promo}>
                      <Typography
                        component="span"
                        className={classes.textPromo}
                        style={{
                          color: "#1A202C",
                        }}
                      >
                        {bookingDetail.promotionCode}
                      </Typography>
                    </Box>
                  )}
                </Box>
                <Box display="flex" alignItems="center" color="blue.blueLight8">
                  <Typography
                    component="span"
                    className={classes.textPromo}
                    style={{
                      color: "#48BB78",
                    }}
                  >
                    {`${
                      bookingDetail?.discount > 0 ? "-" : ""
                    }${bookingDetail?.discount?.formatMoney()}đ`}
                  </Typography>
                </Box>
              </Box>
            </Box>
          </AccordionDetails>
        </Accordion>
      </Box>
      <Dialog
        maxWidth="sm"
        open={open}
        onClose={() => setOpen(false)}
        TransitionComponent={Transition}
      >
        <PolicyRoomModal item={hasBookingDetails} handleClose={handleClose} />
      </Dialog>
    </>
  );
};

export default RateRoomResultBooking;
