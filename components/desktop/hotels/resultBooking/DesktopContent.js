import { Box, Divider, Button, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Layout from "@components/layout/desktop/Layout";
import {
  listString,
  routeStatic,
  listEventHotel,
  LAST_BOOKING_HOTEL,
  flightPaymentMethodCode,
} from "@utils/constants";
import { useRouter } from "next/router";
import * as gtm from "@utils/gtm";
import { isEmpty } from "@utils/helpers";
import { useEffect, useState } from "react";
import Link from "@src/link/Link";
import ButtonComponent from "@src/button/Button";
import { checkingPaymentStatus, getBookingInfo } from "@api/hotels";
import InfoHotel from "@components/desktop/hotels/resultBooking/components/InfoHotel";
import RateRoomResultBooking from "@components/desktop/hotels/resultBooking/components/RateRoomResultBooking";
import StatusResult from "./components/StatusResult";
import RoomInfo from "./components/RoomInfo";

const useStyles = makeStyles((theme) => ({
  homeWrapper: { width: "100%", background: "#EDF2F7", color: "#1A202C" },
  homeContainer: {
    maxWidth: 630,
    margin: "0 auto",
    padding: "32px 0",
    display: "flex",
    flexDirection: "column",
    background: "transparent",
  },
  cardContainer: {
    background: "white",
    width: "100%",
    borderRadius: 8,
    borderTop: "solid 4px #48BB78",
    position: "relative",
  },
  itemContainer: {
    padding: "16px 24px 0 24px",
  },
  wrapBorder: {
    display: "flex",
    justifyContent: "space-around",
  },
  circle: {
    width: 17,
    height: 17,
    transform: "rotate(-45deg)",
    borderRadius: "50%",
    borderLeftColor: "transparent",
    borderBottomColor: "transparent",
    background: theme.palette.gray.grayLight22,
    marginTop: -9,
  },
  boxDownload: {
    margin: "22px auto 30px",
    maxWidth: "630px",
    display: "flex",
    justifyContent: "center",
  },
}));
function WrapperContent({ children }) {
  const classes = useStyles();
  return (
    <Box className={classes.homeWrapper}>
      <Box className={classes.homeContainer}>{children}</Box>
    </Box>
  );
}
let timeIntervalPaymentStatus = null;
const TIME_CALL_API_STATUS = 10000;
const LIST_STATUS = {
  PENDING: "PENDING",
  SETTLED: "SETTLED",
  FAILED: "FAILED",
};

const ResultBooking = () => {
  const classes = useStyles();
  const router = useRouter();
  const [count, setCount] = useState(0);
  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState(LIST_STATUS.FAILED);
  const [bookingDetail, setBookingDetail] = useState({});
  const [transactionInfo, setTransactionInfo] = useState({});
  const [transferInfo, setTransferInfo] = useState({});
  const [transferOptions, setTransferOptions] = useState([]);

  const fetchBookingDetail = async (paramsInit = {}) => {
    try {
      const { data } = await getBookingInfo(paramsInit);
      if (data.code === 200) {
        setBookingDetail(data.data || {});
      }
    } catch (err) {}
  };
  const fetPaymentStatus = async () => {
    try {
      if (count === 0) setLoading(true);
      const { data } = await checkingPaymentStatus({
        transactionId: router.query.transactionId,
        gatewayParams: router.asPath.split("?")[1],
      });
      setCount(1);
      if (data.code === 200 && !isEmpty(data.data)) {
        const tempTransaction = data.data.transactionInfo || {};
        setTransactionInfo(tempTransaction);
        if (
          tempTransaction.paymentMethodInfo.code ===
          flightPaymentMethodCode.BANK_TRANSFER_2
        ) {
          setTransferInfo(data.data.transferInfo || {});
          setTransferOptions(data.data.transferOptions || {});
          localStorage.removeItem(LAST_BOOKING_HOTEL);
        }
        if (
          tempTransaction.status.toUpperCase() === LIST_STATUS.SETTLED ||
          tempTransaction.status.toUpperCase() === LIST_STATUS.FAILED ||
          tempTransaction.paymentMethodInfo.code ===
            flightPaymentMethodCode.BANK_TRANSFER_2
        ) {
          setStatus(tempTransaction.status);
          clearInterval(timeIntervalPaymentStatus);
          const paramsInit = {
            hashBookingId: tempTransaction.hashBookingId,
          };
          fetchBookingDetail(paramsInit);
          localStorage.removeItem(LAST_BOOKING_HOTEL);
          if (tempTransaction.status.toUpperCase() === LIST_STATUS.FAILED) {
            gtm.addEventGtm(listEventHotel.HotelPurchaseFailed);
          } else {
            gtm.addEventGtm(listEventHotel.HotelPurchaseCompleted, {
              amount: tempTransaction?.amount,
              bookingId: transactionInfo?.bookingCode?.substring(1),
            });
          }
        }
      }
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };
  useEffect(() => {
    fetPaymentStatus();
    timeIntervalPaymentStatus = setInterval(() => {
      fetPaymentStatus();
    }, TIME_CALL_API_STATUS);
    if (localStorage.getItem(LAST_BOOKING_HOTEL)) {
      localStorage.removeItem(LAST_BOOKING_HOTEL);
    }
    return () => {
      clearInterval(timeIntervalPaymentStatus);
    }; // eslint-disable-next-line
  }, []);
  if (isEmpty(bookingDetail)) return null;
  return (
    <Layout
      isHeader
      isFooter={false}
      type="booking"
      title="Đặt phòng khách sạn hạng sang giá rẻ - giá tốt nhất trên Mytour.vn | Đặt sát ngày giá càng tốt."
    >
      <WrapperContent>
        <Box className={classes.cardContainer}>
          <Box className={classes.itemContainer}>
            <StatusResult
              status={status}
              LIST_STATUS={LIST_STATUS}
              transactionInfo={transactionInfo}
              transferInfo={transferInfo}
              transferOptions={transferOptions}
              bookingDetail={bookingDetail}
            />
          </Box>
          <Divider />
          <Box className={classes.itemContainer}>
            {!isEmpty(bookingDetail) && (
              <InfoHotel bookingDetail={bookingDetail} />
            )}
          </Box>
          <Box className={classes.itemContainer} style={{ paddingTop: "0" }}>
            <RateRoomResultBooking
              hotelDetail={bookingDetail.hotel}
              bookingDetail={bookingDetail}
            />
          </Box>
        </Box>
        <Box className={classes.wrapBorder}>
          {Array(19)
            .fill(0)
            .map((it, index) => (
              <Box key={index} className={classes.circle} />
            ))}
        </Box>
        <Box className={classes.boxDownload}>
          <Link href="https://k5jr5.app.goo.gl/53g6">
            <img
              style={{ width: "630px", height: "96px" }}
              src="https://storage.googleapis.com/tripi-assets/mytour/banner/icon_download_banner.svg"
              alt=""
            />
          </Link>
        </Box>
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          pt={2}
          style={{ margin: "0 auto" }}
        >
          <Link
            href={{ pathname: routeStatic.HOME.href }}
            style={{ margin: "0 auto", textDecoration: "none" }}
          >
            <ButtonComponent
              typeButton="outlined"
              backgroundColor="#FF1284"
              color="#FFFFFF"
              width="fit-content"
              borderColor="#FF1284"
              borderRadius={8}
              padding="12px 50px"
            >
              <Typography variant="subtitle2">
                {listString.IDS_MT_TEXT_NOT_FOUND_403_DES_3}
              </Typography>
            </ButtonComponent>
          </Link>
        </Box>
      </WrapperContent>
    </Layout>
  );
};

export default ResultBooking;
