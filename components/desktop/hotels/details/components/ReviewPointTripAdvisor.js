import { Box, makeStyles, Typography, withStyles } from "@material-ui/core";
import clsx from "clsx";
import { Circle } from "rc-progress";
import { useState } from "react";
import { getMessageHotelReviews, isEmpty } from "@utils/helpers";
import LinearProgress from "@material-ui/core/LinearProgress";

const useStyles = makeStyles((theme) => ({
  wrapBox: {
    background: theme.palette.gray.grayLight26,
    borderRadius: 8,

    marginTop: 24,
  },
  wrapContent: {
    display: "flex",
    borderRadius: 8,
  },
  wrapHeader: {
    display: "flex",
    justifyContent: "space-between",
  },
  textEvaluate: {
    fontSize: 20,
    lineHeight: "23px",
    fontWeight: 600,
    color: theme.palette.black.black3,
  },
  writeEvaluate: {
    display: "flex",
    alignItems: "center",
  },
  textWriteEvaluate: {
    display: "flex",
    alignItems: "center",
    fontSize: 16,
    fontWeight: 400,
    lineHeight: "19px",
    color: theme.palette.blue.blueLight8,
    cursor: "pointer",
  },
  reviewText: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    display: "flex",
    whiteSpace: "nowrap",
    marginTop: 20,
  },
  countReview: {
    marginLeft: 3,
    color: theme.palette.gray.grayDark7,
  },
  countReviewActive: {
    marginLeft: 3,
    color: theme.palette.blue.blueLight8,
  },
  boxLeft: {
    width: "fit-content",
  },
  review: {
    width: "fit-content",
  },
  boxRight: {
    width: "100%",
    position: "relative",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
  },
  textReview: {
    display: "flex",
    whiteSpace: "nowrap",
    marginBottom: 24,
  },
  noMargin: {
    marginTop: 24,
    display: "flex",
  },
  itemReview: {
    whiteSpace: "nowrap",
    minHeight: "24px",
    paddingLeft: "20px",
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    marginTop: 20,
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    "&:hover": {
      color: theme.palette.blue.blueLight8,
    },
  },
  itemReviewActive: {
    borderLeft: `4px solid ${theme.palette.blue.blueLight8}`,
    color: theme.palette.blue.blueLight8,
  },
  iconPencil: {
    stroke: theme.palette.blue.blueLight8,
  },
  root: {
    position: "relative",
  },
  bottom: {
    color: "blue",
  },
  top: {
    color: "red",
    animationDuration: "550ms",
    position: "absolute",
    left: 0,
  },
  circle: {
    strokeLinecap: "round",
  },
  wrapBoxRight: {
    display: "flex",
    flexDirection: "column",
  },
  wrapBoxRightRating: {
    display: "flex",
    flexDirection: "column",
  },
  itemLevel: {
    marginTop: 10,
    fontSize: 14,
    color: theme.palette.black.black3,
    display: "flex",
    alignItems: "center",
  },
  nonMargin: {
    margin: 0,
  },
  nonMarginBottom: {
    marginTop: 10,
  },
  wrapProgressBar: {
    width: 330,
  },
  wrapProgressBarRating: {
    width: 128,
  },
  countEvaluate: {
    fontSize: 14,
    lineHeight: "16px",
    color: theme.palette.blue.blueLight8,
    textAlign: "left",
    marginLeft: 13,
  },

  countEvaluateRating: {
    fontSize: 14,
    lineHeight: "16px",
    color: theme.palette.black.black3,
    textAlign: "right",
    marginLeft: 10,
  },

  border: {
    width: 1,
    height: 185,
    background: theme.palette.gray.grayLight23,
  },
  levelMessage: {
    fontSize: 14,
    width: 70,
    marginRight: 9,
    lineHeight: "16px",
    fontWeight: 400,
  },
  levelMessageRating: {
    fontSize: 14,
    width: 61,
    marginRight: 9,
    lineHeight: "16px",
    fontWeight: 400,
  },

  wrapBoxRightText: {
    background: theme.palette.gray.grayLight26,
    borderRadius: 8,
    position: "relative",
    marginLeft: 25,
    marginTop: 13,
  },
  wrapQoute: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginLeft: 16,
  },
  boxRightContent: {
    display: "flex",
    marginLeft: 28,
  },
  modalHeader: {
    fontSize: 20,
    lineHeight: "23px",
    fontWeight: 600,
    display: "flex",
    justifyContent: "space-between",
    margin: "24px 0",
  },
  wrapConvient: {
    display: "flex",
    flexDirection: "column",
  },
  childListConvient: {
    fontSize: 14,
    lineHeight: "28px",
  },
  iconConven: {
    width: 24,
    height: 24,
  },
  modalContent: {
    height: 530,
    overflow: "auto",
    margin: "0 -24px",
    padding: "0 24px",
  },
}));
const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 6,
    borderRadius: 100,
    width: 331,
  },
  colorPrimary: {
    backgroundColor: theme.palette.gray.grayLight23,
  },
  bar: {
    borderRadius: 100,
    height: 6,
    backgroundColor: theme.palette.blue.blueLight8,
  },
}))(LinearProgress);

const RatingLinearProgress = withStyles((theme) => ({
  root: {
    height: 6,
    borderRadius: 100,
    width: 128,
  },
  colorPrimary: {
    backgroundColor: theme.palette.gray.grayLight23,
  },
  bar: {
    borderRadius: 100,
    height: 6,
    backgroundColor: theme.palette.gray.grayDark7,
  },
}))(LinearProgress);

const ReviewPointHotel = ({
  item = {},
  handelFilterRatingTrip = () => {},
  filterActiveTrip = null,
  hotelDetail = {},
}) => {
  const valueRatingHotel =
    !isEmpty(hotelDetail.rating) && !isEmpty(hotelDetail.rating.hotel)
      ? hotelDetail.rating.hotel.rating
      : 0;
  function CircularProgressWithLabel({ value }) {
    return (
      <Box style={{ position: "relative", width: 169, height: 169 }}>
        <Circle
          percent={valueRatingHotel * 10}
          strokeWidth="6"
          strokeColor="#00B6F3"
          trailWidth="6"
          trailColor="#E2E8F0"
          style={{ width: 169 }}
        />
        <Box
          style={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            textAlign: "center",
          }}
        >
          <Typography
            variant="caption"
            component="div"
            style={{
              fontWeight: 600,
              fontSize: 52,
              lineHeight: "62px",
              color: "#00B6F3",
            }}
          >
            {valueRatingHotel}
          </Typography>
          <Typography
            variant="caption"
            component="div"
            style={{ fontSize: 16, lineHeight: "19px", color: "#1A202C" }}
          >
            {getMessageHotelReviews(valueRatingHotel)}
          </Typography>
        </Box>
      </Box>
    );
  }

  const itemRating = !isEmpty(hotelDetail.rating)
    ? hotelDetail.rating.hotel
    : {};
  const getListRating = [
    { id: 0, name: "Vị trí", point: itemRating.locationRating },
    { id: 1, name: "Giá cả", point: itemRating.priceRating },
    { id: 2, name: "Phục vụ", point: itemRating.serviceRating },
    { id: 3, name: "Vệ sinh", point: itemRating.cleanlinessRating },
    { id: 4, name: "Tiện nghi", point: itemRating.convenientRating },
  ];
  const [progress, setProgress] = useState(10);
  const classes = useStyles();
  return (
    <>
      <Box className={classes.wrapBox}>
        <Box className={classes.wrapContent}>
          <Box className={classes.boxLeft}>
            <Box className={classes.review}>
              {!isEmpty(item.countByTravelType) &&
                item.countByTravelType.map((el, index) => (
                  <Box
                    className={clsx(
                      classes.itemReview,
                      filterActiveTrip === el.value && classes.itemReviewActive,
                      index === 0 && classes.noMargin,
                      index === item.countByTravelType.length - 1 &&
                        classes.textReview
                    )}
                    key={index}
                    onClick={() => handelFilterRatingTrip(el.value)}
                  >
                    {el.name}{" "}
                    <Box
                      component="span"
                      className={
                        filterActiveTrip === el.value
                          ? classes.countReviewActive
                          : classes.countReview
                      }
                    >
                      {" "}
                      ({el.count})
                    </Box>
                  </Box>
                ))}
            </Box>
          </Box>
          <Box className={classes.boxRight}>
            <Box style={{ marginRight: "0 40px" }}>
              <CircularProgressWithLabel value={50} />
            </Box>
            <Box className={classes.wrapBoxRight}>
              {!isEmpty(item.countByRatingLevel) &&
                item.countByRatingLevel.map((el, index) => (
                  <Box
                    className={clsx(
                      classes.itemLevel,
                      index === 0 && classes.nonMargin,
                      index === item.countByRatingLevel.length - 1 &&
                        clsx(classes.nonMargin, classes.nonMarginBottom)
                    )}
                    key={index.toString()}
                  >
                    <Box className={classes.levelMessage}>{el.name}</Box>

                    <Box className={classes.wrapProgressBar}>
                      <BorderLinearProgress
                        variant="determinate"
                        value={(el.count * 100) / 10}
                      />
                    </Box>
                    <Typography className={classes.countEvaluate}>
                      {el.count}
                    </Typography>
                  </Box>
                ))}
            </Box>
            <Box className={classes.border} />
            <Box className={classes.wrapBoxRightRating}>
              {getListRating.map((el, index) => (
                <Box
                  className={clsx(
                    classes.itemLevel,
                    index === 0 && classes.nonMargin,
                    index === getListRating.length - 1 &&
                      clsx(classes.nonMargin, classes.nonMarginBottom)
                  )}
                  key={index.toString()}
                >
                  <Box className={classes.levelMessageRating}>{el.name}</Box>

                  <Box className={classes.wrapProgressBarRating}>
                    <RatingLinearProgress
                      variant="determinate"
                      value={(el.point * 100) / 10}
                    />
                  </Box>
                  <Typography className={classes.countEvaluateRating}>
                    {el.point}
                  </Typography>
                </Box>
              ))}
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default ReviewPointHotel;
