import {
  Box,
  Grid,
  makeStyles,
  withStyles,
  Fade,
  Backdrop,
  Typography,
} from "@material-ui/core";
import LinearProgress from "@material-ui/core/LinearProgress";
import clsx from "clsx";
import { useState } from "react";
import { adapterAmenitiesGroup, isEmpty, unEscape } from "@utils/helpers";
import { listString, optionClean } from "@utils/constants";
import Modal from "@material-ui/core/Modal";
import { IconClose, IconQuote } from "@public/icons";
import Image from "@src/image/Image";
import sanitizeHtml from "sanitize-html";

const useStyles = makeStyles((theme) => ({
  modal: { display: "flex", alignItems: "center", justifyContent: "center" },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    width: 784,
    borderRadius: 8,
    padding: "0 24px",
    outline: "none",
  },
  wrapBox: { marginTop: 24 },
  boxLeft: {
    fontSize: 20,
    fontWeight: 600,
    lineHeight: "23px",
    color: theme.palette.black.black3,
  },
  boxLeftHeader: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  viewAllConvient: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    color: theme.palette.blue.blueLight8,
    cursor: "pointer",
  },
  convenientHotel: {
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
    color: theme.palette.black.black3,
    margin: "16px 0",
  },
  wrapContainer: {
    margin: 16,
    fontSize: 11,
    lineHeight: "13px",
    fontWeight: "normal",
  },
  classIcon: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    minHeight: 56,
  },
  loadMoreConvenient: {
    height: 56,
    width: 56,
    borderRadius: "50%",
    background: theme.palette.blue.blueLight10,
    color: theme.palette.primary.main,
    display: "flex",
    alignItems: "center",
    justifyContent: "center ",
    fontWeight: 600,
    fontSize: 14,
    marginLeft: 30,
    cursor: "pointer",
  },
  iconConvient: { width: 24, height: 24 },
  nameAmenities: {
    fontSize: 14,
    lineHeight: "16px",
    textAlign: "center",
    minHeight: 50,
    minWidth: 114,
    overflow: "hidden",
    fontWeight: 400,
    marginTop: 12,
    display: "-webkit-box",
    WebkitLineClamp: 3,
    WebkitBoxOrient: "vertical",
  },
  boxRight: { marginLeft: 28 },
  wrapEvaluate: { borderBottom: `1px solid ${theme.palette.gray.grayLight22}` },
  wrapBoxLeft: {
    height: 111,
    border: `1px solid ${theme.palette.blue.blueLight8}`,
    color: theme.palette.blue.blueLight8,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    borderRadius: 8,
    marginBottom: 16,
  },
  rating: { fontWeight: 600, fontSize: 28, lineHeight: "33px" },
  messageReview: { marginTop: 4, lineHeight: "17px" },
  wrapBoxRight: { display: "flex", flexDirection: "column", marginTop: 24 },
  itemLevel: {
    marginTop: 10,
    fontSize: 14,
    color: theme.palette.black.black3,
    display: "flex",
    alignItems: "center",
  },
  nonMargin: { margin: 0 },
  nonMarginBottom: { marginTop: 10 },
  wrapProgressBar: { width: 128 },
  countEvaluate: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.blue.blueLight8,
    width: 21,
    textAlign: "right",
    marginLeft: 10,
  },
  levelMessage: {
    fontSize: 14,
    width: 70,
    marginRight: 9,
    lineHeight: "16px",
    fontWeight: 400,
  },
  wrapBoxRightText: {
    background: theme.palette.gray.grayLight26,
    borderRadius: 8,
    position: "relative",
    marginLeft: 25,
    marginTop: 13,
    width: "100%",
  },
  wrapQoute: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    margin: "16px 0 0 16px",
  },
  boxRightContent: { display: "flex", marginLeft: 28 },
  modalHeader: {
    fontSize: 20,
    lineHeight: "23px",
    fontWeight: 600,
    display: "flex",
    justifyContent: "space-between",
    margin: "24px 0",
  },
  wrapConvient: { display: "flex", flexDirection: "column" },
  childListConvient: { fontSize: 14, lineHeight: "16px" },
  iconConven: { width: 24, height: 24 },
  modalContent: {
    height: 530,
    overflow: "auto",
    margin: "0 -24px",
    padding: "0 24px",
  },
}));

const BorderLinearProgress = withStyles((theme) => ({
  root: { height: 6, borderRadius: 100, width: 128 },
  colorPrimary: {
    backgroundColor: theme.palette.gray.grayLight23,
  },
  bar: {
    borderRadius: 100,
    height: 6,
    backgroundColor: theme.palette.blue.blueLight8,
  },
}))(LinearProgress);

const ConvenientHotel = ({ item = {}, firstReview = {}, scrollToItem }) => {
  const itemRating = !isEmpty(item.rating) ? item.rating.hotel : {};
  const getListRating = [
    { id: 0, name: "Vị trí", point: itemRating.locationRating },
    { id: 1, name: "Giá cả", point: itemRating.priceRating },
    { id: 2, name: "Phục vụ", point: itemRating.serviceRating },
    { id: 3, name: "Vệ sinh", point: itemRating.cleanlinessRating },
    { id: 4, name: "Tiện nghi", point: itemRating.convenientRating },
  ];
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const arrayGroupAmenities = Object.values(item.groupAmenities || {});
  return (
    <>
      <Box className={classes.wrapBox}>
        <Grid container>
          <Grid item sm={6} md={6} lg={6}>
            <Box className={classes.boxLeft}>
              <Box className={classes.boxLeftHeader}>
                <Box>{listString.IDS_MT_TEXT_CONVENIENT_HOTEL}</Box>
                <Box className={classes.viewAllConvient} onClick={handleOpen}>
                  {listString.IDS_MT_TEXT_VIEW_ALL_CONVIENT}
                </Box>
              </Box>
              <Box className={classes.boxLeftContent}>
                <Box display="flex">
                  <Box
                    display="flex"
                    width="100%"
                    style={{ flexWrap: "wrap", marginTop: 24 }}
                  >
                    {!isEmpty(item.groupAmenities) &&
                      adapterAmenitiesGroup(item.groupAmenities).map(
                        (el, index) => {
                          if (index >= 9) return null;
                          return (
                            <Box key={index.toString()} width="20%">
                              <Box
                                display="flex"
                                color="black.black3"
                                letterSpacing={-0.23}
                                lineHeight={1.5}
                                className={classes.classIcon}
                              >
                                <img
                                  src={el.icon}
                                  className={classes.iconConvient}
                                  alt=""
                                />
                                <Box
                                  component="span"
                                  className={classes.nameAmenities}
                                >
                                  {el.name}
                                </Box>
                              </Box>
                            </Box>
                          );
                        }
                      )}
                    {adapterAmenitiesGroup(item.groupAmenities).length > 9 && (
                      <Box
                        className={classes.loadMoreConvenient}
                        onClick={handleOpen}
                      >
                        <Box component="span">{`+${adapterAmenitiesGroup(
                          item.groupAmenities
                        ).length - 9}`}</Box>
                      </Box>
                    )}
                  </Box>
                </Box>
              </Box>
            </Box>
          </Grid>
          <Grid item sm={6} md={6} lg={6}>
            {!isEmpty(firstReview) && (
              <Box
                className={clsx(
                  classes.boxLeft,
                  classes.boxRight,
                  classes.boxLeftHeader
                )}
              >
                <Box>{listString.IDS_MT_TEXT_EVALUATE}</Box>
                <Box
                  className={classes.viewAllConvient}
                  onClick={() => scrollToItem("evaluate")}
                >
                  {listString.IDS_MT_TEXT_VIEW_ALL_EVALUATE}
                </Box>
              </Box>
            )}

            <Box className={classes.boxRightContent}>
              <Box className={classes.wrapBoxRight}>
                {getListRating.map((el, index) => (
                  <Box
                    className={clsx(
                      classes.itemLevel,
                      index === 0 && classes.nonMargin,
                      index === getListRating.length - 1 &&
                        clsx(classes.nonMargin, classes.nonMarginBottom)
                    )}
                    key={index.toString()}
                  >
                    <Box className={classes.levelMessage}>{el.name}</Box>

                    <Box className={classes.wrapProgressBar}>
                      <BorderLinearProgress
                        variant="determinate"
                        value={((2 * el.point).toFixed(1) * 100) / 10}
                      />
                    </Box>
                    <Typography className={classes.countEvaluate}>
                      {(2 * el.point).toFixed(1)}
                    </Typography>
                  </Box>
                ))}
              </Box>
              {!isEmpty(firstReview) && (
                <Box className={classes.wrapBoxRightText}>
                  <Typography variant="subtitle1" className={classes.wrapQoute}>
                    {firstReview.title}
                    <Box mr={8 / 9} ml={1} mt={-1}>
                      <IconQuote />
                    </Box>
                  </Typography>

                  <Box
                    style={{
                      fontSize: "14px",
                      lineHeight: "22px",
                      fontWeight: 400,
                      margin: "0 16px 21px ",
                      display: "-webkit-box",
                      WebkitLineClamp: 4,
                      WebkitBoxOrient: "vertical",
                      overflow: "hidden",
                    }}
                    dangerouslySetInnerHTML={{
                      __html: sanitizeHtml(
                        unEscape(unescape(firstReview.content || "")),
                        optionClean
                      ),
                    }}
                  />
                </Box>
              )}
            </Box>
          </Grid>
        </Grid>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{ timeout: 500 }}
        >
          <Fade in={open}>
            <Box className={classes.paper}>
              <Box className={classes.modalHeader}>
                <Box> {listString.IDS_MT_TEXT_FILTER_CONVENIENT}</Box>
                <Box onClick={handleClose}>
                  <IconClose style={{ cursor: "pointer" }} />
                </Box>
              </Box>
              <Box className={classes.modalContent}>
                {!isEmpty(arrayGroupAmenities) &&
                  arrayGroupAmenities.map((el, index) => (
                    <Box
                      key={index.toString()}
                      className={classes.wrapConvient}
                    >
                      <Box>
                        <Typography
                          variant="subtitle2"
                          style={{
                            lineHeight: "19px",
                            fontSize: 16,
                            margin: "8px 0 16px 0",
                          }}
                        >
                          {el.name}
                        </Typography>
                      </Box>
                      <Box
                        key={index.toString()}
                        style={{ display: "flex", flexWrap: "wrap" }}
                      >
                        {!isEmpty(el.amenities) &&
                          el.amenities.map((val, idx) => (
                            <Box
                              key={idx}
                              style={{
                                display: "flex",
                                width: "33%",
                                alignItems: "center",
                                marginBottom: 16,
                              }}
                            >
                              <Box
                                style={{
                                  width: 22,
                                  height: 22,
                                  marginRight: 13,
                                }}
                              >
                                <img
                                  src={val.icon}
                                  style={{ width: 22, height: 22 }}
                                  alt=""
                                />
                              </Box>
                              <Typography className={classes.childListConvient}>
                                {val.name}{" "}
                              </Typography>
                            </Box>
                          ))}
                      </Box>
                    </Box>
                  ))}
              </Box>
            </Box>
          </Fade>
        </Modal>
      </Box>
    </>
  );
};

export default ConvenientHotel;
