import {
  Box,
  Dialog,
  IconButton,
  makeStyles,
  Slide,
  Typography,
} from "@material-ui/core";
import ButtonComponent from "@src/button/Button";
import { listString, optionClean } from "@utils/constants";
import CloseIcon from "@material-ui/icons/Close";
import { isEmpty, unEscape } from "@utils/helpers";
import clsx from "clsx";
import { forwardRef, useState } from "react";
import sanitizeHtml from "sanitize-html";

const useStyles = makeStyles((theme) => ({
  wrapBox: {
    display: "flex",
  },
  wrapCheckIn: {
    background: theme.palette.gray.grayLight22,
    color: theme.palette.black.black3,
    display: "flex",
    justifyContent: "space-around",
    borderRadius: 8,
    marginBottom: 12,
    width: 168,
  },
  wrapCheckOut: {
    marginLeft: 10,
  },
  textCheckIn: {
    fontWeight: "normal",
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark8,
    textAlign: "center",
  },
  dividerTime: {
    width: 1,
    height: 28,
    background: theme.palette.gray.grayLight23,
    margin: "12px 0",
  },
  timeCheckIn: {
    lineHeight: "21px",
    fontSize: 18,
    fontWeight: 600,
    color: theme.palette.black.black3,
    paddingBottom: 16,
  },
  text: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.gray.grayDark8,
    padding: "16px 0 8px 0",
  },
  policy: {
    overflow: "hidden",
    marginTop: 19,
    lineHeight: "20px",
    display: "-webkit-box",
    WebkitLineClamp: 3,
    WebkitBoxOrient: "vertical",
    maxHeight: 140,
  },
  policyDialog: {
    marginTop: 19,
    lineHeight: "20px",
    height: 350,
    overflow: "auto",
  },
  loadMorePolicy: {
    maxWidth: 128,
    border: `1px solid ${theme.palette.blue.blueLight8}`,
    color: theme.palette.blue.blueLight8,
    borderRadius: 8,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  policyHotel: {
    paddingBottom: 24,
    fontSize: 20,
    lineHeight: "24px",
    fontWeight: 600,
  },
  container: { background: "white", width: "100%" },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    marginBottom: 72,
    marginTop: 12,
  },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
  },
  titleDes: {
    fontWeight: 600,
    fontSize: 18,
    lineHeight: "21px",
    color: "#1A202C",
    marginBottom: 12,
  },

  headerTitleDialog: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    paddingRight: 12,
  },
  dialogContent: {
    padding: "12px 24px",
    overflow: "auto",
    height: 440,
    width: 784,
  },
  titleTextDialog: {
    paddingLeft: 24,
    fontWeight: 600,
    fontSize: 20,
    lineHeight: "24px",
    color: "#1A202C",
  },
  itemPolices: {
    "& p": {
      margin: 0,
    },
  },
}));

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
const PolicyHotel = ({ hotelDetail = {} }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  return (
    <>
      <Box className={classes.wrapHeader}>
        <Box className={classes.policyHotel}>
          {listString.IDS_MT_TEXT_HOTEL_POLICY}
        </Box>
      </Box>
      <Box className={classes.wrapBox}>
        <Box className={classes.wrapCheckIn}>
          <Box className={classes.textCheckIn}>
            <Box className={classes.text}>
              {listString.IDS_MT_TEXT_CHECK_IN_ROOM}
            </Box>
            <Typography variant="subtitle2" className={classes.timeCheckIn}>
              {`Từ ${hotelDetail.checkInTime}`}
            </Typography>
          </Box>
        </Box>
        <Box className={clsx(classes.wrapCheckIn, classes.wrapCheckOut)}>
          <Box className={clsx(classes.textCheckIn, classes.textCheckOut)}>
            <Box className={classes.text}>
              {listString.IDS_MT_TEXT_CHECK_OUT_ROOM}
            </Box>
            <Typography variant="subtitle2" className={classes.timeCheckIn}>
              {`Trước ${hotelDetail.checkOutTime}`}
            </Typography>
          </Box>
        </Box>
      </Box>
      <Box className={classes.policy}>
        {!isEmpty(hotelDetail.policies) &&
          hotelDetail.policies.map((el, index) => {
            if (index > 2) return null;
            return (
              <Box display="flex" flexDirection="column" key={index.toString()}>
                <Box component="span" fontSize={14} fontWeight={600} pb={1}>
                  {el.title}
                </Box>
                <Box
                  fontSize={14}
                  color="gray.grayDark1"
                  maxHeight={132}
                  overflow="hidden"
                  mb={12 / 8}
                  lineHeight="19px"
                  className={classes.itemPolices}
                  dangerouslySetInnerHTML={{
                    __html: sanitizeHtml(
                      unEscape(unescape(el.description || "")),
                      optionClean
                    ),
                  }}
                />
              </Box>
            );
          })}
      </Box>
      {!isEmpty(hotelDetail.policies) && (
        <ButtonComponent
          typeButton="outlined"
          backgroundColor="inherit"
          color="#00B6F3"
          width="fit-content"
          borderColor="#00B6F3"
          borderRadius={8}
          padding="12px 50px"
          style={{ marginTop: 26 }}
          handleClick={() => setOpen(true)}
        >
          {listString.IDS_MT_TEXT_LOAD_MORE}
        </ButtonComponent>
      )}

      <Dialog
        maxWidth="sm"
        open={open}
        onClose={() => setOpen(false)}
        TransitionComponent={Transition}
      >
        <Box className={classes.headerTitleDialog}>
          <Typography variant="body2" className={classes.titleTextDialog}>
            {listString.IDS_MT_TEXT_HOTEL_POLICY}
          </Typography>
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            onClick={() => setOpen(false)}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Box className={classes.dialogContent}>
          <Box className={classes.policyDialog}>
            {!isEmpty(hotelDetail.policies) &&
              hotelDetail.policies.map((el, index) => {
                if (index > 2) return null;
                return (
                  <Box
                    display="flex"
                    flexDirection="column"
                    key={index.toString()}
                  >
                    <Box component="span" fontSize={14} fontWeight={600} pb={1}>
                      {el.title}
                    </Box>
                    <Box
                      fontSize={14}
                      color="gray.grayDark1"
                      maxHeight={132}
                      overflow="hidden"
                      mb={12 / 8}
                      lineHeight="19px"
                      className={classes.itemPolices}
                      dangerouslySetInnerHTML={{
                        __html: sanitizeHtml(
                          unEscape(unescape(el.description || "")),
                          optionClean
                        ),
                      }}
                    />
                  </Box>
                );
              })}
          </Box>
        </Box>
      </Dialog>
    </>
  );
};

export default PolicyHotel;
