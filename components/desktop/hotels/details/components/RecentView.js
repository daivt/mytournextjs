import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { useEffect, useState } from "react";
import { isEmpty } from "@utils/helpers";
import { LAST_HOTEL_BROWSER_ITEMS, listString } from "@utils/constants";
import RecentHotelItem from "@components/common/desktop/itemView/RecentHotelItem";

const useStyles = makeStyles((theme) => ({
  container: { background: "white", width: "100%" },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    marginBottom: 24,
    marginTop: 12,
  },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
  },
  divider: {
    width: "100%",
    height: 4,
    background: "#EDF2F7",
    margin: "24px 0",
  },
}));
const RecentView = () => {
  const classes = useStyles();
  const [recentHotel, setRecentHotel] = useState([]);
  const getRecentHotel = () => {
    if (!isEmpty(localStorage.getItem(LAST_HOTEL_BROWSER_ITEMS)))
      setRecentHotel(
        JSON.parse(localStorage.getItem(LAST_HOTEL_BROWSER_ITEMS))
      );
  };
  useEffect(() => {
    getRecentHotel();
  }, []);
  if (isEmpty(recentHotel)) return null;
  return (
    <>
      <Box className={classes.divider} />
      <Box className={classes.container}>
        <Box className={classes.content}>
          <Box className={classes.hotelGroup} style={{ marginBottom: 32 }}>
            <Typography variant="body2" className={classes.titleText}>
              {listString.IDS_TEXT_VIEW_LAST_HISTORY}
            </Typography>
          </Box>
          <Box
            style={{
              display: "flex",
              justifyContent:
                recentHotel.length >= 6 ? "space-between" : "flex-start",
            }}
          >
            {recentHotel.map((item, i) => {
              if (i >= 6) return null;
              return (
                <Box key={item.id} style={{ marginRight: 24 }}>
                  <RecentHotelItem item={item} />
                </Box>
              );
            })}
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default RecentView;
