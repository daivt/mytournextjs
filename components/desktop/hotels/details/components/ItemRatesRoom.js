import clsx from "clsx";
import { forwardRef, useState } from "react";

import BootstrapTooltip from "@components/common/bootstrapTooltip/BootstrapTooltip";
import PolicyRoomModal from "@components/desktop/hotels/details/components/PolicyRoomModal";
import { Box, Dialog, makeStyles, Slide, Typography } from "@material-ui/core";
import sanitizeHtml from "sanitize-html";

import {
  IconAcreage,
  IconBed,
  IconBoxGif,
  IconBreakFast,
  IconCancelPolicy,
  IconCar,
  IconCheck,
  IconDot,
  IconFlash,
  IconInfoRateRoom,
  IconUser2,
  IconView,
} from "@public/icons";
import {
  LAST_LOCATION_RECENT_SEARCH_HISTORY_ITEMS,
  listString,
  optionClean,
} from "@utils/constants";
import { isEmpty, unEscape } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  rateContainer: {
    borderTop: `1px solid ${theme.palette.gray.grayLight22}`,
    padding: "10px 0",
  },
  textCancelPolicy: {
    fontSize: 14,
    lineHeight: "16px",
    color: theme.palette.gray.grayDark8,
    marginLeft: 8,
    fontWeight: 400,
  },
  iconFlash: {
    stroke: theme.palette.green.greenLight7,
  },
  textFreeBreakFast: {
    color: theme.palette.green.greenLight7,
  },
  boxLeft: { display: "flex", marginTop: 8, alignItems: "center" },
  boxRight: { display: "flex", whiteSpace: "nowrap" },
  iconBreakFast: {
    stroke: theme.palette.gray.grayDark8,
  },
  iconBed: {
    stroke: theme.palette.blue.blueLight8,
    color: theme.palette.blue.blueLight8,
  },
  numberAllotment: {
    color: theme.palette.red.redLight5,
    marginLeft: 0,
    textAlign: "left",
  },
  wrapRoomAndPrice: {
    fontSize: 12,
    color: theme.palette.black.black3,
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    width: "100%",
    justifyContent: "center",
  },
  warpIconShowPriceMobile: {
    display: "flex",
    background: theme.palette.blue.blueLight9,
    width: 107,
    height: 32,
    borderRadius: 4,
    color: theme.palette.primary.main,
    alignItems: "center",
    marginTop: 6,
  },
  wrapShowPriceMobile: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  freeCancelPolicy: {
    stroke: theme.palette.green.greenLight7,
  },

  info: {
    cursor: "pointer",
    marginLeft: 4,
    stroke: theme.palette.gray.grayDark8,
    width: 16,
    height: 16,
  },

  infoActive: {
    width: 16,
    height: 16,
    marginLeft: 4,
    stroke: theme.palette.green.greenLight7,
    cursor: "pointer",
  },
  wrapContent: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    marginTop: 8,
    color: theme.palette.gray.grayDark8,
  },
  wrapRoomName: {
    display: "flex",
    alignItems: "center",
    color: theme.palette.black.black3,
  },
  infoRoom: {
    display: "flex",
    alignItems: "center",
    marginTop: 8,
  },
  IconBed: {
    width: 16,
    height: 16,
  },
  IconUser2: {
    width: 16,
    height: 16,
  },
  iconCar: {
    stroke: theme.palette.gray.grayDark8,
  },
  dateFreeCancel: {
    marginTop: 4,
    fontSize: 12,
    lineHeight: "14px",
    fontWeight: 400,
  },
  wrapSignals: {
    background: theme.palette.green.greenLight8,
    borderRadius: 8,
    display: "flex",
    position: "relative",
    overflow: "hidden",
    marginTop: 12,
  },
  iconGift: {
    background: theme.palette.green.greenLight7,
    display: "flex",
    borderRadius: "8px 0px 0px 8px",
    padding: "8px 8px",
    alignItems: "center",
    height: "100%",
  },
  contentSignal: {
    display: "flex",
    alignItems: "center",
  },
  wrapSignalName: {
    marginLeft: "10%",
    minWidth: "100%",
  },
  signals: {
    display: "flex",
    alignItems: "center",
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    color: theme.palette.green.greenLight7,
    cursor: "pointer",
  },
  signalName: { marginLeft: 6 },
  iconDot: {
    stroke: theme.palette.green.greenLight7,
    fill: theme.palette.green.greenLight7,
    borderRadius: "50%",
  },
  promoCode: {
    borderRadius: 3,
    border: "1px dashed rgba(72, 187, 120, 0.5)",
    background: theme.palette.green.greenLight8,
    padding: "2px 4px ",
    marginLeft: 5,
    fontWeight: 600,
  },
  pricePromo: {
    fontSize: 20,
    lineHeight: "23px",
    fontWeight: 600,
    color: theme.palette.pink.main,
    marginTop: 3,
    textAlign: "right",
  },
  wrapCode: {
    display: "flex",
    alignItems: "center",
    marginTop: 6,
  },
  wrapHotCode: {
    color: theme.palette.green.greenLight7,
    display: "flex",
    alignItems: "center",
  },
  boxPolicies: {
    "& > p": {
      marginTop: 3,
      marginBottom: 0,
    },
  },
}));

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
const getNamePolices = (policies = []) => {
  let textPolicies = "";
  policies.forEach((el, index) => {
    textPolicies += `${
      index !== 0 ? `${index === policies.length - 1 ? " & " : ", "}` : ""
    }${el.shortDescription}`;
  });
  return textPolicies;
};

const ItemRatesRoom = ({
  rateInfo = {},
  roomInfo = {},
  index = 1,
  isShowOption = false,
  handleViewRoomRate = () => {},
  handleBookRoomRate = () => {},
  showGuest = false,
  showRoomName = true,
  styleRoomName = "",
  showRoomArea = true,
  showNumRooms = true,
  showBedGroup = false,
  isFirst = false,
  showFlashAgree = true,
  showNumberAllotment = true,
  styleColorWrap = "",
  showBuySignal = false,
  showPolicies = false,
  showSpecialDeal = false,
}) => {
  const classes = useStyles();
  const handleBooking = () => {
    handleBookRoomRate();
  };
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const getListBed = () => {
    let listBed = "";
    rateInfo.bedGroups.forEach((el, i) => {
      listBed = `${listBed}${i !== 0 ? " hoặc " : ""}${el.bedInfo}`;
    });
    return listBed;
  };
  const arrSignalPromo = (signalPromo = []) => {
    let resultPromo = [];
    resultPromo = signalPromo.filter((item) => item.groupCode === "PROMOTIONS");
    return resultPromo;
  };
  return (
    <>
      <Box className={classes.wrapContent}>
        {showRoomName ? (
          <Box className={classes.wrapRoomName}>
            {!isFirst && (
              <Typography variant="subtitle1">
                Lựa chọn {index + 1}
                {!isEmpty(rateInfo.providerInfo)
                  ? `: ${rateInfo.providerInfo}`
                  : ""}
              </Typography>
            )}
          </Box>
        ) : (
          <>
            <Box className={clsx(classes.wrapRoomName, styleRoomName)}>
              <Typography
                variant="subtitle1"
                component="span"
                style={{ marginRight: "3px" }}
              >
                {showNumRooms && showNumRooms > 0 && "x"}
              </Typography>
              <Typography variant="subtitle1">
                {roomInfo.name}
                {!isEmpty(rateInfo.providerInfo)
                  ? `: ${rateInfo.providerInfo}`
                  : ""}
              </Typography>
            </Box>
            <Box className={clsx(classes.infoRoom, styleColorWrap)}>
              {showGuest && !isEmpty(roomInfo.standardNumberOfGuests) && (
                <>
                  <IconUser2 className={classes.IconUser2} />
                  <Box marginLeft={1}>
                    {roomInfo.standardNumberOfGuests} người
                  </Box>
                </>
              )}

              {showRoomArea && !isEmpty(roomInfo.roomArea) && (
                <>
                  <IconAcreage />
                  <Box marginLeft={1}>{roomInfo.roomArea}m2 </Box>
                </>
              )}

              {!isEmpty(roomInfo.view) && (
                <>
                  <IconView style={{ marginLeft: "20px" }} />
                  <Box marginLeft={1}>{roomInfo.view} </Box>
                </>
              )}
            </Box>
          </>
        )}

        {showBedGroup && !isEmpty(rateInfo.bedGroups[0].bedTypes[0]) && (
          <Box
            display="flex"
            alignItems="center"
            style={{ marginTop: "10px" }}
            className={styleColorWrap}
          >
            <Box style={{ width: 16, height: 16 }}>
              <IconBed className={classes.iconBed} />
            </Box>

            <Box marginLeft={1}>{getListBed()}</Box>
          </Box>
        )}

        {rateInfo.freeCancellation ? (
          <>
            <Box
              style={{
                display: "flex",
                alignItems: "center",
                marginTop: "10px",
                cursor: "pointer",
              }}
              onClick={handleOpen}
            >
              <IconCancelPolicy
                className={`svgFillAll ${classes.freeCancelPolicy}`}
              />
              <Typography
                className={classes.textCancelPolicy}
                style={{ color: "#48BB78" }}
              >
                {listString.IDS_MT_TEXT_FREE_CANCEL}
              </Typography>
              <IconInfoRateRoom
                className={`svgFillAll ${classes.infoActive}`}
              />
            </Box>
            <Typography
              variant="body2"
              style={{ marginLeft: "22px", textAlign: "left" }}
            >
              {!isEmpty(rateInfo.hotelCancellationPolicies) &&
                rateInfo.hotelCancellationPolicies.map((el, i) => (
                  <Typography
                    component="span"
                    className={classes.dateFreeCancel}
                    key={i}
                  >
                    {el.price === el.refundAmount &&
                      `Trước ${el.dateTo.split(" ")[0]}`}
                  </Typography>
                ))}
            </Typography>
          </>
        ) : (
          <Box
            style={{
              display: "flex",
              alignItems: "center",
              marginTop: "10px",
              cursor: "pointer",
            }}
            onClick={() => {
              if (rateInfo.refundable) {
                handleOpen();
              }
            }}
          >
            <IconCancelPolicy />
            <Typography
              className={clsx(classes.textCancelPolicy, styleColorWrap)}
            >
              {rateInfo.shortCancelPolicy}
            </Typography>
            {rateInfo.refundable && (
              <IconInfoRateRoom className={`svgFillAll ${classes.info}`} />
            )}
          </Box>
        )}

        {rateInfo.freeBreakfast ? (
          <Box className={clsx(classes.boxLeft)}>
            <IconBreakFast />
            <Typography
              className={clsx(
                classes.textCancelPolicy,
                classes.textFreeBreakFast
              )}
            >
              {listString.IDS_MT_TEXT_ROOM_FREE_BREAK_FAST}
            </Typography>
          </Box>
        ) : (
          <Box className={classes.boxLeft}>
            <IconBreakFast className={`svgFillAll ${classes.iconBreakFast}`} />
            <Typography
              className={clsx(styleColorWrap, classes.textCancelPolicy)}
            >
              {listString.IDS_MT_TEXT_ROOM_NO_FREE_BREAK_FAST}
            </Typography>
          </Box>
        )}

        {!isEmpty(rateInfo.amenities) && rateInfo.amenities[0].name && (
          <Box className={clsx(classes.boxLeft)}>
            {!isEmpty(rateInfo.amenities) &&
            !isEmpty(rateInfo.amenities[0].icon) ? (
              <img
                src={rateInfo.amenities[0].icon}
                style={{ width: 16, height: 16 }}
                alt=""
              />
            ) : (
              <IconCar />
            )}
            <Typography className={classes.textCancelPolicy}>
              {rateInfo.amenities[0].name}
            </Typography>
          </Box>
        )}
        {showPolicies && !isEmpty(rateInfo.policies) && (
          <Box className={classes.boxLeft}>
            <Box style={{ width: 16, height: 16 }}>
              <IconInfoRateRoom style={{ width: 16, height: 16 }} />
            </Box>
            <BootstrapTooltip
              title={
                !isEmpty(rateInfo.policies) &&
                rateInfo.policies.map((el, idx) => {
                  return (
                    <Box
                      key={idx}
                      fontSize={14}
                      lineHeight="16px"
                      color="white.main"
                      lineHeight="16px"
                      dangerouslySetInnerHTML={{
                        __html: sanitizeHtml(
                          unEscape(unescape(el.description || "")),
                          optionClean
                        ),
                      }}
                    />
                  );
                })
              }
              placement="top"
            >
              <Box
                className={classes.textCancelPolicy}
                style={{
                  display: "-webkit-box",
                  WebkitLineClamp: 3,
                  WebkitBoxOrient: "vertical",
                  overflow: "hidden",
                }}
              >
                {!isEmpty(rateInfo.policies) && (
                  <Box
                    fontSize={14}
                    className={classes.boxPolicies}
                    dangerouslySetInnerHTML={{
                      __html: sanitizeHtml(
                        unEscape(
                          unescape(getNamePolices(rateInfo.policies) || "")
                        ),
                        optionClean
                      ),
                    }}
                  />
                )}
              </Box>
            </BootstrapTooltip>
          </Box>
        )}

        {rateInfo.requestPrice && showFlashAgree ? (
          <Box className={classes.boxLeft}>
            <Box style={{ width: "16px", height: "16px  " }}>
              {" "}
              <IconFlash />
            </Box>
            <Typography
              className={classes.textCancelPolicy}
              style={{ color: "#ED8936" }}
            >
              {listString.IDS_MT_TEXT_CONFIRM_AROUNR_30_MINUTES}
            </Typography>
          </Box>
        ) : (
          <>
            {showFlashAgree && (
              <Box className={classes.boxLeft}>
                <Box style={{ width: "16px", height: "16px " }}>
                  {" "}
                  <IconFlash className={`svgFillAll ${classes.iconFlash}`} />
                </Box>
                <Typography
                  className={classes.textCancelPolicy}
                  style={{ color: "#48BB78" }}
                >
                  {listString.IDS_MT_TEXT_CONFIRM_NOW}
                </Typography>
              </Box>
            )}
          </>
        )}
        {showSpecialDeal && !isEmpty(rateInfo.specialOffer) && (
          <Box className={classes.wrapCode}>
            <IconCheck style={{ marginRight: "6px" }} />
            <Box component="span" className={classes.wrapHotCode}>
              Mã{" "}
              <Typography
                variant="subtitle2"
                component="span"
                className={classes.promoCode}
              >
                {rateInfo.specialOffer.code}
              </Typography>
              <Box
                style={{
                  marginLeft: "5px",
                  whiteSpace: "nowrap",
                  fontSize: "14px",
                  lineHeight: "16px",
                  fontWeight: "400",
                }}
              >
                {" "}
                giảm{" "}
                {rateInfo.specialOffer.valueType === "absolute_value"
                  ? `${rateInfo.specialOffer.value.formatMoney()}đ`
                  : `${" "}${rateInfo.specialOffer.percent}%`}
                <Box component="span"> đã được áp dụng</Box>
              </Box>
            </Box>
          </Box>
        )}
        {showBuySignal && !isEmpty(rateInfo.buyingSignals) && (
          <>
            <Box className={classes.wrapSignals}>
              <Box className={classes.contentSignal}>
                <Box className={classes.iconGift}>
                  <IconBoxGif />
                </Box>
                <Box style={{ padding: "8px 0 4px 0" }}>
                  {arrSignalPromo(rateInfo.buyingSignals).map((el, id) => (
                    <Box className={classes.signals} key={id}>
                      <Box
                        style={{
                          padding: "0 8px 0 0px",
                          display: "flex",
                          alignItems: "center",
                          marginBottom: 4,
                        }}
                      >
                        {rateInfo.buyingSignals.length > 1 && (
                          <IconDot
                            className={`svgFillAll ${classes.iconDot}`}
                            style={{ marginLeft: 6 }}
                          />
                        )}
                        {!isEmpty(el.description) ? (
                          <BootstrapTooltip
                            title={el.description}
                            placement="top"
                          >
                            <Box className={classes.signalName}>{el.name}</Box>
                          </BootstrapTooltip>
                        ) : (
                          <Box className={classes.signalName}>{el.name}</Box>
                        )}
                      </Box>
                    </Box>
                  ))}
                </Box>
              </Box>
            </Box>
          </>
        )}
        {rateInfo.availableAllotment > 0 &&
          rateInfo.availableAllotment <= 5 &&
          showNumberAllotment && (
            <Box style={{ marginTop: "10px" }}>
              <Typography
                className={clsx(
                  classes.textCancelPolicy,
                  classes.numberAllotment
                )}
              >
                {rateInfo.availableAllotment === 1 ? (
                  "Phòng cuối cùng của chúng tôi!"
                ) : (
                  <>
                    {listString.IDS_MT_TEXT_ONLY} {rateInfo.availableAllotment}{" "}
                    phòng trống!
                  </>
                )}
              </Typography>
            </Box>
          )}
      </Box>
      <Dialog
        maxWidth="sm"
        open={open}
        onClose={() => setOpen(false)}
        TransitionComponent={Transition}
      >
        <PolicyRoomModal item={rateInfo} handleClose={handleClose} />
      </Dialog>
    </>
  );
};

export default ItemRatesRoom;
