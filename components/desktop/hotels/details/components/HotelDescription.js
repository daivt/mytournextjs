import React, { useState } from "react";
import { Box, Typography, Dialog, Slide, IconButton } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import CloseIcon from "@material-ui/icons/Close";
import { listString } from "@utils/constants";
import ButtonComponent from "@src/button/Button";
import { isEmpty } from "@utils/helpers";
import Image from "@src/image/Image";
import { IconLoadingHotel } from "@public/icons";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  container: { background: "white", width: "100%" },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    marginBottom: 72,
    marginTop: 12,
  },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
  },
  titleDes: {
    fontWeight: 600,
    fontSize: 18,
    lineHeight: "21px",
    color: "#1A202C",
    marginBottom: 12,
  },
  descriptionGroup: {
    position: "relative",
    display: "flex",
    justifyContent: "flex-end",
  },
  hotelImage: { width: 683, height: 398, borderRadius: 8 },
  descriptionText: {
    position: "absolute",
    background: "white",
    boxShadow:
      "0px 0px 15px rgba(0, 0, 0, 0.05), 0px 20px 30px rgba(0, 0, 0, 0.05)",
    borderRadius: 8,
    left: 0,
    top: "50%",
    transform: "translateY(-50%)",
    width: 620,
    padding: 32,
  },
  headerTitleDialog: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    paddingRight: 12,
  },
  dialogContent: { padding: "12px 24px", overflow: "auto" },
  titleTextDialog: {
    paddingLeft: 24,
    fontWeight: 600,
    fontSize: 20,
    lineHeight: "24px",
    color: "#1A202C",
  },
  hotelDefault: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%,-50%)",
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
const HotelDescription = ({ hotelDetail = {} }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  return (
    <Box className={classes.container}>
      <Box className={classes.content}>
        <Box className={classes.hotelGroup} style={{ marginBottom: 32 }}>
          <Typography variant="body2" className={classes.titleText}>
            {listString.IDS_MT_TEXT_HOTEL_DES}
          </Typography>
        </Box>
        <Box className={classes.descriptionGroup} style={{ marginBottom: 32 }}>
          <Box className={classes.descriptionGroup}>
            {!isEmpty(hotelDetail.thumbnail) ? (
              <Image
                srcImage={
                  !isEmpty(hotelDetail.thumbnail.image)
                    ? hotelDetail.thumbnail.image.medium
                    : hotelDetail.thumbnail.src
                }
                className={classes.hotelImage}
                borderRadiusProp="8px"
              />
            ) : (
              <Box
                className={classes.hotelImage}
                style={{
                  position: "relative",
                  width: 720,
                  background: "#EDF2F7",
                }}
              >
                <Box
                  className={clsx(
                    classes.hotelDefault,
                    classes.backgroundDefault
                  )}
                >
                  <IconLoadingHotel />
                </Box>
              </Box>
            )}
          </Box>
          <Box className={classes.descriptionText}>
            <Typography variant="body2" className={classes.titleDes}>
              {hotelDetail.name}
            </Typography>
            <Box
              style={{
                fontSize: 14,
                lineHeight: "22px",
                maxHeight: 132,
                overflowY: "hidden",
              }}
              dangerouslySetInnerHTML={{
                __html: hotelDetail.descriptions || "",
              }}
            />

            {!isEmpty(hotelDetail.descriptions) && (
              <ButtonComponent
                typeButton="outlined"
                backgroundColor="inherit"
                color="#00B6F3"
                width="fit-content"
                borderColor="#00B6F3"
                borderRadius={8}
                padding="12px 50px"
                style={{ marginTop: 24 }}
                handleClick={() => setOpen(true)}
              >
                {listString.IDS_MT_TEXT_LOAD_MORE}
              </ButtonComponent>
            )}
          </Box>
        </Box>
      </Box>
      <Dialog
        minWidth="sm"
        open={open}
        onClose={() => setOpen(false)}
        TransitionComponent={Transition}
      >
        <Box className={classes.headerTitleDialog}>
          <Typography variant="body2" className={classes.titleTextDialog}>
            {listString.IDS_MT_TEXT_HOTEL_DES}
          </Typography>
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            onClick={() => setOpen(false)}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        <Box className={classes.dialogContent}>
          {!isEmpty(hotelDetail.thumbnail) ? (
            <Image
              srcImage={
                !isEmpty(hotelDetail.thumbnail.image)
                  ? hotelDetail.thumbnail.image.medium
                  : hotelDetail.thumbnail.src
              }
              className={classes.hotelImage}
              borderRadiusProp="8px"
            />
          ) : (
            <Box
              className={classes.hotelImage}
              style={{
                position: "relative",
                width: 720,
                background: "#edf2f7",
              }}
            >
              <Box className={classes.hotelDefault}>
                <IconLoadingHotel />
              </Box>
            </Box>
          )}
          {!isEmpty(hotelDetail.descriptions) && (
            <Box
              style={{ fontSize: 14, lineHeight: "22px", paddingTop: 20 }}
              dangerouslySetInnerHTML={{
                __html: hotelDetail.descriptions || "",
              }}
            />
          )}
        </Box>
      </Dialog>
    </Box>
  );
};

export default HotelDescription;
