import { useState, useEffect } from "react";
import { Box } from "@material-ui/core";
import LazyLoad from "react-lazyload";
import { getHotelsRoom } from "@api/hotels";
import { makeStyles } from "@material-ui/styles";
import { isEmpty } from "@utils/helpers";
import { useRouter } from "next/router";
import SkeletonItemListing from "@components/common/desktop/skeleton/SkeletonItemListing";
import { DELAY_TIMEOUT_POLLING } from "@utils/constants";
import ItemHotelDetail from "./ItemHotelDetail";
import ImageRoomModal from "./ImageRoomModal";
import NoResultRoomDetails from "./NoResultRoomDetails";

const useStyles = makeStyles((theme) => ({
  homeWrapper: { width: "100%", background: "#F7FAFC" },
  homeContainer: { maxWidth: 1188, margin: "0 auto", padding: "24px 0" },
  divider: {
    width: "100%",
    height: 4,
    background: "#EDF2F7",
    margin: "24px 0",
  },
}));
let roomSave = {};
const RoomBooking = ({
  hotelDetail = {},
  paramsInit = {},
  rooms,
  setRooms,
}) => {
  const classes = useStyles();
  const router = useRouter();
  const [roomSelected, setRoomSelected] = useState({});
  const [loading, setLoading] = useState(false);

  let breakPolling = true;
  const getParamsQuery = () => {
    let result = {
      ...paramsInit,
      hotelId: hotelDetail.id,
      // checkIn: paramsInit.checkIn,
      // checkOut: paramsInit.checkOut,
      // adults: paramsInit.adults,
      // children: paramsInit.children,
      // rooms: paramsInit.rooms,
    };
    if (!isEmpty(router.query.priceKey)) {
      result = {
        ...result,
        priceKey: router.query.priceKey,
      };
    }
    return result;
  };

  const getRoomHotel = async () => {
    let polling = true;
    setLoading(true);
    try {
      while (polling && breakPolling) {
        const { data } = await getHotelsRoom(getParamsQuery());
        if (data.code === 200) {
          if (!isEmpty(data.data)) {
            if (JSON.stringify(roomSave) !== JSON.stringify(data.data.items)) {
              roomSave = data.data.items;
              setRooms(data.data.items);
            }
            if (!isEmpty(data.data.items)) setLoading(false);
            if (data.data.completed) {
              polling = false;
              setLoading(false);
              window.scrollTo({ top: 0, behavior: "smooth" });
              break;
            }
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          setLoading(false);
          polling = false;
        }
      }
    } catch (error) {
      setLoading(false);
      polling = false;
    }
  };
  useEffect(() => {
    getRoomHotel();
    return () => {
      roomSave = {};
      breakPolling = false;
    }; // eslint-disable-next-line
  }, []);
  useEffect(() => {
    getRoomHotel(); // eslint-disable-next-line
  }, [router.query]);
  return (
    <Box className={classes.homeWrapper} id="rooms_detail">
      <Box className={classes.homeContainer}>
        {loading && [1, 2, 3].map((el) => <SkeletonItemListing key={el} />)}
        {!loading && isEmpty(rooms) && <NoResultRoomDetails />}
        {!loading &&
          !isEmpty(rooms) &&
          rooms.map((el, index) => (
            <LazyLoad key={el.roomKey} overflow offset={100}>
              <ItemHotelDetail
                key={el.roomKey}
                item={el}
                paramsQuery={getParamsQuery()}
                roomRecommend={index === 0}
                openImageRoom={() => setRoomSelected(el)}
                hotelDetail={hotelDetail}
              />
            </LazyLoad>
          ))}
      </Box>
      {!isEmpty(roomSelected) && (
        <ImageRoomModal
          item={roomSelected}
          hotelId={hotelDetail.id}
          handleClose={() => setRoomSelected({})}
        />
      )}
    </Box>
  );
};

export default RoomBooking;
