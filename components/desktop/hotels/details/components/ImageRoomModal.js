import clsx from "clsx";
import {
  Box,
  makeStyles,
  useTheme,
  Dialog,
  Slide,
  Typography,
} from "@material-ui/core";
import { isEmpty } from "@utils/helpers";
import React from "react";
import Slider from "react-slick";
import { useRouter } from "next/router";
import {
  IconAcreage,
  IconArrowDownSmall,
  IconBreakFast,
  IconCancelPolicy,
  IconInfo,
  IconView,
} from "@public/icons";
import ButtonComponent from "@src/button/Button";
import { listString, routeStatic } from "../../../../../utils/constants";

const useStyles = makeStyles((theme) => ({
  dialogContent: { display: "flex" },
  leftContent: {
    width: 686,
    background: "#EDF2F7",
    borderRadius: "8px 0px 0px 8px",
    padding: 24,
    "& .slick-dots": {
      bottom: 0,
      position: "initial",
      display: "flex !important",
      margin: "0 -4px",
      overflow: "auto",
      "& li": {
        width: "calc(20% - 8px)",
        height: 72,
        borderRadius: 8,
        marginBottom: 8,
        marginTop: 8,
      },
      "& .slick-active": {
        "& img": { border: "2px solid #FF1284" },
      },
    },
  },
  rightContent: {
    width: 420,
    minWidth: 420,
    background: "#FFFFFF",
    borderRadius: "0 8px 8px 0",
    padding: "24px 24px 16px 24px",
    position: "relative",
  },
  iconArrow: { stroke: theme.palette.black.black3 },
  wrapArrow: {
    "&:hover": {
      "& svg": { stroke: "#FF1284", transition: "all ease .2s" },
    },
  },
  roomName: {
    fontWeight: 600,
    fontSize: 18,
    lineHeight: "21px",
    color: "#1A202C",
  },
  convient: {
    fontWeight: 600,
    fontSize: 16,
    lineHeight: "19px",
    color: "#1A202C",
    marginTop: 16,
  },
  boxRightContent: { minHeight: 500 },
  infoRoom: { display: "flex", alignItems: "center", marginTop: 8 },
  IconUser2: { width: 12, height: 12 },
  wrapIcon: { width: 16, height: 16 },
  freeCancelPolicy: { stroke: theme.palette.green.greenLight7 },
  textCancelPolicy: {
    fontSize: 14,
    lineHeight: "16px",
    color: theme.palette.gray.grayDark8,
    marginLeft: 6,
    fontWeight: 400,
  },
  infoActive: {
    marginLeft: 4,
    stroke: theme.palette.green.greenLight7,
    cursor: "pointer",
  },
  dateFreeCancel: {
    marginTop: 4,
    fontSize: 12,
    lineHeight: "14px",
    fontWeight: 400,
  },
  info: {
    cursor: "pointer",
    marginLeft: 4,
    stroke: theme.palette.gray.grayDark8,
  },
  iconBreakFast: { stroke: theme.palette.gray.grayDark8 },
  boxLeft: {
    display: "flex",
    marginTop: 8,
    alignItems: "center",
    paddingBottom: 15,
  },
  wrapRateRooms: { borderBottom: `1px solid #EDF2F7` },
  iconConven: { width: 24, height: 24 },
  wrapConvient: {
    display: "flex",
    maxHeight: 380,
    overflow: "auto",
    flexDirection: "column",
  },

  groupItem: {
    display: "flex",
    alignItems: "center",
    width: "45%",
    marginBottom: "16px",
    marginRight: "5%",
  },
  childListConvient: {
    fontSize: 14,
    lineHeight: "16px",
    marginLeft: 12,
    minHeight: 40,
    display: "flex",
    alignItems: "center",
  },
  textFreeBreakFast: { color: theme.palette.green.greenLight7 },
  dialogCustomizedWidth: { maxWidth: 1106 },
}));
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
function SamplePrevArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, left: 0 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowDownSmall
        className={`svgFillAll ${classes.iconArrow}`}
        style={{ transform: "rotate(90deg)" }}
      />
    </div>
  );
}

function SampleNextArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, right: -38 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowDownSmall
        className={`svgFillAll ${classes.iconArrow}`}
        style={{ transform: "rotate(-90deg)" }}
      />
    </div>
  );
}
const customStyle = {
  background: "white",
  height: "40px",
  width: "40px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 2,
  borderRadius: "100px",
  boxShadow: "0px 8px 12px rgb(0 0 0 / 10%)",
  cursor: "pointer",
  position: "absolute",
  top: "50%",
  transform: "translate(-50%, -50%)",
};
const ImageRoomModal = ({ item = {}, handleClose, hotelId }) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const handleBook = () => {
    handleClose();
    const rateItem = !isEmpty(item.rates) ? item.rates[0] : {};
    router.push({
      pathname: routeStatic.BOOKING_HOTEL.href,
      query: {
        ...router.query,
        hotelId,
        roomKey: item.roomKey,
        rateKey: rateItem.rateKey,
      },
    });
  };
  const settings = {
    customPaging: (index) => {
      return (
        <img
          src={item.images[index]}
          alt=""
          style={{ width: 96, height: 72, borderRadius: 8, objectFit: "cover" }}
        />
      );
    },
    dots: true,
    dotsClass: "slick-dots slick-thumb",
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow customStyleArrow={customStyle} />,
    prevArrow: <SamplePrevArrow customStyleArrow={customStyle} />,
  };
  const getView = (view = []) => {
    return view.join(", ");
  };
  const arrayGroupAmenities = Object.values(item.groupAmenities);
  return (
    <Dialog
      // maxWidth="lg"
      fullWidth
      classes={{ paperFullWidth: classes.dialogCustomizedWidth }}
      open
      onClose={handleClose}
      TransitionComponent={Transition}
    >
      <Box className={classes.dialogContent}>
        <Box className={classes.leftContent}>
          {!isEmpty(item.images) && (
            <Slider {...settings}>
              {item.images.map((el, i) => (
                <div key={i}>
                  <img
                    src={el}
                    alt=""
                    style={{
                      width: 638,
                      height: 478,
                      borderRadius: 8,
                      objectFit: "cover",
                    }}
                  />
                </div>
              ))}
            </Slider>
          )}
        </Box>
        <Box className={classes.rightContent}>
          <Box className={classes.roomName}>{item.name}</Box>
          <Box className={classes.boxRightContent}>
            <Box className={classes.wrapRateRooms}>
              <Box className={classes.infoRoom}>
                {!isEmpty(item.roomArea) && (
                  <>
                    <Box className={classes.wrapIcon}>
                      <IconAcreage />
                    </Box>
                    <Box marginLeft={1}>{item.roomArea}m2 </Box>
                  </>
                )}
                {!isEmpty(item.views) && (
                  <>
                    <Box
                      className={classes.wrapIcon}
                      style={{ marginLeft: !isEmpty(item.roomArea) ? 24 : 0 }}
                    >
                      <IconView />
                    </Box>
                    <Box
                      marginLeft={1}
                      style={{
                        fontSize: 14,
                        lineHeight: "16px",
                        fontWeight: 400,
                      }}
                    >
                      {getView(item.views)}{" "}
                    </Box>
                  </>
                )}
              </Box>
              {item.rates[0].freeCancellation ? (
                <>
                  <Box
                    style={{
                      display: "flex",
                      alignItems: "center",
                      marginTop: "10px",
                    }}
                  >
                    <IconCancelPolicy
                      className={`svgFillAll ${classes.freeCancelPolicy}`}
                    />
                    <Typography
                      className={classes.textCancelPolicy}
                      style={{ color: "#48BB78" }}
                    >
                      {listString.IDS_MT_TEXT_FREE_CANCEL}
                    </Typography>
                    <IconInfo
                      className={`svgFillAll ${classes.infoActive}`}
                      // onClick={handleOpen}
                    />
                  </Box>
                  <Typography
                    variant="body2"
                    style={{ marginLeft: "22px", textAlign: "left" }}
                  >
                    {!isEmpty(item.rates[0].hotelCancellationPolicies) &&
                      item.rates[0].hotelCancellationPolicies.map((el, i) => (
                        <Typography
                          component="span"
                          className={classes.dateFreeCancel}
                          key={i}
                        >
                          {el.price === el.refundAmount &&
                            `Trước ${el.dateTo.split(" ")[0]}`}
                        </Typography>
                      ))}
                  </Typography>
                </>
              ) : (
                <Box
                  style={{
                    display: "flex",
                    alignItems: "center",
                    marginTop: 10,
                  }}
                >
                  <IconCancelPolicy />
                  <Typography className={classes.textCancelPolicy}>
                    {item.rates[0].shortCancelPolicy}
                  </Typography>
                  {item.rates[0].refundable && (
                    <IconInfo
                      className={`svgFillAll ${classes.info}`}
                      // onClick={handleOpen}
                    />
                  )}
                </Box>
              )}
              {item.rates[0].freeBreakfast ? (
                <Box className={classes.boxLeft}>
                  <IconBreakFast />
                  <Typography
                    className={clsx(
                      classes.textCancelPolicy,
                      classes.textFreeBreakFast
                    )}
                  >
                    {listString.IDS_MT_TEXT_ROOM_FREE_BREAK_FAST}
                  </Typography>
                </Box>
              ) : (
                <Box className={classes.boxLeft}>
                  <IconBreakFast
                    className={`svgFillAll ${classes.iconBreakFast}`}
                  />
                  <Typography className={classes.textCancelPolicy}>
                    {listString.IDS_MT_TEXT_ROOM_NO_FREE_BREAK_FAST}
                  </Typography>
                </Box>
              )}
            </Box>
            <Box className={classes.convient}>
              {listString.IDS_MT_TEXT_ROOM_CONVIENT}
            </Box>
            <Box className={classes.modalContent}>
              {!isEmpty(arrayGroupAmenities) &&
                arrayGroupAmenities.map((el, index) => (
                  <Box key={index.toString()} className={classes.wrapConvient}>
                    <Box>
                      <Typography
                        variant="subtitle2"
                        style={{
                          lineHeight: "19px",
                          fontSize: 16,
                          margin: "8px 0 16px 0",
                        }}
                      >
                        {el.name}
                      </Typography>
                    </Box>
                    <Box
                      key={index.toString()}
                      style={{ display: "flex", flexWrap: "wrap" }}
                    >
                      {!isEmpty(el.amenities) &&
                        el.amenities.map((val, idx) => (
                          <Box
                            key={idx}
                            style={{
                              display: "flex",
                              width: "50%",
                              alignItems: "center",
                              marginBottom: 16,
                            }}
                          >
                            <Box
                              style={{ width: 24, height: 24, marginRight: 12 }}
                            >
                              <img
                                src={val.icon}
                                style={{ width: 24, height: 24 }}
                                alt=""
                              />
                            </Box>
                            <Typography className={classes.childListConvient}>
                              {val.name}{" "}
                            </Typography>
                          </Box>
                        ))}
                    </Box>
                  </Box>
                ))}
            </Box>
          </Box>
          <ButtonComponent
            backgroundColor={theme.palette.secondary.main}
            width={372}
            height={44}
            fontSize={16}
            fontWeight={600}
            borderRadius={8}
            handleClick={handleBook}
            style={{ marginTop: 10 }}
          >
            {listString.IDS_MT_SELECT_ROOM}
          </ButtonComponent>
        </Box>
      </Box>
    </Dialog>
  );
};
export default ImageRoomModal;
