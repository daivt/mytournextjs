import { getHotelReviews, getTripAdvisorReviews } from "@api/hotels";
import ReviewCommentHotel from "@components/desktop/hotels/details/components/ReviewCommentHotel";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
// import { IconPencil } from "@public/icons";
import { listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import { useEffect, useState } from "react";
import WriteReviewModal from "./WriteReviewModal";

const useStyles = makeStyles((theme) => ({
  homeWrapper: { width: "100%", background: theme.palette.white.main },
  homeContainer: { maxWidth: 1188, margin: "0 auto", padding: "24px 0" },
  divider: {
    width: "100%",
    height: 4,
    background: "#EDF2F7",
    margin: "24px 0",
    fontSize: 20,
    fontWeight: 6,
    lineHeight: "23px",
  },
  wrapContent: {
    display: "flex",
    borderRadius: 8,
  },
  wrapHeader: {
    display: "flex",
    justifyContent: "space-between",
  },
  textEvaluate: {
    fontSize: 20,
    lineHeight: "23px",
    fontWeight: 600,
    color: theme.palette.black.black3,
  },
  writeEvaluate: {
    display: "flex",
    alignItems: "center",
    border: `1px solid ${theme.palette.blue.blueLight8}`,
    borderRadius: 8,
  },
  textWriteEvaluate: {
    display: "flex",
    alignItems: "center",
    fontSize: 16,
    fontWeight: 400,
    lineHeight: "19px",
    color: theme.palette.blue.blueLight8,
    cursor: "pointer",
    padding: "10px 8px 9px 8px",
  },
  reviewAll: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    marginLeft: 10,
    color: theme.palette.green.greenLight7,
  },
  iconPencil: {
    stroke: theme.palette.blue.blueLight8,
    marginLeft: 8,
  },
}));
const pagination = { page: 1, size: 10 };
const EvaluateHotel = ({ hotelDetail = {}, setFirstReview, id }) => {
  const classes = useStyles();
  const [reviews, setReviews] = useState([]);
  const [reviewsTripadvisor, setReviewsTripadvisor] = useState([]);
  const [loading, setLoading] = useState(true);

  const fetchReviews = async (filter) => {
    try {
      setLoading(true);
      const dataDTO = { size: 5, hotelId: hotelDetail.id, ...filter };
      const { data } = await getHotelReviews(dataDTO);
      if (data.code === 200 && !isEmpty(data.data)) {
        setReviews(data.data);
        if (pagination.page === 1 && !isEmpty(data.data.items)) {
          setFirstReview(data.data.items[0]);
        }
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };
  const fetchReviewTripadvisor = async (filter) => {
    try {
      setLoading(true);
      const dataDTO = { size: 5, hotelId: hotelDetail.id, ...filter };
      const { data } = await getTripAdvisorReviews(dataDTO);
      if (data.code === 200 && !isEmpty(data.data)) {
        setReviewsTripadvisor(data.data);
      }
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchReviews();
    fetchReviewTripadvisor(); // eslint-disable-next-line
  }, [hotelDetail.id]);
  if (!reviews) return null;
  return (
    <Box className={classes.homeWrapper} id={id}>
      <Box className={classes.wrapHeader}>
        <Box className={classes.textEvaluate}>
          {listString.IDS_MT_TEXT_EVALUATE}
          <Box component="span" className={classes.reviewAll}>
            {listString.IDS_MT_ALL_REVIEW_FORM_MYTOUR}
          </Box>
        </Box>
        {/* <Box className={classes.writeEvaluate}>
          <Box>
            <IconPencil className={`svgFillAll ${classes.iconPencil}`} />
          </Box>
          <Box className={classes.textWriteEvaluate}>
            {listString.IDS_MT_TEXT_WRITE_REVIEW_HOTEL}
          </Box>
        </Box> */}
      </Box>
      <ReviewCommentHotel
        hotelDetail={hotelDetail}
        reviews={reviews || {}}
        reviewsTripadvisor={reviewsTripadvisor || {}}
        fetchReviews={fetchReviews}
        fetchReviewTripadvisor={fetchReviewTripadvisor}
        loading={loading}
      />
      <WriteReviewModal hotelDetail={hotelDetail} />
    </Box>
  );
};

export default EvaluateHotel;
