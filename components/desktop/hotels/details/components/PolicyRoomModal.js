import {
  Box,
  Drawer,
  IconButton,
  makeStyles,
  Typography,
  useTheme,
} from "@material-ui/core";
import { IconClose, IconDot } from "@public/icons";
import { listString, optionClean } from "@utils/constants";
import { isEmpty, unEscape } from "@utils/helpers";
import moment from "moment";
import { useState } from "react";
import sanitizeHtml from "sanitize-html";

const useStyles = makeStyles((theme) => ({
  paper: {
    borderRadius: 8,
    width: 784,
    height: "fit-content",
    overflow: "auto",
    outline: "none",
    background: theme.palette.white.main,
  },
  wrapButtonClose: { padding: 0 },
  wraperHeader: {
    padding: "12px 16px",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    height: 48,
    display: "flex",
    alignItems: "center",
  },
  wrapHeaderDraw: {
    display: "flex",
    alignItems: "center",
    height: 48,
    borderBottom: "1px solid #EDF2F7",
    width: "100%",
  },
  textFilter: {
    textAlign: "center",
    width: "calc(100% - 40px)",
  },
  wraperContent: {
    margin: 16,
    wordBreak: "break-all",
    fontSize: 14,
    lineHeight: "22px",
  },
  wrapGrid: {
    display: "flex",
    marginTop: 12,
  },
  iconDot: {
    marginRight: 12,
    marginTop: 8,
  },
}));
const getTime = (time) => {
  const temp = !isEmpty(time) && time.split(":");
  temp.pop();
  return temp.join(":");
};

const getContentPolicy = (type, refundAmount, percent, dateFrom, dateTo) => {
  const dateToText = !isEmpty(dateTo) ? ` ${dateTo.split(" ")[0]}` : "";
  const dateFromText = !isEmpty(dateFrom) ? ` ${dateFrom.split(" ")[0]}` : "";
  let resultText = "";
  switch (type) {
    case "amount":
      if (dateFrom === null) {
        resultText = `Bạn sẽ không được hoàn tiền nếu hủy phòng từ ${getTime(
          dateTo.split(" ")[1]
        )} ngày ${dateToText}`;
      } else if (dateTo === null) {
        resultText = `Chúng tôi miễn phí hoàn hủy nếu hủy phòng trước ${getTime(
          dateFrom.split(" ")[1]
        )} ngày ${dateFromText} `;
      } else {
        resultText = `Bạn sẽ được hoàn ${refundAmount.formatMoney()} VNĐ nếu hủy phòng từ ${getTime(
          dateTo.split(" ")[1]
        )} ngày ${dateToText} đến trước ${getTime(
          dateFrom.split(" ")[1]
        )} ngày ${dateFromText}`;
      }
      return resultText;

    case "percent":
      if (dateFrom === null) {
        resultText = `Bạn sẽ không được hoàn tiền nếu hủy phòng từ ${
          dateTo.split(" ")[1]
        } ngày ${dateToText}`;
      } else if (dateTo === null) {
        resultText = `Bạn sẽ được hoàn ${percent} % nếu hủy phòng trước  ${
          dateFrom.split(" ")[1]
        } ngày ${dateFromText} `;
      } else {
        resultText = `Bạn sẽ được hoàn ${percent} %  nếu hủy phòng từ  ${
          dateTo.split(" ")[1]
        } ngày ${dateToText} ${
          dateFrom.split(" ")[1]
        } đến trước  ngày ${dateFromText}`;
      }
      return resultText;
    default:
      return "";
  }
};

const PolicyRoomlModal = ({ item = {}, handleClose = () => {} }) => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Box className={classes.paper}>
      <Box className={classes.wraperHeader}>
        <Box className={classes.wrapHeaderDraw}>
          <IconButton
            onClick={() => {
              handleClose();
            }}
            className={classes.wrapButtonClose}
          >
            <IconClose />
          </IconButton>
          <Typography variant="subtitle1" className={classes.textFilter}>
            {listString.IDS_MT_TEXT_ROOM_CANCEL_POLICY}
          </Typography>
        </Box>
      </Box>
      <Box className={classes.wraperContent}>
        <Typography variant="subtitle1">
          {listString.IDS_MT_TEXT_ROOM_CANCEL_POLICY}
        </Typography>
        {!isEmpty(item.hotelCancellationPolicies) && !item.hiddenPrice ? (
          item.hotelCancellationPolicies.map((el, index) => (
            <Box className={classes.wrapGrid} key={index.toString()}>
              <Box className={classes.iconDot}>
                <IconDot />
              </Box>

              <Box className={classes.policy}>
                {getContentPolicy(
                  el.cancellationType,
                  el.refundAmount,
                  el.percent,
                  el.dateTo,
                  el.dateFrom
                )}
              </Box>
            </Box>
          ))
        ) : (
          <Box style={{ fontSize: 14, lineHeight: "16px", marginTop: 10 }}>
            Đăng nhập để xem giá và chính sách
          </Box>
        )}

        {!isEmpty(item.checkinInstructions) && (
          <>
            <Typography variant="subtitle1" style={{ marginTop: "16px" }}>
              {listString.IDS_MT_TEXT_POLICY_NOTE}
            </Typography>
            <Box
              fontSize={14}
              color="gray.grayDark1"
              lineHeight="22px"
              dangerouslySetInnerHTML={{
                __html: sanitizeHtml(
                  unEscape(unescape(item.checkinInstructions || "")),
                  optionClean
                ),
              }}
            />
          </>
        )}
      </Box>
    </Box>
  );
};
PolicyRoomlModal.propTypes = {};
export default PolicyRoomlModal;
