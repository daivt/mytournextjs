import { useEffect, useState, forwardRef } from "react";
import {
  Box,
  Dialog,
  IconButton,
  makeStyles,
  Slide,
  useTheme,
} from "@material-ui/core";
import { useRouter } from "next/router";
import ButtonComponent from "@src/button/Button";
import { getInterestingPlaces } from "@api/hotels";
import { listString, routeStatic } from "@utils/constants";
import CloseIcon from "@material-ui/icons/Close";
import MapDetail from "@components/common/map/MapDetail";
import { isEmpty } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  boxContainer: { display: "flex", minHeight: 340 },
  boxLeft: { width: "40%" },
  boxRight: { width: "60%", marginLeft: 40 },
  policyHotel: {
    paddingBottom: 24,
    fontSize: 20,
    lineHeight: "24px",
    fontWeight: 600,
  },
  container: { background: "white", width: "100%" },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    marginBottom: 72,
    marginTop: 12,
  },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
  },
  dialogContent: { padding: "12px 24px", overflow: "auto" },
  itemContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 20,
    color: "#1A202C",
  },
  itemInfo: { display: "flex", alignItems: "center" },
  icon: {
    width: 40,
    height: 40,
    background: "rgba(229, 62, 62, 0.1)",
    borderRadius: 16,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    cursor: "pointer",
  },
}));

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
const TopAddress = ({ hotelDetail = {}, roomItem = {}, id }) => {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = useState(false);
  const router = useRouter();
  const [places, setPlaces] = useState({});
  const [placeActive, setPlace] = useState({});
  const coordinate = !isEmpty(hotelDetail.address)
    ? hotelDetail.address.coordinate
    : {};
  const fetchData = async () => {
    if (isEmpty(coordinate)) return;
    try {
      const { data } = await getInterestingPlaces({
        page: 1,
        size: 40,
        hotelId: hotelDetail.id,
        latitude: coordinate.latitude,
        longitude: coordinate.longitude,
      });
      if (data.code === 200) setPlaces(data.data);
    } catch (error) {}
  };
  useEffect(() => {
    fetchData(); // eslint-disable-next-line
  }, [hotelDetail.id]);
  const handleBook = () => {
    setOpen(false);
    const rateItem = !isEmpty(roomItem.rates) ? roomItem.rates[0] : {};
    router.push({
      pathname: routeStatic.BOOKING_HOTEL.href,
      query: {
        ...router.query,
        hotelId: hotelDetail.id,
        roomKey: roomItem.roomKey,
        rateKey: rateItem.rateKey,
      },
    });
  };
  const getPlaceItem = (el, i) => {
    const iconInfo = (places.types || []).find((v) => el.typeId === v.id);
    return (
      <Box className={classes.itemContainer} key={i}>
        <Box className={classes.itemInfo}>
          {iconInfo && (
            <Box className={classes.icon} onClick={() => setPlace(el)}>
              <img
                src={iconInfo.icon}
                alt=""
                style={{ width: 20, height: 20 }}
              />
            </Box>
          )}
          <Box
            style={{
              display: "flex",
              flexDirection: "column",
              marginLeft: 16,
            }}
          >
            <span style={{ fontSize: 16, lineHeight: "19px" }}>{el.name}</span>
            <span
              style={{
                fontSize: 14,
                lineHeight: "17px",
                color: "#4A5568",
                marginTop: 2,
              }}
            >
              {iconInfo.name}
            </span>
          </Box>
        </Box>
        <span style={{ fontSize: 14, lineHeight: "17px" }}>
          {el.distanceMeters > 999
            ? `${el.distanceMeters / 1000} km`
            : `${el.distanceMeters} m`}
        </span>
      </Box>
    );
  };
  return (
    <>
      <Box className={classes.wrapHeader} id={id}>
        <Box className={classes.policyHotel}>
          {listString.IDS_MT_HIGHT_LIGHT_ADDRESS}
        </Box>
      </Box>
      <Box className={classes.boxContainer}>
        <Box className={classes.boxLeft}>
          {!isEmpty(places.items) &&
            places.items.map((el, i) => {
              if (i >= 5) return null;
              return getPlaceItem(el, i);
            })}

          <ButtonComponent
            typeButton="outlined"
            backgroundColor="inherit"
            color="#00B6F3"
            width="100%"
            borderColor="#00B6F3"
            borderRadius={8}
            padding="12px 50px"
            handleClick={() => setOpen(true)}
          >
            {listString.IDS_MT_MORE_ADDRESS}
          </ButtonComponent>
        </Box>
        <Box className={classes.boxRight}>
          <MapDetail
            position={!isEmpty(placeActive) ? placeActive : coordinate}
            markerPosition={coordinate}
            places={places}
            type="top_address"
          />
        </Box>
      </Box>

      <Dialog
        maxWidth="lg"
        fullWidth
        open={open}
        onClose={() => setOpen(false)}
        TransitionComponent={Transition}
      >
        <Box className={classes.boxContainer}>
          <Box className={classes.boxLeft}>
            <IconButton
              style={{ marginLeft: 18 }}
              onClick={() => setOpen(false)}
            >
              <CloseIcon />
            </IconButton>
            <Box
              style={{
                maxHeight: 720,
                minHeight: 720,
                overflowY: "auto",
                padding: "0 24px",
              }}
            >
              {!isEmpty(places.items) &&
                places.items.map((el, i) => {
                  return getPlaceItem(el, i);
                })}
            </Box>

            <ButtonComponent
              backgroundColor={theme.palette.secondary.main}
              height={44}
              fontSize={16}
              fontWeight={600}
              borderRadius={8}
              style={{ margin: "12px 24px", width: "calc(100% - 48px)" }}
              handleClick={handleBook}
            >
              {listString.IDS_MT_SELECT_ROOM}
            </ButtonComponent>
          </Box>
          <Box
            className={classes.boxRight}
            style={{ minHeight: 800, marginLeft: 0 }}
          >
            <MapDetail
              position={!isEmpty(placeActive) ? placeActive : coordinate}
              markerPosition={coordinate}
              places={places}
            />
          </Box>
        </Box>
      </Dialog>
    </>
  );
};

export default TopAddress;
