import { Box, Typography } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/styles";
import { IconSad } from "@public/icons";
import { listString } from "@utils/constants";
import ButtonComponent from "@src/button/Button";
import { useDispatchSystem } from "@contextProvider/ContextProvider";
import { actionUpdateSearchDate } from "@contextProvider/system/Actions";
import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  wrapIconResult: {
    marginTop: 64,
  },
  wrapResultSearch: {
    background: "white",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    border: "1px solid #EDF2F7",
    boxShadow: "0px 1px 1px rgba(0, 0, 0, 0.05)",
    borderRadius: 8,
  },
  missRoom: {
    fontSize: 16,
    lineHeight: "19px",
    fontWeight: 600,
  },
  result: {
    display: "flex",
    marginTop: 16,
    lineHeight: "19px",
    alignItems: "center",
  },
  textSearchAgain: {
    fontSize: 14,
    fontWeight: 400,
    lineHeight: "22px",
    marginTop: 10,
    color: theme.palette.black.black3,
    width: 627,
    textAlign: "center",
    marginBottom: 16,
  },
  btnBook: {
    marginBottom: 64,
  },
  imageEmpty: {
    width: 193,
    height: 170,
  },
}));

const NoResultRoomDetails = () => {
  const classes = useStyles();
  const theme = useTheme();
  const dispatch = useDispatchSystem();
  const openSearchDate = () => {
    dispatch(actionUpdateSearchDate(true));
  };
  return (
    <>
      <Box className={classes.wrapResultSearch}>
        <Box className={classes.wrapIconResult}>
          <Image
            srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_no_result_search_listing.svg"
            className={classes.imageEmpty}
          />
        </Box>
        <Typography variant="subtitle1" className={classes.result}>
          <IconSad style={{ marginRight: 8 }} />
          <Box className={classes.missRoom}>Bạn bỏ lỡ mất rồi</Box>
        </Typography>
        <Box className={classes.textSearchAgain}>
          Phòng cuối cùng của chúng tôi vừa được bán hết. Vui lòng chọn ngày
          khác hoặc liên hệ với chúng tôi qua&nbsp;
          <strong>hotline: 1900 2083</strong> để được hỗ trợ liên hệ khách sạn
        </Box>
        <ButtonComponent
          backgroundColor={theme.palette.secondary.main}
          height={48}
          width={174}
          fontSize={16}
          fontWeight={600}
          borderRadius={8}
          className={classes.btnBook}
          handleClick={openSearchDate}
        >
          Chọn ngày khác
        </ButtonComponent>
      </Box>
    </>
  );
};
NoResultRoomDetails.propTypes = {};
export default NoResultRoomDetails;
