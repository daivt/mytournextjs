import { Box, Typography } from "@material-ui/core";
import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import HotelItemVertical from "@components/common/desktop/itemView/HotelItemVertical";
import { adapterHotelAvailability, isEmpty } from "@utils/helpers";
import SlideShow from "@components/common/slideShow/SlideShow";
import { IconArrowRight } from "@public/icons";
import {
  DELAY_TIMEOUT_POLLING,
  listString,
  RADIUS_GET_HOTEL_LAT_LONG,
} from "@utils/constants";
import { getHotelsAvailability } from "@api/hotels";

const useStyles = makeStyles((theme) => ({
  container: { background: "white", width: "100%", borderRadius: 12 },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "column",
  },
  hotelGroup: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between",
  },
  hotelText: { display: "flex", flexDirection: "column" },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
  },
  imageContent: {
    borderRadius: 8,
    margin: "0 -12px",
    marginTop: 24,
    display: "flex",
    "& > .slick-list": {
      width: "calc(100% + 12px)",
      margin: "0 -6px",
    },
  },
  titleSubText: {
    fontSize: 14,
    lineHeight: "17px",
    color: "#4A5568",
    marginTop: 8,
  },
  iconArrow: {
    width: 16,
    height: 17,
    stroke: theme.palette.black.black3,
  },
  wrapArrow: {
    "&:hover": {
      "& svg": {
        stroke: theme.palette.blue.blueLight8,
        transition: "all ease .2s",
      },
    },
  },
  divider: {
    width: "100%",
    height: 4,
    background: "#EDF2F7",
    margin: "24px 0",
  },
}));

const customStyle = {
  background: "white",
  height: "40px",
  width: "40px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 2,
  borderRadius: "100px",
  boxShadow: "0px 8px 12px rgb(0 0 0 / 10%)",
  cursor: "pointer",
  position: "absolute",
  top: "50%",
  transform: "translate(-50%, -50%)",
};
function SamplePrevArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, left: 0 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight
        className={`svgFillAll ${classes.iconArrow}`}
        style={{ transform: "rotate(180deg)" }}
      />
    </div>
  );
}

function SampleNextArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, right: -34 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight className={`svgFillAll ${classes.iconArrow}`} />
    </div>
  );
}
const HotelAround = ({ hotelDetail = {}, paramsUrl = {} }) => {
  let breakPolling = true;
  const classes = useStyles();
  const [listHotel, setListHotel] = useState([]);
  const [idxActive, setActive] = useState(0);

  const settings = {
    dots: false,
    infinite: listHotel.length > 4,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    nextArrow: <SampleNextArrow customStyleArrow={customStyle} />,
    prevArrow:
      idxActive > 0 ? (
        <SamplePrevArrow customStyleArrow={customStyle} />
      ) : (
        <></>
      ),
    beforeChange: (current, next) => setActive(next),
  };

  const getRecentHotel = async () => {
    const coordinate = !isEmpty(hotelDetail.address)
      ? hotelDetail.address.coordinate
      : {};
    if (isEmpty(coordinate)) return;
    let polling = true;
    try {
      const params = {
        ...paramsUrl,
        latitude: coordinate.latitude,
        longitude: coordinate.longitude,
        radius: RADIUS_GET_HOTEL_LAT_LONG,
        searchType: "latlon",
        page: 1,
        size: 10,
      };
      while (polling && breakPolling) {
        const { data } = await getHotelsAvailability(params);
        if (data.code === 200) {
          if (data.data.completed) {
            setListHotel(data.data.items);
            polling = false;
            break;
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      polling = false;
    }
  };
  useEffect(() => {
    if (!isEmpty(hotelDetail)) {
      getRecentHotel();
    } // eslint-disable-next-line
  }, [hotelDetail]);

  useEffect(() => {
    return () => {
      breakPolling = false;
    };
  }, []);
  if (isEmpty(listHotel)) return null;
  return (
    <>
      <Box className={classes.divider} />
      <Box className={classes.container}>
        <Box className={classes.content}>
          <Box className={classes.hotelText}>
            <Typography variant="body2" className={classes.titleText}>
              {listString.IDS_MT_HOTEL_AROUND}
            </Typography>
          </Box>

          {!isEmpty(listHotel) && (
            <SlideShow settingProps={settings} className={classes.imageContent}>
              {adapterHotelAvailability(listHotel).map((el) => {
                if (el.id === hotelDetail.id) return null;
                return (
                  <Box style={{ marginBottom: 24 }} key={el.id}>
                    <HotelItemVertical
                      item={el}
                      isTopSale
                      isHotelInterest
                      isShowLastBook
                      isShowStatus
                      isCountBooking={false}
                      isCountBooking
                      isShowStatus={false}
                    />
                  </Box>
                );
              })}
            </SlideShow>
          )}
        </Box>
      </Box>
    </>
  );
};

export default HotelAround;
