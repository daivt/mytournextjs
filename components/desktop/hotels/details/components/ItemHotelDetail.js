import { useState } from "react";
import ItemRatesRoom from "@components/desktop/hotels/details/components/ItemRatesRoom";
import {
  Box,
  makeStyles,
  TableBody,
  Typography,
  useTheme,
} from "@material-ui/core";
import { useRouter } from "next/router";
import Table from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import {
  IconAcreage,
  IconBedDouble,
  IconBedSingle,
  IconUserOrther,
  IconView,
  IconArrowDownToggle,
} from "@public/icons";
import ButtonComponent from "@src/button/Button";
import Image from "@src/image/Image";
import { isEmpty, getMemberDeal } from "@utils/helpers";
import {
  listString,
  routeStatic,
  listEventHotel,
  prefixUrlIcon,
  listIcons,
} from "@utils/constants";
import clsx from "clsx";
import moment from "moment";
import sortBy from "lodash/sortBy";
import AuthWrapper from "@components/layout/desktop/components/AuthWrapper";

import * as gtm from "@utils/gtm";
const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
    "& .MuiPaper-elevation1": {
      boxShadow: "none",
    },
  },
  wrapContainer: {
    padding: "0 24px",
    display: "flex",
    background: "white",
    border: "1px solid #EDF2F7",
    boxSizing: "border-box",
    boxShadow: "0px 1px 1px rgba(0, 0, 0, 0.05)",
    borderRadius: 8,
    marginBottom: 16,
  },
  wrapAlbum: { width: 202, paddingTop: 24 },
  hoverImages: {
    display: "none",
  },
  wrapImage: {
    position: "relative",
    cursor: "pointer",
    "&:hover": {
      "& > .MuiBox-root:first-child": {
        position: "absolute",
        display: "block",
        left: "calc(100% + 16px)",
        transition: "all ease .2s",
      },
    },
  },
  imgLarge: {
    width: "100%",
    height: 152,
    objectFit: "cover",
    zIndex: 0,
  },
  wrapImageSmall: { display: "flex", marginTop: 2 },
  loadViewImages: {
    fontSize: 14,
    fontWeight: 400,
    lineHeight: "16px",
    marginTop: 10,
    color: theme.palette.blue.blueLight8,
    textAlign: "left",
    "&:hover": { cursor: "pointer" },
  },
  imageLarge: {
    position: "relative",
    width: 202,
  },
  imgSmall: { width: "100%", height: 50, objectFit: "cover" },
  imgSmallCenter: { padding: "0 2px" },
  wrapGuest: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "24px 37px 0 27px",
    whiteSpace: "nowrap",
  },
  wrapBoxLeft: { marginLeft: 16, width: "100%" },
  wrapRoomAndPrice: {
    fontSize: 12,
    color: theme.palette.black.black3,
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    padding: "16px 0 0 0",
    width: "100%",
  },
  roomName: { fontSize: 14, lineHeight: "16px", fontWeight: 400 },
  subPrice: {
    textDecorationLine: "line-through",
    color: theme.palette.gray.grayDark8,
    margin: "6px 0 0 0",
    fontSize: 14,
    lineHeight: "16px",
  },
  mainPrice: {
    fontWeight: 600,
    fontSize: 20,
    lineHeight: "23px",
    marginTop: 2,
  },
  discountPercent: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 42,
    minHeight: 18,
    color: theme.palette.white.main,
    background: theme.palette.pink.main,
    borderRadius: "3px 3px 0px 3px",
    position: "relative",
    fontSize: 12,
    fontWeight: 600,
    lineHeight: "14px",
    marginTop: 12,
  },
  discountText: {
    padding: "2px 4px",
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 600,
  },
  triangleBottomright: {
    width: 0,
    height: 0,
    borderStyle: "solid",
    borderColor: `transparent ${theme.palette.pink.main} transparent transparent`,
    borderWidth: "0px 5px 5px 0",
    position: "absolute",
    right: 0,
    bottom: -4,
  },
  favorite: { fill: "#FF1284", stroke: theme.palette.white.main },
  notFavorite: { fill: "rgba(0, 0, 0, 0.6)", stroke: theme.palette.white.main },
  ratingVote: { color: theme.palette.yellow.yellowLight3 },
  hotelName: {
    fontWeight: 600,
    color: theme.palette.black.black3,
    display: "-webkit-box",
    WebkitLineClamp: 3,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
    fontSize: 16,
    lineHeight: "19px",
  },
  iconNoBreakFast: { stroke: theme.palette.gray.grayDark8, marginRight: 6 },
  iconCar: { stroke: theme.palette.gray.grayDark8, marginRight: 6 },
  hotelLocation: {
    fontSize: 12,
    lineHeight: "14px",
    display: "-webkit-box",
    WebkitLineClamp: 2,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
  },
  iconBreakFast: {
    width: 16,
    height: 16,
    marginRight: 6,
    stroke: theme.palette.green.greenLight7,
  },
  wrapPriceOnMobile: {
    background: theme.palette.blue.blueLight9,
    color: theme.palette.blue.blueLight8,
    display: "flex",
    alignItems: "center",
    marginTop: 6,
    borderRadius: 4,
  },
  discountBookingApp: {
    fontSize: 12,
    fontWeight: 600,
    color: theme.palette.blue.blueLight8,
  },
  wrapPromoCode: {
    border: `1px dashed ${theme.palette.gray.grayLight25}`,
    display: "flex",
    alignItems: "center",
    paddingTop: 7,
    flexDirection: "column",
    marginTop: 12,
    borderRadius: 8,
    whiteSpace: "no-wrap",
  },
  wrapMissAllotment: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    color: theme.palette.red.redLight5,
    background: theme.palette.red.redLight6,
    borderRadius: 8,
    width: 224,
    padding: 10,
    marginTop: 5,
  },
  discountPromoCode: {
    background: theme.palette.blue.blueLight8,
    color: theme.palette.white.main,
    padding: "2px 4px",
    borderRadius: 3,
    marginLeft: 4,
  },
  promoCode: {
    borderRadius: 3,
    border: "1px dashed rgba(72, 187, 120, 0.5)",
    background: theme.palette.green.greenLight8,
    padding: "2px 4px 1px 4px",
    marginLeft: 5,
    fontWeight: 600,
  },
  pricePromo: {
    fontSize: 20,
    lineHeight: "23px",
    fontWeight: 600,
    color: theme.palette.pink.main,
    marginTop: 3,
  },
  wrapCode: {
    display: "flex",
    alignItems: "center",
    marginTop: 6,
    padding: "2px 4px",
    borderRadius: 3,
  },
  wrapLastBooking: {
    marginTop: 10,
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.blue.blueLight8,
    background: theme.palette.blue.blueLight9,
    borderRadius: 4,
    padding: "4px 8px 3px 6px",
    width: 178,
    whiteSpace: "nowrap",
  },
  wrapHotCode: {
    color: theme.palette.green.greenLight7,
    display: "flex",
    alignItems: "center",
  },
  priceOfFreeCancel: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    color: theme.palette.green.greenLight7,
    marginTop: 6,
  },
  wrapLoginUsePromo: { display: "flex", textAlign: "right" },
  discountMember: {
    marginTop: 6,
    fontSize: 14,
    lineHeight: "17px",
    fontStyle: "italic",
    textAlign: "right",
  },
  verticalItem: {
    textAlign: "center",
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    verticalAlign: "top",
    padding: 0,
    borderLeft: "1px solid rgba(224, 224, 224, 1)",
    borderTop: "1px solid rgba(224, 224, 224, 1)",
    "&.MuiTableCell-root": { borderBottom: "none" },
  },
  verticalItemFirst: {
    paddingLeft: 0,
    borderTop: "1px solid rgba(224, 224, 224, 1)",
    "&.MuiTableCell-root": { borderBottom: "none" },
    verticalAlign: "top",
    width: 381,
    borderLeft: "none",
  },
  noneBorderTop: { borderTop: "none" },
  wrapPromo: {
    border: `1px dashed ${theme.palette.gray.grayLight23} `,
    display: "flex",
    justifyContent: "space-between",
    borderRadius: 4,
    marginTop: 8,
  },
  discountPromo: {
    fontSize: 12,
    lineHeight: "14px",
    fontWeight: 600,
    borderRadius: "0px 100px 100px 0px",
    background: theme.palette.pink.main,
    color: theme.palette.white.main,
    margin: "11px 0 11px 0",
    display: "flex",
    alignItems: "center",
    padding: "5px 4px",
  },
  wrapCodeDiscount: {
    fontSize: 14,
    fontWeight: 600,
    lineHeight: "16px",
    color: theme.palette.white.main,
    background: theme.palette.blue.blueLight8,
    padding: "1px 3px",
    margin: "0 4px ",
    borderRadius: 3,
    alignItems: "center",
    display: "flex",
  },
  wrapPriceCode: {
    fontSize: 20,
    lineHeight: "23px",
    fontWeight: 600,
    margin: "3px 0 7px -8px",
    width: "100%",
    textAlign: "right",
    wordBreak: "break-word",
  },
  night: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    color: theme.palette.pink.main,
    marginLeft: 3,
  },
  wrapCodeText: {
    color: theme.palette.blue.blueLight8,
    alignItems: "center",
    display: "flex",
    fontSize: 14,
    fontWeight: 600,
    lineHeight: "16px",
  },
  wrapRoomName: {
    display: "flex",
    alignItems: "center",
    color: theme.palette.black.black3,
    paddingRight: 10,
    marginTop: 24,
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
  },
  infoRoom: {
    display: "flex",
    alignItems: "center",
    margin: "8px 0 15px 0 ",
  },
  IconUser2: {
    width: 12,
    height: 12,
  },
  toolTipPrice: {
    display: "none",
    justifyContent: "space-between",
    width: 267,
    zIndex: 2,
    right: -70,
    bottom: -111,
    position: "relative",
  },
  detailsPrice: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    color: theme.palette.blue.blueLight8,
    margin: "12px 0 30px",
    cursor: "pointer",
    position: "relative",
    "&:hover": {
      "& > .MuiBox-root": {
        display: "flex",
        border: "1px solid #E2E8F0",
        borderRadius: 8,
        boxShadow: " 0px 8px 8px rgba(0, 0, 0, 0.1)",
        position: "absolute",
        background: "#fff",
        color: theme.palette.black.black3,
        top: 25,
        height: 160,
        minWidth: 361,
        overflowY: "auto",
      },
    },
  },
  itemText: {
    margin: "12px 0 0 12px",
  },
  itemPrice: {
    margin: "12px 12px 0 12px",
  },
  triangleTopight: {
    position: "absolute",
    transform: "rotate(180deg)",
    background: theme.palette.white.main,
    right: 90,
    top: -8,
  },
  iconTriage: {
    stroke: theme.palette.gray.grayLight23,
  },
  iconBedDouble: {
    fill: theme.palette.gray.grayDark7,
  },
  iconRecommendImg: {
    width: 111,
    height: 24,
    position: "absolute",
    top: -6,
    left: -14,
  },
  iconDoubleSingleBed: {
    width: 42,
    height: 24,
  },
}));

const ItemHotelDetail = ({
  item = {},
  paramsQuery = {},
  roomRecommend = false,
  openImageRoom,
  hotelDetail = {},
}) => {
  const classes = useStyles();
  const router = useRouter();
  const theme = useTheme();
  const checkIn = moment(router.query.checkIn, "DD-MM-YYYY");
  const checkOut = moment(router.query.checkOut, "DD-MM-YYYY");
  const numRooms = router.query.rooms;
  const [activeImage, setActiveImage] = useState("");
  const countNight = Math.abs(
    moment(checkIn)
      .startOf("day")
      .diff(checkOut.startOf("day"), "days")
  );
  const handleBook = (rateItem) => {
    let result = {
      roomKey: item.roomKey,
      rateKey: rateItem.rateKey,
      promotionCode: !isEmpty(rateItem.promotions)
        ? rateItem.promotions[0].code
        : "",
      ...paramsQuery,
    };
    if (!isEmpty(paramsQuery.childrenAges)) {
      result = { ...result, childrenAges: paramsQuery.childrenAges.join(",") };
    }

    gtm.addEventGtm(listEventHotel.HotelAddToCart);
    router.push({
      pathname: routeStatic.BOOKING_HOTEL.href,
      query: { roomKey: item.roomKey, rateKey: rateItem.rateKey, ...result },
    });
  };
  const getTableStyle = (className, idx, isFirst) => {
    return idx === 0 && isFirst
      ? clsx(className, classes.noneBorderTop)
      : className;
  };
  const getListBed = (rate = {}) => {
    let listBed = [];
    rate.bedGroups.forEach((el) => {
      let bedTemp = {
        shrui: el.shrui,
        bedInfo: el.bedInfo,
      };
      let bedTypesTemp = [];
      el.bedTypes.forEach((it) => {
        for (let index = 0; index < it.numberOfBed; index += 1) {
          bedTypesTemp.push(it.size);
        }
      });
      bedTemp = {
        ...bedTemp,
        bedTypes: bedTypesTemp,
        numberBed: bedTypesTemp.length,
      };
      listBed.push(bedTemp);
    });
    listBed = sortBy(listBed, ["numberBed"]);
    return listBed;
  };
  const getView = (view = []) => {
    return view.join(", ");
  };

  // tong gia chua app ma khuyen mai
  const totalPrice = (ratePrice) => {
    return ratePrice.price;
  };
  const getDiscount = (ratePrice) => {
    const priceDetail = !isEmpty(ratePrice) ? ratePrice.priceDetail : {};
    return (
      Math.floor(Math.abs(priceDetail.discount) / (numRooms * countNight)) || 0
    );
  };

  const nightPrice = (ratePrice) => {
    return ratePrice.basePrice;
  };

  // phu phi
  const totalSurcharge = (ratePrice) => {
    const priceDetail = !isEmpty(ratePrice) ? ratePrice.priceDetail : {};
    const occupancies = !isEmpty(priceDetail) ? priceDetail.occupancies : [];
    return Math.floor(
      occupancies.map((el) => el.surchargePerDay).reduce((a, b) => a + b, 0) /
        numRooms
    );
  };

  // thue va phi dich vu khach san
  const vatPrice = (ratePrice) => {
    return (
      totalPrice(ratePrice) -
      nightPrice(ratePrice) +
      getDiscount(ratePrice) -
      totalSurcharge(ratePrice)
    );
  };

  const dataList = item.rates;
  return (
    <>
      <Box className={classes.wrapBox}>
        <Box
          container="false"
          className={classes.wrapContainer}
          style={roomRecommend ? { border: "1px solid #00B6F3" } : {}}
        >
          <Box className={classes.wrapAlbum}>
            <Box className={classes.wrapImage} onClick={openImageRoom}>
              <Box className={classes.hoverImages}>
                {!isEmpty(activeImage) ? (
                  <img
                    src={activeImage}
                    style={{
                      maxWidth: 465,
                      maxHeight: 349,
                      borderRadius: 8,
                      position: "relative",
                      zIndex: 1,
                    }}
                    alt=""
                  />
                ) : (
                  <Box
                    style={{
                      width: 465,
                      height: 349,
                      borderRadius: 8,
                      position: "relative",
                      zIndex: 1,
                      background: "#EDF2F7",
                    }}
                    alt=""
                  />
                )}
              </Box>
              <Box
                className={classes.imageLarge}
                onMouseMove={() => setActiveImage(item.images[0])}
              >
                <Image
                  srcImage={!isEmpty(item.images) && item.images[0]}
                  className={classes.imgLarge}
                  style={{
                    borderRadius:
                      isEmpty(item.images[1]) &&
                      isEmpty(item.images[2]) &&
                      isEmpty(item.images[3])
                        ? "8px"
                        : "8px 8px 0px 0px",
                  }}
                  borderRadiusProp={
                    isEmpty(item.images[1]) &&
                    isEmpty(item.images[2]) &&
                    isEmpty(item.images[3])
                      ? "8px"
                      : "8px 8px 0px 0px"
                  }
                />
                {roomRecommend && (
                  <Image
                    srcImage={`${prefixUrlIcon}${listIcons.IconRecommendRoom}`}
                    className={classes.iconRecommendImg}
                  />
                )}
              </Box>
              {!isEmpty(item.images) && (
                <Box className={classes.wrapImageSmall}>
                  {!isEmpty(item.images[1]) && (
                    <Box
                      width="100%"
                      height="100%"
                      onMouseMove={() => setActiveImage(item.images[1])}
                    >
                      <Image
                        srcImage={item.images[1]}
                        className={classes.imgSmall}
                        style={{
                          borderRadius: isEmpty(item.images[2])
                            ? "0px 0px 8px 8px"
                            : "0px 0px 0px 8px",
                        }}
                        borderRadiusProp={
                          isEmpty(item.images[2])
                            ? "0px 0px 8px 8px"
                            : "0px 0px 0px 8px"
                        }
                      />
                    </Box>
                  )}
                  {!isEmpty(item.images[2]) && (
                    <Box
                      width="100%"
                      height="100%"
                      onMouseMove={() => setActiveImage(item.images[2])}
                    >
                      <Image
                        srcImage={item.images[2]}
                        className={clsx(
                          classes.imgSmall,
                          classes.imgSmallCenter
                        )}
                        style={{
                          borderRadius: isEmpty(item.images[3])
                            ? "0px 0px 8px 0px"
                            : 0,
                          paddingRight: isEmpty(item.images[3]) ? 0 : 2,
                        }}
                        borderRadiusProp={
                          isEmpty(item.images[3]) ? "0px 0px 8px 0px" : 0
                        }
                      />
                    </Box>
                  )}
                  {!isEmpty(item.images[3]) && (
                    <Box
                      width="100%"
                      height="100%"
                      onMouseMove={() => setActiveImage(item.images[3])}
                    >
                      <Image
                        srcImage={item.images[3]}
                        className={classes.imgSmall}
                        style={{ borderRadius: "0px 0px 8px 0px" }}
                        borderRadiusProp="0px 0px 8px 0px"
                      />
                    </Box>
                  )}
                </Box>
              )}
            </Box>
            <Box className={classes.loadViewImages} onClick={openImageRoom}>
              {listString.IDS_MT_TEXT_VIEW_IMAGE_CONVIENT}
            </Box>
          </Box>

          <Box className={classes.wrapBoxLeft}>
            {dataList.length > 1 && (
              <>
                <Box className={classes.wrapRoomName}>{item.name}</Box>
                <Box className={classes.infoRoom}>
                  {!isEmpty(item.roomArea) && (
                    <>
                      <Box style={{ width: 16, height: 16 }}>
                        <IconAcreage />
                      </Box>
                      <Box
                        style={{
                          fontSize: 14,
                          lineHeight: "16px",
                          fontWeight: 400,
                          marginRight: 24,
                          marginLeft: 8,
                        }}
                      >
                        {item.roomArea}m2
                      </Box>
                    </>
                  )}
                  {!isEmpty(item.views) && (
                    <>
                      <Box style={{ width: 16, height: 16 }}>
                        <IconView />
                      </Box>
                      <Box
                        marginLeft={1}
                        style={{
                          fontSize: "14px",
                          lineHeight: "16px",
                          fontWeight: 400,
                        }}
                      >
                        {getView(item.views)}
                      </Box>
                    </>
                  )}
                </Box>
              </>
            )}
            <TableContainer style={{ height: "100%", overflow: "unset" }}>
              <Table className={classes.table} aria-label="simple table">
                {!isEmpty(dataList) &&
                  dataList.map((el, i) => {
                    const isFirst = dataList.length === 1;
                    return (
                      <TableBody key={i}>
                        <TableRow style={{ marginBottom: 16 }}>
                          <TableCell
                            className={getTableStyle(
                              classes.verticalItemFirst,
                              i,
                              isFirst
                            )}
                          >
                            <ItemRatesRoom
                              rateInfo={el}
                              roomInfo={item}
                              showRoomName={dataList.length > 1}
                              index={i}
                              showNumRooms={false}
                              isFirst={isFirst}
                              showBuySignal
                              showPolicies
                              showSpecialDeal
                              showNumerAllotment
                            />
                          </TableCell>

                          <TableCell
                            className={getTableStyle(
                              classes.verticalItem,
                              i,
                              isFirst
                            )}
                            style={{
                              height: isFirst ? 300 : "auto",
                              width: "98px",
                            }}
                          >
                            <Box className={classes.wrapGuest}>
                              <IconUserOrther />
                              {`x${item.standardNumberOfGuests} người`}
                            </Box>
                          </TableCell>

                          <TableCell
                            className={getTableStyle(
                              classes.verticalItem,
                              i,
                              isFirst
                            )}
                            style={{
                              height: isFirst ? 300 : "auto",
                              width: "170px",
                            }}
                          >
                            {!isEmpty(el) &&
                              getListBed(el).map((elm, idx) => (
                                <>
                                  <Box
                                    className={classes.itemBed}
                                    key={idx}
                                    display="flex"
                                    flexDirection="column-reverse"
                                    style={{
                                      margin: "0 atuo",
                                      alignItems: "center",
                                      padding: "24px 37px 0 27px",
                                    }}
                                  >
                                    <Box component="span">{elm.bedInfo}</Box>
                                    <Box display="flex" alignItems="center">
                                      {elm.numberBed > 2 ? (
                                        <Image
                                          srcImage={`${prefixUrlIcon}${
                                            elm.bedTypes.includes("twin")
                                              ? listIcons.Icon3SingleBed
                                              : listIcons.IconDoubleSingleBed
                                          }`}
                                          className={
                                            classes.iconDoubleSingleBed
                                          }
                                        />
                                      ) : (
                                        elm.bedTypes.map((it, index) => (
                                          <Box
                                            key={index.toString()}
                                            display="flex"
                                            pr={2 / 8}
                                          >
                                            {it
                                              .toLowerCase()
                                              .includes("twin") ? (
                                              <IconBedSingle
                                                className={`svgFillAll ${classes.iconBedDouble}`}
                                              />
                                            ) : (
                                              <IconBedDouble
                                                className={`svgFillAll ${classes.iconBedDouble}`}
                                              />
                                            )}
                                          </Box>
                                        ))
                                      )}
                                    </Box>
                                  </Box>
                                  {idx !== getListBed(el).length - 1 && (
                                    <Box
                                      color="gray.grayDark7"
                                      fontSize={14}
                                      lineHeight="17px"
                                      pt={3}
                                    >
                                      {" "}
                                      --- hoặc ---
                                    </Box>
                                  )}
                                </>
                              ))}
                          </TableCell>
                          <TableCell
                            className={getTableStyle(
                              classes.verticalItem,
                              i,
                              isFirst
                            )}
                            style={{
                              height: isFirst ? 300 : "auto",
                              width: "250px",
                              position: "relative",
                              borderRight: "none",
                            }}
                          >
                            <Box className={classes.wrapRoomAndPrice}>
                              {!isEmpty(el.basePromotionInfo) &&
                                !el.hiddenPrice && (
                                  <>
                                    <Box className={classes.discountPercent}>
                                      <Box className={classes.discountText}>
                                        {
                                          el.basePromotionInfo
                                            .discountPercentage
                                        }
                                        %
                                      </Box>
                                      <Box
                                        className={classes.triangleBottomright}
                                      />
                                    </Box>
                                    <Box
                                      component="span"
                                      className={classes.subPrice}
                                    >
                                      {el.basePromotionInfo.priceBeforePromotion.formatMoney()}{" "}
                                      /đêm
                                    </Box>
                                  </>
                                )}
                              {!isEmpty(el.basePrice) &&
                                el.basePrice > 0 &&
                                !el.hiddenPrice && (
                                  <Box
                                    component="span"
                                    className={classes.mainPrice}
                                  >
                                    {`${el.basePrice.formatMoney()}đ`}
                                    <Box
                                      component="span"
                                      color="gray.grayDark8"
                                      fontWeight="normal"
                                      style={{
                                        fontSize: "14px",
                                        lineHeight: "16px",
                                        fontWeight: 400,
                                      }}
                                    >
                                      {" "}
                                      /đêm
                                    </Box>
                                  </Box>
                                )}

                              {!isEmpty(el.promotions) && !el.hiddenPrice && (
                                <Box className={classes.wrapPromo}>
                                  <Box
                                    display="flex"
                                    flexDirection="column"
                                    style={{ alignItems: "center" }}
                                  >
                                    <Typography
                                      style={{
                                        marginTop: "5px",
                                        fontWeight: 400,
                                        display: "flex",
                                        marginLeft: "12px",
                                        fontSize: "14px",
                                        alignItems: "center",
                                        whiteSpace: "nowrap",
                                      }}
                                    >
                                      {listString.IDS_MT_TEXT_TYPE_PROMO_CODE}
                                      :&nbsp;
                                      <span className={classes.wrapCodeText}>
                                        {!isEmpty(el.promotions[0]) &&
                                          !isEmpty(
                                            el.promotions[0].basePromotion
                                          ) &&
                                          el.promotions[0].code}
                                      </span>
                                      <span
                                        className={classes.wrapCodeDiscount}
                                      >
                                        {`-${
                                          !isEmpty(el.promotions[0]) &&
                                          !isEmpty(
                                            el.promotions[0].basePromotion
                                          ) &&
                                          el.promotions[0].basePromotion
                                            .valueType === "percent"
                                            ? `${el.promotions[0].basePromotion.percent}%`
                                            : `${el.promotions[0].basePromotion.value.formatMoney()}đ`
                                        }`}
                                      </span>
                                    </Typography>

                                    <Typography
                                      variant="body2"
                                      color="secondary"
                                      className={classes.wrapPriceCode}
                                    >
                                      {`${!isEmpty(el.promotions[0]) &&
                                        !isEmpty(
                                          el.promotions[0].basePromotion
                                        ) &&
                                        el.promotions[0].basePromotion.price.formatMoney()}đ`}
                                      <Box
                                        component="span"
                                        className={classes.night}
                                      >
                                        /đêm
                                      </Box>
                                    </Typography>
                                  </Box>
                                </Box>
                              )}

                              {item.missAllotment && (
                                <Box className={classes.wrapMissAllotment}>
                                  {listString.IDS_MT_TEXT_YOU_MISS_ALLOTMENT}
                                </Box>
                              )}
                              {!el.hiddenPrice && (
                                <ButtonComponent
                                  style={{ margin: "12px 0 0 0" }}
                                  backgroundColor={theme.palette.secondary.main}
                                  height={44}
                                  width={156}
                                  fontSize={16}
                                  fontWeight={600}
                                  borderRadius={8}
                                  handleClick={() => handleBook(el)}
                                >
                                  {listString.IDS_MT_TEXT_BOOKING}
                                </ButtonComponent>
                              )}
                              {el.hiddenPrice && (
                                <Typography
                                  variant="subtitle1"
                                  style={{ fontWeight: 400 }}
                                >
                                  {listString.IDS_MT_TEXT_LOGIN_USE_PROMO}
                                  <Typography
                                    variant="subtitle1"
                                    color="secondary"
                                    style={{ marginLeft: 5 }}
                                    component="span"
                                  >
                                    {`Giảm ${
                                      !isEmpty(el.basePromotionInfo)
                                        ? getMemberDeal(
                                            el.basePromotionInfo,
                                            el.basePrice
                                          )
                                        : "10%"
                                    }`}
                                  </Typography>
                                  <Box className={classes.discountMember}>
                                    {listString.IDS_MT_TEXT_DISCOUNT_ON_MEMBER}
                                  </Box>
                                  <AuthWrapper typeModal="MODAL_LOGIN">
                                    <Box
                                      display="flex"
                                      justifyContent="flex-end"
                                    >
                                      <ButtonComponent
                                        style={{
                                          margin: "12px 0 0 0",
                                        }}
                                        backgroundColor={
                                          theme.palette.secondary.main
                                        }
                                        height={44}
                                        width={156}
                                        fontSize={16}
                                        fontWeight={600}
                                        borderRadius={8}
                                      >
                                        {listString.IDS_MT_TEXT_LOGIN}
                                      </ButtonComponent>
                                    </Box>
                                  </AuthWrapper>
                                </Typography>
                              )}
                              {!el.hiddenPrice && (
                                <Box className={classes.detailsPrice}>
                                  {listString.IDS_MT_TEXT_PRICE_DETAIL}

                                  <Box className={classes.toolTipPrice}>
                                    <Box className={classes.triangleTopight}>
                                      <IconArrowDownToggle
                                        className={`svgFillAll ${classes.iconTriage}`}
                                      />
                                    </Box>
                                    <Box style={{ textAlign: "left" }}>
                                      <Box className={classes.itemText}>
                                        1 phòng x 1 đêm
                                      </Box>
                                      {getDiscount(el) > 0 && (
                                        <Box
                                          className={classes.itemText}
                                          color="#48BB78"
                                        >
                                          Chúng tôi khớp giá, giảm thêm
                                        </Box>
                                      )}
                                      {totalSurcharge(el) > 0 && (
                                        <Box className={classes.itemText}>
                                          Phụ phí
                                        </Box>
                                      )}
                                      {vatPrice(el) > 2 && (
                                        <Box className={classes.itemText}>
                                          {`${
                                            el.includedVAT
                                              ? "Thuế và phí "
                                              : "Phí "
                                          }dịch vụ khách sạn`}
                                        </Box>
                                      )}

                                      <Box
                                        className={classes.itemText}
                                        style={{
                                          fontWeight: 600,
                                          paddingBottom: 12,
                                        }}
                                      >
                                        Tổng giá
                                      </Box>
                                    </Box>

                                    <Box style={{ textAlign: "right" }}>
                                      <Box className={classes.itemPrice}>
                                        <Box component="span">{`${nightPrice(
                                          el
                                        ).formatMoney()}đ`}</Box>
                                      </Box>

                                      {getDiscount(el) > 0 && (
                                        <Box
                                          className={classes.itemPrice}
                                          color="#48BB78"
                                        >
                                          <Box component="span">
                                            -
                                            {`${getDiscount(
                                              el
                                            ).formatMoney()}đ`}
                                          </Box>
                                        </Box>
                                      )}

                                      {totalSurcharge(el) > 0 && (
                                        <Box className={classes.itemPrice}>
                                          {`${totalSurcharge(
                                            el
                                          ).formatMoney()}đ`}
                                        </Box>
                                      )}
                                      {vatPrice(el) > 2 && (
                                        <Box className={classes.itemPrice}>
                                          {`${vatPrice(el).formatMoney()}đ`}
                                        </Box>
                                      )}

                                      <Box
                                        className={classes.itemPrice}
                                        style={{ paddingBottom: 12 }}
                                      >
                                        {`${totalPrice(el).formatMoney()}đ`}
                                      </Box>
                                    </Box>
                                  </Box>
                                </Box>
                              )}
                            </Box>
                          </TableCell>
                        </TableRow>
                      </TableBody>
                    );
                  })}
              </Table>
            </TableContainer>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default ItemHotelDetail;
