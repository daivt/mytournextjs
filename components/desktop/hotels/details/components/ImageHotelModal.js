import {
  Box,
  makeStyles,
  useTheme,
  Dialog,
  Slide,
  AppBar,
  Tabs,
  Tab,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { isEmpty } from "@utils/helpers";
import React, { useRef } from "react";
import Slider from "react-slick";
import { IconArrowDownSmall, IconLoadingHotel } from "@public/icons";
import CloseIcon from "@material-ui/icons/Close";

const useStyles = makeStyles((theme) => ({
  dialogContent: {},
  leftContent: {
    width: "100%",
    borderRadius: "8px 0px 0px 8px",
    padding: 24,
    position: "relative",
    "& .slick-dots": {
      bottom: 0,
      position: "initial",
      display: "flex !important",
      margin: "8px -4px 0 -4px",
      overflow: "auto",
      minHeight: 142,
      "& li": {
        width: 96,
        height: 72,
        borderRadius: 8,
        marginBottom: 8,
        marginTop: 52,
      },
      "& .slick-active": {
        "& img": {
          border: "2px solid #FF1284",
        },
      },
    },
  },
  iconArrow: {
    stroke: theme.palette.black.black3,
  },
  wrapArrow: {
    "&:hover": {
      "& svg": {
        stroke: "#FF1284",
        transition: "all ease .2s",
      },
    },
  },
  appBar: {
    position: "relative",
    borderRadius: 0,
    background: "white",
    color: "#1A202C",
    boxShadow: "none",
    display: "flex",
    alignItems: "flex-end",
    padding: 10,
  },
  tabContainer: {
    position: "absolute",
    bottom: 130,
    display: "flex",
    width: "calc(100% - 48px)",
  },
  tabItem: {},
  imageHolder: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 96,
    height: 72,
    borderRadius: 8,
    backgroundColor: theme.palette.gray.grayLight22,
    opacity: 1,
    transition: "opacity 500ms",
  },
}));
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
function SamplePrevArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, left: 0 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowDownSmall
        className={`svgFillAll ${classes.iconArrow}`}
        style={{ transform: "rotate(90deg)" }}
      />
    </div>
  );
}

function SampleNextArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{ ...customStyleArrow, right: -38 }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowDownSmall
        className={`svgFillAll ${classes.iconArrow}`}
        style={{ transform: "rotate(-90deg)" }}
      />
    </div>
  );
}
const customStyle = {
  background: "white",
  height: 40,
  width: 40,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 2,
  borderRadius: "100px",
  boxShadow: "0px 8px 12px rgb(0 0 0 / 10%)",
  cursor: "pointer",
  position: "absolute",
  top: "calc(50% - 72px)",
  transform: "translate(-50%, -50%)",
};
const AntTabs = withStyles({
  root: {
    borderBottom: "2px solid #EDF2F7",
    width: "100%",
  },
  indicator: { backgroundColor: "#00B6F3" },
})(Tabs);
const AntTab = withStyles((theme) => ({
  root: {
    textTransform: "none",
    minWidth: 72,
    fontWeight: 600,
    marginRight: 24,
    "&:hover": {
      color: "#00B6F3",
      opacity: 1,
    },
    "&$selected": { color: "#00B6F3" },
    "&:focus": { color: "#00B6F3" },
  },
  selected: {},
}))((props) => <Tab disableRipple {...props} />);

const defaultBG =
  "https://test.mytourcdn.com/120x90,q90/themes/images/pc-listing-default.png";
const ImageHotelModal = ({ hotelDetail = {}, handleClose }) => {
  const classes = useStyles();
  const theme = useTheme();
  const slideEl = useRef();
  const [value, setValue] = React.useState(0);
  const [dataImages, setDataImages] = React.useState(hotelDetail.images || []);
  const groupImage = () => {
    let result = {};
    if (!isEmpty(hotelDetail.images)) {
      hotelDetail.images.forEach((el) => {
        if (el.photoType) {
          if (isEmpty(result[el.photoType.type])) {
            result = {
              ...result,
              [el.photoType.type]: { title: el.photoType.name, data: [el] },
            };
          } else {
            result[el.photoType.type].data.push(el);
          }
        }
      });
    }
    return result;
  };
  const groupData = groupImage();
  const handleChange = (newValue, data) => {
    if (slideEl.current) slideEl.current.slickGoTo(0);
    setDataImages(data);
    setValue(newValue);
  };
  const settings = {
    customPaging: (index) => {
      const imageItem = hotelDetail.images[index] || {};
      if (index >= dataImages.length) {
        return <img src={defaultBG} alt="" style={{ opacity: 0 }} />;
      }
      if (isEmpty(imageItem.image)) {
        return (
          <div className={classes.imageHolder}>
            <IconLoadingHotel />
          </div>
        );
      }
      return (
        <img
          src={
            !isEmpty(imageItem.image)
              ? hotelDetail.images[index].image.small
              : defaultBG
          }
          alt=""
          id={imageItem.id || `default_${index}`}
          style={{ width: 96, height: 72, borderRadius: 8 }}
        />
      );
    },
    dots: true,
    lazyLoad: true,
    dotsClass: "slick-dots slick-thumb",
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    ref: slideEl,
    nextArrow: (
      <SampleNextArrow
        customStyleArrow={{
          ...customStyle,
          display: dataImages.length > 1 ? "flex" : "none",
        }}
      />
    ),
    prevArrow: (
      <SamplePrevArrow
        customStyleArrow={{
          ...customStyle,
          display: dataImages.length > 1 ? "flex" : "none",
        }}
      />
    ),
    beforeChange: (current, next) => {
      const imageItem = dataImages[next] || {};
      const activeEl = document.getElementById(
        imageItem.id || `default_${next}`
      );
      const containerEl = document.getElementsByClassName("slick-dots");
      if (activeEl && containerEl) {
        if (
          activeEl.getBoundingClientRect().right >
          containerEl[0].getBoundingClientRect().right
        ) {
          containerEl[0].scrollTo({ left: next * 100, behavior: "smooth" });
        }
        if (
          activeEl.getBoundingClientRect().left <
          containerEl[0].getBoundingClientRect().left
        ) {
          containerEl[0].scrollTo({ left: next * 100, behavior: "smooth" });
        }
      }
    },
  };
  return (
    <Dialog
      maxWidth="sm"
      fullWidth
      open
      onClose={handleClose}
      TransitionComponent={Transition}
    >
      <AppBar className={classes.appBar}>
        <CloseIcon
          style={{ fontSize: 24, cursor: "pointer" }}
          onClick={handleClose}
        />
      </AppBar>
      <Box className={classes.dialogContent}>
        <Box className={classes.leftContent}>
          {!isEmpty(dataImages) && (
            <>
              <Slider {...settings}>
                {dataImages.map((el, i) => (
                  <div key={i}>
                    <img
                      src={!isEmpty(el.image) ? el.image.large : defaultBG}
                      alt=""
                      style={{
                        width: 720,
                        height: 478,
                        borderRadius: 8,
                        objectFit: "cover",
                      }}
                    />
                  </div>
                ))}
                {dataImages.length === 1 && (
                  <img src={defaultBG} alt="" style={{ opacity: 0 }} />
                )}
              </Slider>
              <Box className={classes.tabContainer}>
                <AntTabs value={value}>
                  <AntTab
                    label={`Tất cả (${hotelDetail.images.length})`}
                    onClick={() => handleChange(0, hotelDetail.images)}
                  />
                  {Object.keys(groupData).map((el, i) => {
                    return (
                      <AntTab
                        key={el}
                        label={`${groupData[el].title} (${groupData[el].data.length})`}
                        onClick={() => handleChange(i + 1, groupData[el].data)}
                      />
                    );
                  })}
                </AntTabs>
              </Box>
            </>
          )}
        </Box>
      </Box>
    </Dialog>
  );
};
export default ImageHotelModal;
