import { getHotelReviews, getTripAdvisorReviews } from "@api/hotels";
import { Box, Grid, makeStyles, Typography } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import {
  IconMytourReview,
  IconPencilReview,
  IconSuitCase,
  IconExternalLink,
} from "@public/icons";
import Image from "@src/image/Image";
import Link from "@src/link/Link";
import UsePagination from "@src/pagination/Pagination";
import { listIcons, listString, prefixUrlIcon } from "@utils/constants";
import { getFirstName, getMessageHotelReviews, isEmpty } from "@utils/helpers";
import clsx from "clsx";
import moment from "moment";
import { useEffect, useState } from "react";
import ReviewPointHotel from "./ReviewPointHotel";

const useStyles = makeStyles((theme) => ({
  wrapContent: {
    display: "flex",
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
    marginTop: 24,
    paddingBottom: 24,
  },
  nonBorder: {
    borderBottom: "none",
  },
  boxLeft: {
    display: "flex",
  },
  headerContainer: {
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    flexDirection: "column",
    margin: "0 auto",
    position: "relative",
  },
  headerContent: {
    display: "flex",
    justifyContent: "space-between  ",
    color: "#1A202C",
    marginTop: 25,
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
  },
  divider: {
    height: 2,
    background: "#00B6F3",
    margin: "auto",
    marginTop: 12,
  },
  textItem: { marginRight: 32, cursor: "pointer" },
  textDetails: {
    border: `1px solid ${theme.palette.gray.grayLight24}`,
    background: theme.palette.white.main,
    color: theme.palette.black.black3,
    padding: "6px 12px",
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    width: "fit-content",
    borderRadius: 8,
    height: "fit-content",
    marginRight: 12,
    marginBottom: 8,
    cursor: "pointer",
  },
  textDetailsActive: {
    background: theme.palette.blue.blueLight8,
    color: theme.palette.white.main,
  },

  textNoMargin: {
    marginRight: 0,
  },
  avatar: {
    display: "flex",
    color: theme.palette.primary.main,
    background: theme.palette.blue.blueLight9,
    fontSize: 24,
    lineHeight: "28px",
    fontWeight: 600,
    width: 64,
    height: 64,
  },
  dateReview: {
    marginLeft: 12,
  },
  boxRightReview: {
    display: "flex",
    alignItems: "center",
    marginTop: 8,
  },
  textReview: {
    fontSize: 12,
    fontWeight: 400,
    lineHeight: "14px",
    color: theme.palette.gray.grayDark8,
  },
  boxRight: {
    display: "flex",
    flexDirection: "column",
    // marginLeft: 64,
  },
  description: {
    fontSize: 14,
    lineHeight: "24px",
    fontWeight: 400,
    display: "-webkit-box",
    WebkitLineClamp: 3,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
    marginTop: 8,
  },
  pointReview: {
    borderRadius: 8,
    background: theme.palette.blue.blueLight13,
    color: theme.palette.blue.blueLight8,
    fontSize: 24,
    lineHeight: "28px",
    fontWeight: 600,
    textAlign: "center",
    marginLeft: 30,
    width: 88,
  },
  point: {
    padding: "9px 0 0 0",
    display: "flex",
    alignItems: "center",
    width: "fit-content",
    margin: "0 auto",
  },
  headerBoxRight: {
    display: "flex",
  },
  messageReview: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    color: theme.palette.black.black3,
    whiteSpace: "nowrap",
    paddingBottom: 9,
  },
  paginationContainer: {
    width: "100%",
    display: "flex",
    justifyContent: "flex-end",
    margin: "24px 0",
  },
  loadingMore: {
    position: "absolute",
    zIndex: 3,
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(255,255,255,0.7)",
  },
  imgLoading: {
    zIndex: 4,
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
  iconTripadvisor: {
    width: 18,
    height: 18,
    marginRight: 5,
  },
}));
const menu = [
  {
    name: listString.IDS_MT_REVIEW_HOTEL_BY_MYTOUR,
    alias: "mytour",
    icon: <IconMytourReview />,
  },
  {
    name: listString.IDS_MT_REVIEW_HOTEL_BY_TRIPADVISOR,
    alias: "tripadvisor",
    icon: (
      <img
        src={`${prefixUrlIcon}${listIcons.IconTripAdvisorReview}`}
        style={{ width: 24, height: 24 }}
        alt="icon_tripadvisor_review"
      />
    ),
  },
];
let pageActive = 1;
let sortBy = "";
let type = "mytour";
let filterActive = null;
const ReviewCommentHotel = ({
  hotelDetail = {},
  reviews = {},
  reviewsTripadvisor = {},
  fetchReviews,
  fetchReviewTripadvisor,
  loading,
}) => {
  const classes = useStyles();

  const handleFetchData = () => {
    let params = { page: pageActive, sortBy };
    if (!isEmpty(filterActive)) {
      params = { ...params, filters: { travelTypes: [filterActive] } };
    }
    if (type === "mytour") fetchReviews(params);
    else fetchReviewTripadvisor(params);
  };
  const handleSort = (sort) => {
    sortBy = sortBy === sort ? "" : sort;
    pageActive = 1;
    handleFetchData();
  };
  const handelFilterRating = (value) => {
    filterActive = filterActive === value ? null : value;
    pageActive = 1;
    handleFetchData();
  };

  const handleChangePage = (page) => () => {
    pageActive = page;
    handleFetchData();
  };
  useEffect(() => {
    if (isEmpty(sortBy) && !isEmpty(reviews.sortOptions)) {
      sortBy = reviews.sortOptions[0].code;
      handleFetchData();
    } // eslint-disable-next-line
  }, [reviews]);
  useEffect(() => {
    return () => {
      pageActive = 1;
      sortBy = "";
      type = "mytour";
      filterActive = null;
    };
  }, []);
  return (
    <>
      {!isEmpty(reviews) && (
        <>
          <ReviewPointHotel
            item={reviews}
            handelFilterRating={handelFilterRating}
            filterActive={filterActive}
            hotelDetail={hotelDetail}
          />
          <Box className={classes.wrapHeader}>
            <Box className={classes.headerContainer}>
              <Box className={classes.headerContent}>
                <Box display="flex">
                  {menu.map((el, i) => (
                    <Box
                      className={classes.textItem}
                      key={i}
                      onClick={() => {
                        sortBy =
                          el.alias === "mytour"
                            ? reviews.sortOptions[0].code
                            : reviewsTripadvisor.sortOptions[0].code;
                        pageActive = 1;
                        type = el.alias;
                        filterActive = null;
                        handleFetchData();
                      }}
                    >
                      <Box
                        display="flex"
                        style={{
                          color: type === el.alias ? "#00B6F3" : "#1A202C",
                          alginItems: "center",
                        }}
                      >
                        <Box style={{ marginRight: "8px" }}>{el.icon}</Box>
                        {el.name}
                      </Box>
                      {type === el.alias && <div className={classes.divider} />}
                    </Box>
                  ))}
                </Box>
                {type === "mytour" ? (
                  <Box display="flex">
                    {!isEmpty(reviews.items) &&
                      !isEmpty(reviews.sortOptions) &&
                      reviews.sortOptions.map((el, index) => (
                        <Box className={classes.wrapListSort} key={index}>
                          <Box
                            className={clsx(
                              classes.textDetails,
                              sortBy === el.code && classes.textDetailsActive,
                              index === reviews.sortOptions.length - 1 &&
                                classes.textNoMargin
                            )}
                            onClick={() => handleSort(el.code)}
                          >
                            {el.textDetail}
                          </Box>
                        </Box>
                      ))}
                  </Box>
                ) : (
                  <Box display="flex">
                    {!isEmpty(reviews.items) &&
                      !isEmpty(reviewsTripadvisor.sortOptions) &&
                      reviewsTripadvisor.sortOptions.map((el, index) => (
                        <Box className={classes.wrapListSort} key={index}>
                          <Box
                            className={clsx(
                              classes.textDetails,
                              sortBy === el.code && classes.textDetailsActive,
                              index ===
                                reviewsTripadvisor.sortOptions.length - 1 &&
                                classes.textNoMargin
                            )}
                            onClick={() => handleSort(el.code)}
                          >
                            {el.textDetail}
                          </Box>
                        </Box>
                      ))}
                  </Box>
                )}
              </Box>
            </Box>
          </Box>

          {type === "mytour" ? (
            <Box style={{ position: "relative" }}>
              {loading && (
                <>
                  <Box className={classes.loadingMore} />
                  <img
                    src="https://staticproxy.mytourcdn.com/0x0/themes/images/mloading.gif"
                    className={classes.imgLoading}
                    alt=""
                  />
                </>
              )}
              {!isEmpty(reviews.items) &&
                reviews.items.map((e, i) => {
                  if (i > 4) return null;
                  return (
                    <Grid container className={classes.wrapContent} key={i}>
                      <Grid item sm={3} md={3} lg={3}>
                        <Box className={classes.boxLeft}>
                          <Avatar className={classes.avatar}>
                            {getFirstName(e.username)}
                          </Avatar>

                          <Box className={classes.dateReview}>
                            <Typography
                              variant="subtitle1"
                              className={classes.userName}
                            >
                              {!isEmpty(e.username)
                                ? e.username
                                : "Khách lưu trú"}
                            </Typography>
                            <Box
                              className={classes.dateReiew}
                              display="flex"
                              flexDirection="column"
                            >
                              <Box className={classes.boxRightReview}>
                                <IconPencilReview
                                  style={{ marginRight: "8px" }}
                                />
                                <Box className={classes.textReview}>
                                  {moment(e.publishedDate).format("DD/MM/YYYY")}
                                </Box>
                              </Box>
                              <Box className={classes.boxRightReview}>
                                <IconSuitCase style={{ marginRight: "8px" }} />
                                <Box className={classes.textReview}>
                                  {e.travelType}
                                </Box>
                              </Box>
                            </Box>
                          </Box>
                        </Box>
                      </Grid>
                      <Grid
                        item
                        sm={9}
                        md={9}
                        lg={9}
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <Box className={classes.boxRight}>
                          <Typography variant="subtitle1">{e.title}</Typography>

                          {!isEmpty(e.content) && (
                            <Box className={classes.description}>
                              <div
                                // eslint-disable-next-line react/no-danger
                                dangerouslySetInnerHTML={{ __html: e.content }}
                              />
                            </Box>
                          )}

                          <Box className={classes.images}>
                            {!isEmpty(e.images) &&
                              e.images.map((elm, idx) => (
                                <img
                                  key={idx}
                                  src={e}
                                  style={{
                                    width: 80,
                                    height: 80,
                                    objectFit: "cover",
                                    borderRadius: 8,
                                  }}
                                  alt=""
                                />
                              ))}
                          </Box>
                        </Box>
                        <Box className={classes.wrapPoint}>
                          {!isEmpty(e.rating) && (
                            <Box className={classes.pointReview}>
                              <Box className={classes.point}>
                                {2 * e.rating === 10
                                  ? 10
                                  : (2 * e.rating).toFixed(1)}
                              </Box>

                              <Box className={classes.messageReview}>
                                {getMessageHotelReviews(e.rating)}
                              </Box>
                            </Box>
                          )}
                        </Box>
                      </Grid>
                    </Grid>
                  );
                })}
            </Box>
          ) : (
            <Box style={{ position: "relative" }}>
              {loading === true && (
                <>
                  <Box className={classes.loadingMore} />
                  <img
                    src="https://staticproxy.mytourcdn.com/0x0/themes/images/mloading.gif"
                    className={classes.imgLoading}
                    alt=""
                  />
                </>
              )}
              {!isEmpty(reviewsTripadvisor.items) &&
                reviewsTripadvisor.items.map((e, i) => {
                  return (
                    <Grid container className={classes.wrapContent} key={i}>
                      <Grid item sm={3} md={3} lg={3}>
                        <Box className={classes.boxLeft}>
                          {!isEmpty(e.username) && (
                            <Avatar className={classes.avatar}>
                              {e.username.split("")[0]}
                            </Avatar>
                          )}

                          <Box className={classes.dateReview}>
                            <Typography
                              variant="subtitle1"
                              className={classes.userName}
                            >
                              {e.username}
                            </Typography>
                            <Box
                              className={classes.dateReiew}
                              display="flex"
                              flexDirection="column"
                            >
                              <Box className={classes.boxRightReview}>
                                <IconPencilReview
                                  style={{ marginRight: "8px" }}
                                />
                                <Box className={classes.textReview}>
                                  {moment(e.publishedDate).format("DD/MM/YYYY")}
                                </Box>
                              </Box>

                              <Box className={classes.boxRightReview}>
                                <IconSuitCase style={{ marginRight: "8px" }} />
                                <Box className={classes.textReview}>
                                  {e.travelType}
                                </Box>
                              </Box>
                            </Box>
                          </Box>
                        </Box>
                      </Grid>
                      <Grid
                        item
                        sm={9}
                        md={9}
                        lg={9}
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <Box className={classes.boxRight}>
                          <Typography variant="subtitle1">{e.title}</Typography>

                          {!isEmpty(e.content) && (
                            <Box className={classes.description}>
                              {e.content}
                            </Box>
                          )}

                          <Box className={classes.images}>
                            {!isEmpty(e.images) &&
                              e.images.map((elm, idx) => (
                                <img
                                  key={idx}
                                  src={e.images}
                                  style={{
                                    width: 80,
                                    height: 80,
                                    objectFit: "cover",
                                    borderRadius: 8,
                                  }}
                                  alt=""
                                />
                              ))}
                          </Box>
                        </Box>
                        <Box className={classes.wrapPoint}>
                          {!isEmpty(e.rating) && (
                            <Box className={classes.pointReview}>
                              <Box className={classes.point}>
                                <Image
                                  srcImage={`${prefixUrlIcon}${listIcons.IconTripadvisor}`}
                                  className={classes.iconTripadvisor}
                                />
                                {2 * e.rating === 10
                                  ? 10
                                  : (2 * e.rating).toFixed(1)}
                              </Box>
                              <Box className={classes.messageReview}>
                                {getMessageHotelReviews(e.rating)}
                              </Box>
                            </Box>
                          )}
                        </Box>
                      </Grid>
                    </Grid>
                  );
                })}
            </Box>
          )}
          <Box className={classes.paginationContainer}>
            {type === "mytour" ? (
              <UsePagination
                pageSize={5}
                total={
                  type === "mytour" ? reviews.total : reviewsTripadvisor.total
                }
                pageNum={pageActive}
                className={classes.paginationContainer}
                handleChangePage={handleChangePage}
              />
            ) : (
              <>
                {!isEmpty(reviewsTripadvisor.reviewUrl) && (
                  <Link
                    href={reviewsTripadvisor.reviewUrl}
                    target="_blank"
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      width: "100%",
                      fontSize: 14,
                      lineHeight: "17px",
                      color: "#00B6F3",
                    }}
                  >
                    {listString.IDS_MT_VIEW_MORE_REVIEW_TRIPADVISOR}&nbsp;
                    <IconExternalLink />
                  </Link>
                )}
              </>
            )}
          </Box>
        </>
      )}
    </>
  );
};

export default ReviewCommentHotel;
