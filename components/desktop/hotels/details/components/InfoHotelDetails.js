import { addFavorite, removeFavorite } from "@api/user";
import BootstrapTooltip from "@components/common/bootstrapTooltip/BootstrapTooltip";
import Vote from "@components/common/desktop/vote/Vote";
import {
  Box,
  Grid,
  IconButton,
  makeStyles,
  Typography,
  useTheme,
} from "@material-ui/core";
import {
  IconAlbum,
  IconFlash,
  IconHeart,
  IconLocationBorder,
} from "@public/icons";
import ButtonComponent from "@src/button/Button";
import Image from "@src/image/Image";
import {
  LAST_HOTEL_FAVORITES,
  listIcons,
  listString,
  prefixUrlIcon,
  TOKEN,
} from "@utils/constants";
import { getMessageHotelReviews, isEmpty } from "@utils/helpers";
import clsx from "clsx";
import cookie from "js-cookie";
import moment from "moment";
import { useSnackbar } from "notistack";
import { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  wrapIcon: {
    display: "flex",
    alignItems: "center",
  },
  textIcon: {
    fontSize: 12,
    lineHeight: "14px",
    fontWeight: 600,
    color: theme.palette.white.main,
    marginLeft: 6,
    padding: "5px 6px 5px 0",
  },
  iconFlash: {
    fill: theme.palette.white.main,
    margin: "4px 0 4px 4px",
  },
  wrapPrice: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    width: "30%",
  },
  hotelName: {
    fontSize: 24,
    lineHeight: "28px",
    fontWeight: 600,
    color: theme.palette.black.black3,
    display: "flex",
    alignItems: "flex-start",
    justifyContent: "space-between",
    flexDirection: "column",
    width: "70%",
  },
  wrapRatingStar: {
    marginTop: 8,
    display: "flex",
    alignItems: "center",
  },
  wrapPoint: {
    display: "flex",
    alignItems: "center",
    marginTop: 12,
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
  },
  point: {
    background: theme.palette.blue.blueLight8,
    color: theme.palette.white.main,
    padding: "2px 4px",
    fontSize: 14,
    lineHeight: "17px",
    fontWeight: 600,
    borderRadius: 4,
    marginRight: 4,
  },
  messageReview: {
    display: "flex",
    alignItems: "center",
    fontSize: 14,
    lineHeight: "17px",
    fontWeight: 400,
  },
  countReview: {
    color: theme.palette.gray.grayDark7,
    fontSize: 14,
    lineHeight: "17px",
  },
  color: theme.palette.gray.grayDark7,
  loadReview: {
    color: theme.palette.blue.blueLight8,
    "&:hover": {
      cursor: "pointer",
    },
  },
  wrapLocation: {
    display: "flex",
    alignItems: "center",
    marginTop: 10,
  },
  iconLocation: {
    marginRight: 6,
  },
  location: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    color: theme.palette.black.black3,
  },
  viewMap: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    "&:hover": {
      textDecoration: "none",
    },
  },
  mainPrice: {
    fontSize: 20,
    fontWeight: 600,
    lineHeight: "23px",
    marginTop: 3,
    whiteSpace: "nowrap",
  },
  night: {
    fontSize: 14,
    fontWeight: 400,
    lineHeight: "16px",
    marginLeft: 3,
  },
  wrapContent: {
    display: "flex",
    justifyContent: "space-between",
  },
  wrapHotelName: {
    width: "100%",
  },
  subPrice: {
    display: "flex",
  },
  wrapAlbum: {
    marginTop: 13,
    position: "relative",
    cursor: "pointer",
  },
  wrapDiscount: {
    background: theme.palette.pink.main,
    color: theme.palette.white.main,
    borderRadius: 3,
    padding: "2px 4px",
    maxHeight: 19,
  },
  imgLarge: {
    width: "100%",
    height: 362,
    borderRadius: "8px 0px 0px 8px",
    paddingRight: 0,
    position: "relative",
    zIndex: 0,
  },
  wrapIconHeart: {
    position: "absolute",
    top: 16,
    right: 16,
    "&:hover": {
      cursor: "pointer",
    },
  },
  imgSmall: {
    height: 179,
    zIndex: 0,
  },
  wrapLoadMore: {
    position: "absolute",
    top: 0,
    display: "flex",
    alignItems: "center",
    color: theme.palette.white.main,
    fontSize: 18,
    fontWeight: 600,
    lineHeight: "21px",
    width: "calc(100% - 4px)",
    height: "100%",
    background: "rgba(0, 0, 0, 0.5)",
    display: "flex",
    justifyContent: "center",
    borderRadius: "0 0 8px 0",
  },
  itemBannerSmall: {
    position: "relative",
  },
  itemBgBlur: {
    height: 179,
    width: "calc(100% - 4px)",
    borderRadius: "0px 0px 8px 0px",
    top: 0,
    marginLeft: 4,
    cursor: "pointer",
  },
  boxTripadvisor: {
    height: 14,
    width: 1,
    background: theme.palette.gray.grayLight23,
    margin: "0 8px",
  },
  wrapHotelLoved: {
    position: "absolute",
    top: 12,
    right: 12,
  },
  iconButtonFavorite: {
    padding: 0,
  },
  favorite: {
    fill: "#FF1284",
    stroke: theme.palette.white.main,
  },
  notFavorite: {
    fill: "rgba(0, 0, 0, 0.6)",
    stroke: theme.palette.white.main,
  },
  hotelDefault: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%,-50%)",
  },
  backgroundDefault: {
    background: theme.palette.gray.grayLight22,
    zIndex: 0,
  },
  wrapTypeHotel: {
    background: "#FF1284",
    color: theme.palette.white.main,
    display: "flex",
    alignItems: "center",
    fontSize: 11,
    fontWeight: 400,
    lineHeight: "13px",
    borderRadius: 4,
    width: "fit-content",
    borderBottomRightRadius: 0,
  },
  clipPath: {
    width: 15,
    height: 20,
    clipPath: "polygon(105% 0, 48% 0, 100% 100%, 102% 100%)",
    background: "#ffdced",
  },
  villa: {
    height: 20,
    fontSize: 12,
    lineHeight: "14px",
    fontWeight: 600,
    color: "#FF1284",
    padding: "3px 6px",
    marginLeft: 0,
    background: "rgba(255, 18, 132, 0.15)",
    display: "flex",
    alignItems: "center",
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    whiteSpace: "nowrap",
  },
  iconMPlus: {
    width: 63,
    height: 20,
  },
  iconTripadvisor: {
    width: 16,
    height: 16,
    marginRight: 3,
  },
}));
const InfoHotelDetails = ({
  item = {},
  roomItem = {},
  scrollToItem,
  openImageHotel,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const rateItem = !isEmpty(roomItem.rates) ? roomItem.rates[0] : {};
  // const promotionInfo = rateItem.promotionInfo || {};
  const handleBook = () => {
    scrollToItem("rooms_detail");
  };
  const [isFavorite, setIsFavorite] = useState(item.isFavoriteHotel);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  useEffect(() => {
    checkFavoriteHotel();
  }, []);
  const checkFavoriteHotel = () => {
    const listHotelFavorites =
      JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
    if (isEmpty(cookie.get(TOKEN)) && !isEmpty(listHotelFavorites)) {
      setIsFavorite(listHotelFavorites.includes(item.id));
    }
  };
  const handleFavorite = (event) => {
    event.preventDefault();
    if (!isEmpty(cookie.get(TOKEN))) {
      changeFavorite();
    } else {
      let listHotelFavoritesTemp =
        JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
      if (isFavorite) {
        listHotelFavoritesTemp = listHotelFavoritesTemp.filter(
          (el) => el !== item.id
        );
      } else {
        listHotelFavoritesTemp.unshift(item.id);
      }
      localStorage.setItem(
        LAST_HOTEL_FAVORITES,
        JSON.stringify(listHotelFavoritesTemp)
      );
      setIsFavorite(!isFavorite);
    }
    event.stopPropagation();
  };
  const changeFavorite = async () => {
    try {
      if (isFavorite) {
        const { data } = await removeFavorite({ hotelId: item.id });
        if (data.code === 200) {
          setIsFavorite(!isFavorite);
        } else {
          data.message &&
            enqueueSnackbar(
              data.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
        }
      } else {
        const { data } = await addFavorite({ hotelId: item.id });
        if (data.code === 200) {
          setIsFavorite(!isFavorite);
        } else {
          data.message &&
            enqueueSnackbar(
              data.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
        }
      }
    } catch (error) {}
  };
  return (
    <>
      <Box className={classes.wrapBox}>
        <Box className={classes.wrapIcon}>
          {!isEmpty(item.tags) && (
            <Box
              display="flex"
              alignItems="center"
              bgcolor="#FFBC39"
              style={{ borderRadius: 4, marginRight: 12 }}
            >
              <Typography className={classes.textIcon}>
                {item.tags[0].name}
              </Typography>
            </Box>
          )}

          {!isEmpty(item.lastBookedTime) &&
          moment().diff(moment(item.lastBookedTime)) <= 7200000 ? (
            <BootstrapTooltip
              title={listString.IDS_MT_TEXT_HOTEL_TOP_SALING}
              placement="top"
            >
              <Box
                display="flex"
                alignItems="center"
                bgcolor="#E53E3E"
                style={{ borderRadius: 4, height: 24, cursor: "pointer" }}
              >
                <IconFlash
                  className={classes.iconSynchronize}
                  className={`svgFillAll ${classes.iconFlash}`}
                />{" "}
                <Typography className={classes.textIcon}>
                  {listString.IDS_MT_TEXT_SALING_TOP}
                </Typography>
              </Box>
            </BootstrapTooltip>
          ) : (
            <Box
              display="flex"
              alignItems="center"
              bgcolor="#E53E3E"
              style={{ borderRadius: 4, height: 24 }}
            ></Box>
          )}
        </Box>

        <Box className={classes.wrapContent}>
          <Box className={classes.wrapHotelName}>
            <Box display="flex" justifyContent="space-between">
              <Box
                className={classes.hotelName}
                style={{
                  marginTop:
                    !isEmpty(item.tags) ||
                    (!isEmpty(item.lastBookedTime) &&
                      moment().diff(moment(item.lastBookedTime)) <= 7200000)
                      ? 12
                      : 0,
                }}
              >
                {!isEmpty(item.name) && item.name}
                {!isEmpty(item.category) && (
                  <Box display="flex" alignItems="center">
                    <Box className={classes.wrapRatingStar}>
                      <Vote
                        maxValue={item.starNumber}
                        value={item.starNumber}
                      />
                    </Box>
                    <Box
                      display="flex"
                      alignItems="center"
                      style={{ marginLeft: 12, marginTop: 8 }}
                    >
                      {item.category.code === "villa" && (
                        <>
                          <Box className={classes.wrapTypeHotel}>
                            <Box
                              display="flex"
                              alignItems="flex-end"
                              style={{ marginLeft: 6 }}
                            >
                              <Image
                                srcImage={`${prefixUrlIcon}${listIcons.IconMPlus}`}
                                className={classes.iconMPlus}
                              />
                              <Box className={classes.clipPath}></Box>
                            </Box>
                          </Box>
                          <Box className={classes.villa}> Villa, Căn hộ</Box>
                        </>
                      )}
                    </Box>
                  </Box>
                )}
              </Box>
              <Box className={classes.wrapPrice}>
                {!rateItem.hiddenPrice && (
                  <Box
                    display="flex"
                    alignItems="flex-end"
                    flexDirection="column"
                  >
                    {!isEmpty(rateItem.basePromotionInfo) &&
                      !isEmpty(
                        rateItem.basePromotionInfo.priceBeforePromotion
                      ) && (
                        <Box className={classes.subPrice}>
                          <Box
                            style={{
                              fontSize: 14,
                              lineHeight: "17px",
                              textDecoration: "line-through",
                              marginRight: 5,
                              whiteSpace: "nowrap",
                              fontWeight: "normal",
                              color: "gray.grayDark8",
                            }}
                          >
                            {`${rateItem.basePromotionInfo.priceBeforePromotion.formatMoney()}đ`}{" "}
                            /đêm
                          </Box>
                          <Typography
                            component="span"
                            variant="subtitle2"
                            className={classes.wrapDiscount}
                          >{`${rateItem.basePromotionInfo.discountPercentage ||
                            ""}%`}</Typography>
                        </Box>
                      )}
                    {!isEmpty(rateItem.price) && (
                      <Box className={classes.mainPrice}>
                        {`${rateItem.basePrice.formatMoney()}đ`}
                        <Box component="span" className={classes.night}>
                          /đêm
                        </Box>
                      </Box>
                    )}
                  </Box>
                )}
                {!isEmpty(rateItem.basePrice) && (
                  <ButtonComponent
                    style={{ marginLeft: "16px" }}
                    width={156}
                    backgroundColor={theme.palette.secondary.main}
                    height={44}
                    fontSize={16}
                    fontWeight={600}
                    borderRadius={8}
                    handleClick={handleBook}
                  >
                    {listString.IDS_MT_SELECT_ROOM}
                  </ButtonComponent>
                )}
              </Box>
            </Box>
            {!isEmpty(item.rating) &&
              !isEmpty(item.rating.hotel) &&
              item.rating.hotel.rating > 0 && (
                <Box className={classes.wrapPoint}>
                  <Box className={classes.point}>
                    {2 * item.rating.hotel.rating === 10
                      ? 10
                      : (2 * item.rating.hotel.rating).toFixed(1)}
                  </Box>
                  <Box className={classes.messageReview}>
                    {getMessageHotelReviews(item.rating.hotel.rating)}
                    <Box component="span" className={classes.countReview}>
                      &nbsp;
                      {item?.rating?.hotel.count > 0 && (
                        <Typography
                          component="span"
                          className={classes.countReview}
                        >
                          ({item?.rating?.hotel.count} {""}đánh giá)
                        </Typography>
                      )}
                    </Box>
                    &nbsp;
                    {!isEmpty(item.rating) &&
                      !isEmpty(item.rating.tripadvisor) &&
                      item.rating.tripadvisor.rating > 0 && (
                        <>
                          <Box className={classes.boxTripadvisor} />
                          <Image
                            srcImage={`${prefixUrlIcon}${listIcons.IconTripadvisor}`}
                            className={classes.iconTripadvisor}
                          />
                          {2 * item.rating.tripadvisor.rating === 10
                            ? 10
                            : (2 * item.rating.tripadvisor.rating).toFixed(1)}
                        </>
                      )}
                    &nbsp;
                    <Box
                      component="span"
                      className={classes.loadReview}
                      onClick={() => scrollToItem("evaluate")}
                    >
                      {listString.IDS_MT_TEXT_HOTEL_SEE_REVIEW}
                    </Box>
                  </Box>
                </Box>
              )}
            <Box className={classes.wrapLocation}>
              <Box className={classes.iconLocation}>
                <IconLocationBorder />
              </Box>
              <Box className={classes.location}>
                {!isEmpty(item.address) && item.address.address}
              </Box>
              <Box
                component="span"
                className={clsx(classes.loadReview, classes.viewMap)}
                onClick={() => scrollToItem("top_places")}
              >
                &nbsp;{listString.IDS_MT_TEXT_HOTEL_VIEW_MAP}
              </Box>
            </Box>
          </Box>
        </Box>
        <Box className={classes.wrapAlbum} onClick={openImageHotel}>
          <Grid container className={classes.locationDestinationBetween}>
            <Grid item lg={6} md={6} sm={6}>
              {!isEmpty(item.images) && item.images.length >= 1 ? (
                <Box style={{ position: "relative", paddingRight: 4 }}>
                  <Image
                    srcImage={
                      !isEmpty(item.images[0].image)
                        ? item.images[0].image.medium
                        : item.images[0].src
                    }
                    className={classes.imgLarge}
                    borderRadiusProp="8px 0px 0px 8px"
                  />
                  <Box className={classes.wrapHotelLoved}>
                    <IconButton
                      onClick={handleFavorite}
                      className={classes.iconButtonFavorite}
                    >
                      <IconHeart
                        className={`svgFillAll ${
                          isFavorite ? classes.favorite : classes.notFavorite
                        }`}
                      />
                    </IconButton>
                  </Box>
                </Box>
              ) : (
                <Box
                  className={clsx(classes.imgLarge, classes.backgroundDefault)}
                  style={{ position: "relative" }}
                />
              )}
            </Grid>

            <Grid item lg={6} md={6} sm={6}>
              <Grid container>
                <Grid item lg={6} md={6} sm={6}>
                  {!isEmpty(item.images) && item.images.length >= 2 ? (
                    <Image
                      srcImage={
                        !isEmpty(item.images[1].image)
                          ? item.images[1].image.small
                          : item.images[1].src
                      }
                      className={classes.imgSmall}
                    />
                  ) : (
                    <Box
                      className={clsx(
                        classes.imgSmall,
                        classes.backgroundDefault
                      )}
                      style={{ marginLeft: 4 }}
                    />
                  )}
                </Grid>
                <Grid item lg={6} md={6} sm={6}>
                  {!isEmpty(item.images) && item.images.length >= 3 ? (
                    <Image
                      srcImage={
                        !isEmpty(item.images[2].image)
                          ? item.images[2].image.small
                          : item.images[2].src
                      }
                      className={classes.imgSmall}
                      style={{
                        borderRadius: "0px 8px 0px 0px",
                        marginLeft: 4,
                        width: "calc(100% - 4px)",
                      }}
                    />
                  ) : (
                    <Box
                      className={clsx(
                        classes.imgSmall,
                        classes.backgroundDefault
                      )}
                      style={{ marginLeft: 4, borderRadius: "0px 8px 0px 0px" }}
                    />
                  )}
                </Grid>
              </Grid>
              <Grid container style={{ paddingTop: 4 }}>
                <Grid item lg={6} md={6} sm={6}>
                  {!isEmpty(item.images) && item.images.length >= 4 ? (
                    <Image
                      srcImage={
                        !isEmpty(item.images[3].image)
                          ? item.images[3].image.small
                          : item.images[3].src
                      }
                      className={classes.imgSmall}
                    />
                  ) : (
                    <Box
                      className={clsx(
                        classes.imgSmall,
                        classes.backgroundDefault
                      )}
                    />
                  )}
                </Grid>
                <Grid
                  item
                  lg={6}
                  md={6}
                  sm={6}
                  className={classes.itemBannerSmall}
                >
                  {!isEmpty(item.images) && item.images.length >= 5 ? (
                    <Box className={classes.itemBgBlur}>
                      <Image
                        srcImage={item.images[4].src}
                        className={classes.imgSmall}
                        style={{ borderRadius: "0px 0px 8px 0px" }}
                      />

                      <Box
                        className={classes.wrapLoadMore}
                        style={{ cursor: "pointer" }}
                      >
                        <Box display="flex" alignItems="center">
                          {`+${!isEmpty(item.images) &&
                            item.images.length > 5 &&
                            item.images.length - 5}`}
                          <IconAlbum style={{ marginLeft: 4, marginTop: -4 }} />
                        </Box>
                      </Box>
                    </Box>
                  ) : (
                    <Box
                      className={clsx(
                        classes.imgSmall,
                        classes.backgroundDefault
                      )}
                      style={{ borderRadius: "0px 0px 8px 0px", marginLeft: 4 }}
                    />
                  )}
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </>
  );
};

export default InfoHotelDetails;
