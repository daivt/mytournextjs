import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Box, Breadcrumbs, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Link from "@src/link/Link";
import Layout from "@components/layout/desktop/Layout";
import {
  listIcons,
  listString,
  prefixUrlIcon,
  routeStatic,
} from "@utils/constants";
import {
  isEmpty,
  checkAndChangeCheckIO,
  handleSaveHotelDetailToStorage,
} from "@utils/helpers";
import ConvenientHotel from "@components/desktop/hotels/details/components/ConvenientHotel";
import InfoHotelDetails from "./components/InfoHotelDetails";
import RecentView from "./components/RecentView";
import HotelAround from "./components/HotelAround";
import HotelDescription from "./components/HotelDescription";
import RoomBooking from "./components/RoomBooking";
import EvaluateHotel from "./components/EvaluateHotel";
import PolicyHotel from "./components/PolicyHotel";
import TopAddress from "./components/TopAddress";
import ImageHotelModal from "./components/ImageHotelModal";

const useStyles = makeStyles((theme) => ({
  homeWrapper: { width: "100%", background: "white" },
  homeContainer: { maxWidth: 1188, margin: "0 auto", padding: "16px 0" },
  divider: {
    width: "100%",
    height: 4,
    background: "#EDF2F7",
    margin: "24px 0",
  },
  priceTag: {
    fontSize: 14,
    lineHeight: "17px",
    color: theme.palette.green.greenLight7,
    marginLeft: 12,
    padding: "9px 12px 9px 0",
  },
  warPriceTag: {
    marginTop: 24,
    display: "flex",
    alignItems: "center",
    borderRadius: 8,
    color: theme.palette.green.greenLight7,
    background: theme.palette.green.greenLight8,
  },
  selectRooms: {
    fontSize: 20,
    lineHeight: "23px",
    fontWeight: 600,
    marginTop: 32,
  },
  wrapBox: {
    maxWidth: 1180,
    margin: "0 auto",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    background: "#F7FAFC",
  },
  bannerImg: {
    width: "100%",
    maxHeight: 124,
    borderRadius: 8,
  },
}));
function WrapperContent({ children }) {
  const classes = useStyles();
  return (
    <Box className={classes.homeWrapper}>
      <Box className={classes.homeContainer}>{children}</Box>
    </Box>
  );
}
const HotelDetail = ({ paramsInit = {}, hotelDetail = {} }) => {
  const classes = useStyles();
  const [rooms, setRooms] = useState([]);
  const [firstReview, setFirstReview] = useState({});
  const [openImageHotel, setOpenImageHotel] = useState(false);
  const router = useRouter();
  const paramsUrl = {
    ...paramsInit,
    checkIn: checkAndChangeCheckIO(paramsInit.checkIn, "checkIn"),
    checkOut: checkAndChangeCheckIO(paramsInit.checkOut, "checkOut"),
    adults: Number(paramsInit.adults),
    rooms: Number(paramsInit.rooms),
    children: Number(paramsInit.children),
  };
  const getLinkInfo = (item) => {
    let params = { ...paramsUrl };
    if (paramsUrl.childrenAges) {
      params = { ...paramsUrl, childrenAges: paramsUrl.childrenAges.join(",") };
    }
    return {
      pathname: routeStatic.LISTING_HOTEL.href,
      query: {
        slug: [
          `${item.aliasCode}`,
          `khach-san-tai-${item.name.stringSlug()}.html`,
        ],
        ...params,
      },
    };
  };
  const checkShowComparePrice = () => {
    let isShow = false;
    rooms.forEach((el) => {
      el.rates.forEach((item) => {
        if (!isEmpty(item.specialOffer)) isShow = true;
      });
    });
    return isShow;
  };
  const scrollToItem = (idElement) => {
    const element = document.getElementById(idElement);
    if (element) {
      element.scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "nearest",
      });
    }
  };
  useEffect(() => {
    if (router?.query?.listReview) {
      scrollToItem("evaluate");
    }
    if (!isEmpty(hotelDetail)) {
      handleSaveHotelDetailToStorage(hotelDetail);
    }
  }, []);
  return (
    <Layout
      isHeader
      isFooter
      type="detail"
      paramsUrl={paramsUrl}
      hotelDetail={hotelDetail}
      title={`Giá phòng ${hotelDetail.name} từ ${paramsInit.checkIn} đến ${paramsInit.checkOut} | Đảm bảo giá tốt nhất`}
      description={`${hotelDetail.name} - Đặt phòng khách sạn ${hotelDetail.name} trực tuyến ở ${hotelDetail?.address?.provinceName} từ ${paramsInit.checkIn} đến ${paramsInit.checkOut}, cam kết giá rẻ nhất, nhanh nhất và dịch vụ khách hàng tốt nhất.`}
      keywords={`khách sạn, đặt phòng khách sạn, đặt phòng giá rẻ, đặt khách sạn hạng sang giá tốt, đặt khách sạn sát ngày giá tốt, đặt phòng khách sạn thuận tiện, hỗ trợ dịch vụ khách hàng 24/7 đặt phòng khách sạn, ${hotelDetail.name} giá rẻ`}
      iconOgImage={hotelDetail?.thumbnail?.image?.small}
    >
      <>
        <WrapperContent>
          <Breadcrumbs
            separator="›"
            style={{
              marginBottom: 32,
              fontSize: 12,
              lineHeight: "14px",
              fontWeight: 400,
              color: "#4A5568",
            }}
          >
            <Link color="inherit" href={{ pathname: routeStatic.HOTEL.href }}>
              {listString.IDS_MT_TEXT_HOTEL}
            </Link>
            {!isEmpty(hotelDetail.location) &&
              !isEmpty(hotelDetail.location.details) &&
              hotelDetail.location.details.map((el, i) => {
                return (
                  <Link
                    key={el.id}
                    color="inherit"
                    href={{ ...getLinkInfo(el) }}
                  >
                    {el.name}
                  </Link>
                );
              })}
            <Typography
              color="textPrimary"
              style={{ fontSize: 12, lineHeight: "14px", fontWeight: 400 }}
            >
              {hotelDetail.name}
            </Typography>
          </Breadcrumbs>
          <InfoHotelDetails
            item={hotelDetail}
            roomItem={!isEmpty(rooms) ? rooms[0] : {}}
            scrollToItem={scrollToItem}
            openImageHotel={() => setOpenImageHotel(true)}
          />
          <ConvenientHotel
            item={hotelDetail}
            firstReview={firstReview}
            scrollToItem={scrollToItem}
          />
        </WrapperContent>
        <Box id="banner_details" className={classes.bannerImg} />
        <Box className={classes.wrapBox}>
          <Box className={classes.selectRooms}>
            {listString.IDS_MT_SELECT_ROOM}
          </Box>
          {checkShowComparePrice() && (
            <Box className={classes.warPriceTag}>
              <Box style={{ width: 36, height: 36, margin: "6px 0 6px 12px" }}>
                <img
                  src={`${prefixUrlIcon}${listIcons.IconPriceTag2}`}
                  alt="icon_price_tag"
                  style={{ width: 36, height: 36 }}
                />
              </Box>
              <Typography className={classes.priceTag}>
                {listString.IDS_MT_TEXT_COMPARE_PRICE_THREE_SOURCE}
              </Typography>
            </Box>
          )}
        </Box>
        <RoomBooking
          hotelDetail={hotelDetail}
          paramsInit={paramsUrl}
          rooms={rooms}
          setRooms={setRooms}
        />
        <WrapperContent>
          <EvaluateHotel
            hotelDetail={hotelDetail}
            id="evaluate"
            setFirstReview={setFirstReview}
          />
          <Box className={classes.divider} />
          <TopAddress
            id="top_places"
            hotelDetail={hotelDetail}
            roomItem={!isEmpty(rooms) ? rooms[0] : {}}
          />
          <Box className={classes.divider} />
          <PolicyHotel hotelDetail={hotelDetail} />
          <Box className={classes.divider} />
          <HotelDescription hotelDetail={hotelDetail} />
          <HotelAround hotelDetail={hotelDetail} paramsUrl={paramsUrl} />
          <RecentView />
        </WrapperContent>
        {openImageHotel && (
          <ImageHotelModal
            hotelDetail={hotelDetail}
            handleClose={() => setOpenImageHotel(false)}
          />
        )}
      </>
    </Layout>
  );
};

export default HotelDetail;
