import { useState } from "react";
import { Box, Typography } from "@material-ui/core";
import { useRouter } from "next/router";
import { makeStyles } from "@material-ui/styles";
import { listString, routeStatic } from "@utils/constants";
import { cityList } from "@utils/dataFake";
import Link from "@src/link/Link";

const useStyles = makeStyles((theme) => ({
  headerContainer: {
    maxWidth: 1188,
    width: "100%",
    display: "flex",
    flexDirection: "column",
    margin: "0 auto",
  },
  headerContent: {
    display: "flex",
    color: "#1A202C",
    margin: "16px 0 16px 0",
  },
  textItem: { marginRight: 32, cursor: "pointer" },
  cityContentGroup: { display: "flex", flexWrap: "wrap" },
  cityText: {
    fontSize: 14,
    lineHeight: "32px",
    color: "#1A202C",
    width: "calc(100% / 6)",
  },
  listItem: {
    ...theme.typography.fontSize,
    lineHeight: "32px",
  },
}));

const CityList = () => {
  const classes = useStyles();

  return (
    <Box
      style={{
        width: "100%",
        paddingBottom: 126,
        paddingTop: 20,
        position: "relative",
        zIndex: 1,
      }}
    >
      <Box className={classes.headerContainer}>
        <Box className={classes.headerContent}>
          <Box className={classes.textItem}>
            <Typography
              variant="body2"
              style={{
                fontWeight: 600,
                fontSize: 24,
                lineHeight: "28px",
                color: "#1A202C",
                marginBottom: 8,
              }}
            >
              {listString.IDS_MT_HOT_HOTEL_TEXT}
            </Typography>
          </Box>
        </Box>
        <Box className={classes.cityContentGroup}>
          <>
            {cityList.map((el) => (
              <Box key={el.aliasCode} className={classes.cityText}>
                <Link
                  href={{
                    pathname: routeStatic.TOP_PRICE_HOTEL.href,
                    query: {
                      slug: [
                        `${el.aliasCode}`,
                        `khach-san-tai-${el.name.stringSlug()}.html`,
                      ],
                    },
                  }}
                  className="link-text"
                >
                  <Typography variant="caption" className={classes.listItem}>
                    {el.name}
                  </Typography>
                </Link>
              </Box>
            ))}
          </>
        </Box>
      </Box>
    </Box>
  );
};

export default CityList;
