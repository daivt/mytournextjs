import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Image from "@src/image/Image";
import SlideShow from "@components/common/slideShow/SlideShow";
import { IconArrowRight } from "@public/icons";
import { isEmpty } from "@utils/helpers";
import Link from "@src/link/Link";
import { listString, routeStatic } from "@utils/constants";

const useStyles = makeStyles((theme) => ({
  container: { background: "white", width: "100%" },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    marginBottom: 72,
    marginTop: 12,
  },
  imageContent: { borderRadius: 8, paddingTop: 24 },
  imageSlick: { width: 380, height: 160, borderRadius: 8 },
  linkBtn: {
    color: theme.palette.black.black3,
    textDecoration: "none !important",
  },
  slickDot: {
    bottom: "-16px !important",
    "& li": {
      height: 2,
      width: 16,
      margin: "0 2px",
      "& div": {
        height: 2,
        width: 16,
        borderRadius: 100,
        backgroundColor: theme.palette.gray.grayLight22,
        border: "none",
      },
    },
    "&.slick-active": {
      "& div": {
        backgroundColor: theme.palette.blue.blueLight8,
      },
    },
  },
  previewIcon: {
    transform: "rotate(180deg)",
  },
  image: {
    width: 128,
    height: 128,
    borderRadius: 1000,
  },
  wrapLocation: {
    fontSize: 12,
    lineHeight: "14px",
    fontWeight: 400,
    marginTop: 6,
    color: theme.palette.gray.grayDark8,
  },
  wrapImage: {
    display: "flex",
    flexDirection: "column",
    textAlign: "center",
    width: 128,
  },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
  },
  iconArrow: {
    width: 16,
    height: 17,
    stroke: theme.palette.black.black3,
  },
  wrapArrow: {
    "&:hover": {
      "& svg": {
        stroke: theme.palette.blue.blueLight8,
        transition: "all ease .2s",
      },
    },
  },
}));
const customStyle = {
  background: "white",
  height: "40px",
  width: "40px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 2,
  borderRadius: "100px",
  boxShadow: "0px 8px 12px rgb(0 0 0 / 10%)",
  cursor: "pointer",
};
function SamplePrevArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{
        ...customStyleArrow,
        position: "absolute",
        top: "50%",
        left: 0,
        transform: "translate(-50%, -50%)",
      }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight
        className={`svgFillAll ${classes.iconArrow}`}
        style={{ transform: "rotate(180deg)" }}
      />
    </div>
  );
}

function SampleNextArrow(props) {
  const classes = useStyles();
  const { customStyleArrow, onClick } = props;
  return (
    <div
      className={classes.wrapArrow}
      style={{
        ...customStyleArrow,
        position: "absolute",
        right: "-23px",
        top: "50%",
        transform: "translate(-50%, -50%)",
      }}
      onClick={onClick}
      aria-hidden="true"
    >
      <IconArrowRight className={`svgFillAll ${classes.iconArrow}`} />
    </div>
  );
}

const LocationList = ({ topLocation = [], paramsUrl = {} }) => {
  const classes = useStyles();
  const settings = {
    dots: false,
    infinite: topLocation.length > 7,
    speed: 500,
    slidesToShow: 7,
    slidesToScroll: 5,
    nextArrow: <SampleNextArrow customStyleArrow={customStyle} />,
    prevArrow: <SamplePrevArrow customStyleArrow={customStyle} />,
  };
  return (
    <Box className={classes.container}>
      <Box className={classes.content}>
        <Box className={classes.hotelGroup} style={{ marginBottom: 32 }}>
          <Typography variant="body2" className={classes.titleText}>
            {listString.IDS_MT_HOTEL_FOLLOW_ADDRESS}
          </Typography>
        </Box>
        {!isEmpty(topLocation) && (
          <SlideShow
            settingProps={settings}
            className={classes.imageContent}
            slickDotStyle={classes.slickDot}
          >
            {topLocation.map((item, i) => {
              return (
                <Link
                  href={{
                    pathname: routeStatic.LISTING_HOTEL.href,
                    query: {
                      slug: [
                        `${item.aliasCode}`,
                        `khach-san-tai-${
                          !isEmpty(item.name) ? item.name.stringSlug() : ""
                        }.html`,
                      ],
                      ...paramsUrl,
                    },
                  }}
                  className={classes.linkBtn}
                  key={i}
                >
                  <Box className={classes.wrapContent}>
                    <Box className={classes.wrapImage}>
                      <Image
                        srcImage={item.thumb}
                        className={classes.image}
                        alt={item.name}
                      />
                      <Typography variant="subtitle1" style={{ marginTop: 16 }}>
                        {item.name}
                      </Typography>
                      <Typography className={classes.wrapLocation}>
                        {`${item.numHotels} khách sạn`}
                      </Typography>
                    </Box>
                  </Box>
                </Link>
              );
            })}
          </SlideShow>
        )}
      </Box>
    </Box>
  );
};

export default LocationList;
