import { useState, useEffect } from "react";
import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import HotelItemVertical from "@components/common/desktop/itemView/HotelItemVertical";
import {
  isEmpty,
  adapterHotelHotDealPc,
  paramsDefaultHotel,
} from "@utils/helpers";
import { useRouter } from "next/router";
import Link from "@src/link/Link";
import {
  listString,
  routeStatic,
  DELAY_TIMEOUT_POLLING,
} from "@utils/constants";
import ButtonComponent from "@src/button/Button";
import { getHotelsAvailability } from "@api/hotels";
import { getTopHotels } from "@api/homes";

const useStyles = makeStyles((theme) => ({
  container: {
    background: "#EDF6F9",
    width: "calc(100% - 48px)",
    borderRadius: 12,
    margin: "0 24px 24px 24px",
    padding: "56px 0",
  },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "column",
  },
  hotelGroup: { display: "flex", flexWrap: "wrap" },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
  },
  buttonGroup: { display: "flex" },
}));
const CityHotel = ({ cityInfo = {} }) => {
  const classes = useStyles();
  const router = useRouter();
  let breakPolling = true;
  const [topHotels, setTopHotels] = useState([]);
  const mergerPriceToData = (data = [], result = []) => {
    const dataResult = [...result];
    data.forEach((el) => {
      dataResult.forEach((item, index) => {
        if (item.id === el.id) {
          dataResult[index] = {
            ...item,
            basePromotionInfo: el.basePromotionInfo,
            price: el.basePrice,
            hiddenPrice: el.hiddenPrice,
          };
          return;
        }
      });
    });
    setTopHotels(dataResult);
  };
  const fetchPriceHotel = async (ids, topHotelData) => {
    let polling = true;
    try {
      const dataDto = {
        ...paramsDefaultHotel(),
        hotelIds: ids,
        searchType: "hotel",
      };
      while (polling && breakPolling) {
        const { data } = await getHotelsAvailability(dataDto);
        if (data.code === 200) {
          if (data.data.completed) {
            mergerPriceToData(data.data.items, topHotelData);
            polling = false;
            break;
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      polling = false;
    }
  };
  const fetchData = async () => {
    try {
      let params = { page: 1, size: 20, ...paramsDefaultHotel() };
      if (!isEmpty(cityInfo)) params = { ...params, province: cityInfo.id };
      const { data } = await getTopHotels({ ...params });
      if (data.code === 200 && !isEmpty(data.data)) {
        setTopHotels(data.data.items || []);
        if (!isEmpty(data.data.items)) {
          fetchPriceHotel(
            data.data.items.map((el) => el.id),
            data.data.items
          );
        }
      }
    } catch (error) {}
  };
  useEffect(() => {
    fetchData();
    return () => {
      breakPolling = false;
    }; // eslint-disable-next-line
  }, []);
  if (isEmpty(topHotels)) return <></>;
  return (
    <Box className={classes.container}>
      <Box className={classes.content}>
        <Box
          className={classes.hotelGroup}
          style={{ marginBottom: 32, justifyContent: "space-between" }}
        >
          <Typography variant="body2" className={classes.titleText}>
            Khách sạn tại {cityInfo.name}
          </Typography>
        </Box>
        <Box className={classes.hotelGroup} style={{ margin: "0 -12px" }}>
          {!isEmpty(topHotels) &&
            adapterHotelHotDealPc(topHotels).map((el, i) => (
              <Box style={{ margin: "0 12px", marginBottom: 24 }} key={i}>
                <HotelItemVertical
                  item={el}
                  isShowLastBook
                  isTopSale
                  isHotelInterest
                  isCountBooking={false}
                  isShowStatus
                  paramsFilterInit={paramsDefaultHotel()}
                />
              </Box>
            ))}
        </Box>
        <Box display="flex" justifyContent="center" pt={2}>
          <Link
            href={{
              pathname: routeStatic.LISTING_HOTEL.href,
              query: { ...router.query },
            }}
            className={classes.linkBtn}
          >
            <ButtonComponent
              typeButton="outlined"
              backgroundColor="inherit"
              color="#00B6F3"
              width="fit-content"
              borderColor="#00B6F3"
              borderRadius={8}
              padding="12px 50px"
            >
              {listString.IDS_TEXT_VIEW_ALL}
            </ButtonComponent>
          </Link>
        </Box>
      </Box>
    </Box>
  );
};

export default CityHotel;
