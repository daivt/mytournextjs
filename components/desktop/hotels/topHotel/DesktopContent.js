import { useState, useEffect } from "react";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Layout from "@components/layout/desktop/Layout";
import InfoDescriptionHotelCard from "@components/common/desktop/card/InfoDescriptionHotelCard";
import { useRouter } from "next/router";
import { paramsDefaultHotel } from "@utils/helpers";
import MyCoupon from "@components/common/desktop/myCoupon/MyCoupon";
import CityHotel from "./components/CityHotel";
import LocationList from "./components/LocationList";
import RecentView from "./components/RecentView";
import { cityList, topAddressLocation } from "../../../../utils/dataFake";
import CityList from "./components/CityList";

const useStyles = makeStyles((theme) => ({
  homeWrapper: { width: "100%", background: "white" },
  homeContainer: {
    maxWidth: 1188,
    margin: "0 auto",
    padding: "24px 0 56px 0",
  },
}));
const Home = ({ topLocation = [] }) => {
  const classes = useStyles();
  const router = useRouter();
  const aliasCode = router.query.slug[0];
  const cityInfo = [...cityList, ...topAddressLocation].find(
    (v) => v.aliasCode === aliasCode
  );
  const [isServerLoaded, setServerLoaded] = useState(false);

  useEffect(() => {
    setServerLoaded(true);
  }, []);
  return (
    <Layout
      isHeader
      isFooter
      type="top_hotel"
      topLocation={topLocation}
      cityInfo={cityInfo}
      title="Đặt phòng khách sạn hạng sang giá rẻ - giá tốt nhất trên Mytour.vn | Đặt sát ngày giá càng tốt."
    >
      <Box className={classes.homeWrapper}>
        <Box className={classes.homeContainer}>
          <InfoDescriptionHotelCard />
        </Box>
        {isServerLoaded && <MyCoupon />}
        <LocationList
          topLocation={topLocation}
          paramsUrl={paramsDefaultHotel()}
        />
        {isServerLoaded && <RecentView />}
        <CityHotel cityInfo={cityInfo} />
        <CityList />
      </Box>
    </Layout>
  );
};

export default Home;
