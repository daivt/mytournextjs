import { Box, Typography } from '@material-ui/core';
import { useRouter } from 'next/router';
import { useTheme } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/styles';
import { listString } from '@utils/constants';
import Image from '@src/image/Image';
import { isEmpty } from '@utils/helpers';
import ItemRatesRoom from '@components/desktop/hotels/details/components/ItemRatesRoom';
import InfoPrice from '@components/desktop/hotels/booking/components/InfoPrice';

import { IconClock, IconCheck } from '@public/icons';
import moment from 'moment';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  roomContainer: { width: '100%' },
  content: {
    width: '100%',
    borderRadius: 12,
    padding: '40px 24px 0 24px',
    display: 'flex',
    flexDirection: 'column',
    marginTop: -12,
  },
  divider: {
    width: '100%',
    height: 4,
    background: '#EDF2F7',
    margin: '12px 0',
  },
  imgHotel: { width: 432, height: 114, borderRadius: 8, position: 'relative' },
  IconUser2: {
    width: 12,
    height: 12,
  },
  roomName: {
    fontSize: 18,
    fontWeight: 600,
    lineHeight: '21px',
    justifyContent: 'inherit !important',
  },
  lastBooking: {
    marginTop: 12,
    borderRadius: 8,
    background: theme.palette.blue.blueLight9,
    color: theme.palette.primary.main,
    fontSize: 14,
    lineHeight: '16px',
    fontWeight: 400,
  },
  agreeRoom: {
    background: theme.palette.pink.pink1,
    color: theme.palette.pink.main,
    fontWeight: 400,
    lineHeight: '20px',
  },
  lastTime: {
    fontWeight: 600,
    marginLeft: 2,
  },
  iconClock: {
    stroke: theme.palette.primary.main,
    margin: '0 10px 0 14px',
  },
  iconCheck: {
    stroke: theme.palette.pink.main,
    margin: '0 10px 0 14px',
  },
  styleColorWrap: {
    color: theme.palette.black.black3,
  },
  discount: {
    background: '#FF1284',
    color: theme.palette.white.main,
    fontSize: 18,
    lineHeight: '21px',
    fontWeight: 600,
    borderRadius: '8px 0',
    display: 'flex',
    alignItems: 'center',
    width: 125,
    justifyContent: 'center',
    padding: '5px 10px',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 2,
    whiteSpace: 'nowrap',
  },
}));

const RoomInfo = ({
  rateDetail = {},
  hotelDetail = {},
  listPromotion = [],
  promotionActive = {},
  handleSetVoucher = () => {},
  promotionDiscount = {},
  isRequestPrice = false,
  feePayment = {},
  applyVoucher = {},
  showTooTipPromotion = {},
  statusTooltipAddPromotion = {},
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const checkIn = moment(router.query.checkIn, 'DD-MM-YYYY');
  const checkOut = moment(router.query.checkOut, 'DD-MM-YYYY');
  const numRooms = router.query.rooms;
  const countNight = Math.abs(
    moment(checkIn)
      .startOf('day')
      .diff(checkOut.startOf('day'), 'days')
  );
  const rateDetailRoom = !isEmpty(rateDetail.rates)
    ? rateDetail.rates[0].basePromotionInfo
    : {};
  return (
    <Box className={classes.roomContainer}>
      <Box className={classes.content}>
        {!isEmpty(rateDetail.thumbnail) ? (
          <Box className={classes.imgHotel}>
            {!isEmpty(rateDetailRoom) &&
              rateDetailRoom.discountPercentage &&
              rateDetailRoom.discountPercentage > 0 && (
                <Box className={classes.discount}>
                  <Box
                    style={{ fontWeight: 400, fontSize: 16, marginRight: 2 }}
                  >
                    Giảm giá{' '}
                  </Box>
                  {`${rateDetailRoom.discountPercentage}%`}
                </Box>
              )}

            <Image
              srcImage={rateDetail.thumbnail}
              alt=""
              className={classes.imgHotel}
              borderRadiusProp="8px"
            />
          </Box>
        ) : (
          <Box
            style={{ background: '#EDF2F7' }}
            className={classes.imgHotel}
            borderRadiusProp="8px"
          >
            {!isEmpty(rateDetailRoom) &&
              rateDetailRoom.discountPercentage &&
              rateDetailRoom.discountPercentage > 0 && (
                <Box className={classes.discount}>
                  <Box style={{ fontWeight: 400, fontSize: 16 }}>
                    Giảm giá &nbsp;
                  </Box>
                  {`${rateDetailRoom.discountPercentage}%`}
                </Box>
              )}
          </Box>
        )}

        {!isEmpty(rateDetail.rates) &&
          rateDetail.rates.map((el, index) => (
            <ItemRatesRoom
              key={index}
              rateInfo={el}
              index={index}
              roomInfo={rateDetail}
              showRoomName={false}
              styleRoomName={classes.roomName}
              showRoomArea={false}
              showNumRooms={router.query.rooms}
              showBedGroup
              showNumRooms={false}
              showFlashAgree={false}
              showNumberAllotment={false}
              showGuest
              styleColorWrap={classes.styleColorWrap}
              showBuySignal={false}
              showSpecialDeal
              showBuySignal
              showPolicies
              showFlashAgree
            />
          ))}
        <Box className={classes.lastBooking}>
          {!isEmpty(hotelDetail.lastBookedTime) &&
            moment().diff(moment(hotelDetail.lastBookedTime)) <= 86400000 && (
              <Box
                display="flex"
                alignItems="center"
                style={{ padding: '8px 0 7px' }}
              >
                <IconClock className={`svgFillAll ${classes.iconClock}`} />
                {listString.IDS_MT_TEXT_BOOKING_LAST_TIME}

                <Box className={classes.lastTime}>
                  {moment().diff(moment(hotelDetail.lastBookedTime)) <=
                    86400000 && moment(hotelDetail.lastBookedTime).fromNow()}
                </Box>
              </Box>
            )}
        </Box>

        {isRequestPrice ? (
          <Box className={clsx(classes.lastBooking, classes.agreeRoom)}>
            <Box
              display="flex"
              alignItems="center"
              style={{ padding: '8px 0 7px' }}
            >
              <Box>
                <IconCheck className={`svgFillAll ${classes.iconCheck}`} />
              </Box>
              <Box style={{ marginLeft: '2px' }}>
                {listString.IDS_MT_TEXT_AGREE_ROOM_EARLY}
              </Box>
            </Box>
          </Box>
        ) : (
          <>
            {!isEmpty(rateDetail.rates) &&
              rateDetail.rates[0].availableAllotment <= 5 && (
                <Box className={clsx(classes.lastBooking, classes.agreeRoom)}>
                  <Box
                    display="flex"
                    alignItems="center"
                    style={{ padding: '8px 0 7px' }}
                  >
                    <Box style={{ margin: '0 12px 0 12px' }}>
                      {`Đừng bỏ lỡ! ${
                        rateDetail.rates[0].availableAllotment === 1
                          ? 'Phòng cuối cùng của chúng tôi.'
                          : `Chúng tôi chỉ còn ${rateDetail.rates[0].availableAllotment} phòng có
                giá này.`
                      } Hãy đặt ngay!`}
                    </Box>
                  </Box>
                </Box>
              )}
          </>
        )}
      </Box>

      <Box className={classes.divider} />
      <Box className={classes.content} style={{ marginTop: '-40px' }}>
        {!isEmpty(rateDetail.rates) &&
          rateDetail.rates.map((el, index) => (
            <InfoPrice
              hotelDetail={hotelDetail}
              listPromotion={listPromotion}
              promotionActive={promotionActive}
              handleSetVoucher={handleSetVoucher}
              promotionDiscount={promotionDiscount || {}}
              key={index}
              rates={el}
              numRooms={numRooms}
              countNight={countNight}
              feePayment={feePayment}
              applyVoucher={applyVoucher}
              showTooTipPromotion={showTooTipPromotion}
              statusTooltipAddPromotion={statusTooltipAddPromotion}
            />
          ))}
      </Box>
    </Box>
  );
};

export default RoomInfo;
