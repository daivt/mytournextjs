import {
  Box,
  Typography,
  Radio,
  Accordion,
  Checkbox,
  TextField,
  Tooltip,
} from "@material-ui/core";
import sanitizeHtml from "sanitize-html";
import { withStyles } from "@material-ui/core/styles";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import { useField, useFormikContext } from "formik";
import { useRef, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import {
  listString,
  optionClean,
  prefixUrlIcon,
  listIcons,
} from "@utils/constants";
import FormControlTextField from "@src/form/FormControlTextField";
import {
  IconBedDouble,
  IconBedSingle,
  IconMinusFill,
  IconPlusFill,
} from "@public/icons";
import clsx from "clsx";
import { isEmpty, unEscape } from "@utils/helpers";
import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  blockContainer: {
    width: "100%",
    borderRadius: 8,
    padding: 24,
    background: "white",
    marginTop: 12,
    fontSize: 14,
    lineHeight: "17px",
  },
  titleBlock: {
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
    marginBottom: 16,
  },
  normalText: { fontSize: 14, lineHeight: "17px" },
  bedGroup: {
    display: "flex",
    alignItems: "center",
    marginBottom: 16,
    flexWrap: "wrap",
  },
  iconRadio: {
    borderRadius: "50%",
    width: 20,
    height: 20,
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
  },
  icon: {
    borderRadius: 6,
    width: 22,
    height: 22,
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
    "$root.Mui-focusVisible &": {
      outline: "2px auto #00B6F3",
      outlineOffset: 2,
    },
  },
  checkedIconRadio: {
    backgroundColor: "#00B6F3",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: 22,
      height: 22,
      backgroundImage: "url(/icons/icon_checked.svg)",
      backgroundSize: "cover",
      content: '""',
    },
  },
  checkBoxWrapper: {
    display: "flex",
    alignItems: "center",
    margin: "0 -9px",
    cursor: "pointer",
  },
  collapseContainer: {
    marginBottom: 12,
    boxShadow: "none",
    background: "#F7FAFC",
    borderRadius: 8,
    "&:before": { height: 0 },
    "&.Mui-expanded": { minHeight: 40, margin: "12px 0" },
  },
  summaryInfo: {
    minHeight: 40,
    "&.Mui-expanded": { minHeight: 40 },
    "& > .MuiAccordionSummary-content": { margin: "12px 0 8px 0" },
  },
  detailWrapper: {
    flexDirection: "column",
    padding: "0 16px 16px",
    "> input": { fontSize: 14 },
  },
  otherText: {
    borderRadius: 8,
    minHeight: 20,
    "& > .MuiInputBase-inputMultiline": { fontSize: 14 },
  },
  inputItem: {
    margin: 0,
    marginBottom: 20,
    "& > .MuiInput-root": { marginTop: 0 },
  },
  invoiceTextGroup: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  tooltipText: {
    fontSize: 14,
    lineHeight: "22px",
    "& li": { marginBottom: 12 },
  },
  rowGroupItem: {
    display: "flex",
    justifyContent: "space-between",
  },
  wrapInvoice: {
    background: "#F7FAFC",
    borderRadius: 8,
    padding: 16,
  },
  iconDot: {
    fill: theme.palette.black.black3,
  },
  exportInvoice: {
    display: "flex",
    alignItems: "center",
    color: theme.palette.black.black3,
  },
  borderRadius: "50%",
  invoiceDes: {
    "& > p": {
      lineHeight: "14px !important",
    },
    paddingTop: 5,
  },
  iconDoubleSingleBed: {
    width: 42,
    height: 24,
  },
  iconBedDouble: {
    fill: theme.palette.gray.grayDark7,
  },
}));
const MyTextField = ({ ...props }) => {
  const [field, meta, helpers] = useField(props);
  const { submitCount } = useFormikContext();
  return (
    <TextField
      fullWidth
      {...field}
      {...props}
      error={submitCount > 0 && meta.error ? meta.error : ""}
    />
  );
};
const MyCheckboxField = ({ ...props }) => {
  const [field, meta, helpers] = useField(props);
  return <Checkbox {...field} {...props} />;
};
const HtmlTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: "#FFFFFF",
    color: "#1A202C",
    maxWidth: 388,
    fontSize: 14,
    lineHeight: "22px",
    borderRadius: 8,
    boxShadow: "0px 8px 8px rgba(0, 0, 0, 0.1)",
    border: "1px solid #E2E8F0",
    padding: "24px 24px 12px 24px",
  },
  arrow: { color: "#C4C4C4" },
}))(Tooltip);

const RequireInfo = ({
  hotelDetail = {},
  valueForm = {},
  setFieldValue,
  bedList,
  shrui,
  setShrui,
  roomRate = {},
}) => {
  const invoiceInfo =
    !isEmpty(roomRate.rates) &&
    !isEmpty(roomRate.rates[0]) &&
    !isEmpty(roomRate.rates[0].invoiceInfo)
      ? roomRate.rates[0].invoiceInfo
      : "";
  const classes = useStyles();
  const [isDoubleBed, setDoubleBed] = useState(false);
  const [openRequire, setOpenRequire] = useState(false);
  const [openOrder, setOpenOrder] = useState(false);
  const inputElSmoke = useRef(null);
  const inputHightFloorRoom = useRef(null);
  const inputExportInvoice = useRef(null);
  const checkIcon = (className) => (
    <span className={clsx(className, classes.checkedIconRadio)} />
  );
  const handleRequireSmoke = () => {
    inputElSmoke.current.click();
  };
  const handleOrderFloorRoom = () => {
    inputHightFloorRoom.current.click();
  };
  const handleExportInvoice = () => {
    inputExportInvoice.current.click();
  };

  const requireText = (textStr) => {
    return (
      <span
        style={{
          fontSize: 16,
          fontWeight: 400,
          lineHeight: "19px",
          color: "#718096",
        }}
      >
        {textStr}
        <span style={{ color: "red" }}>*</span>
      </span>
    );
  };
  const nonRequireText = (textStr) => {
    return (
      <span
        style={{
          fontSize: 16,
          fontWeight: 400,
          lineHeight: "19px",
          color: "#718096",
        }}
      >
        {textStr}
      </span>
    );
  };
  return (
    <>
      <Box className={classes.blockContainer}>
        <Typography variant="body2" className={classes.titleBlock}>
          {listString.IDS_MT_TEXT_REQUEST_SPECIAL}
        </Typography>
        <Typography
          variant="body2"
          className={classes.normalText}
          style={{ marginBottom: 12 }}
        >
          {listString.IDS_MT_SELECT_BED_TYPE}
        </Typography>
        <Box className={classes.bedGroup}>
          {!isEmpty(bedList) &&
            bedList.map((el) => (
              <Box
                key={el.shrui}
                style={{
                  display: "flex",
                  width: "50%",
                  alignItems: "center",
                }}
                onClick={() => setShrui(el.shrui)}
              >
                <Radio
                  checkedIcon={checkIcon(classes.iconRadio)}
                  icon={<span className={classes.iconRadio} />}
                  checked={shrui === el.shrui}
                />
                {el.numberBed > 2 ? (
                  <Box display="flex" pr={2 / 8}>
                    <Image
                      srcImage={`${prefixUrlIcon}${
                        el.bedTypes.includes("twin")
                          ? listIcons.Icon3SingleBed
                          : listIcons.IconDoubleSingleBed
                      }`}
                      className={classes.iconDoubleSingleBed}
                    />
                  </Box>
                ) : (
                  el.bedTypes.map((item, index) => (
                    <Box key={index.toString()} display="flex" pr={2 / 8}>
                      {item.toLowerCase().includes("twin") ? (
                        <IconBedSingle
                          className={`svgFillAll ${classes.iconBedDouble}`}
                        />
                      ) : (
                        <IconBedDouble
                          className={`svgFillAll ${classes.iconBedDouble}`}
                        />
                      )}
                    </Box>
                  ))
                )}
                <Typography
                  variant="body2"
                  className={classes.normalText}
                  style={{ marginLeft: 8 }}
                >
                  {el.bedInfo}
                </Typography>
              </Box>
            ))}
        </Box>

        <Accordion
          className={classes.collapseContainer}
          expanded={openRequire}
          onChange={() => setOpenRequire(!openRequire)}
        >
          <AccordionSummary
            expandIcon={openRequire ? <IconMinusFill /> : <IconPlusFill />}
            className={classes.summaryInfo}
          >
            <Typography
              className={classes.normalText}
              style={{ fontWeight: 600 }}
            >
              {listString.IDS_MT_TEXT_REQUEST_SPECIAL}
            </Typography>
          </AccordionSummary>
          <AccordionDetails className={classes.detailWrapper}>
            <Box className={classes.checkBoxWrapper}>
              <MyCheckboxField
                className={classes.root}
                color="default"
                name="noSmoking"
                checkedIcon={checkIcon(classes.icon)}
                icon={<span className={classes.icon} />}
                inputRef={inputElSmoke}
              />
              <Box
                component="span"
                className={classes.textItem}
                onClick={handleRequireSmoke}
              >
                {listString.IDS_MT_NO_SMOKE_ROOM}
              </Box>
            </Box>
            <Box className={classes.checkBoxWrapper}>
              <MyCheckboxField
                className={classes.root}
                color="default"
                name="isHighFloor"
                checkedIcon={checkIcon(classes.icon)}
                icon={<span className={classes.icon} />}
                inputRef={inputHightFloorRoom}
              />
              <Box
                component="span"
                className={classes.textItem}
                onClick={handleOrderFloorRoom}
              >
                {listString.IDS_MT_ROOM_HIGH_LEVEL}
              </Box>
            </Box>
            <Typography
              className={classes.normalText}
              style={{ fontWeight: 600, margin: "12px 0" }}
            >
              {listString.IDS_MT_TEXT_REQUEST_PRIVATE}
            </Typography>
            <FormControlTextField
              hideHelperText
              formControlStyle={{ marginRight: 0 }}
              className={classes.otherText}
              placeholder={
                listString.IDS_MT_TEXT_PLACEHOLDER_ENTER_REQUEST_OTHER
              }
              multiline
              rows={3}
              value={valueForm.specialRequest}
              onChange={(e) => setFieldValue("specialRequest", e.target.value)}
            />
          </AccordionDetails>
        </Accordion>
        {invoiceInfo.invoiceType === "not_marketplace" ? (
          <Accordion
            className={classes.collapseContainer}
            expanded={openOrder}
            onChange={() => setOpenOrder(!openOrder)}
          >
            <AccordionSummary
              expandIcon={openOrder ? <IconMinusFill /> : <IconPlusFill />}
              className={classes.summaryInfo}
            >
              <Typography
                className={classes.normalText}
                style={{ fontWeight: 600 }}
              >
                {listString.IDS_TEXT_EXPORT_INVOICE}
              </Typography>
            </AccordionSummary>
            <AccordionDetails className={classes.detailWrapper}>
              <Box className={classes.invoiceTextGroup}>
                <Box className={classes.checkBoxWrapper}>
                  <MyCheckboxField
                    className={classes.root}
                    color="default"
                    name="invoiceRequest"
                    checkedIcon={checkIcon(classes.icon)}
                    icon={<span className={classes.icon} />}
                    inputRef={inputExportInvoice}
                  />
                  <Box
                    component="span"
                    className={classes.textItem}
                    onClick={handleExportInvoice}
                  >
                    {listString.IDS_MT_INVOICE_REQUIRE}
                  </Box>
                </Box>
                <HtmlTooltip
                  title={
                    <Box>
                      <Typography
                        className={classes.tooltipText}
                        style={{ fontWeight: 600 }}
                      >
                        {listString.IDS_TEXT_CONDITION_INVOICE_TITLE}
                      </Typography>
                      <ul
                        className={classes.tooltipText}
                        style={{ paddingLeft: 16 }}
                      >
                        <li>{listString.IDS_MT_REQUIRE_INVOICE_DES_1}</li>
                        <li>{listString.IDS_MT_REQUIRE_INVOICE_DES_2}</li>
                        <li>{listString.IDS_MT_REQUIRE_INVOICE_DES_3}</li>
                        <li>{listString.IDS_MT_REQUIRE_INVOICE_DES_4}</li>
                      </ul>
                    </Box>
                  }
                  arrow
                >
                  <span
                    className={classes.textItem}
                    style={{ color: "#00B6F3" }}
                  >
                    {listString.IDS_MT_INVOICE_REQUIRE}
                  </span>
                </HtmlTooltip>
              </Box>
              {valueForm.invoiceRequest && (
                <>
                  <Box className={classes.rowGroupItem}>
                    <MyTextField
                      fullWidth
                      // required
                      label={requireText(listString.IDS_MT_TEXT_CODE_INVOICE)}
                      name="taxNumber"
                      className={classes.inputItem}
                      style={{ marginTop: 0, width: "calc(50% - 12px)" }}
                    />
                    <MyTextField
                      fullWidth
                      label={requireText(
                        listString.IDS_TEXT_PLACEHOLDER_INVOICE_CUSTOMER_NAME
                      )}
                      name="recipientName"
                      // required
                      className={classes.inputItem}
                      style={{ marginTop: 0, width: "calc(50% - 12px)" }}
                    />
                  </Box>
                  <Box className={classes.rowGroupItem}>
                    <MyTextField
                      fullWidth
                      label={requireText(
                        listString.IDS_TEXT_PLACEHOLDER_INVOICE_NAME
                      )}
                      name="companyName"
                      // required
                      className={classes.inputItem}
                      style={{ marginTop: 0, width: "calc(50% - 12px)" }}
                    />
                    <MyTextField
                      fullWidth
                      // required
                      label={requireText(listString.IDS_MT_TEXT_EMAIL)}
                      name="recipientEmail"
                      className={classes.inputItem}
                      style={{ marginTop: 0, width: "calc(50% - 12px)" }}
                    />
                  </Box>
                  <Box className={classes.rowGroupItem}>
                    <MyTextField
                      fullWidth
                      label={requireText(
                        listString.IDS_TEXT_PLACEHOLDER_INVOICE_ADDRESS
                      )}
                      name="companyAddress"
                      className={classes.inputItem}
                      style={{ marginTop: 0, width: "calc(50% - 12px)" }}
                    />
                    <MyTextField
                      fullWidth
                      label={nonRequireText(
                        listString.IDS_TEXT_PLACEHOLDER_INVOICE_NOTE
                      )}
                      name="note"
                      className={classes.inputItem}
                      style={{ marginTop: 0, width: "calc(50% - 12px)" }}
                    />
                  </Box>
                  <MyTextField
                    fullWidth
                    label={requireText(
                      listString.IDS_TEXT_PLACEHOLDER_INVOICE_RECEIVER
                    )}
                    name="recipientAddress"
                    className={classes.inputItem}
                  />
                </>
              )}
            </AccordionDetails>
          </Accordion>
        ) : (
          <Box className={classes.wrapInvoice}>
            <Typography
              className={classes.normalText}
              style={{ fontWeight: 600 }}
            >
              {listString.IDS_TEXT_EXPORT_INVOICE}
            </Typography>
            <Box
              fontSize={14}
              lineHeight="22px"
              color="gray.grayDark1"
              className={classes.invoiceDes}
              dangerouslySetInnerHTML={{
                __html: sanitizeHtml(
                  unEscape(unescape(invoiceInfo.invoiceDescription || "")),
                  optionClean
                ),
              }}
            />
          </Box>
        )}
      </Box>
    </>
  );
};

export default RequireInfo;
