import { useEffect, useRef } from "react";
import { Box, Typography, TextField, Checkbox } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { listString, listEventHotel } from "@utils/constants";
import { IconUser } from "@public/icons";
import clsx from "clsx";
import { isEmpty } from "@utils/helpers";
import { useField, useFormikContext } from "formik";
import AuthWrapper from "@components/layout/desktop/components/AuthWrapper";
import { useSystem } from "@contextProvider/ContextProvider";
import * as gtm from "@utils/gtm";
const useStyles = makeStyles((theme) => ({
  blockContainer: {
    width: "100%",
    borderRadius: 8,
    padding: 24,
    background: "white",
    marginTop: 12,
  },
  titleBlock: {
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
    marginBottom: 10,
  },
  titleCustomerInfo: {
    fontSize: 14,
    lineHeight: "17px",
    fontWeight: 600,
    marginTop: 16,
    marginBottom: 10,
  },
  inputItem: {
    margin: 0,
    marginBottom: 20,
    "& > .MuiInput-root": { marginTop: 0 },
  },
  icon: {
    borderRadius: 6,
    width: 22,
    height: 22,
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
    "$root.Mui-focusVisible &": {
      outline: "2px auto #00B6F3",
      outlineOffset: 2,
    },
  },
  checkedIconRadio: {
    backgroundColor: "#00B6F3",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: 22,
      height: 22,
      backgroundImage: "url(/icons/icon_checked.svg)",
      backgroundSize: "cover",
      content: '""',
    },
  },
  checkBoxWrapper: {
    display: "flex",
    alignItems: "center",
    margin: "0 -9px",
    cursor: "pointer",
  },
  textItem: { fontSize: 14, lineHeight: "17px" },
  descriptionText: {
    display: "flex",
    alignItems: "center",
    background: "rgba(0, 182, 243, 0.1)",
    borderRadius: 8,
    padding: "10px 14px",
    fontSize: 14,
    lineHeight: "17px",
    marginBottom: 12,
  },
}));
const MyTextField = ({ ...props }) => {
  const [field, meta, helpers] = useField(props);
  const { submitCount } = useFormikContext();
  return (
    <TextField
      fullWidth
      {...field}
      {...props}
      error={submitCount > 0 && meta.error ? meta.error : ""}
    />
  );
};
const MyCheckboxField = ({ ...props }) => {
  const [field, meta, helpers] = useField(props);
  return <Checkbox {...field} {...props} />;
};
const ContactInfo = ({ hotelDetail = {}, valueForm = {} }) => {
  const classes = useStyles();
  const { systemReducer } = useSystem();
  const informationUser = systemReducer.informationUser || {};
  const isLogin = !isEmpty(informationUser);
  const { setFieldValue, values } = useFormikContext();
  useEffect(() => {
    const informationUser = systemReducer.informationUser || {};
    if (!isEmpty(informationUser)) {
      setFieldValue("name", informationUser?.name || "");
      setFieldValue("phoneNumber", informationUser?.phoneInfo || "");
      setFieldValue("email", informationUser?.emailInfo || "");
    }
  }, [systemReducer]);
  const checkIcon = (className) => (
    <span className={clsx(className, classes.checkedIconRadio)} />
  );
  const requireText = (textStr) => {
    return (
      <span
        style={{
          fontSize: 16,
          fontWeight: 400,
          lineHeight: "19px",
          color: "#718096",
        }}
      >
        {textStr}
        <span style={{ color: "red" }}>*</span>
      </span>
    );
  };
  const nonRequireText = (textStr) => {
    return (
      <span
        style={{
          fontSize: 16,
          fontWeight: 400,
          lineHeight: "19px",
          color: "#718096",
        }}
      >
        {textStr}
      </span>
    );
  };
  const handleOrder = () => {
    inputEl.current.click();
  };

  const handleBlur = (type = "") => {
    if (type === "phoneNumber") {
      gtm.addEventGtm(listEventHotel.HotelInitCheckout);
    } else {
      gtm.addEventGtm(listEventHotel.HotelInitCheckout);
    }
  };

  const inputEl = useRef(null);
  return (
    <>
      <Box className={classes.blockContainer}>
        <Typography variant="body2" className={classes.titleBlock}>
          {listString.IDS_MT_TEXT_INFO_CONTACT}
        </Typography>
        {!isLogin && (
          <AuthWrapper typeModal="MODAL_LOGIN">
            <Typography variant="body2" className={classes.descriptionText}>
              <IconUser className="svgFillAll" style={{ stroke: "#00B6F3" }} />
              &nbsp;
              <span style={{ color: "#00B6F3", cursor: "pointer" }}>
                {listString.IDS_MT_TEXT_LOGIN}
              </span>
              &nbsp;
              <span>để đặt phòng nhanh hơn, không cần nhập thông tin!</span>
            </Typography>
          </AuthWrapper>
        )}

        <MyTextField
          fullWidth
          // required
          label={requireText(listString.IDS_MT_TEXT_CONTACT_NAME_SIMPLE)}
          name="name"
          className={classes.inputItem}
          style={{ marginTop: 0 }}
        />
        <MyTextField
          fullWidth
          // required
          label={requireText(listString.IDS_MT_TEXT_PHONE_NUMBER)}
          name="phoneNumber"
          className={classes.inputItem}
          style={{ width: "49%" }}
          onBlur={() => handleBlur("phoneNumber")}
        />
        <MyTextField
          fullWidth
          // required
          label={requireText(listString.IDS_MT_TEXT_EMAIL)}
          name="email"
          className={classes.inputItem}
          style={{ width: "49%", marginLeft: "2%" }}
          onBlur={() => handleBlur("email")}
        />
        <Box className={classes.checkBoxWrapper}>
          <MyCheckboxField
            className={classes.root}
            inputRef={inputEl}
            color="default"
            name="other"
            checkedIcon={checkIcon(classes.icon)}
            icon={<span className={classes.icon} />}
          />
          <span className={classes.textItem} onClick={handleOrder}>
            {listString.IDS_MT_ORDER_ROOM_FOR_OTHER_CUSTOMER}
          </span>
        </Box>
        {valueForm.other && (
          <>
            <Typography variant="body2" className={classes.titleCustomerInfo}>
              {listString.IDS_MT_OTHER_CUSTOMER_INFO}
            </Typography>
            <MyTextField
              fullWidth
              // required
              label={nonRequireText(listString.IDS_MT_TEXT_CONTACT_NAME_SIMPLE)}
              name="contactName"
              className={classes.inputItem}
            />
          </>
        )}
      </Box>
    </>
  );
};

export default ContactInfo;
