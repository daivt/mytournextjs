import { makeStyles } from "@material-ui/styles";
import { Backdrop, Box, Fade, Modal, useTheme } from "@material-ui/core";

import {
  IconCheckBorder,
  IconPencil,
  IconPromoCode,
  IconAdd,
} from "@public/icons";
import * as gtm from "@utils/gtm";
import { isEmpty } from "@utils/helpers";
import { listString, listEventHotel } from "@utils/constants";

import PromoCodeModal from "@components/desktop/hotels/booking/components/PromoCodeModal";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  blockContainer: {
    width: "100%",
    borderRadius: 8,
    background: "white",
    marginTop: 12,
  },
  titleBlock: {
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
    marginBottom: 10,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  iconPromoCode: {
    padding: "18px 0 19px 16px",
    display: "flex",
    alignItems: "center",
  },
  subTract: {
    position: "relative",
  },
  IconCheckBorder: {},
  iconPencil: {
    stroke: theme.palette.blue.blueLight8,
    margin: "0 24px",
    cursor: "pointer",
  },
  iconAdd: {
    stroke: theme.palette.white.main,
    fill: theme.palette.blue.blueLight8,
    margin: "0 38px",
    cursor: "pointer",
    width: 24,
    height: 24,
  },
  wrapIconSubtract: {
    display: "flex",
    alignItems: "center",
  },
  subtract: {
    height: "fit-content",
    borderRadius: 4,
    border: "1px solid #48BB78",
    width: "fit-content",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    wordBreak: "break-all",
    borderLeftColor: "transparent",
  },
  promo: {
    fontWeight: 600,
    fontSize: 14,
    lineHeight: "16px",
    color: theme.palette.green.greenLight7,
    textAlign: "center",
    marginRight: 10,
  },
  iconRadio: {
    borderRadius: "50%",
    width: 20,
    height: 20,
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
  },
  wrapBorder: {
    display: "flex",
    flexDirection: "column",
    position: "absolute",
    left: -1,
  },
  circle: {
    width: 6,
    height: 6,
    transform: "rotate(40deg)",
    borderRadius: "50%",
    borderLeftColor: "transparent",
    borderBottomColor: "transparent",
    background: "transparent",
    border: "1px solid #48BB78",
  },
}));

const PromotionInfo = ({
  listPromotion = [],
  promotionActive = {},
  handleSetVoucher = () => {},
  handleUseEnterCode = () => {},
  open,
  setOpen,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const handleSelectPromo = (item) => {
    gtm.addEventGtm(listEventHotel.HotelAddPromo);
    handleSetVoucher(item);
  };

  const handleOpen = () => {
    gtm.addEventGtm(listEventHotel.HotelViewListPromo);
    setOpen(true);
  };
  const handleClose = () => setOpen(false);

  return (
    <>
      <Box className={classes.blockContainer}>
        <Box className={classes.titleBlock}>
          <Box className={classes.iconPromoCode}>
            <IconPromoCode style={{ marginRight: 12 }} />{" "}
            {listString.IDS_TEXT_VOUCHER}
          </Box>

          <Box className={classes.subTract}>
            <Box className={classes.wrapIconSubtract}>
              {!isEmpty(promotionActive.code) && (
                <Box style={{ position: "relative", overflow: "hidden" }}>
                  <Box className={classes.subtract}>
                    <Box className={classes.wrapBorder}>
                      {Array(5)
                        .fill(0)
                        .map((it, index) => (
                          <Box key={index} className={classes.circle} />
                        ))}
                    </Box>
                    <Box width="16px" height="16px" style={{ margin: 8 }}>
                      {" "}
                      <IconCheckBorder className={classes.IconCheckBorder} />
                    </Box>
                    <Box className={classes.promo}>
                      <Box component="span" className={classes.textPromo}>
                        {promotionActive.code}
                      </Box>
                    </Box>
                  </Box>
                </Box>
              )}

              <Box
                onClick={() => {
                  handleOpen();
                }}
              >
                {!isEmpty(promotionActive) ? (
                  <IconPencil className={`svgFillAll ${classes.iconPencil}`} />
                ) : (
                  <Box className={classes.iconButton}>
                    <IconAdd className={`svgFillAll ${classes.iconAdd}`} />
                  </Box>
                )}
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{ timeout: 500 }}
      >
        <Fade in={open}>
          <PromoCodeModal
            bgCircle={theme.palette.gray.grayLight22}
            hasBorder={false}
            listPromotion={listPromotion}
            promotionActive={promotionActive}
            handleSetVoucher={handleSelectPromo}
            bgCircle={theme.palette.gray.grayLight22}
            handleClose={handleClose}
            handleUseEnterCode={handleUseEnterCode}
          />
        </Fade>
      </Modal>
    </>
  );
};

export default PromotionInfo;
