import { useEffect, useState } from "react";
import { Box, Dialog, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Link from "@src/link/Link";
import { IconClock } from "@public/icons";
import { listString, routeStatic } from "@utils/constants";
import ButtonComponent from "@src/button/Button";
import { isEmpty } from "@utils/helpers";
import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  timeBlock: {
    width: "100%",
    background: "#FFBC39",
    borderRadius: 8,
    padding: 8,
    textAlign: "center",
    display: "flex",
    justifyContent: "center",
  },
  textItem: {
    display: "flex",
    alignItems: "center",
    fontSize: 14,
    lineHeight: "17px",
  },
  dialogContent: {
    width: 428,
    padding: "24px 12px 12px 12px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  buttonGroup: { display: "flex", width: "100%" },
  textTitleDialog: {
    padding: "0 12px",
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
    margin: "24px 0 8px 0",
    textAlign: "center",
  },
  textDesDialog: {
    padding: "0 12px",
    fontSize: 14,
    lineHeight: "22px",
    margin: "0 0 12px 0",
    textAlign: "center",
  },
  linkDetail: {
    textDecoration: "none",
    width: "100%",
    margin: "0 12px 12px",
    "&:hover": { textDecoration: "none" },
  },
  iconTimeOut: {
    width: 241,
    height: 210,
  },
}));

const TimeBooking = ({ fetRoomRate, hotelDetail = {}, paramsInit = {} }) => {
  const classes = useStyles();
  const [time, setTime] = useState(15 * 60);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (time > 0) setTimeout(() => setTime(time - 1), 1000);
    else setOpen(true); // eslint-disable-next-line
  }, [time]);
  const getLinkInfo = () => {
    return {
      pathname: routeStatic.HOTEL_DETAIL.href,
      query: {
        alias: `${hotelDetail.id}-${
          !isEmpty(hotelDetail.name) ? hotelDetail.name.stringSlug() : ""
        }.html`,
        checkIn: paramsInit.checkIn,
        checkOut: paramsInit.checkOut,
        adults: Number(paramsInit.adults),
        rooms: Number(paramsInit.rooms),
        children: Number(paramsInit.children),
      },
    };
  };
  const min = (time - (time % 60)) / 60;
  const sec = Number(time - min * 60);
  return (
    <>
      <Box className={classes.timeBlock}>
        <Box style={{ display: "flex", alignItems: "center" }}>
          <IconClock className="svgFillAll" style={{ stroke: "#1A202C" }} />
          <span style={{ marginLeft: 10, fontSize: 14, lineHeight: "17px" }}>
            Thời gian hoàn tất thanh toán {min}:{sec > 9 ? sec : `0${sec}`}
          </span>
        </Box>
      </Box>
      <Dialog
        onClose={() => setOpen(false)}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <Box className={classes.dialogContent}>
          <Image
            srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_time_out.svg"
            className={classes.iconTimeOut}
          />
          <Typography variant="body2" className={classes.textTitleDialog}>
            {listString.IDS_MT_TIME_BOOKING_OUT_TEXT}
          </Typography>
          <Typography variant="body2" className={classes.textDesDialog}>
            {listString.IDS_MT_TIME_BOOKING_OUT_DESCRIPTION}
          </Typography>
          <Box className={classes.buttonGroup}>
            <Link
              color="inherit"
              href={{ ...getLinkInfo() }}
              className={classes.linkDetail}
            >
              <ButtonComponent
                backgroundColor="#E2E8F0"
                color="#1A202C"
                height={48}
                fontSize={16}
                fontWeight={600}
                borderRadius={8}
                handleClick={() => {
                  setOpen(false);
                }}
              >
                {listString.IDS_MT_SELECT_OTHER_ROOM}
              </ButtonComponent>
            </Link>
            <ButtonComponent
              backgroundColor="#FF1284"
              color="#FFFFFF"
              height={48}
              fontSize={16}
              fontWeight={600}
              borderRadius={8}
              style={{ margin: "0 12px 12px" }}
              handleClick={() => {
                setOpen(false);
                setTime(15 * 60);
                fetRoomRate();
              }}
            >
              {listString.IDS_MT_CONTINUE_BOOKING}
            </ButtonComponent>
          </Box>
        </Box>
      </Dialog>
    </>
  );
};

export default TimeBooking;
