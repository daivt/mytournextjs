import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Vote from "@components/common/desktop/vote/Vote";
import moment from "moment";
import { formatDate } from "utils/constants";
import { IconStar } from "@public/icons";
import Image from "@src/image/Image";
import { isEmpty } from "@utils/helpers";

const useStyles = makeStyles((theme) => ({
  blockContainer: {
    width: "100%",
    borderRadius: 8,
    padding: 24,
    background: "white",
    marginTop: 12,
    display: "flex",
  },
  titleBlock: {
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
    marginBottom: 10,
  },
  infoGroup: { display: "flex", marginTop: 16 },
  infoItem: {
    display: "flex",
    flexDirection: "column",
    borderLeft: "solid 1px #E2E8F0",
    padding: "0 10px",
    marginRight: 30,
  },
  titleInfo: { fontSize: 14, lineHeight: "17px", fontWeight: 600 },
  textInfo: { fontSize: 14, lineHeight: "17px", marginTop: 6 },
  wrapRatingStar: {
    marginTop: 5,
    marginBottom: 7,
    display: "flex",
  },
  imgHotel: {
    width: 112,
    height: 112,
    marginRight: 16,
  },
}));
const arrStar = [1, 2, 3, 4, 5];
const HotelInfo = ({ hotelDetail = {}, roomRate = {}, paramsInit = {} }) => {
  const classes = useStyles();

  return (
    <>
      <Box className={classes.blockContainer}>
        <Box className={classes.imgHotel}>
          <Image
            srcImage={
              !isEmpty(hotelDetail.thumbnail) &&
              !isEmpty(hotelDetail.thumbnail.image) &&
              hotelDetail.thumbnail.image.small
            }
            style={{ borderRadius: 8 }}
          />
        </Box>
        <Box>
          <Typography variant="body2" className={classes.titleBlock}>
            {hotelDetail.name}
          </Typography>
          <Box className={classes.wrapRatingStar}>
            {!isEmpty(hotelDetail.starNumber) && (
              <Vote
                maxValue={hotelDetail.starNumber}
                value={hotelDetail.starNumber}
              />
            )}
          </Box>
          <Box className={classes.infoGroup}>
            <Box className={classes.infoItem}>
              <Typography variant="body2" className={classes.titleInfo}>
                Nhận phòng
              </Typography>
              <Typography variant="body2" className={classes.textInfo}>
                {paramsInit.checkIn}
              </Typography>
            </Box>
            <Box className={classes.infoItem}>
              <Typography variant="body2" className={classes.titleInfo}>
                Trả phòng
              </Typography>
              <Typography variant="body2" className={classes.textInfo}>
                {paramsInit.checkOut}
              </Typography>
            </Box>
            <Box className={classes.infoItem}>
              <Typography variant="body2" className={classes.titleInfo}>
                Số đêm
              </Typography>
              <Typography variant="body2" className={classes.textInfo}>
                {Math.abs(
                  moment(paramsInit.checkIn, formatDate.firstDay)
                    .startOf("day")
                    .diff(
                      moment(paramsInit.checkOut, formatDate.firstDay).startOf(
                        "day"
                      ),
                      "days"
                    )
                )}
              </Typography>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default HotelInfo;
