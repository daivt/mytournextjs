import BootstrapTooltip from "@components/common/bootstrapTooltip/BootstrapTooltip";
import { Box, makeStyles, Typography } from "@material-ui/core";
import { IconArrowDownSmall } from "@public/icons";
import { listIcons, listString, prefixUrlIcon } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import { useState } from "react";
import Image from "@src/image/Image";
const useStyles = makeStyles((theme) => ({
  wrapContainer: {
    display: "flex",
    flexDirection: "column",
    fontSize: 14,
    color: theme.palette.black.black3,
  },
  itemPrice: {
    fontSize: 14,
    fontWeight: 400,
    display: "flex",
    justifyContent: "space-between",
    lineHeight: "16px",
    borderBottom: `1px solid ${theme.palette.gray.grayLight22}`,
    padding: "14px 0 12px",
    position: "relative",
  },
  totalPrice: {
    display: "flex",
    justifyContent: "space-between",
    lineHeight: "17px",
    fontWeight: 600,
    paddingTop: 14,
  },
  promo: {
    border: `1px solid ${theme.palette.gray.grayLight24}`,
    padding: "4px 6px",
    marginLeft: 8,
    borderRadius: 8,
  },
  textPromo: {
    fontSize: 14,
    lineHeight: "17px",
    display: "flex",
  },
  wrapPromo: {
    display: "flex",
    alignItems: "center",
    marginTop: 3,
  },
  discountPercent: {
    background: theme.palette.pink.main,
    color: theme.palette.white.main,
    fontSize: 12,
    lineHeight: "14px",
    fontWeight: 600,
    padding: "1px 3px",
    borderRadius: 4,
  },
  priceBeforePromo: {
    fontSize: 12,
    fontWeight: 400,
    lineHeight: "14px",
    color: theme.palette.gray.grayDark7,
    textDecoration: "line-through",
    marginLeft: 6,
  },
  priceInfo: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    textAlign: "right",
  },
  includedVat: {
    fontSize: 12,
    lineHeight: "14px",
    color: theme.palette.gray.grayDark7,
    fontWeight: 400,
    marginTop: 2,
    paddingBottom: 14,
  },

  IconArrowUp: {
    stroke: theme.palette.blue.blueLight8,
    transform: "rotate(180deg)",
    cursor: "pointer",
    margin: 6,
  },
  IconArrowDown: {
    stroke: theme.palette.blue.blueLight8,
    cursor: "pointer",
    margin: 6,
  },
  customTooltip: {
    background: theme.palette.green.greenLight7,
    padding: 8,
    borderRadius: 8,
    fontSize: 14,
    lineHeight: "17px",
    margin: "-37px -110px",
    position: "absolute",
  },
  arrowTooltip: {
    color: theme.palette.green.greenLight7,
  },
  piggy: {
    background: "#FFE7F3",
    color: "#FF1284",
    borderRadius: 8,
    alignItems: "center",
    display: "flex",
    marginBottom: 24,
  },
  iconPiggyDollar: {
    width: 20,
    height: 20,
  },
}));
const InfoPrice = ({
  promotionActive = {},
  promotionDiscount = {},
  rates = {},
  numRooms = 0,
  countNight = 0,
  feePayment = {},
  applyVoucher = {},
  showTooTipPromotion = false,
  statusTooltipAddPromotion = {},
}) => {
  const classes = useStyles();

  const [collapse, setCollapse] = useState(false);

  const priceDetail = !isEmpty(rates) ? rates.priceDetail : {};
  const occupancies = !isEmpty(priceDetail) ? priceDetail.occupancies : [];

  const totalPrice = () => {
    if (numRooms && rates?.price && countNight) {
      return numRooms * countNight * rates.price;
    } else {
      return 0;
    }
  };

  // Thuế và phí dịch vụ khách sạn
  const vatPrice = () => {
    return totalPrice() - nightPrice() + getDiscount() - totalSurcharge();
  };

  // Phụ Phí
  const totalSurcharge = () => {
    return occupancies
      .map((el) => el.totalSurcharge)
      .reduce((a, b) => a + b, 0);
  };

  // Giảm giá Markup
  const getDiscount = () => {
    return Math.abs(priceDetail?.discount) || 0;
  };

  // Tổng giá các đêm
  const nightPrice = () => {
    if (numRooms && rates?.basePrice && countNight) {
      return numRooms * countNight * rates?.basePrice;
    } else {
      return 0;
    }
  };

  // Tổng giá sau khi áp dụng mã khuyến mãi
  const totalPriceAfterDiscount = () => {
    if (!isEmpty(promotionDiscount) && !isEmpty(feePayment)) {
      return totalPrice() - (promotionDiscount.discount || 0) + feePayment;
    } else if (
      !isEmpty(feePayment) &&
      feePayment > 0 &&
      isEmpty(promotionDiscount)
    ) {
      return totalPrice() + feePayment;
    } else {
      return totalPrice();
    }
  };
  //Piggy save money
  const piggySaveMoney = () => {
    let discount = 0;
    if (!isEmpty(promotionDiscount) && getDiscount() && getDiscount() > 0) {
      discount = promotionDiscount.discount + getDiscount();
    } else if (!isEmpty(promotionDiscount)) {
      discount = promotionDiscount.discount;
    } else {
      discount = getDiscount();
    }
    return discount;
  };

  return (
    <Box className={classes.wrapContainer}>
      <Box className={classes.itemPrice}>
        <Box component="span" display="flex">
          {`${numRooms} phòng x ${countNight} đêm`}
          <IconArrowDownSmall
            className={`svgFillAll ${
              collapse ? classes.IconArrowUp : classes.IconArrowDown
            }`}
            onClick={() => setCollapse(!collapse)}
          />
        </Box>
        <Box component="span" className={classes.priceInfo}>
          <Box component="span">{`${nightPrice().formatMoney()}đ`}</Box>
          {!isEmpty(rates?.basePromotionInfo) && (
            <Box className={classes.wrapPromo}>
              <Box className={classes.discountPercent}>
                {`-${rates?.basePromotionInfo?.discountPercentage}%`}
              </Box>
              <Box className={classes.priceBeforePromo}>
                {`${(
                  rates?.basePromotionInfo?.priceBeforePromotion *
                  numRooms *
                  countNight
                ).formatMoney()}đ`}
              </Box>
            </Box>
          )}
        </Box>
      </Box>

      {collapse && (
        <Box
          className={classes.itemPrice}
          style={{ background: "#EDF2F7", padding: 12 }}
        >
          <Box component="span">1 phòng x 1 đêm</Box>
          <Box display="flex" flexDirection="column" alignItems="flex-end">
            {`${rates?.basePrice?.formatMoney()}đ`}
            {!isEmpty(rates?.basePromotionInfo) && (
              <Box className={classes.priceBeforePromo}>
                {`${rates?.basePromotionInfo?.priceBeforePromotion?.formatMoney()}đ`}
              </Box>
            )}
          </Box>
        </Box>
      )}

      {totalSurcharge() > 0 && (
        <Box className={classes.itemPrice}>
          <Box component="span">{`Phụ phí`}</Box>
          {`${totalSurcharge().formatMoney()}đ`}
        </Box>
      )}

      {getDiscount() > 0 && (
        <Box className={classes.itemPrice} color="green.greenLight7">
          <Box
            component="span"
            style={{ fontWeight: 600 }}
          >{`Chúng tôi khớp giá, giảm thêm`}</Box>
          {`-${getDiscount().formatMoney()}đ`}
        </Box>
      )}

      {vatPrice() > 2 && (
        <Box className={classes.itemPrice}>
          <Box component="span">{`${
            rates.includedVAT ? "Thuế và phí " : "Phí "
          }dịch vụ khách sạn`}</Box>
          {`${vatPrice().formatMoney()}đ`}
        </Box>
      )}

      {!isEmpty(promotionActive.code) && applyVoucher && (
        <Box
          className={classes.itemPrice}
          fontWeight={600}
          style={{ borderBottom: "4px solid #EDF2F7" }}
        >
          <Box component="span">{`Tổng giá phòng`}</Box>
          <Box component="span" style={{ textAlign: "right" }}>
            {`${totalPrice().formatMoney()}đ`}
          </Box>
        </Box>
      )}

      {!isEmpty(feePayment) && feePayment > 0 && (
        <Box className={classes.itemPrice}>
          <Box component="span">Phụ phí thanh toán</Box>
          {`${feePayment.formatMoney()}đ`}
        </Box>
      )}

      {!isEmpty(promotionActive.code) && applyVoucher && (
        <BootstrapTooltip
          title={
            <Box component="span">
              Đã áp dụng mã
              <Box
                component="span"
                fontWeight={600}
              >{`  ${promotionActive?.code?.toUpperCase()} - giảm ${promotionDiscount?.discount?.formatMoney()} đ`}</Box>
            </Box>
          }
          placement="top"
          classes={{
            tooltip: classes.customTooltip,
            arrow: classes.arrowTooltip,
          }}
          open={showTooTipPromotion === statusTooltipAddPromotion.SET}
        >
          <Box className={classes.itemPrice}>
            <Box component="span" className={classes.wrapPromo}>
              {listString.IDS_TEXT_VOUCHER}{" "}
              <Box component="span" className={classes.promo}>
                <Typography
                  component="span"
                  className={classes.textPromo}
                  style={{
                    color: "#1A202C",
                  }}
                >
                  {promotionActive?.code?.toUpperCase()}
                </Typography>
              </Box>
            </Box>

            <Box display="flex" alignItems="center" color="blue.blueLight8">
              {promotionActive?.code && (
                <Typography
                  component="span"
                  className={classes.textPromo}
                  style={{
                    color: "#48BB78",
                  }}
                >
                  {!isEmpty(promotionDiscount) &&
                    `-${promotionDiscount?.discount.formatMoney()}đ`}
                </Typography>
              )}
            </Box>
          </Box>
        </BootstrapTooltip>
      )}

      <Box className={classes.totalPrice}>
        <Box component="span">
          {listString.IDS_TEXT_ORDER_PAYMENT_TOTAL_PRICE}
        </Box>
        <Box component="span" fontSize={18} style={{ textAlign: "right" }}>
          {`${Math.max(totalPriceAfterDiscount(), 0).formatMoney()}đ`}
        </Box>
      </Box>
      <Box className={classes.includedVat}>
        {rates.includedVAT
          ? "Đã bao gồm thuế, phí, VAT"
          : "Giá chưa bao gồm VAT"}
      </Box>

      {(!isEmpty(promotionDiscount.discount) || !isEmpty(getDiscount())) &&
        piggySaveMoney() > 0 && (
          <Box className={classes.piggy}>
            <Box style={{ padding: 6 }}>
              <Image
                srcImage={`${prefixUrlIcon}${listIcons.IconPiggyDollar}`}
                className={classes.iconPiggyDollar}
              />
            </Box>
            {listString.IDS_MT_YOU_SAVE_MONEY}{" "}
            <Box style={{ fontWeight: 600 }}>
              &nbsp;{`${piggySaveMoney().formatMoney()}đ`}{" "}
            </Box>
          </Box>
        )}
    </Box>
  );
};
export default InfoPrice;
