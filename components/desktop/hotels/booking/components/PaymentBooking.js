import { useEffect } from "react";
import { Box, Typography, Divider, Radio, Accordion } from "@material-ui/core";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import { makeStyles } from "@material-ui/styles";
import clsx from "clsx";
import moment from "moment";
import { isEmpty } from "@utils/helpers";
import {
  flightPaymentMethodCode,
  listString,
  listEventHotel,
  prefixUrlIcon,
  listIcons,
} from "@utils/constants";
import {
  IconMethodBankTransfer,
  IconQRPay,
  IconATM,
  IconCheck,
} from "@public/icons";
import ButtonComponent from "@src/button/Button";
import * as gtm from "@utils/gtm";

const useStyles = makeStyles((theme) => ({
  blockContainer: {
    width: "100%",
    borderRadius: 8,
    padding: 24,
    background: "white",
    marginTop: 12,
  },
  titleBlock: {
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
    marginBottom: 10,
  },
  normalText: { fontSize: 14, lineHeight: "17px", color: "#4A5568" },
  paymentAction: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
  },
  paymentText: {
    background: "rgba(237, 137, 54, 0.1)",
    fontSize: 14,
    lineHeight: "22px",
    color: "#ED8936",
    padding: 12,
    borderRadius: 8,
    marginBottom: 16,
  },
  freeCancelText: {
    fontSize: 14,
    lineHeight: "17px",
    color: "#48BB78",
    fontWeight: 600,
    textTransform: "uppercase",
    marginTop: 40,
  },
  subTitleBlock: {
    fontSize: 14,
    lineHeight: "17px",
    color: "#48BB78",
    marginBottom: 12,
  },
  itemWrapper: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    cursor: "pointer",
    margin: "0 -9px 12px -4px",
  },
  subItemWrapper: {
    display: "flex",
    flexDirection: "column",
    margin: "0 -9px 12px -4px",
  },
  itemLeft: { display: "flex", alignItems: "center" },
  itemTextPayment: {
    fontSize: 16,
    lineHeight: "19px",
    color: "#1A202C",
    marginLeft: 12,
  },
  iconRadio: {
    borderRadius: "50%",
    width: 20,
    height: 20,
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
  },
  checkedIconRadio: {
    backgroundColor: "#00B6F3",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: 20,
      height: 20,
      backgroundImage: "url(/icons/icon_checked.svg)",
      backgroundSize: "cover",
      content: '""',
    },
  },
  bankGroup: {
    display: "flex",
    flexWrap: "wrap",
    width: "100%",
    marginLeft: -4,
  },
  bankItem: {
    width: "calc(20% - 8px)",
    height: 52,
    padding: 10,
    border: "1px solid #E2E8F0",
    borderRadius: 8,
    margin: 4,
    cursor: "pointer",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    "&:hover": { border: "1px solid #00B6F3" },
  },
  activeItem: {
    border: "1px solid #00B6F3",
    "&:hover": { border: "1px solid #00B6F3" },
  },
  radioInfo: {
    "& > .MuiAccordionSummary-expandIcon.Mui-expanded": {
      transform: "rotate(0deg) ",
    },
    "& > .MuiAccordionSummary-content.Mui-expanded": { margin: 0 },
    "& > .MuiAccordionSummary-content": { margin: 0 },
  },
  collapseContainer: {
    boxShadow: "none",
    borderRadius: 8,
    "&:before": { height: 0 },
    "&.Mui-expanded": { minHeight: 40, margin: 0 },
    "& > .MuiAccordionSummary-root": { padding: 0, minHeight: 0 },
  },
  textPaymentGroup: {
    display: "flex",
    flexDirection: "column",
  },
  freePaymentText: {
    marginLeft: 12,
    marginTop: 6,
    color: "#718096",
    fontSize: 12,
    lineHeight: "14px",
  },
  oneAllotment: {
    fontSize: 14,
    lineHeight: "17px",
    color: "#E53E3E",
    marginTop: 40,
  },
}));

const PaymentBooking = ({
  hotelDetail = {},
  paymentMethods = [],
  handleSubmit,
  valuesForm = {},
  setFieldValue,
  isRequestPrice = false,
  roomRate = {},
  loading = false,
  promotionDiscount = {},
  setFeePayment,
}) => {
  const classes = useStyles();
  const checkIcon = (className) => (
    <span className={clsx(className, classes.checkedIconRadio)} />
  );
  const getIcon = (code) => {
    if (code === flightPaymentMethodCode.QR_CODE) return <IconQRPay />;
    if (code === flightPaymentMethodCode.ATM) return <IconATM />;
    if (code === flightPaymentMethodCode.VISA_MASTER)
      return (
        <img
          src={`${prefixUrlIcon}${listIcons.IconMethodVisa}`}
          alt="logo_payment_visa"
          style={{ width: 32, height: 32 }}
        />
      );
    if (
      code === flightPaymentMethodCode.BANK_TRANSFER ||
      code === flightPaymentMethodCode.BANK_TRANSFER_2
    )
      return <IconMethodBankTransfer />;
    return <IconMethodBankTransfer />;
  };
  const getSubPayment = (code, bankList) => {
    let result = null;
    const subText = (
      <>
        <Typography
          variant="body2"
          className={classes.normalText}
          style={{ display: "flex", margin: "10px 0" }}
        >
          <IconCheck /> &nbsp;
          {listString.IDS_MT_TEXT_PAYMENT_METHOD_ATM_DES}
        </Typography>
        <Typography
          variant="body2"
          className={classes.normalText}
          style={{ display: "flex", marginBottom: 10 }}
        >
          <IconCheck /> &nbsp;
          {listString.IDS_MT_VN_PAY_GATE}
        </Typography>
        <Typography
          variant="body2"
          className={classes.normalText}
          style={{ display: "flex", marginBottom: 10 }}
        >
          <IconCheck /> &nbsp;
          {listString.IDS_MT_CONFIRM_VIA_EMAIL_SMS}
        </Typography>
      </>
    );
    const banks = (
      <>
        {!isEmpty(bankList) && (
          <Box className={classes.bankGroup}>
            {bankList.map((el) => (
              <Box
                key={el.id}
                className={clsx(
                  classes.bankItem,
                  valuesForm.bankId === el.id && classes.activeItem
                )}
                onClick={() => setFieldValue("bankId", el.id)}
              >
                <img
                  src={el.logoUrl}
                  style={{
                    width: "auto",
                    height: "auto",
                    maxWidth: 94,
                    maxHeight: 28,
                  }}
                  alt=""
                />
              </Box>
            ))}
          </Box>
        )}
      </>
    );
    if (code === flightPaymentMethodCode.ATM)
      result = (
        <>
          <Typography
            variant="body2"
            className={classes.itemTextPayment}
            style={{ marginLeft: 0, marginBottom: 12, fontWeight: 600 }}
          >
            {listString.IDS_MT_TEXT_SELECT_BANK}
          </Typography>
          {banks}
          {subText}
        </>
      );
    else if (code === flightPaymentMethodCode.VISA_MASTER)
      result = (
        <>
          <Typography
            variant="body2"
            className={classes.itemTextPayment}
            style={{ marginLeft: 0, marginBottom: 12 }}
          >
            Loại thẻ:
          </Typography>
          {banks}
          {subText}
        </>
      );
    else if (
      code === flightPaymentMethodCode.BANK_TRANSFER ||
      code === flightPaymentMethodCode.BANK_TRANSFER_2
    ) {
      result = (
        <>
          <Typography
            variant="body2"
            style={{ margin: "0 0 12px 0", fontSize: 14, lineHeight: "22px" }}
          >
            Hướng dẫn thanh toán sẽ được gửi tới email{" "}
            {valuesForm.email ? <b>{valuesForm.email}</b> : "đặt phòng"}. Vui
            lòng thanh toán trước
            <span style={{ color: "red" }}>
              &nbsp;
              {moment(new Date().getTime() + 2 * 3600000).format(
                "HH:mm DD/MM/YYYY"
              )}
              .
            </span>
            <b> Quá thời hạn thanh toán trên, đơn phòng sẽ tự động bị hủy.</b>
          </Typography>
          <Typography
            variant="body2"
            className={classes.itemTextPayment}
            style={{ marginLeft: 0, marginBottom: 12, fontWeight: 600 }}
          >
            {listString.IDS_MT_TEXT_SELECT_BANK}
          </Typography>
          {banks}
        </>
      );
    }

    return result ? (
      <Box className={classes.subItemWrapper}>{result}</Box>
    ) : null;
  };
  const getPaymentFee = (item) => {
    return (
      item.fixedFee +
      (item.percentFee *
        (item.amount -
          (!isEmpty(promotionDiscount) && !isEmpty(promotionDiscount.discount)
            ? promotionDiscount.discount
            : 0))) /
        100
    );
  };
  useEffect(() => {
    const temp = (paymentMethods || []).find(
      (v) => v.id === valuesForm.paymentMethodId
    );
    if (temp) setFeePayment(getPaymentFee(temp)); // eslint-disable-next-line
  }, [promotionDiscount]);
  return (
    <>
      <Box className={classes.blockContainer}>
        {!isRequestPrice && (
          <>
            <Typography variant="body2" className={classes.titleBlock}>
              {listString.IDS_TEXT_ORDER_PAYMENT_METHOD}
            </Typography>
            <Typography variant="body2" className={classes.subTitleBlock}>
              {listString.IDS_MT_TEXT_DES_PAYMENT_METHOD}
            </Typography>
            <Divider style={{ marginBottom: 12 }} />
          </>
        )}
        {!isRequestPrice &&
          !isEmpty(paymentMethods) &&
          paymentMethods.map((item) => {
            const feePaymentMethod = getPaymentFee(item);
            return (
              <Box key={item.id}>
                <Accordion
                  className={classes.collapseContainer}
                  expanded={valuesForm.paymentMethodId === item.id}
                  onChange={(e) => {
                    setFieldValue("paymentMethodId", item.id);
                    setFeePayment(feePaymentMethod);
                    if (valuesForm.paymentMethodId !== item.id) {
                      if (
                        !isEmpty(item.bankList) &&
                        item.bankList.length === 1
                      ) {
                        setFieldValue("bankId", item.bankList[0].id);
                      } else setFieldValue("bankId", undefined);
                    }
                  }}
                >
                  <AccordionSummary
                    expandIcon={
                      <Radio
                        checkedIcon={checkIcon(classes.iconRadio)}
                        icon={<span className={classes.iconRadio} />}
                        checked={valuesForm.paymentMethodId === item.id}
                        onChange={(e) => {
                          gtm.addEventGtm(listEventHotel.HotelAddPaymentInfo);
                          setFieldValue("paymentMethodId", item.id);
                          if (valuesForm.paymentMethodId !== item.id) {
                            if (
                              !isEmpty(item.bankList) &&
                              item.bankList.length === 1
                            ) {
                              setFieldValue("bankId", item.bankList[0].id);
                            } else setFieldValue("bankId", undefined);
                          }
                        }}
                      />
                    }
                    className={classes.radioInfo}
                  >
                    <Box className={classes.itemLeft}>
                      {getIcon(item.code)}
                      <Box className={classes.textPaymentGroup}>
                        <span className={classes.itemTextPayment}>
                          {item.name}
                        </span>
                        {feePaymentMethod > 0 && (
                          <span className={classes.freePaymentText}>
                            Phụ phí {`${feePaymentMethod.formatMoney()}đ`}
                          </span>
                        )}
                      </Box>
                    </Box>
                  </AccordionSummary>
                  {!isEmpty(item.bankList) && (
                    <AccordionDetails className={classes.detailWrapper}>
                      {valuesForm.paymentMethodId === item.id &&
                        getSubPayment(item.code, item.bankList)}
                    </AccordionDetails>
                  )}
                </Accordion>
                <Divider />
              </Box>
            );
          })}

        {isRequestPrice && (
          <Typography variant="body2" className={classes.paymentText}>
            {listString.IDS_MT_PAYMENT_SUCCESS_DESCRIPTION}
          </Typography>
        )}
        <Box className={classes.paymentAction}>
          {!isEmpty(roomRate.freeCancellation) && roomRate.freeCancellation && (
            <Typography variant="body2" className={classes.freeCancelText}>
              {listString.IDS_MT_FREE_CANCEL}
            </Typography>
          )}
          {!isEmpty(roomRate.availableAllotment) &&
            roomRate.availableAllotment === 1 && (
              <Typography variant="body2" className={classes.oneAllotment}>
                {listString.IDS_MT_ONLY_ONE_ALLOTMENT}
              </Typography>
            )}
          <ButtonComponent
            type="submit"
            backgroundColor="#FF1284"
            color="#FFFFFF"
            height={48}
            fontSize={16}
            fontWeight={600}
            borderRadius={8}
            padding="14px 40px"
            style={{ margin: "12px 0", width: "auto" }}
            loading={loading}
            handleClick={handleSubmit}
          >
            {isRequestPrice
              ? listString.IDS_MT_SEND_ORDER_ROOM
              : listString.IDS_MT_TEXT_PAYMENT}
          </ButtonComponent>
          <Typography variant="body2" className={classes.normalText}>
            Bằng cách nhấn nút Thanh toán, bạn đồng ý với
          </Typography>
          <Typography variant="body2" className={classes.normalText}>
            <a
              href="/news/135157-dieu-kien-va-dieu-khoan.html"
              style={{ color: "#00B6F3" }}
              target="_blank"
            >
              Điều kiện và điều khoản
            </a>{" "}
            của chúng tôi
          </Typography>
        </Box>
      </Box>
    </>
  );
};

export default PaymentBooking;
