import * as yup from "yup";
import { Box, useTheme, Dialog, Typography } from "@material-ui/core";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import sortBy from "lodash/sortBy";
import { useSnackbar } from "notistack";
import { makeStyles } from "@material-ui/styles";
import Link from "@src/link/Link";
import { Formik, Form } from "formik";
import snackbarSetting from "@src/alert/Alert";
import {
  getRoomsRates,
  getListPromotionCode,
  createBookingsBook,
  updateBookingsBook,
  getDiscountPromotionCode,
  getHotelsRoom,
  getPaymentMethods,
  createPaymentForBooking,
} from "@api/hotels";
import { isEmpty } from "@utils/helpers";
import {
  listString,
  routeStatic,
  LAST_BOOKING_HOTEL,
  DELAY_TIMEOUT_POLLING,
  listEventHotel,
} from "@utils/constants";
import ButtonComponent from "@src/button/Button";
import * as gtm from "@utils/gtm";

import RoomInfo from "./RoomInfo";
import TimeBooking from "./TimeBooking";
import HotelInfo from "./HotelInfo";
import ContactInfo from "./ContactInfo";
import RequireInfo from "./RequireInfo";
import PromotionInfo from "./PromotionInfo";
import PaymentBooking from "./PaymentBooking";

const useStyles = makeStyles((theme) => ({
  leftWrapper: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    paddingRight: 24,
  },
  rightWrapper: {
    display: "flex",
    flexDirection: "column",
    width: 480,
    borderRadius: 8,
    border: "1px solid #EDF2F7",
    background: "#FFFFFF",
    position: "sticky",
    top: 84,
  },
  dialogContent: {
    width: 428,
    padding: "24px 12px 12px 12px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  buttonGroup: { display: "flex", width: "100%" },
  textTitleDialog: {
    padding: "0 12px",
    fontSize: 18,
    lineHeight: "21px",
    fontWeight: 600,
    margin: "12px 0 8px 0",
    textAlign: "center",
  },
  textDesDialog: {
    padding: "0 12px",
    fontSize: 14,
    lineHeight: "22px",
    margin: "12px 48px",
    textAlign: "center",
  },
  linkDetail: {
    textDecoration: "none",
    width: "100%",
    margin: "0 12px 12px",
    "&:hover": { textDecoration: "none" },
  },
}));
const statusTooltipAddPromotion = {
  NONE: "NONE",
  SET: "SET",
  DONE: "DONE",
};
const BookingContent = ({ hotelDetail = {}, paramsInit = {} }) => {
  const router = useRouter();
  const classes = useStyles();
  const theme = useTheme();
  const [roomRate, setRoomRate] = useState({});
  const [listPromotion, setListPromotion] = useState([]);
  const [promotionActive, setPromotionActive] = useState({});
  const [promotionDiscount, setPromotionDiscount] = useState({});
  const [paymentMethods, setPaymentMethods] = useState([]);
  const [promoFailed, setPromoFailed] = useState(false);
  const [isLoadPromotionFirst, setLoadPromotionFirst] = useState(true);
  const [messageError, setMessageError] = useState(null);
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [shrui, setShrui] = useState("");
  const [feePayment, setFeePayment] = useState(0);
  const [applyVoucher, setApplyVoucher] = useState(false);
  const [showTooTipPromotion, setShowTooTipPromotion] = useState(
    statusTooltipAddPromotion.NONE
  );
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  let breakPolling = true;
  const showError = (message, type) => {
    enqueueSnackbar(
      message,
      snackbarSetting((key) => closeSnackbar(key), { color: type || "error" })
    );
  };
  const getListBed = (rate = {}) => {
    let listBed = [];
    rate.bedGroups.forEach((el) => {
      let bedTemp = { shrui: el.shrui, bedInfo: el.bedInfo };
      const bedTypesTemp = [];
      el.bedTypes.forEach((item) => {
        for (let index = 0; index < item.numberOfBed; index += 1) {
          bedTypesTemp.push(item.size);
        }
      });
      bedTemp = {
        ...bedTemp,
        bedTypes: bedTypesTemp,
        numberBed: bedTypesTemp.length,
      };
      listBed.push(bedTemp);
    });
    listBed = sortBy(listBed, ["numberBed"]);
    return listBed;
  };
  const setInitShrui = (rate = {}) => {
    const listBed = getListBed(!isEmpty(rate.rates) ? rate.rates[0] : {});
    setShrui(listBed[0].shrui);
  };
  const fetPromotionCode = async (bookingCode = "") => {
    try {
      const { data } = await getListPromotionCode({
        page: 1,
        size: 10,
        bookingCode,
      });
      if (data.code === 200) {
        setListPromotion(data?.data.items);
        if (!isEmpty(data.data.items)) {
          if (!isEmpty(router.query.promotionCode)) {
            const promotionSelect = data?.data?.items.find(
              (el) =>
                el.code.toUpperCase() ===
                router.query.promotionCode.toUpperCase()
            );
            if (!isEmpty(promotionSelect)) {
              setPromotionActive(promotionSelect);
            } else {
              setPromotionActive(
                !isEmpty(data?.data?.items) ? data?.data?.items[0] : {}
              );
            }
          } else {
            setPromotionActive(
              !isEmpty(data?.data?.items) ? data?.data?.items[0] : {}
            );
          }
        }
      }
    } catch (error) {}
  };
  const fetchPaymentMethod = async () => {
    try {
      const lastBookingHotel =
        JSON.parse(localStorage.getItem(LAST_BOOKING_HOTEL)) || {};
      if (!isEmpty(lastBookingHotel)) {
        const { data } = await getPaymentMethods({
          bookingCode: lastBookingHotel.orderCode,
        });
        if (!isEmpty(data.data)) setPaymentMethods(data.data.items || []);
      }
    } catch (err) {}
  };

  const fetchDiscountPromotionCode = async (code = "", isEnterCode = false) => {
    try {
      const lastBookingHotel =
        JSON.parse(localStorage.getItem(LAST_BOOKING_HOTEL)) || {};
      if (!isEmpty(lastBookingHotel)) {
        const { data } = await getDiscountPromotionCode({
          code,
          bookingCode: lastBookingHotel.orderCode,
        });
        if (data.code === 200) {
          if (showTooTipPromotion === statusTooltipAddPromotion.NONE) {
            setShowTooTipPromotion(statusTooltipAddPromotion.SET);
            setTimeout(() => {
              setShowTooTipPromotion(statusTooltipAddPromotion.DONE);
            }, 2000);
          }
          setLoadPromotionFirst(true);
          setPromotionDiscount(data.data || {});
          setApplyVoucher(true);
          if (isEnterCode) {
            showError("Thêm mã giảm giá thành công", "success");
            setOpen(false);
            setPromotionActive({ id: data.data.id, code, type: "enter" });
          }
        } else {
          if (!isLoadPromotionFirst || isEnterCode) {
            showError(data.message);
          }
          setPromoFailed(true);
          setPromotionActive({});
          setPromotionDiscount({});
        }
      }
    } catch (error) {}
  };
  const handleSetVoucher = (item) => {
    setLoadPromotionFirst(false);
    if (item.id === promotionActive.id) {
      setPromotionActive({});
      setPromotionDiscount({});
    } else {
      setPromotionActive(item);
    }
  };

  const getDataForBooking = (rateItem = {}) => {
    const roomRatesTemp = [];
    const rates = rateItem.rates || [];
    rates.forEach((el) => {
      roomRatesTemp.push({
        shrui: !isEmpty(el.bedGroups) ? el.bedGroups[0].shrui : "",
        quantity: 1,
      });
    });
    return {
      ...paramsInit,
      roomRates: roomRatesTemp,
    };
  };
  const handleUseEnterCode = (code) => {
    fetchDiscountPromotionCode(code, true);
    // fetchPaymentMethod();
  };
  const handleErrorBooking = (data) => {
    setLoading(false);
    if (
      data.code === 402 ||
      data.code === 5101 ||
      data.code === 5102 ||
      data.code === 5103 ||
      data.code === 5104 ||
      data.code === 5108
    ) {
      // data.message && showError(data.message);
      localStorage.removeItem(LAST_BOOKING_HOTEL);
      router.reload();
    } else if (data.code === 5201 || data.code === 5202 || data.code === 5203) {
      setMessageError(data.message || "");
    } else {
      data.message && showError(data.message);
    }
  };
  const checkForBooking = async (rate = {}) => {
    try {
      const lastBookingHotel =
        JSON.parse(localStorage.getItem(LAST_BOOKING_HOTEL)) || {};
      let dataDTO = getDataForBooking(rate);
      let dataBooking = {};
      if (!isEmpty(lastBookingHotel)) {
        dataDTO = {
          ...dataDTO,
          orderToken: lastBookingHotel.orderToken,
          bookingId: lastBookingHotel.bookingId,
        };
        const { data } = await updateBookingsBook(dataDTO);
        if (data.code === 200) {
          dataBooking = data.data || {};
        } else {
          handleErrorBooking(data);
        }
      } else {
        const { data } = await createBookingsBook(dataDTO);
        if (data.code === 200) {
          dataBooking = data.data || {};
        } else {
          handleErrorBooking(data);
        }
      }
      if (!isEmpty(dataBooking)) {
        const lastBookingHotelSave = {
          bookingId: dataBooking.id,
          orderToken: dataBooking.orderToken,
          orderCode: dataBooking.orderCode,
        };
        localStorage.setItem(
          LAST_BOOKING_HOTEL,
          JSON.stringify(lastBookingHotelSave)
        );
        fetPromotionCode(dataBooking.orderCode);
        fetchPaymentMethod();
      }
    } catch (error) {}
  };

  const getRoomHotel = async () => {
    let polling = true;
    try {
      while (polling && breakPolling) {
        const { data } = await getHotelsRoom(paramsInit);
        if (data.code === 200) {
          if (data.data.completed) {
            if (isEmpty(data.data.items)) {
              setMessageError(listString.IDS_MT_EMPTY_ROOM_BOOKING);
            } else {
              const tempRate = !isEmpty(data.data.items)
                ? data.data.items[0]
                : {};
              setRoomRate(tempRate);
              setInitShrui(tempRate);
              if (!tempRate.outOfRoom) {
                checkForBooking(tempRate);
              }
            }
            polling = false;
            break;
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      polling = false;
    }
  };
  const fetRoomRate = async () => {
    try {
      const { data } = await getRoomsRates(paramsInit);
      if (data.code === 200) {
        setRoomRate(data.data || {});

        if (!data.data.outOfRoom) {
          setInitShrui(data.data);
          checkForBooking(data.data);
        } else {
          getRoomHotel(); // polling
        }
      }
    } catch (error) {}
  };
  const getRequestPrice = () => {
    let result = false;
    if (!isEmpty(roomRate.rates)) {
      const temp = roomRate.rates.find((v) => v.rateKey === paramsInit.rateKey);
      if (!isEmpty(temp)) result = temp.requestPrice;
    }
    return result;
  };

  useEffect(() => {
    fetRoomRate();
    return () => {
      breakPolling = false;
    };
  }, []);

  useEffect(() => {
    if (!isEmpty(promotionActive)) {
      fetchDiscountPromotionCode(promotionActive.code);
      // fetchPaymentMethod();
    } // eslint-disable-next-line
  }, [promotionActive]);
  const storeSchema = yup.object().shape({
    name: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_YOUR_NAME),
    email: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_EMAIL)
      .email(listString.IDS_MT_TEXT_EMAIL_INVALIDATE),
    phoneNumber: yup
      .number()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_PHONE_NUMBER),
  });

  const handleSubmit = async (values) => {
    try {
      const lastBookingHotel =
        JSON.parse(localStorage.getItem(LAST_BOOKING_HOTEL)) || {};
      const dataBooking = getDataForBooking(roomRate);
      const tempRoomRates = dataBooking.roomRates.map((el) => {
        return {
          ...el,
          shrui,
          contactName: values.contactName || values.name,
          phoneNumber: values.phoneNumber,
          specialRequest: values.specialRequest,
          contactEmail: values.email,
          noSmoking: values.noSmoking,
          isHighFloor: values.isHighFloor,
        };
      });
      if (dataBooking.rooms > 1) {
        for (let i = 1; i < paramsInit.rooms; i += 1) {
          tempRoomRates.push(tempRoomRates[0]);
        }
      }
      let dataDTO = {
        ...dataBooking,
        roomRates: tempRoomRates,
        invoiceRequest: Boolean(values.invoiceRequest),
        customer: {
          email: values.email,
          name: values.name,
          phoneNumber: values.phoneNumber,
        },
      };
      // validate bankID
      const paymentTemp = paymentMethods.find(
        (v) => v.id === values.paymentMethodId
      );
      if (
        paymentTemp &&
        !isEmpty(paymentTemp.bankList) &&
        isEmpty(values.bankId)
      ) {
        showError("Bạn vui lòng chọn ngân hàng bạn muốn chuyển khoản!");
        setLoading(false);
        return;
      }
      if (values.invoiceRequest) {
        dataDTO = {
          ...dataDTO,
          invoiceInfo: {
            companyAddress: values.companyAddress,
            companyName: values.companyName,
            taxNumber: values.taxNumber,
            customerName: values.recipientName,
            recipientName: values.recipientName,
            recipientEmail: values.recipientEmail,
            note: values.note,
            recipientAddress: values.recipientAddress,
            recipientPhone: values.phoneNumber,
          },
        };
      }
      let lastBookingHotelSave = {};
      if (isEmpty(lastBookingHotel)) {
        const { data } = await createBookingsBook({
          ...dataDTO,
          isFinal: true,
        });
        if (data.code === 200 && !isEmpty(data.data)) {
          lastBookingHotelSave = {
            bookingId: data.data.id,
            orderToken: data.data.orderToken,
            orderCode: data.data.orderCode,
          };
          // setLoading(false);
        } else {
          handleErrorBooking(data);
        }
      } else {
        const { data } = await updateBookingsBook({
          ...dataDTO,
          orderToken: lastBookingHotel.orderToken,
          bookingId: lastBookingHotel.bookingId,
          isFinal: true,
        });
        if (data.code === 200 && !isEmpty(data.data)) {
          lastBookingHotelSave = {
            bookingId: data.data.id,
            orderToken: data.data.orderToken,
            orderCode: data.data.orderCode,
          };
          // setLoading(false);
        } else {
          handleErrorBooking(data);
        }
      }

      if (!isEmpty(lastBookingHotelSave)) {
        localStorage.setItem(
          LAST_BOOKING_HOTEL,
          JSON.stringify(lastBookingHotelSave)
        );
        let temp = { bookingCode: lastBookingHotelSave.orderCode };
        if (getRequestPrice()) {
          temp = { ...temp, paymentMethodCode: "PR" };
        } else {
          temp = { ...temp, paymentMethodId: values.paymentMethodId };
        }
        if (values.bankId && !getRequestPrice()) {
          temp = { ...temp, bankId: values.bankId };
        }
        if (promotionActive.code) {
          temp = { ...temp, promotionCode: promotionActive.code };
        }
        gtm.addEventGtm(listEventHotel.HotelPurchase, {
          bookingId: lastBookingHotelSave?.bookingId,
        });

        const res = await createPaymentForBooking(temp);
        if (res.data.code === 200) {
          // setLoading(false);
          if (res.data.data.isRedirect) {
            window.location.href = res.data.data.paymentLink;
          } else {
            router.push({
              pathname: routeStatic.RESULT_PAYMENT_BOOKING.href,
              query: {
                transactionId: res.data.data.transactionId,
              },
            });
          }
        } else {
          setLoading(false);
          res.data.message && showError(res.data.message);
          if (res.data.code === 402) {
            localStorage.removeItem(LAST_BOOKING_HOTEL);
            router.back();
          }
        }
      }
    } catch (error) {
    } finally {
      // setLoading(false);
    }
  };
  const getLinkInfo = () => {
    let result = { ...paramsInit };
    if (!isEmpty(paramsInit.childrenAges)) {
      result = { ...result, childrenAges: paramsInit.childrenAges.join(",") };
    }
    return {
      pathname: routeStatic.HOTEL_DETAIL.href,
      query: {
        alias: `${hotelDetail.id}-${
          !isEmpty(hotelDetail.name) ? hotelDetail.name.stringSlug() : ""
        }.html`,
        ...result,
      },
    };
  };
  return (
    <>
      <Box className={classes.leftWrapper}>
        <TimeBooking
          fetRoomRate={fetRoomRate}
          hotelDetail={hotelDetail}
          paramsInit={paramsInit}
        />
        <HotelInfo
          hotelDetail={hotelDetail}
          roomRate={roomRate}
          paramsInit={paramsInit}
        />
        <Formik
          initialValues={{
            name: "",
            email: "",
            phoneNumber: "",
            paymentMethodId: null,
          }}
          validationSchema={storeSchema}
          onSubmit={async (values) => {}}
        >
          {({ values, setValues, errors, validateForm, setFieldValue }) => {
            return (
              <Form>
                <ContactInfo hotelDetail={hotelDetail} valueForm={values} />
                <RequireInfo
                  roomRate={roomRate}
                  hotelDetail={hotelDetail}
                  valueForm={values}
                  setFieldValue={setFieldValue}
                  bedList={
                    !isEmpty(roomRate.rates)
                      ? getListBed(roomRate.rates[0])
                      : []
                  }
                  shrui={shrui}
                  setShrui={setShrui}
                />
                <PromotionInfo
                  hotelDetail={hotelDetail}
                  listPromotion={listPromotion}
                  promotionActive={promotionActive}
                  handleSetVoucher={handleSetVoucher}
                  promotionDiscount={promotionDiscount || {}}
                  paramsInit={paramsInit}
                  handleUseEnterCode={handleUseEnterCode}
                  open={open}
                  setOpen={setOpen}
                  isLoadPromotionFirst={isLoadPromotionFirst}
                />
                <PaymentBooking
                  hotelDetail={hotelDetail}
                  paymentMethods={paymentMethods}
                  promotionDiscount={promotionDiscount || {}}
                  valuesForm={values}
                  setFieldValue={setFieldValue}
                  roomRate={!isEmpty(roomRate.rates) ? roomRate.rates[0] : []}
                  handleSubmit={async () => {
                    setLoading(true);
                    const resErrors = await validateForm();
                    if (isEmpty(resErrors)) {
                      let messError = null;
                      if (values.invoiceRequest) {
                        if (
                          isEmpty(values.taxNumber) ||
                          (!isEmpty(values.taxNumber) &&
                            isEmpty(values.taxNumber.trim()))
                        )
                          messError = "Mã số thuế không hợp lệ";
                        else if (
                          isEmpty(values.recipientName) ||
                          (!isEmpty(values.recipientName) &&
                            isEmpty(values.recipientName.trim()))
                        )
                          messError = "Tên khách hàng không hợp lệ";
                        else if (
                          isEmpty(values.companyName) ||
                          (!isEmpty(values.companyName) &&
                            isEmpty(values.companyName.trim()))
                        )
                          messError = "Tên công ty không hợp lệ";
                        else if (
                          isEmpty(values.recipientEmail) ||
                          (!isEmpty(values.recipientEmail) &&
                            isEmpty(values.recipientEmail.trim()))
                        )
                          messError = "Email không hợp lệ";
                        else if (
                          isEmpty(values.companyAddress) ||
                          (!isEmpty(values.companyAddress) &&
                            isEmpty(values.companyAddress.trim()))
                        )
                          messError = "Địa chỉ công ty không hợp lệ";
                        else if (
                          isEmpty(values.recipientAddress) ||
                          (!isEmpty(values.recipientAddress) &&
                            isEmpty(values.recipientAddress.trim()))
                        )
                          messError = "Địa chỉ người nhận không hợp lệ";
                      }
                      if (!isEmpty(messError)) {
                        showError(messError);
                        setLoading(false);
                      } else handleSubmit(values);
                    } else {
                      setLoading(false);
                      showError("Bạn cần điền đầy đủ thông tin liên hệ!");
                    }
                  }}
                  isRequestPrice={getRequestPrice()}
                  loading={loading}
                  setFeePayment={setFeePayment}
                />
              </Form>
            );
          }}
        </Formik>
      </Box>
      <Box className={classes.rightWrapper}>
        <RoomInfo
          rateDetail={roomRate || {}}
          hotelDetail={hotelDetail || {}}
          promotionActive={promotionActive || {}}
          promotionDiscount={promotionDiscount || {}}
          isRequestPrice={getRequestPrice()}
          feePayment={feePayment}
          applyVoucher={applyVoucher}
          showTooTipPromotion={showTooTipPromotion}
          statusTooltipAddPromotion={statusTooltipAddPromotion}
        />
      </Box>
      {!isEmpty(messageError) && (
        <Dialog
          aria-labelledby="simple-dialog-title"
          open={!isEmpty(messageError)}
        >
          <Box className={classes.dialogContent}>
            <Typography variant="body2" className={classes.textTitleDialog}>
              Mytour.vn
            </Typography>

            <Typography variant="body2" className={classes.textDesDialog}>
              {messageError}
            </Typography>
            <Box className={classes.buttonGroup}>
              <Link
                color="inherit"
                href={{ ...getLinkInfo() }}
                className={classes.linkDetail}
              >
                <ButtonComponent
                  backgroundColor="#FF1284"
                  color="#FFFFFF"
                  height={48}
                  fontSize={16}
                  fontWeight={600}
                  borderRadius={8}
                  handleClick={() => {
                    setMessageError(null);
                  }}
                >
                  {listString.IDS_MT_SELECT_OTHER_ROOM}
                </ButtonComponent>
              </Link>
            </Box>
          </Box>
        </Dialog>
      )}
    </>
  );
};

export default BookingContent;
