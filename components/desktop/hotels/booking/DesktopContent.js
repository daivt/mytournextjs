import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Layout from "@components/layout/desktop/Layout";
import { listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import BookingContent from "./components/BookingContent";

const useStyles = makeStyles((theme) => ({
  homeWrapper: { width: "100%", background: "#EDF2F7", color: "#1A202C" },
  homeContainer: {
    maxWidth: 1188,
    margin: "0 auto",
    padding: "24px 0",
    display: "flex",
    alignItems: "flex-start",
  },
}));
function WrapperContent({ children }) {
  const classes = useStyles();
  return (
    <Box className={classes.homeWrapper}>
      <Box className={classes.homeContainer}>{children}</Box>
    </Box>
  );
}
const Booking = ({ paramsInit = {}, hotelDetail = {} }) => {
  const classes = useStyles();
  return (
    <Layout
      isHeader
      isFooter={false}
      type="booking"
      titleHeaderBack={listString.IDS_MT_TEXT_BOOKING}
      title={`Đơn đặt phòng khách sạn ${hotelDetail.name} | Mytour.vn đảm bảo giá tốt nhất!`}
      iconOgImage={
        !isEmpty(hotelDetail.thumbnail) ? hotelDetail.thumbnail.src : ""
      }
    >
      <WrapperContent>
        <BookingContent paramsInit={paramsInit} hotelDetail={hotelDetail} />
      </WrapperContent>
    </Layout>
  );
};

export default Booking;
