import { useState, useEffect } from "react";
import { Box, Breadcrumbs, Typography } from "@material-ui/core";
import Link from "@src/link/Link";
import { makeStyles } from "@material-ui/styles";
import { useRouter } from "next/router";
import { isEmpty, checkAndChangeCheckIO } from "@utils/helpers";
import Layout from "@components/layout/desktop/Layout";
import {
  listString,
  routeStatic,
  DELAY_TIMEOUT_POLLING,
  RADIUS_GET_HOTEL_LAT_LONG,
} from "@utils/constants";
import { IconMarkerLocation } from "@public/icons";
import Image from "@src/image/Image";
import { getHotelsAvailability } from "@api/hotels";
import { dataUserVote } from "@utils/dataFake";
import ListHotel from "./components/ListHotel";
import FiltersHotel from "./components/FiltersHotel";
import RecentView from "./components/RecentView";
import MapListing from "./components/MapListing";
import FiltersMap from "./components/FiltersMap";
import Introduction from "./components/Introduction";

const useStyles = makeStyles((theme) => ({
  homeWrapper: { width: "100%", background: "#F4F8FA" },
  homeContainer: { maxWidth: 1188, margin: "0 auto" },
  homeMainLayout: { display: "flex", width: "100%", alignItems: "flex-start" },
  contentContainer: {
    display: "flex",
    alignItems: "flex-start",
    alignContent: "flex-start",
  },
  filterContainer: {
    background: "white",
    borderRadius: 8,
    color: "#1A202C",
    marginBottom: 24,
  },
  divider: { width: "100%", height: 4, background: "#EDF2F7", marginTop: 20 },
  headerHome: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 24,
  },
  locationBox: {
    display: "flex",
    alignItems: "center",
    borderRadius: 12,
    fontWeight: 600,
    fontSize: 14,
    lineHeight: "17px",
    cursor: "pointer",
  },
  wrapMap: {
    position: "relative",
  },
  marker: {
    position: "absolute",
    display: "flex",
    width: 136,
    justifyContent: "center",
    top: 16,
  },
  hotelInfoText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    width: "100%",
  },
  contentBlock: { background: "white" },
  textLink: {
    display: "flex",
    color: "#1A202C",
    fontSize: 24,
    lineHeight: "28px",
    fontWeight: 600,
    "&:hover": {
      textDecoration: "none",
    },
    justifyContent: "space-between",
  },
}));
const initFilterMap = {
  stars: [],
  ratingRanges: [],
  types: [],
  priceRanges: [{ minPrice: 0, maxPrice: 5000000 }],
};
let filterMap = initFilterMap;
let hotelListSave = {};
const Listing = ({
  paramsQuery = {},
  filtersQuery = {},
  sortOptions = [],
  location = {},
  filters = {},
  items = [],
  totalHotels = 0,
  listingTitle,
}) => {
  const router = useRouter();
  const classes = useStyles();
  const [hotelData, setHotelData] = useState({ items, total: totalHotels });
  const [isMap, setMap] = useState(router.query.type === "chain" || false);
  const [loading, setLoading] = useState(false);
  const [itemActive, setItemActive] = useState({});
  const [pageMap, setPageMap] = useState(1);
  const { page } = paramsQuery;
  let paramsLink = {
    checkIn: checkAndChangeCheckIO(paramsQuery.checkIn, "checkIn"),
    checkOut: checkAndChangeCheckIO(paramsQuery.checkOut, "checkOut"),
    adults: Number(paramsQuery.adults),
    rooms: Number(paramsQuery.rooms),
    children: Number(paramsQuery.children),
  };
  if (paramsQuery.childrenAges) {
    paramsLink = {
      ...paramsLink,
      childrenAges: paramsQuery.childrenAges.join(","),
    };
  }
  if (paramsQuery.sortBy) {
    paramsLink = { ...paramsLink, sortBy: paramsQuery.sortBy };
  }
  let paramsUrl = {
    ...paramsQuery,
    checkIn: checkAndChangeCheckIO(paramsQuery.checkIn),
    checkOut: checkAndChangeCheckIO(paramsQuery.checkOut),
    adults: Number(paramsQuery.adults),
    rooms: Number(paramsQuery.rooms),
    children: Number(paramsQuery.children),
  };
  if (!isEmpty(paramsQuery.sort)) {
    paramsUrl = { ...paramsUrl, sort: paramsQuery.sort };
    paramsLink = { ...paramsLink, sort: paramsQuery.sort };
  }
  if (!isEmpty(page)) {
    paramsUrl = { ...paramsUrl, page };
    paramsLink = { ...paramsLink, page };
  }
  if (paramsQuery.aliasCode === "ksgb") {
    paramsUrl = {
      ...paramsUrl,
      latitude: paramsQuery.latitude,
      longitude: paramsQuery.longitude,
      radius: RADIUS_GET_HOTEL_LAT_LONG,
    };
  }
  let breakPolling = true;

  const convertFilterApi = () => {
    const rates = [];
    if (!isEmpty(filtersQuery.ratingRanges)) {
      filtersQuery.ratingRanges.forEach((el) => {
        const temp = dataUserVote.find((v) => v.id === el);
        if (temp) rates.push(temp.value);
      });
    }
    return { ...filtersQuery, ratingRanges: rates };
  };
  const fetchData = async (
    param = { searchType: "location" }
    // isFirstRequest = false
  ) => {
    let polling = true;
    try {
      setLoading(true);
      while (polling && breakPolling) {
        let dataDTO = {
          page,
          filters: isMap ? filterMap : convertFilterApi(),
          ...paramsUrl,
          size: 40,
        };
        if (param.searchType === "location") {
          dataDTO = { ...dataDTO, aliasCode: paramsQuery.aliasCode };
        }
        if (router.query.type === "chain") {
          if (router.query.chain) {
            dataDTO = { ...dataDTO, chainId: router.query.chain || null };
            delete dataDTO.aliasCode;
            delete dataDTO.searchType;
          } else {
            dataDTO = {
              ...dataDTO,
              aliasCode: paramsQuery.aliasCode,
              locationCode: router.query.locationCode || null,
              searchType: router.query.searchType || "location",
            };
          }
        }
        dataDTO = { ...dataDTO, ...param };
        const { data } = await getHotelsAvailability(dataDTO);
        if (data.code === 200) {
          if (!isEmpty(data.data)) {
            if (!isEmpty(data.data.items)) setLoading(false);
            if (JSON.stringify(hotelListSave) !== JSON.stringify(data.data)) {
              hotelListSave = { ...data.data };
              setHotelData(data.data);
            }
            if (data.data.completed) {
              polling = false;
              window.scrollTo({ top: 0, behavior: "smooth" });
              break;
            }
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          setLoading(false);
          polling = false;
        }
      }
    } catch (error) {
    } finally {
      setLoading(false);
      polling = false;
    }
  };
  useEffect(() => {
    if (isMap && router.query.type !== "chain") setMap(false); // eslint-disable-next-line
  }, [location.name]);
  useEffect(() => {
    fetchData({ searchType: "location" });
    return () => {
      breakPolling = false;
      hotelListSave = {};
      filterMap = initFilterMap;
    }; // eslint-disable-next-line
  }, [paramsQuery]);
  const getLinkInfo = (item) => {
    return {
      pathname: routeStatic.LISTING_HOTEL.href,
      query: {
        ...router.query,
        page: 1,
        slug: [
          `${item.aliasCode}`,
          `khach-san-tai-${item.name.stringSlug()}.html`,
        ],
      },
    };
  };
  const updateFilterMap = (val = {}) => {
    filterMap = val;
    setPageMap(1);
  };
  return (
    <Layout
      isHeader
      isFooter
      type="listing"
      location={location}
      paramsQuery={paramsQuery}
      title={`Khách sạn giá tốt ở ${
        paramsQuery.aliasCode === "ksgb" ? "gần bạn" : location.name
      } từ ${paramsUrl.checkIn} đến ${
        paramsUrl.checkOut
      } | Đảm bảo giá tốt nhất`}
      paramsUrl={paramsUrl}
    >
      <>
        <Box className={classes.homeWrapper}>
          <Box className={classes.homeContainer}>
            <Box className={classes.homeMainLayout}>
              <Box
                style={{
                  width: "100%",
                  paddingTop: 24,
                  marginLeft: isMap ? "calc(598px - 50vw)" : "auto",
                  paddingLeft: isMap ? 24 : 0,
                }}
              >
                <Box className={classes.headerHome}>
                  <Box className={classes.hotelInfoText}>
                    <Box>
                      <Breadcrumbs
                        separator="›"
                        style={{
                          marginBottom: 24,
                          fontSize: 12,
                          lineHeight: "14px",
                          fontWeight: 400,
                        }}
                      >
                        <Link
                          color="inherit"
                          href={{ pathname: routeStatic.HOTEL.href }}
                        >
                          {listString.IDS_MT_TEXT_HOTEL}
                        </Link>
                        {!isEmpty(location.details) &&
                          location.details.map((el, i) => {
                            if (i === location.details.length - 1) {
                              return (
                                <Typography
                                  color="textPrimary"
                                  key={el.id}
                                  style={{
                                    fontSize: 12,
                                    lineHeight: "14px",
                                    fontWeight: 400,
                                  }}
                                >
                                  {el.name}
                                </Typography>
                              );
                            }
                            return (
                              <Link
                                key={el.id}
                                color="inherit"
                                href={{ ...getLinkInfo(el) }}
                              >
                                {el.name}
                              </Link>
                            );
                          })}
                      </Breadcrumbs>
                      <Box
                        display="flex"
                        justifyContent="space-between"
                        alignItems="center"
                      >
                        <Link
                          href={{
                            pathname: routeStatic.LISTING_HOTEL.href,
                            query: {
                              slug: [
                                `${paramsQuery.aliasCode}`,
                                `${paramsQuery.listing}`,
                              ],
                              ...paramsLink,
                            },
                          }}
                          className={classes.textLink}
                        >
                          <Box>
                            {hotelData.total}{" "}
                            {isMap && router.query.type !== "chain"
                              ? `Khách sạn tại ${location.name}`
                              : listingTitle}
                          </Box>
                        </Link>
                        {!isMap && (
                          <Box
                            className={classes.locationBox}
                            onClick={() => {
                              setPageMap(1);
                              setMap(true);
                            }}
                          >
                            <Box className={classes.wrapMap}>
                              <Image
                                srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_map_bg.svg"
                                style={{
                                  width: 136,
                                  height: 48,
                                  position: "relative",
                                }}
                              />
                              <Box className={classes.marker}>
                                <Box style={{ width: 20, height: 20 }}>
                                  <IconMarkerLocation
                                    style={{ marginRight: 8 }}
                                  />
                                </Box>
                                {listString.IDS_MT_TEXT_MAP}
                              </Box>
                            </Box>
                          </Box>
                        )}
                      </Box>
                    </Box>
                  </Box>
                </Box>
                {isMap && (
                  <Box className={classes.contentContainer}>
                    <FiltersMap
                      filterData={filters}
                      updateFilterMap={updateFilterMap}
                      fetchData={(param) => {
                        fetchData({
                          ...param,
                          page: 1,
                          searchType: "location",
                        });
                      }}
                    />
                  </Box>
                )}
                <Box className={classes.contentContainer}>
                  <Box
                    className={classes.filterContainer}
                    style={{ width: isMap ? 0 : 279 }}
                  >
                    <FiltersHotel
                      filterData={filters}
                      isMap={isMap}
                      paramsQuery={paramsQuery}
                      location={location}
                      filtersQuery={filtersQuery}
                      paramsUrl={paramsLink}
                    />
                  </Box>
                  <ListHotel
                    paramsQuery={paramsQuery}
                    isMap={isMap}
                    hotelData={hotelData}
                    size={40}
                    page={isMap ? pageMap : page}
                    sortOptions={sortOptions}
                    loading={loading}
                    itemActive={itemActive}
                    setItemActive={setItemActive}
                    handlePagination={(newPage) => {
                      setPageMap(newPage);
                      fetchData({ page: newPage, filters: filterMap });
                    }}
                  />
                </Box>
              </Box>
              {isMap && (
                <MapListing
                  closeMap={() => {
                    setMap(false);
                    updateFilterMap({});
                    fetchData({ searchType: "location" });
                  }}
                  hotelList={hotelData.items || []}
                  location={location}
                  fetchData={(param) => {
                    fetchData({ ...param, filters: filterMap });
                  }}
                  itemActive={itemActive}
                  setItemActive={setItemActive}
                />
              )}
            </Box>
          </Box>
        </Box>
        <Box className={classes.contentBlock}>
          {/* <Introduction /> */}
          <RecentView />
        </Box>
      </>
    </Layout>
  );
};

export default Listing;
