import clsx from "clsx";
import OutsideClickHandler from "react-outside-click-handler";
import { Box, Typography, Checkbox } from "@material-ui/core";
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import RangerSliderPrice from "@src/slider/RangerSliderPrice";
import Vote from "@components/common/desktop/vote/Vote";
import { listString } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import ButtonComponent from "@src/button/Button";

const useStyles = makeStyles((theme) => ({
  divider: { height: 1, width: "100%", background: "#EDF2F7" },
  content: { padding: "0 4px", position: "relative" },
  titleContent: {
    fontSize: 14,
    lineHeight: "17px",
    border: "1px solid #A0AEC0",
    borderRadius: 8,
    padding: "8px 12px",
    cursor: "pointer",
  },
  icon: {
    borderRadius: 6,
    width: 22,
    height: 22,
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
    "$root.Mui-focusVisible &": {
      outline: "2px auto #00B6F3",
      outlineOffset: 2,
    },
  },
  checkedIconRadio: {
    backgroundColor: "#00B6F3",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: 22,
      height: 22,
      backgroundImage: "url(/icons/icon_checked.svg)",
      backgroundSize: "cover",
      content: '""',
    },
  },
  checkBoxWrapper: {
    display: "flex",
    alignItems: "center",
    margin: "0 -9px",
    cursor: "pointer",
  },
  textItem: {
    fontSize: 14,
    lineHeight: "17px",
    display: "-webkit-box",
    WebkitLineClamp: 1,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
  },
  titleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  dropDownContainer: {
    background: "white",
    border: "1px solid #E2E8F0",
    boxSizing: "border-box",
    boxShadow: "0px 8px 8px rgba(0, 0, 0, 0.1)",
    borderRadius: 8,
    padding: 16,
    width: 230,
    position: "absolute",
    zIndex: 1,
    marginTop: 12,
  },
  saveButton: { width: "100%", display: "flex", justifyContent: "flex-end" },
  titleDropDown: { fontSize: 14, lineHeight: "17px", fontWeight: 600 },
}));

const hotelStarsData = [5, 4, 3, 2, 1];
const FilterMapItem = ({
  dataItems = [],
  filters = [],
  title = "",
  type = "",
  setFilter,
  collapse,
  setCollapse,
}) => {
  const classes = useStyles();
  const checkIcon = (
    <span className={clsx(classes.icon, classes.checkedIconRadio)} />
  );
  const [value, setValue] = useState({ ...filters });
  useEffect(() => {
    if (JSON.stringify(filters) !== JSON.stringify(value)) setValue(filters); // eslint-disable-next-line
  }, [filters]);
  const changePrice = (event, newValue) => {
    setValue({
      ...value,
      priceRanges: [{ minPrice: newValue[0], maxPrice: newValue[1] }],
    });
  };
  const handleClick = (id, isChecked) => {
    if (isChecked) {
      setValue({ ...value, [type]: value[type].filter((v) => v !== id) });
    } else {
      setValue({ ...value, [type]: [...value[type], id] });
    }
  };
  const clearValue = () => setValue({ ...filters });
  const handleSubmit = () => {
    setFilter(value);
    setCollapse(null);
  };

  const data = type === "stars" ? hotelStarsData : dataItems;
  return (
    <Box className={classes.content}>
      <Box className={classes.titleContainer}>
        <Typography
          variant="body2"
          className={classes.titleContent}
          onClick={() => setCollapse(type)}
        >
          {title}
        </Typography>
      </Box>
      {collapse === type && (
        <OutsideClickHandler
          onOutsideClick={() => {
            setCollapse(null);
            clearValue();
          }}
        >
          <Box className={classes.dropDownContainer}>
            <Typography variant="body2" className={classes.titleDropDown}>
              {title}
            </Typography>
            {type === "priceRanges" && (
              <>
                <Typography
                  variant="subtitle2"
                  style={{ fontWeight: "normal", marginTop: "8px" }}
                >
                  {`${value.priceRanges[0].minPrice.formatMoney()}đ - `}
                  {`${value.priceRanges[0].maxPrice.formatMoney()}${
                    value.priceRanges[0].maxPrice === 5000000 ? "+ " : ""
                  }đ`}
                </Typography>
                <Box style={{ width: "100%", padding: "8px 14px 8px 12px" }}>
                  <RangerSliderPrice
                    valueRange={[
                      value.priceRanges[0].minPrice,
                      value.priceRanges[0].maxPrice,
                    ]}
                    handleChangeRange={changePrice}
                  />
                </Box>
              </>
            )}
            {data.map((el, i) => {
              const isChecked =
                !isEmpty(value[type]) &&
                value[type].includes(!isEmpty(el.id) ? el.id : el);
              return (
                <Box
                  key={i}
                  className={classes.checkBoxWrapper}
                  onClick={() =>
                    handleClick(!isEmpty(el.id) ? el.id : el, isChecked)
                  }
                >
                  <Checkbox
                    checked={isChecked}
                    className={classes.root}
                    color="default"
                    checkedIcon={checkIcon}
                    icon={<span className={classes.icon} />}
                  />
                  {type === "stars" ? (
                    <Vote maxValue={el} value={el} />
                  ) : (
                    <span className={classes.textItem} title={el.name}>
                      {el.name}
                    </span>
                  )}
                </Box>
              );
            })}
            <Box className={classes.saveButton}>
              <ButtonComponent
                color="white"
                width="auto"
                padding="10px 20px"
                style={{ background: "#FF1284", marginTop: 6 }}
                handleClick={handleSubmit}
              >
                <Typography
                  variant="body1"
                  component="span"
                  style={{ alignItems: "flex-end", display: "flex" }}
                >
                  {listString.IDS_MT_TEXT_SAVE}
                </Typography>
              </ButtonComponent>
            </Box>
          </Box>
        </OutsideClickHandler>
      )}
    </Box>
  );
};

export default FilterMapItem;
