import { Box, Typography } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  wrapperContent: {
    display: "flex",
    flexDirection: "column",
  },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    paddingBottom: 60,
    paddingTop: 32,
  },
  title: { fontWeight: 600, fontSize: 18, lineHeight: "21px", marginBottom: 8 },
  textItem: { fontWeight: "normal", fontSize: 14, lineHeight: "22px" },
}));

const Introduction = () => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <Box className={classes.wrapperContent}>
      <Box className={classes.content}>
        <Typography className={classes.title}>Giới thiệu đà nẵng</Typography>
        <Typography className={classes.textItem}>
          Đà Nẵng, một trong 20 thành phố sạch nhất thế giới, là điểm sáng không
          thể bỏ qua nếu bạn muốn khám phá vùng Nam Trung Bộ nước ta. Đà Nẵng, ở
          đó có sắc hoa tươi bốn mùa nở rộ, có sương mây lảng vảng bồng bềnh, có
          những cơn gió lạnh êm đềm tản mát từng làn hơi thở dịu êm. Tại nơi đó
          còn có những món ngon khó tả, ăn là ghiền, ngửi là nghiện, những món
          ăn đặc sản chắc chắn bạn không thể bỏ qua.
        </Typography>
      </Box>
    </Box>
  );
};
export default Introduction;
