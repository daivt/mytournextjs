import { addFavorite, removeFavorite } from "@api/user";
import AuthWrapper from "@components/layout/desktop/components/AuthWrapper";
import { Box, IconButton, Typography } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/styles";
import {
  IconBed,
  IconBreakFast,
  IconCheck,
  IconHeart,
  IconSightSeeing,
} from "@public/icons";
import ButtonComponent from "@src/button/Button";
import Image from "@src/image/Image";
import Link from "@src/link/Link";
import { areEqualHotelItemListing, isEmpty } from "@utils/helpers";
import cookie from "js-cookie";
import moment from "moment";
import { useSnackbar } from "notistack";
import { memo, useEffect, useState } from "react";
import {
  LAST_HOTEL_FAVORITES,
  listIcons,
  listString,
  prefixUrlIcon,
  routeStatic,
  TOKEN,
} from "utils/constants";
import Vote from "@components/common/desktop/vote/Vote";

const useStyles = makeStyles((theme) => ({
  wrapAnimation: {
    background: theme.palette.white.main,
    margin: 0,
    width: "100%",
    borderRadius: 8,
  },
  linkHotelDetail: { textDecoration: "none !important" },
  wrapImgHotel: { position: "relative", height: "100%" },
  imgHotel: { height: 180, width: "100%", borderRadius: 8, objectFit: "cover" },
  wrapHotelLoved: { position: "absolute", top: 12, right: 12 },
  iconButtonFavorite: { padding: 0 },
  wrapInfoHotel: {
    marginLeft: 16,
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
  },
  wrapHotelNames: { color: theme.palette.black.black3 },
  wrapRatingStar: {
    margin: "6px 0 8px 0",
    display: "flex",
  },
  wrapOption: { display: "flex", whiteSpace: "nowrap" },
  wrapNoOption: { padding: 0 },
  wrapTypeHotel: {
    background: "#FF1284",
    color: theme.palette.white.main,
    display: "flex",
    alignItems: "center",
    fontSize: 11,
    fontWeight: 400,
    lineHeight: "13px",
    borderRadius: 4,
    width: "fit-content",
    borderBottomRightRadius: 0,
  },
  iconTypeHotel: {
    stroke: theme.palette.white.main,
    fill: theme.palette.white.main,
  },
  point: {
    borderRadius: 4,
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 600,
    color: theme.palette.white.main,
    background: theme.palette.blue.blueLight8,
    padding: "1px 4px;",
  },
  ratingText: {
    marginLeft: 4,
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    color: theme.palette.black.black3,
  },
  countComment: {
    color: theme.palette.gray.grayDark7,
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
  },
  boxTripAdvisor: {
    height: 14,
    width: 1,
    background: theme.palette.gray.grayLight23,
    margin: "0 8px",
  },
  wrapLocation: {
    color: theme.palette.black.black3,
    fontSize: 12,
    lineHeight: "14px",
    display: "flex",
    alignItems: "center",
    marginTop: 6,
  },
  wrapBreakFast: {
    display: "flex",
    marginTop: 6,
  },
  breakFast: {
    display: "flex",
    alignItems: "center",
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
  },
  wrapRoomAndPrice: {
    fontSize: 12,
    color: theme.palette.black.black3,
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
  },
  roomName: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    textAlign: "left",
  },
  subPrice: {
    textDecorationLine: "line-through",
    color: theme.palette.gray.grayDark8,
    margin: "4px 0 2px 0",
    fontSize: 14,
    lineHeight: "16px",
  },
  mainPrice: { fontWeight: 600, fontSize: 20, lineHeight: "23px" },
  discountPercent: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 42,
    minHeight: 18,
    color: theme.palette.white.main,
    background: theme.palette.pink.main,
    borderRadius: "3px 3px 0px 3px",
    position: "relative",
    fontSize: 14,
    fontWeight: 600,
    lineHeight: "16px",
  },
  discountText: {
    padding: "2px 4px",
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 600,
  },
  triangleBottomright: {
    width: 0,
    height: 0,
    borderStyle: "solid",
    borderColor: `transparent ${theme.palette.pink.main} transparent transparent`,
    borderWidth: "0px 5px 5px 0",
    position: "absolute",
    right: 0,
    bottom: -4,
  },
  favorite: { fill: "#FF1284", stroke: theme.palette.white.main },
  notFavorite: { fill: "rgba(0, 0, 0, 0.6)", stroke: theme.palette.white.main },
  ratingVote: { color: theme.palette.yellow.yellowLight3 },

  iconNoBreakFast: { stroke: theme.palette.gray.grayDark8, marginRight: 6 },
  iconConvient: { width: 16, height: 16, marginRight: 6 },
  hotelLocation: {
    fontSize: 14,
    lineHeight: "16px",
    display: "-webkit-box",
    WebkitLineClamp: 2,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
    fontWeight: 400,
  },
  iconBreakFast: {
    width: 16,
    height: 16,
    marginRight: 6,
    stroke: theme.palette.green.greenLight7,
  },
  wrapPriceOnMobile: {
    background: theme.palette.blue.blueLight9,
    color: theme.palette.blue.blueLight8,
    display: "flex",
    alignItems: "center",
    marginTop: 6,
    borderRadius: 4,
    fontSize: 12,
    lineHeight: "14px",
  },
  discountBookingApp: {
    fontSize: 12,
    fontWeight: 600,
    color: theme.palette.blue.blueLight8,
  },
  wrapPromoCode: {
    border: `1px dashed ${theme.palette.gray.grayLight25}`,
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    marginTop: 8,
    borderRadius: 8,
    whiteSpace: "nowrap",
  },
  wrapMissAllotment: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    color: theme.palette.red.redLight5,
    background: theme.palette.red.redLight6,
    borderRadius: 8,
    width: 224,
    padding: 10,
    marginTop: 5,
  },
  discountPromoCode: {
    background: theme.palette.blue.blueLight8,
    color: theme.palette.white.main,
    padding: "2px 4px",
    borderRadius: 3,
    marginLeft: 4,
  },
  promoCode: {
    borderRadius: 3,
    border: "1px dashed rgba(72, 187, 120, 0.5)",
    background: theme.palette.green.greenLight8,
    padding: "2px 4px ",
    marginLeft: 5,
    fontWeight: 600,
  },
  pricePromo: {
    fontSize: 20,
    lineHeight: "23px",
    fontWeight: 600,
    color: theme.palette.pink.main,
    marginTop: 3,
    textAlign: "right",
  },
  wrapCode: { display: "flex", alignItems: "center", marginTop: 6 },
  wrapHotCode: {
    color: theme.palette.green.greenLight7,
    display: "flex",
    alignItems: "center",
  },
  priceOfFreeCancel: {
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    color: theme.palette.green.greenLight7,
    marginTop: 6,
  },
  wrapLoginUsePromo: { display: "flex", textAlign: "right" },
  discountMember: {
    marginTop: 6,
    fontSize: 14,
    lineHeight: "17px",
    fontStyle: "italic",
  },
  lastBooking: {
    marginTop: 10,
    borderRadius: 8,
    background: theme.palette.blue.blueLight9,
    color: theme.palette.primary.main,
    fontSize: 14,
    lineHeight: "16px",
    fontWeight: 400,
    padding: "4px 8px",
    fontSize: "14px",
    lineHeight: "16px",
    fontWeight: 400,
    justifyContent: "center",
    width: "fit-content",
  },
  tagInfoText: {
    fontSize: 12,
    fontWeight: 600,
    lineHeight: "14px",
    color: "#fff",
    background: "#E53E3E",
    borderRadius: 4,
    padding: "2px 4px",
    width: "fit-content",
    height: 20,
    display: "flex",
    alignItems: "center",
  },
  clipPath: {
    width: 15,
    height: 20,
    clipPath: "polygon(105% 0, 48% 0, 100% 100%, 102% 100%)",
    background: "#ffdced",
  },
  villa: {
    height: 20,
    fontSize: 12,
    lineHeight: "14px",
    fontWeight: 600,
    color: "#FF1284",
    padding: "3px 6px",
    marginLeft: 0,
    background: "rgba(255, 18, 132, 0.15)",
    display: "flex",
    alignItems: "center",
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    whiteSpace: "nowrap",
  },
  iconMPlus: {
    width: 63,
    height: 20,
  },
  iconTripadvisor: {
    width: 18,
    height: 18,
    marginRight: 3,
  },
}));
const hotelNameStyle = {
  fontWeight: 600,
  display: "-webkit-box",
  WebkitLineClamp: 3,
  WebkitBoxOrient: "vertical",
  overflow: "hidden",
  fontSize: 18,
  lineHeight: "21px",
  marginTop: 8,
};
const arrStar = [1, 2, 3, 4, 5];
const ItemHotelListing = ({
  item = {},
  styleHotelItem = "",
  paramsFilterInit = {},
}) => {
  const promotion = !isEmpty(item.promotions) ? item.promotions[0] : {};
  const basePromotion = !isEmpty(promotion.basePromotion)
    ? promotion.basePromotion
    : {};
  const theme = useTheme();
  const classes = useStyles();
  const [isFavorite, setIsFavorite] = useState(item.isFavoriteHotel);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  useEffect(() => {
    checkFavoriteHotel();
  }, []);

  useEffect(() => {
    setIsFavorite(item.isFavoriteHotel);
  }, [item]);

  const checkFavoriteHotel = () => {
    const listHotelFavorites =
      JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
    if (isEmpty(cookie.get(TOKEN)) && !isEmpty(listHotelFavorites)) {
      setIsFavorite(listHotelFavorites.includes(item.id));
    }
  };
  const handleFavorite = (event) => {
    event.preventDefault();
    if (!isEmpty(cookie.get(TOKEN))) {
      changeFavorite();
    } else {
      let listHotelFavoritesTemp =
        JSON.parse(localStorage.getItem(LAST_HOTEL_FAVORITES)) || [];
      if (isFavorite) {
        listHotelFavoritesTemp = listHotelFavoritesTemp.filter(
          (el) => el !== item.id
        );
      } else {
        listHotelFavoritesTemp.unshift(item.id);
      }
      localStorage.setItem(
        LAST_HOTEL_FAVORITES,
        JSON.stringify(listHotelFavoritesTemp)
      );
      setIsFavorite(!isFavorite);
    }
  };
  const changeFavorite = async () => {
    try {
      if (isFavorite) {
        const { data } = await removeFavorite({ hotelId: item.id });
        if (data.code === 200) {
          setIsFavorite(!isFavorite);
        } else {
          data?.message &&
            enqueueSnackbar(
              data?.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
        }
      } else {
        const { data } = await addFavorite({ hotelId: item.id });
        if (data.code === 200) {
          setIsFavorite(!isFavorite);
        } else {
          data?.message &&
            enqueueSnackbar(
              data?.message,
              snackbarSetting((key) => closeSnackbar(key), {
                color: "error",
              })
            );
        }
      }
    } catch (error) {}
  };
  const getLinkDetail = () => {
    let result = {
      pathname: routeStatic.HOTEL_DETAIL.href,
      query: {
        alias: `${item.id}-${item?.hotelName.stringSlug()}.html`,
        checkIn: paramsFilterInit.checkIn,
        checkOut: paramsFilterInit.checkOut,
        adults: Number(paramsFilterInit.adults),
        rooms: Number(paramsFilterInit.rooms),
        children: Number(paramsFilterInit.children),
        priceKey: item.priceKey || "",
      },
    };
    if (!isEmpty(paramsFilterInit.childrenAges)) {
      result = {
        ...result,
        query: {
          ...result.query,
          childrenAges: paramsFilterInit.childrenAges.join(","),
        },
      };
    }
    return result;
  };
  return (
    <Link
      href={{ ...getLinkDetail() }}
      target="_blank"
      className={classes.linkHotelDetail}
    >
      <Box className={classes.wrapAnimation}>
        <Box className="hotel-container" style={{ width: "100%" }}>
          <Box style={{ width: 240 }}>
            <Box className={classes.wrapImgHotel}>
              {!isEmpty(item.srcImage) && (
                <img
                  src={item.srcImage}
                  className={classes.imgHotel}
                  style={{ borderRadius: 8 }}
                />
              )}
              <Box className={classes.wrapHotelLoved}>
                <IconButton
                  onClick={handleFavorite}
                  className={classes.iconButtonFavorite}
                >
                  <IconHeart
                    className={`svgFillAll ${
                      isFavorite ? classes.favorite : classes.notFavorite
                    }`}
                  />
                </IconButton>
              </Box>
            </Box>
          </Box>
          <Box style={{ width: "calc(100% - 256px)" }}>
            <Box className={classes.wrapInfoHotel}>
              <Box style={{ minHeight: 180, minWidth: "60%" }}>
                <Box className={classes.wrapOption}>
                  {!isEmpty(item.tags) && (
                    <Box
                      style={{ background: "#FFBC39", marginRight: 8 }}
                      className={classes.tagInfoText}
                      title={item.tags[0].description}
                    >
                      {item.tags[0].name}
                    </Box>
                  )}

                  {!isEmpty(item.lastBookedTime) &&
                    moment().diff(moment(item.lastBookedTime)) <= 7200000 && (
                      <Box
                        style={{
                          marginLeft: `${!isEmpty(item.tags) ? 8 : "0"}`,
                        }}
                        className={classes.tagInfoText}
                      >
                        {listString.IDS_MT_TEXT_SALING_TOP}
                      </Box>
                    )}
                </Box>

                <Box style={hotelNameStyle} className="hotel-name">
                  {item.hotelName}
                </Box>
                {!isEmpty(item.category) && (
                  <Box display="flex" alignItems="center">
                    <Box className={classes.wrapRatingStar}>
                      {!isEmpty(item.ratingStar) && (
                        <Vote
                          maxValue={item.ratingStar}
                          value={item.ratingStar}
                        />
                      )}
                    </Box>
                    <Box
                      display="flex"
                      alignItems="center"
                      style={{ marginLeft: 12 }}
                    >
                      {item.category.code === "villa" && (
                        <>
                          <Box className={classes.wrapTypeHotel}>
                            <Box
                              display="flex"
                              alignItems="flex-end"
                              style={{ marginLeft: 6 }}
                            >
                              <Image
                                srcImage={`${prefixUrlIcon}${listIcons.IconMPlus}`}
                                className={classes.iconMPlus}
                              />
                              <Box className={classes.clipPath}></Box>
                            </Box>
                          </Box>
                          <Box className={classes.villa}> Villa, Căn hộ</Box>
                        </>
                      )}
                    </Box>
                  </Box>
                )}

                <Box
                  display="flex"
                  alignItems="center"
                  style={{ fontSize: 14, fontWeight: 400, lineHeight: "16px" }}
                >
                  <Box component="span" className={classes.point}>
                    {item.ratingPoint}
                  </Box>
                  <Box component="span" className={classes.ratingText}>
                    {item.ratingText}
                  </Box>
                  <Box
                    component="span"
                    className={classes.countComment}
                    style={{ marginLeft: 4 }}
                  >
                    {!isEmpty(item.countComment) &&
                      item.countComment > 0 &&
                      `(${item.countComment} đánh giá)`}
                  </Box>

                  {!isEmpty(item.taRating) && (
                    <>
                      <Box className={classes.boxTripAdvisor} />
                      <Image
                        srcImage={`${prefixUrlIcon}${listIcons.IconTripadvisor}`}
                        className={classes.iconTripadvisor}
                      />
                      <Box style={{ color: "#1A202C" }}>
                        {" "}
                        {2 * item.taRating === 10
                          ? 10
                          : (2 * item.taRating).toFixed(1)}
                      </Box>
                    </>
                  )}
                </Box>
                <Box component="span" className={classes.location}>
                  <Box className={classes.wrapLocation}>
                    <Box style={{ width: 16, height: 16 }}>
                      <IconSightSeeing />
                    </Box>

                    <Box marginLeft={7 / 8} className={classes.hotelLocation}>
                      {item.location}
                    </Box>
                  </Box>
                </Box>
                {!isEmpty(item.roomName) && (
                  <Box component="span" className={classes.location}>
                    <Box className={classes.wrapLocation}>
                      <Box style={{ width: 16, height: 16 }}>
                        <IconBed />
                      </Box>

                      <Box marginLeft={7 / 8} className={classes.roomName}>
                        {item.roomName}
                      </Box>
                    </Box>
                  </Box>
                )}

                <Box className={classes.wrapBreakFast}>
                  <Box className={classes.breakFast}>
                    {item.freeBreakfast ? (
                      <Box
                        component="span"
                        color="green.greenLight7"
                        display="flex"
                        alignItems="center"
                      >
                        <IconBreakFast
                          className={`svgFillAll ${classes.iconBreakFast}`}
                        />
                        {listString.IDS_MT_TEXT_ROOM_FREE_BREAK_FAST}
                      </Box>
                    ) : (
                      <Box
                        component="span"
                        color="black.black3"
                        display="flex"
                        alignItems="center"
                      >
                        <IconBreakFast
                          className={`svgFillAll ${classes.iconNoBreakFast}`}
                        />
                        {listString.IDS_MT_TEXT_ROOM_NO_FREE_BREAK_FAST}
                      </Box>
                    )}
                  </Box>
                </Box>
                {!isEmpty(item.amenities) && (
                  <Box className={classes.wrapBreakFast}>
                    <Box className={classes.breakFast}>
                      <Box
                        component="span"
                        color="black.black3"
                        display="flex"
                        alignItems="center"
                      >
                        <img
                          src={item.amenities[0].icon}
                          className={`${classes.iconConvient}`}
                        />
                        {item.amenities[0].name}
                      </Box>
                    </Box>
                  </Box>
                )}
                {!isEmpty(item.specialOffer) && (
                  <Box className={classes.wrapCode}>
                    <IconCheck style={{ marginRight: "6px" }} />
                    <Box component="span" className={classes.wrapHotCode}>
                      Mã{" "}
                      <Typography
                        variant="subtitle2"
                        component="span"
                        className={classes.promoCode}
                      >
                        {item.specialOffer.code}
                      </Typography>
                      <Box
                        style={{
                          marginLeft: "5px",
                          whiteSpace: "nowrap",
                          fontSize: "14px",
                          lineHeight: "16px",
                          fontWeight: "400",
                        }}
                      >
                        {" "}
                        giảm{" "}
                        {item.specialOffer.valueType === "absolute_value"
                          ? `${item.specialOffer.value.formatMoney()}đ`
                          : `${" "}${item.specialOffer.percent}%`}
                        <Box component="span"> đã được áp dụng</Box>
                      </Box>
                    </Box>
                  </Box>
                )}
                {!isEmpty(item.lastBookedTime) &&
                  moment().diff(moment(item.lastBookedTime)) <= 86400000 && (
                    <Box
                      className={classes.lastBooking}
                      display="flex"
                      alignItems="center"
                      justifyItems="center"
                    >
                      {`Vừa được đặt cách đây ${moment(
                        item.lastBookedTime
                      ).fromNow()}`}
                    </Box>
                  )}
                {!item.outOfRoom &&
                  item?.allotment > 0 &&
                  item?.allotment <= 5 && (
                    <Box
                      className={classes.wrapBreakFast}
                      style={{ marginTop: "10px" }}
                    >
                      <Box className={classes.breakFast}>
                        <Typography
                          variant="body2"
                          component="span"
                          style={{ color: "#E53E3E" }}
                        >
                          Chỉ còn {item?.allotment} phòng trống
                        </Typography>
                      </Box>
                    </Box>
                  )}
              </Box>
              <Box
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  alignItems: "flex-end",
                  minWidth: "40%",
                }}
              >
                <Box className={classes.wrapRoomAndPrice}>
                  {!item.hiddenPrice && (
                    <>
                      {!isEmpty(item.discountPercent) && (
                        <Box className={classes.discountPercent}>
                          <Box className={classes.discountText}>
                            {item.discountPercent}%
                          </Box>
                          <Box className={classes.triangleBottomright}></Box>
                        </Box>
                      )}
                      {!isEmpty(item.subPrice) && item.subPrice > 0 && (
                        <Box component="span" className={classes.subPrice}>
                          {item.subPrice.formatMoney()}đ /đêm
                        </Box>
                      )}
                    </>
                  )}

                  {!isEmpty(item.mainPrice) &&
                    !item.hiddenPrice &&
                    item.mainPrice >= 0 && (
                      <Box component="span" className={classes.mainPrice}>
                        {`${item.mainPrice.formatMoney()}đ `}
                        <Box
                          component="span"
                          style={{
                            fontSize: 14,
                            lineHeight: "16px",
                            fontWeight: 400,
                            color: "#4A5568",
                          }}
                        >
                          /đêm
                        </Box>
                      </Box>
                    )}

                  {!isEmpty(basePromotion) && !item.hiddenPrice && (
                    <Box className={classes.wrapPromoCode}>
                      <Box
                        style={{
                          padding: "7px 12px",
                          fontSize: "14px",
                          lineHeight: "16px",
                          fontWeight: "400",
                        }}
                      >
                        <Box flexDirection="row" display="flex">
                          {listString.IDS_MT_TEXT_TYPE_PROMO_CODE}:&nbsp;
                          <Typography
                            variant="subtitle2"
                            color="primary"
                            style={{ marginLeft: 2 }}
                          >
                            {promotion.code}
                            <Typography
                              variant="subtitle2"
                              component="span"
                              className={classes.discountPromoCode}
                            >
                              {basePromotion.valueType === "percent"
                                ? `-${basePromotion.percent}%`
                                : `-${basePromotion.value.formatMoney()}đ`}
                            </Typography>
                          </Typography>
                        </Box>

                        <Box className={classes.wrapPricePromo}>
                          <Box className={classes.pricePromo}>
                            {`${basePromotion.price.formatMoney()}đ `}
                            <Box
                              component="span"
                              style={{
                                fontSize: 14,
                                fontWeight: "400",
                                color: "pink.main",
                              }}
                            >
                              /đêm
                            </Box>
                          </Box>
                        </Box>
                      </Box>
                    </Box>
                  )}
                  {item.freeCancellation && (
                    <Box className={classes.priceOfFreeCancel}>
                      Giá gồm <b>hủy phòng miễn phí</b>
                    </Box>
                  )}
                  {item.outOfRoom && (
                    <Box component="span" className={classes.wrapMissAllotment}>
                      {listString.IDS_MT_TEXT_YOU_MISS_ALLOTMENT}
                    </Box>
                  )}

                  {item.hiddenPrice && (
                    <Box className={classes.wrapLoginUsePromo}>
                      <Typography
                        variant="subtitle1"
                        style={{ fontWeight: 400 }}
                      >
                        {listString.IDS_MT_TEXT_LOGIN_USE_PROMO}
                        <Typography
                          variant="subtitle1"
                          color="secondary"
                          style={{ marginLeft: 5 }}
                          component="span"
                        >
                          {`Giảm ${item.memberDeal}`}
                        </Typography>
                        <Box className={classes.discountMember}>
                          {listString.IDS_MT_TEXT_DISCOUNT_ON_MEMBER}
                        </Box>
                        <AuthWrapper typeModal="MODAL_LOGIN">
                          <ButtonComponent
                            backgroundColor={theme.palette.secondary.main}
                            height={40}
                            width={117}
                            fontSize={16}
                            fontWeight={600}
                            borderRadius={8}
                            style={{ marginTop: 8 }}
                          >
                            {listString.IDS_MT_TEXT_LOGIN}
                          </ButtonComponent>
                        </AuthWrapper>
                      </Typography>
                    </Box>
                  )}
                </Box>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </Link>
  );
};
export default memo(ItemHotelListing, areEqualHotelItemListing);
