import { useState } from "react";
import { Box } from "@material-ui/core";
import { useRouter } from "next/router";
import clsx from "clsx";
// import LazyLoad from "react-lazyload";
import { makeStyles } from "@material-ui/styles";
import { routeStatic } from "@utils/constants";
import { adapterHotelAvailability, isEmpty } from "@utils/helpers";
import UsePagination from "@src/pagination/Pagination";
import SkeletonItemListing from "@components/common/desktop/skeleton/SkeletonItemListing";
import NoResultHotel from "./NoResultHotel";
import ItemHotelListing from "./ItemHotelListing";

const useStyles = makeStyles((theme) => ({
  listContainer: { padding: "0 0 0 24px", width: "100%" },
  bannerImg: { width: "100%", maxHeight: 132, borderRadius: 8 },
  listHotel: { marginTop: 12 },
  typeResultContainer: {
    display: "flex",
    fontWeight: 600,
    fontSize: 14,
    lineHeight: "17px",
    borderBottom: "solid 1px #E2E8F0",
    marginBottom: 16,
  },
  typeItem: {
    borderBottom: "solid 1px #F4F8FA",
    color: "#1A202C",
    cursor: "pointer",
    paddingBottom: 10,
    marginRight: 35,
    "&:hover": {
      color: "#00B6F3",
      borderBottom: "solid 1px #00B6F3",
    },
  },
  activeItem: { color: "#00B6F3", borderBottom: "solid 1px #00B6F3" },
  paginationContainer: {
    display: "flex",
    justifyContent: "center",
    margin: "32px 0",
    "& .MuiPaginationItem-textPrimary.Mui-selected": { color: "white" },
    "& .MuiPaginationItem-page:hover": {
      background: "#E2E8F0",
      color: "#1A202C",
    },
  },
}));

const ListHotel = ({
  hotelData = {},
  paramsQuery = {},
  isMap,
  size = 40,
  page = 1,
  sortOptions = [],
  loading = false,
  itemActive,
  setItemActive,
  handlePagination,
}) => {
  const classes = useStyles();
  const router = useRouter();
  const handleSelectedItemSort = (sortCode) => {
    router.push({
      pathname: routeStatic.LISTING_HOTEL.href,
      query: { ...router.query, sortBy: sortCode, page: 1 },
    });
  };
  const handleChangePage = (newPage) => () => {
    if (isMap) handlePagination(newPage);
  };
  return (
    <Box className={classes.listContainer} style={{ padding: isMap && 0 }}>
      <Box id="banner_listing" className={classes.bannerImg} />
      <Box className={classes.listHotel} style={{ marginRight: isMap && 24 }}>
        {!loading && isEmpty(hotelData.items) ? (
          <NoResultHotel />
        ) : (
          <>
            <Box className={classes.typeResultContainer}>
              {!isEmpty(sortOptions) &&
                sortOptions.map((el) => (
                  <Box
                    key={el.code}
                    className={clsx(
                      classes.typeItem,
                      (!isEmpty(paramsQuery.sortBy)
                        ? paramsQuery.sortBy === el.code
                        : sortOptions[0].code === el.code) && classes.activeItem
                    )}
                    onClick={() => handleSelectedItemSort(el.code)}
                  >
                    {el.textDetail}
                  </Box>
                ))}
            </Box>
            {loading &&
              [1, 2, 3, 4, 5].map((el) => <SkeletonItemListing key={el} />)}
            {!loading &&
              adapterHotelAvailability(hotelData.items).map((el, i) => (
                <Box
                  key={i}
                  id={el.id}
                  style={
                    el.id === itemActive.id && isMap
                      ? {
                          margin: "12px 0",
                          border: "1px solid #00B6F3",
                          borderRadius: 8,
                        }
                      : { margin: "12px 0" }
                  }
                  onMouseOver={() => {
                    if (el.id !== itemActive.id) {
                      setItemActive(hotelData.items[i]);
                    }
                  }}
                >
                  {/* <LazyLoad overflow offset={100}> */}
                  <ItemHotelListing paramsFilterInit={paramsQuery} item={el} />
                  {/* </LazyLoad> */}
                </Box>
              ))}
            {!loading && (
              <Box className={classes.paginationContainer}>
                <UsePagination
                  pageSize={size}
                  total={hotelData.total}
                  pageNum={page}
                  className={classes.paginationContainer}
                  isLink={!isMap}
                  handleChangePage={handleChangePage}
                />
              </Box>
            )}
          </>
        )}
      </Box>
    </Box>
  );
};

export default ListHotel;
