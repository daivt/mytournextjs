import { Box, Typography } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/styles";
import { listString } from "@utils/constants";
import ButtonComponent from "@src/button/Button";
import Image from "@src/image/Image";
const useStyles = makeStyles((theme) => ({
  wrapResultSearch: {
    marginTop: 155,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
  result: {
    marginTop: 16,
    fontSize: 14,
    lineHeight: "19px",
    fontWeight: 400,
  },
  textSearchAgain: {
    marginTop: 8,
    color: theme.palette.gray.grayDark8,
    fontSize: 14,
    lineHeight: "6px",
    fontWeight: 400,
  },
  wrapBtn: {
    marginTop: 16,
  },
  imageEmpty: {
    width: 193,
    height: 170,
  },
}));

const NoResultHotel = () => {
  const classes = useStyles();
  const theme = useTheme();
  return (
    <>
      <Box className={classes.wrapResultSearch}>
        <Box className={classes.wrapIconResult}>
          <Image
            srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_no_result_search_listing.svg"
            className={classes.imageEmpty}
          />
        </Box>
        <Typography className={classes.result}>
          {listString.IDS_MT_NO_RESULT_FILTER_HOTEL}
        </Typography>
        <Typography className={classes.textSearchAgain}>
          {listString.IDS_MT_CHANGE_OPTION_FILTER}
        </Typography>
        <ButtonComponent
          className={classes.wrapBtn}
          backgroundColor={theme.palette.blue.blueLight8}
          height={40}
          fontSize={16}
          fontWeight={600}
          borderRadius={8}
          width={144}
        >
          {listString.IDS_MT_TEXT_SEARCH}
        </ButtonComponent>
      </Box>
    </>
  );
};
NoResultHotel.propTypes = {};
export default NoResultHotel;
