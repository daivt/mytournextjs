import { Box, Typography } from "@material-ui/core";
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import { listString, routeStatic } from "@utils/constants";
import { dataUserVote } from "@utils/dataFake";
import RangerSliderPrice from "@src/slider/RangerSliderPrice";
import { isEmpty } from "@utils/helpers";
import { useRouter } from "next/router";
import Link from "@src/link/Link";
import FilterItem from "./FilterItem";

const useStyles = makeStyles((theme) => ({
  filterHeader: {
    padding: "14px 16px",
    fontWeight: 600,
    fontSize: 16,
    lineHeight: "19px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  divider: { height: 1, width: "100%", background: "#EDF2F7" },
  content: { padding: 16 },
  titleContent: { fontWeight: 600, fontSize: 14, lineHeight: "17px" },
  textClearFilter: {
    fontSize: 14,
    lineHeight: "17px",
    color: "#00B6F3",
    cursor: "pointer",
  },
  textLink: {
    "&:hover": { textDecoration: "none" },
  },
}));

const defaultPrice = { minPrice: 0, maxPrice: 5000000 };
const FiltersHotel = ({
  filterData = {},
  isMap,
  paramsQuery = {},
  location = {},
  filtersQuery = {},
  paramsUrl = {},
}) => {
  const classes = useStyles();
  const router = useRouter();
  const [prices, setPrices] = useState(
    !isEmpty(filtersQuery.priceRanges)
      ? filtersQuery.priceRanges[0]
      : defaultPrice
  );
  const filters = {
    stars: filtersQuery.stars || [],
    services: filtersQuery.services || [],
    types: filtersQuery.types || [],
    locations: filtersQuery.locations || [],
    ratingRanges: filtersQuery.ratingRanges || [],
    amenities: filtersQuery.amenities || [],
  };
  const resetPrice = () => setPrices(defaultPrice);
  useEffect(() => {
    resetPrice();
  }, [location.name]);

  useEffect(() => {
    if (!isEmpty(filtersQuery.priceRanges)) {
      setPrices(filtersQuery.priceRanges[0]);
    }
  }, [filtersQuery]);
  const changePrice = (event, newValue) => {
    setPrices({ minPrice: newValue[0], maxPrice: newValue[1] });
  };
  const getMaxPrice = (max) => {
    return max >= defaultPrice.maxPrice ? defaultPrice.maxPrice : max;
  };

  const onChangeCommitted = (event, newValue) => {
    const tempParams = { minPrice: newValue[0], maxPrice: newValue[1] };
    router.push({
      pathname: routeStatic.LISTING_HOTEL.href,
      query: { ...router.query, ...tempParams, page: 1 },
    });
  };
  if (isMap) return null;
  const dataList = [
    { title: listString.IDS_MT_HOTEL_LEVEL, type: "stars" },
    { title: listString.IDS_MT_TEXT_SERVICE_ATTACH, type: "services" },
    { title: listString.IDS_MT_TEXT_USER_VOTE, type: "ratingRanges" },
    { title: listString.IDS_MT_TEXT_HOTEL_TYPE, type: "types" },
    { title: listString.IDS_MT_TEXT_AREA, type: "locations" },
    { title: listString.IDS_MT_TEXT_FILTER_CONVENIENT, type: "amenities" },
  ];
  return (
    <>
      <Box className={classes.filterHeader}>
        {listString.IDS_MT_TEXT_FILTER}
        <Link
          href={{
            pathname: routeStatic.LISTING_HOTEL.href,
            query: {
              slug: [
                `${paramsQuery.aliasCode}`,
                `khach-san-tai-${
                  !isEmpty(location.name) ? location.name.stringSlug() : ""
                }.html`,
              ],
              ...paramsUrl,
            },
          }}
          className={classes.textLink}
          onClick={resetPrice}
        >
          <Typography variant="body2" className={classes.textClearFilter}>
            {listString.IDS_MT_CLEAR_FILTER}
          </Typography>
        </Link>
      </Box>
      <Box className={classes.divider} />
      <Box className={classes.content}>
        <Typography variant="body2" className={classes.titleContent}>
          {listString.IDS_MT_RANGE_PRICE}
        </Typography>
        <Typography
          variant="subtitle2"
          style={{ fontWeight: "normal", marginTop: "8px" }}
        >
          {`${prices.minPrice.formatMoney()}đ - `}
          {`${getMaxPrice(prices.maxPrice).formatMoney()}${
            prices.maxPrice >= defaultPrice.maxPrice ? "+ " : ""
          }đ`}
        </Typography>
        <Box style={{ width: "100%", paddingRight: 14 }}>
          <RangerSliderPrice
            valueRange={[prices.minPrice, getMaxPrice(prices.maxPrice)]}
            handleChangeRange={changePrice}
            onChangeCommitted={onChangeCommitted}
          />
        </Box>
      </Box>
      {dataList.map((el, i) => (
        <FilterItem
          key={i}
          title={el.title}
          type={el.type}
          dataItems={
            el.type === "ratingRanges"
              ? dataUserVote
              : filterData[el.type] || []
          }
          filters={filters}
          filterData={filterData}
          paramsQuery={paramsQuery}
          location={location}
          paramsUrl={paramsUrl}
        />
      ))}
    </>
  );
};

export default FiltersHotel;
