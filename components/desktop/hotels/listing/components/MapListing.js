import { useState } from "react";
import Map from "@components/common/map/Map";
import { Box, Checkbox } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { IconClose } from "@public/icons";
import { listString } from "@utils/constants";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  mapContainer: {
    background: "#EDF6F9",
    width: "50vw",
    marginRight: "calc(598px - 50vw)",
    height: "calc(100vh - 140px)",
    position: "sticky",
    top: 140,
  },
  mapHeader: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "6px 10px",
    position: "absolute",
    zIndex: 1,
    width: "calc(100% - 32px)",
  },
  icon: {
    borderRadius: 6,
    width: 22,
    height: 22,
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
    "$root.Mui-focusVisible &": {
      outline: "2px auto #00B6F3",
      outlineOffset: 2,
    },
  },
  checkedIconRadio: {
    backgroundColor: "#00B6F3",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: 22,
      height: 22,
      backgroundImage: "url(/icons/icon_checked.svg)",
      backgroundSize: "cover",
      content: '""',
    },
  },
  closeIcon: {
    width: 36,
    height: 36,
    background: "white",
    borderRadius: 36,
    padding: 6,
    cursor: "pointer",
  },
  moveGroup: {
    borderRadius: 30,
    background: "white",
    padding: "0 12px 0 0",
    display: "flex",
    alignItems: "center",
  },
}));

const MapListing = ({
  closeMap,
  hotelList = {},
  fetchData,
  location,
  itemActive,
  setItemActive,
}) => {
  const classes = useStyles();
  const [activeMove, setActiveMove] = useState(false);
  const handleActive = (it) => {
    const activeEl = document.getElementById(it.id);
    if (activeEl) {
      activeEl.scrollIntoView({ behavior: "smooth", block: "center" });
    }
    setItemActive(it);
  };
  const checkIcon = (
    <span className={clsx(classes.icon, classes.checkedIconRadio)} />
  );
  const handleCenterChanged = (position) => {
    fetchData({ ...position, searchType: "latlon" });
  };
  return (
    <Box className={classes.mapContainer}>
      <Box className={classes.mapHeader}>
        <Box className={classes.closeIcon} onClick={closeMap}>
          <IconClose />
        </Box>
        <Box
          className={classes.moveGroup}
          onClick={() => setActiveMove(!activeMove)}
        >
          <Checkbox
            checked={activeMove}
            className={classes.root}
            color="default"
            checkedIcon={checkIcon}
            icon={<span className={classes.icon} />}
          />{" "}
          <span style={{ fontSize: 14, lineHeight: "17px" }}>
            {listString.IDS_MT_FIND_WHEN_MOVE_MAP}
          </span>
        </Box>
        <Box />
      </Box>
      <Map
        items={hotelList}
        itemActive={itemActive}
        centerAddress={{
          latitude: location.latitude,
          longitude: location.longitude,
        }}
        activeMove={activeMove}
        handleActive={handleActive}
        handleCenterChanged={handleCenterChanged}
      />
    </Box>
  );
};
export default MapListing;
