import { Box } from "@material-ui/core";
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import { listString } from "@utils/constants";
import { dataUserVote } from "@utils/dataFake";
import { isEmpty } from "@utils/helpers";
import FilterMapItem from "./FilterMapItem";

const useStyles = makeStyles((theme) => ({
  filterHeaderContainer: { display: "flex", margin: "0 0 0 -4px" },
}));

const FiltersMap = ({ filterData = {}, fetchData, updateFilterMap }) => {
  const classes = useStyles();
  const [collapse, setCollapse] = useState(null);
  const [filters, setFilter] = useState({
    stars: [],
    ratingRanges: [],
    types: [],
    priceRanges: [{ minPrice: 0, maxPrice: 5000000 }],
  });
  const handleFilterChange = () => {
    const ratingRangesTemp = [];
    let result = { ...filters };
    if (!isEmpty(result.ratingRanges)) {
      result.ratingRanges.forEach((el) => {
        ratingRangesTemp.push(dataUserVote[el].value);
      });
    }
    const priceRangesTemp = [];
    if (!isEmpty(result.priceRanges)) {
      result.priceRanges.forEach((el) => {
        priceRangesTemp.push({
          minPrice: el.minPrice,
          maxPrice: el.maxPrice < 5000000 ? el.maxPrice : 1000000000,
        });
      });
    }
    result = {
      ...result,
      ratingRanges: ratingRangesTemp,
      priceRanges: priceRangesTemp,
    };
    if (updateFilterMap) updateFilterMap(result);
    fetchData({ filters: result });
  };
  useEffect(() => {
    handleFilterChange(); // eslint-disable-next-line
  }, [filters]);
  return (
    <Box className={classes.filterHeaderContainer}>
      <FilterMapItem
        title={listString.IDS_MT_RANGE_PRICE}
        type="priceRanges"
        filters={filters}
        setFilter={setFilter}
        collapse={collapse}
        setCollapse={setCollapse}
      />
      <FilterMapItem
        title={listString.IDS_MT_HOTEL_LEVEL}
        type="stars"
        filters={filters}
        setFilter={setFilter}
        collapse={collapse}
        setCollapse={setCollapse}
      />
      <FilterMapItem
        title={listString.IDS_MT_TEXT_USER_VOTE}
        type="ratingRanges"
        dataItems={dataUserVote}
        filters={filters}
        setFilter={setFilter}
        collapse={collapse}
        setCollapse={setCollapse}
      />
      <FilterMapItem
        title={listString.IDS_MT_TEXT_HOTEL_TYPE}
        type="types"
        dataItems={filterData.types || []}
        filters={filters}
        setFilter={setFilter}
        collapse={collapse}
        setCollapse={setCollapse}
      />
    </Box>
  );
};

export default FiltersMap;
