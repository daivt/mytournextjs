import clsx from "clsx";
import { Box, Typography, Checkbox, Radio } from "@material-ui/core";
import { useState } from "react";
import { IconArrowDownSmall } from "@public/icons";
import { makeStyles } from "@material-ui/styles";
import Vote from "@components/common/desktop/vote/Vote";
import { listString, routeStatic } from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import Link from "@src/link/Link";
import ButtonComponent from "@src/button/Button";
import { useRouter } from "next/router";

const useStyles = makeStyles((theme) => ({
  divider: { height: 1, width: "100%", background: "#EDF2F7" },
  content: { padding: 16 },
  titleContent: { fontWeight: 600, fontSize: 14, lineHeight: "17px" },
  icon: {
    borderRadius: 6,
    width: 22,
    height: 22,
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
    "$root.Mui-focusVisible &": {
      outline: "2px auto #00B6F3",
      outlineOffset: 2,
    },
  },
  iconRadio: {
    borderRadius: "50%",
    width: 22,
    height: 22,
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
  },
  checkedIconRadio: {
    backgroundColor: "#00B6F3",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: 22,
      height: 22,
      backgroundImage: "url(/icons/icon_checked.svg)",
      backgroundSize: "cover",
      content: '""',
    },
  },
  checkBoxWrapper: {
    display: "flex",
    alignItems: "center",
    margin: "0 -9px",
    cursor: "pointer",
  },
  textItem: {
    fontSize: 14,
    lineHeight: "17px",
    display: "-webkit-box",
    WebkitLineClamp: 1,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
  },
  IconArrowUpToggle: {
    stroke: theme.palette.blue.blueLight8,
    marginLeft: 8,
    transform: "rotate(180deg)",
    cursor: "pointer",
  },
  IconArrowDownToggle: {
    stroke: theme.palette.blue.blueLight8,
    marginLeft: 8,
    cursor: "pointer",
  },
  IconArrowUp: {
    stroke: "#718096",
    transform: "rotate(180deg)",
    cursor: "pointer",
    "&:hover": { stroke: "#1A202C" },
  },
  IconArrowDown: {
    stroke: "#718096",
    cursor: "pointer",
    "&:hover": { stroke: "#1A202C" },
  },
  titleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  textLink: {
    color: "inherit",
    textDecoration: "none",
    "&:hover": { textDecoration: "none" },
  },
}));

const hotelStarsData = [5, 4, 3, 2, 1];
const FilterItem = ({
  dataItems = [],
  filters = {},
  title = "",
  type = "",
  paramsQuery = {},
  location = {},
  filterData = {},
  paramsUrl = {},
}) => {
  const getParamsLink = () => {
    return paramsUrl;
  };
  const classes = useStyles();
  const router = useRouter();
  const [collapse, setCollapse] = useState(false);
  const checkIcon = (className) => (
    <span className={clsx(className, classes.checkedIconRadio)} />
  );
  const handleClick = (id, isChecked) => {
    let result = { ...filters };
    if (type === "locations") {
      result = { ...result, [type]: isChecked ? [] : [id] };
    } else if (isChecked) {
      result = { ...result, [type]: result[type].filter((v) => v !== id) };
    } else {
      result[type].push(id);
    }
    let params = { page: 1 };
    if (!isEmpty(result.amenities))
      params = { ...params, amenities: result.amenities.join(",") };
    if (!isEmpty(result.stars))
      params = { ...params, stars: result.stars.join(",") };
    if (!isEmpty(result.locations))
      params = { ...params, locations: result.locations.join(",") };
    if (!isEmpty(result.ratingRanges))
      params = { ...params, ratingRanges: result.ratingRanges.join(",") };
    if (!isEmpty(result.services))
      params = { ...params, services: result.services.join(",") };
    if (!isEmpty(result.types))
      params = { ...params, types: result.types.join(",") };
    router.push({
      pathname: routeStatic.LISTING_HOTEL.href,
      query: {
        slug: [
          `${paramsQuery.aliasCode}`,
          `khach-san-tai-${location.name.stringSlug()}.html`,
        ],
        ...getParamsLink(),
        ...params,
      },
    });
  };
  const getLocationName = () => {
    const { locations } = filters;
    let result = { ...location };
    if (locations.length === 1) {
      const temp = filterData.locations.find((v) => v.id === locations[0]);
      if (!isEmpty(temp)) result = { ...temp };
    }
    return result.name || "";
  };
  const data = type === "stars" ? hotelStarsData : dataItems;
  const getLinkFilter = (item) => {
    const params = { ...getParamsLink(), page: 1 };
    const {
      // SEO
      stars,
      services,
      amenities,
      locations,
      // params
      ratingRanges,
      types,
    } = filters;
    if (
      type === "ratingRanges" ||
      type === "types" ||
      !isEmpty(ratingRanges) ||
      !isEmpty(types) ||
      stars.length > 2 ||
      services.length > 2 ||
      amenities.length > 2 ||
      locations.length > 1
    ) {
      return null;
    }
    if (type === "stars") {
      if (
        stars.length <= 2 &&
        (isEmpty(services) || isEmpty(amenities)) &&
        services.length <= 1 &&
        amenities.length <= 1
      ) {
        let itemSelected = {};
        if (!isEmpty(services)) {
          itemSelected = filterData.services.find((v) => v.id === services[0]);
        }
        if (!isEmpty(amenities)) {
          itemSelected = filterData.amenities.find(
            (v) => v.id === amenities[0]
          );
        }
        if (stars.length === 1 && stars[0] !== item) return null;
        let starItem = null;
        if (stars.length === 2) {
          const [fist, last] = stars;
          if (!stars.includes(item)) return null;
          if (fist === item) starItem = last;
          if (last === item) starItem = fist;
        }
        return {
          pathname: routeStatic.LISTING_HOTEL.href,
          query: {
            slug: [
              `${paramsQuery.aliasCode}`,
              `khach-san${
                stars.length === 1
                  ? ""
                  : `-${!isEmpty(starItem) ? starItem : item}-sao`
              }${
                !isEmpty(itemSelected)
                  ? `-${itemSelected.name.stringSlug()}`
                  : ""
              }-tai-${getLocationName().stringSlug()}.html`,
            ],
            ...params,
          },
        };
      }
      return null;
    }
    if (type === "services" || type === "amenities") {
      if ((!isEmpty(services) && !isEmpty(amenities)) || stars.length > 1)
        return null;
      let result = null;
      if (isEmpty(services) && isEmpty(amenities) && stars.length <= 1) {
        result = { ...item };
      }
      if (
        (amenities.length === 1 && amenities[0] === item.id) ||
        (services.length === 1 && services[0] === item.id)
      ) {
        result = { name: null };
      }
      if (amenities.length === 2) {
        const [fist, last] = amenities;
        if (fist === item.id)
          result = filterData.amenities.find((v) => v.id === last);
        if (last === item.id)
          result = filterData.amenities.find((v) => v.id === fist);
      }
      if (services.length === 2) {
        const [fist, last] = services;
        if (fist === item.id)
          result = filterData.services.find((v) => v.id === last);
        if (last === item.id)
          result = filterData.services.find((v) => v.id === fist);
      }
      if (!isEmpty(result)) {
        return {
          pathname: routeStatic.LISTING_HOTEL.href,
          query: {
            slug: [
              `${paramsQuery.aliasCode}`,
              `khach-san${!isEmpty(stars) ? `-${stars[0]}-sao` : ""}${
                !isEmpty(result.name) ? `-${result.name.stringSlug()}` : ""
              }-tai-${getLocationName().stringSlug()}.html`,
            ],
            ...params,
          },
        };
      }
      return null;
    }
    if (type === "locations") {
      const starVal = stars.length === 1 ? stars[0] : null;
      let result = null;
      if (services.length === 1 && isEmpty(amenities)) {
        const temp = filterData.services.find((v) => v.id === services[0]);
        if (!isEmpty(temp)) result = { ...temp };
      } else if (amenities.length === 1 && isEmpty(services)) {
        const temp = filterData.amenities.find((v) => v.id === amenities[0]);
        if (!isEmpty(temp)) result = { ...temp };
      }
      return {
        pathname: routeStatic.LISTING_HOTEL.href,
        query: {
          slug: [
            `${item.aliasCode}`,
            `khach-san${!isEmpty(starVal) ? `-${starVal}-sao` : ""}${
              !isEmpty(result) ? `-${result.name.stringSlug()}` : ""
            }-tai-${
              paramsQuery.aliasCode === item.aliasCode
                ? `${!isEmpty(location.name) ? location.name.stringSlug() : ""}`
                : `${!isEmpty(item.name) ? item.name.stringSlug() : ""}`
            }.html`,
          ],
          ...params,
        },
      };
    }
    return null;
  };
  return (
    <>
      <Box className={classes.divider} />
      <Box className={classes.content}>
        <Box className={classes.titleContainer}>
          <Typography variant="body2" className={classes.titleContent}>
            {title}
          </Typography>
          {!isEmpty(data) && data.length > 5 && (
            <IconArrowDownSmall
              className={`svgFillAll ${
                collapse ? classes.IconArrowUp : classes.IconArrowDown
              }`}
              onClick={() => setCollapse(!collapse)}
            />
          )}
        </Box>
        {data.map((el, i) => {
          if (i >= 5 && !collapse) return null;
          const isChecked =
            !isEmpty(filters[type]) &&
            filters[type].includes(!isEmpty(el.id) ? el.id : el);

          const contentItem = (
            <>
              {type === "locations" ? (
                <Radio
                  checkedIcon={checkIcon(classes.iconRadio)}
                  icon={<span className={classes.iconRadio} />}
                  checked={isChecked || el.aliasCode === paramsQuery.aliasCode}
                />
              ) : (
                <Checkbox
                  checked={isChecked}
                  className={classes.root}
                  color="default"
                  checkedIcon={checkIcon(classes.icon)}
                  icon={<span className={classes.icon} />}
                />
              )}
              {type === "stars" ? (
                <Vote maxValue={el} value={el} />
              ) : (
                <span className={classes.textItem} title={el.name}>
                  {el.name}
                </span>
              )}
            </>
          );
          if (getLinkFilter(el))
            return (
              <Link
                key={i}
                href={getLinkFilter(el)}
                className={classes.textLink}
              >
                <Box className={classes.checkBoxWrapper}>{contentItem}</Box>
              </Link>
            );
          return (
            <Box
              key={i}
              className={classes.checkBoxWrapper}
              onClick={() =>
                handleClick(!isEmpty(el.id) ? el.id : el, isChecked)
              }
            >
              {contentItem}
            </Box>
          );
        })}
        {!isEmpty(data) && data.length > 5 && (
          <ButtonComponent
            typeButton="text"
            color="#00B6F3"
            width="100%"
            padding="6px 8px 6px 0"
            handleClick={() => setCollapse(!collapse)}
          >
            <Typography
              variant="body1"
              component="span"
              style={{ alignItems: "center", display: "flex" }}
            >
              {collapse
                ? listString.IDS_MT_TEXT_LOAD_COMPACT
                : listString.IDS_MT_TEXT_LOAD_MORE}
              <IconArrowDownSmall
                className={`svgFillAll ${
                  collapse
                    ? classes.IconArrowUpToggle
                    : classes.IconArrowDownToggle
                }`}
              />
            </Typography>
          </ButtonComponent>
        )}
      </Box>
    </>
  );
};

export default FilterItem;
