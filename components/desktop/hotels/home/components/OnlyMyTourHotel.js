import { useState, useEffect } from "react";
import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import HotelItemVertical from "@components/common/desktop/itemView/HotelItemVertical";
import {
  isEmpty,
  adapterHotelHotDeal,
  paramsDefaultHotel,
  adapterHotelHotDealPc,
} from "@utils/helpers";
import Link from "@src/link/Link";
import {
  listString,
  routeStatic,
  DELAY_TIMEOUT_POLLING,
  prefixUrlIcon,
  listIcons,
} from "@utils/constants";
import ButtonComponent from "@src/button/Button";
import { getHotelsAvailability } from "@api/hotels";
import { getTopHotels } from "@api/homes";
import {
  IconBedOnlyMytour,
  IconHotSaleOnlyMytour,
  IconLikeOnlyMytour,
} from "@public/icons";
import clsx from "clsx";
import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  container: {
    background: "#EDF6F9",
    width: "calc(100% - 48px)",
    borderRadius: 12,
    margin: "0 24px 24px 24px",
    padding: "56px 0",
  },
  content: {
    maxWidth: 1188,
    width: "100%",
    margin: "0 auto",
    display: "flex",
    justifyContent: "space-between",
    flexDirection: "column",
  },
  hotelGroup: { display: "flex", flexWrap: "wrap" },
  hotelIcon: {
    justifyContent: "space-between",
  },
  wrapIcon: {
    width: 44,
    height: 44,
    background: theme.palette.white.main,
    borderRadius: 100,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  icon: {
    display: "flex",
    flexDirection: "column",
  },
  titleText: {
    fontWeight: 600,
    fontSize: 24,
    lineHeight: "29px",
    color: "#1A202C",
  },
  buttonGroup: { display: "flex" },
  title: {
    marginTop: 8,
    fontSize: 14,
    lineHeight: "16px",
  },
  iconOnlyMytour: {
    width: 24,
    height: 24,
  },
}));
const OnlyMyTourHotel = () => {
  const classes = useStyles();
  let breakPolling = true;
  const [topHotels, setTopHotels] = useState([]);
  const mergePriceToData = (data = [], result = []) => {
    const dataResult = [...result];
    data.forEach((el) => {
      dataResult.forEach((item, index) => {
        if (item.id === el.id) {
          dataResult[index] = {
            ...item,
            basePromotionInfo: el.basePromotionInfo,
            price: el.basePrice,
            hiddenPrice: el.hiddenPrice,
          };
          return;
        }
      });
    });
    setTopHotels(dataResult);
  };
  const fetchPriceHotel = async (ids, topHotelData) => {
    let polling = true;
    try {
      const dataDto = {
        ...paramsDefaultHotel(),
        hotelIds: ids,
        searchType: "hotel",
      };
      while (polling && breakPolling) {
        const { data } = await getHotelsAvailability(dataDto);
        if (data.code === 200) {
          if (data.data.completed) {
            mergePriceToData(data.data.items, topHotelData);
            polling = false;
            break;
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      polling = false;
    }
  };
  const fetchData = async (param = {}) => {
    try {
      const params = { page: 1, size: 20, ...paramsDefaultHotel() };

      const { data } = await getTopHotels({ ...params, ...param });
      if (data.code === 200 && !isEmpty(data.data)) {
        setTopHotels(data.data.items || []);
        if (!isEmpty(data.data.items)) {
          fetchPriceHotel(
            data.data.items.map((el) => el.id),
            data.data.items
          );
        }
      }
    } catch (error) {}
  };
  useEffect(() => {
    fetchData();
    return () => {
      breakPolling = false;
    }; // eslint-disable-next-line
  }, []);
  return (
    <Box className={classes.container}>
      <Box className={classes.content}>
        <Box
          className={clsx(classes.hotelGroup, classes.hotelIcon)}
          style={{
            marginBottom: 44,
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Typography variant="body2" className={classes.titleText}>
            {listString.IDS_MT_TOP_PRICE_HOTEL_ONLY_MYTOUR}
          </Typography>
          <Box display="flex">
            <Box
              display="flex"
              flexDirection="column"
              alignItems="center"
              style={{ marginRight: 56 }}
            >
              <Box className={classes.wrapIcon}>
                <Box className={classes.icon}>
                  <IconBedOnlyMytour />
                </Box>
              </Box>
              <Box className={classes.title}>Khách sạn mới</Box>
            </Box>

            <Box
              display="flex"
              flexDirection="column"
              alignItems="center"
              style={{ marginRight: 56 }}
            >
              <Box className={classes.wrapIcon}>
                <Box className={classes.icon}>
                  <IconHotSaleOnlyMytour />
                </Box>
              </Box>
              <Box className={classes.title}>Giá hấp dẫn</Box>
            </Box>

            <Box
              display="flex"
              flexDirection="column"
              alignItems="center"
              style={{ marginRight: 56 }}
            >
              <Box className={classes.wrapIcon}>
                <Box className={classes.icon}>
                  <Image
                    srcImage={`${prefixUrlIcon}${listIcons.IconRefreshOnlyMytour}`}
                    className={classes.iconOnlyMytour}
                  />
                </Box>
              </Box>
              <Box className={classes.title}>Cập nhật hàng tuần</Box>
            </Box>

            <Box display="flex" flexDirection="column" alignItems="center">
              <Box className={classes.wrapIcon}>
                <Box className={classes.icon}>
                  <IconLikeOnlyMytour />
                </Box>
              </Box>
              <Box className={classes.title}>Dành riêng cho bạn</Box>
            </Box>
          </Box>
        </Box>
        <Box className={classes.hotelGroup} style={{ margin: "0 -12px" }}>
          {!isEmpty(topHotels) &&
            adapterHotelHotDealPc(topHotels).map((el, i) => {
              if (i > 7) return null;
              return (
                <Box style={{ margin: "0 12px", marginBottom: 24 }} key={i}>
                  <HotelItemVertical
                    item={el}
                    isShowLastBook
                    isTopSale
                    isShowStatus
                    isHotelInterest
                    isCountBooking={false}
                  />
                </Box>
              );
            })}
        </Box>
      </Box>
    </Box>
  );
};

export default OnlyMyTourHotel;
