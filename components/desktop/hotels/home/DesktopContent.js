import { useState, useEffect } from "react";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Layout from "@components/layout/desktop/Layout";
import InfoDescriptionHotelCard from "@components/common/desktop/card/InfoDescriptionHotelCard";
import { paramsDefaultHotel } from "@utils/helpers";
import MyCoupon from "@components/common/desktop/myCoupon/MyCoupon";
import OnlyMyTourHotel from "./components/OnlyMyTourHotel";
import LocationList from "./components/LocationList";
import RecentView from "./components/RecentView";
import CityList from "./components/CityList";

const useStyles = makeStyles((theme) => ({
  homeWrapper: { width: "100%", background: "white" },
  homeContainer: {
    maxWidth: 1188,
    margin: "0 auto",
    padding: "24px 0 56px 0",
  },
}));
const Home = ({ topLocation = [] }) => {
  const classes = useStyles();
  const [isServerLoaded, setServerLoaded] = useState(false);

  useEffect(() => {
    setServerLoaded(true);
  }, []);

  return (
    <Layout
      isHeader
      isFooter
      type="hotel"
      topLocation={topLocation}
      title="Đặt phòng khách sạn hạng sang giá rẻ - giá tốt nhất trên Mytour.vn | Đặt sát ngày giá càng tốt."
    >
      <Box className={classes.homeWrapper}>
        {isServerLoaded && (
          <Box className={classes.homeContainer}>
            <InfoDescriptionHotelCard />
          </Box>
        )}
        {isServerLoaded && <MyCoupon />}
        <LocationList
          topLocation={topLocation}
          paramsUrl={paramsDefaultHotel()}
        />
        {isServerLoaded && <RecentView />}
        <OnlyMyTourHotel />
        <CityList />
      </Box>
    </Layout>
  );
};

export default Home;
