import dynamic from "next/dynamic";

const DesktopContent = dynamic(() =>
  import("@components/desktop/home/partnership/DesktopContent")
);

export default function Partnership() {
  return <DesktopContent />;
}
