import { Box } from "@material-ui/core";
import LockIcon from "@material-ui/icons/Lock";
import Link from "@src/link/Link";
import Layout from "@components/layout/desktop/Layout";

import { listString, routeStatic } from "@utils/constants";

const Custom404 = () => {
  return (
    <Layout isInsPreview={false}>
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        pt={68 / 8}
        color="gray.grayDark1"
        fontWeight={500}
        maxWidth={335}
        margin="0 auto"
        lineHeight={1}
      >
        <Box fontSize={128}>{listString.IDS_MT_TEXT_NOT_FOUND_403_DES_1}</Box>
        <Box fontSize={32} textAlign="center" lineHeight={1.5} pt={2} pb={3}>
          {listString.IDS_MT_TEXT_NOT_FOUND_403_DES_2}
        </Box>
        <Link href={routeStatic.HOME.href}>
          <Box fontSize={20} pb={66 / 8}>
            {listString.IDS_MT_TEXT_NOT_FOUND_403_DES_3}
          </Box>
        </Link>
        <LockIcon style={{ height: 212, width: 212 }} />
      </Box>
    </Layout>
  );
};

export default Custom404;
