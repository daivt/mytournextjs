const ListingHotel = () => {
  return <></>;
};

export default ListingHotel;

export async function getServerSideProps({ params, req, res, query }) {
  res.writeHead(301, {
    Location: `/khach-san/${query.hotels}/${query.listing}`,
  });
  res.end();
  return {
    props: {},
  };
}
