import { resolveHotel } from "@api/homes";

const OldHotel = () => {
  return <></>;
};

export default OldHotel;

export async function getServerSideProps({ params, req, res, query }) {
  try {
    const { data } = await resolveHotel({
      slug: query.hotels,
      agencyId: 25,
    });
    if (data.code === 200) {
      let locationRedirect = "";
      if (query.hotels.includes("-danh-gia-")) {
        locationRedirect = `/khach-san/${
          data?.data?.id
        }-${data?.data?.name.stringSlug()}.html?listReview=true`;
      } else {
        locationRedirect = `/khach-san/${
          data?.data?.id
        }-${data?.data?.name.stringSlug()}.html`;
      }
      res.writeHead(301, {
        Location: locationRedirect,
      });
      res.end();
    } else if (data.code === 400) {
      res.writeHead(301, {
        Location: `/?detail=true`,
      });
      res.end();
    }
  } catch (error) {}

  return {
    props: {},
  };
}
