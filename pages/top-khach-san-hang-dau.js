import { useState, useEffect } from "react";
import { Box, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import InfiniteScroll from "react-infinite-scroller";

import { getHotelsHotDeal } from "@api/homes";
import { listString } from "@utils/constants";
import { getHotelsAvailability } from "@api/hotels";
import {
  isEmpty,
  adapterHotelHotDeal,
  paramsDefaultHotel,
} from "@utils/helpers";

import * as actionTypes from "@contextProvider/system/ActionTypes";
import { useDispatchSystem } from "@contextProvider/ContextProvider";

import Image from "@src/image/Image";
import Layout from "@components/layout/mobile/Layout";
import HotelCard from "@components/common/card/hotel/HotelCard";
import LoadingMore from "@components/common/loading/LoadingMore";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    color: theme.palette.black.black3,
    fontSize: 14,
    padding: "0 16px",
  },
  topBanner: {
    height: 183,
    width: "100%",
  },
  wrapperItem: {
    paddingTop: 16,
  },
  itemHotelCard: {
    paddingBottom: 16,
  },
  classNameCard: {
    minWidth: "80%",
  },
}));

const pageSize = 20;

const HotDealsList = () => {
  const classes = useStyles();
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(true);
  const [dataHotel, setDataHotel] = useState([]);
  const [totalHotel, setTotalHotel] = useState(0);

  let breakPolling = true;
  const dispatch = useDispatchSystem();

  useEffect(() => {
    return () => {
      breakPolling = false;
    };
  }, []);

  const loadFunc = async () => {
    const pageTemp = page;
    try {
      setLoading(false);
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: true,
      });
      const { data } = await getHotelsHotDeal({
        page: pageTemp,
        size: pageSize,
        ...paramsDefaultHotel(),
      });
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
      if (data.code === 200) {
        const dataConcat = [...dataHotel, ...data?.data?.items];
        fetPriceHotelHotDeal(data?.data?.items, dataConcat);
        setPage(pageTemp + 1);
        setDataHotel(dataConcat);
        setTotalHotel(data?.data.total);
        if (pageTemp * pageSize < data?.data.total) {
          setLoading(true);
        }
      }
    } catch (error) {
      dispatch({
        type: actionTypes.SET_VISIBLE_FETCHING,
        payload: false,
      });
    }
  };
  const fetPriceHotelHotDeal = (dataFetPrice = [], dataConcat = []) => {
    if (!isEmpty(dataFetPrice)) {
      const hotelHotDealIds = dataFetPrice.map((el) => el.id);
      fetPriceHotel(dataConcat, hotelHotDealIds);
    }
  };

  const fetPriceHotel = async (dataConcat = [], ids = []) => {
    let polling = true;
    try {
      const dataDto = {
        ...paramsDefaultHotel(),
        hotelIds: ids,
        searchType: "hotel",
      };
      while (polling && breakPolling) {
        const { data } = await getHotelsAvailability(dataDto);
        if (data.code === 200) {
          mergerPriceToData(dataConcat, data?.data.items);
          if (data.data.completed) {
            polling = false;
            break;
          }
          await new Promise((resolve) =>
            setTimeout(resolve, DELAY_TIMEOUT_POLLING)
          );
        } else {
          polling = false;
        }
      }
    } catch (error) {
      polling = false;
    }
  };

  const mergerPriceToData = (dataConcat = [], dataPoling = []) => {
    let dataResult = [...dataConcat];
    dataPoling.forEach((el) => {
      dataResult.forEach((item, index) => {
        if (item.id === el.id) {
          dataResult[index] = {
            ...item,
            promotionInfo: el.basePromotionInfo,
            price: el.basePrice,
            hiddenPrice: el.hiddenPrice,
          };
        }
      });
    });
    setDataHotel(dataResult);
  };
  return (
    <Layout
      isHeaderBack
      titleHeaderBack={listString.IDS_TEXT_CAN_YOU_LOVE}
      title="Top khách sạn hàng đầu chỉ có trên Mytour"
    >
      <Image
        srcImage="https://storage.googleapis.com/tripi-assets/mytour/banner/img_banner_top_search.png"
        className={classes.topBanner}
      />
      <Box className={classes.container}>
        <InfiniteScroll pageStart={page} loadMore={loadFunc} hasMore={loading}>
          <Grid container spacing={2} className={classes.wrapperItem}>
            {adapterHotelHotDeal(dataHotel).map((el) => (
              <Grid
                item
                lg={6}
                md={6}
                sm={6}
                xs={6}
                key={el.id}
                className={classes.itemHotelCard}
              >
                <HotelCard
                  item={el}
                  isHotelInterest={false}
                  isCountBooking={false}
                  isShowDiscountPercent={false}
                  isShowPriceInImage={false}
                />
              </Grid>
            ))}
          </Grid>
          {!loading && page * pageSize < totalHotel && <LoadingMore />}
        </InfiniteScroll>
      </Box>
    </Layout>
  );
};

export default HotDealsList;
