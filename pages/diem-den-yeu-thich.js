import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import { getTopLocation } from "@api/homes";
import { listString } from "@utils/constants";

import Layout from "@components/layout/mobile/Layout";
import LocationBannerCard from "@components/common/card/hotel/LocationBannerCard";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    color: theme.palette.black.black3,
    fontSize: 14,
  },
  styleImageBanner: {
    height: 164,
    borderRadius: 0,
  },
}));

const FavoriteDestinations = ({ topLocation = [] }) => {
  const classes = useStyles();

  return (
    <Layout
      isHeaderBack
      titleHeaderBack={listString.IDS_TEXT_DESTINATION_FAVORITE}
    >
      <Box className={classes.container}>
        {topLocation.map((el, index) => (
          <Box key={el.provinceId || index} pb={12 / 8}>
            <LocationBannerCard
              styleImageBanner={classes.styleImageBanner}
              item={el}
              isHome={false}
            />
          </Box>
        ))}
      </Box>
    </Layout>
  );
};

export default FavoriteDestinations;

export async function getStaticProps(context) {
  let topLocation = [];
  try {
    const { data } = await getTopLocation({ page: 1, size: 100 });
    if (data.code === 200) {
      topLocation = data?.data?.items || [];
    }
  } catch (error) {}
  return {
    props: {
      topLocation,
    }, // will be passed to the page component as props
    revalidate: 10,
  };
}
