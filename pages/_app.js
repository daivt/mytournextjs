import Router, { useRouter } from "next/router";
import { MuiThemeProvider } from "@material-ui/core";
import NProgress from "nprogress";
import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { SnackbarProvider } from "notistack";
import CssBaseline from "@material-ui/core/CssBaseline";
import "nprogress/nprogress.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { ThemeProvider } from "styled-components";

import * as gtm from "@utils/gtm";
import { SystemProvider } from "../contextProvider/ContextProvider";
import { MUI_THEME, THEME } from "../src/setupTheme";
import ChatContainer from "../components/chatWidget/chat/ChatContainer";
import "../styles/globals.scss";
import "../styles/svg.scss";
import "../components/chatWidget/chat/Chat.scss";
import "../components/chatWidget/assets/theme/colors.module.scss";

import "../utils/prototype";

NProgress.configure({ showSpinner: true });
Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

export default function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const [isServerLoaded, setServerLoaded] = useState(false);
  useEffect(() => {
    const handleRouteChange = (url) => {
      gtm.pageview(url);
    };
    router.events.on("routeChangeComplete", handleRouteChange);
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, [router.events]);

  useEffect(() => {
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
    setServerLoaded(true);
  }, []);
  return (
    <ThemeProvider theme={THEME}>
      <MuiThemeProvider theme={MUI_THEME}>
        <CssBaseline />
        <SystemProvider>
          <SnackbarProvider maxSnack={3}>
            <>
              <Component {...pageProps} />
              {!pageProps.isMobile && isServerLoaded && (
                <ChatContainer caId={17} />
              )}
            </>
          </SnackbarProvider>
        </SystemProvider>
      </MuiThemeProvider>
    </ThemeProvider>
  );
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
};
