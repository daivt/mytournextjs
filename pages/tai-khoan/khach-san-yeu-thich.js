import dynamic from "next/dynamic";

const DesktopContent = dynamic(() =>
  import("@components/desktop/account/FavoriteHotel")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/account/FavoriteHotel")
);

import { checkIsMobile } from "@utils/helpers";

export default function Home({ isMobile = false }) {
  return <>{isMobile ? <MobileContent /> : <DesktopContent />}</>;
}

export async function getServerSideProps({ req }) {
  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
    },
  };
}
