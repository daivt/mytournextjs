import dynamic from "next/dynamic";

const DesktopContent = dynamic(() =>
  import("@components/desktop/account/AccountManager")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/account/AccountManager")
);

import { isEmpty, checkIsMobile, getHeaderInServer } from "@utils/helpers";

export default function Home({ isMobile = false }) {
  return <>{isMobile ? <MobileContent /> : <DesktopContent />}</>;
}

export async function getServerSideProps({ req, res }) {
  const headerResult = getHeaderInServer(req.headers || {});
  if (
    isEmpty(headerResult.token) &&
    !checkIsMobile(req.headers["user-agent"])
  ) {
    res.writeHead(301, {
      Location: `/`,
    });
    res.end();
  }
  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
    },
  };
}
