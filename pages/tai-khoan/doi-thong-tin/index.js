import * as yup from "yup";
import cookie from "js-cookie";
import { useState } from "react";
import { Formik, Form } from "formik";
import { useSnackbar } from "notistack";
import { useDropzone } from "react-dropzone";
import { makeStyles } from "@material-ui/styles";
import { Box, Avatar, Badge } from "@material-ui/core";

import { isEmpty } from "@utils/helpers";
import { IconCamera } from "@public/icons";
import { listString, TOKEN } from "@utils/constants";
import { uploadAvatar, updateUserInfo } from "@api/user";
import * as actionTypes from "@contextProvider/system/ActionTypes";
import { useSystem, useDispatchSystem } from "@contextProvider/ContextProvider";

import snackbarSetting from "@src/alert/Alert";
import ButtonComponent from "@src/button/Button";
import Layout from "@components/layout/mobile/Layout";
import FieldTextBaseContent from "@src/form/FieldTextBaseContent";

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
  },
  infoAccount: {
    position: "relative",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 20,
  },
  diver: {
    height: 1,
    width: "100%",
    backgroundColor: theme.palette.gray.grayLight22,
    margin: "8px 0",
  },
  avatarDefault: {
    width: 88,
    height: 88,
    backgroundColor: theme.palette.blue.blueLight8,
    fontSize: 40,
    fontWeight: 600,
  },
  avatarUser: {
    width: 88,
    height: 88,
  },
  boxBtnRegister: {
    paddingTop: 10,
  },
  iconCamera: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 32,
    height: 32,
    borderRadius: "50%",
    backgroundColor: theme.palette.white.main,
    boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.15)",
  },
}));
const AccountInfo = () => {
  const classes = useStyles();
  const { systemReducer } = useSystem();
  const dispatch = useDispatchSystem();
  const [profilePhoto, setProfilePhoto] = useState(
    !isEmpty(systemReducer.informationUser)
      ? systemReducer.informationUser.profilePhoto
      : ""
  );
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const storeSchema = yup.object().shape({
    name: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_YOUR_NAME),
    emailInfo: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_EMAIL)
      .email(listString.IDS_MT_TEXT_EMAIL_INVALIDATE),
  });
  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/*",
    maxFiles: 1,
    multiple: false,
    onDrop: (acceptedFiles) => {
      acceptedFiles.forEach((el) => {
        const formData = new FormData();
        formData.append("file", el);
        uploadImage(formData);
      });
    },
  });

  const uploadImage = async (dataDto = {}) => {
    const { data } = await uploadAvatar(dataDto);
    if (data.code === 200) {
      setProfilePhoto(data.photo.link);
    }
  };

  const informationUser = systemReducer.informationUser || {};
  const nameUser = informationUser.name || "";
  const nameSplit = nameUser.split(" ") || [];
  const lastNameUSer = nameSplit.pop() || "";
  return (
    <Layout
      isHeaderBack
      isShowTitleInit
      titleHeaderBack={listString.IDS_MT_TEXT_ACCOUNT}
    >
      <Box className={classes.container}>
        <Box className={classes.diver} />
        <Box className={classes.infoAccount}>
          <Badge
            overlap="circle"
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "right",
            }}
            badgeContent={
              <Box className={classes.iconCamera}>
                <IconCamera />
              </Box>
            }
            {...getRootProps({ className: "dropzone" })}
          >
            <input {...getInputProps()} />
            {!isEmpty(profilePhoto) ? (
              <Avatar
                alt="Remy Sharp"
                src={profilePhoto}
                className={classes.avatarUser}
              />
            ) : (
              <Avatar className={classes.avatarDefault}>
                {lastNameUSer[0] || "H"}
              </Avatar>
            )}
          </Badge>
        </Box>
        <Box p="12px 16px 0">
          <Formik
            initialValues={{
              name: informationUser.name || "",
              emailInfo: informationUser.emailInfo || "",
              phoneInfo: informationUser.phoneInfo || "",
              address: informationUser.address || "",
              return_user_info: true,
              access_token: cookie.get(TOKEN),
            }}
            onSubmit={async (values, { setSubmitting }) => {
              try {
                const dataDTO = {
                  ...values,
                  profile_photo: profilePhoto,
                };
                const { data } = await updateUserInfo(dataDTO);
                if (data.code === 200) {
                  enqueueSnackbar(
                    "Thay đổi thông tin thành công!",
                    snackbarSetting((key) => closeSnackbar(key), {
                      color: "success",
                    })
                  );
                  dispatch({
                    type: actionTypes.SET_INFORMATION_USER,
                    payload: data.data,
                  });
                } else {
                  data?.message &&
                    enqueueSnackbar(
                      data?.message,
                      snackbarSetting((key) => closeSnackbar(key), {
                        color: "error",
                      })
                    );
                }
              } catch (error) {}
            }}
            validationSchema={storeSchema}
          >
            {() => {
              return (
                <Form>
                  <FieldTextBaseContent
                    id="field-name"
                    name="name"
                    inputProps={{
                      autoComplete: "off",
                    }}
                    label={listString.IDS_MT_TEXT_CONTACT_NAME_SIMPLE}
                  />
                  <FieldTextBaseContent
                    id="field-email"
                    name="emailInfo"
                    inputProps={{
                      autoComplete: "off",
                    }}
                    label={listString.IDS_MT_TEXT_EMAIL}
                  />
                  <FieldTextBaseContent
                    id="field-phoneNumber"
                    disabled
                    name="phoneInfo"
                    inputProps={{
                      autoComplete: "off",
                    }}
                    label={listString.IDS_MT_TEXT_PHONE_NUMBER}
                  />
                  <FieldTextBaseContent
                    id="field-phoneNumber"
                    name="address"
                    inputProps={{
                      autoComplete: "off",
                    }}
                    label="Địa chỉ"
                  />
                  {/* <FieldTextBaseContent
                    id="field-phoneNumber"
                    name="provice"
                    inputProps={{
                      autoComplete: "off",
                    }}
                    label="Tỉnh/thành phố"
                  /> */}
                  <Box className={classes.boxBtnRegister}>
                    <ButtonComponent
                      backgroundColor="#FF1284"
                      borderRadius={8}
                      height={52}
                      padding="10px 40px"
                      fontSize={16}
                      fontWeight={600}
                      type="submit"
                    >
                      Lưu lại
                    </ButtonComponent>
                  </Box>
                </Form>
              );
            }}
          </Formik>
        </Box>
      </Box>
    </Layout>
  );
};

export default AccountInfo;
