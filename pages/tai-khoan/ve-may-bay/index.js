import dynamic from "next/dynamic";

const DesktopContent = dynamic(() =>
  import("@components/desktop/account/order/flight/OrderFlight")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/account/order/flight/OrderFlight")
);

import { checkIsMobile } from "@utils/helpers";

export default function Home({
  isMobile = false,
  query = {},
  codeFlight = {},
}) {
  return (
    <>
      {isMobile ? (
        <MobileContent query={query} codeFlight={codeFlight} />
      ) : (
        <DesktopContent query={query} codeFlight={codeFlight} />
      )}
    </>
  );
}

export async function getServerSideProps({ params, req, res, query }) {
  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
      query,
    },
  };
}
