import dynamic from "next/dynamic";

const MobileContent = dynamic(() =>
  import("@components/mobile/account/order/flight/orderDetail/MobileContent")
);

const DesktopContent = dynamic(() =>
  import("@components/desktop/account/order/flight/orderDetail/DesktopContent")
);

import { checkIsMobile } from "@utils/helpers";

export default function Home({ isMobile = false }) {
  return <>{isMobile ? <MobileContent /> : <DesktopContent />}</>;
}

export async function getServerSideProps({ params, req, res }) {
  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
    },
  };
}
