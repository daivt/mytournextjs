import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import Link from "@src/link/Link";
import Layout from "@components/layout/desktop/Layout";

import { listString, routeStatic } from "@utils/constants";

import Image from "@src/image/Image";

const useStyles = makeStyles((theme) => ({
  imageEmpty: {
    width: 287,
    height: 274,
  },
}));

const Custom404 = () => {
  const classes = useStyles();

  return (
    <Layout isInsPreview={false}>
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        pt={68 / 8}
        color="gray.grayDark1"
        fontWeight={500}
        maxWidth={335}
        margin="0 auto"
        lineHeight={1}
      >
        <Box fontSize={128}>{listString.IDS_MT_TEXT_NOT_FOUND_404_DES_1}</Box>
        <Box fontSize={64} py={6 / 8}>
          {listString.IDS_MT_TEXT_NOT_FOUND_404_DES_2}
        </Box>
        <Box fontSize={32} textAlign="center" lineHeight={1.5}>
          {listString.IDS_MT_TEXT_NOT_FOUND_404_DES_3}
        </Box>
        <Link href={routeStatic.HOME.href}>
          <Box fontSize={20} pb={130 / 8}>
            {listString.IDS_MT_TEXT_NOT_FOUND_404_DES_4}
          </Box>
        </Link>
        <Image
          srcImage="https://storage.googleapis.com/tripi-assets/mytour/icons/icon_404.svg"
          className={classes.imageEmpty}
        />
      </Box>
    </Layout>
  );
};

export default Custom404;
