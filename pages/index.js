import dynamic from "next/dynamic";

const DesktopContent = dynamic(() =>
  import("@components/desktop/home/DesktopContent")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/homes/MobileContent")
);

import { checkIsMobile } from "@utils/helpers";
import { getTopLocation } from "@api/homes";

export default function Home({ isMobile = false, topLocation = [] }) {
  return (
    <>
      {isMobile ? (
        <MobileContent topLocation={topLocation} />
      ) : (
        <DesktopContent topLocation={topLocation} />
      )}
    </>
  );
}

export async function getServerSideProps({ params, req, res }) {
  let topLocation = [];
  try {
    const { data } = await getTopLocation({ page: 1, size: 20 });
    if (data.code === 200) {
      topLocation = data?.data?.items || [];
    }
  } catch (error) {}
  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
      topLocation,
    },
  };
}
