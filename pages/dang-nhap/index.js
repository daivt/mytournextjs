import * as yup from "yup";
import { useState, useEffect } from "react";
import cookie from "js-cookie";
import { Formik, Form } from "formik";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { makeStyles } from "@material-ui/styles";
import { Box, InputAdornment, IconButton } from "@material-ui/core";

import { LoginAccount } from "@api/user";
import ButtonComponent from "@src/button/Button";
import { IconVisibility, IconVisibilityOff } from "@public/icons";
import {
  listString,
  routeStatic,
  TOKEN,
  LAST_BOOKING_HOTEL,
} from "@utils/constants";
import { isEmpty } from "@utils/helpers";
import * as actionTypes from "@contextProvider/system/ActionTypes";
import { useSystem, useDispatchSystem } from "@contextProvider/ContextProvider";

import Link from "@src/link/Link";
import snackbarSetting from "@src/alert/Alert";
import Layout from "@components/layout/mobile/Layout";
import FieldTextContent from "@src/form/FieldTextContent";
import ButtonLoginViaGGFB from "@components/common/login/ButtonLoginViaGGFB";

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
    padding: "12px 32px",
    position: "relative",
    minHeight: "calc(100vh - 48px)",
  },
  iconPhone: {
    fill: theme.palette.primary.main,
  },
  boxBtnRegisterFixed: {
    position: "fixed",
    bottom: 16,
    width: "calc(100% - 60px)",
  },
  boxBtnRegister: {
    paddingTop: 52,
  },
}));
let redirectUriTemp = "";
const Login = () => {
  const router = useRouter();
  const classes = useStyles();
  const dispatch = useDispatchSystem();
  const { systemReducer } = useSystem();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [isFocus, setIsFocus] = useState(false);
  const [isClient, setIsClient] = useState(false);

  const storeSchema = yup.object().shape({
    account: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_PHONE),
    password: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_PASSWORD)
      .min(6, listString.IDS_MT_TEXT_VALIDATE_PASSWORD)
      .max(50, listString.IDS_MT_TEXT_VALIDATE_PASSWORD),
  });
  const handleFocus = (value) => {
    setIsFocus(value);
  };

  useEffect(() => {
    const IS_SERVER = typeof window === "undefined";
    if (!IS_SERVER) {
      redirectUriTemp = `${window.location.origin}/dang-nhap`;
    }
    setIsClient(true);
  }, []);

  return (
    <Layout
      isHeaderBack
      isShowTitleInit
      titleHeaderBack={listString.IDS_MT_TEXT_LOGIN}
    >
      <Box className={classes.container}>
        {isClient && <ButtonLoginViaGGFB redirectUri={redirectUriTemp} />}

        <Box
          lineHeight="17px"
          display="flex"
          justifyContent="center"
          pt={3}
          pb={2}
        >
          {listString.IDS_MT_TEXT_LOGIN_BY_PHONE_OR_EMAIL}
        </Box>
        <Formik
          initialValues={{
            account: "",
            password: "",
            showPassword: false,
          }}
          onSubmit={async (values) => {
            try {
              const dataDTO = {
                ...values,
              };
              delete dataDTO.showPassword;
              dispatch({
                type: actionTypes.SET_VISIBLE_FETCHING,
                payload: true,
              });
              const { data } = await LoginAccount(dataDTO);
              dispatch({
                type: actionTypes.SET_VISIBLE_FETCHING,
                payload: false,
              });
              if (data.code === 200) {
                localStorage.removeItem(LAST_BOOKING_HOTEL);
                cookie.set(TOKEN, data.data.loginToken, { expires: 365 });
                router.back();
                data?.message &&
                  enqueueSnackbar(
                    listString.IDS_MT_TEXT_LOGIN_ACCOUNT_SUCCESS,
                    snackbarSetting((key) => closeSnackbar(key), {
                      color: "success",
                    })
                  );
              } else {
                data?.message &&
                  enqueueSnackbar(
                    data?.message,
                    snackbarSetting((key) => closeSnackbar(key), {
                      color: "error",
                    })
                  );
              }
            } catch (error) {
              dispatch({
                type: actionTypes.SET_VISIBLE_FETCHING,
                payload: true,
              });
            }
          }}
          validationSchema={storeSchema}
        >
          {({ values, setFieldValue }) => {
            return (
              <Form>
                <FieldTextContent
                  name="account"
                  optional
                  placeholder={listString.IDS_MT_TEXT_ENTER_PHONE_OR_EMAIL}
                  inputStyle={{
                    border: "none",
                    height: 52,
                    backgroundColor: "#EDF2F7",
                    borderRadius: 8,
                    paddingLeft: 16,
                  }}
                  inputProps={{
                    autoComplete: "off",
                  }}
                  onFocus={() => handleFocus(true)}
                  onBlur={() => handleFocus(false)}
                />
                <FieldTextContent
                  name="password"
                  type={values.showPassword ? "text" : "password"}
                  optional
                  placeholder={listString.IDS_MT_TEXT_PASSWORD}
                  inputStyle={{
                    border: "none",
                    height: 52,
                    backgroundColor: "#EDF2F7",
                    borderRadius: 8,
                    paddingLeft: 16,
                    paddingRight: 16,
                  }}
                  inputProps={{
                    autoComplete: "off",
                  }}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={() => {
                          setFieldValue("showPassword", !values.showPassword);
                        }}
                        edge="end"
                      >
                        {values.showPassword ? (
                          <IconVisibility />
                        ) : (
                          <IconVisibilityOff />
                        )}
                      </IconButton>
                    </InputAdornment>
                  }
                  onFocus={() => handleFocus(true)}
                  onBlur={() => handleFocus(false)}
                />
                <ButtonComponent
                  backgroundColor="#FF1284"
                  borderRadius={8}
                  height={52}
                  padding="10px 40px"
                  fontSize={16}
                  fontWeight={600}
                  type="submit"
                  // loading={loading}
                >
                  {listString.IDS_MT_TEXT_LOGIN}
                </ButtonComponent>
                {/* <Box display="flex" justifyContent="center" pt={3}>
                  <Link
                    href={{
                      pathname: routeStatic.FORGET_PASSWORD.href,
                    }}
                    className={classes.linkHotelDetail}
                  >
                    {listString.IDS_MT_TEXT_FORGET_PASSWORD}
                  </Link>
                </Box> */}
                <Box
                  className={
                    isFocus
                      ? classes.boxBtnRegister
                      : classes.boxBtnRegisterFixed
                  }
                >
                  <ButtonComponent
                    backgroundColor="initial"
                    borderRadius={8}
                    height={52}
                    padding="10px 40px"
                    fontSize={16}
                    fontWeight={600}
                    typeButton="outlined"
                    borderColor="#FF1284"
                    color="#FF1284"
                    handleClick={() =>
                      router.push({
                        pathname: routeStatic.REGISTER_ACCOUNT.href,
                      })
                    }
                  >
                    Đăng ký tài khoản
                  </ButtonComponent>
                </Box>
              </Form>
            );
          }}
        </Formik>
      </Box>
    </Layout>
  );
};

export default Login;
