import dynamic from "next/dynamic";

import { checkIsMobile } from "@utils/helpers";

const DesktopContent = dynamic(() =>
  import("@components/desktop/hotelHotDeal/DesktopContent")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/hotels/top-hotel/MobileContent")
);

const HotelHotDealContainer = ({
  isMobile = false,
  hotelHotDeal = [],
  totalHotels = 0,
}) => {
  return (
    <>
      {isMobile ? (
        <MobileContent />
      ) : (
        <DesktopContent hotelHotDeal={hotelHotDeal} totalHotels={totalHotels} />
      )}
    </>
  );
};
export default HotelHotDealContainer;

export async function getServerSideProps({ params, req, res, query }) {
  let hotelHotDeal = [];
  let totalHotels = 0;
  // try {
  //   const { data } = await getTopHotels({
  //     page: 1,
  //     size: 20,
  //     ...paramsDefaultHotel(),
  //   });
  //   hotelHotDeal = data?.data?.items || [];
  //   totalHotels = data?.data.total || 0;
  // } catch (error) {}
  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
      hotelHotDeal,
      totalHotels,
    },
  };
}
