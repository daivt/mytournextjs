import { useSnackbar } from "notistack";
import { useRouter } from "next/router";
import { Box } from "@material-ui/core";
import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import OtpInput from "react-otp-input";

import { sendSignupOTP } from "@api/user";
import { listString, LAST_REGISTER_INFO, routeStatic } from "@utils/constants";

import snackbarSetting from "@src/alert/Alert";
import ButtonComponent from "@src/button/Button";
import Layout from "@components/layout/mobile/Layout";

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
    padding: "12px 32px",
    borderTop: "1px solid #EDF2F7",
  },
  iconPhone: {
    fill: theme.palette.primary.main,
  },
  otpHaveValue: {
    // "& input": {
    //   backgroundColor: theme.palette.primary.main,
    //   color: theme.palette.white.main,
    //   borderColor: theme.palette.primary.main,
    //   "&:focus": {
    //     backgroundColor: theme.palette.primary.main,
    //     color: theme.palette.white.main,
    //     borderColor: theme.palette.primary.main,
    //   },
    // },
  },
  containerStyle: {
    width: "100%",
  },
  inputStyle: {
    textAlign: "center",
    borderRadius: "50%",
    border: `1px solid ${theme.palette.primary.main}`,
    fontSize: 24,
    width: "50px !important",
    height: 50,
    margin: "0 4px",
    fontWeight: 600,
    color: theme.palette.primary.main,
    "&:focus": {
      outline: "none",
    },
  },
}));

let timeInterval = null;
let countDownTemp = 120;

const RegisterStep2 = () => {
  const classes = useStyles();
  const router = useRouter();

  const [lastRegisterInfo, setLastRegisterInfo] = useState({});
  const [countDown, setCountDown] = useState(120);
  const [otp, setOtp] = useState("");

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  useEffect(() => {
    const lastRegisterInfo =
      JSON.parse(localStorage.getItem(LAST_REGISTER_INFO)) || {};
    setLastRegisterInfo(lastRegisterInfo);
    genCountDown();
    countDownTemp = 120;
    return () => {
      clearInterval(timeInterval);
    };
  }, []);

  const genCountDown = () => {
    timeInterval = setInterval(() => {
      if (countDownTemp === 0) {
        countDownTemp = 120;
        clearInterval(timeInterval);
      } else {
        countDownTemp = countDownTemp - 1;
        setCountDown(countDownTemp);
      }
    }, 1000);
  };

  const handleSubmitOtp = () => {
    if (otp.length === 6) {
      const values = {
        ...lastRegisterInfo,
        otp: otp,
      };
      localStorage.setItem(LAST_REGISTER_INFO, JSON.stringify(values));
      router.push({
        pathname: routeStatic.REGISTER_ACCOUNT_STEP_3.href,
      });
    } else {
      enqueueSnackbar(
        "Vui lòng nhập đầy đủ mã OTP",
        snackbarSetting((key) => closeSnackbar(key), {
          color: "error",
        })
      );
    }
  };
  const reSendOtp = async () => {
    try {
      const dataDto = {
        phone: lastRegisterInfo.phone,
      };
      const { data } = await sendSignupOTP(dataDto);
      if (data.code === 200) {
        countDownTemp = 120;
        genCountDown();
        enqueueSnackbar(
          data?.message,
          snackbarSetting((key) => closeSnackbar(key), {
            color: "success",
          })
        );
      } else {
        data?.message &&
          enqueueSnackbar(
            data?.message,
            snackbarSetting((key) => closeSnackbar(key), {
              color: "error",
            })
          );
      }
    } catch (error) {}
  };

  const handleChange = (otp) => {
    setOtp(otp);
  };
  return (
    <Layout
      isHeaderBack
      isShowTitleInit
      titleHeaderBack={listString.IDS_MT_TEXT_SIGNUP}
    >
      <Box className={classes.container}>
        <Box display="flex" flexDirection="column" alignItems="center" pb={3}>
          <Box component="span" lineHeight="17px">
            {listString.IDS_MT_TEXT_OTP_SEND_PHONE}
          </Box>
          <Box component="span" lineHeight="21px" fontSize={18} pt={6 / 8}>
            {lastRegisterInfo.phone}
          </Box>
        </Box>
        <Box pb={28 / 8} mx={-12 / 8} display="flex" justifyContent="center">
          <OtpInput
            onChange={handleChange}
            numInputs={6}
            value={otp}
            shouldAutoFocus
            isInputNum
            inputStyle={classes.inputStyle}
            containerStyle={classes.containerStyle}
          />
        </Box>
        <ButtonComponent
          backgroundColor="#FF1284"
          borderRadius={8}
          height={52}
          padding="10px 40px"
          fontSize={16}
          fontWeight={600}
          handleClick={handleSubmitOtp}
        >
          {listString.IDS_MT_TEXT_CONFIRM}
        </ButtonComponent>
        <Box
          display="flex"
          justifyContent="center"
          alignItems="center"
          lineHeight="17px"
          pt={3}
        >
          <ButtonComponent
            padding="8px 4px"
            fontSize={14}
            height={17}
            typeButton="text"
            color={countDown !== 0 ? "#1a202c" : "#00B6F3"}
            width="fit-content"
            handleClick={() => {
              if (countDown === 0) {
                reSendOtp();
              }
            }}
            disableRipple={countDown !== 0}
          >
            Gửi lại mã.
          </ButtonComponent>
          {countDown !== 0 && <Box color="primary.main">{`${countDown}s`}</Box>}
        </Box>
      </Box>
    </Layout>
  );
};

export default RegisterStep2;
