import * as yup from "yup";
import { Formik, Form } from "formik";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { Box } from "@material-ui/core";
import { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";

import { sendSignupOTP } from "@api/user";
import ButtonComponent from "@src/button/Button";
import { IconPhoneContact } from "@public/icons";
import { listString, routeStatic, LAST_REGISTER_INFO } from "@utils/constants";

import snackbarSetting from "@src/alert/Alert";
import Layout from "@components/layout/mobile/Layout";
import FieldTextContent from "@src/form/FieldTextContent";
import ButtonLoginViaGGFB from "@components/common/login/ButtonLoginViaGGFB";

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
    padding: "12px 32px",
  },
  iconPhone: {
    fill: theme.palette.primary.main,
  },
}));

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
let redirectUriTemp = "";
const Register = () => {
  const router = useRouter();
  const classes = useStyles();
  const [isClient, setIsClient] = useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const storeSchema = yup.object().shape({
    phone: yup
      .string()
      .matches(phoneRegExp, listString.IDS_MT_TEXT_INVALID_PHONE_NUMBER)
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_PHONE)
      .min(9, listString.IDS_MT_TEXT_INVALID_PHONE_NUMBER)
      .max(11, listString.IDS_MT_TEXT_INVALID_PHONE_NUMBER),
  });

  useEffect(() => {
    const IS_SERVER = typeof window === "undefined";
    if (!IS_SERVER) {
      redirectUriTemp = `${window.location.origin}/dang-ky`;
    }
    setIsClient(true);
  }, []);

  return (
    <Layout
      isHeaderBack
      isShowTitleInit
      titleHeaderBack={listString.IDS_MT_TEXT_SIGNUP}
    >
      <Box className={classes.container}>
        {isClient && (
          <ButtonLoginViaGGFB isLogin={false} redirectUri={redirectUriTemp} />
        )}

        <Box
          lineHeight="17px"
          display="flex"
          justifyContent="center"
          pt={4}
          pb={12 / 8}
        >
          {listString.IDS_MT_TEXT_REGISTER_PHONE}
        </Box>
        <Formik
          initialValues={{
            phone: "",
          }}
          onSubmit={async (values) => {
            try {
              const { data } = await sendSignupOTP(values);
              if (data.code === 200) {
                localStorage.setItem(
                  LAST_REGISTER_INFO,
                  JSON.stringify(values)
                );
                router.push({
                  pathname: routeStatic.REGISTER_ACCOUNT_STEP_2.href,
                });
              } else {
                data?.message &&
                  enqueueSnackbar(
                    data?.message,
                    snackbarSetting((key) => closeSnackbar(key), {
                      color: "error",
                    })
                  );
              }
            } catch (error) {}
          }}
          validationSchema={storeSchema}
        >
          {({}) => {
            return (
              <Form>
                <FieldTextContent
                  name="phone"
                  optional
                  placeholder={
                    listString.IDS_MT_TEXT_PLACEHOLDER_ENTER_PHONE_NUMBER
                  }
                  inputStyle={{
                    border: "none",
                    height: 52,
                    backgroundColor: "#EDF2F7",
                    borderRadius: 8,
                    paddingLeft: 16,
                  }}
                  inputProps={{
                    autoComplete: "off",
                  }}
                  startAdornment={
                    <IconPhoneContact
                      className={`svgFillAll ${classes.iconPhone}`}
                    />
                  }
                />
                <ButtonComponent
                  backgroundColor="#FF1284"
                  borderRadius={8}
                  height={52}
                  padding="10px 40px"
                  fontSize={16}
                  fontWeight={600}
                  type="submit"
                >
                  {listString.IDS_MT_TEXT_SIGNUP}
                </ButtonComponent>
              </Form>
            );
          }}
        </Formik>
      </Box>
    </Layout>
  );
};

export default Register;
