import dynamic from "next/dynamic";

const InstallApp = dynamic(() =>
  import("@components/desktop/home/install/InstallApp")
);

export default function InstallAppLanding() {
  return <InstallApp />;
}
