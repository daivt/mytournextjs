import dynamic from "next/dynamic";

import { getHotelDetails } from "@api/hotels";

const DesktopContent = dynamic(() =>
  import("@components/desktop/hotels/booking/DesktopContent")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/hotels/booking/MobileContent")
);

import { isEmpty, checkIsMobile, checkAndChangeCheckIO } from "@utils/helpers";

export default function Home({
  isMobile = false,
  hotelDetail = {},
  paramsInit = {},
}) {
  return (
    <>
      {isMobile ? (
        <MobileContent hotelDetail={hotelDetail} paramsInit={paramsInit} />
      ) : (
        <DesktopContent hotelDetail={hotelDetail} paramsInit={paramsInit} />
      )}
    </>
  );
}

export async function getServerSideProps({ params, req, res, query }) {
  let hotelDetail = {};
  let paramsInit = {};
  try {
    const dataDto = {
      checkIn: checkAndChangeCheckIO(query.checkIn, "checkIn"),
      checkOut: checkAndChangeCheckIO(query.checkOut, "checkOut"),
      adults: parseInt(query.adults) || 2,
      children: parseInt(query.children) || 0,
      rooms: parseInt(query.rooms) || 1,
      hotelId: query.hotelId,
    };
    paramsInit = {
      ...dataDto,
      roomKey: query.roomKey,
      rateKey: query.rateKey,
    };
    if (!isEmpty(query.childrenAges)) {
      paramsInit = {
        ...paramsInit,
        childrenAges: query.childrenAges.split(",").map(Number),
      };
    }
    const { data } = await getHotelDetails(dataDto);
    hotelDetail = data.data || {};
  } catch (error) {}
  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
      hotelDetail,
      paramsInit,
    },
  };
}
