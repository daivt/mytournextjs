import dynamic from "next/dynamic";

const DesktopContent = dynamic(() =>
  import("@components/desktop/hotels/resultBooking/DesktopContent")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/hotels/resultBooking/MobileContent")
);

import { checkIsMobile } from "@utils/helpers";

export default function Home({ isMobile = false }) {
  return <>{isMobile ? <MobileContent /> : <DesktopContent />}</>;
}

export async function getServerSideProps({ params, req, res, query }) {
  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
    },
  };
}
