import { useEffect } from "react";
import dynamic from "next/dynamic";

import { getHotelsAvailability } from "@api/hotels";
import { formatDate, listEventHotel } from "@utils/constants";
import { listFieldFilterHotel, dataUserVote } from "@utils/dataFake";
import {
  isEmpty,
  checkIsMobile,
  getHeaderInServer,
  paramsDefaultHotel,
  checkAndChangeCheckIO,
} from "@utils/helpers";
import moment from "moment";
import * as gtm from "@utils/gtm";

const DesktopContent = dynamic(() =>
  import("@components/desktop/hotels/listing/DesktopContent")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/hotels/listing/MobileContent")
);

const ListingHotel = ({
  isMobile = false,
  location = {},
  sortOptions = [],
  filters = {},
  items = [],
  totalHotels = 0,
  paramsQuery = {},
  filtersQuery = {},
  size = 40,
  listingTitle,
  timeServer = "",
}) => {
  useEffect(() => {
    pushGtm(location);
  }, []);

  const pushGtm = (location = {}) => {
    let addressGtm = {};
    const details = location?.details || [];
    details.forEach((el, index) => {
      if (index === 0) {
        // add province
        addressGtm = {
          ...addressGtm,
          provinceId: el?.id || "",
          provinceName: el?.name || "",
        };
      } else if (index === 1) {
        // add district
        addressGtm = {
          ...addressGtm,
          districtId: el?.id || "",
          districtName: el?.name || "",
        };
      }
    });
    gtm.addEventGtm(listEventHotel.HotelViewList, { address: addressGtm });
  };

  return (
    <>
      {isMobile ? (
        <MobileContent
          paramsQuery={paramsQuery}
          filtersQuery={filtersQuery}
          sortOptions={sortOptions}
          location={location}
          filters={filters}
          items={items}
          totalHotels={totalHotels}
          timeServer={timeServer}
        />
      ) : (
        <DesktopContent
          paramsQuery={paramsQuery}
          filtersQuery={filtersQuery}
          sortOptions={sortOptions}
          location={location}
          filters={filters}
          items={items}
          totalHotels={totalHotels}
          size={size}
          listingTitle={listingTitle}
        />
      )}
    </>
  );
};

export default ListingHotel;

export async function getServerSideProps({ params, req, res, query }) {
  let totalHotels = 0;
  let location = {};
  let sortOptions = [];
  let filters = {};
  let items = [];
  let paramsQuery = {};
  let filtersQuery = {};
  let listingTitle = "";
  let slugName = "";
  let timeServer = "";
  const headerResult = getHeaderInServer(req.headers || {});
  try {
    const keys = Object.keys(query);
    keys.forEach((el) => {
      if (listFieldFilterHotel.includes(el)) {
        if (el === "minPrice" || el === "maxPrice") {
          filtersQuery = {
            ...filtersQuery,
            priceRanges: [
              {
                minPrice: !isEmpty(query.minPrice)
                  ? parseInt(query.minPrice)
                  : 0,
                maxPrice:
                  !isEmpty(query.maxPrice) && parseInt(query.maxPrice) < 5000000
                    ? parseInt(query.maxPrice)
                    : 1000000000,
              },
            ],
          };
        } else if (el === "ratingRanges") {
          let ratingRangesTemp = [];
          const ratingRangesKeys =
            query.ratingRanges.split(",").map(Number) || [];
          ratingRangesKeys.forEach((el) => {
            ratingRangesTemp.push(dataUserVote[el].value);
          });
          filtersQuery = { ...filtersQuery, ratingRanges: ratingRangesTemp };
        } else {
          filtersQuery = {
            ...filtersQuery,
            [el]: !isEmpty(query[el]) ? query[el].split(",").map(Number) : [],
          };
        }
      }
    });
    slugName = query.slug[1]
      .replace(`khach-san-tai-`, "")
      .replace(`.html`, "")
      .split("-")
      .map((v) => `${v.charAt(0).toUpperCase()}${v.slice(1)}`)
      .join(" ");
    paramsQuery = {
      aliasCode: query.slug[0],
      listing: query.slug[1],
      page: !isEmpty(query.page) ? Number(query.page) : 1,
      size: checkIsMobile(req.headers["user-agent"]) ? 40 : 20,
      checkIn: checkAndChangeCheckIO(query.checkIn, "checkIn"),
      checkOut: checkAndChangeCheckIO(query.checkOut, "checkOut"),
      adults: parseInt(query.adults) || paramsDefaultHotel().adults,
      rooms: parseInt(query.rooms) || paramsDefaultHotel().rooms,
      children: parseInt(query.children) || paramsDefaultHotel().children,
      sortBy: query.sortBy || "",
      isFirstRequest: true,
    };
    if (!isEmpty(query.childrenAges)) {
      paramsQuery = {
        ...paramsQuery,
        childrenAges: query.childrenAges.split(",").map(Number),
      };
    }
    if (query.slug[0] === "ksgb") {
      paramsQuery = {
        ...paramsQuery,
        latitude: query.latitude || null,
        longitude: query.longitude || null,
        searchType: "latlon",
        radius: 10,
      };
    } else {
      if (query.chain) {
        paramsQuery = { ...paramsQuery, chainId: query.chain || null };
        delete paramsQuery.aliasCode;
        delete paramsQuery.searchType;
      } else {
        paramsQuery = {
          ...paramsQuery,
          locationCode: query.locationCode || null,
          searchType: query.searchType || "location",
        };
      }
    }
    const dataDTO = { ...paramsQuery, filters: filtersQuery };
    let headers = {};
    if (!isEmpty(headerResult.token)) {
      headers = {
        ...headers,
        login_token: headerResult.token,
      };
    }
    const { data } = await getHotelsAvailability(dataDTO, headers);
    if (data.code === 200 && !isEmpty(data.data)) {
      location = data?.data?.location || {};
      filters = data?.data?.filters || {};
      sortOptions = data?.data?.sortOptions || [];
      items = data?.data?.items || [];
      totalHotels = data?.data?.total;
      const listing = paramsQuery.listing;
      const filterInfo = listing
        .replace(
          `-tai-${!isEmpty(location.name) ? location.name.stringSlug() : ""}`,
          ""
        )
        .replace(`khach-san-`, "");
      const amenitiesItem = filters.amenities.find((v) =>
        filterInfo.includes(v.name.stringSlug())
      );
      let tempAmenities = !isEmpty(amenitiesItem) ? [amenitiesItem.id] : [];
      if (!isEmpty(query.amenities)) {
        tempAmenities = [
          ...tempAmenities,
          ...query.amenities.split(",").map((v) => Number(v)),
        ];
      }
      const servicesItem = filters.services.find((v) =>
        filterInfo.includes(v.name.stringSlug())
      );
      let tempServices = !isEmpty(servicesItem) ? [servicesItem.id] : [];
      if (!isEmpty(query.services)) {
        tempServices = [
          ...tempServices,
          ...query.services.split(",").map((v) => Number(v)),
        ];
      }
      const hotelStarItem = [5, 4, 3, 2, 1].find((v) =>
        filterInfo.includes(`${v}-sao`)
      );
      listingTitle = `Khách sạn${hotelStarItem ? ` ${hotelStarItem} sao` : ""}${
        amenitiesItem ? ` ${amenitiesItem.name}` : ""
      }${servicesItem ? ` ${servicesItem.name}` : ""} ${
        query.type === "chain" ? "thuộc chuỗi" : "tại"
      } ${!isEmpty(location.name) ? location.name : slugName}`;

      let tempHotelStar = !isEmpty(hotelStarItem) ? [hotelStarItem] : [];

      if (!isEmpty(query.stars)) {
        tempHotelStar = [
          ...tempHotelStar,
          ...query.stars.split(",").map((v) => Number(v)),
        ];
      }
      filtersQuery = {
        ...filtersQuery,
        amenities: tempAmenities,
        services: tempServices,
        stars: tempHotelStar,
        ratingRanges: !isEmpty(query.ratingRanges)
          ? query.ratingRanges.split(",").map((v) => Number(v))
          : [],
        types: !isEmpty(query.types)
          ? query.types.split(",").map((v) => Number(v))
          : [],
      };
    }
    timeServer = moment().format(formatDate.firstDay);
  } catch (error) {}
  return {
    props: {
      totalHotels,
      location: {
        ...location,
        name: !isEmpty(location.name) ? location.name : slugName,
      },
      isMobile: checkIsMobile(req.headers["user-agent"]),
      sortOptions,
      filters,
      items,
      paramsQuery: { ...paramsQuery, isFirstRequest: false },
      filtersQuery,
      listingTitle,
      timeServer,
    },
  };
}
