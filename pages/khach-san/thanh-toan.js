import dynamic from "next/dynamic";

import { getHotelDetails } from "@api/hotels";
import { paramsDefaultHotel } from "@utils/helpers";

const DesktopContent = dynamic(() =>
  import("@components/mobile/hotels/payment/MobileContent")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/hotels/payment/MobileContent")
);

import { checkIsMobile } from "@utils/helpers";

export default function Home({ isMobile = false, hotelDetail = {} }) {
  return (
    <>
      {isMobile ? (
        <MobileContent hotelDetail={hotelDetail} />
      ) : (
        <DesktopContent hotelDetail={hotelDetail} />
      )}
    </>
  );
}

export async function getServerSideProps({ params, req, res, query }) {
  let hotelDetail = {};
  try {
    const dataDto = {
      ...paramsDefaultHotel(),
      hotelId: query.hotelId,
    };
    const { data } = await getHotelDetails(dataDto);
    hotelDetail = data.data || {};
  } catch (error) {}
  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
      hotelDetail,
    },
  };
}
