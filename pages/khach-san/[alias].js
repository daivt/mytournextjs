import { useEffect } from "react";
import dynamic from "next/dynamic";

import { getHotelDetails } from "@api/hotels";
import { listEventHotel } from "@utils/constants";

import * as gtm from "@utils/gtm";
import {
  isEmpty,
  checkIsMobile,
  getHeaderInServer,
  checkAndChangeCheckIO,
  paramsDefaultHotel,
} from "@utils/helpers";

const DesktopContent = dynamic(() =>
  import("@components/desktop/hotels/details/DesktopContent")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/hotels/details/MobileContent")
);

const hotelDetail = ({
  isMobile = false,
  paramsInit = {},
  hotelDetail = {},
}) => {
  useEffect(() => {
    gtm.addEventGtm(listEventHotel.HotelViewDetail, {
      hotelId: hotelDetail?.id || "",
      starNumber: hotelDetail?.starNumber,
    });
  }, []);
  return (
    <>
      {isMobile ? (
        <MobileContent paramsInit={paramsInit} hotelDetail={hotelDetail} />
      ) : (
        <DesktopContent paramsInit={paramsInit} hotelDetail={hotelDetail} />
      )}
    </>
  );
};

export async function getServerSideProps({ params, req, res, query }) {
  let paramsInit = {};
  let hotelDetail = {};
  const headerResult = getHeaderInServer(req.headers || {});
  try {
    paramsInit = {
      checkIn: checkAndChangeCheckIO(query.checkIn, "checkIn"),
      checkOut: checkAndChangeCheckIO(query.checkOut, "checkOut"),
      adults: parseInt(query.adults) || paramsDefaultHotel().adults,
      children: parseInt(query.children) || paramsDefaultHotel().children,
      rooms: parseInt(query.rooms) || paramsDefaultHotel().rooms,
      hotelId: query.alias.split("-")[0],
    };
    if (!isEmpty(query.childrenAges)) {
      paramsInit = {
        ...paramsInit,
        childrenAges: query.childrenAges.split(",").map(Number),
      };
    }
    let headers = {};
    if (!isEmpty(headerResult.token)) {
      headers = {
        ...headers,
        login_token: headerResult.token,
      };
    }
    const { data } = await getHotelDetails(paramsInit, headers);
    if (data.code === 200) {
      hotelDetail = data.data || {};
      const aliasHotel = `${
        hotelDetail.id
      }-${hotelDetail?.name.stringSlug()}.html`;
      if (query.alias !== aliasHotel) {
        res.writeHead(301, {
          Location: aliasHotel,
        });
        res.end();
      }
    } else if (data.code === 4102 || data.code === 4103) {
      console.log("not exits");
      res.writeHead(301, {
        Location: `/?detail=true`,
      });
      res.end();
    }
  } catch (error) {}

  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
      paramsInit,
      hotelDetail,
    },
  };
}

export default hotelDetail;
