import dynamic from "next/dynamic";

const DesktopContent = dynamic(() =>
  import("@components/desktop/hotels/topHotel/DesktopContent")
);

const MobileContent = dynamic(() =>
  import("@components/mobile/hotels/listCity/MobileContent")
);

import { getTopLocation } from "@api/homes";
import { checkIsMobile } from "@utils/helpers";

const TopHotel = ({ isMobile = false, topLocation = [] }) => {
  return (
    <>
      {isMobile ? (
        <MobileContent topLocation={topLocation} />
      ) : (
        <DesktopContent topLocation={topLocation} />
      )}
    </>
  );
};

export default TopHotel;

export async function getServerSideProps({ params, req, res }) {
  let topLocation = [];
  try {
    const { data } = await getTopLocation({ page: 1, size: 25 });
    topLocation = data?.data?.items || [];
  } catch (error) {}
  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
      topLocation,
    },
  };
}
