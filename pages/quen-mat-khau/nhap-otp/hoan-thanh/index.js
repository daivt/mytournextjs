import * as yup from "yup";
import { Formik, Form } from "formik";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { makeStyles } from "@material-ui/styles";
import { Box, InputAdornment, IconButton } from "@material-ui/core";

import { forgotPasswordV2 } from "@api/user";
import ButtonComponent from "@src/button/Button";
import { IconVisibility, IconVisibilityOff } from "@public/icons";
import { listString, routeStatic, LAST_REGISTER_INFO } from "@utils/constants";

import snackbarSetting from "@src/alert/Alert";
import Layout from "@components/layout/mobile/Layout";
import FieldTextContent from "@src/form/FieldTextContent";

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
  },
  iconPhone: {
    fill: theme.palette.primary.main,
  },
}));

const ForgotPasswordStep3 = () => {
  const router = useRouter();
  const classes = useStyles();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const storeSchema = yup.object().shape({
    password: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_PASSWORD)
      .min(6, listString.IDS_MT_TEXT_VALIDATE_PASSWORD)
      .max(50, listString.IDS_MT_TEXT_VALIDATE_PASSWORD),
    rePassword: yup
      .string()
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_CONFIRM_PASSWORD)
      .oneOf(
        [yup.ref("password"), null],
        listString.IDS_MT_TEXT_PASSWORD_CONFIRM_NOT_MATCH_PASSWORD
      )
      .min(6, listString.IDS_MT_TEXT_VALIDATE_PASSWORD)
      .max(50, listString.IDS_MT_TEXT_VALIDATE_PASSWORD),
  });
  return (
    <Layout>
      <Box className={classes.container}>
        <Box
          display="flex"
          alignItems="center"
          justifyContent="center"
          height={48}
          fontSize={16}
          fontWeight={600}
          borderBottom="1px solid #EDF2F7"
        >
          {listString.IDS_MT_TEXT_FORGET_PASSWORD}
        </Box>
        <Formik
          initialValues={{
            password: "",
            rePassword: "",
            showPassword: false,
            showRePassword: false,
          }}
          onSubmit={async (values) => {
            const lastRegisterInfo =
              JSON.parse(localStorage.getItem(LAST_REGISTER_INFO)) || {};
            const dataDTO = {
              ...values,
              ...lastRegisterInfo,
              user_name: lastRegisterInfo.phone,
            };
            delete dataDTO.phone;
            delete dataDTO.showPassword;
            delete dataDTO.rePassword;
            delete dataDTO.showRePassword;
            try {
              const { data } = await forgotPasswordV2(dataDTO);
              if (data.code === 200) {
                localStorage.removeItem(LAST_REGISTER_INFO);
                router.push({
                  pathname: routeStatic.LOGIN.href,
                });
                data?.message &&
                  enqueueSnackbar(
                    listString.IDS_MT_TEXT_FORGET_PASSWORD_SUCCESS,
                    snackbarSetting((key) => closeSnackbar(key), {
                      color: "success",
                    })
                  );
              } else {
                data?.message &&
                  enqueueSnackbar(
                    data?.message,
                    snackbarSetting((key) => closeSnackbar(key), {
                      color: "error",
                    })
                  );
              }
            } catch (error) {}
          }}
          validationSchema={storeSchema}
        >
          {({ values, setFieldValue }) => {
            return (
              <Form>
                <Box p="24px 32px 0">
                  <FieldTextContent
                    name="password"
                    type={values.showPassword ? "text" : "password"}
                    optional
                    placeholder={listString.IDS_MT_TEXT_NEW_PASSWORD}
                    inputStyle={{
                      border: "none",
                      height: 52,
                      backgroundColor: "#EDF2F7",
                      borderRadius: 8,
                      paddingLeft: 16,
                      paddingRight: 16,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={() => {
                            setFieldValue("showPassword", !values.showPassword);
                          }}
                          edge="end"
                        >
                          {values.showPassword ? (
                            <IconVisibility />
                          ) : (
                            <IconVisibilityOff />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                  <FieldTextContent
                    name="rePassword"
                    optional
                    placeholder={listString.IDS_MT_TEXT_RE_PASSWORD}
                    type={values.showRePassword ? "text" : "password"}
                    inputStyle={{
                      border: "none",
                      height: 52,
                      backgroundColor: "#EDF2F7",
                      borderRadius: 8,
                      paddingLeft: 16,
                      paddingRight: 16,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={() => {
                            setFieldValue(
                              "showRePassword",
                              !values.showRePassword
                            );
                          }}
                          edge="end"
                        >
                          {values.showRePassword ? (
                            <IconVisibility />
                          ) : (
                            <IconVisibilityOff />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                  <ButtonComponent
                    backgroundColor="#FF1284"
                    borderRadius={8}
                    height={52}
                    padding="10px 40px"
                    fontSize={16}
                    fontWeight={600}
                    type="submit"
                  >
                    {listString.IDS_MT_TEXT_CHANGE_PASSWORD}
                  </ButtonComponent>
                </Box>
              </Form>
            );
          }}
        </Formik>
      </Box>
    </Layout>
  );
};

export default ForgotPasswordStep3;
