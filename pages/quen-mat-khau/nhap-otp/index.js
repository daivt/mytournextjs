import { useSnackbar } from "notistack";
import { useRouter } from "next/router";
import { Box, InputBase } from "@material-ui/core";
import { useEffect, useState, useRef } from "react";
import { makeStyles, withStyles } from "@material-ui/styles";

import { isEmpty } from "utils/helpers";
import { sendSignupOTP } from "@api/user";
import { listString, LAST_REGISTER_INFO, routeStatic } from "@utils/constants";

import snackbarSetting from "@src/alert/Alert";
import ButtonComponent from "@src/button/Button";
import Layout from "@components/layout/mobile/Layout";

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
    padding: "12px 32px",
    borderTop: "1px solid #EDF2F7",
  },
  iconPhone: {
    fill: theme.palette.primary.main,
  },
  otpHaveValue: {
    "& input": {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.white.main,
      borderColor: theme.palette.primary.main,
      "&:focus": {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.white.main,
        borderColor: theme.palette.primary.main,
      },
    },
  },
}));

const BootstrapInput = withStyles((theme) => ({
  root: {
    "label + &": {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    textAlign: "center",
    borderRadius: "50%",
    position: "relative",
    border: `1px solid ${theme.palette.gray.grayLight22}`,
    backgroundColor: theme.palette.gray.grayLight22,
    fontSize: 24,
    width: 50,
    height: 50,
    padding: 0,
    margin: "0 4px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    lineHeight: 29,
    fontWeight: 600,
    color: theme.palette.primary.main,
    "&:focus": {
      borderColor: theme.palette.primary.main,
      backgroundColor: "#fff",
    },
  },
}))(InputBase);
const listOtp = [1, 2, 3, 4, 5, 6];
const validNumberRegex = /^[0-9]*$/;
// let indexFocus = 999;

const ForgotPasswordStep2 = () => {
  const classes = useStyles();
  const router = useRouter();

  const [lastRegisterInfo, setLastRegisterInfo] = useState({});
  const inputRef = useRef([]);
  const [otpValue, setOtpValue] = useState({
    1: "",
    2: "",
    3: "",
    4: "",
    5: "",
    6: "",
  });

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  useEffect(() => {
    const lastRegisterInfo =
      JSON.parse(localStorage.getItem(LAST_REGISTER_INFO)) || {};
    setLastRegisterInfo(lastRegisterInfo);
    inputRef.current[1].focus();
    // window.addEventListener("keydown", handleKeydown);
    // return () => {
    //   window.removeEventListener("keydown", () => handleKeydown);
    // };
  }, []);

  const handleKeydown = (event) => {
    const key = event.key; // const {key} = event; ES6+
    if (key === "Backspace" || key === "Delete") {
      // inputRef.current.forEach((el, idx) => {
      if (indexFocus > 0 && indexFocus < 7) {
        // setOtpValue({
        //   ...otpValue,
        //   [indexFocus]: "",
        // });
        if (indexFocus > 1) {
          inputRef.current[indexFocus - 1].focus();
        }
      }
      // });
    }
  };

  const handleSubmitOtp = () => {
    let otpTemp = "";
    listOtp.forEach((el) => {
      otpTemp += otpValue[el];
    });
    if (otpTemp.length === 6) {
      const values = {
        ...lastRegisterInfo,
        otp: otpTemp,
      };
      localStorage.setItem(LAST_REGISTER_INFO, JSON.stringify(values));
      router.push({
        pathname: routeStatic.FORGET_PASSWORD_STEP_3.href,
      });
    } else {
      enqueueSnackbar(
        "Vui lòng nhập đầy đủ mã OTP",
        snackbarSetting((key) => closeSnackbar(key), {
          color: "error",
        })
      );
    }
  };
  return (
    <Layout
      isHeaderBack
      isShowTitleInit
      titleHeaderBack={listString.IDS_MT_TEXT_FORGET_PASSWORD}
    >
      <Box className={classes.container}>
        <Box display="flex" flexDirection="column" alignItems="center" pb={3}>
          <Box component="span" lineHeight="17px">
            {listString.IDS_MT_TEXT_OTP_SEND_PHONE}
          </Box>
          <Box component="span" lineHeight="21px" fontSize={18} pt={6 / 8}>
            {lastRegisterInfo.phone}
          </Box>
        </Box>
        <Box pb={28 / 8} mx={-12 / 8} display="flex" justifyContent="center">
          {listOtp.map((idx) => (
            <BootstrapInput
              key={idx}
              id={`otp-${idx}`}
              inputProps={{
                autoComplete: "off",
                align: "right",
              }}
              // onFocus={() => {
              //   indexFocus = idx;
              // }}
              value={otpValue[idx]}
              inputRef={(el) => (inputRef.current[idx] = el)}
              className={!isEmpty(otpValue[idx]) ? classes.otpHaveValue : ""}
              onChange={(e) => {
                if (e.target.value.length === 0) {
                  setOtpValue({
                    ...otpValue,
                    [idx]: e.target.value,
                  });
                }
                if (
                  validNumberRegex.test(e.target.value) &&
                  e.target.value.length === 1
                ) {
                  if (idx < 6) {
                    inputRef.current[idx + 1].focus();
                  }
                  setOtpValue({
                    ...otpValue,
                    [idx]: e.target.value,
                  });
                }
                if (
                  validNumberRegex.test(e.target.value) &&
                  e.target.value.length === 2
                ) {
                  setOtpValue({
                    ...otpValue,
                    [idx]: e.target.value[1],
                  });
                  if (idx < 6) {
                    inputRef.current[idx + 1].focus();
                  }
                }
              }}
            />
          ))}
        </Box>
        <ButtonComponent
          backgroundColor="#FF1284"
          borderRadius={8}
          height={52}
          padding="10px 40px"
          fontSize={16}
          fontWeight={600}
          handleClick={handleSubmitOtp}
        >
          {listString.IDS_MT_TEXT_CONFIRM}
        </ButtonComponent>
        <Box display="flex" justifyContent="center" lineHeight="17px" pt={3}>
          <Box pr={2 / 8}>Gửi lại mã.</Box>
          <Box color="primary.main">50s</Box>
        </Box>
      </Box>
    </Layout>
  );
};

export default ForgotPasswordStep2;
