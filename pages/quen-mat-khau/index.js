import * as yup from "yup";
import { Formik, Form } from "formik";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

import { sendForgotPasswordOTP } from "@api/user";
import ButtonComponent from "@src/button/Button";
import { IconPhoneContact } from "@public/icons";
import { listString, routeStatic, LAST_REGISTER_INFO } from "@utils/constants";

import snackbarSetting from "@src/alert/Alert";
import Layout from "@components/layout/mobile/Layout";
import FieldTextContent from "@src/form/FieldTextContent";

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.black.black3,
    fontSize: 14,
    padding: "12px 32px",
  },
  desForgetPassword: {
    width: "calc(100% + 64px)",
    margin: "0 -32px",
    padding: "16px 32px 12px",
    display: "flex",
    textAlign: "center",
    lineHeight: "22px",
    justifyContent: "center",
    borderTop: "1px solid #EDF2F7",
  },
  iconPhone: {
    fill: theme.palette.primary.main,
  },
}));

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

const ForgotPassword = () => {
  const router = useRouter();
  const classes = useStyles();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const storeSchema = yup.object().shape({
    phone: yup
      .string()
      .matches(phoneRegExp, listString.IDS_MT_TEXT_INVALID_PHONE_NUMBER)
      .trim()
      .required(listString.IDS_MT_TEXT_PLEASE_ENTER_PHONE),
  });
  return (
    <Layout
      isHeaderBack
      isShowTitleInit
      titleHeaderBack={listString.IDS_MT_TEXT_FORGET_PASSWORD}
    >
      <Box className={classes.container}>
        <Box className={classes.desForgetPassword}>
          {listString.IDS_MT_TEXT_FORGOT_PASSWORD_DES}
        </Box>
        <Formik
          initialValues={{
            phone: "",
          }}
          onSubmit={async (values) => {
            try {
              const { data } = await sendForgotPasswordOTP(values);
              if (data.code === 200) {
                localStorage.setItem(
                  LAST_REGISTER_INFO,
                  JSON.stringify(values)
                );
                router.push({
                  pathname: routeStatic.FORGET_PASSWORD_STEP_2.href,
                });
              } else {
                data?.message &&
                  enqueueSnackbar(
                    data?.message,
                    snackbarSetting((key) => closeSnackbar(key), {
                      color: "error",
                    })
                  );
              }
            } catch (error) {}
          }}
          validationSchema={storeSchema}
        >
          {({}) => {
            return (
              <Form>
                <FieldTextContent
                  name="phone"
                  optional
                  placeholder={
                    listString.IDS_MT_TEXT_PLACEHOLDER_ENTER_PHONE_NUMBER
                  }
                  inputStyle={{
                    border: "none",
                    height: 52,
                    backgroundColor: "#EDF2F7",
                    borderRadius: 8,
                    paddingLeft: 16,
                  }}
                  inputProps={{
                    autoComplete: "off",
                  }}
                  startAdornment={
                    <IconPhoneContact
                      className={`svgFillAll ${classes.iconPhone}`}
                    />
                  }
                />
                <ButtonComponent
                  backgroundColor="#FF1284"
                  borderRadius={8}
                  height={52}
                  padding="10px 40px"
                  fontSize={16}
                  fontWeight={600}
                  type="submit"
                >
                  {listString.IDS_MT_TEXT_CHANGE_PASSWORD}
                </ButtonComponent>
              </Form>
            );
          }}
        </Formik>
      </Box>
    </Layout>
  );
};

export default ForgotPassword;
