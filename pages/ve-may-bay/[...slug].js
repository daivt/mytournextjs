import { bannerFlight } from "@utils/constants";
import { domesticInBound } from "@utils/domestic";
import {
  checkIsMobile,
  getFlightInfomation,
  getFlightListInfomation,
  handleTitleFlight
} from "@utils/helpers";
import dynamic from "next/dynamic";

const DesktopContent = dynamic(() =>
  import("@components/desktop/flight/home/DesktopContent")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/flight/MobileContent")
);

export default function Home({
  isMobile = false,
  query = {},
  codeFlight = {},
  codeFlightList = [],
}) {
  return (
    <>
      {isMobile ? (
        <MobileContent
          queryCodeFlight={codeFlight}
          query={query}
          codeFlightList={codeFlightList}
        />
      ) : (
        <DesktopContent
          queryCodeFlight={codeFlight}
          topLocation={[]}
          query={query}
          codeFlightList={codeFlightList}
          {...handleTitleFlight(query, codeFlight)}
        />
      )}
    </>
  );
}

export async function getServerSideProps({ params, req, res, query }) {
  let codeFlight = {};
  let codeFlightList = getFlightListInfomation(query, domesticInBound);
  if (getFlightInfomation(query, domesticInBound)) {
    codeFlight = getFlightInfomation(query, domesticInBound);
  } else {
    codeFlight = {
      code: "SGN-",
      depart: "Hồ Chí Minh",
      arrival: "",
      provinceId: 11,
      country: "Việt Nam",
      thumb: bannerFlight,
    };
  }
  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
      query: query,
      codeFlight: codeFlight,
      codeFlightList: codeFlightList,
    },
  };
}
