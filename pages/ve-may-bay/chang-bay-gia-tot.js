import { searchHotTickets } from "@api/flight";
import FlightBookingReviewCard from "@components/common/card/FlightBookingReviewCard";
import Layout from "@components/layout/mobile/Layout";
import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { formatDate, listString } from "@utils/constants";
import { isEmpty, handleTitleFlight } from "@utils/helpers";
import { DATE_FORMAT, DATE_FORMAT_BACK_END } from "@utils/moment";
import find from "lodash/find";
import minBy from "lodash/minBy";
import moment from "moment";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { domesticInBound } from "@utils/domestic";
import take from "lodash/take";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    color: theme.palette.black.black3,
    fontSize: 14,
    padding: "0 16px",
  },
  listFlightCard: {
    backgroundColor: theme.palette.gray.grayLight23,
    padding: 16,
    height: "100%",
    minHeight: "100vh",
  },
  styleWrapper: {
    marginBottom: 16,
  },
}));

const GoodPrices = ({ query = {}, codeFlight = {} }) => {
  const classes = useStyles();
  const [listData, setListData] = useState([]);
  const router = useRouter();
  useEffect(() => {
    getBestFly();
  }, []);

  const getBestFly = async () => {
    try {
      let listInBound = [];
      domesticInBound.forEach((data, index) => {
        const airportCode = data.code.split("-");
        if (airportCode[0] === "SGN") {
          listInBound.push({
            arrivalCity: data?.arrival,
            departCity: "Hồ Chí Minh",
            fromAirport: "SGN",
            toAirport: airportCode[1],
          });
        }
      });

      const itinerariesParam = take(listInBound, 5);

      const dataDTO = {
        adults: 1,
        children: 0,
        fromDate: moment().format(formatDate.firstYear),
        groupByDate: true,
        infants: 0,
        itineraries: itinerariesParam,
        toDate: moment()
          .add(15, "days")
          .format(formatDate.firstYear),
      };
      let listData = [];
      const { data } = await searchHotTickets(dataDTO);
      data.data.hotTickets.forEach((el) => {
        if (!isEmpty(el.priceOptions)) {
          const tickets = minBy(el.priceOptions, "totalPrice");
          const itinerary = {
            fromAirport: find(
              data?.data?.airports,
              (o) => o.code === el.itinerary.fromAirport
            ),
            toAirport: find(
              data?.data?.airports,
              (o) => o.code === el.itinerary.toAirport
            ),
          };
          const airlines = find(
            data?.data?.airlines,
            (o) => o.id === tickets.airlineId
          );
          listData.push({
            origin: el.itinerary.fromAirport,
            destination: el.itinerary.toAirport,
            flightFrom: itinerary.fromAirport.name,
            flightTo: itinerary.toAirport.name,
            date: tickets.departureDate,
            airlineLogo: airlines.logo,
            airlineName: airlines.name,
            discountPercent: tickets.discountAdult,
            subPrice:
              (tickets.totalPrice * (100 + tickets.discountAdult)) / 100,
            mainPrice: tickets.totalPrice,
            farePrice: tickets?.farePrice || 0,
          });
        }
      });
      setListData(listData);
    } catch (error) {}
  };

  const handleClickTicket = (item) => () => {
    router.push({
      pathname: "/ve-may-bay/result/outbound",
      query: {
        adultCount: 1,
        childCount: 0,
        infantCount: 0,
        seatSearch: "economy",
        departureDate: item.date
          ? moment(item.date, DATE_FORMAT).format(DATE_FORMAT_BACK_END)
          : moment().format(DATE_FORMAT_BACK_END),
        origin_code: item.origin,
        origin_location: item.flightFrom,
        destination_code: item.destination,
        destination_location: item.flightTo,
      },
    });
  };

  return (
    <Layout
      isHeaderBack
      titleHeaderBack={listString.IDS_TEXT_FLY_HOT_DEAL}
      {...handleTitleFlight(query, codeFlight)}
    >
      <Box className={classes.container}>
        <Typography variant="h5">{listString.IDS_TEXT_FLY_HOT_DEAL}</Typography>
        <Box component="span" color="black.black4" py={1} lineHeight="17px">
          {listString.IDS_TEXT_FLY_HOT_DEAL}
        </Box>
      </Box>
      <Box className={classes.listFlightCard}>
        {listData.map((el, index) => (
          <FlightBookingReviewCard
            key={index}
            bgCircle="#E2E8F0"
            item={el}
            styleWrapper={classes.styleWrapper}
            handleClickTicket={handleClickTicket}
          />
        ))}
      </Box>
    </Layout>
  );
};

export default GoodPrices;

export async function getServerSideProps({ params, req, res, query }) {
  return {
    props: {
      query,
    },
  };
}
