import { bannerFlight } from "@utils/constants";
import { checkIsMobile, handleTitleFlight } from "@utils/helpers";
import dynamic from "next/dynamic";

const DesktopContent = dynamic(() =>
  import("@components/desktop/flight/home/DesktopContent")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/flight/MobileContent")
);

export default function Home({
  isMobile = false,
  query = {},
  codeFlight = {},
}) {
  return (
    <>
      {isMobile ? (
        <MobileContent query={query} queryCodeFlight={codeFlight} />
      ) : (
        <DesktopContent
          isHomeFight
          topLocation={[]}
          query={query}
          {...handleTitleFlight(query, codeFlight)}
        />
      )}
    </>
  );
}

export async function getServerSideProps({ params, req, res, query }) {
  const codeFlight = {
    code: "SGN-",
    depart: "Hồ Chí Minh",
    arrival: "",
    provinceId: 11,
    country: "Việt Nam",
    thumb: bannerFlight,
  };
  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
      query: query,
      codeFlight: codeFlight,
    },
  };
}
