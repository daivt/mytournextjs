import dynamic from "next/dynamic";
import { handleTitleFlight } from "@utils/helpers";

const DesktopContent = dynamic(() =>
  import("@components/desktop/flight/payment/paymentHolding/DesktopContent")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/flight/payment/paymentHolding/MobileContent")
);

import { checkIsMobile } from "@utils/helpers";

export default function Home({
  isMobile = false,
  query = {},
  codeFlight = {},
}) {
  return (
    <>
      {isMobile ? (
        <MobileContent {...handleTitleFlight(query, codeFlight)} />
      ) : (
        <DesktopContent {...handleTitleFlight(query, codeFlight)} />
      )}
    </>
  );
}

export async function getServerSideProps({ params, req, res, query }) {
  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
      query,
    },
  };
}
