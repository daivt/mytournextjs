import { displayPaymentResult, getFlightBookingDetail } from "@api/flight";
import Layout from "@components/layout/mobile/Layout";
import PaymentResult from "@components/mobile/flight/payment/result/PaymentResult";
import { isEmpty, handleTitleFlight } from "@utils/helpers";
import PropTypes from "prop-types";
const OrderCode = ({ bookingId = "", query = {}, codeFlight = {} }) => {
  const [bookingDetail, setBookingDetail] = React.useState({});
  const [paymentType, setPaymentType] = React.useState({});

  const getFlightBookingInformation = async () => {
    try {
      const { data } = await getFlightBookingDetail({
        bookingIdEncoded: bookingId,
      });
      if (data.code === 200) {
        setBookingDetail(data.data);
      }
    } catch (error) {}
  };

  const getDisplayResult = async () => {
    try {
      const { data } = await displayPaymentResult({
        url: window.location.href,
      });
      setPaymentType(data?.paymentStatus);
    } catch (error) {}
  };

  React.useEffect(() => {
    getDisplayResult();
    getFlightBookingInformation();
  }, []);

  if (isEmpty(bookingDetail)) return null;

  return (
    <Layout {...handleTitleFlight(query, codeFlight)}>
      <PaymentResult orderDetail={bookingDetail} paymentType={paymentType} />
    </Layout>
  );
};

OrderCode.propTypes = {
  bookingId: PropTypes.string.isRequired,
};

export async function getServerSideProps({ params, query }) {
  const bookingId = params?.bookingId;
  return {
    props: { bookingId, query },
  };
}

export default OrderCode;
