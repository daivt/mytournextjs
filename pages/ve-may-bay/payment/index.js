import dynamic from "next/dynamic";
import Layout from "@components/layout/mobile/Layout";
import { handleTitleFlight } from "@utils/helpers";

const DesktopContent = dynamic(() =>
  import("@components/desktop/flight/payment/Payment")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/flight/payment/Payment")
);

import { checkIsMobile } from "@utils/helpers";

export default function Home({
  isMobile = false,
  query = {},
  codeFlight = {},
}) {
  return (
    <>
      {isMobile ? (
        <Layout isHeader={false} {...handleTitleFlight(query, codeFlight)}>
          <MobileContent />
        </Layout>
      ) : (
        <DesktopContent {...handleTitleFlight(query, codeFlight)} />
      )}
    </>
  );
}

export async function getServerSideProps({ params, req, res, query }) {
  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
      query,
    },
  };
}
