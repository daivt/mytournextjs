import { useEffect } from "react";
import Layout from "@components/layout/mobile/Layout";
import FlightHeaderListing from "@components/mobile/flight/listing/FlightHeaderListing";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { airlinesName, listEventFlight } from "@utils/constants";
import { domesticInBound } from "@utils/domestic";
import {
  checkIsMobile,
  getFlightInfomation,
  handleTitleFlight,
} from "@utils/helpers";
import dynamic from "next/dynamic";
import * as gtm from "@utils/gtm";
const DesktopContentOutBound = dynamic(() =>
  import("@components/desktop/flight/listing/DesktopContentOutBound")
);
const MobileContentOutBound = dynamic(() =>
  import("@components/mobile/flight/listing/MobileContentOutBound")
);
const DesktopContentInBound = dynamic(() =>
  import("@components/desktop/flight/listing/DesktopContentInBound")
);
const MobileContentInBound = dynamic(() =>
  import("@components/mobile/flight/listing/MobileContentInBound")
);

const useStyles = makeStyles((theme) => ({
  headerListing: {
    position: "sticky",
    backgroundColor: theme.palette.white.main,
    zIndex: 20,
    top: 0,
  },
}));
export default function Home({
  isMobile = false,
  query,
  codeFlight = {},
  isOutbound = true,
  airlinesId = 0,
}) {
  const classes = useStyles();
  useEffect(() => {
    gtm.addEventGtm(listEventFlight.FlightViewListTicket);
  }, []);

  useEffect(() => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  }, []);
  if (isOutbound) {
    return (
      <>
        {isMobile ? (
          <Layout {...handleTitleFlight(query, codeFlight)}>
            <Box position="sticky" top={0} zIndex={1}>
              <Box className={classes.headerListing}>
                <FlightHeaderListing dataQuery={query} isHideScroll={false} />
              </Box>
            </Box>
            <MobileContentOutBound dataQuery={query} />
          </Layout>
        ) : (
          <DesktopContentOutBound
            dataQuery={query}
            codeFlight={codeFlight}
            airlinesId={airlinesId}
            query={query}
            {...handleTitleFlight(query, codeFlight)}
          />
        )}
      </>
    );
  } else {
    return (
      <>
        {isMobile ? (
          <Layout>
            <Box position="sticky" top={0} zIndex={1}>
              <Box className={classes.headerListing}>
                <FlightHeaderListing dataQuery={query} isHideScroll={true} />
              </Box>
            </Box>
            <MobileContentInBound dataQuery={query} />
          </Layout>
        ) : (
          <DesktopContentInBound
            dataQuery={query}
            codeFlight={codeFlight}
            query={query}
            {...handleTitleFlight(query, codeFlight)}
          />
        )}
      </>
    );
  }
}

export async function getServerSideProps({ params, req, res, query }) {
  let codeFlight = {};
  if (getFlightInfomation(query, domesticInBound)) {
    codeFlight = getFlightInfomation(query, domesticInBound);
  } else {
    codeFlight = {
      code: "SGN-HAN",
      depart: "Hồ Chí Minh",
      arrival: "Hà Nội",
      provinceId: 11,
      country: "Việt Nam",
      thumb:
        "https://content.skyscnr.com/02f6535440ff778b7292a2c90cf4e027/GettyImages-476258421.jpg",
    };
  }

  return {
    props: {
      isMobile: checkIsMobile(req.headers["user-agent"]),
      query: query,
      codeFlight: codeFlight,
      isOutbound: params?.slug[0] === "outbound",
      airlinesId: params?.slug[1]
        ? airlinesName.find(
            (element) => params?.slug[1].indexOf(element?.name) !== -1
          )?.id
        : 0,
    },
  };
}
