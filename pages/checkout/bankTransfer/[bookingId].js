import { getFlightBookingDetail } from "@api/flight";
import { paymentTypes } from "@utils/constants";
import { checkIsMobile, isEmpty, handleTitleFlight } from "@utils/helpers";
import dynamic from "next/dynamic";
import { useEffect, useState } from "react";

const DesktopContent = dynamic(() =>
  import("@components/desktop/flight/payment/result/PaymentResult")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/flight/payment/result/PaymentResult")
);

export default function Booking({
  bookingId = "",
  isMobile = false,
  query = {},
  codeFlight = {},
}) {
  const [bookingDetail, setBookingDetail] = useState({});

  const getFlightBookingInformation = async () => {
    try {
      const { data } = await getFlightBookingDetail({
        id: bookingId,
      });
      if (data.code === 200) {
        setBookingDetail(data.data);
      }
    } catch (error) {}
  };

  useEffect(() => {
    getFlightBookingInformation();
  }, []);

  if (isEmpty(bookingDetail)) return null;

  return (
    <>
      {isMobile ? (
        <MobileContent
          orderDetail={bookingDetail}
          paymentType={paymentTypes.SUCCESS}
          fromBankTransfer={true}
          {...handleTitleFlight(query, codeFlight)}
        />
      ) : (
        <DesktopContent
          orderDetail={bookingDetail}
          paymentType={paymentTypes.SUCCESS}
          fromBankTransfer={true}
          {...handleTitleFlight(query, codeFlight)}
        />
      )}
    </>
  );
}

export async function getServerSideProps({ params, req, query }) {
  const bookingId = params?.bookingId.substr(4);
  return {
    props: {
      bookingId,
      isMobile: checkIsMobile(req.headers["user-agent"]),
      query,
    },
  };
}
