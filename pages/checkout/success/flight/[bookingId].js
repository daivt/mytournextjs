import { displayPaymentResult, getFlightBookingDetail } from "@api/flight";
import { checkIsMobile, isEmpty, handleTitleFlight } from "@utils/helpers";
import dynamic from "next/dynamic";
import { useEffect, useState } from "react";
const DesktopContent = dynamic(() =>
  import("@components/desktop/flight/payment/result/PaymentResult")
);
const MobileContent = dynamic(() =>
  import("@components/mobile/flight/payment/result/PaymentResult")
);

export default function Booking({
  bookingId = "",
  isMobile = false,
  query = {},
  codeFlight = {},
}) {
  const [bookingDetail, setBookingDetail] = useState({});
  const [paymentType, setPaymentType] = useState({});

  const getFlightBookingInformation = async () => {
    try {
      const { data } = await getFlightBookingDetail({
        bookingIdEncoded: bookingId,
      });
      if (data.code === 200) {
        setBookingDetail(data.data);
      }
    } catch (error) {}
  };

  const getDisplayResult = async () => {
    try {
      const { data } = await displayPaymentResult({
        url: window.location.href,
      });
      setPaymentType(data?.paymentStatus);
    } catch (error) {}
  };

  useEffect(() => {
    getDisplayResult();
    getFlightBookingInformation();
  }, []);

  if (isEmpty(bookingDetail)) return null;

  return (
    <>
      {isMobile ? (
        <MobileContent
          orderDetail={bookingDetail}
          paymentType={paymentType}
          {...handleTitleFlight(query, codeFlight)}
        />
      ) : (
        <DesktopContent
          orderDetail={bookingDetail}
          paymentType={paymentType}
          {...handleTitleFlight(query, codeFlight)}
        />
      )}
    </>
  );
}

export async function getServerSideProps({ params, req, query }) {
  const bookingId = params?.bookingId;
  return {
    props: {
      bookingId,
      isMobile: checkIsMobile(req.headers["user-agent"]),
      query,
    },
  };
}
