import { makeStyles } from "@material-ui/styles";
const utilStyles = makeStyles((theme) => ({
  container: {
    maxWidth: 1188,
    margin: "0 auto",
    [theme.breakpoints.down("md")]: {
      width: "100%",
      margin: 0,
      padding: "0 25px",
    },
  },
  mgT5: {
    marginTop: 5,
  },
  scrollViewHorizontal: {
    display: "flex",
    width: "100%",
    overflowX: "auto",
    overflowY: "hidden",
    "&::-webkit-scrollbar": {
      display: "none",
    },
  },
}));

export default utilStyles;
