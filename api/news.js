/**
 * Lấy thông tin trang danh sách tin tức
 * @param categoryId | number | Id chuyên mục
 * @param offset | number
 * @param limit | number
 * @returns {string}
 */
export const fetchNewsListing = ({ id, offset, limit }) => {
  return `query {
    newsListing(offset: ${offset} limit: ${limit} categoryId: ${id}) {
      total
      news{
        id
        date
        picture
        shortDesc
        slug
        title
        totalView
      }
      breadcrumb(categoryId:${id}){
        name
        link
      }
    }
  }`;
};

/**
 * Lấy thông tin chi tiết trang tin tức
 * @param ids | array | mảng chưa ID tin tức
 * @returns {string}
 */
export const fetchNewsDetail = ({ nId }) => {
  return `query 
  {
    news(ids:${[nId]}){
      id
      breadcrumb{
        link
        name
      }
      categoryId
      date
      desc
      lastestNews{
        id
        categoryId
        date
        picture
        slug
        title
        totalView
      }
      title
      totalView
    }
  }`;
};
