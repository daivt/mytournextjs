import api from "@utils/api";
import * as constants from "@utils/constants";

export const fetListCity = (data) => {
  const option = {
    method: "post",
    url: "/graphql",
    data,
    noAuthentication: true,
  };
  return api(option);
};

export const hotelAutoComplete = (params = {}) => {
  const option = {
    method: "get",
    url: "/v3/suggestions/auto-complete",
    params,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const getBannerNews = (params = {}) => {
  const option = {
    method: "get",
    url: "/homepage/breakingNews",
    params,
    noAuthentication: true,
    severVice: constants.FLIGHT_SERVICE,
  };
  return api(option);
};

export const getTopLocation = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/locations/top-locations",
    data,
    noAuthentication: true,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const getHotelsHotDeal = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/hotels/hot-deal",
    data,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const getTopHotels = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/locations/top-hotels",
    data,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const getChainsHotels = (params = {}) => {
  return api({
    method: "post",
    url: "/v3/chains",
    severVice: constants.HOTEL_SERVICE,
    data: { page: 1, size: 12, ...params },
  });
};

export const getListPost = (params = {}) => {
  const option = {
    method: "get",
    url: "/app/location/get-list-post",
    params,
    // noAuthentication: true,
    severVice: constants.MY_TOUR_SERVICE,
  };
  return api(option);
};

export const getDetailPost = (params = {}) => {
  const option = {
    method: "get",
    url: "/app/location/get-detail-post",
    params,
    // noAuthentication: true,
    severVice: constants.MY_TOUR_SERVICE,
  };
  return api(option);
};

export const getAllProvinces = (data = {}) => {
  const option = {
    method: "post",
    url: "/static/provinces",
    data,
    noAuthentication: true,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};
export const getAllHotels = (data = {}) => {
  const option = {
    method: "post",
    url: "/static/hotels",
    data,
    noAuthentication: true,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const resolveHotel = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/hotels/resolve-hotel",
    data,
    noAuthentication: true,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};
