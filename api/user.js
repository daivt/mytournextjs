import api from "@utils/api";
import * as constants from "@utils/constants";

export const sendSignupOTP = (data = {}) => {
  const option = {
    method: "post",
    url: "/sendSignupOTP",
    data,
    noAuthentication: true,
    severVice: constants.ACCOUNT_SERVICE,
  };
  return api(option);
};

export const signupAccount = (data = {}) => {
  const option = {
    method: "post",
    url: "/signupV2",
    data,
    noAuthentication: true,
    severVice: constants.ACCOUNT_SERVICE,
  };
  return api(option);
};

export const LoginAccount = (data = {}) => {
  const option = {
    method: "post",
    url: "/login",
    data,
    noAuthentication: true,
    severVice: constants.AUTH_SERVICE,
  };
  return api(option);
};

export const LogoutAccount = (access_token = "") => {
  const option = {
    method: "get",
    url: `/account/logout/${access_token}`,
    severVice: constants.FLIGHT_SERVICE,
  };
  return api(option);
};

export const validateAccessToken = (params = {}) => {
  const option = {
    method: "get",
    url: "/validateAccessToken",
    params,
    severVice: constants.ACCOUNT_SERVICE,
  };
  return api(option);
};

export const addFavorite = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/favorites/add",
    data,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const removeFavorite = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/favorites/remove",
    data,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const getListFavorite = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/favorites/hotels",
    data,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const LoginAccountViaFbGg = (data = {}) => {
  const option = {
    method: "post",
    url: "/loginOauth2",
    data,
    severVice: constants.ACCOUNT_SERVICE,
  };
  return api(option);
};

export const sendForgotPasswordOTP = (data = {}) => {
  const option = {
    method: "post",
    url: "/sendForgotPasswordOTP",
    data,
    noAuthentication: true,
    severVice: constants.ACCOUNT_SERVICE,
  };
  return api(option);
};
export const forgotPasswordV2 = (data = {}) => {
  const option = {
    method: "post",
    url: "/forgotPasswordV2",
    data,
    noAuthentication: true,
    severVice: constants.ACCOUNT_SERVICE,
  };
  return api(option);
};
export const uploadAvatar = (data = {}) => {
  const option = {
    method: "post",
    url: "/photos/upload",
    data,
    severVice: constants.FLIGHT_SERVICE,
  };
  return api(option);
};
export const updateUserInfo = (data = {}) => {
  const option = {
    method: "post",
    url: "/update/metaData",
    data,
    severVice: constants.ACCOUNT_SERVICE,
  };
  return api(option);
};
