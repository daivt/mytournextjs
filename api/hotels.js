import api from "@utils/api";
import * as constants from "@utils/constants";

export const getHotelsAvailability = (data = {}, headers = {}) => {
  const option = {
    method: "post",
    url: "/v3/hotels/availability",
    data: {
      ...data,
      useBasePrice: true,
    },
    headers,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const getHotelDetails = (data = {}, headers = {}) => {
  const option = {
    method: "post",
    url: "/v3/hotels/detail",
    data,
    headers,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const getHotelsRoom = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/rooms/availability",
    data: {
      ...data,
      useBasePrice: true,
    },
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const getHotelReviews = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/reviews",
    data,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const getTripAdvisorReviews = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/reviews/tripadvisor",
    data,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const getInterestingPlaces = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/interesting-places",
    data,
    noAuthentication: true,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const getRoomsRates = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/rooms/rates",
    data,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const createBookingsBook = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/bookings/create",
    data,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const updateBookingsBook = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/bookings/update",
    data,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const getBookingInfo = (params = {}) => {
  const option = {
    method: "get",
    url: "/v3/bookings/detail",
    params,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const getPaymentMethods = (data = {}) => {
  const option = {
    method: "post",
    url: "/payment-methods",
    data,
    severVice: constants.PAYMENT_SERVICE,
  };
  return api(option);
};

export const getListPromotionCode = (data = {}) => {
  const option = {
    method: "post",
    url: "/promotion-codes",
    data,
    severVice: constants.PROMOTION_SERVICE,
  };
  return api(option);
};

export const getDetailsPromotionCode = (params = {}) => {
  const option = {
    method: "get",
    url: "/promotion-codes",
    params,
    severVice: constants.PROMOTION_SERVICE,
  };
  return api(option);
};

export const getDiscountPromotionCode = (data = {}) => {
  const option = {
    method: "post",
    url: "/check-code",
    data,
    severVice: constants.PROMOTION_SERVICE,
  };
  return api(option);
};

export const createPaymentForBooking = (data = {}) => {
  const option = {
    method: "post",
    url: "/payments",
    data,
    severVice: constants.PAYMENT_SERVICE,
  };
  return api(option);
};

export const checkingPaymentStatus = (data = {}) => {
  const option = {
    method: "post",
    url: "/payments/status",
    data,
    severVice: constants.PAYMENT_SERVICE,
  };
  return api(option);
};

export const getPaymentInfo = (data = {}) => {
  const option = {
    method: "post",
    url: "payments/info",
    data,
    severVice: constants.PAYMENT_SERVICE,
  };
  return api(option);
};

export const getMyBooking = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/accounts/bookings",
    data,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};

export const getBookingDetails = (params = {}) => {
  const option = {
    method: "get",
    url: "/v3/accounts/booking-details",
    params,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};
export const getPromotions = (data = {}) => {
  const option = {
    method: "post",
    url: "/promotion-codes",
    data,
    severVice: constants.PROMOTION_SERVICE,
  };
  return api(option);
};
export const getChainsHotel = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/chains/top-hotels",
    data,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};
export const addReviewRating = (data = {}) => {
  const option = {
    method: "post",
    url: "/v3/reviews/rating",
    data,
    severVice: constants.HOTEL_SERVICE,
  };
  return api(option);
};
export const uploadImage = (data = {}) => {
  const option = {
    method: "post",
    url: "/photos/upload",
    data,
    severVice: constants.FLIGHT_SERVICE,
  };
  return api(option);
};
