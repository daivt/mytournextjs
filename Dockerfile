#FROM bitnami/node:13 as builder
FROM node:14.15.0-stretch-slim as builder
RUN apt-get update && apt-get install -y --no-install-recommends git openssh-server
ARG SSH_PRIVATE_KEY
RUN mkdir /root/.ssh/ \
    && echo ${SSH_PRIVATE_KEY} >> /root/.ssh/id_rsa
RUN sed -i 's/\s\+/\n/g' /root/.ssh/id_rsa \
    && sed -i ':a;$!{N;1,4 s/\n/ /;ba;}' /root/.ssh/id_rsa \
    && sed -e :a -e '$q;N;5,$D;ba' /root/.ssh/id_rsa > /root/.ssh/end_id_rsa \
    && sed -i ':a;$!{N;s/\n/ /;ba;}' /root/.ssh/end_id_rsa \
    && sed -i ':a;$!N;1,4ba;P;$d;D' /root/.ssh/id_rsa \
    && cat /root/.ssh/end_id_rsa >> /root/.ssh/id_rsa \
    && chmod 600 /root/.ssh/id_rsa
RUN ssh-keyscan gitlab.mytour.vn 52.77.6.204 >> /root/.ssh/known_hosts
WORKDIR /app
COPY package.json /app
RUN npm install --unsafe-perm
#RUN npm install
COPY . /app
RUN yarn build

#FROM bitnami/node:13-prod
FROM node:14.15.0-stretch-slim
RUN echo "Asia/Ho_Chi_Minh" > /etc/timezone \
    && rm /etc/localtime \
    && dpkg-reconfigure -f noninteractive tzdata
RUN mkdir -p /home/node/ver5-ui
WORKDIR /home/node/ver5-ui
RUN chown node:node /home/node/ver5-ui
USER node
COPY --from=builder /app .
# RUN chown -R node:node /home/node

EXPOSE 8077

CMD ["yarn", "start"]